package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;

/**
 * Class to test the EiMarketPlace Event Listing Fetcher.
 */
public class TicketMasterEventListingFetcherTest {	
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher()  throws Exception {
		EventListingFetcher eventListingFetcher = new TicketMasterEventListingFetcher();
				
		Object[][] queries = {
				{"South Pacific", null, queryDateFormat.parse("2009/09/01 00:00:00"), queryDateFormat.parse("2009/10/01 23:59:59")},
				{"Sparks", null, queryDateFormat.parse("2009/09/08 00:00:00"), queryDateFormat.parse("2009/09/12 23:59:59")},
				{"THE PHANTOM OF THE OPERA", null, queryDateFormat.parse("2009/09/25 00:00:00"), queryDateFormat.parse("2009/09/28 00:00:00")},
				{"US Open", null, queryDateFormat.parse("2009/09/10 00:00:00"), queryDateFormat.parse("2009/09/20 00:00:00")}
		};
		
		for (Object[] query: queries) {
			Date startDate = new Date();
			System.out.println("TICKETMASTER URL=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3],null,false,false);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENTHIT=" + eventHit);
			}

			Date endDate = new Date();
			System.out.println("************** Test " + query[0] + " took " + (((endDate.getTime() - startDate.getTime()) / 1000)) + " s");
		}		
	}
}
