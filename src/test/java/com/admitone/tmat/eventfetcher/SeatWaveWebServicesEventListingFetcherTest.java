package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingFetcher;

/**
 * Class to test the SeatWave Event Listing Fetcher.
 */
public class SeatWaveWebServicesEventListingFetcherTest {
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher() throws Exception {
		
		new ClassPathXmlApplicationContext(new String[]{
				"spring-dao-database.xml"
			});		
		
		
		EventListingFetcher eventListingFetcher = new SeatWaveWebServicesEventListingFetcher();
				
		Object[][] queries = {
				{"", "", queryDateFormat.parse("2010/02/18 00:00:00"), queryDateFormat.parse("2010/02/19 00:00:00")}				
//				{"2010 Winter Games: Ice Hockey - Mens (IH021) SVK vs. RUS", "", queryDateFormat.parse("2010/02/18 00:00:00"), queryDateFormat.parse("2010/02/19 00:00:00")}				
				//{"lady gaga", "Metro Radio Arena", queryDateFormat.parse("2010/03/01 00:00:00"), queryDateFormat.parse("2010/03/30 00:00:00")}/*,
				//{"rock am ring", null, queryDateFormat.parse("2010/01/01 00:00:00"), queryDateFormat.parse("2010/12/01 00:00:00")},
				//{"metallica", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
				//{"reading festival", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")}*/
		};
		
		
		for (Object[] query: queries) {
			System.out.println("SEATWAVE URL=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3],null,false,false);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENT HIT=" + eventHit);
			}
		}		
	}
}
