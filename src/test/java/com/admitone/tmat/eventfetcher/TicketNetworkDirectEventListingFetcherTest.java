package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingFetcher;

/**
 * Class to test the TicketNetworkDirect Event Listing Fetcher.
 */
public class TicketNetworkDirectEventListingFetcherTest {
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher() throws Exception {
		EventListingFetcher eventListingFetcher = new TicketNetworkDirectEventListingFetcher();		

		Object[][] queries = {
				{"kiss", null, queryDateFormat.parse("2010/08/01 00:00:00"), queryDateFormat.parse("2010/13/31 00:00:00")}
//				{"metallica", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")}
		};
		
		
		for (Object[] query: queries) {
			System.out.println("TICKET NETWORK DIRECT QUERY=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3],null,false,false);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENT HIT=" + eventHit);
			}
		}		
	}
}
