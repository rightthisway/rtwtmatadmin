package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingFetcher;

/**
 * Class to test the Viagogo Event Listing Fetcher.
 */
public class ViagogoEventListingFetcherTest {
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher() throws Exception {
		EventListingFetcher eventListingFetcher = new ViagogoEventListingFetcher();		

		Object[][] queries = {
				{"sevilla", null, queryDateFormat.parse("2010/01/24 00:00:00"), queryDateFormat.parse("2010/01/30 23:59:59")}				
//				{"naranjo", null, queryDateFormat.parse("2010/01/01 00:00:00"), queryDateFormat.parse("2010/12/30 23:59:59")}				
//				{"sevilla karten", null, queryDateFormat.parse("2010/03/01 00:00:00"), queryDateFormat.parse("2010/03/30 23:59:59")}				
//				{"fifa world cup", null, queryDateFormat.parse("2010/06/24 00:00:00"), queryDateFormat.parse("2010/06/24 23:59:59")}/*,
//				{"u2", null, queryDateFormat.parse("2009/01/01 00:00:00"), queryDateFormat.parse("2010/12/01 00:00:00")},
//				{"festival", null, queryDateFormat.parse("2009/01/01 00:00:00"), queryDateFormat.parse("2010/12/01 00:00:00")},
//				{"reading festival", null, queryDateFormat.parse("2009/01/01 00:00:00"), queryDateFormat.parse("2010/12/01 00:00:00")}*/
		};
		
		for (Object[] query: queries) {
			System.out.println("VIAGOGO QUERY=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3],null,false,false);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENT HIT=" + eventHit);
			}
		}		
	}
}
