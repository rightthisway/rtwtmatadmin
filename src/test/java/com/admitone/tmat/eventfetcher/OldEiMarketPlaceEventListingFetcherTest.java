package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;

/**
 * Class to test the EiMarketPlace Event Listing Fetcher.
 */
public class OldEiMarketPlaceEventListingFetcherTest {	
	private static DateFormat queryDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Test(groups="eventfetcher")
	public void testEventListingFetcher()  throws Exception {
		OldEiMarketPlaceEventListingFetcher eventListingFetcher = new OldEiMarketPlaceEventListingFetcher();
		eventListingFetcher.setCredential(new EIMPSimpleCredential("admit1", "super2010"));
				
		Object[][] queries = {
				{"us open tennis", "", queryDateFormat.parse("2010/02/11 00:00:00"), queryDateFormat.parse("2010/12/12 00:00:00")},
//			{"2010-Army-Navy Game Football", "", queryDateFormat.parse("2010/12/11 00:00:00"), queryDateFormat.parse("2010/12/12 00:00:00")},
//			{"2010 Winter Games: Bobsleigh - Mens Four-man (BS005)", "", queryDateFormat.parse("2010/02/24 00:00:00"), queryDateFormat.parse("2010/02/28 00:00:00")}			
			//	{"penn state", null, queryDateFormat.parse("2009/09/26 00:00:00"), queryDateFormat.parse("2009/09/27 00:00:00")},
			//	{"jvc jazz festival", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
			//	{"metallica", "madison", queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")},
			//	{"u2", null, queryDateFormat.parse("2008/01/01 00:00:00"), queryDateFormat.parse("2010/01/01 00:00:00")}
			//	{"belmont stakes", null, queryDateFormat.parse("2010/06/01 00:00:00"), queryDateFormat.parse("2010/06/20 00:00:00")}
		};
		
		for (Object[] query: queries) {
			Date startDate = new Date();
			System.out.println("EIMARKETPLACE URL=" + query[0]);
			
			Collection<EventHit> eventHits = null;
			boolean error = false;
			try {
				eventHits = eventListingFetcher.getEventList((String)query[0], (String)query[1], (Date)query[2], (Date)query[3]);
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			
			assert error == false;
			assert eventHits.size() > 0;
			
			for(EventHit eventHit: eventHits) {
				System.out.println("EVENT HIT=" + eventHit);
			}

			Date endDate = new Date();
			System.out.println("************** Test " + query[0] + " took " + (((endDate.getTime() - startDate.getTime()) / 1000)) + " s");

		}		
	}
}
