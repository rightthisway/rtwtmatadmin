package com.admitone.tmat.ticketfetcher;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;

public class SeatWaveFeedTicketListingFetcherTest {
	
	static {
		new ClassPathXmlApplicationContext(new String[]{
				"spring-dao-database.xml"
			});
	}

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		TicketListingCrawl crawl = new TicketListingCrawl();
		crawl.setSiteId(Site.SEATWAVE_FEED);
		
		SeatWaveFeedTicketListingFetcher fetcher = new SeatWaveFeedTicketListingFetcher();
		
		boolean error = false;
		
		try {
			fetcher.fetchTicketListing(ticketHitIndexer, crawl);			
		} catch (Exception e) {
			e.printStackTrace();
			error = true;
		}
		
		assert ticketHitIndexer.getFetchedTicketHits().size() > 0;
		
		assert error == false;
	}
}