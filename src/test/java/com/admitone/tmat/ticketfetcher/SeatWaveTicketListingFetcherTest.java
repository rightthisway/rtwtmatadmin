package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the SeatWave Ticket Listing Fetcher.
 */
public class SeatWaveTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(null);
		TicketListingFetcher ticketListingFetcher = new SeatWaveWebServicesTicketListingFetcher();
				
		String[] urls = {
				"http://www.seatwave.com/fc-barcelona-tickets/camp-nou-tickets/21-february-2010/perf/238609"
				// "http://www.seatwave.com/rod-stewart-tickets/the-o2-arena-tickets/01-june-2010/perf/280542"
				//"http://www.seatwave.com/real-madrid-fc-tickets/estadio-santiago-bernabeu-tickets/11-april-2010/perf/238725",
				//"http://www.seatwave.com/billy-elliot-tickets/victoria-palace-theatre-tickets/19-december-2009/perf/141199",
				//"http://www.seatwave.com/readingfestivaltickets/season"
		};
		
		String[] wrongUrls = {
				/*
				"http://www.seatwave.com/rock-am-ring-tickets/season",
				"http://www.seatwave.com/u2/season",
				"http://www.seatwave.com/chicago-bulls-vs-utah-jazz-tickets/the-o2-arena-tickets/06-october-2009/perf/1000"
				*/
		};
		
		for (String url: urls) {
			System.out.println("SEATWAVE URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.SEATWAVE);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			for (TicketHit ticketHit: ticketHits) {
				System.out.println("TICKET HIT=" + ticketHit);
			}			
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			System.out.println("SEATWAVE URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.SEATWAVE);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();

			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		

	}
}
