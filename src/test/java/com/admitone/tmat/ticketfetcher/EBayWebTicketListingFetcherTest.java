package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.EBayWebTicketItemFetcher;
import com.admitone.tmat.ticketfetcher.EBayWebTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the EBayWeb Ticket Listing Fetcher.
 */
public class EBayWebTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new EBayWebTicketListingFetcher();
		ticketListingFetcher.setTicketItemFetcher(new EBayWebTicketItemFetcher());
	
		// eBay queries
		String[][] queries = {
				{"2009", "12", "12", "metallica"},
				{"2009", "6", "13", "billy elliot"},
				{"2009", "6", "28", "brewers giants"}
		};

		String[][] wrongQueries = {
				{"2001", "1", "2", "metallica"},
				{"2002", "2", "3", "billy elliot"},
				{"2003", "3", "4", "brewers giants"}
		};

		for (String[] query: queries) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EBAY);
			ticketListingCrawl.setExtraParameter("queryYear", query[0]);
			ticketListingCrawl.setExtraParameter("queryMonth", query[1]);
			ticketListingCrawl.setExtraParameter("queryDay", query[2]);
			ticketListingCrawl.setExtraParameter("queryString", query[3]);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String[] query: wrongQueries) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.EBAY);
			ticketListingCrawl.setExtraParameter("queryYear", query[0]);
			ticketListingCrawl.setExtraParameter("queryMonth", query[1]);
			ticketListingCrawl.setExtraParameter("queryDay", query[2]);
			ticketListingCrawl.setExtraParameter("queryString", query[3]);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
