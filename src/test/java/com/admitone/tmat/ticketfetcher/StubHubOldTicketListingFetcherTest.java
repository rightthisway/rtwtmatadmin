package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.StubHubOldTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the Old StubHub Ticket Listing Fetcher.
 */
public class StubHubOldTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10000);
		TicketListingFetcher ticketListingFetcher = new StubHubOldTicketListingFetcher();
				
		String[] urls = {
				"http://www.stubhub.com/?event_id=721578"/*,
				"http://www.stubhub.com/us-open-tennis-tickets/us-open-tennis-championship-8-31-2009-765236/",
				"http://www.stubhub.com/?event_id=721477",
				"http://www.stubhub.com/boise-state-football-tickets/boise-state-vs-san-jose-state-10-31-2009-816143/",
				"http://www.stubhub.com/metallica-tickets/metallica-san-jose-hp-pavilion-12-12-2009-812105/",
				"http://www.stubhub.com/billy-elliot-new-york-tickets/billy-elliot-new-york-1-3-2010-780498/"	*/			
		};
		
		String[] wrongUrls = {
				"http://www.stubhub.com/us-open-tennis-tickets/us-open-tennis-championship-8-31-2009-000001/",
				"http://www.stubhub.com/?event_id=1"
		};
		
		for (String url: urls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			System.out.println("TICKETS INDEXED="  + ticketHits.size());

			ticketHitIndexer.reset();
		}
		
		for (String url: wrongUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			System.out.println("TICKETS INDEXED="  + ticketHits.size());			
			
			ticketHitIndexer.reset();
		}		
		
	}
}
