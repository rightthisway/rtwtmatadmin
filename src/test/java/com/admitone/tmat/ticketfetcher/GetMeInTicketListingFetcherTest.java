package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.GetMeInTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the GetMeIn Ticket Listing Fetcher.
 */
public class GetMeInTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(100);
		TicketListingFetcher ticketListingFetcher = new GetMeInTicketListingFetcher();
				
		String[] urls = {
				"http://www.getmein.com/tickets/lady-gaga-tickets/sheffield-161546.html"
				//"http://www.getmein.com/tickets/hard-rock-calling-feat-paul-mccartney-tickets/london-166122.html"
//				"http://www.getmein.com/tickets/5th-ashes-test-england-v-australia-tickets/london-97850.html",
//				"http://www.getmein.com/tickets/michael-jackson-tickets/london-119861.html",
//				"http://www.getmein.com/tickets/wimbledon-tickets/london-110120.html",
//				"http://www.getmein.com/tickets/u2-tickets/zagreb-125754.html"
		};

		String[] wrongUrls = {
				"http://www.getmein.com/tickets/5th-ashes-test-england-v-australia-tickets/london-10000.html",
				"http://www.getmein.com/tickets/u2-tickets/zagreb-12345.html"
		};

		for (String url: urls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.GET_ME_IN);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			System.out.println("URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.GET_ME_IN);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		

	}
}
