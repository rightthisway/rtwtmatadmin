package com.admitone.tmat.ticketfetcher;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the StubHub Ticket Listing Fetcher.
 */
public class TicketNetworkWebAndWSCsvTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer wsTicketHitIndexer = new TestTicketHitIndexer(10000);
		TestTicketHitIndexer webTicketHitIndexer = new TestTicketHitIndexer(10000);
		TicketListingFetcher wsTicketListingFetcher = new TicketNetworkDirectTicketListingFetcher();
		TicketListingFetcher webTicketListingFetcher = new TicketNetworkTicketListingFetcher();
				
		String url = "http://zeromarkup.com/ResultsTicket.aspx?evtid=1276771&event=James+Taylor+%26+Carole+King";
		TicketListingCrawl webTicketListingCrawl = new TicketListingCrawl();
		webTicketListingCrawl.setSiteId(Site.TICKET_NETWORK);
		webTicketListingCrawl.setQueryUrl(url);
		webTicketListingCrawl.resetStats();

		TicketListingCrawl wsTicketListingCrawl = new TicketListingCrawl();
		wsTicketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
		wsTicketListingCrawl.setQueryUrl(url);
		wsTicketListingCrawl.resetStats();
		
		

		boolean error = false;
		try {
			wsTicketListingFetcher.fetchTicketListing(wsTicketHitIndexer, wsTicketListingCrawl);
			webTicketListingFetcher.fetchTicketListing(webTicketHitIndexer, webTicketListingCrawl);
		} catch (InterruptedTicketListingCrawlException e) {
			// do nothing
		} catch (Exception e) {
			e.printStackTrace();
			error = true;
		}
		assert error == false;
		
		for(TicketHit ticketHit: wsTicketHitIndexer.getFetchedTicketHits()) {			
			System.out.println(String.format("%1$s,%2$s,=\"%3$s\",%4$s,%5$f,%6$s", 
					ticketHit.getSection(),
					ticketHit.getRow(),
					ticketHit.getSeat(),
					ticketHit.getQuantity(),
					ticketHit.getBuyItNowPrice(),
					"TND"));
		}
		for(TicketHit ticketHit: webTicketHitIndexer.getFetchedTicketHits()) {			
			System.out.println(String.format("%1$s,%2$s,=\"%3$s\",%4$s,%5$f,%6$s", 
					ticketHit.getSection(),
					ticketHit.getRow(),
					ticketHit.getSeat(),
					ticketHit.getQuantity(),
					ticketHit.getBuyItNowPrice(),
					"ZeroMarkup"));
		}

		
	}
}
