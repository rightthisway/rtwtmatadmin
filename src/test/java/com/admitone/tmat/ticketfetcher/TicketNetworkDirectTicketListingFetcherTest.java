package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;

/**
 * Class to test the TicketNetworkDirect(WS) Ticket Listing Fetcher.
 */
public class TicketNetworkDirectTicketListingFetcherTest {

	// comment it as the webservice of TND is sometimes down
	// @Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new TicketNetworkDirectTicketListingFetcher();
	
		// eBay queries
		String[] queryEventIds = {				
				"1328443", // lady gaga
				"1040361", // billy elliot
				"1053168" // boise vs broncos
		};

		String[] wrongQueryEventIds = {				
				"1000",
				"1001"
		};

		for (String queryEventId: queryEventIds) {
			System.out.println("QUERY EVENT ID=" + queryEventId);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
			ticketListingCrawl.setExtraParameter("queryEventId", queryEventId);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String queryEventId: wrongQueryEventIds) {
			System.out.println("QUERY EVENT ID=" + queryEventId);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
			ticketListingCrawl.setExtraParameter("queryEventId", queryEventId);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		

	}
}
