package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.ViagogoTicketListingFetcher;

/**
 * Class to test the Viagogo Ticket Listing Fetcher.
 */
public class ViagogoTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new ViagogoTicketListingFetcher();
				
		String[] urls = {
				"http://www.viagogo.de/Entradas-Deportes/Futbol/La-Liga-Primera-Division/FC-Barcelona-Entradas/E-214400"
//				"http://www.viagogo.es/Entradas-Deportes/Tenis/Madrid-Open-ATP-Masters-Series-Entradas/E-228668"
//				"http://www.viagogo.com/Sports-Tickets/Soccer/International-soccer/Spanish-Premier-League/FC-Barcelona-Tickets/E-214559"
//				"http://www.viagogo.com/Sports-Tickets/Soccer/International-soccer/Spanish-Premier-League/FC-Barcelona-Tickets/E-214400"
//				"http://www.viagogo.com/Sports-Tickets/Basketball/NBA/NBA-Europe-Tickets/E-194054",
//				"http://www.viagogo.com/Sports-Tickets/Baseball/Major-League-Baseball/MLB-Regular-Season/New-York-Yankees-Tickets/E-161939",
//				"http://www.viagogo.com/Sports-Tickets/Football/NFL/Regular-Season-NFL/Indianapolis-Colts-Tickets/E-196318",
//				"http://www.viagogo.com/Concert-Tickets/Hard-Rock-Metal/Metallica-Tickets/E-197803",
//				"http://www.viagogo.com/Arts-and-Theater-Tickets/Musicals/Billy-Elliot-the-Musical-Tickets/E-186919"*/
		};
		
		String[] wrongUrls = {
				"http://www.viagogo.com/Concert-Tickets/Rock-and-Pop/Britney-Spears-Tickets/E-175028"
		};

		
		for (String url: urls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.VIAGOGO);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();			
		}		

		for (String url: wrongUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.VIAGOGO);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();			
		}		

	}
}
