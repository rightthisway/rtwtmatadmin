package com.admitone.tmat.ticketfetcher;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.StubHubFeedTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.StubHubWebTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the StubHub Ticket Listing Fetcher.
 */
public class StubHubWebAndFeedTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer feedTicketHitIndexer = new TestTicketHitIndexer(10000);
		TestTicketHitIndexer webTicketHitIndexer = new TestTicketHitIndexer(10000);
		TicketListingFetcher feedTicketListingFetcher = new StubHubFeedTicketListingFetcher();
		TicketListingFetcher webTicketListingFetcher = new StubHubWebTicketListingFetcher();
				
		String[] urls = {
				"http://www.stubhub.com/lady-gaga-tickets/lady-gaga-new-york-madison-square-garden-7-7-2010-945218/",
				"http://www.stubhub.com/lady-gaga-tickets/lady-gaga-new-york-madison-square-garden-7-9-2010-945365/",
				"http://www.stubhub.com/u2-tickets/u2-oakland-oakland-alameda-county-coliseum-6-16-2010-804684/"
/*				"http://www.stubhub.com/lady-gaga-tickets/lady-gaga-new-york-madison-square-garden-7-7-2010-945218/"
				"http://www.stubhub.com/notre-dame-football-tickets/notre-dame-vs-connecticut-11-21-2009-760689/",
				"http://www.stubhub.com/kiss-tickets/kiss-alive-35-dallas-american-airlines-center-12-6-2009-882754/",
				"http://www.stubhub.com/boise-state-football-tickets/boise-state-vs-san-jose-state-10-31-2009-816143/",
				"http://www.stubhub.com/metallica-tickets/metallica-san-jose-hp-pavilion-12-12-2009-812105/",
				"http://www.stubhub.com/billy-elliot-new-york-tickets/billy-elliot-new-york-1-3-2010-780498/"		*/		
		};
		
		Set<String> feedTicketIds = new HashSet<String>();
		Set<String> webTicketIds = new HashSet<String>();
		
		for (String url: urls) {
			System.out.println("TICKET LISTING URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				feedTicketListingFetcher.fetchTicketListing(feedTicketHitIndexer, ticketListingCrawl);
				webTicketListingFetcher.fetchTicketListing(webTicketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> feedTicketHits = feedTicketHitIndexer.getFetchedTicketHits();
			Map<String, TicketHit> feedTicketById = new HashMap<String, TicketHit>();
			for(TicketHit feedTicketHit: feedTicketHits) {
				feedTicketIds.add(feedTicketHit.getItemId());
				feedTicketById.put(feedTicketHit.getItemId(), feedTicketHit);
			}
			
			Collection<TicketHit> webTicketHits = webTicketHitIndexer.getFetchedTicketHits();
			Map<String, TicketHit> webTicketById = new HashMap<String, TicketHit>();
			for(TicketHit webTicketHit: webTicketHits) {
				webTicketIds.add(webTicketHit.getItemId());
				webTicketById.put(webTicketHit.getItemId(), webTicketHit);
			}
			
			Set<String> diffWebTicketIds = new HashSet<String>(webTicketIds);
			Set<String> diffFeedTicketIds = new HashSet<String>(feedTicketIds);

			System.out.println("TICKET INDEXED FROM FEED=" + feedTicketHits.size());
			System.out.println("TICKET INDEXED FROM WEB=" + webTicketHits.size());

			// see which ids appears in feed but not in web
			for(String ticketId: feedTicketIds) {
				diffWebTicketIds.remove(ticketId);
			}
			
			System.out.println("EXTRA TICKET IN FEED");
			for(String ticketId: diffWebTicketIds) {
				TicketHit ticketHit = webTicketById.get(ticketId);
				System.out.println("TICKET HIT=" + ticketHit);
			}
			
			// see which ids appears in web but not in feed
			for(String ticketId: webTicketIds) {
				diffFeedTicketIds.remove(ticketId);
			}

			System.out.println("MISSING TICKET IN FEED");
			for(String ticketId: diffFeedTicketIds) {
				TicketHit ticketHit = feedTicketById.get(ticketId);
				System.out.println("TICKET HIT=" + ticketHit);
			}
			/*
			// see if the delivery options are the same
			for(TicketHit webTicketHit: webTicketHits) {
				// TODO: compare the section, row, prices, ...
				TicketHit feedTicketHit = feedTicketById.get(webTicketHit.getItemId());
				if (feedTicketHit != null) {
					if (!webTicketHit.getSection().equals(feedTicketHit.getSection())) {
						System.out.println("SECTION DON'T MATCH FOR ");
						System.out.println("FEED=" + feedTicketHit);
						System.out.println("WEB =" + webTicketHit);
					}
					if (!webTicketHit.getRow().equals(feedTicketHit.getRow())) {
						System.out.println("ROW DON'T MATCH FOR ");
						System.out.println("FEED=" + feedTicketHit);
						System.out.println("WEB =" + webTicketHit);
					}
					if (!("" + webTicketHit.getTicketDeliveryType()).equals("" + feedTicketHit.getTicketDeliveryType())) {
						System.out.println("DELIVERY TYPE DON'T MATCH FOR ");
						System.out.println("FEED=" + feedTicketHit);
						System.out.println("WEB =" + webTicketHit);
					}
				}
				webTicketById.put(webTicketHit.getItemId(), webTicketHit);
			}
			*/

			
			feedTicketHitIndexer.reset();
			webTicketHitIndexer.reset();
		}
		
	}
}
