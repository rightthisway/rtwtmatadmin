package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.StubHubWebTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

/**
 * Class to test the StubHub Ticket Listing Fetcher.
 */
public class StubHubWebTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(100000);
		TicketListingFetcher ticketListingFetcher = new StubHubWebTicketListingFetcher();
				
		String[] urls = {
				"http://www.stubhub.com/?event_id=896551",
				"http://www.stubhub.com/lady-gaga-tickets/lady-gaga-new-york-madison-square-garden-7-7-2010-945218/",
				"http://www.stubhub.com/mlb-all-star-game-tickets/all-star-sunday-7-11-2010-854373/"				
				//"http://www.stubhub.com/mets-vs-marlins-4-5-2010-896151/"
//				"http://www.stubhub.com/michael-buble-tickets/michael-buble-new-york-madison-square-garden-3-20-2010-916434/?isGen2Event=1"
				//"http://www.stubhub.com/new-york-yankees-spring-training-tickets/yankees-vs-pirates-spring-training-3-9-2010-911545/"
//"http://www.stubhub.com/wwe-monday-night-raw-11-16-2009-883872/?flash=0&flash8=1"/*,
				//"http://www.stubhub.com/los-angeles-angels-playoff-tickets/angels-vs-yankees-10-22-2009-857097/"
				//"http://www.stubhub.com/notre-dame-football-tickets/notre-dame-vs-connecticut-11-21-2009-760689/",
				//"http://www.stubhub.com/kiss-tickets/kiss-alive-35-dallas-american-airlines-center-12-6-2009-882754/",
				//"http://www.stubhub.com/boise-state-football-tickets/boise-state-vs-san-jose-state-10-31-2009-816143/",
				//"http://www.stubhub.com/metallica-tickets/metallica-san-jose-hp-pavilion-12-12-2009-812105/",
				//"http://www.stubhub.com/billy-elliot-new-york-tickets/billy-elliot-new-york-1-3-2010-780498/"		*/		
		};
		
		String[] wrongUrls = {
			/*	"http://www.stubhub.com/us-open-tennis-tickets/us-open-tennis-championship-8-31-2009-000001/",
				"http://www.stubhub.com/?event_id=1"*/
		};
		
		for (String url: urls) {
			System.out.println("TICKET LISTING URL=" + url);
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			for(TicketHit ticketHit: ticketHits) {
				System.out.println("TIX=" + ticketHit);
			}
			
			System.out.println("TICKETS INDEXED="  + ticketHits.size());

			ticketHitIndexer.reset();
		}
		
		for (String url: wrongUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		
		
	}
}
