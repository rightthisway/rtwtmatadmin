package com.admitone.tmat.ticketfetcher;

import java.util.Collection;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.WSTicketsTicketListingFetcher;

/**
 * Class to test the WSTickets Ticket Listing Fetcher.
 */
public class WSTicketsTicketListingFetcherTest {

	@Test(groups="ticketfetcher")
	public void testTicketListingFetcher() {
		TestTicketHitIndexer ticketHitIndexer = new TestTicketHitIndexer(10);
		TicketListingFetcher ticketListingFetcher = new WSTicketsTicketListingFetcher();
				
		String[] urls = {
				"http://wstickets.com/football/nfl/Super_Bowl_Tickets.html",
				"http://tickets.wstickets.com/feed/tickets.asp?Siteid=1&EVID=100598",
				"http://tickets.wstickets.com/feed/tickets.asp?Siteid=1&EVID=97980",
				"http://tickets.wstickets.com/feed/tickets.asp?Siteid=1&EVID=100336"
		};
		
		String[] eimpUrls = {
				"http://wstickets.com/football/nfl/New_England_Patriots_Tickets.html",
				"http://www.wstickets.com/concerts/tickets/keith_urban_tickets.html",
				"http://www.wstickets.com/concerts/tickets/fleetwood_mac_tickets.html"
		};
		
		for (String url: urls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.WS_TICKETS);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			assert error == false;
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() > 0;
			
			ticketHitIndexer.reset();
		}		

		for (String url: eimpUrls) {
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setSiteId(Site.WS_TICKETS);
			ticketListingCrawl.setQueryUrl(url);
			ticketListingCrawl.resetStats();
			
			boolean error = false;
			String errorMessage = null;
			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
				// do nothing
			} catch (Exception e) {
				error = true;
				errorMessage = e.getMessage();
			}
			assert error == true;
			assert errorMessage.contains("Use the EventInventory crawl to handle this event");
			
			Collection<TicketHit> ticketHits = ticketHitIndexer.getFetchedTicketHits();
			assert ticketHits.size() == 0;
			
			ticketHitIndexer.reset();
		}		


	}
}
