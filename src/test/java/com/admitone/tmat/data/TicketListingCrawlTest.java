package com.admitone.tmat.data;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import com.admitone.tmat.crawler.TicketListingCrawl;

/**
 * Test class for TicketListingCrawl.
 */
public class TicketListingCrawlTest {
	
	@Test
	public void testIsEquivalent() {
		TicketListingCrawl stubHubTicketListingCrawl1 = new TicketListingCrawl();
		stubHubTicketListingCrawl1.setSiteId(Site.STUB_HUB);
		stubHubTicketListingCrawl1.setEventId(1);
		stubHubTicketListingCrawl1.setQueryUrl("http://www.stubhub.com/billy-elliot-new-york-tickets/billy-elliot-new-york-1-3-2010-780498/");

		TicketListingCrawl stubHubTicketListingCrawl2 = new TicketListingCrawl();
		stubHubTicketListingCrawl2.setSiteId(Site.STUB_HUB);
		stubHubTicketListingCrawl2.setEventId(2);
		stubHubTicketListingCrawl2.setQueryUrl("http://www.stubhub.com/metallica-tickets/metallica-san-jose-hp-pavilion-12-12-2009-812105/");

		TicketListingCrawl stubHubTicketListingCrawl3 = new TicketListingCrawl();
		stubHubTicketListingCrawl3.setSiteId(Site.STUB_HUB);
		stubHubTicketListingCrawl3.setEventId(1);
		stubHubTicketListingCrawl3.setQueryUrl("http://www.stubhub.com/billy-elliot-new-york-tickets/billy-elliot-new-york-1-3-2010-780498/");

		assert stubHubTicketListingCrawl1.isEquivalent(stubHubTicketListingCrawl1);
		assert stubHubTicketListingCrawl2.isEquivalent(stubHubTicketListingCrawl2);
		assert stubHubTicketListingCrawl3.isEquivalent(stubHubTicketListingCrawl3);
		
		assert !stubHubTicketListingCrawl1.isEquivalent(stubHubTicketListingCrawl2);
		assert !stubHubTicketListingCrawl2.isEquivalent(stubHubTicketListingCrawl1);
		assert !stubHubTicketListingCrawl2.isEquivalent(stubHubTicketListingCrawl3);
		assert !stubHubTicketListingCrawl3.isEquivalent(stubHubTicketListingCrawl2);

		assert stubHubTicketListingCrawl1.isEquivalent(stubHubTicketListingCrawl3);
		assert stubHubTicketListingCrawl3.isEquivalent(stubHubTicketListingCrawl1);

		TicketListingCrawl ebayTicketListingCrawl1 = new TicketListingCrawl();
		ebayTicketListingCrawl1.setSiteId(Site.EBAY);
		ebayTicketListingCrawl1.setEventId(1);
		Map<String, String> ebayParameters1 = new HashMap<String, String>();
		ebayParameters1.put("queryKeywords", "britney spears");		
		ebayParameters1.put("queryYear", "2009");		
		ebayParameters1.put("queryMonth", "2");		
		ebayParameters1.put("queryDay", "1");		
		ebayTicketListingCrawl1.setExtraParameterMap(ebayParameters1);

		TicketListingCrawl ebayTicketListingCrawl2 = new TicketListingCrawl();
		ebayTicketListingCrawl2.setSiteId(Site.EBAY);
		ebayTicketListingCrawl2.setEventId(1);
		Map<String, String> ebayParameters2 = new HashMap<String, String>();
		ebayParameters2.put("queryKeywords", "michael jackson");		
		ebayParameters2.put("queryYear", "2009");		
		ebayParameters2.put("queryMonth", "7");		
		ebayParameters2.put("queryDay", "10");		
		ebayTicketListingCrawl2.setExtraParameterMap(ebayParameters2);

		TicketListingCrawl ebayTicketListingCrawl3 = new TicketListingCrawl();
		ebayTicketListingCrawl3.setSiteId(Site.EBAY);
		ebayTicketListingCrawl3.setEventId(1);
		Map<String, String> ebayParameters3 = new HashMap<String, String>();
		ebayParameters3.put("queryKeywords", "britney spears");		
		ebayParameters3.put("queryYear", "2009");		
		ebayParameters3.put("queryMonth", "2");		
		ebayParameters3.put("queryDay", "1");		
		ebayTicketListingCrawl3.setExtraParameterMap(ebayParameters3);

		assert ebayTicketListingCrawl1.isEquivalent(ebayTicketListingCrawl1);
		assert ebayTicketListingCrawl2.isEquivalent(ebayTicketListingCrawl2);
		assert ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl3);

		assert !ebayTicketListingCrawl1.isEquivalent(ebayTicketListingCrawl2);
		assert !ebayTicketListingCrawl2.isEquivalent(ebayTicketListingCrawl1);
		assert !ebayTicketListingCrawl2.isEquivalent(ebayTicketListingCrawl3);
		assert !ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl2);

		assert ebayTicketListingCrawl1.isEquivalent(ebayTicketListingCrawl3);
		assert ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl1);
		
		//
		// test with null values
		//

		TicketListingCrawl ebayTicketListingCrawl4 = new TicketListingCrawl();
		ebayTicketListingCrawl4.setSiteId(null);
		ebayTicketListingCrawl4.setEventId(1);
		ebayTicketListingCrawl4.setExtraParameterMap(ebayParameters3);

		TicketListingCrawl ebayTicketListingCrawl5 = new TicketListingCrawl();
		ebayTicketListingCrawl5.setSiteId(Site.EBAY);
		ebayTicketListingCrawl5.setEventId(null);
		ebayTicketListingCrawl5.setExtraParameterMap(ebayParameters3);

		TicketListingCrawl ebayTicketListingCrawl6 = new TicketListingCrawl();
		ebayTicketListingCrawl5.setSiteId(Site.EBAY);
		ebayTicketListingCrawl5.setEventId(1);
		ebayTicketListingCrawl5.setExtraParameterMap(new HashMap<String, String>());
		
		TicketListingCrawl stubHubTicketListingCrawl4 = new TicketListingCrawl();
		stubHubTicketListingCrawl4.setSiteId(Site.STUB_HUB);
		stubHubTicketListingCrawl4.setEventId(1);
		stubHubTicketListingCrawl4.setQueryUrl(null);

		// check if no NullPointerException are thrown.
		assert !ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl4);
		assert !ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl5);
		assert !ebayTicketListingCrawl3.isEquivalent(ebayTicketListingCrawl6);
		assert !stubHubTicketListingCrawl3.isEquivalent(stubHubTicketListingCrawl4);
	}
}
