package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.enums.ArtistStatus;
import com.admitone.tmat.enums.TourType;

public class BulkInsertTest {
	public final static Integer NUM_TOURS = 500; 
	
	private Collection<Artist> generateArtists(Integer artistId) {
		Collection<Artist> artists = new ArrayList<Artist>();
		for (int i = 0; i < NUM_TOURS; i++) {
			Artist artist = new Artist();
			artist.setName("TOUR " + i);
			artist.setArtistStatus(ArtistStatus.ACTIVE);
			artist.setGrandChildTourCategoryId(51);
			artists.add(artist);
		}
		return artists;
	}
	
//	@BeforeClass(groups={"dao", "hibernate", "bulk"})
	public void init() {
		new ClassPathXmlApplicationContext(new String[]{
				"spring-dao-database.xml",
			});		
	}
	
//	@Test(groups={"dao", "hibernate", "bulk"})
	public void simpleInsertTest() {		
		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Artist> artists = generateArtists(artist.getId());
		Date start = new Date();
		for (Artist tour: artists) {
			DAORegistry.getArtistDAO().save(tour);
		}
		Date end = new Date();
		
		System.out.println(NUM_TOURS + " saved: " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		for (Artist tour: artists) {
			DAORegistry.getArtistDAO().delete(tour);
		}
		end = new Date();
		System.out.println(NUM_TOURS + " deleted: " + (end.getTime() - start.getTime()) + " ms");

		DAORegistry.getArtistDAO().delete(artist);		
	}
	
//	@Test(groups={"dao", "hibernate", "bulk"})
	public void bulkInsertTest() {

		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Artist> artists = generateArtists(artist.getId());
		Date start = new Date();
		DAORegistry.getArtistDAO().saveAll(artists);
		Date end = new Date();
		System.out.println(NUM_TOURS + " saved (BULK): " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		DAORegistry.getArtistDAO().deleteAll(artists);
		end = new Date();
		System.out.println(NUM_TOURS + " deleted (BULK): " + (end.getTime() - start.getTime()) + " ms");
		
		DAORegistry.getArtistDAO().delete(artist);
	}

//	@Test(groups={"dao", "hibernate", "bulk"})
	public void bulkSaveOrUpdateTest() {

		Artist artist = new Artist();
		artist.setName("TEST_BULK_ARTIST");
		DAORegistry.getArtistDAO().save(artist);
		
		Collection<Artist> artists = generateArtists(artist.getId());
		Date start = new Date();
		DAORegistry.getArtistDAO().saveAll(artists);
		Date end = new Date();
		System.out.println(NUM_TOURS + " saved (SaveOrUpdate BULK): " + (end.getTime() - start.getTime()) + " ms");

		start = new Date();
		DAORegistry.getArtistDAO().deleteAll(artists);
		end = new Date();
		System.out.println(NUM_TOURS + " deleted (BULK): " + (end.getTime() - start.getTime()) + " ms");
		
		DAORegistry.getArtistDAO().delete(artist);
	}

}
