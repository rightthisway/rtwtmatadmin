package com.admitone.tmat.utils;

import org.testng.annotations.Test;

import com.admitone.tmat.utils.QueryUtil;

public class QueryUtilTest {
	
	@Test(groups="query")
	public void testMatchAllTerms() {
		assert QueryUtil.matchAllTerms(null, null) == true;
		assert QueryUtil.matchAllTerms(null, "") == true;
		assert QueryUtil.matchAllTerms(null, "not empty string") == true;
		assert QueryUtil.matchAllTerms("", null) == true;
		assert QueryUtil.matchAllTerms("", "not empty string") == true;

		assert QueryUtil.matchAllTerms("query", "") == false;
		assert QueryUtil.matchAllTerms("query", null) == false;

		assert QueryUtil.matchAllTerms("madison garden", "madison garden") == true;
		assert QueryUtil.matchAllTerms("mad", "madison garden") == true;
		assert QueryUtil.matchAllTerms("wrong query", "madison garden") == false;
		assert QueryUtil.matchAllTerms("garden", "madison garden") == true;
		assert QueryUtil.matchAllTerms("madison", "madison garden") == true;
		assert QueryUtil.matchAllTerms("baden", "baden baden") == true;
		assert QueryUtil.matchAllTerms("baden baden", "baden") == true;
	}

	@Test(groups="query")
	public void testMatchAnyTerms() {
		assert QueryUtil.matchAnyTerms(null, null) == true;
		assert QueryUtil.matchAnyTerms(null, "") == true;
		assert QueryUtil.matchAnyTerms(null, "not empty string") == true;
		assert QueryUtil.matchAnyTerms("", null) == true;
		assert QueryUtil.matchAnyTerms("", "not empty string") == true;

		assert QueryUtil.matchAnyTerms("query", "") == false;
		assert QueryUtil.matchAnyTerms("query", null) == false;

		assert QueryUtil.matchAnyTerms("madison garden", "madison garden") == true;
		assert QueryUtil.matchAnyTerms("mad", "madison garden") == true;
		assert QueryUtil.matchAnyTerms("wrong query", "madison garden") == false;
		assert QueryUtil.matchAnyTerms("garden", "madison garden") == true;
		assert QueryUtil.matchAnyTerms("madison", "madison garden") == true;
		assert QueryUtil.matchAnyTerms("madison garden", "madison  park") == true;
		assert QueryUtil.matchAnyTerms("madison garden", "great garden") == true;
		assert QueryUtil.matchAnyTerms("baden", "baden baden") == true;
		assert QueryUtil.matchAnyTerms("baden baden", "baden") == true;
	}

	@Test(groups="query")
	public void testNormalizeQuery() {
		// check if special characters are removed
		assert QueryUtil.normalizeQuery("new/york     giants").equals("new york giants");
		
		// check if stopwords are removed
		assert QueryUtil.normalizeQuery("the festival in new york").equals("festival new york");

		// check if words contains stopword are not removed
		assert QueryUtil.normalizeQuery("theme of the forest").equals("theme forest");
	}
	
	@Test(groups="query")
	public void getMatchingScore() {
		assert QueryUtil.getMatchingScore("new york giants", "new york giants") == 3;
		assert QueryUtil.getMatchingScore("ny giants", "new york giants") == 1;
		assert QueryUtil.getMatchingScore(null, null) == 0;
		assert QueryUtil.getMatchingScore(null, "giants") == 0;
		assert QueryUtil.getMatchingScore("giants", null) == 0;
		assert QueryUtil.getMatchingScore("giants vs yankees", "yankees vs giants") == 3;				
	}
}
