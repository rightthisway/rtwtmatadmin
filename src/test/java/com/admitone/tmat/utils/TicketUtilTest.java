package com.admitone.tmat.utils;

import java.util.ArrayList;

import org.testng.annotations.Test;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.web.Constants;

public class TicketUtilTest {
	
	@Test(groups="util")
	public void testIsAdmitOneTicket() {
		Constants.getInstance().setAdmit1NotePattern(" .!$");
		/*
		Ticket ticket1 = new Ticket();
		ticket1.setSiteId(Site.TICKETS_NOW);				
		assert TicketUtil.isAdmitOneTicket(ticket1) == false;

		Ticket ticket2 = new Ticket();
		ticket2.setSiteId(Site.TICKETS_NOW);				
		assert TicketUtil.isAdmitOneTicket(ticket2) == true;

		Ticket ticket3 = new Ticket();
		ticket3.setSiteId(Site.TICKETS_NOW);				
		assert TicketUtil.isAdmitOneTicket(ticket3) == false;
		
		Ticket ticket4 = new Ticket();
		ticket4.setSiteId(Site.EI_MARKETPLACE);				
		assert TicketUtil.isAdmitOneTicket(ticket4) == false;

		Ticket ticket5 = new Ticket();
		ticket5.setSiteId(Site.EI_MARKETPLACE);				
		ticket5.setSeller("ADMT1");				
		assert TicketUtil.isAdmitOneTicket(ticket5) == true;
		*/
	}
	
	@Test(groups="util")
	public void testGetTicketDuplicateKey() {
		// compare tickets by having one field at the time which are modified.
		
		Ticket ticket1 = new Ticket();
		ticket1.setRemainingQuantity(2);
		//ticket1.setNormalizedSection("A");
		//ticket1.setNormalizedRow("111");
		ticket1.setSiteId(Site.TICKETS_NOW);
		ticket1.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey1 = TicketUtil.getTicketDuplicateKey(ticket1, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);		

		Ticket ticket2 = new Ticket();
		ticket2.setRemainingQuantity(2);
		//ticket2.setNormalizedSection("A");
		//ticket2.setNormalizedRow("111");
		ticket2.setSiteId(Site.TICKETS_NOW);
		ticket2.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey2 = TicketUtil.getTicketDuplicateKey(ticket2, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
		assert duplicateKey1.equals(duplicateKey2) == false;

		Ticket ticket3 = new Ticket();
		ticket3.setRemainingQuantity(1);
		//ticket3.setNormalizedSection("A");
		//ticket3.setNormalizedRow("111");
		ticket3.setSiteId(Site.TICKETS_NOW);
		ticket3.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey3 = TicketUtil.getTicketDuplicateKey(ticket3, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);		
		assert duplicateKey1.equals(duplicateKey3) == false;

		Ticket ticket4 = new Ticket();
		ticket4.setRemainingQuantity(2);
		//ticket4.setNormalizedSection("B");
		//ticket4.setNormalizedRow("111");
		ticket4.setSiteId(Site.TICKETS_NOW);
		ticket4.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey4 = TicketUtil.getTicketDuplicateKey(ticket4, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
		assert duplicateKey1.equals(duplicateKey4) == false;

		Ticket ticket5 = new Ticket();
		ticket5.setRemainingQuantity(2);
		//ticket5.setNormalizedSection("A");
		//ticket5.setNormalizedRow("5");
		ticket5.setSiteId(Site.TICKETS_NOW);
		ticket5.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey5 = TicketUtil.getTicketDuplicateKey(ticket5, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);		
		assert duplicateKey1.equals(duplicateKey5) == false;

		Ticket ticket6 = new Ticket();
		ticket6.setRemainingQuantity(2);
		//ticket6.setNormalizedSection("A");
		//ticket6.setNormalizedRow("111");
		ticket6.setSiteId(Site.TICKET_MASTER);
		ticket6.setTicketStatus(TicketStatus.ACTIVE);
		String duplicateKey6 = TicketUtil.getTicketDuplicateKey(ticket6, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);		
		assert duplicateKey1.equals(duplicateKey6) == true;

		Ticket ticket7 = new Ticket();
		ticket7.setRemainingQuantity(2);
		//ticket7.setNormalizedSection("A");
		//ticket7.setNormalizedRow("111");
		ticket7.setSiteId(Site.TICKETS_NOW);
		ticket7.setTicketStatus(TicketStatus.EXPIRED);
		String duplicateKey7 = TicketUtil.getTicketDuplicateKey(ticket7, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);		
		assert duplicateKey1.equals(duplicateKey7) == false;
	}
	
	// FIXME: broken because of new parameter of computeAdjustedPrice
	//@Test(groups="util")
	public void computeAdjustedPrice() {
		/*
		// because of the imprecision of the double type, we got an issue on the operation below
		assert TicketUtil.computeAdjustedPrice(10.0, PriceRounding.NONE, 123.56) - 135.916 < 0.0000001;

		assert TicketUtil.computeAdjustedPrice(10.0, PriceRounding.DOWN_TO_NEAREST_DOLLAR, 123.56) == 135;
		assert TicketUtil.computeAdjustedPrice(10.0, PriceRounding.UP_TO_NEAREST_DOLLAR, 123.56) == 136;

		// because of the imprecision of the double type, we got an issue on the operation below
		assert TicketUtil.computeAdjustedPrice(0.0, PriceRounding.NONE, 123.56) - 123.56 < 0.0000001;
		assert TicketUtil.computeAdjustedPrice(0.0, PriceRounding.DOWN_TO_NEAREST_DOLLAR, 123.56) == 123;
		assert TicketUtil.computeAdjustedPrice(0.0, PriceRounding.UP_TO_NEAREST_DOLLAR, 123.56) == 124;

		// because of the imprecision of the double type, we got an issue on the operation below
		assert TicketUtil.computeAdjustedPrice(-10.0, PriceRounding.NONE, 123.56) - 111.204 < 0.0000001;
		assert TicketUtil.computeAdjustedPrice(-10.0, PriceRounding.DOWN_TO_NEAREST_DOLLAR, 123.56) == 111;
		assert TicketUtil.computeAdjustedPrice(-10.0, PriceRounding.UP_TO_NEAREST_DOLLAR, 123.56) == 112;
		*/
	}
	
	@Test(groups="util")
	public void testForEmpty() {
		assert TicketUtil.removeDuplicateTickets(new ArrayList<Ticket>(), RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS).size() == 0;
	}
}
