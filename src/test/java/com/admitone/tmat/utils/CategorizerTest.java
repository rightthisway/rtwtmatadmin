package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.easymock.EasyMock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.admitone.tmat.dao.CategoryMappingDAO;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.dao.SynonymDAO;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Synonym;

public class CategorizerTest {
	public static final Integer SPORT_EVENT_ID = 1;
	public static final Integer THEATER_EVENT_ID = 2;
	public static final Integer OTHER_EVENT_ID = 3;

	public static final Integer SPORT_CATEGORY_ID = 1; 
	public static final Integer THEATER_CATEGORY_ID = 2; 
	public static final Integer OTHER_CATEGORY_ID = 3; 
	
	@BeforeClass(groups={"categorization"})
	public void init() {
		DAORegistry daoRegistry = new DAORegistry();
		
		// the synonym dao will always return the same list of synonyms whenever a class
		// will do DAORegistry.getSynonymDAO().getAll()
		SynonymDAO synonymDAOMock = EasyMock.createMock(SynonymDAO.class);
		Collection<Synonym> synonymList = new ArrayList<Synonym>();
		synonymList.add(new Synonym("center", "centre, ctr"));
		synonymList.add(new Synonym("mezzanine", "mezz"));
		synonymList.add(new Synonym("orchestra", "orch"));
		synonymList.add(new Synonym("rear mezzanine", "rmezz, rmzz"));
		EasyMock.expect(synonymDAOMock.getAll()).andReturn(synonymList).anyTimes();		
		EasyMock.replay(synonymDAOMock);
		daoRegistry.setSynonymDAO(synonymDAOMock);
		SectionRowStripper.init();
		
		// create mock for categoryMappingDAO
		CategoryMappingDAO categoryMappingDAOMock = EasyMock.createMock(CategoryMappingDAO.class);
		Collection<CategoryMapping> categoryMappingList = new ArrayList<CategoryMapping>();
		categoryMappingList.add(new CategoryMapping(SPORT_CATEGORY_ID, "100", "100", "*", "*"));
		categoryMappingList.add(new CategoryMapping(SPORT_CATEGORY_ID, "200", "200", "100", "100"));
		categoryMappingList.add(new CategoryMapping(SPORT_CATEGORY_ID, "300", "400", "*", "*"));
		categoryMappingList.add(new CategoryMapping(SPORT_CATEGORY_ID, "500", "600", "100", "200"));

		categoryMappingList.add(new CategoryMapping(THEATER_CATEGORY_ID, "orchestra center", "orchestra center", "B", "B"));
		categoryMappingList.add(new CategoryMapping(THEATER_CATEGORY_ID, "orchestra rear", "orchestra rear", "*", "*"));
		categoryMappingList.add(new CategoryMapping(THEATER_CATEGORY_ID, "orchestra 5", "orchestra 50", "C", "H"));
		categoryMappingList.add(new CategoryMapping(THEATER_CATEGORY_ID, ">mezzanine", ">mezzanine", "B", "E"));

		EasyMock.expect(categoryMappingDAOMock.getAll()).andReturn(categoryMappingList).anyTimes();
		EasyMock.replay(categoryMappingDAOMock);
		daoRegistry.setCategoryMappingDAO(categoryMappingDAOMock);
		Categorizer.init();
	}
	
	private void testCategory(String section, String row, Integer eventId, Integer expectedCatId, boolean isMatch) {
		Integer resultCatId = Categorizer.computeCategory(eventId, section, row, "test",new CategoryMapping());
		
		if (isMatch) {
			assert resultCatId != null && resultCatId == expectedCatId: "Section: " + section + " / Row: " + row + " should have been categorized";
		} else {
			assert resultCatId == null: "Section: " + section + " / Row: " + row + " should not have been categorized";			
		}
	}
	
	@Test(groups={"categorization"})
	public void testCategorizerOnSportEvents() {
		System.out.println("========================================================\n"
				         + "                       SPORT EVENT                      \n"
				         + "========================================================");
		System.out.println("Testing Category Mapping: Section: 100 / Row: *");
		testCategory("100", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("99", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("101", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: 200 / Row: 100");
		testCategory("200", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("200", "99", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("200", "101", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("199", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("201", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: 300-400 / Row: *");
		testCategory("350", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("300", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("400", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("299", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("401", "10", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: 500-600 / Row: 100-200");
		testCategory("550", "150", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("550", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("550", "200", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("550", "99", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("550", "201", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);

		testCategory("500", "150", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("500", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("500", "200", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("500", "99", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("500", "201", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);

		testCategory("600", "150", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("600", "100", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("600", "200", SPORT_EVENT_ID, SPORT_CATEGORY_ID, true);
		testCategory("600", "99", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
		testCategory("600", "201", SPORT_EVENT_ID, SPORT_CATEGORY_ID, false);
	}
	
	@Test(groups={"categorization"})
	public void testCategorizerOnTheaterEvents() {
		System.out.println("========================================================\n"
						 + "                     THEATER EVENT                      \n"
		                 + "========================================================");
		System.out.println("Testing Category Mapping: Section: orchestra center / Row: B");
		testCategory("orchestra center", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra center", "A", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra center", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra center 2", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: orchestra rear / Row: *");
		testCategory("orchestra rear", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra rear", "A", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra rear 2", "A", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: orchestra center / Row: A");
		testCategory("orchestra center", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra center", "A", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra center", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: orchestra 5-orchestra 50 / Row: C-H");
		testCategory("orchestra 25", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 25", "H", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 25", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 25", "I", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		testCategory("orchestra 5", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 5", "H", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 5", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 5", "I", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		
		testCategory("orchestra 50", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 50", "H", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("orchestra 50", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 50", "I", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		
		testCategory("orchestra 4", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 4", "H", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 4", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 4", "I", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		testCategory("orchestra 51", "C", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 51", "H", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 51", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("orchestra 51", "I", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);

		System.out.println("Testing Category Mapping: Section: >mezzanine / Row: B-E");
		testCategory("mezzanine 51", "B", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("mezzanine 51", "E", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
		testCategory("mezzanine 51", "A", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("mezzanine 51", "F", THEATER_EVENT_ID, THEATER_CATEGORY_ID, false);
		testCategory("mezzanineTest 51", "E", THEATER_EVENT_ID, THEATER_CATEGORY_ID, true);
	}
	
	@Test(groups={"categorization"})
	public void testCategorizerOnOtherEvents() {
		// in categorization, we don't distinguish between theater and sport events
		// the pattern for other events are already defined in the theater and sport events
		
	}

	@Test(enabled = false)
	public static void main(String[] args) {
		CategorizerTest test = new CategorizerTest();
		test.init();
		test.testCategorizerOnSportEvents();
		test.testCategorizerOnTheaterEvents();
	}
}
