package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.easymock.EasyMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.dao.SynonymDAO;
import com.admitone.tmat.data.Synonym;

public class SectionRowStripperTest {
	@BeforeMethod(groups={"strip", "categorization"})
	public void initMethod() {
		// the synonym dao will always return the same list of synonyms whenever a class
		// will do DAORegistry.getSynonymDAO().getAll()
		SynonymDAO synonymDAOMock = EasyMock.createMock(SynonymDAO.class);
		Collection<Synonym> list = new ArrayList<Synonym>();
		list.add(new Synonym("center", "centre, ctr"));
		list.add(new Synonym("mezzanine", "mezz"));
		list.add(new Synonym("orchestra", "orch"));
		list.add(new Synonym("rear mezzanine", "rmezz, rmzz"));
		EasyMock.expect(synonymDAOMock.getAll()).andReturn(list).anyTimes();		
		DAORegistry daoRegistry = new DAORegistry();
		daoRegistry.setSynonymDAO(synonymDAOMock);
		EasyMock.replay(synonymDAOMock);
		SectionRowStripper.init();
	}
	
	private void testStrip(String original, String transformed, String expected) {
		assert transformed.equals(expected): "The string '" + original + "' was incorrectly stripped to '" + transformed + "' and should have been stripped to '" + expected + "'";
	}
	
	@Test(groups={"strip", "categorization"})
	public void testSectionRowStripperOnSportEvents() {
		
		System.out.println("Testing Sporting Event Stripping rules: remove text before number");
		
		String test = "NICE! Section A 251 UPPER";
		Map<String, String> map = new HashMap<String, String>();
		testStrip(test, SectionRowStripper.stripSportingEvent(test,map), "251 UPPER");

		test = "NICE! Section A251 UPPER";
		testStrip(test, SectionRowStripper.stripSportingEvent(test,map), "251 UPPER");

		test = "NICE! Section 251UPPER";
		testStrip(test, SectionRowStripper.stripSportingEvent(test,map), "251UPPER");
	}
	
	@Test(groups={"strip", "categorization"})
	public void testSectionRowStripperOnTheaterEvents() {
		System.out.println("Testing Scrubbing for Theater events");
		Map<String, String> map = new HashMap<String, String>();
		String test = "NICE! Mezz 251 UPPER";
		testStrip(test, SectionRowStripper.stripTheaterEvent(test,map), "mezzanine 251");

		test = "NICE! Mezz251 UPPER";
		testStrip(test, SectionRowStripper.stripTheaterEvent(test,map), "mezzanine 251");

		test = "NICE! Mezz 251UPPER";
		testStrip(test, SectionRowStripper.stripTheaterEvent(test,map), "mezzanine");

		test = "NICE! Mezz aa251";
		testStrip(test, SectionRowStripper.stripTheaterEvent(test,map), "mezzanine");
	}
	
	@Test(groups={"strip", "categorization"})
	public void testSectionRowStripperOnOtherEvents() {
		System.out.println("Testing Other events");
		
		String test = "NICE! A 25";
		testStrip(test, SectionRowStripper.stripOtherEvent(test), test);

		test = "NICE! Mezz 456";
		testStrip(test, SectionRowStripper.stripOtherEvent(test), test);
	}
	

	@Test(enabled=false)
	public static void main(String[] args) {
		SectionRowStripperTest test = new SectionRowStripperTest();
		test.initMethod();
		test.testSectionRowStripperOnSportEvents();
		test.testSectionRowStripperOnTheaterEvents();
		test.testSectionRowStripperOnOtherEvents();
	}
}