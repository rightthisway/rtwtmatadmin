package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Date;

import org.testng.annotations.Test;

import com.admitone.tmat.dao.CacheDAORegistry;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortBroadcast;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.pojo.CastStatus;

public class ShortBroadcastUtilTest {
	
	private DAORegistry daoRegistry;
	
	public ShortBroadcastUtilTest(DAORegistry daoRegistry) {
		this.daoRegistry = daoRegistry;
	}
	
	public void testGetStatusByEvent() {

		Event testEvent = new Event();
		testEvent.setId(new Integer(10000));
		testEvent.setLocalDate(new java.sql.Date(new Date().getTime()));
		testEvent.setArtistId(new Integer(98));
		testEvent.setVenueId(new Integer(50));
		testEvent.setTimeZoneId("ACT");
		testEvent.setName("Ohio State Buckeyes vs. Usc Trojans");
		testEvent.setEventStatus(EventStatus.ACTIVE);
		
		//CastStatus status = ShortBroadcastUtil.getStatusByEvent(null, testEvent);
		//assert status == null;
		
		ArrayList<ShortBroadcast> testCasts = new ArrayList<ShortBroadcast>();
		double totalExposure = 0.00;
		for(int i = 0; i < 5 ; i++){
			ShortBroadcast testCast = new ShortBroadcast();
			testCast.setId(new Integer(20000 + i));
			testCast.setPrice(new Double(10.00 + (i*1.00)));
			testCast.setQuantity(new Integer(1 + i));
			Event event = new Event();
			event.setId(100000);
			testCast.setEvent(event);
			testCast.setSection("NoSection");
			testCast.setRow("NoRow");
			testCasts.add(testCast);
			totalExposure = totalExposure + ((10.00 + (i*1.00))*(1 + i));
		}
		/*			
		CastStatus status1 = ShortBroadcastUtil.getStatusByEvent(testCasts, testEvent);
		assert status1 != null;
		assert status1.getChildren() != null;
		assert status1.getChildren().size() == 1;
		assert status1.getExposure() != 0;
		System.out.println("Status: " + status1.getExposure() + " test: " + totalExposure);
		assert status1.getExposure() == totalExposure;
		CastStatus childStatus1 = status1.getChildren().iterator().next();
		assert childStatus1.getChildren() != null;
		assert childStatus1.getChildren().size() == 5;
		assert childStatus1.getExposure() != 0;
		assert childStatus1.getExposure() == totalExposure;
		for(CastStatus grandChildStatus1 : childStatus1.getChildren()){
			assert grandChildStatus1.getChildren() == null;
			assert grandChildStatus1.getPrice() != 0;
			assert grandChildStatus1.getQuantity() != 0;
			assert grandChildStatus1.getExposure() != 0;
			assert grandChildStatus1.getExposure() == grandChildStatus1.getPrice() * grandChildStatus1.getQuantity();
		}
		*/
	}
}
