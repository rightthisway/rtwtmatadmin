package com.admitone.tmat.utils;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.admitone.tmat.utils.redmine.RedmineAttachment;
import com.admitone.tmat.utils.redmine.WebRedmineClient;

public class WebRedmineClientTest {
	
	public WebRedmineClientTest() {
		new ClassPathXmlApplicationContext(new String[]{"spring-dao-database.xml"});
	}
	
	@Test
	public void postIssueTest() throws Exception {
		WebRedmineClient client = new WebRedmineClient();
		/*
		String result = client.postRedmineIssue(WebRedmineClient.BUG_TRACKER_ID, "POST ISSUE TEST", "DISREGARD THIS MESSAGE");
		System.out.println("REDMINE ISSUE=" + result);
		*/
		String result = client.postRedmineIssue(WebRedmineClient.BUG_TRACKER_ID, "POST ISSUE TEST WITH ATTACHMENT", "DISREGARD THIS MESSAGE", 
				new RedmineAttachment[]{new RedmineAttachment("THIS IS A TEXT CONTENT".getBytes(), "text/plain", "test.txt")}, 1);
		System.out.println("REDMINE ISSUE=" + result);

	}
}
