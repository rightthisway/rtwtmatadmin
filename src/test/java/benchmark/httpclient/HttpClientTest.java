package benchmark.httpclient;

import java.io.InputStream;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class HttpClientTest {
	private static String[] urls = {
		"http://www.zeromarkup.com/ResultsTicket.aspx?evtid=927324&event=Boston+Red+Sox+Vs.+Atlanta+Braves",
		"http://www.zeromarkup.com/ResultsTicket.aspx?evtid=1049692&event=Jersey+Boys",
		"http://zeromarkup.com/ResultsTicket.aspx?evtid=927427&event=New+York+Yankees+vs.+Boston+Red+Sox",
		"http://www.zeromarkup.com/ResultsTicket.aspx?evtid=1010404&event=Colorado+Buffaloes+Vs.+Kansas+Jayhawks"
	};

	@Test
	public void testUrlConnection() {
		System.out.println("======================================");
		System.out.println("TEST URL CONNECTION");

		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
				URL url = new URL(u);
				InputStream is = url.openStream();
				int len;
				int byteCount = 0;
				StringBuffer buffer = new StringBuffer();
				byte[] bytes = new byte[16324];
				while ((len = is.read(bytes)) > 0) {
					buffer.append(new String(bytes,0,len));
					byteCount += len;
				}				
				String content = buffer.toString();

				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}
	
	
	@Test
	public void testHttpConnection1() {
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING HttpEntityUtils.toString()");

		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
				
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				String content = EntityUtils.toString(entity);			
				
				int byteCount = content.length();

				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}
	

	@Test
	public void testHttpConnection2() {
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING REGULAR READ");

		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
				
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				
				InputStream is = entity.getContent();
				int len;
				int byteCount = 0;
				StringBuffer buffer = new StringBuffer();
				byte[] bytes = new byte[16324];
				while ((len = is.read(bytes)) > 0) {
					buffer.append(new String(bytes,0,len));
					byteCount += len;
				}				
				String content = buffer.toString();
				
				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}

	@Test()
	public void testHttpConnection3() {
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING HttpEntityUtils.toString() AND GZIP");
		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				String content = EntityUtils.toString(entity);			
				
				int byteCount = content.length();

				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}

	@Test()
	public void testHttpConnection4() {
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING REGULAR READ AND GZIP");

		int totalByteCount = 0;
		int totalFetchingTime = 0;

		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				
				InputStream is = entity.getContent();
				int len;
				int byteCount = 0;
				StringBuffer buffer = new StringBuffer();
				byte[] bytes = new byte[16324];
				while ((len = is.read(bytes)) > 0) {
					buffer.append(new String(bytes,0,len));
					byteCount += len;
				}				
				String content = buffer.toString();
				
				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}

	@Test()
	public void testHttpConnection6() throws Exception{
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING HTTPCLIENTSTORE USING HttpEntityUtils.toString()");		
		
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient();

		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				String content = EntityUtils.toString(entity);			
				
				int byteCount = content.length();
				
				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}

	
	@Test()
	public void testHttpConnection5() throws Exception{
		System.out.println("======================================");
		System.out.println("TEST HTTP CONNECTION USING HTTPCLIENTSTORE USING REGULAR READ");
		
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient();

		int totalByteCount = 0;
		int totalFetchingTime = 0;
		
		for(String u: urls) {
			try {
				long startFetchingTime = System.currentTimeMillis();
								
				HttpGet httpGet = new HttpGet(u);
				HttpResponse response = httpClient.execute(httpGet);

				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());			
				
				InputStream is = entity.getContent();
				int len;
				int byteCount = 0;
				StringBuffer buffer = new StringBuffer();
				byte[] bytes = new byte[16324];
				while ((len = is.read(bytes)) > 0) {
					buffer.append(new String(bytes,0,len));
					byteCount += len;
				}				
				String content = buffer.toString();
				
				System.out.println("Fetched page: " + u);
				System.out.println("Size of the page: " + byteCount);
				System.out.println("Time: " + (System.currentTimeMillis() - startFetchingTime) + "ms");
				System.out.println();

				totalByteCount += byteCount;
				totalFetchingTime += System.currentTimeMillis() - startFetchingTime;
			} catch(Exception e) {
				e.printStackTrace();
			}						
		}

		System.out.println("Total Size: " + totalByteCount);
		System.out.println("Total Time: " + totalFetchingTime + "ms");
	}


}