--
-- SuperBowl 2009 - Feb 1 2009
--

INSERT INTO artist (id, name) VALUES(3, 'SuperBowl');
INSERT INTO tour(id, artist_id, name, location, start_date, end_date) VALUES (3, 3, 'SuperBowl XLIII [2009]', 'Tampa, FL, United States', '2009-02-01', '2009-02-01');
INSERT INTO venue (id,  building, city, state, country) VALUES
	(41, 'Raymond James Stadium', 'Tampa', 'FL', 'US');
	
INSERT INTO event (id, tour_id, event_date, venue_id) VALUES (52, 3, '2009-02-01', 41);
INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled, extra_parameters) VALUES
	(5, 'ebay-superbowl-2009-02-01', 'ebay', 3600, 3, 52, true, "queryYear=2009\nqueryMonth=2\nqueryDay=1\nqueryString=Superbowl");

INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled, query_url) VALUES
	(6, 'fansnap-superbowl-2009-02-01', 'fansnap', 3600, 3, 52, true, 'http://www.fansnap.com/seats/60024-on');
