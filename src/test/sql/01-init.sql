-- INSERT INTO artist (id, name) VALUES (0, "No Artist");
-- UPDATE artist SET id="0" WHERE name="No Artist";
-- INSERT INTO venue (id,  building, city, state, country) VALUES (0, 'No Building', 'No City', 'No  State', 'No Country');
-- UPDATE venue SET id="0" WHERE building="No Building";
-- INSERT INTO tour(id, artist_id, name, location, start_date, end_date) VALUES (0, 0, 'No Tour', 'No Location', '1970-01-01', '1970-01-01');
-- UPDATE tour SET id=0 WHERE name="No Tour";

INSERT INTO property(name, value) VALUES ('crawler.thread.count', 5);
INSERT INTO property(name, value) VALUES ('crawler.startup.enabled', '0');
INSERT INTO property(name, value) VALUES ('crawler.email.from', 'Tixmarket Crawler <service@yoonew.com>');
INSERT INTO property(name, value) VALUES ('crawler.email.to', '');
INSERT INTO property(name, value) VALUES ('crawler.email.error.enabled', '1');
INSERT INTO property(name, value) VALUES ('crawler.email.creator.enabled', '1');
INSERT INTO property(name, value) VALUES ('crawler.email.startStop.enabled', '1');
INSERT INTO property(name, value) VALUES ('crawler.email.lastUpdater.enabled', '1');
INSERT INTO property(name, value) VALUES ('crawler.ticket.ttl', '120');

INSERT INTO property(name, value) VALUES ('smtp.host', '');
INSERT INTO property(name, value) VALUES ('smtp.port', '');
INSERT INTO property(name, value) VALUES ('smtp.username', '');
INSERT INTO property(name, value) VALUES ('smtp.password', '');
INSERT INTO property(name, value) VALUES ('smtp.secure.connection', 'no');
INSERT INTO property(name, value) VALUES ('smtp.auth', 'true');
INSERT INTO property(name, value) VALUES ('admit1.note.pattern', ' --$');
INSERT INTO property(name, value) VALUES ('eimarketplace.username', 'admit1');
INSERT INTO property(name, value) VALUES ('eimarketplace.password', 'crisp09');

INSERT INTO property(name, value) VALUES('feedback.email.to', '');
INSERT INTO property(name, value) VALUES('feedback.error.email.to', '');

INSERT INTO property(name, value) VALUES('feedback.jira.enabled', '1');
INSERT INTO property(name, value) VALUES('feedback.jira.project', 'TMAT');
INSERT INTO property(name, value) VALUES('feedback.jira.components', 'TMAT');
INSERT INTO property(name, value) VALUES('feedback.jira.affectsVersions', '1.0');
INSERT INTO property(name, value) VALUES('feedback.jira.uri', 'https://jiraa1.yoonew.com');
INSERT INTO property(name, value) VALUES('feedback.jira.rpcPath', '/rpc/xmlrpc');
INSERT INTO property(name, value) VALUES('feedback.jira.username', 'tmat-feedback');
INSERT INTO property(name, value) VALUES('feedback.jira.password', 'falafel');

INSERT INTO property(name, value) VALUES('crawler.error.readyCrawlCountThreshold', '100');
INSERT INTO property(name, value) VALUES('crawler.error.crawlDurationThreshold', '900');
INSERT INTO property(name, value) VALUES('crawler.error.noTicketsFoundError', '0');
INSERT INTO property(name, value) VALUES('crawler.error.checkFrequency', '600');

INSERT INTO property (name, value) VALUES ('feedback.redmine.enabled', '1');
INSERT INTO property (name, value) VALUES ('feedback.redmine.uri', 'http://192.168.0.32:9000');
INSERT INTO property (name, value) VALUES ('feedback.redmine.project', 'tmat');
INSERT INTO property (name, value) VALUES ('feedback.redmine.username', 'feedback');
INSERT INTO property (name, value) VALUES ('feedback.redmine.password', 'ravioli');

--
-- unknown artist
--
insert into artist values (0, "Unknown Artist");

--
-- Synonyms
--

INSERT INTO synonym (name, value) VALUES ('mezzanine', 'mezz');
INSERT INTO synonym (name, value) VALUES ('rear mezzanine', 'rmezz, rmzz');
INSERT INTO synonym (name, value) VALUES ('orchestra', 'orch');
INSERT INTO synonym (name, value) VALUES ('center', 'centre, ctr');

--
-- Sites
--

-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('ebay', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('eimarketplace', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('eventinventory', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('fansnap', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('getmein', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('razorgator', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('seatwave', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('stubhub', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('ticketmaster', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('ticketnetwork', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('ticketnetworkdirect', 'NONE', 1);
-- INSERT INTO site (id, price_rounding, max_concurrent_connections) VALUES('seatwavefeed', 'NONE', 1);

--
-- CRAWL
--

-- INSERT INTO ticket_listing_crawl (id, name, site_id, crawl_frequency, tour_id, event_id, enabled) VALUES (1, 'SEATWAVE FEED', 'seatwavefeed', 1800, null, null, 1);