<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<jsp:useBean id="now" class="java.util.Date"/>

<div id="footer">
	&copy; 2010 Right This Way LLC. This page was generated in
	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${(now.time - requestDate.time) / 1000}"/>	
	 seconds. 
</div>