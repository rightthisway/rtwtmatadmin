<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<style>
	BODY {
		<c:choose>
			<c:when test="${constants.ipAddress == '172.31.255.12'}">background-image: url(../images/bg-admitone-test.gif);</c:when>
			<c:otherwise>background-image: url(../images/bg-admitone-test.gif);</c:otherwise>
		</c:choose>
		
	}
</style>

<div style="text-align:center">

	<div style="margin-left: auto; margin-right: auto;margin-top: 40px;padding-top: 20px; padding-bottom:20px; padding-left: 10px;padding-right: 10px;width: 500px;text-align:left;background-image:url(../images/dot-white-60.png);-moz-border-radius: 20px;-webkit-border-radius: 20px;">
		<center>
		<%
		/*
			<c:choose>
				<c:when test="${constants.ipAddress == '172.31.255.12'}"><img src="../images/logo-admit-one.gif" border="0"/></c:when>
				<c:otherwise><img src="../images/logo-admit-one-test.gif" border="0"/></c:otherwise>
			</c:choose>
		 */
		 %>
		 <img src="../images/Admin Logo.jpg" border="0"/>
		</center>
		
		<h1>TMAT Login</h1>
		
		<div class="info">
			<div class="infoText">
				Welcome to TMAT! <br />Enter your account details below to login to TMAT.
			</div>
		</div>
		
		
		<c:if test="${showError}">
		  <div class="error"><div class="errorText">Username or password is incorrect! Please try again.</div></div>
		</c:if>
		
		<center>
			<form id="loginForm" action="<c:url value='../j_acegi_security_check'/>" method="POST" onsubmit="changeLoginButton()">	
			  <table style="margin-left: auto; margin-right: auto;margin-top: 20px;">
			    <tr>
			      <td class="leftLabel">Username:</td>
			      <td><input type="text" name="j_username"/></td>
			    </tr>
			    <tr>
			      <td class="leftLabel">Password:</td>
			      <td><input type="password" name="j_password"/></td>
			    </tr>
			    <tr>
			      <td></td>
			      <td><input type="checkbox" id="remember_me" name="_acegi_security_remember_me"/><label for="remember_me">Remember me</label></td>
			    </tr>
			    <tr>
			      <td colspan="2" align="center">
			      	<br />
					<input id="login" type="submit" class="medButton" value="Login" />
					<span id="loginWait" style="display:none"><img src="../images/process-running.gif" align="absbottom"/> <b>Logging in...</b></span>
			      </td>
			    </tr>
			  </table>
			</form>
		</center>
		
	  	  <c:if test="${not empty forgotPasswordInfo}">
		    <div class="info">
	  	        ${forgotPasswordInfo}
	  	    </div>
	  	  </c:if>
	  	  
	  	  <c:if test="${not empty forgotPasswordError}">
		    <div class="error">
			    <div class="errorText">
	  	        	${forgotPasswordError}
	  	        </div>
	  	    </div>
	  	  </c:if>

		  	  <c:if test="${empty success}">
		  	    <c:if test="${empty forgotPasswordError}">
		  	      <center><a href="#" id="showForgotPasswordLink" onclick="showForgotPasswordDiv()" style="display:block;overflow:hidden">I forgot my username/password</a></center>
		  	    </c:if>
			    <div id="forgotPasswordDiv" class="info" <c:if test="${empty forgotPasswordError}">style="display:none"</c:if>>
			      <div class="infoText">
			      	<center>
				      <form action="ResetPassword" method="POST">
				        Please enter your username or email address so we can send you the instructions to reset your password:<br/><br/>
				        <b>Username/Email:</b> <input type="text" name="username"/>
					    <input type="submit" class="medButton" value="Submit" />
					  </form>
			      	</center>
				  </div>
			    </div>
			  </c:if>
		</center>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$("INPUT[name='j_username']").focus();
	});

	function showForgotPasswordDiv() {
		$('#showForgotPasswordLink').hide();		
		$('#forgotPasswordDiv').css({opacity: 0});
		$('#forgotPasswordDiv').show();
		$('#forgotPasswordDiv').animate({opacity: 1.0},  500);
		$("INPUT[name='username']").focus();		
	}
	
	function changeLoginButton() {
		$('#login').hide();
		$('#loginWait').show();
		return true;
	}
</script>