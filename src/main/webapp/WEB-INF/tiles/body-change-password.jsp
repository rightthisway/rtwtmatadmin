<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div style="border:1px dashed black;  margin-top: 40px;padding: 20px; width: 500px;margin-left: auto; margin-right: auto;"
	<center>
		<img src="../images/yoonew_logo.gif" border="0"/>
	</center>
	
	<h1>Tixmarket - Password Reset</h1>
	
	<div class="info">
		<div class="infoText">
			Welcome ${username}! <br />Please set a new password.
		</div>
	</div>
	
	<c:if test="${not empty error}">
	  <div class="error"><div class="errorText">${error}</div></div>
	</c:if>

	<c:if test="${not empty info}">
	  <div class="info"><div class="infoText">${info}</div></div>
	</c:if>

    <c:choose>
      <c:when test="${success}">
        <center>
          <a href="Login">Go to the login page</a>
        </center>
      </c:when>
      <c:otherwise>
		<form id="loginForm" action="ChangePassword" method="POST">	
		  <input type="hidden" name="username" value="${username}"/>
		  <input type="hidden" name="code" value="${code}"/>
		  <table style="margin-left: auto; margin-right: auto;margin-top: 20px;">
		    <tr>
		      <td class="leftLabel">Password:</td>
		      <td><input type="password" name="password"/></td>
		    </tr>
		    <tr>
		      <td class="leftLabel">Confirm Password:</td>
		      <td><input type="password" name="password2"/></td>
		    </tr>
		    <tr>
		      <td colspan="2" align="center">
		      	<br />
				<input type="submit" name="submit" class="medButton" value="Update" />
		      </td>
		    </tr>
		  </table>
		</form>      
      </c:otherwise>
    </c:choose>	
</div>
</script>