<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Report</a> 
  &gt; TMAT Report
</div>


<h1>RTW-2 Category Sales Report</h1>

<form action="RotCategorySalesReport" method="post">

	<table align="center" width="310px;">
		<tr>
			<td colspan="2"><label id="msg"></label></td>
		</tr>
		<tr>
			<td>
			<b>Start Date: </b>

<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>
		
		<tr>
<td>
    <b>End Date: </b>

<select name="toMonth" id="toMonth" style="margin-left: 7px;">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay" >
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="toYear" id="toYear" >
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>
		
		<tr>
			<td>
			<b>Type : </b>
				<select id="reportType" name="reportType" style="margin-left: 27px;">
					<option value="ALL">ALL</option>
					<option value="AUTOCAT">TN AUTO</option>
					<option value="MINICATS">TN MINICATS</option>
					<option value="VIPCATS">TN VIP MINICATS</option>
					<option value="VIPAUTO">TN VIP AUTOCATS</option>
					<option value="LASTROW MINICATS">TN LASTROW MINICATS</option>
					<option value="VIPLR">TN VIP LASTROW</option>
					<option value="LARRYLAST">TN LARRYLAST</option>
					<option value="MANUAL">MANUAL</option>
					<option value="ZONES PRICING">ZONES PRICING</option>
					<option value="ZONED LASTROW MINICATS">ZONED LASTROW MINICATS</option>
					<option value="ZONES">ZONES</option>
					<!-- <option value="SGK">SGK LASTFIVEROW</option> -->
					<option value="VIVID MINICATS">VIVID MINICATS</option>
					<option value="VIVID LARRY">VIVID LARRY</option>
					<option value="SCOREBIG MINICATS">SCOREBIG MINICATS</option>
					<!-- <option value="SCOREBIG AUTO">SCOREBIG AUTO</option> -->
					<option value="SCOREBIG LASTROW MINICATS">SCOREBIG LASTROW MINICATS</option>
					<option value="TICKET CITY MINICATS">TICKET CITY MINICATS</option>
					<option value="TICKET CITY VIPMINICATS">TICKET CITY VIPMINICATS</option>
					<option value="TICKET CITY LASTROW MINICATS">TICKET CITY LASTROW MINICATS</option>
					<option value="FANXCHANGE LASTROW MINICATS">FANXCHANGE LASTROW MINICATS</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
			<b>Po Info : </b>
				<select id="poInfo" name="poInfo" style="margin-left: 16px;">
					<option value="ALL">ALL</option>
					<option value="WITHPO">WITH A PO</option>
					<option value="WITHOUTPO">WITHOUT A PO</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><input id="downloadReportButton" style="width:160px;margin-left:60px;" name="action" type="submit" class="medButton" value="Download Report"/></td>
		</tr>	
	</table>
</form>

<script type="text/javascript">

$(document).ready(function() {
	$('.calendar').datepicker({
		'buttonImageOnly': true,
        buttonImage: '../images/ico-calendar.gif',
        showOn: 'button',
        showButtonPanel: true,
		changeMonth: true,
		changeYear: true,	        
        dateFormat:'yy-mm-dd',
        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
         maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)
	});
	
	$('#fromCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
        }
    );

	$('#fromCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#fromYear').val(tokens[0]*1);
			$('#fromMonth').val(parseInt(tokens[1]*1));
			$('#fromDay').val(parseInt(tokens[2]*1));
		}
	);
	
	$('#toCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
        }
    );
	
	$('#toCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#toYear').val(tokens[0]*1);
			$('#toMonth').val(parseInt(tokens[1])*1);
			$('#toDay').val(parseInt(tokens[2])*1);
		}
	);			
});


</script>