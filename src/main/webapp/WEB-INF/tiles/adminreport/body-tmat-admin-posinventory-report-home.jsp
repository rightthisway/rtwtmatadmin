<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function downloadOnClick(action){
	$("#formId").submit();
}
</script>

</head>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Report</a> 
  &gt; TMAT Report
</div>
<h1>Pos Inventory Report</h1>
<body>
<br>
<br>
<div align="center">
<form:form modelAttribute="exosure" action="TMATPosInventoryReport" method="post" id="formId">


<table>

<tr>
<td><b>Select Broker:</b></td>
			<td>
					<select id="brokerId" name="brokerId">
					 <option value="" <c:if test="${brokerId eq 0}"> Selected </c:if> >Select</option>
					<!-- <option value="">Select</option> -->
					<c:forEach items="${brokers}" var="broker">
			     		<option value="${broker.id}" <c:if test="${broker.id eq brokerId}"> Selected </c:if> >${broker.name}</option>
					</c:forEach>									
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
</tr>
</table>
<br>
<table>
<tr>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick();"/></td>
</tr>
</table>
</form:form>
</div>
</body>
</html>