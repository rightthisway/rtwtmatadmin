<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript"
	src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>
		
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Report</a> &gt; TMAT Report
</div>

<h1>Events without Map.</h1>

<form action="DownloadNoMapEvents" method="post">
	<table>

		
		<tr>
			<td> <b>Select Parent Category : </b> <select name="parentCategory">
					<option value="All">All</option>
					<option value="Sports">Sports</option>
					<option value="Concerts">Concerts</option>
					<option value="Theater">Theatre</option>
					<option value="Other">Other</option>
			</select></td>
		</tr>

		<tr>
			<td><b>Select Country : </b> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<select id="countryId" name="country" multiple="multiple">
					<option value="US&CA">US & CA</option>
					<option value="others">Rest of the world</option>
					<%-- <c:forEach var="countryMap" items="${countries}">
				<option value="${countryMap.key}">${countryMap.value}</option>
			</c:forEach> --%>
			</select></td>
		</tr>
		<tr>
			</br>
			<td>&nbsp</td>
			<td><input id="downloadReportButton"
				style="width: 160px; margin-left: -40px;" name="action"
				type="submit" class="medButton" value="Download Report"/></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
$(document).ready(function(){
    $("#downloadReportButton").click(function () {
    	 var options = $('#countryId > option:selected');
         if(options.length == 0){
             alert('Please select country!');
             return false;
         }
        return true; 
    });
});
</script>	
