<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">

$(document).ready(function(){
	 
	//var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	//copyAll();
	/* function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox(); */
	$('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
			else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
			else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
		
			$('#autoArtistVenue').val('');
			 
			
			if(row[0]=='ARTIST'){
				 
				var oldArtistIds = $("#artist").val();
				oldArtistIds = oldArtistIds+","+row[1];
				
				var oldArtistNames = $("#artistName").val();
				
				//oldArtistNames = oldArtistNames+","+row[2];
				
				 
				if(null == oldArtistNames || oldArtistNames == '' || oldArtistNames.length <= 0){
					oldArtistNames = row[2];
				}else{
					oldArtistNames = oldArtistNames+","+row[2];
				} 
				
				$("#artist").val(oldArtistIds);
				$("#artistName").val(oldArtistNames); 
				
			} else if(row[0]=='VENUE'){
				//$('#selectedOption').text('Venue');
				
				var oldVenueIds = $("#venue").val();
				oldVenueIds = oldVenueIds+","+row[1];
				
				var oldVenueNames = $("#venueName").val();
			//	oldVenueNames = oldVenueNames+","+row[2];
				
				 if(null == oldVenueNames || oldVenueNames == '' || oldVenueNames.length <= 0){
					oldVenueNames = row[2];
				}else{
					oldVenueNames = oldVenueNames+","+row[2];
				} 
				
				$("#venue").val(oldVenueIds);
				$("#venueName").val(oldVenueNames);
				 
				
			}  
			
			var artistNames = $("#artistName").val();
			var venueNames = $("#venueName").val();
			//var wholeText = "<b>Artist :-</b> "+artistNames+" <br> <b>Venue :-</b> "+venueNames;
			$('#selectedVenues').text(venueNames);
			$('#selectedArtist').text(artistNames);
			
	});

	/* $("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change'); */
	
	   
	
});
	
/* function getEvents(isArtist,id){
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;;
	} 
	
	else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;;
	} 
}  */

/* function submitForms(action){
	//$("#action").val(action);
	$("#status").val(action);
	alert($("#action").val());
	$("#formId").submit();
} */

/* 
function validateForm(){

	var flag= true;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		
		id = this.id.replace('checkbox','broker');
		value = $.trim($("#"+id).val());
		if(value=='') {
			alert('Broker can not be blank.');
			$("#"+id).focus();
			flag = false;
			return ;
		}
	});
	if(!flag){
		return flag;	
	}
	return true;
} */

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

/* function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
} */

/* 

function downloadOnClick(action) {
	$("#formId").submit();
} */


function copyAllEvents(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		//copyTextField('broker');
	}else{
		$('.selectCheck').attr('checked', false);
	}
}
	
	
function submitForms(action){
	if(action == "reset"){
		location.href = "VividSeatTicketInventory";
		return;
	}else if(action == "getinventory"){
		
		
		/* var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			 
			flag = false;
		});
		if(!flag){
			return flag;	
		}
		return true; */
		
		$('#formId').attr('action', 'DownloadVividSeatTicketInventory');
	}else if(action == "search"){
		$('#formId').attr('action', 'VividSeatTicketInventory');
	}
	//alert($('#formId').attr('action'));
	$("#action").val(action);
	$("#formId").submit();
}

function reloadPage(){
	window.location.href = "VividSeatTicketInventory";
}


function resetFields(){
	//$("#eventName").val("");
	$("#artist").val("");
	$("#venue").val("");
	$("#artistName").val("");
	$("#venueName").val("");
	//$("#venueName").val("");
	$("#selectedArtist").text("");
	$("#selectedVenues").text("");
}
 
function changeOptionType(){
	
	var value = $("#optionType").val();
	
	if(value == 'ALL'){
		resetFields();
		$("#eventRowId").hide(); 
		$("#av_auto_search_row_id").hide();
		$("#av_auto_artist_row_id").hide(); 
		$("#av_auto_venue_row_id").hide(); 
		$("#av_auto_search_button_row_id").hide(); 
		$("#allEventsRowId").show(); 
		$("#ebayInventoryTabel").hide(); 
		$("#dateFilterRowId").hide(); 
		$("#fromDateRowId").hide();
		$("#toDateRowId").hide();
		
	}else if(value == "EVENT"){
		resetFields();
		$("#eventRowId").show();
		$("#av_auto_search_row_id").hide();
		$("#av_auto_artist_row_id").hide(); 
		$("#av_auto_venue_row_id").hide(); 
		//$("#av_auto_search_button_row_id").hide();
		$("#av_auto_search_button_row_id").show(); 
		$("#allEventsRowId").hide();
		$("#fromDateRowId").show();
		$("#toDateRowId").show();
		$("#dateFilterRowId").show();
		$("#fromDateRowId").hide();
		$("#toDateRowId").hide();
		//$("#ebayInventoryTabel").hide(); 
		
		//chooseDateFilter("NO");
	}else{
		$("#eventName").val("");
		$("#eventRowId").hide();
		$("#av_auto_search_row_id").show();
		$("#av_auto_artist_row_id").show(); 
		$("#av_auto_venue_row_id").show(); 
		$("#av_auto_search_button_row_id").show(); 
		$("#allEventsRowId").hide();
		$("#dateFilterRowId").show();
		$("#fromDateRowId").hide();
		$("#toDateRowId").hide();
		//chooseDateFilter("NO");
	}  
}
function chooseDateFilter(option){
	
	if(option == 'YES'){
		$("#fromDateRowId").show();
		$("#toDateRowId").show();
	}else{
		$("#fromDateRowId").hide();
		$("#toDateRowId").hide();
	}
	 
}

</script>

</head>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Report</a> &gt; TMAT Report
</div>
<h1>Vividseats Ticket Inventory</h1>
<body>
	<br>
	<br>
	<div align="center">
		<form:form modelAttribute="exosure" action="VividSeatTicketInventory"
			method="post" id="formId">
			<table>
			 

				<tr>
					<td><b>Download Type</b></td>
					<td>
					<select id="optionType" name="optionType" onchange="changeOptionType();">
						<option value="ALL" <c:if test="${optionType eq 'ALL' or downloadType eq null or downloadType eq ''}"> Selected </c:if>>ALL</option>
						<option value="EVENT" <c:if test="${optionType eq 'EVENT'}"> Selected </c:if>>EVENT</option>
						<option value="ARTISTORVENUE" <c:if test="${optionType eq 'ARTISTORVENUE'}"> Selected </c:if>>ARTIST OR VENUE</option>
					</select>
					</td>
					<td></td>
					<td colspan="2"></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr style="display: none;" id="allEventsRowId">
					<td colspan="6" align="center">
						<table border="1">
						
							<tr>
								<td><b>Last Report Generated at</b></td>
								<td>
									${lastReportGeneratedAt}
								</td>
							</tr>
							
							<tr>
								<td><b>Running Now</b></td>
								<td>
									<c:choose>
										<c:when test="${isRunningNow}">
											YES
										</c:when>
										<c:otherwise>
											NO
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr>
								<td>
									<input type="button" value="Download" <c:if test="${isRunningNow}">disabled="disabled"</c:if> class="medButton" id="update" name="update2" onclick="javascript:submitForms('getinventory');" /></td>
								<td>
									<input type="button" value="Reset" onclick="javascript:reloadPage();" class="medButton">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="display: none;" id="eventRowId" >
					<td><b>Search by Event Name </b></td>
					<td>
						<input type="text" id="eventName" name="eventName" value="${eventName}">
					</td>
					<td></td>
					<td colspan="2"></td>
					<td></td>
				</tr>
				
				<tr id="av_auto_search_row_id"  style="display: none;">
					<td><b>Select Artist or Venue:</b></td>
					<td>
						<input type="hidden" id="artist" name="artist" value ="${artist}">
						<input type="hidden" id="venue" name="venue" value ="${venue}">
						<input type="hidden" id="artistName" name="artistName" value ="${artistName}">
						<input type="hidden" id="venueName" name="venueName" value ="${venueName}">
						<%-- <input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
						<input type="hidden" id="Child" name="Child" value ="${Child}"> --%>
						<input type="text" id="autoArtistVenue" name="autoArtistVenue">
					</td>
					<td></td>
					<td colspan="2"></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				 <tr id="av_auto_artist_row_id"  style="display: none;">
					<td colspan="1" ><b>ARTIST: </b></td>
					<td colspan="5" ><span id="selectedArtist" >${selectedArtist}</span></td>
				</tr>
				 
				<tr id="av_auto_venue_row_id"  style="display: none;">
					<td colspan="1" ><b>VENUE: </b></td>
					<td colspan="5" ><span id="selectedVenues" >${selectedVenues}</span></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				
				<tr id="dateFilterRowId" style="display: none;">
					<td><b>Date Filter</b></td>
					<td>
						<input type="radio" id="dateFilter1" name="dateFilter" value="YES" onclick="chooseDateFilter('YES');" <c:if test="${dateFilter eq 'YES'}">checked</c:if> > YES
						<input type="radio" id="dateFilter1" name="dateFilter" value="NO" onclick="chooseDateFilter('NO');" <c:if test="${dateFilter ne 'YES'}">checked</c:if> > NO
					</td>
					<td></td>
					<td colspan="2"></td>
					<td></td>
				</tr>
				
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				
					<tr id="fromDateRowId" style="display: none;">
						<td colspan="6">
							<b>Start Date: </b> 
						 <select name="fromMonth" id="fromMonth">
								<c:forEach var="month" begin="1" end="12" step="1">
									<option value="${month}"
										<c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
								</c:forEach>
						</select> / <select name="fromDay" id="fromDay">
								<c:forEach var="day" begin="1" end="31" step="1">
									<option value="${day}"
										<c:if test="${day == fromDate}">selected</c:if>>${day}</option>
								</c:forEach>
						</select> / <select name="fromYear" id="fromYear">
								<c:forEach var="year" begin="2019" end="2025" step="1">
									<option value="${year}"
										<c:if test="${year == fromYear}">selected</c:if>>${year}</option>
								</c:forEach>
						</select> <input type="text" id="fromCalendar" class="calendar"
							style="visibility: hidden; width: 0px;" /></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr id="toDateRowId" style="display: none;">
					<td colspan="6"><b>End Date: </b> <select name="toMonth" id="toMonth"
						style="margin-left: 7px;">
							<c:forEach var="month" begin="1" end="12" step="1">
								<option value="${month}"
									<c:if test="${month == toMonth}">selected</c:if>>${month}</option>
							</c:forEach>
					</select> / <select name="toDay" id="toDay">
							<c:forEach var="day" begin="1" end="31" step="1">
								<option value="${day}"
									<c:if test="${day == toDate}">selected</c:if>>${day}</option>
							</c:forEach>
					</select> / <select name="toYear" id="toYear">
							<c:forEach var="year" begin="2019" end="2025" step="1">
								<option value="${year}"
									<c:if test="${year == toYear}">selected</c:if>>${year}</option>
							</c:forEach>
					</select> <input type="text" id="toCalendar" class="calendar"
						style="visibility: hidden; width: 0px;" /></td>
				</tr>
				 
				<tr id="av_auto_search_button_row_id"  style="display: none;">
					<td colspan="6" align="center">
						<input type="hidden" id="status" name="status" />
						<input type="hidden" name="action" id="action"/>
						<input type="button" value="Get Events" onclick="javascript:submitForms('search');" class="medButton">
						<input type="button" value="Reset" onclick="javascript:reloadPage();" class="medButton">
					</td>
				</tr>
				
			</table>
			<br />
			
			<c:if test="${showEventGrid}">

			<c:if test="${not empty eventList}">
				<div id="ebayInventoryTabel"> 
				
					<div style="float: right; padding-right: 320px;">
						<input type="button" value="Download" class="medButton" id="update1"
							name="update1" onclick="javascript:submitForms('getinventory');" />
					</div>

					<display:table list="${eventList}" defaultsort="3"
						decorator="org.displaytag.decorator.TotalTableDecorator"
						id="event" requestURI="#" class="list">

						<display:column
							title='<br/><input type="checkbox" name="copyAll" id="copyAll" onclick="copyAllEvents();" >'>
							<input type="checkbox" name="checkbox_${event.eventId}"
								id="checkbox_${event.eventId}"
								checked="checked"
								class="selectCheck" />
						</display:column>

						<display:column title="EvenName" sortable="true">${event.eventName}</display:column>

						<display:column title="Event Date" sortable="true">
							${event.eventDate}
						</display:column>
						
						<display:column title="Event Time" sortable="true">
							${event.eventTime}
						</display:column>
						
						<display:column title="Artist" sortable="true">${event.artistName}</display:column>

						<display:column title="Venue" sortable="true">${event.venueName}</display:column>
 
					</display:table>

					<br />
					<div style="float: right; padding-right: 320px;">
						<input type="button" value="Download" class="medButton" id="update2"
							name="update2" onclick="javascript:submitForms('getinventory');" />
					</div>
				</div>

			</c:if>
			<c:if test="${empty eventList}">
				<div align='center'>
					<h4>
						<span id="emptyMsg">There isn't any event details for this
							selection</span>
					</h4>
				</div>
				<table id="listAudit"></table>
				<br />
				<br />
			</c:if>
			</c:if>
			<br/> <br/>
			<!-- <table>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td align="center"><input type="button" value="Download"
						class="medButton" id="update2" name="update2"
						onclick="javascript:downloadOnClick();" /></td>
				</tr>
			</table> -->
		</form:form>
	</div>
</body>
<script type="text/javascript">
changeOptionType();

chooseDateFilter('${dateFilter}');


$(document).ready(function() {
	$('.calendar').datepicker({
		'buttonImageOnly': true,
        buttonImage: '../images/ico-calendar.gif',
        showOn: 'button',
        showButtonPanel: true,
		changeMonth: true,
		changeYear: true,	        
        dateFormat:'yy-mm-dd',
        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
         maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)
	});
	
	$('#fromCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
        }
    );

	$('#fromCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#fromYear').val(tokens[0]*1);
			$('#fromMonth').val(parseInt(tokens[1]*1));
			$('#fromDay').val(parseInt(tokens[2]*1));
		}
	);
	
	$('#toCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
        }
    );
	
	$('#toCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#toYear').val(tokens[0]*1);
			$('#toMonth').val(parseInt(tokens[1])*1);
			$('#toDay').val(parseInt(tokens[2])*1);
		}
	);			
});



</script>
</html>