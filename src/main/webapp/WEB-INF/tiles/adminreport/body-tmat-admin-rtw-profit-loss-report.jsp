<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Report</a> 
  &gt; TMAT Report
</div>


<h1> Profit And Loss Report</h1>

<form action="DownloadRtwProfitAndLossReport" method="post">
<label style="color:red;font-size:15px;">${returnMessage}</label>
<input type="hidden" name="action" value="action"/>
	<table align="center" width="420px;">
		<tr>
			<td colspan="2"><label id="msg"></label></td>
		</tr>
		 <tr>
			<td><b> Broker </b></td>
			<td>
				<select name="broker" id="broker">
					<option value="All">-All-</option>
					<option value="RTW">RTW</option>
					<option value="RTW-2">RTW-2</option>
					
				</select>
			</td>
		</tr> 
		<tr>
			<td>
			<b>Start Date: </b>
			</td>
			<td>

<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>
	

<tr>
<td>
    <b>End Date: </b>
	</td>
	<td>

<select name="toMonth" id="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay" >
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="toYear" id="toYear" >
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>

		
	
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><input id="downloadReportButton" style="width:160px;margin-left:60px;" name="action" type="submit" class="medButton" value="Download"/></td>
		</tr>	
	</table>
</form>

<script type="text/javascript">

$(document).ready(function() {
	$('.calendar').datepicker({
		'buttonImageOnly': true,
        buttonImage: '../images/ico-calendar.gif',
        showOn: 'button',
        showButtonPanel: true,
		changeMonth: true,
		changeYear: true,	        
        dateFormat:'yy-mm-dd',
        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
         maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)
	});
	
	$('#fromCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
        }
    );

	$('#fromCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#fromYear').val(tokens[0]*1);
			$('#fromMonth').val(parseInt(tokens[1]*1));
			$('#fromDay').val(parseInt(tokens[2]*1));
		}
	);
	
	$('#toCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
        }
    );
	
	$('#toCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#toYear').val(tokens[0]*1);
			$('#toMonth').val(parseInt(tokens[1])*1);
			$('#toDay').val(parseInt(tokens[2])*1);
		}
	);			
});


</script>