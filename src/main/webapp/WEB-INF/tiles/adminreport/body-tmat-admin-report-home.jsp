<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Report</a> 
  &gt; TMAT Report
</div>


<h1>TMAT Reports</h1>
<table>

		<tr >
			
			<td valign="top">  
				<h2>Events Report  </h2>
				<table>
					<!-- <tr>
						<td>
							<a class="buttonLink" href="BrokerEventsReport?brokerId=2"> 
								<img src="../images/ico-search.gif" align="absbottom"/> JKT events for Autopricing.</a>
						</td>
					</tr> -->
					<!-- <tr>
						<td>
							<a class="buttonLink" href="BrokerEventsReport?brokerId=3"> 
								<img src="../images/ico-search.gif" align="absbottom"/> ROT events for Autopricing.</a>
						</td>
					</tr> -->
					<!-- <tr>
						<td>
							<a class="buttonLink" href="BrokerEventsReport?brokerId=5"> 
								<img src="../images/ico-search.gif" align="absbottom"/> TixCity events for Autopricing.</a>
						</td>
					</tr> -->
					<tr>
						<td>
							<a class="buttonLink" href="EventsWithoutMapReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Events without map.</a>
						</td>
					</tr>
					  
					<tr>
						<td>
							<a class="buttonLink" href="DownloadLastTwoDaysEventsNotInTmatReport">
								<img src="../images/ico-search.gif" align="absbottom"/> Event Created in POS in last two days but not in TMAT.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="EventsNotInTmatReport">
								<img src="../images/ico-search.gif" align="absbottom"/> Events in POS but not in TMAT.</a>
						</td>
					</tr>
					 
					<tr>
						<td>
							<a class="buttonLink" href="DownloadTmatPOSParentCategoryMissmatchEvents">
								<img src="../images/ico-search.gif" align="absbottom"/> TMAT and POS parent category missmatch Events.</a>
						</td>
					</tr>
					
					 <tr>
						<td>
							<a class="buttonLink" href="DownloadExchangeEventsWithoutLocalEvents?brokerId=5">
								<img src="../images/ico-search.gif" align="absbottom"/> Exchange event not having local events in RTW.</a>
						</td>
					</tr>
					
					<tr>
						<td>
							<a class="buttonLink" href="DownloadExchangeEventsWithoutLocalEvents?brokerId=10">
								<img src="../images/ico-search.gif" align="absbottom"/> Exchange event not having local events in RTW-2.</a>
						</td>
					</tr>
					<!-- <tr>
						<td>
							<a class="buttonLink" href="DownloadExchangeEventsWithoutLocalEvents?brokerId=5">
								<img src="../images/ico-search.gif" align="absbottom"/> Exchange event not having local events in TixCity.</a>
						</td>
					</tr> -->
					<tr>
						<td>
							<a class="buttonLink" href="EventsWithoutCSVReport">
								<img src="../images/ico-search.gif" align="absbottom"/> Events without CSV.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadTmatPosVenueMismatchEvents">
								<img src="../images/ico-search.gif" align="absbottom"/> TMAT and POS Venue Missmatch Events.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadMissingSVGandCSVZones">
							<img src="../images/ico-search.gif" align="absbottom"> SVG and CSV Missing Zones.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadSvgZonesWithNoListingsInRTF">
							<img src="../images/ico-search.gif" align="absbottom"> SVG Zones With No Listings in RTF.</a>
						</td>
					</tr>
				</table>

			</td>
			
			<td valign="top">  
				<h2>Crawls Report  </h2>
				<table>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadDuplicateCrawlsEvents"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Event with multiple crawls of same exchange.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadZeroCrawlsEventsReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Events With Zero Crawls.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="DownloadZeroCrawlsEventsReportForShTevoTnow"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Events With Zero Crawls for SH,TEVO and TNOW.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="EditorDownloadLessCrawlEvents?latestEvent=false"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Events With Two or Less than Two Crawls.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="tmatStatisticsReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> TMAT Statistics.</a>
						</td>
					</tr>
					
					
					<tr>
						<td>
							<a class="buttonLink" href="GetEventCrawlsSalesDetails"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Event Crawls Sales Report.</a>
						</td>
					</tr>
					
					
				</table>

			</td>
			
			<td valign="top">  
				<h2>Sales Report  </h2>
				<table>
				<tr>
						<td>
							<a class="buttonLink" href="MailBrokerCategorySales"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Mail Broker Category Sales Report.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="CategorySalesReportEntry?brokerId=7"> 
								<img src="../images/ico-search.gif" align="absbottom"/> TixCity Category Sales.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="CategorySalesReportEntry?brokerId=5"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Category Sales.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="CategorySalesReportEntry?brokerId=10"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW-2 Category Sales.</a>
						</td>
					</tr>
					<!-- <tr>
						<td>
							<a class="buttonLink" href="CategorySalesReportEntry?brokerId=4"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Manhattan Category Sales.</a>
						</td>
					</tr> -->
					<tr>
						<td>
							<a class="buttonLink" href="PosInventoryReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> Pos Inventory Report.</a>
						</td>
					</tr>
					<tr>
						<td>
							<!-- <a class="buttonLink" href="EventsWithSales"> 
								<img src="../images/ico-search.gif" align="absbottom"/> TND Sales Events Details</a> -->
								<a class="buttonLink" href="LoadTNEventsSalesReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> TN Events Sales</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="RtwPOSUnfilledShortsWithLongInventory"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW POS Unfilled Shorts with Possible Long Inventory.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="RtwPosUnSentOrderDetails"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Next 15 days Unsent Order Details.</a>
						</td>
					</tr>
					<tr>
						<td>
							<a class="buttonLink" href="RtwProfitAndLossReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Profit And Loss Report</a>
						</td>
					</tr>
					
					<tr>
						<td>
							<a class="buttonLink" href="RtwProfitAndLossFilledReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Profit And Loss Filled Report</a>
						</td>
					</tr>
					
					<tr>
						<td>
							<a class="buttonLink" href="GetRTWUnsoldInventory"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Unsold Inventory </a>
						</td>
					</tr>
					
					<tr>
						<td>
							<a class="buttonLink" href="GetRTWUnsoldInventoryAverageCost"> 
								<img src="../images/ico-search.gif" align="absbottom"/> RTW Unsold Inventory Average </a>
						</td>
					</tr>
					
					<tr>
						<td>
							<a class="buttonLink" href="TNDRtfNoSaleReport"> 
								<img src="../images/ico-search.gif" align="absbottom"/>TND RTF No Sale Report </a>
						</td>
					</tr>
					
				</table>
				</td>
				
				<td valign="top">  
				<h2>Inventory Report  </h2>
				<table>
					<tr>
						<td>
							<a class="buttonLink" href="VividSeatTicketInventory"> 
								<img src="../images/ico-search.gif" align="absbottom"/>Download Vividseats Tickets</a>
						</td>
					</tr>
				</table>

			</td>
		</tr>
		
</table>