<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript"
	src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>
		
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Report</a> &gt; TMAT Report
</div>

<h1>Events without CSV.</h1>

<form action="DownloadEventsWithoutCSVReport" method="post">
	<table>
		<tr>
			<td><b>Event Creation Start Date: </b> <select name="fromMonth"
				id="fromMonth">
					<c:forEach var="month" begin="1" end="12" step="1">
						<option value="${month}"
							<c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
					</c:forEach>
			</select> / <select name="fromDay" id="fromDay">
					<c:forEach var="day" begin="1" end="31" step="1">
						<option value="${day}"
							<c:if test="${day == fromDate}">selected</c:if>>${day}</option>
					</c:forEach>
			</select> / <select name="fromYear" id="fromYear">
					<c:forEach var="year" begin="2009" end="2020" step="1">
						<option value="${year}"
							<c:if test="${year == fromYear}">selected</c:if>>${year}</option>
					</c:forEach>
			</select> <input type="text" id="fromCalendar" class="calendar"
				style="visibility: hidden; width: 0px;" /></td>
		</tr>

		<tr>
			<td><b>Event Creation End Date: </b> <select name="toMonth"
				id="toMonth">
					<c:forEach var="month" begin="1" end="12" step="1">
						<option value="${month}"
							<c:if test="${month == toMonth}">selected</c:if>>${month}</option>
					</c:forEach>
			</select> / <select name="toDay" id="toDay">
					<c:forEach var="day" begin="1" end="31" step="1">
						<option value="${day}"
							<c:if test="${day == toDate}">selected</c:if>>${day}</option>
					</c:forEach>
			</select> / <select name="toYear" id="toYear">
					<c:forEach var="year" begin="2009" end="2020" step="1">
						<option value="${year}"
							<c:if test="${year == toYear}">selected</c:if>>${year}</option>
					</c:forEach>
			</select> <input type="text" id="toCalendar" class="calendar"
				style="visibility: hidden; width: 0px;" /></td>
		</tr>
		<tr>
			<!-- <td><b>Select Parent Category : </b> </td> -->
			<td> <b>Select Parent Category : </b> <select name="parentCategory">
					<option value="All">All</option>
					<option value="Sports">Sports</option>
					<option value="Concerts">Concerts</option>
					<option value="Theater">Theatre</option>
					<option value="Other">Other</option>
			</select></td>
		</tr>

		<!-- Country multi-select filter -->
		<tr>
			<!-- <td><b>Select Country : </b>
			</td> -->
			<td><b>Select Country : </b> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<select id="countryId" name="country" multiple="multiple">
					<option value="US&CA">US & CA</option>
					<option value="others">Rest of the world</option>
					<%-- <c:forEach var="countryMap" items="${countries}">
				<option value="${countryMap.key}">${countryMap.value}</option>
			</c:forEach> --%>
			</select></td>
		</tr>
		<tr>
			</br>
			<td>&nbsp</td>
			<td><input id="downloadReportButton"
				style="width: 160px; margin-left: -40px;" name="action"
				type="submit" class="medButton" value="Download Report"/></td>
		</tr>
	</table>
</form>
<script type="text/javascript">
$(document).ready(function(){
    $("#downloadReportButton").click(function () {
    	 var options = $('#countryId > option:selected');
         if(options.length == 0){
             alert('Please select country!');
             return false;
         }
        return true; 
    });
});
</script>	
