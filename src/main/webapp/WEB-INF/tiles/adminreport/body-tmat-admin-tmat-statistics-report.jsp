<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Report</a> &gt; TMAT Report
</div>

<head>
<style>
#date {
	margin-right: 0px;
	margin-left: 800px;
	border-top-width: 0px;
	margin-top: -210px;
	padding-top: 0px;
	padding-bottom: 250px;
}

table,th,td {
	border-collapse: collapse;
	border: 1px solid black;
}
	
th,td {
	padding: 5px;
}
</style>
</head>

<h1>TMAT Statistics</h1>

<p style="font-size: 15px">Total number of events in Tmat that are
	in active state : ${numevents}</p>
<table border="1px" cellspacing="0px" width="40%">
	<thead>
		<th>Sports</th>
		<th>Concerts</th>
		<!-- <th>Default</th> -->
		<th>Theater</th>
		<!-- <th>Las Vegas</th> -->
		<th>Other</th>
		<th>Total</th>
	</thead>
	<tbody>
		<tr align="center">
			<td style="padding: 5px;">${sports}</td>
			<td style="padding: 5px;">${concert}</td>
			<!-- <td style="padding:5px;" >$defaults</td> -->
			<td style="padding: 5px;">${theater}</td>
			<!-- <td style="padding:5px;">$lasvegas</td> -->
			<td style="padding: 5px;">${other}</td>
			<td style="padding: 5px;">${numevents}</td>
		</tr>
	</tbody>
</table>
<br />
<p style="font-size: 15px">Total number of crawls that are in active
	mode : ${totalenabledcrawl}</p>
<table border="1px" cellspacing="0px" width="40%">
	<thead>
		<th>Exchanges</th>
		<th>Sports</th>
		<th>Theater</th>
		<th>Concert</th>
		<!-- <th>Las vegas</th> -->
		<th>Other</th>
		<th>Total</th>
	</thead>
	<tbody>
		<tr align="center">
			<th>TicketNetworkDirect</th>
			<td style="padding: 5px;">${sportTnd}(${tndSportPer}%)</td>
			<td style="padding: 5px;">${theaterTnd}(${tndTheaterPer}%)</td>
			<td style="padding: 5px;">${concertTnd}(${tndConcertPer}%)</td>
			<!--  <td style="padding:5px;">$lasVegasTnd</td> -->
			<!-- <td style="padding:5px;">$defaultTnd</td> -->
			<td style="padding: 5px;">${otherTnd}(${tndOtherPer}%)</td>
			<td style="padding: 5px;">${totalTnd}</td>
		</tr>
		<tr align="center">
			<th>StubHub</th>
			<td style="padding: 5px;">${sportStubhub}(${shSportPer}%)</td>
			<td style="padding: 5px;">${theaterStubhub}(${shTheaterPer}%)</td>
			<td style="padding: 5px;">${concertStubhub}(${shConcertPer}%)</td>
			<!-- <td style="padding:5px;">$lasVegasStubhub</td> -->
			<!-- <td style="padding:5px;">$defaultStubhub</td> -->
			<td style="padding: 5px;">${otherStubhub}(${shOtherPer}%)</td>
			<td style="padding: 5px;">${totalStubhub}</td>
		</tr>
		<tr align="center">
			<th>TicketEvolution</th>
			<td style="padding: 5px;">${sportTe} (${teSportPer}%)</td>
			<td style="padding: 5px;">${theaterTe}(${teTheaterPer}%)</td>
			<td style="padding: 5px;">${concertTe}(${teConcertPer}%)</td>
			<!-- <td style="padding:5px;">$lasVegasTe</td> -->
			<!-- <td style="padding:5px;">$defaultTe</td> -->
			<td style="padding: 5px;">${otherTe}(${teOtherPer}%)</td>
			<td style="padding: 5px;">${totalTe}</td>
		</tr>
		<tr align="center">
			<th>TicketsNow</th>
			<td style="padding: 5px;">${sportTnow}(${tnowSportPer}%)</td>
			<td style="padding: 5px;">${theaterTnow}(${tnowTheaterPer}%)</td>
			<td style="padding: 5px;">${concertTnow}(${tnowConcertPer}%)</td>
			<!--  <td style="padding:5px;">$lasVegasTnow</td> -->
			<!-- <td style="padding:5px;">$defaultTnow</td> -->
			<td style="padding: 5px;">${otherTnow}(${tnowOtherPer}%)</td>
			<td style="padding: 5px;">${totalTnow}</td>
		</tr>
			<tr align="center">
			<th>FlashSeat</th>
			<td style="padding: 5px;">${sportFlSheet}(${fsSportPer}%)</td>
			<td style="padding: 5px;">${theaterFlSheet}(${fsTheaterPer}%)</td>
			<td style="padding: 5px;">${concertFlSheet}(${fsConcertPer}%)</td>
			<td style="padding: 5px;">${otherFlSheet}(${fsOtherPer}%)</td>
			<td style="padding: 5px;">${totalFlSheet}</td>
		</tr>
			</tr>
			<tr align="center">
			<th>VividSeat</th>
			<td style="padding: 5px;">${sportVs}(${vsSportPer}%)</td>
			<td style="padding: 5px;">${theaterVs}(${vsTheaterPer}%)</td>
			<td style="padding: 5px;">${concertVs}(${vsConcertPer}%)</td>
			<td style="padding: 5px;">${otherVs}(${vsOtherPer}%)</td>
			<td style="padding: 5px;">${totalVs}</td>
		</tr>
		<tr align="center">
			<th>Total</th>
			<td style="padding: 5px;">${totalSport}</td>
			<td style="padding: 5px;">${totalTheater}</td>
			<td style="padding: 5px;">${totalConcert}</td>
			<!-- <td style="padding:5px;">$totalLasVegas</td> -->
			<!-- <td style="padding:5px;">$totalDefault</td> -->
			<td style="padding: 5px;">${totalOther}</td>
			<td style="padding: 5px;">${total}</td>
		</tr>
	</tbody>
</table>

<div id="date">
	<c:if test="${fileNotFound=='true'}">
		<p id="p" style="Color: red">
			<b>The Report is not present for this Date</b>
		<p>
			</br>
	</c:if>
	<b>TMAT Statistics Report Download :</b></br> </br>
	<form action="downloadTMATStatisticsReport" id="formId">
	<c:choose>
        <c:when test="${fileNotFound=='true'}">
                  <fmt:parseDate value="${applyDate}" type="date" pattern="MM/dd/yyyy" var="now"/>  
         </c:when>
         <c:otherwise>
                <c:set var="now" value="<%=new java.util.Date()%>" />
         </c:otherwise>
    </c:choose>		
				
	
		Select Date:&nbsp <input type="text"value="<fmt:formatDate value='${now}' pattern='MM/dd/yyyy' />"
			class="applyDateClass" name="applyDate" /> &nbsp;&nbsp; 
			<input type="submit" class="medButton" value="download">
	</form>
</div>
<script type="text/javascript">

$(function() {
    $(".applyDateClass").datepicker({
	dateFormat: 'mm/dd/yy',
	minDate: -14,
    maxDate:0
	});
    
    $(".medButton").click(function(){
    	 document.getElementById("p").style.visibility = "hidden";
    });
});
  
<!--

</script>

