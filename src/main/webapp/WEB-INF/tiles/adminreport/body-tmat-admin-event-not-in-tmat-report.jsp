<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Report</a> 
  &gt; TMAT Report
</div>


<h1>Events not in TMAT but in POS.</h1>

<form action="DownloadEventsNotInTmatReport" method="post">
<table>
<tr>
<td>
<b>Select Parent Category : </b>
</td>
<td>
<select name="parentCategory">
<option value="All">All</option>
<option value="Sports">Sports</option>
<option value="Concerts">Concerts</option>
<option value="Theatre">Theatre</option>
<option value="Other">Other</option>
</select>
</td>
</tr>
<tr>
</br>
<td>&nbsp</td>
<td>
<input id="downloadReportButton" style="width:160px;margin-left:-40px;" name="action" type="submit" class="medButton" value="Download Report"/>
</td>
</tr>
</table>
</form>