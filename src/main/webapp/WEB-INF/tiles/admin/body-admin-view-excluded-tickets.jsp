<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; View Excluded Cheapest Tickes
</div>

<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${info}
  </div>
</div>
</c:if>

<form id="eventSelector" method="Post" action="TmatInstantFiles" onsubmit="$('#eventSubmit').hide(); $('#loadingSpan').show()">

Tour: 
<select name="tourId" id="tourIdSelect">
<option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
</select>

<c:choose>
  <c:when test="${tourId == 'ALL'}">
    <c:set var="maxTitleChars" value="10"/>
  </c:when>
  <c:otherwise>
    <c:set var="maxTitleChars" value="22"/>
  </c:otherwise>
</c:choose>
<br />
<c:if test="${tourId != 'ALL'}">
Event: 
<select name="eventId" id="eventIdSelect">
<option value="ALL" <c:if test="${eventId == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="event" items="${events}">
    <option value="${event.id}" <c:if test="${eventId == event.id}">selected</c:if>>${event.name} - <fmt:formatDate value="${event.localDate}" pattern="MM/dd/yyyy" />
      <c:choose>
        <c:when test="${not empty event.localTime}">
          <fmt:formatDate value="${event.localTime}" pattern="HH:mm" />
        </c:when>
        <c:otherwise>
          TBD
        </c:otherwise>
      </c:choose>
    </option>
  </c:forEach>
</select>
<c:if test="${eventId != 'ALL'}"> 
</c:if> 
</c:if>
<br/><br/>
<!--
Category: 
<select name="category" id="categorySelect">
<option value="ALL" <c:if test="${categorySelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="category" items="${categorys}">
    <option value="${category}" <c:if test="${categorySelected == category}">selected</c:if>>${category}</option>
  </c:forEach>
</select> -->
Section: 
<select name="section" id="sectionSelect">
<option value="ALL" <c:if test="${sectionSelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="section" items="${sections}">
    <option value="${section}" <c:if test="${sectionSelected == section}">selected</c:if>>${section}</option>
  </c:forEach>
</select>
Row: 
<select name="row" id="rowSelect">
<option value="ALL" <c:if test="${rowSelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="row" items="${rows}">
    <option value="${row}" <c:if test="${rowSelected == row}">selected</c:if>>${row}</option>
  </c:forEach>
</select>
Seat:
<select name="seat" id="seatSelect">
<option value="ALL" <c:if test="${seatSelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="seat" items="${seats}">
    <option value="${seat}" <c:if test="${seatSelected == seat}">selected</c:if>>${seat}</option>
  </c:forEach>
</select>

<br/><br/>
Sites: (<a href="#" onclick="return checkAllSites()">check all</a> - <a href="#" onclick="return uncheckAllSites()">uncheck all</a>)
<div style="clear:both"></div>
<c:forEach var="sid" items="${siteIds}">
	<c:set var="safeSiteId" value="[${sid}]" />

	<div style="float: left; width: 280px;height: 24px;">
		<input type="checkbox" id="checkbox-${sid}" name="checkbox-${sid}" class="site-checkbox"
			<c:set var="safeSid" value="[${sid}]" /> 
			<c:if test="${siteFilters[sid] and not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">checked</c:if>
			<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">disabled</c:if>
		>
		<label for="checkbox-${sid}">
	    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
			<c:choose>
			  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
			    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
				  ${sid}
			    </span>
			  </c:when>
			  <c:otherwise>
			    ${sid}
			  </c:otherwise>
			</c:choose>
		</label>
	</div>
</c:forEach>
<div style="clear:both"></div>
<input id="eventSelectorAction" type="hidden" name="action" value="" /> 
&nbsp;&nbsp;
    <input type="button" class="medButton" value="View" onclick="$('#eventSelectorAction').val('search'); $('#eventSelector').submit()"/> &nbsp;<br/>
<span id="loadingSpan" style="display:none">
  <img src="../images/process-running.gif" align="absbottom"/> Loading...
</span>

<br/>
<c:if test="${fn:length(tickets) > 0}"><br/><b>Total Number Of Tickets Found: <c:out value="${fn:length(tickets)}"/></b></c:if>
<display:table class="list" name="${tickets}" id="ticket" requestURI="TmatInstantFiles">			
			<display:column title="Id" property="id" sortable="true"/>
			<display:column title="Event Name" property="event.name" sortable="true"/>
			<display:column title="Event Date" property="event.localDate" sortable="true"/>
			<display:column title="Event Time" property="event.localTime" sortable="true"/>
			<display:column title="Zone" property="category.symbol" sortable="true"/>
			<display:column title="Section" property="section" sortable="true"/>
			<display:column title="Row" property="row" sortable="true"/>
			<display:column title="Seat" property="seat" sortable="true"/>
			<display:column title="Price" property="currentPrice" sortable="true"/>
			<display:column title="Seller" property="seller" sortable="true"/>
			
			
		</display:table>


</form>


 
<script type="text/javascript">

$(document).ready(function(){
$('#tourIdSelect').change(function(){ 
$('#eventSelector').submit();
		});
$('#eventIdSelect').change(function(){ 
$('#eventSelector').submit();
		});
$('#sectionSelect').change(function(){ 
$('#eventSelector').submit();
		});	
$('#rowSelect').change(function(){ 
$('#eventSelector').submit();
		});	
		/*
$('#categorySelect').change(function(){ 
$('#eventSelector').submit();
		});	*/
			
      	    });
			function checkAllSites() {
		$.each($('.site-checkbox'), function(intIndex, input) {
		  if (!$(input).attr('disabled')) {
		  	$(input).attr('checked', 'true').change();
		  }
		});
		return false;
	}

	function uncheckAllSites() {
		$('.site-checkbox').removeAttr('checked').change();
		return false;
  	}

</script>
