<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a>  
	&gt; InstantTickets
</div>

<c:if test="${not empty error}">
  <div class="error">
    <div class="errorText">
      ${error}
    </div>
  </div>
</c:if>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<h1> SH Instant Tickets </h1>		
<form action="InstantTickets" method="POST">
  List of Event IDs of events to be included for instant tickets (separated by a comma):<br/>
  <textarea name="eventIds" rows="4" cols="120">${eventIds}</textarea><br/>
  <input type="submit" class="medButton" name="action" value="update"/>
</form>


<div class="info">
	<div class=infoText">
		The CSV file is in the following format: <br/>
		Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId
	</div>	
</div>
<br/>
Please click on the link to download the StubHub InstantTickets file in csv format:<br/>
<a href="DownloadInstantTicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Instant Ticket CSV</a></br>
<a href="DownloadInstantTicketTNDCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Instant TND Ticket CSV</a></br>
<a href="DownloadCustomizedInstantTicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Customized Instant Ticket CSV</a>
		