<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script src="http://handsontable.com/dist/handsontable.full.js"></script>
<link rel="stylesheet" media="screen" href="http://handsontable.com/dist/handsontable.full.css">
<body>
<div id = "info" style="background-color:#8181F7">
	<span id ="message" ></span>
	<br/>
	<br/>
</div>
<br/>
<div align = "center">
<form action="EditorLoadCategoryMapping" method="post" id="formEditorLoadCategoryMapping" name="formEditorLoadCategoryMapping">
	<table>
		<tr>
			<td>
				Venue :
			</td>
			<td>
			<span id="venueLabelSpanId" style="display: none;">
				<span id="venueNameDiv" >

					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" style="display: block;">
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
						<input type="hidden" id="venueId" name="venueId"/>
				<%-- <select id ="venueId" name ="venueId" onchange="javascript:getAllCategoryGroupbyVenue($(this).val(),'')">
					<option value=""> --- Select Venue--- </option>
					<c:forEach var="venue" items="${venues}">
						<option value="${venue.id}" <c:if test="${venue.id == venueId}">selected</c:if>> ${venue.building}</option>
					</c:forEach>
				</select> --%>
			</td>
		</tr>
	
	<tr>
			<td>
				Category Group :
			</td>
			<td>
				<select id ="categoryGroup" name ="categoryGroup">
					<option value=""> --- Select Category Group --- </option>
					<c:forEach var="venueCat" items="${venueCategories}">
						<option value="${venueCat.categoryGroup}" <c:if test ="${categoryGroup==venueCat.categoryGroup}">selected</c:if>>${venueCat.categoryGroup}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
	
		<tr>
			<td colspan ="2" align ="center">
				<input type="button" name="load" id="load" class="medButton" value="Load" />
				<input type="button" name="save" id="save" class="medButton" value ="Save" />
			</td>
		</tr>
	</table>
</form>
</div>
<br/>
<br/>
<br/>
<div id="excelcontainer" ></div>
<!--<pre id="exampleconsole"></pre> -->



</body>
<script type="text/javascript">

$(document).ready(function(){
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1] + " - " + row[0];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val('');
		$("#venueId").val(row[0]);
		$('#venueNameDiv').text(row[1]);
			
		$('#venueLabelSpanId').show();
		$('#venueSelectSpanId').hide();
		
		getAllCategoryGroupbyVenue(row[0],'');
	});
});
		
		function callChangeVenue(){
			$('#venueLabelSpanId').hide();
			$('#venueSelectSpanId').show();
			$('#venueId').val('');
		}

$('#info').hide();
function getAllCategoryGroupbyVenue(venueId,categoryGroup){
	$.ajax({
		type: "post",
		dataType:'text',
		url:"GetVenueCategoriesByVenue",
		data: {venueId:venueId},
		//cache:false,
		success: function(res){
			if(res.trim()!=''){
				var jsonData = JSON.parse(res);
				$('#categoryGroup').children().remove();
				var rowText = "<option value=''>--- Select Category Group ---</option>"
				for (var i = 0; i < jsonData.length; i++) {
					var data= jsonData[i]; 
					var selected ='';
					if(data.name== categoryGroup){
						selected = "selected ='selected'";
					}
					rowText += "<option " + selected + " value=" + data.name + ">" + data.name + "</option>"
				}	
				$('#categoryGroup').append(rowText);
			}else{
				$('#categoryGroup').children().remove();
				$('#categoryGroup').append("<option value=''>--- Select Category Group ---</option>");
			}
		},
		error: function(request, error) {
			alert(" Can't do because: " + error);
		}
	});
}

var data = ${categories};
 var
    $$ = function(id) {
      return document.getElementById(id);
    },
    container = $$('excelcontainer'),
   // exampleConsole = $$('exampleconsole'),
    load = $$('load'),
    save = $$('save');
    
    
	var hot = new Handsontable(container, {
	  data: data,
	  minSpareRows: 1,
	  rowHeaders: true,
	  colHeaders: ['Venue', 'Section', 'Row', 'Seat','Category','Category Group','Larry Section','Equivalent Cats','Normalized Section and Row','Row Range','LastRow','Alternate Row'],
	  contextMenu: true,
	  
	});

	Handsontable.Dom.addEvent(load, 'click', function() {
		$('#formEditorLoadCategoryMapping').submit();
	});

  Handsontable.Dom.addEvent(save, 'click', function() {
    // save all cell's data
	var argumentDetails ={data: JSON.stringify(hot.getData()),venueId:$('#venueId').val(),categoryGroup:$('#categoryGroup').val()};
	$.ajax({
		type: "post",
		dataType:'text',
		url:"EditorSaveCategoryMapping",
		data: argumentDetails,
		//cache:false,
		success: function(res){
			var temp = res.split('&');
			var venueId;
			var info;
			var categoryGroup;
			for(var i=0;i<temp.length;i++){
				var args = temp[i];
				if(args.indexOf('venueId')>=0){
					venueId = args.split('=')[1];
				}else if(args.indexOf('categoryGroup')>=0){
					categoryGroup = args.split('=')[1];
				}else if(args.indexOf('info')>=0){
					info = args.split('=')[1];
				}
			}
			
			$('#venueId').val(venueId);
			getAllCategoryGroupbyVenue(venueId,categoryGroup);
			$('#message').html(info);
			$('#info').show();
		},
		error: function(request, error) {
			alert(" Can't do because: " + error);
		}
	});
    
  });

  
</script>
