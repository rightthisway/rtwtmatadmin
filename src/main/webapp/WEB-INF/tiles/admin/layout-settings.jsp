<%@include file="/WEB-INF/tiles/taglibs.jsp"%>


<tiles:useAttribute id="selectedSettingMenu" name="selectedSettingMenu"
	scope="request" />

<div id="adminSubMenu">
	
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Mail Settings'}">
			<b>Mail Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditMailerProperties">Mail Settings</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Feedback Settings'}">
			<b>Feedback Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditFeedbackSettings">Feedback Settings</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Alert Settings'}">
			<b>Alert Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditAlertSettings">Alert Settings</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Quotes'}">
			<b>Quotes Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminQuotes">Quotes Settings</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Crawler Settings'}">
			<b>Crawler Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditCrawlerProperties">Crawler Settings</a>
		</c:otherwise>
	</c:choose>
	<%-- |
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Default Autopricing'}">
			<b>Default Autopricing</b>
		</c:when>
		<c:otherwise>
			<a href="DefaultAutopricing">Default Autopricing</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Exchange Markup'}">
			<b>Exchange Markup</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditExchangeMarkup">Exchange Markup</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSettingMenu == 'Log Files'}">
			<b>Log Files</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditLogFiles">Log Files</a>
		</c:otherwise>
	</c:choose> --%>
</div>

<tiles:insertAttribute name="subBody1" />
	