	<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
	
	<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
	<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
	<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
	<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />
	
	<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	  <a href="..">Home</a>
	  &gt; Edit SH Instant Events
	</div>
	
	<c:if test="${not empty info}">
	<div class="info">
	  <div class="infoText">
	    ${info}
	  </div>
	</div>
	</c:if>
	
	<form id="eventSelector" method="POST" action="EditInstantEvents" >
	
	Tour: 
	<select name="tourId" id="tourIdSelect">
	<option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
	  <c:forEach var="tour" items="${tours}">
	    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
	  </c:forEach>
	</select>
	
	<c:choose>
	  <c:when test="${tourId == 'ALL'}">
	    <c:set var="maxTitleChars" value="10"/>
	  </c:when>
	  <c:otherwise>
	    <c:set var="maxTitleChars" value="22"/>
	  </c:otherwise>
	</c:choose>
	<input id="action" type="hidden" name="action" value="" /> 
	<!-- <input id="eventSubmit" type="submit" value="EDIT" class="medButton"/> -->
	<span id="loadingSpan" style="display:none">
	  <img src="../images/process-running.gif" align="absbottom"/> Loading...
	</span>
	
	<br />
	<br />
	<br />
	<div align="center">
		<input type="button" class="medButton" value="Update" onclick="update();"/> &nbsp;
		<input type="button" class="medButton" value="Remove" onclick="remove();"/> &nbsp;
	    <input type="button" class="medButton" value="Cancel" onclick="$('#minThresholdSpan').show(); $('#editminThresholdSpan').hide()" />
	<br />
	<br />
	<a href="javascript:selectAll()">All</a>
	<a href="javascript:selectNone()">None</a>
	
	<input onClick="javascript:copyAll()" type="checkbox" id="copyAllCheckbox" >Copy All Parameter of first Event to all remaining Events
	<br />
		<display:table class="list" name="${instantEvents}" id="instantEvent" requestURI="EditInstantEvents">
			<display:column >
					<input type="checkbox" id="event_${instantEvent.event.id}" name="event_${instantEvent.event.id}" class="checkboxes"/>
			</display:column>
			<display:column title="Id" property="event.id" sortable="true"/>
			<display:column title="Name" property="event.name" sortable="true"/>
			<display:column title="Date" property="event.localDate" sortable="true" format="{0,date,MMM dd yyyy}"/>
			<display:column title="Time" property="event.localTime" sortable="true" format="{0,date,hh:mm aa}"/>
			<display:column title="MinThreshold">
				<input type="text" id="minThreshold-event_${instantEvent.event.id}" name="minThreshold-event_${instantEvent.event.id}" value="${instantEvent.minThreshold}" size="5"/>
			</display:column>
			<display:column title="MaxThreshold" sortable="true">
				<input type="text" id="maxThreshold-event_${instantEvent.event.id}"
				name="maxThreshold-event_${instantEvent.event.id}" value="${instantEvent.maxThreshold}" size="5"/>
			</display:column>
			<display:column title="TimeExpiry">
				<input type="text" id="timeExpiry-event_${instantEvent.event.id}" name="timeExpiry-event_${instantEvent.event.id}" value="${instantEvent.expiryTime}" size="5"/>
			</display:column>
			<display:column title="Markup" sortable="true">
				<input type="text" id="markupPercent-event_${instantEvent.event.id}" name="markupPercent-event_${instantEvent.event.id}" value="${instantEvent.markupPercent}" size="5"/>
			</display:column>
			<display:column title="Sales%">
				<input type="text" id="salesPercent-event_${instantEvent.event.id}" name="salesPercent-event_${instantEvent.event.id}" value="${instantEvent.salesPercent}" size="5"/>
			</display:column>
			<display:column title="Shipping Fee" sortable="true">
				<input type="text" id="shippingFee-event_${instantEvent.event.id}" name="shippingFee-event_${instantEvent.event.id}" value="${instantEvent.shippingFee}" size="5"/>
			</display:column>
		</display:table>
		<br />
		<input type="button" class="medButton" value="Update" onclick="update();"/> &nbsp;
		<input type="button" class="medButton" value="Remove" onclick="remove();"/> &nbsp;
	    <input type="button" class="medButton" value="Cancel" onclick="$('#minThresholdSpan').show(); $('#editminThresholdSpan').hide()" />
	
	</div>
	    
	  </form>
	
	
	 
	<script type="text/javascript">
	$('#eventList').hide();
	$(document).ready(function(){
	
	$('#tourIdSelect').change(function(){ 
		
	if ($(this).val()== $('#tourIdSelect').val())         
	      document.location.href="EditInstantEvents?tourId=" + $(this).val();
		});
		
	});
	function selectAll(){
		$(".checkboxes").each(function(){
			this.checked=true;
		});
	}
	function selectNone(){
		$(".checkboxes").each(function(){
			this.checked=false;
		});
	}
	
	function validate(){
			$(".checkboxes").each(function(){
				if(this.checked){
					if(!isFloat($("#maxThreshold-"+this.id).val())){
						alert($("#maxThreshold"+this.id).val() + " is not valid.");
						$("#maxThreshold"+this.id).focus();
						return false;
					}
					if(!isFloat($("#minThreshold-"+this.id).val())){
						alert($("#minThreshold-"+this.id).val() + " is not valid.");
						$("#minThreshold-"+this.id+"").focus();
						return false;
					}
					if(!isInteger($("#timeExpiry-"+this.id).val())){
						alert($("#timeExpiry-"+this.id).val() + " is not valid.");
						$("#timeExpiry-"+this.id).focus();
						return false;
					}
					if(!isFloat($("#markupPercent-"+this.id).val())){
						alert($("#markupPercent-"+this.id).val() + " is not valid.");
						$("#markupPercent-"+this.id).focus();
						return false;
					}
					if(!isFloat($("#salesPercent-"+this.id).val())){
						alert($("#salesPercent-"+this.id).val() + " is not valid.");
						$("#salesPercent-"+this.id).focus();
						return false;
					}
					if(!isFloat($("#shippingFee-"+this.id).val())){
						alert($("#shippingFee-"+this.id).val() + " is not valid.");
						$("#shippingFee-"+this.id).focus();
						return false;
					}
				}
			});
			return true;
	}
	function update(){
		var flag = false;
		$(".checkboxes").each(function(){
			if(this.checked){
				flag=true;
			}
		});
		if(!flag){
			alert("Please select atleaset one event");
			return false;
		}
		if(validate()){
			$('#action').val('updateEvents');
			$('#eventSelector').submit();
		}
	}
	
	function remove(){
		var flag = false;
		$(".checkboxes").each(function(){
			if(this.checked){
				flag=true;
			}
		});
		if(!flag){
			alert("Please select atleaset one event");
			return false;
		}
		$('#action').val('removeEvents');
		$('#eventSelector').submit();
	}
	function isInteger(value){
		var integerExpression=/^\d+$/;
		return integerExpression.test(value);
	}
	
	function isFloat(value){
		var integerExpression=/^\d+.{0,1}\d{0,2}$/;
		return integerExpression.test(value);
	}
	
	function copyAll(){
		selectAll();
		var isFirst=true;
		var minThreshold;
		var maxThreshold;
		var timeExpiry;
		var markupPercent;
		var salesPercent;
		var shippingFee;

		$(".checkboxes").each(function(){
			if(!$("#copyAllCheckbox").attr('checked')){
				return;
			}
			if(isFirst){
				if(!isFloat($("#maxThreshold-"+this.id).val())){
					alert($("#maxThreshold-"+this.id).val() + " is not valid.");
					$("#maxThreshold-"+this.id).focus();
					return false;
				}
				if(!isFloat($("#minThreshold-"+this.id).val())){
					alert($("#minThreshold-"+this.id).val() + " is not valid.");
					$("#minThreshold-"+this.id).focus();
					return false;
				}
				if(!isInteger($("#timeExpiry-"+this.id).val())){
					alert($("#timeExpiry-"+this.id).val() + " is not valid.");
					$("#timeExpiry-"+this.id).focus();
					return false;
				}
				if(!isFloat($("#markupPercent-"+this.id).val())){
					alert($("#markupPercent-"+this.id).val() + " is not valid.");
					$("#markupPercent-"+this.id).focus();
					return false;
				}
				if(!isFloat($("#salesPercent-"+this.id).val())){
					alert($("#salesPercent-"+this.id).val() + " is not valid.");
					$("#salesPercent-"+this.id).focus();
					return false;
				}
				if(!isFloat($("#shippingFee-"+this.id).val())){
					alert($("#shippingFee-"+this.id).val() + " is not valid.");
					$("#shippingFee-"+this.id).focus();
					return false;
				}
				isFirst=false;
				maxThreshold=$("#maxThreshold-"+this.id).val();
				minThreshold=$("#minThreshold-"+this.id).val();
				timeExpiry=$("#timeExpiry-"+this.id).val();
				markupPercent=$("#markupPercent-"+this.id).val();
				salesPercent=$("#salesPercent-"+this.id).val();
			}
			else{
				$("#maxThreshold-"+this.id).val(maxThreshold);
				$("#minThreshold-"+this.id).val(minThreshold);
				$("#timeExpiry-"+this.id).val(timeExpiry);
				$("#markupPercent-"+this.id).val(markupPercent);
				$("#salesPercent-"+this.id).val(salesPercent);

			}
		});
	}
	</script>
