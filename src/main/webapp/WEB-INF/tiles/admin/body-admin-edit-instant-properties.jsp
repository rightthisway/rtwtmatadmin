<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Edit Instant File Properties
</div>

<h1>Instant File Properties</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<form:form commandName="instantFilesPropertiesCommand" method="post">
	<fieldset>
	 	<legend>SH Instant File Property</legend>
		<table>
		<tr>
				<td>File Name</td>
				<td>
					<form:input path="instantTicketsScrubedTNWithThresholdFileLoc" cssClass="longInput" />
				</td>
			</tr>
			<tr>
				<td>Notes</td>
				<td>
					<form:input path="instantTicketsNotes" cssClass="longInput"/>
				</td>
			</tr>
			<tr>
				<td>Scrubbed % Check</td>
				<td>
					<form:input path="shinstantTnScrubPercentCheck" cssClass="shortInput" />
				</td>
			</tr>
			<tr>
				<td>%Markup</td>
				<td>
					<form:input path="shMarkupPercent" cssClass="shortInput" />
				</td>
			</tr>
		</table>
	</fieldset>
	<br />
	<fieldset>
	 	<legend>TN Mercury File Scrubed Without Threshold Property</legend>
		<table>
			<tr>
				<td>File Name</td>
				<td>
					<form:input path="mercuryTicketsScrubedNothresholdFileLoc" cssClass="longInput" />
				</td>
			</tr>
			<tr>
				<td>Notes</td>
				<td>
					<form:input path="mercuryTicketsNotes" cssClass="longInput"/>
				</td>
			</tr>
			
		</table>
	</fieldset>
	<br />
	<fieldset>
	 	<legend>TN Mercury Categorized File Scrubed</legend>
		<table>
			<tr>
				<td>File Name</td>
				<td>
					<form:input path="mercuryTicketsCategorizedScrubFileLoc" cssClass="longInput" />
				</td>
			</tr>
			<tr>
				<td>Notes</td>
				<td>
					<form:input path="mercuryTicketsCategorizedScrubNotes" cssClass="longInput"/>
				</td>
			</tr>
			
		</table>
	</fieldset>
	<br />
	<fieldset>
	 	<legend>TN Mercury File Scrubed Without Threshold</legend>
		<table>
			<tr>
				<td>File Name</td>
				<td>
					<form:input path="mercuryTndScrubedTicketsFileLoc" cssClass="longInput" />
				</td>
			</tr>
			<tr>
				<td>Notes</td>
				<td>
					<form:input path="mercuryTndScrubedTicketsNotes" cssClass="longInput"/>
				</td>
			</tr>
			
		</table>
	</fieldset>
	<br/>
		<fieldset>
	 	<legend>TN Mercury Common Properties</legend>
		<table>
			<tr>
				<td>Markup %</td>
				<td>
					<form:input path="tnMarkupPercent" cssClass="shortInput" />
				</td>
			</tr>
			<tr>
				<td>Scrubbed % Check</td>
				<td>
					<form:input path="tnMercuryScrubPercentCheck" cssClass="shortInput"/>
				</td>
			</tr>
			
		</table>
	</fieldset>
	<br/>
	<fieldset>
	<legend>TicketsNow Instant File Scrubbed by SH Property</legend>
		<table>
			<tr>
				<td>File Name</td>
				<td>
					<form:input path="tnowScrubedSHFileLoc" cssClass="longInput" />
				</td>
			</tr>
		</table>
	</fieldset>
	<fieldset>
	<legend>TicketsNow Instant File Scrubbed by TN Property</legend>
		<table>
			<tr>
				<td>File Name</td>
				<td>
					<form:input path="tnowScrubedTNFileLoc" cssClass="longInput" />
				</td>
			</tr>
			
			</table>
	</fieldset>
	<fieldset>
	<legend>TicketsNow Instant File Common Property</legend>
		<table>
			<tr>
				<td>Notes</td>
				<td>
					<form:input path="tnowInstantTicketsNotes" cssClass="longInput"/>
				</td>
			</tr>
			<tr>
				<td>Scrubbed % Check</td>
				<td>
					<form:input path="tnowInstantScrubPercentCheck" cssClass="shortInput" />
				</td>
			</tr>
			</table>
	</fieldset>
	<center><input type="submit" class="medButton"></center>
	<br />

</form:form>
