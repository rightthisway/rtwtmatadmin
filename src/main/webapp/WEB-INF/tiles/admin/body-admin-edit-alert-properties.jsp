<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Edit Alert Properties
</div>

<h1>Edit Alert Properties</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<form action="AdminEditAlertSettings" method="post">

<table>
	<tr>
		<td>CC all alerts to (separate email by commas, leave blank if unused):</td>
		<td><input name="alertMailCc" id="alertMailCc" value="${alertMailCc}" type="text" style="width:200px"/></td>
	</tr>
	<tr>
		<td>BCC all alerts to (separate email by commas, leave blank if unused):</td>
		<td><input name="alertMailBcc" id="alertMailBcc" value="${alertMailBcc}" type="text" style="width:200px"/></td>
	</tr>
	<!-- 
	<tr>
		<td>CC all portal alerts to (separate email by commas, leave blank if unused):</td>
		<td><input name="alertPortalMailCc" value="${alertPortalMailCc}" type="text" style="width:200px"/></td>
	</tr>
	<tr>
		<td>BCC all portal alerts to (separate email by commas, leave blank if unused):</td>
		<td><input name="alertPortalMailBcc" value="${alertPortalMailBcc}" type="text" style="width:200px"/></td>
	</tr>
	 -->	
	<tr>
		<td>Send alerts to the associated market maker:</td>
		<td><input name="alertMailMarketMakerEnabled" type="checkbox" <c:if test="${alertMailMarketMakerEnabled}">checked</c:if> /></td>
	</tr>
	<tr>
		<td>Number of alert threads:</td>
		<td>
			<select name="alertThreadCount">
				<c:forEach var="i" begin="1" end="10">
					<option value="${i}" <c:if test="${alertThreadCount == i}">selected</c:if>>${i}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
</table>

<input type="hidden" name="action" value="update" />
<input class="medButton" type="submit" value="Save Changes" name="submit" onclick="return ValidateData();"/>
<input class="medButton" type="button" onclick="document.location.href='AdminMaintenance'" value="Cancel"/>
</form>
<script type="text/javascript">

function ValidateData(){	
	if(document.getElementById('alertMailCc').value==""){
		alert("Please enter the CC all alerts to");
		return false;
		
	}else{
		return true;
	}
	/*else if(document.getElementById('alertMailBcc').value==""){
		alert("Please enter the BCC all alerts to");
		return false;
	}*/
	
	

}
</script>