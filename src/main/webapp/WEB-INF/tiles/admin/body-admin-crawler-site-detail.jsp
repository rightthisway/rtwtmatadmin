<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="EditorCrawlerCounter">Counter</a> &gt; CounterBySite
</div>

<h2>Crawlers striked on ${siteId}</h2>
<form>
	<div>
		<table align="center">
			<tr>
				<td >
					<b>Start Date : </b>	
				</td>
				<td>
					<input type="text" name="startDate" id="startDate" value=${startDate}>
					<input type="hidden" value="${system}" name="system" id="system" >
				</td>
			</tr>
			<tr>
				<td >
					<b>End Date &nbsp;&nbsp;: </b>
				</td>
				<td>
					<input type="hidden" value="${siteId}" name="siteId" id="siteId" >
					<input type="text" name="endDate" id="endDate" value=${endDate}>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Submit" class="medButton" > 
				</td>
			</tr>
		</table>
	</div>
</form>	
<div align="center"">
	<display:table class="list" name="${crawls}" id="crawl" requestURI="CrawlerSiteCounter" >
	    <display:column title="CrawlId" style="18px;" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
		    <a href="javascript:void(0)" onclick="getCrawlerHistory(${crawl.crawlId},'${crawl.system}',$('#startDate').val(),$('#endDate').val())">${crawl.crawlId}</a>
	    </display:column>
	    <display:column title="System" property="system" sortable="true"/>
	    <display:column title="Name" property="siteId" />
	    <display:column title="Total Hit" property="counter" />
	</display:table>
</div>


<script type="text/javascript">

		$('INPUT[name=startDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		$('INPUT[name=endDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
	function getCrawlerHistory(crawlId,system,startDate,endDate){
		window.location="CrawlerHistory?crawlId="+crawlId+"&system="+ system +"&startDate="+ startDate +"&endDate=" + endDate;
	}
</script>