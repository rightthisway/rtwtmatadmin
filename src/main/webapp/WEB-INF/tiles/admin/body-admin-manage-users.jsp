<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Manage Users
</div>


<form:form action="AdminManageUsers" method="post" id="formId">
<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<h1>User Management</h1>

<a href="AdminRegisterUser"><img src="../images/ico-plus.gif" align="absbottom" /> Add user</a>
<br /><br />
<div style="margin-top:16px;margin-bottom:8px;margin-left:539px;">
	<div style="float: left"><b>Brokers:</b></div>
				<select id="brokerId" name="brokerId" >
					<option value="">---ALL---</option>
					<c:forEach items="${brokers }" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${brokerId eq brokerObj.id}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>										
				</select>
				</div>
<br/> <br/>
				<input type="hidden" id="status" name="status"/>
				<input type="button" value="Get Users" onclick="submitForms('get');" class="medButton1">
				
<br/> <br/>
</form:form>
<div style="margin-top:16px;margin-bottom:8px;">
	<div style="float: left"><b>Legends:</b></div>
	
				

	<img src="../images/ico-user.gif" align="absbottom" /> Connected
	&nbsp;&nbsp;&nbsp;&nbsp;
	<img src="../images/ico-user-off.gif" align="absbottom" /> Not Connected
	&nbsp;&nbsp;&nbsp;&nbsp;
	<img src="../images/ico-lock.gif" align="absbottom" /> Locked (Disabled)
</div>
	
<display:table class="list" name="${users}" id="user" requestURI="AdminManageUsers">
    <display:column sortable="true">
    	<c:choose>
    		<c:when test="${user.isLiveUser}">
		    	<img src="../images/ico-user.gif" align="absbottom" />
    		</c:when>
    		<c:otherwise>
		    	<img src="../images/ico-user-off.gif" align="absbottom" />
    		</c:otherwise>
    	</c:choose>
    	<a href="AdminDetailUser?userName=${user.username}">${user.username}</a>
    	
    	<c:if test="${user.locked}">
    		<img src="../images/ico-lock.gif" align="absbottom" />
    	</c:if>    	
    </display:column>
    <display:column title="Application" sortable="true">
    <b>${user.applicationName}</b>
     </display:column>
    <display:column title="E-Mail" sortable="true">
    	<img src="../images/ico-mail.gif" align="absbottom" />${user.email}
    </display:column>
    <display:column title="First Name" sortable="true" property="firstName" />
    <display:column title="Last Name" sortable="true" property="lastName" />
    <display:column title="Roles" sortable="true" style="white-space:nowrap;width:370px;">

		<c:set var="hasSuperAdminRole" value="0" />
		<c:set var="hasAdminRole" value="0" />
		<c:set var="hasEditorRole" value="0" />
		<c:set var="hasUserRole" value="0" />
		<c:set var="hasEbayRole" value="0" />
		<c:set var="hasLiteAdminRole" value="0" />
		<c:set var="hasLiteUserRole" value="0" />
		<c:set var="hasBrokerUserRole" value="0" />

    	<c:forEach var="role" items="${user.roles}">
    		<c:set var="shortRole" value="${fn:substring(role.name, 5, fn:length(role.name))}" />
    		<c:choose>
    			<c:when test="${shortRole == 'SUPER_ADMIN'}"><c:set var="hasSuperAdminRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'ADMIN'}"><c:set var="hasAdminRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'EDITOR'}"><c:set var="hasEditorRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'USER'}"><c:set var="hasUserRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'EBAY'}"><c:set var="hasEbayRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'LITE_ADMIN'}"><c:set var="hasLiteAdminRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'LITE_USER'}"><c:set var="hasLiteUserRole" value="1" /></c:when>
	    		<c:when test="${shortRole == 'BROKER_USER'}"><c:set var="hasBrokerUserRole" value="1" /></c:when>
	    	</c:choose>	    	    			
    	</c:forEach>
    	
    	<div class="superAdminRoleTag" <c:if test="${hasSuperAdminRole == 0}">style="visibility:hidden"</c:if> >SUPER ADMIN</div>
    	<div class="adminRoleTag" <c:if test="${hasAdminRole == 0}">style="visibility:hidden"</c:if> >ADMIN</div>
    	<div class="editorRoleTag" <c:if test="${hasEditorRole == 0}">style="visibility:hidden"</c:if> >EDITOR</div>
    	<div class="userRoleTag" <c:if test="${hasUserRole == 0}">style="visibility:hidden"</c:if> >USER</div>
    	<div class="ebayRoleTag" <c:if test="${hasEbayRole == 0}">style="visibility:hidden"</c:if> >EBAY</div>
		<div class="liteAdminRoleTag" <c:if test="${hasLiteAdminRole == 0}">style="visibility:hidden"</c:if> >LITE ADMIN </div>
		<div class="liteUserRoleTag" <c:if test="${hasLiteUserRole == 0}">style="visibility:hidden"</c:if> >LITE USER </div>
		<div class="ebayRoleTag" <c:if test="${hasBrokerUserRole == 0}">style="visibility:hidden"</c:if> >BROKER USER </div>
		
    </display:column>
    <display:column title="Phone" sortable="true">
    	<c:if test="${not empty user.phone}">
    		<img src="../images/ico-phone.gif" align="absbottom" />${user.phone}
    	</c:if>
    </display:column>
    <display:column>
    	<a href="AdminEditUser?username=${user.username}"><img src="../images/ico-edit.gif" align="absbottom" /> Edit</a>
    </display:column>
    <display:column>
    	<a href="#" id="${user.username}" onclick="confirmDelete(this.id)"><img src="../images/ico-delete.gif" align="absbottom" /> Delete</a>
    </display:column>
    <display:column>
    	<a href="AdminUserAudit?userName=${user.username}" id="${user.username}" ><img src="../images/ico-edit.gif" align="absbottom" /> Audit</a>
    </display:column>
</display:table>

<h2>Upload Market Maker Event Mappings</h2>

<c:if test="${not empty error}">
<div class="error">
<div class="errorText">
${error}
</div>
</div>
</c:if>

<div class="info">
	<div class="infoText">
		The required format for Market Maker Event Mappings is: 
		<br/>
		User Name, Event Name, Event Date, Event Time, Event Venue, Percent Coverage, Start Date, End Date
	</div>
</div> 

<form enctype="multipart/form-data" action="AdminUploadMMEventCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
<b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" value="Upload"/>
</form>
<script type="text/javascript">
  function confirmDelete(username) {
    var answer = confirm("Are you sure you want to delete the user " + username + "? (All his crawls will be assigned to Admin)");
    if (answer) {
      location.href='AdminDeleteUser?username=' + username;
    }
  }
  
  function submitForms(action){
		$("#status").val(action);
		$("#formId").submit();
	}
  
  
  
</script>