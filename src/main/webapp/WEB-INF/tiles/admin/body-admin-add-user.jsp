<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="AdminManageUsers">Manage Users</a> 
  &gt; Add User
</div>

<h1>Add user</h1>

<form:form action="AdminRegisterUser" commandName="user" method="POST" onsubmit="return addUser()">
<table>
	<tr>
		<td>Username:</td>
		<td>
		  <form:input path="username" />
          <form:errors path="username" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>Password:</td>
		<td>
		  <form:password path="password"/>
          <form:errors path="password" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>Re-enter Password:</td>
		<td>
		  <form:password path="password2"/>
          <form:errors path="password2" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>E-Mail:</td>
		<td>
		  <form:input path="email"/>
          <form:errors path="email" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>First Name:</td>
		<td>
		  <form:input path="firstName" />
          <form:errors path="firstName" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>Last Name:</td>
		<td>
		  <form:input path="lastName"/>
          <form:errors path="lastName" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>Phone:</td>
		<td>
		  <form:input path="phone" />
          <form:errors path="phone" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td>Locked (Disabled):</td>
		<td>
		  <form:radiobutton path="locked" value="false" />False
		  <form:radiobutton path="locked" value="true" />True
          <form:errors path="locked" cssClass="error" />
		</td>
	</tr>
	<tr>
	  <td>Roles:</td>
      <td>
      	  <div id="radio">
			<form:radiobutton  path="rolesStrList" value="role_super_admin" />Super Admin
			<form:radiobutton path="rolesStrList" value="role_editor" />Editor
			<form:radiobutton path="rolesStrList" value="role_user"  />User
			<form:radiobutton path="rolesStrList" value="role_offshore"  />Offshore Admin
			<form:radiobutton path="rolesStrList" value="role_broker_user" />Broker User
			<form:radiobutton path="rolesStrList" value="role_lite_admin"  />Lite Admin
			<form:radiobutton path="rolesStrList" value="role_lite_browse" />Lite Browse
			<form:radiobutton path="rolesStrList" value="role_lite_user" />Lite User
			<form:errors path="roles" cssClass="error" />
      </td>
	</tr>
	<tr id="brokerList" style="display:none;">
		<td>Broker:</td>
		<td>
		<form:select id="brokername" multiple="true" path="brokerId">
			<%-- <form:option value="">---Select---</form:option> --%>
			<form:option value="0">---ALL---</form:option>
			<c:forEach items="${brokers}" var="broker">
				<form:option value="${broker.id}">${broker.name}</form:option>
			</c:forEach>
		</form:select>
          <form:errors path="brokerId" cssClass="error" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Create User" class="medButton" />
		</td>
	</tr>
</table>
</form:form>

<script language="javascript" type="text/javascript">

$(document).ready(function(){


if($("#radio5").attr('checked'))
{
	$("#brokerList").css('display','');
}


if($("#radio6").attr('checked'))
{
	$("#brokerList").css('display','');
}
$("#radio8").click(function(){
	
	$("#radio").hide();
    	
   }
);
$("#radio7").click(function(){
	
	$("#radio").show();
  	
   }
);
});

var isBrowseLiteSelected = false;

function processBroker(brokerValue)
{
	
	var brokerList = document.getElementById("brokerList");
	
		if(brokerValue == "role_lite_admin" ||brokerValue == "role_lite_user" )
		{
			brokerList.setAttribute('style','');
			isBrowseLiteSelected=true;
		}
		else
		{
			brokerList.setAttribute('style','display:none');
			isBrowseLiteSelected=false;
		}
}

function addUser()
{
	if(isBrowseLiteSelected)
	{
		var brokername = document.getElementById("brokername");
		brokername = brokername.options[brokername.selectedIndex].value;
		if(brokername == "")
		{
		alert("Please select a broker.");
		return false;
		}
	}
	
	
		
		
	//return true;
}

</script>