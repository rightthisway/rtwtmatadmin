<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Edit Crawler Properties
</div>

<div id="quickLinks">		 	
  <%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
    <a href="EditorCrawlerControls">Crawler Controls</a>
  <%-- </authz:authorize> --%>
</div>
<div style="clear:both"></div>

<h1>Edit Crawler Properties</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<c:if test="${not empty param.error}">
  <div class="error"><div class="errorText">${param.error}</div></div>
</c:if>

<form:form commandName="crawlerPropertiesCommand" method="POST" onsubmit="submitForm()">
	<h2>Crawler Properties</h2>
	<table>
		<tr>
			<td>Start Crawler at Startup</td>
			<td>
				<form:checkbox path="crawlerStartupEnabled" />
				<form:errors path="crawlerStartupEnabled" cssClass="error" />	
			</td>
		</tr>
		
		<tr>
			<td>Crawler Thread Count</td>
			<td>
				<form:select path="crawlerThreadCount">
					<c:forEach var="c" begin="1" end="20">
						<form:option value="${c}">${c}</form:option>
					</c:forEach>
				</form:select>
				<form:errors path="crawlerThreadCount" cssClass="error" />	
			</td>
		</tr>
		<tr>
			<td>Crawler Duration Threshold</td>
			<td>
				<form:select path="crawlMaxDuration">
					<c:forEach var="c" begin="1" end="30">
						<form:option value="${c}">${c} minutes</form:option>
					</c:forEach>
				</form:select>			
			</td>
		</tr>
	</table>

	<h2>Crawler Scheduling</h2>
	  <table>
	    <tr>
	      <td>Scheduler Mode:</td>
	      <td>
	        <form:select path="crawlerSchedulerMode" onchange="changeSchedularMode(this.value);">
	          <form:option value="ALWAYS_SCHEDULED">ALWAYS SCHEDULED (prioritized)</form:option>
	          <form:option value="ALWAYS_UNSCHEDULED">ALWAYS UNSCHEDULED (same priority for all)</form:option>
	          <form:option value="SCHEDULED_UNSCHEDULED">SCHEDULED UNSCHEDULED (both)</form:option>
	        </form:select>
	      </td>
	    </tr>
	    <tr id="sportsSchedulePropertiesTr" <c:if test="${crawlerSchedulerMode == 'ALWAYS_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td width="180px">Sports Schedule Properties: </td>
	      <td>
	      <form:hidden path="crawlerSchedulerPropertiesString"/>
	        <form:input path="crawlerSportsSchedulerPropertiesString" cssClass="longInput" />
	         <i>
	  Format is: numDays d = number unit, ... (e.g., 0d=30m, 3d=1h, 7d=50y which means if the event occurs between 0 and 3 days, the crawl frequency
	  will be 30 minutes, between 3 and 7, it will be 1 hour and more than 7 days will be every 50 year.)
	         </i>
	     </td> 
	    </tr>
	    
	    <tr id="concertsSchedulePropertiesTr" <c:if test="${crawlerSchedulerMode == 'ALWAYS_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td width="180px">Concert Schedule Properties: </td>
	      <td>
	        <form:input path="crawlerConcertsSchedulerPropertiesString" cssClass="longInput" />
	         
	     </td> 
	    </tr>
	    
	    <tr id="theaterSchedulePropertiesTr" <c:if test="${crawlerSchedulerMode == 'ALWAYS_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td width="180px">Theater Schedule Properties: </td>
	      <td>
	        <form:input path="crawlerTheaterSchedulerPropertiesString" cssClass="longInput" />
	        
	     </td> 
	    </tr>
	    
	    <tr id="defaultSchedulePropertiesTr" <c:if test="${crawlerSchedulerMode == 'ALWAYS_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td width="180px">Default Schedule Properties: </td>
	      <td>
	        <form:input path="crawlerDefaultSchedulerPropertiesString" cssClass="longInput" />
	        
	     </td> 
	    </tr>
	    
	    <tr id="lasVegasSchedulePropertiesTr" <c:if test="${crawlerSchedulerMode == 'ALWAYS_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td width="180px">Las Vegas Schedule Properties: </td>
	      <td>
	        <form:input path="crawlerLasVegasSchedulerPropertiesString" cssClass="longInput" />
	        
	     </td> 
	    </tr>
	    
	    
	    <tr id="scheduledCrawlerPeriodTr" <c:if test="${crawlerPropertiesCommand.crawlerSchedulerMode != 'SCHEDULED_UNSCHEDULED'}">style="display:none"</c:if>>
	      <td>Scheduled Crawler Period: </td>
	      <td>
	        Start: 
	        <form:select path="crawlerSchedulerStartHour">
  			  <c:forEach var="c" begin="0" end="23">
		        <form:option value="${c}">${c}:00</form:option>
		      </c:forEach>
	        </form:select>
	        Stop: 
	        <form:select path="crawlerSchedulerStopHour">
  			  <c:forEach var="c" begin="0" end="23">
		        <form:option value="${c}">${c}:00</form:option>
		      </c:forEach>
	        </form:select>
	        <i>(Crawler will be running using frequency specified in in Schedule Properties during this period)</i>
	      </td>
	    </tr>
	    <tr>
	      <td>Idle Crawler Period:</td>
	      <td>
	        <form:checkbox id="crawlerIdlePeriodEnabled" path="crawlerIdlePeriodEnabled" onchange="if ($(this).attr('checked')) $('#idleCrawlerPeriodSpan').show(); else $('#idleCrawlerPeriodSpan').hide()" /><label for="crawlerIdlePeriodEnabled"> Enabled</label>
	        <span id="idleCrawlerPeriodSpan" <c:if test="${crawlerPropertiesCommand.crawlerIdlePeriodEnabled == false}">style="display:none"</c:if>>
		        - Start:
		        <form:select path="crawlerIdlePeriodStartHour">
	  			  <c:forEach var="c" begin="0" end="23">
			        <form:option value="${c}">${c}:00</form:option>
			      </c:forEach>
		        </form:select>
		        Stop:
		        <form:select path="crawlerIdlePeriodStopHour">
	  			  <c:forEach var="c" begin="0" end="23">
			        <form:option value="${c}">${c}:00</form:option>
			      </c:forEach>
		        </form:select>
		        <i>(Crawler will be suspended during this period, useful when doing backups)</i>
		      </span>
	      </td>
	    </tr>
	    <tr>
	      <td>Minimum Crawler Period: </td>
	      <td>
	        <form:select path="crawlerMinCrawlPeriod">
  			  <c:forEach var="c" begin="1" end="15">
		        <form:option value="${c}">${c} minutes</form:option>
		      </c:forEach>
	        </form:select>
	        <i>(A Crawl cannot be rerun if its last crawl was done within this period)</i>
	      </td>
	    </tr>
	    <tr>
	      <td>Proactive Factor: </td>
	      <td>
	        <form:select path="crawlerProactiveFactor">
  			  <c:forEach var="c" begin="0" end="100" step="10">
		        <form:option value="${c}">${c}%</form:option>
		      </c:forEach>
	        </form:select>
            <i>(Crawlers can start ahead of their expecting date by Proactive Factor x Frequency. Help reduce delay of lower priority crawls)</i>
		  </td>
	    </tr>
	    <tr>
	      <td>Sorting Time Slice:</td>
	      <td>
	        <form:select path="crawlerSortingTimeSlice">
  			  <c:forEach var="c" begin="1" end="30">
		        <form:option value="${c}">${c} minutes</form:option>
		      </c:forEach>
		    </form:select>
		    <i>(Time slice considered for ordering crawls by priority. Provide delay upper bound for lower priority crawls)</i>
		   </td>
	    </tr>
	  </table>

	<h2>Crawler Email Properties</h2>	
	<table>
		<tr>
			<td>Send crawler notifications to</td>
			<td>
				<form:input path="crawlerEmailTo" cssClass="longInput" />
				<form:errors path="crawlerEmailTo" cssClass="error" />	
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<small><i>Note: You can leave this field blank. If you want to define several recipients, separate them<br /> by comma (e.g. user1@test.com, user2@test.com)</i></small> 
			</td>
		</tr>
	
		<tr>			
			<td>Send e-mail on crawl error</td>
			<td>
				<form:checkbox path="crawlerEmailErrorEnabled" onchange="toggleCrawlerEmailTableVisibility(this.checked)" />
				<form:errors path="crawlerEmailErrorEnabled" cssClass="error" /><br />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table id="crawlerEmailTable" style="<c:if test="${not crawlerPropertiesCommand.crawlerEmailErrorEnabled}">display:none;</c:if>margin-left:40px;padding-left:10px;border-left: 2px  solid #850001">
					<tr>
						<td>Crawl Error Check Frequency (in seconds)</td>
						<td>
							<form:input path="checkFrequency" cssStyle="width:40px" />
							<form:errors path="checkFrequency" cssClass="error" />	
						</td>
					</tr>
					<tr>
						<td>Send email to crawl creator</td>
						<td>
							<form:checkbox path="crawlerEmailCreatorEnabled" />
							<form:errors path="crawlerEmailCreatorEnabled" cssClass="error" />	
						</td>
					</tr>
					<tr>
						<td>Send email to crawl last updater</td>
						<td>
							<form:checkbox path="crawlerEmailLastUpdaterEnabled" />
							<form:errors path="crawlerEmailLastUpdaterEnabled" cssClass="error" />	
						</td>
					</tr>
					<tr>
						<td>Ready Crawl Count Threshold</td>
						<td>
							<form:input path="readyCrawlCountThreshold" cssStyle="width:40px" />
							<form:errors path="readyCrawlCountThreshold" cssClass="error" />	
						</td>
					</tr>
					<tr>
						<td>Crawl Duration Threshold (in seconds)</td>
						<td>
							<form:input path="crawlDurationThreshold" cssStyle="width:40px" />
							<form:errors path="crawlDurationThreshold" cssClass="error" />	
						</td>
					</tr>
					<tr>
						<td>Send emails on NO TICKET FOUND</td>
						<td>
							<form:checkbox path="noTicketsFoundError" />
							<form:errors path="noTicketsFoundError" cssClass="error" />	
						</td>
					</tr>
					
				</table>
					
			</td>
		</tr>		
		<tr>			
			<td>Send e-mail when crawler is started/stopped</td>
			<td>
				<form:checkbox path="crawlerEmailStartStopEnabled" />
				<form:errors path="crawlerEmailStartStopEnabled" cssClass="error" />	
			</td>
		</tr>		

	</table>

	<h2>Ticket Identification</h2>
	<table>
		<tr>
			<td colspan="2">
				Enter the regular expression which matches the note that identifies the ticket as owned by AdmitOne.<br /> 			
				We are using the regular expression syntax defined on this <a href="http://java.sun.com/j2se/1.4.2/docs/api/java/util/regex/Pattern.html#sum">page</a>.
				
				Use commas to separate multiple notes. DO NOT use commas for ticket identification.
			</td>
		</tr>
		<tr>
			<td>AdmitOne Ticket Identification</td>
			<td>
				<form:input path="admit1NotePattern" cssClass="longInput" />
				<form:errors path="admit1NotePattern" cssClass="error" />					
			</td>
		</tr>
	</table>

	<h2>Ticket Expiration</h2>
	When crawl is successful (or found no tickets), all tickets processed during the previous crawls but not during this last crawl get expired.<br />
	In the case, the crawl yields an error (cannot connect to the website or a java exception), all tickets which were not updated for the number of hours below will be expired.<br />
	<br />
	
	Ticket Time To Live:	
	<form:select path="crawlerTicketTimeToLive">
		<form:option value="60">1 hour</form:option>
		<form:option value="120">2 hours</form:option>
		<form:option value="180">3 hours</form:option>
		<form:option value="240">4 hours</form:option>
	</form:select>	
	<form:errors path="crawlerTicketTimeToLive" cssClass="error" />
	<br /><br />
	Note: an expired ticket can be reactivated if it is processed again during a crawl.
	<br />	
	<br />	

	<div id="buttonContainer">			
		<input type="submit" name="submit" value="Save Changes" class="medButton" />
		<input type="button" value="Cancel" class="medButton" onclick="document.location.href='AdminEditCrawlerProperties'" />
	</div>

	<span id="loading" style="color:red;font-weight:bold;display:none">
		<img src="../images/process-running.gif" align="absbottom" /> Saving...		
		Please wait, this may take a while.
	</span>

</form:form>

<script type="text/javascript">
	function toggleCrawlerEmailTableVisibility(checked) {
		if (checked) {
			$('#crawlerEmailTable').show();
		} else {
			$('#crawlerEmailTable').hide();		
		}
	};
	
	function submitForm() {
		$('#buttonContainer').hide();
		$('#loading').show();
	};	
	
	function changeSchedularMode(value){
		  if(value  == 'SCHEDULED_UNSCHEDULED'){
			  $('#scheduledCrawlerPeriodTr').show(); 
			  $('#sportsSchedulePropertiesTr').show();
			  $('#concertsSchedulePropertiesTr').show();
			  $('#theaterSchedulePropertiesTr').show();
			  $('#defaultSchedulePropertiesTr').show();
			  $('#lasVegasSchedulePropertiesTr').show();
		  }else if(value  == 'ALWAYS_UNSCHEDULED'){
			  $('#scheduledCrawlerPeriodTr').hide();
			  $('#sportsSchedulePropertiesTr').hide(); 
			  $('#concertsSchedulePropertiesTr').hide(); 
			  $('#theaterSchedulePropertiesTr').hide(); 
			  $('#defaultSchedulePropertiesTr').hide(); 
			  $('#lasVegasSchedulePropertiesTr').hide(); 
		  }else{
			  $('#scheduledCrawlerPeriodTr').hide();
			  $('#sportsSchedulePropertiesTr').show();
			  $('#concertsSchedulePropertiesTr').show();
			  $('#theaterSchedulePropertiesTr').show();
			  $('#defaultSchedulePropertiesTr').show();
			  $('#lasVegasSchedulePropertiesTr').show();
		  }
	  }
	changeSchedularMode('${crawlerPropertiesCommand.crawlerSchedulerMode}');
</script>