<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	<a href="..">Home</a> &gt; 
	<a href="EditorCrawlerCounter">Counter</a> &gt; 
	<a href="CrawlerSiteCounter?siteId=${crawl.siteId}&system=${system}">CounterBySite </a> &gt;  
  	CrawlCounter
</div>
<h2>Crawler Detail</h2>
<form  method="post">
	<div align="center">
		<table bgcolor="white">
			<tr>
				<td>
					<b> CrawlId </b>:${crawl.id}  
				</td>
				<td>
					<b> Crawl Name </b>:${crawl.name}
				</td>
			</tr>
					<tr>
				<td>
					<b> Site Id </b>:${crawl.siteId}  
				</td>
				<td>
					<b> CrawlFrequency </b>: ${crawl.crawlFrequency} 
					
				</td>
			</tr>
	
			<tr>
				<td>
					<b> Creator </b>: ${crawl.creator}
					
				</td>
				<td>
					<b> LastUpdated By </b>: ${crawl.lastUpdater} 
	
				</td>
			</tr>
				<tr>
				<td>
					<b> Last successful run on </b>:  <fmt:formatDate type="both" dateStyle="default" timeStyle="default" value="${crawl.lastSuccessfulRun}"/>
				</td>
				<td>
					<b> LastUpdated on </b>: <fmt:formatDate type="both" dateStyle="default" timeStyle="default" value="${crawl.lastUpdated}"/>	
				</td>
			</tr>
			<tr>
				<td>
					StartDate:
				</td>
				<td>
					<input type="text" name="startDate" id="startDate" value="${startDate}">
				</td>
			</tr>
			<tr>
				<td>
					EndDate:
				</td>
				<td>
					<input type="text" name="endDate" id="endDate" value="${endDate}">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Submit" class="medButton" onClick="getCrawlerCounter(${crawl.id},$('#startDate').val(),$('#endDate').val())"> 
				</td>
			</tr>
		</table>
	<br />
	</div>
	
</form>
<div align="center">
	<display:table class="list" name="${hits}" id="hit" requestURI="CrawlerHistory" >
	    <display:column title="System" property="system" />
	    <display:column title="Successful Hit" >
	    	<fmt:formatDate pattern="MMMM dd, yyyy HH:mm:ss aa" value="${hit.successfulHit}" />
	    </display:column>
		<display:column title="Total Hit" property="counter" />
	</display:table>
</div>


<script type="text/javascript">

		$('INPUT[name=startDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		$('INPUT[name=endDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
	
function getCrawlerCounter(crawlId,startDate,endDate){
document.forms[0].action="CrawlerHistory?crawlId="+crawlId;
document.forms[0].submit();

}
</script>