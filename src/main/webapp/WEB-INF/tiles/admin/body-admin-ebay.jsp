<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; <a href="AdminMaintenance">Admin</a> 
  &gt; eBay
</div>


<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${ebayInfo}
  </div>
</div>
</c:if>

<h2>Settings</h2>

<c:if test="${not empty info || not empty param.info}">
	<div id="info" class="info">
	  <div id="infoText" class="infoText">
	    ${info} ${param.info}
	  </div>
	</div>
</c:if>

<form id="updateForm" method="POST" action="AdminEbay" onsubmit="$('.submitButton').hide();$('.updatingSpan').show()">
  <table>
    <tr>
      <td>Notification Recipients for any updates:</td>
      <td><input type="text" name="emails" value="${emails}" size="60"/> <i>(emails separated by commas)</i></td>
    </tr>
    <tr>
      <td>Email subject:</td>
      <td><input type="text" name="subject" value="${subject}" size="60"/></td>
    </tr>
    <tr>
      <td>Ebay Check Period:</td>
      <td>
        <select name="checkPeriod">
          <c:forEach var="time" begin="5" end="60" step="5">
            <option value="${time}" <c:if test="${time == checkPeriod}">selected</c:if>>${time} minutes</option>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
      <td>Ebay Default RPT Factor:</td>
      <td>
        <input type="text" name="rptFactor" value="${rptFactor}" size="4"/>% <i>(additional percent to be added to the online price, e.g., 25)</i>
      </td>
    </tr>
    <tr>
      <td>EIMP Fan Addition Factor:</td>
      <td>
        <input type="text" name="fanAddition" value="${fanAddition}" size="4"/>% <i>(additional percent to be added when ticket is from <img src="../images/ico-eimarketplace.gif" align="absbottom"/> FAN, e.g., 15)</i>
      </td>
    </tr>
    <tr>
      <td>Zone Scheme:</td>
      <td>
        <select name="categoryScheme">
          <c:forEach var="scheme" items="${categorySchemes}">
            <option value="${scheme}" <c:if test="${scheme == categoryScheme}">selected</c:if>>${scheme}</option>
          </c:forEach> 
        </select> <i>(Zone Scheme to consider for the ticket groups)</i>
      </td>
    </tr>
    <tr>
      <td>Net Profit Filter:</td>
      <td>$USD <input type="text" name="netProfitFilter" size="4" value="${netProfitFilter}"/> <i>(Does not consider ticket groups which are below this value)</i></td>
    </tr>
    <tr>
      <td>Crawler Frequency:</td>
      <td>
        <select name="crawlerFrequency">
          <c:forEach var="frequency" begin="30" end="240" step="30">
            <option value="${frequency}" <c:if test="${crawlerFrequency == frequency}">selected</c:if>>${frequency} mins</option>
          </c:forEach>
        </select> <i>(Frequencies of crawler for events handled by eBays)</i>
      </td>
    </tr>
    <tr>
      <td>Invalid Group Addition:</td>
      <td>
        <input type="text" name="invalidAddition" value="${invalidAddition}" size="4"/>% <i>(additional percent to be added when a ticket group is invalid, e.g., 900)</i>
      </td>
    </tr>
    <tr>
      <td>Ticket ID Time to Live:</td>
      <td>
        <select name="ticketIdTTL">
          <c:forEach var="numDays" begin="1" end="60">
          		<option value="${numDays}" <c:if test="${ticketIdTTL == numDays}">selected</c:if>>${numDays}</option>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>  
      <td>List Easy Note Template:</td>
      <td>
        <textarea name="ebayListEasyNotes" cols="50">${ebayListEasyNotes}</textArea>
        <i>(The map name can be specified using <b>[MAP]</b>)</i>
      </td>
    </tr>
    <tr>  
      <td>Image Directory URL:</td>
      <td>
        <input name="ebayImageDirUrl" value="${ebayImageDirUrl}" size="100"/>
      </td>
    </tr>
  </table>
  
  <input type="submit" class="medButton submitButton" name="submit" value="Update"/>  
  <span class="updatingSpan" style="display:none"><img src="../images/process-running.gif" align="absbottom"/> Updating...</span>
  
  
  <br/><img src="../images/ico-reload.gif" align="absbottom" /> <a href='./AdminEbay?action="refresh"'>Force Refresh</a> (Last refreshed on ${lastRefresh})<br/>
  
  <h2>List Easy</h2>
  <table>
    <tr>
      <td>Login:</td>
      <td>
        <input type="text" id="listEasyLogin" name="listEasyLogin" value="${listEasyLogin}"/>
        <input type="button" class="medButton" value="Check" onclick="checkListEasy()"/>
        <span id="listEasyInfo"></span>
      </td>
    </tr>
    <tr>
      <td>Password:</td>
      <td><input type="password" id="listEasyPassword" name="listEasyPassword" value="${listEasyPassword}"/></td>
    </tr>
  </table>
  <input type="submit" class="medButton submitButton" name="submit" value="Update"/>  
  <span class="updatingSpan" style="display:none"><img src="../images/process-running.gif" align="absbottom"/> Updating...</span>
  
</form>

<script type="text/javascript">
  function checkListEasy() {
    DataDwr.sendCheckListEasy($('#listEasyLogin').val(), $('#listEasyPassword').val(),
    					  function(response) {
    					    if (response) {
    					      $('#listEasyInfo').html('<img src="../images/ico-enable.gif" align="absbottom"/> OK');
    					    } else {
    					     $('#listEasyInfo').html('<img src="../images/ico-error.gif" align="absbottom"/> Login/Password incorrect');
    					    }
    					  }); 
  }


  function forceRefresh() {
	    $('#action').val("refresh");
	    $('#updateForm').submit();
	  }
</script>