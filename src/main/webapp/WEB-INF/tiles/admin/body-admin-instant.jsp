<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Add Instant Events
</div>

<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${info}
  </div>
</div>
</c:if>

<form id="browseInstantEventsForm" action="BrowseInstantEvents" method="GET">
<input id="action" type="hidden" name="action" value="" />
Tour:
<select name="tourId" onchange="location.href='BrowseInstantEvents?tourId=' + $(this).val()">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
</select>

<b>Select:</b>
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<b>Action:</b>
<input type="button" class="medButton"  value="Add to SH" onclick="addSelectedSHEvents()"/>&nbsp;&nbsp;
 <input type="button" class="medButton" value="Add to TN" onclick="addSelectedTNEvents()"/>&nbsp;&nbsp;
 <input type="button" class="medButton" value="Add to EI" onclick="addSelectedEIEvents()"/>&nbsp;&nbsp;
 <input type="button" class="medButton" value="Add to TNow" onclick="addSelectedTnowEvents()"/>&nbsp;&nbsp; 
<br/>
<br/>


<display:table class="list" name="${events}" id="event" requestURI="BrowseInstantEvents?tourId=${tourId}" pagesize="20">
 
  <display:column>
    <input type="checkbox" class="event_checkbox" name="event_${event.id}"/>
  </display:column>
  <c:if test="${tourId == 'ALL'}">
    <display:column title="Tour" sortable="true">${event.artist.name}</display:column>
  </c:if>
  <display:column title="Event" sortable="true">${event.name}</display:column>
  <display:column title="Date" sortable="true">
    <fmt:formatDate pattern="MM/dd/yy" value="${event.localDate}" />
    <c:choose>
      <c:when test="${event.localTime == null}">
        TBD
      </c:when>
      <c:otherwise>
        <fmt:formatDate pattern="HH:mm" value="${event.localTime}" />
      </c:otherwise>
    </c:choose>
  </display:column>
  <display:column title="Venue" sortable="true">
   ${event.venue.building}
  </display:column>
</display:table>   

</form>
<br/>

<a href="DownloadCustomizedInstantTicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Customized Instant Ticket CSV</a><br/><br/>
<a href="DownloadCustomizedMercuryTicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Customized Mercury Ticket CSV</a><br/><br/>
<a href="DownloadCustomizedEITicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download Customized EI Ticket CSV</a>

<script type="text/javascript">
 
  function selectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }

  function unselectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }
  
  function addSelectedSHEvents() {
    if ($('.event_checkbox:checked').length == 0) {
      alert("Please select at least one event");
      return;
    }
    
    var answer = confirm("Are you sure you want to add the selected events to SH instant events?");
    if (answer) {
       $('#action').val('addSH');
       $('#browseInstantEventsForm').submit();
    }
  }
    
    function addSelectedTNEvents() {
    if ($('.event_checkbox:checked').length == 0) {
      alert("Please select at least one event");
      return;
    }
    
    var answer = confirm("Are you sure you want to add the selected events to TN instant events?");
    if (answer) {
       $('#action').val('addTN');
       $('#browseInstantEventsForm').submit();
    }
  }
  
  
  function addSelectedEIEvents() {
    if ($('.event_checkbox:checked').length == 0) {
      alert("Please select at least one event");
      return;
    }
    
    var answer = confirm("Are you sure you want to add the selected events to EI instant events?");
    if (answer) {
       $('#action').val('addEI');
       $('#browseInstantEventsForm').submit();
    }
  }

  function addSelectedTnowEvents() {
	    if($('.event_checkbox:checked').length == 0) {
	      alert("Please select at least one event");
	      return;
	    }
	    
	    var answer = confirm("Are you sure you want to add the selected events to TNow instant events?");
	    if (answer) {
	       $('#action').val('addTnow');
	       $('#browseInstantEventsForm').submit();
	    }
	  }
  
  
</script>