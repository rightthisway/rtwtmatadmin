<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
	 
	function changeAction(){
		var flag =false;
		$('#ticketTypeSelectError').css("display","none");
		$('#exchangeSelectError').css("display","none");
		$('#currencyTypeError').css("display","none");
		$('#serviceFeesError').css("display","none");
		$('#shippingError').css("display","none");
		
		if($('#ticketTypeSelect').val()=='--'){
			$('#ticketTypeSelectError').css("display","block");
			flag=true;
		}
		if($('#exchangeSelect').val()=='--'){
			$('#exchangeSelectError').css("display","block");
			flag=true;
		}
		if($('#currencyType').val()=='--'){
			$('#currencyTypeError').css("display","block");
			flag=true;
		}
		if($('#serviceFees').val()=='' || isNaN($('#serviceFees').val())){
			$('#serviceFeesError').css("display","block");
			flag=true;
		}
		if($('#shipping').val()=='' || isNaN($('#shipping').val())){
			$('#shippingError').css("display","block");
			flag=true;
		}
		if(flag){
			return false;
		}
		
		if($('#id').val()==''){
			$('#action').val('add');
		}else{
			$('#action').val('edit');
		}
	}
	
	function getTicketTypeNew(exchangeName){
		$.ajax({
			url:"GetTicketType",
			data:{exchangeName : exchangeName},
			success:function(result){
				var siteList = result.split(",");
				$('#ticketTypeSelect').children().remove();
				for(var i = 0 ; i < siteList.length ; i++){
					$('#ticketTypeSelect').append('<option value="' + siteList[i] + '">' + siteList[i] + '</option>'); 
				}
			}
		});
	}
	
	function getTicketType(exchangeVar,type){
	
		var str = exchangeVar;
		//alert(type);
		//alert(str);
		$('#ticketTypeSelect').children().remove();
		$('#ticketTypeSelect').append(' <option value="--">--Select--</option>'); 
		var option ='REGULAR';
		var selected ='';
		if(type==option){
			selected='selected';
		}
		var optionStr = '<option value="REGULAR" '+ selected + '>REGULAR</option>'; 
		//alert(optionStr);
		if(str == "admitoneeventinventory"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "ebay"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "eibox"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "eimarketplace"){
			$('#ticketTypeSelect').append(optionStr); 
			
		}else if(str == "eventinventory"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "fansnap"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "getmein"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "razorgator"){
			$('#ticketTypeSelect').append(optionStr); 
			
		}else if(str == "seatwave"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "seatwavefeed"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "stubhub"){
			$('#ticketTypeSelect').append(optionStr); 
			option ='INSTANT';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="INSTANT" ' + selected + '>INSTANT</option>'); 
			option ='EDELIVERY';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="EDELIVERY" ' + selected + '>EDELIVERY</option>'); 
		}else if(str == "stubhubfeed"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "ticketevolution"){
			$('#ticketTypeSelect').append(optionStr);
			option ='ETICKETS';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="ETICKETS" ' + selected + '>ETICKETS</option>'); 
		}else if(str == "ticketevolution"){
			$('#ticketTypeSelect').append(optionStr); 
			option ='ETICKETS';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="ETICKETS" ' + selected + '>ETICKETS</option>');
		}else if(str == "ticketmaster"){
			$('#ticketTypeSelect').append(optionStr); 
		}else if(str == "ticketnetwork"){
			$('#ticketTypeSelect').append(optionStr);
		}else if(str == "ticketnetworkdirect"){
			$('#ticketTypeSelect').append(optionStr);
			option ='MERCURY';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="MERCURY" '+ selected + '>MERCURY</option>');
			option ='MERCURYPOS';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="MERCURYPOS" ' + selected + '>MERCURYPOS</option>');
		}else if(str == "ticketsnow"){
			$('#ticketTypeSelect').append(optionStr);
			option ='INSTANT';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="INSTANT" ' + selected + '>INSTANT</option>'); 
			option ='ETICKETS';
			selected='';
			if(type==option){
				selected='selected';
			}
			$('#ticketTypeSelect').append('<option value="ETICKETS" ' + selected + '>ETICKETS</option>');
		}else if(str == "ticketsolutions"){
			$('#ticketTypeSelect').append(optionStr);
		}else if(str == "viagogo"){
			$('#ticketTypeSelect').append(optionStr);
		}else if(str == "wstickets"){
			$('#ticketTypeSelect').append(optionStr);
		}
	}
	
	$(document).ready(function() {
		//alert($('#exchangeSelect'));
		//alert('${ticketType}');
		getTicketType($('#exchangeSelect').val(),'${ticketType}');
	});
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Default Tour Price
</div>
<br/>
<center>
	<c:choose>
		<c:when test="${not empty info}">
			<div class="info">
				<div class="infoText">
					${info}
				</div>
				
			</div>
		</c:when>
		<c:when test="${not empty error}">
			<div class="error">
				<div class="errorText">
					${error}
				</div>
				
			</div>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
	<form action="" method="post" onsubmit="return changeAction()">
		<table>
			<tr>
				<td>
					Exchange :
				</td>
				<td>
					<input type= "hidden" name="id" id="id" value="${id}"/>
					<input type= "hidden" name="action" id="action" value="${action}"/>
					<select name="exchangeSelect" id="exchangeSelect" onchange="getTicketTypeNew($(this).val())">
						<option value="--">Select</option>
						<c:forEach items="${sites}" var="site">
							<option <c:if test="${exchange==site.id}">selected</c:if> id="${site.id}">${site.id }</option>
						</c:forEach>
					</select>
					<div id="exchangeSelectError" style="display:none;">
						<font color="red">Select Exchange first.</font>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					Ticket Type:
				</td>
				<td>
					<select name="ticketTypeSelect" id="ticketTypeSelect">
						<option value="--">--Select--</option>
					</select>
					<div id="ticketTypeSelectError" style="display:none;">
						<font color="red">Select Ticket Type first.</font>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
					Service Fees Type:
				</td>
				<td>
					<select name="currencyType" id="currencyType">
						<option value="--">--Select--</option>
						<option <c:if test="${currencyType=='0'}">selected</c:if> value="0">$</option>
						<option <c:if test="${currencyType=='1'}">selected</c:if> value="1">%</option>
					</select>
					<div id="currencyTypeError" style="display:none;">
						<font color="red">Select Service Fees Type first.</font>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					Service Fees:
				</td>
				<td>
					<input type= "text" name="serviceFees" id="serviceFees" value="${serviceFees}"/>
					<div id="serviceFeesError" style="display:none;">
						<font color="red">Enter valid Service Fees.</font>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					Shipping:
				</td>
				<td>
					<input type= "text" name="shipping" id="shipping"  value="${shipping}"/>
					<div id="shippingError" style="display:none;">
						<font color="red">Enter valid Shipping.</font>
					</div>
				</td>
			</tr>
			
			<tr>
				<td colspan="2" align ="center"> 
					<input type= "submit" value="Submit" class="medButton" />
				</td>
			</tr>
		</table>
	</form>
	<div>
		<display:table class="list" name="${exchangeTourPrices}" id="exchange" requestURI="DefaultPriceManagement" pagesize="50">
			<display:column sortable="true" >
				<a href="DefaultPriceManagement?edit=${exchange.id}">Edit</a>
			</display:column>
			<display:column title="Exchange"  sortable="true" property="exchange"/>
			<display:column title="TicketType"  sortable="true" property="ticketType"/>
			<display:column title="ServiceFees"  sortable="true">
				${exchange.serviceFees} 
				<c:choose>
					<c:when test="${exchange.currencyType==0}">
					$
					</c:when>
					<c:otherwise>
					%
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="ShippingFees"  sortable="true" property="shipping" />
			
		</display:table>
	</div>
	
</center>