<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; System Stats 
</div>

<c:if test="${not empty param.info}">
    <div class="info">
      <div class="infoText">${param.info}
    </div>
  </div>
</c:if>

<h1>System Stats</h1>
<b>Application started at <fmt:formatDate value="${applicationStartDate}" pattern="yyyy/MM/dd - HH:mm:ss" /></b><br />
<b>Master Node: ${constants.hostName} (${constants.ipAddress})</b><br />
<b>Dispatch Queue Size: </b> ${dispatchQueueSize}
<br />
<div id="container-tabs" style="margin-top: 5px">
  <ul>
    <c:forEach var="stat" items="${stats}">
      <li><a href="#fragment-${stat.name}"><img src="../images/ico-screen.gif" align="absbottom"/> ${stat.name}</a></li>      
    </c:forEach>
  </ul>
  <c:forEach var="stat" items="${stats}">
    <div id="fragment-${stat.name}">
    	<div style="float:right">
    	  <a href="#" onclick="return confirmStopNode('${stat.nodeId}', '${stat.hostname}')"><img src="../images/ico-delete.gif" align="absbottom"/> Stop Node</a>
    	</div>
        <b>Last update:</b> <fmt:formatDate value="${stat.lastUpdate}" pattern="yyyy/MM/dd - HH:mm:ss" /><br/>
		<b>Hostname:</b> ${stat.systemStat.hostname} (${stat.systemStat.ipAddress})<br />
		
		<h2>JVM</h2>
		<table>
			<tr>
				<td><b>Max. Amount of Memory usable by the JVM:</b></td>
				<td><fmt:formatNumber pattern="0.00" value="${stat.systemStat.maxMemory / (1024 * 1024)}" /><i>MB</i></td>
			</tr>
			<tr>
				<td><b>Total Amount of Memory in the JVM:</b></td>
				<td><fmt:formatNumber pattern="0.00" value="${stat.systemStat.totalMemory / (1024 * 1024)}" /><i>MB</i></td>
			</tr>
			<tr>
				<td><b>Free Memory in the JVM:</b></td>
				<td><fmt:formatNumber pattern="0.00" value="${stat.systemStat.freeMemory / (1024 * 1024)}" /><i>MB</i></td>
			</tr>
		</table>
		<br />
		<b>Number of used / total HttpClients in the store</b><br />
		<table class="list">
			<thead>
				<tr>
					<th></th>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<th><img src="../images/ico-${siteId}.gif" title="${siteId}"/></th>					
					</c:forEach>
					<th>default</th>
				</tr>
			</thead>
			<tr>
				<td>Number of used HttpClients in the store</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">${stat.systemStat.allocatedHttpClientCountBySiteId[siteId] - freeHttpClientCountBySiteId[siteId]}</td>
				</c:forEach>
				<td align="right">${stat.systemStat.allocatedHttpClientCountBySiteId["default"] - freeHttpClientCountBySiteId["default"]}</td>
			</tr>
			<tr>
				<td>Number of allocated HttpClients in the store</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">${stat.systemStat.allocatedHttpClientCountBySiteId[siteId]}</td>
				</c:forEach>
				<td align="right">${stat.systemStat.allocatedHttpClientCountBySiteId["default"]}</td>
			</tr>
			<tr>
				<td>Avg. HttpClient Life Time</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${stat.systemStat.averageHttpClientLifeTimeBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${stat.systemStat.averageHttpClientLifeTimeBySiteId[siteId] / (60 * 1000) - 0.50}" />m<fmt:formatNumber pattern="00" value="${(averageHttpClientLifeTimeBySiteId[siteId] / 1000) % 60}" />s
							</c:otherwise>
						</c:choose>
					</td>
				</c:forEach>
				<td align="right">
					<c:choose>
						<c:when test="${stat.systemStat.averageHttpClientLifeTimeBySiteId['default'] == 0}">-</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0" value="${stat.systemStat.averageHttpClientLifeTimeBySiteId['default'] / (60 * 1000) - 0.50}" />m<fmt:formatNumber pattern="00" value="${(averageHttpClientLifeTimeBySiteId['default'] / 1000) % 60}" />s
						</c:otherwise>
					</c:choose>						
				</td>
			</tr>
		</table>

		<h2>Crawler Concurrent Site Connections</h2>
		<table class="list">
			<thead>
				<tr>
					<th></th>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<th><img src="../images/ico-${siteId}.gif" title="${siteId}"/></th>					
					</c:forEach>
				</tr>
			</thead>
			<tbody>
			  <tr>
			    <td>Num. Connections</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td>${stat.siteStatMap[siteId].activeTicketListingCrawl}</td>					
					</c:forEach>
			  </tr>
			  <tr class="odd">
			    <td>Max Connections</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td>${maxActiveConnectionsBySiteId[siteId]}</td>					
				</c:forEach>
			  </tr>
			  <tr>
			    <td>Total Connections</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td>${maxMasterActiveConnectionsBySiteId[siteId]}</td>					
				</c:forEach>
			  </tr>
			  <tr class="odd">
			    <td>Total Max Connections</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td><fmt:formatNumber pattern="0" value="${maxActiveConnectionsBySiteId[siteId] * global.numNodes * 1.5}"/></td>					
				</c:forEach>
			  </tr>
			</tbody>
		</table>

		<h2>Crawler Threads</h2>
		<table class="list">
		  <thead>
		    <th>Thread</th>
		    <c:forEach var="entry" items="${stat.threadLastUpdatedEntrySet}">
		      <th>${entry.key}</th>
		    </c:forEach>
		  </thead>
		  <tbody>
		    <tr>
		      <td>Last update</td>
		    	<c:forEach var="entry" items="${stat.threadLastUpdatedEntrySet}">
		      	  <td><fmt:formatDate value="${entry.value}" pattern="HH:mm:ss" /></td>
		    	</c:forEach>		      
		    </tr>
		  </tbody>
		</table>
		
		<h2>Database Stats</h2>
		<table>
		    <tr>
		      <td><b>Number of Connections:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBConnectionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Busy Connections:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBBusyConnectionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Idle Connections:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBIdleConnectionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Unclosed Orphaned Connections:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBUnclosedOrphanedConnectionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Close Statements:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBCloseStatementCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Flush:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBCloseStatementCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Connect:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBConnectCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Query Executions:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBQueryExecutionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Transactions:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBTransactionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Sucessful Transactions:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBSuccessfulTransactionCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Session Opens:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBSessionOpenCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Session Closes:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBSessionCloseCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Prepare Statements:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBPrepareStatementCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Number of Optimistic Failures:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBOptimisticFailureCount}"/></td>
		    </tr>
		    <tr>
		      <td><b>Query Execution Max Time:</b></td>
		      <td><fmt:formatNumber value="${stat.systemStat.DBOptimisticFailureCount / 1000}"/> seconds</td>
		    </tr>
		</table>
    </div>
  </c:forEach>
</div>  

<script type="text/javascript">
  $('#container-tabs').tabs();
  
  function confirmStopNode(nodeId, hostname) {
    var answer = confirm("Are you sure you want to stop the node " + nodeId + "-" + hostname + "?");
    if (answer) {
      document.location.href='Admin?action=stop&nodeId=' + nodeId;
      return true;
    }
    
    return false;
  }
</script>

<h2>Data</h2>    
<table>    
	<tr>
		<td><b>Number of Users:</b></td>
		<td><fmt:formatNumber value="${userCount}"/></td>
	</tr>
	<tr>
		<td><b>Number of Artists:</b></td>
		<td><fmt:formatNumber value="${artistCount}" /></td>
	</tr>
	<tr>
		<td><b>Number of Tours:</b></td>
		<td><fmt:formatNumber value="${tourCount}" /></td>
	</tr>
	<tr>
		<td><b>Number of Events:</b></td>
		<td><fmt:formatNumber value="${eventCount}" /></td>
	</tr>
	<tr>
		<td><b>Number of Active Tickets:</b></td>
		<td><fmt:formatNumber value="${activeTicketCount}" /></td>
	</tr>
	<tr>
		<td><b>Number of Short Transactions:</b></td>
		<td><fmt:formatNumber value="${shortTransactionCount}" /></td>
	</tr>
	<tr>
		<td><b>Number of Short Broadcasts:</b></td>
		<td><fmt:formatNumber value="${shortBroadcastCount}" /></td>
	</tr>
</table>
<a href="#" onclick="$('#hibernateStats').show(); $(this).hide(); return false" style="margin-top:20px"><br />Show System Settings</a>
<div id="hibernateStats" style="display:none">
<h2>Hibernate</h2>
<table>
    <c:forEach var="property" items="${hibernateProperties}">
      <tr>
        <td><b>${property.key}:</b></td>
        <td>${property.value}</td>
      </tr>
    </c:forEach>
</table>
</div>