<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Edit Instant Event Settings
</div>

<h1>Instant Event Settings</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<form id="InstantEventSettingsForm" action="AdminEditInstantEventSettings" method="GET">
	<input id ="action" type="hidden" name="action" value="" />
<h2>Stubhub(Instant Tickets)</h2>
<table>
<tr><td>
	Time of Expiration(in hours) <br /> 
	<select name="shExpiry" value="${shExpiry}" style="width:60px"><br />
	<c:forEach var="hr" begin="12" end="60" step="12">
      <option value="${hr}" <c:if test="${hr == shExpiry}">selected</c:if>>${hr}hr</option>
    </c:forEach> </select>
</tr>	
<tr><td>	Markup Percent(in percentage)<br /> 
	<input name="shMarkup" value="${shMarkup}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Sales Percent(in percentage)<br /> 
	<input name="shSales" value="${shSales}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Shipping Fee(in USD)<br /> 
	<input name="shShipping" value="${shShipping}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like $</i></td>
</tr>	
<tr><td>	Minimum Threshold(in USD)<br /> 
	<input name="shminTh" value="${shminTh}" style="width:150px"></td>
</tr>	
<tr><td>	Maximum Threshold(in USD)<br /> 
	<input name="shmaxTh" value="${shmaxTh}" style="width:150px"></td></tr>	
<tr><td><input type="button" value="Update SH settings" class="medButton" style="width: 150px" onclick="updateSH()"/></td></tr>
<table>	
	
<h2>TicketNetwork(Mercury Tickets)</h2>
<table>
<tr><td>
	Time of Expiration(in hours) <br /> 
	<select name="tnExpiry" value="${tnExpiry}" style="width:60px"><br />
	<c:forEach var="hr" begin="12" end="60" step="12">
      <option value="${hr}" <c:if test="${hr == tnExpiry}">selected</c:if>>${hr}hr</option>
    </c:forEach> </select>
</tr>	
<tr><td>	Markup Percent(in percentage)<br /> 
	<input name="tnMarkup" value="${tnMarkup}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Sales Percent(in percentage)<br /> 
	<input name="tnSales" value="${tnSales}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Shipping Fee(in USD)<br /> 
	<input name="tnShipping" value="${tnShipping}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like $</i></td>
</tr>	
<tr><td>	Minimum Threshold(in USD)<br /> 
	<input name="tnminTh" value="${tnminTh}" style="width:150px"></td>
</tr>	
<tr><td>	Maximum Threshold(in USD)<br /> 
	<input name="tnmaxTh" value="${tnmaxTh}" style="width:150px"></td></tr>
<tr><td>
<input type="button" value="Update TN Settings" class="medButton" style="width: 150px" onclick="updateTN()"/></td></tr>		
<table>	

<h2>EIMP(Fan Inventory)</h2>
<table>
<tr><td>
	Time of Expiration(in hours) <br /> 
	<select name="eiExpiry" value="${eiExpiry}" style="width:60px"><br />
	<c:forEach var="hr" begin="12" end="60" step="12">
      <option value="${hr}" <c:if test="${hr == eiExpiry}">selected</c:if>>${hr}hr</option>
    </c:forEach> </select>
	<br /></td>
</tr>	
<tr><td>	Markup Percent(in percentage)<br /> 
	<input name="eiMarkup" value="${eiMarkup}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Sales Percent(in percentage)<br /> 
	<input name="eiSales" value="${eiSales}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like %</i></td>
</tr>	
<tr><td>	Shipping Fee(in USD)<br /> 
	<input name="eiShipping" value="${eiShipping}" style="width:150px">&nbsp;<i>Please enter only the numerical value, no special character symbols like $</i></td>
</tr>	
<tr><td>	Minimum Threshold(in USD)<br /> 
	<input name="eiminTh" value="${eiminTh}" style="width:150px"></td>
</tr>	
<tr><td>	Maximum Threshold(in USD)<br /> 
	<input name="eimaxTh" value="${eimaxTh}" style="width:150px"></td></tr>	
	<tr><td>
<input type="button" value="Update EI Settings" class="medButton" style="width: 150px" onclick="updateEI()"/></td></tr>
<table>		
</form>

<script type="text/javascript">
	function updateSH() {
       $('#action').val('updateSHsetting');
       $('#InstantEventSettingsForm').submit();
   }
   
   function updateTN() {
       $('#action').val('updateTNsetting');
       $('#InstantEventSettingsForm').submit();
   }
   
   function updateEI() {
       $('#action').val('updateEIsetting');
       $('#InstantEventSettingsForm').submit();
   }
</script>