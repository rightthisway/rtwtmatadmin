<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Counter
</div>
<h2>Total Crawler's Hit on Exchanges</h2>
<form>
	<div>
		<table align="center">
			<tr>
				<td >
					<b>Start Date : </b>	
				</td>
				<td>
					<input type="hidden" value="${system}" name="system" id="system" >
					<input type="text" name="startDate" id="startDate" value=${startDate}>
				</td>
			</tr>
			<tr>
				<td >
					<b>End Date &nbsp;&nbsp;: </b>
				</td>
				<td>
					<input type="hidden" value="${siteId}" name="siteId" id="siteId" >
					<input type="text" name="endDate" id="endDate" value=${endDate}>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="Submit" class="medButton" > 
				</td>
			</tr>
		</table>
	</div>
</form>	


	<div align="center"">
		<display:table class="list" name="${siteIds}" id="site" requestURI="EditorCrawlerCounter" >
		    <display:column title="System" property="system" />
		    <display:column title="SiteId" style="18px;" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
			    <a href="javascript:void(0)" onclick="getCrawlerSiteCounter('${site.siteId}','${site.system}',$('#startDate').val(),$('#endDate').val())">${site.siteId}</a>
		    </display:column>
		    
		    <display:column title="Total Hit" property="counter" />
		</display:table>
	</div>
	
	<script type="text/javascript">

		$('INPUT[name=startDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		$('INPUT[name=endDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		function getCrawlerSiteCounter(siteId,system,startDate,endDate){
			$('#system').val(system);
			$('#siteId').val(siteId);
			window.location="CrawlerSiteCounter?siteId=" +siteId+ "&system="+ system +"&startDate="+ startDate +"&endDate=" + endDate;
		}
</script>