<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<%@page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript">
var rowIncrement=3;
$(document).ready(function(){	 
	 $("#isAllArtist").click(function() {
		if((document.getElementById("isAllArtist").checked)){		
		$("#artistSelect").each(function(){
				$("#artistSelect option").attr("selected","selected");				
				});
				/*$("#artistSelect option:selected").each(function () {
                //str += $(this).text() + " ";
				////alert($(this).val());
				addRow("dataTable",$(this).val());
              });*/
		deleteAllRow("dataTable");		
		}
	 }) ;
	 
	 $("#isAllEvent").click(function() {
			if((document.getElementById("isAllEvent").checked)){		
			$("#eventSelect").each(function(){
					$("#eventSelect option").attr("selected","selected");				
					});
					/*$("#artistSelect option:selected").each(function () {
	                //str += $(this).text() + " ";
					////alert($(this).val());
					addRow("dataTable",$(this).val());
	              });*/
			deleteAllRow("dataTable");		
			}
	}) ;
	 
	if('${exchangeSelected}' != ''){
	 	$("#exchangeSelect").val('${exchangeSelected}');
	 }
	 $("#exchangeSelect").change(function () {
	 
		 var str = "";
         $("#exchangeSelect option:selected").each(function () {  
				str += $(this).val();		  
         });
		 
		 $('#ticketTypeSelect').children().remove();
         $('#ticketTypeSelect').append(' <option value="--">--Select--</option>'); 
		
         var optionStr = '<option value="REGULAR" <c:if test="${ticketTypeSelected == 'REGULAR'}">selected</c:if>>REGULAR</option>'; 
         if(str == "admitoneeventinventory"){
        	 $('#ticketTypeSelect').append(optionStr);        	 
         }else if(str == "ebay"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eibox"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eimarketplace"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eventinventory"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "fansnap"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "getmein"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "razorgator"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "seatwave"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "seatwavefeed"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "stubhub"){
        	 $('#ticketTypeSelect').append(optionStr); 
        	 $('#ticketTypeSelect').append('<option value="INSTANT" <c:if test="${ticketTypeSelected == 'INSTANT'}">selected</c:if>>INSTANT</option>'); 
        	 $('#ticketTypeSelect').append('<option value="EDELIVERY" <c:if test="${ticketTypeSelected == 'EDELIVERY'}">selected</c:if>>EDELIVERY</option>'); 
         }else if(str == "stubhubfeed"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "ticketevolution"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="ETICKETS" <c:if test="${ticketTypeSelected == 'ETICKETS'}">selected</c:if>>ETICKETS</option>');  
         }else if(str == "ticketmaster"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "ticketnetwork"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "ticketnetworkdirect"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="MERCURY" <c:if test="${ticketTypeSelected == 'MERCURY'}">selected</c:if>>MERCURY</option>');
        	 $('#ticketTypeSelect').append('<option value="MERCURYPOS" <c:if test="${ticketTypeSelected == 'MERCURYPOS'}">selected</c:if>>MERCURYPOS</option>');
         }else if(str == "ticketsnow"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="INSTANT" <c:if test="${ticketTypeSelected == 'INSTANT'}">selected</c:if>>INSTANT</option>'); 
        	 $('#ticketTypeSelect').append('<option value="ETICKETS" <c:if test="${ticketTypeSelected == 'ETICKETS'}">selected</c:if>>ETICKETS</option>');
         }else if(str == "ticketsolutions"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "viagogo"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "wstickets"){
        	 $('#ticketTypeSelect').append(optionStr);
         }
         
         
	 }).change();
		'<c:if test="${viewData == 'true'}">'
			loadData();
		'</c:if>'
		$("#exchangeSelect").click(function () {          
		  $('#isAllExchange').attr('checked', false);		  
         
        });
		
		$("#ticketTypeSelect").click(function () {          
		  $('#isAllTicketType').attr('checked', false);		  
         
        });
		
	
	 $("#isAllParent").click(function() {
		if((document.getElementById("isAllParent").checked)){
		
		$("#parentSelect").each(function(){
				$("#parentSelect option").attr("selected","selected");				
				});	
				var str = "";
				$("#parentSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
				setChildInfo(str);
		}
	 }) ;
	 
	 
	 var isFirstLoad = '${isFirst}'	;
	
	 $("#parentSelect").change(function () {
	
	
          var str = "";
          $("#parentSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
			   if(!(isFirstLoad == 'true')){
			    $('#isAllParent').attr('checked', false);
				setChildInfo(str);	
			}				
        })
        .change();
	
	$("#childSelect").change(function () { 
	 
          var str = "";
          $("#childSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
				if(!(isFirstLoad == 'true')){
				$('#isAllChild').attr('checked', false);
				setGrandChildInfo(str);	
			}				
        })
        .change();
		
	$("#grandChildSelect").change(function () {
	 
	
	 
          var str = "";
          $("#grandChildSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
				//alert(str);
				if(!(isFirstLoad == 'true')){
				$('#isAllGrandChild').attr('checked', false);
				setArtistInfo(str);
				}
        })
        .change();		
	
	$("#artistSelect").change(function () {
          var str = "";
		   $("#artistSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
		  if(!(isFirstLoad == 'true')){
			 $('#isAllArtist').attr('checked', false);
		  }
		  setEventInfo(str);
//		  isFirstLoad = 'false';
 //         deleteAllRow("dataTable");
         /// $("div").text(str);
        }).change();
	// 

	$("#eventSelect").change(function () {
          var str = "";
		  if(!(isFirstLoad == 'true')){
			$('#isAllEvent').attr('checked', false);
		  }
		  isFirstLoad = 'false';
          deleteAllRow("dataTable");
         /// $("div").text(str);
     }).change();

	 $("#isAllChild").click(function() {
		if((document.getElementById("isAllChild").checked)){
		
		$("#childSelect").each(function(){
				$("#childSelect option").attr("selected","selected");				
				});
			var str = "";
          $("#childSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 				
				setGrandChildInfo(str);
		}
	 }) ;
	 
		
	 $("#isAllGrandChild").click(function() {
		$('#processing').show();
		if((document.getElementById("isAllGrandChild").checked)){
		$("#grandChildSelect").each(function(){
				$("#grandChildSelect option").attr("selected","selected");				
				});	
		  var str = "";
          $("#grandChildSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              });   
				
				setArtistInfo(str);
		}
		
	 }) ;	 
		
	$("#getData").click(function() {
		var selectedArtists="";
		var selectedEvents="";
		var selectedParent = "";
		var selectedChild = "";
		var selectedGrandChild = "";
		var selectedExchange="";
		var selectedTicketType="";
		rowIncrement=3;
		
		//alert("Clicked");
		$("#parentSelect option:selected").each(function () {
        	selectedParent += $(this).val() + ";";
				////alert($(this).val());
				
		});
		if(selectedParent == ""){
			alert("Please select parent category");
			return false;
		}
		$("#childSelect option:selected").each(function () {
			selectedChild += $(this).val() + ";";
////alert($(this).val());

		});
		if(selectedChild == ""){
			alert("Please select child category");
			return false;
		}	
		$("#grandChildSelect option:selected").each(function () {
			selectedGrandChild += $(this).val() + ";";
				////alert($(this).val());
				
		});
		if(selectedGrandChild == ""){
			alert("Please select grand child category");
			return false;
		}				
		$("#artistSelect option:selected").each(function () {
            selectedArtists += $(this).val() + ";";
			//alert($(this).val());
		});
		if(selectedArtists == ""){
			alert("Please select artist");
			return false;
		}
		$("#eventSelect option:selected").each(function () {
			selectedEvents += $(this).val() + ";";
			//alert($(this).val());
				
        });
		if(selectedEvents == ""){
			alert("Please select artist");
			return false;
		}
		if($('#exchangeSelect').val()!='--'){
			selectedExchange += $('#exchangeSelect').val() + ";";
			//alert($(this).val());
        }else{
			alert("Please select exchange.");
			return false;
		}
		if($("#ticketTypeSelect").val()!='--'){
			selectedTicketType  += $('#ticketTypeSelect').val() + ";";
			//alert($(this).val());
        }else{
			alert("Please select TicketType.");
			return false;
		}
			
	  	deleteAllRow("dataTable");
	 	addRow("dataTable",selectedEvents,selectedExchange,selectedTicketType);
	 }) ;
	 /*	
	 $("#isAllExchange").click(function() {
		if((document.getElementById("isAllExchange").checked)){
		
		$("#exchangeSelect").each(function(){
				$("#exchangeSelect option").attr("selected","selected");				
				});			
		}
	 });
	 
	 $("#isAllTicketType").click(function() {
		if((document.getElementById("isAllTicketType").checked)){
		
		$("#ticketTypeSelect").each(function(){
				$("#ticketTypeSelect option").attr("selected","selected");				
				});			
		}
	 });
	 
	 	if('${isAllExchangeSelected}' == 'on'){
		 $("#exchangeSelect").each(function(){
					$("#exchangeSelect option").attr("selected","selected");				
					});	
		 }
		 if('${isAllTicketTypeSelected}' == 'on'){
		 $("#ticketTypeSelect").each(function(){
					$("#ticketTypeSelect option").attr("selected","selected");				
					});
		}
		
		*/
});
function  setChildInfo(parentid){
////alert("hello");
	   //$('.error').hide();
	   deleteAllRow('dataTable');
	   $('#childSelect').children().remove();
	   //$('#child').append("<option value=''>-Select-</option>");
	   $('#grandChildSelect').children().remove();
	   $('#artistSelect').children().remove();
	   $('#isAllChild').attr('checked', false);	   
	   $('#isAllGrandChild').attr('checked', false);	   
	   $('#isAllArtist').attr('checked', false);
	   
	   //$('#grandChild').append("<option value=''>-Select-</option>");
	    
	   var parent = parentid;	
		////alert(parent);
		var myurl = "GetCategory?parent="+parent;	
		 if(parent!='' && parent!='-Select-'){
	 	 $.ajax({
			url:myurl,
			success: function(res){
			//	//alert(res);
			//var jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#childSelect').append(rowText);
              }				
				
			}
 
		}); 
		}
		 
		 
} 
function  setGrandChildInfo(childID){
	$('.error').hide();
	deleteAllRow('dataTable');
	$('#grandChildSelect').children().remove();	
	$('#artistSelect').children().remove();
	$('#isAllGrandChild').attr('checked', false);
	$('#isAllArtist').attr('checked', false);
	 
	var child = childID;
	 
	 var url = "GetCategory?child="+child;	
	 if(child!=''){
	 $.ajax({
		url:url,
		success: function(res){
			 var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#grandChildSelect').append(rowText);
              }	
			 //$('#grandChildSelect').append(res);
			 
			}
		
		
	}); 
	 }
	 
 } 
 function  setArtistInfo(grandChildID){
	$('.error').hide(); 
	deleteAllRow('dataTable');
	$('#artistSelect').children().remove();	
	$('#isAllArtist').attr('checked', false);	
	var grandChild = grandChildID;
	 if(grandChild!=''){
	 var url = "GetCategory?grandChild="+grandChild;		   
	 $.ajax({
		url:url,
		success: function(res){
				$('#processing').show();
			  var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#artistSelect').append(rowText);
              }
			 //$('#artistSelect').children().remove().end().append(res);
			 $('#processing').hide();
			}
		
		
	}); 
	 }
	 
 } 

  function  setEventInfo(artistId){
	$('.error').hide(); 
	deleteAllRow('dataTable');
	$('#eventSelect').children().remove();	
	$('#isAllEvent').attr('checked', false);	
	// var grandChild = grandChildID;
	 if(artistId!=''){
	 var url = "GetCategory?artistId="+artistId;		   
	 $.ajax({
		url:url,
		success: function(res){
				$('#processing').show();
			  var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#eventSelect').append(rowText);
              }
			 //$('#artistSelect').children().remove().end().append(res);
			 $('#processing').hide();
			}
		
		
	}); 
	 }
	 
 } 

 function addRow(tableID,eventId,selectedExchange,selectedticketType) {			
			var artistName="";
			var eventName="";
			var table = document.getElementById(tableID);
			$.post("GetPurchasePriceByEvent", { eventId: eventId ,exchange: selectedExchange ,tickettype:selectedticketType},
			function(data) {		 
				var jsonArrayData = JSON.parse(data);	
				//alert(jsonArrayData);
				var count=0;
				for (var j = 0; j < jsonArrayData.length; j++) {
					var jsonData = jsonArrayData[j];
					for (var i = 0; i < jsonData.length; i++) {
					
						var singleData = jsonData[i];
						var eventId = singleData.id;
						var eventName = singleData.eventName;
						var artistName = singleData.artistName;
						////alert(artistID+"-"+artistName);
						var rowCount = table.rows.length;
						var row = table.insertRow(rowCount);
						row.id = eventId+"_row_"+singleData.exchange;
						
						row.class="rowClass";
						var cellSelect = row.insertCell(0);
						var elementSelect = document.createElement("input");
						elementSelect.name=eventId+"_select_"+singleData.exchange+"_"+singleData.tickettype;
						elementSelect.setAttribute("id", eventId+"_select_"+singleData.exchange+"_"+singleData.tickettype);	
						elementSelect.setAttribute("class", "myClass");
						elementSelect.type = "checkbox";			
						cellSelect.appendChild(elementSelect);
						var input = document.createElement("input");
						input.setAttribute("type", "hidden");
						input.setAttribute("name", eventId+"_"+singleData.exchange+"_"+singleData.tickettype);
						input.setAttribute("value", singleData.rowid);
						cellSelect.appendChild(input);
						
						var cellEventId = row.insertCell(1);
						cellEventId.innerHTML = eventId;
						
						var cellEventName = row.insertCell(2);
						cellEventName.innerHTML = eventName;
						
						var cellEventDate = row.insertCell(3);
						cellEventDate.innerHtml = singleData.eventDate;

						var cellEventTime = row.insertCell(4);
						cellEventDate.innerHtml = singleData.eventTime;

						var cellVenue = row.insertCell(5);
						cellVenue.innerHtml = singleData.venue;


						var cellArtist = row.insertCell(6);
						cellVenue.innerHtml = singleData.artistName;
						
						var items= ${siteList};			

						var elementEventId = document.createElement("input");
						elementEventId.id=eventId+"_exchange_"+singleData.exchange+"_"+singleData.tickettype;
						elementEventId.name=eventId+"_exchange_"+singleData.exchange+"_"+singleData.tickettype;
						
						/*
						elementEventId.type = "text";
						elementEventId.value=singleData.exchange;
						elementEventId.setAttribute("readonly","true");
						cellartistName.appendChild(elem2);
						var cell4 = row.insertCell(4);
						var tickettypes= ${ticketTypeList};	
						///alert('${ticketTypeList}');
						var elem3 = document.createElement("input");
						//alert(singleData.exchange);
						elem3.id=eventId+"_tickettype_"+singleData.exchange+"_"+singleData.tickettype;
						elem3.name=eventId+"_tickettype_"+singleData.exchange+"_"+singleData.tickettype;
						elem3.setAttribute("readonly","true");
						
						elem3.type = "text";
						elem3.value=singleData.tickettype;
						cell4.appendChild(elem3);
						var cell5 = row.insertCell(5);
						var element4 = document.createElement("input");

						element4.id=eventId+"_servicefee_"+singleData.exchange+"_"+singleData.tickettype;
						element4.name=eventId+"_servicefee_"+singleData.exchange+"_"+singleData.tickettype;
						
						element4.type = "text";
						element4.value =singleData.servicefee;

						var element42 = document.createElement("select");
						var currencyTypes = {"0":"$","1":"%"}
						element42.id=eventId+"_currencytype_"+singleData.exchange+"_"+singleData.tickettype;
						element42.name=eventId+"_currencytype_"+singleData.exchange+"_"+singleData.tickettype;			

						for ( var key1 in currencyTypes) {			
							var ov1 = document.createElement("option");				
							ov1.value = key1; 
							ov1.appendChild(document.createTextNode(currencyTypes[key1]))
							element42.appendChild(ov1);
						}
						element42.value=singleData.currencytype;
						cell5.appendChild(element4);
						cell5.appendChild(element42);
						var cell6 = row.insertCell(6);
						var element5 = document.createElement("input");
						element5.id=eventId+"_shipping_"+singleData.exchange+"_"+singleData.tickettype;
						element5.name=eventId+"_shipping_"+singleData.exchange+"_"+singleData.tickettype;
						
						element5.type = "text";
						element5.value =singleData.shipping;
						
						cell6.appendChild(element5);
						
						var element6 = document.createElement("input");
						element6.id=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_exist";
						element6.name=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_exist";
						element6.type = "hidden";
						element6.value =singleData.exist;
						cell6.appendChild(element6);
						
						var element7 = document.createElement("input");
						element7.id=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_id";
						element7.name=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_id";
						element7.type = "hidden";
						element7.value =singleData.rowId;
						
						cell6.appendChild(element7);
						
						var cell7 = row.insertCell(7);
						if(i==(jsonData.length-1)){
							
							if(i==(jsonData.length-1)){
								//alert(i+"-"+(jsonData.length-1))
								var row1 = table.insertRow(rowCount+1);
								row1.id = j+"_"+i+"_row";
								row1.insertCell(0);
								row1.insertCell(1);
								row1.insertCell(2);
								row1.insertCell(3);
								row1.insertCell(4);
								row1.insertCell(5);
								row1.insertCell(6);
								row1.insertCell(7);
								$("#"+j+"_"+i+"_row").attr("class", "darken");
								count++;
							}
						
						}*/			
						count++;		
					}
				}
				
				//}
			}); 
            
			//}
			//}
			//}
        }
		 function addNewRow(tableID,artistID,artistname,rowCount,flag) {
		 //alert(tableID+"\t"+artistID+"\t"+artistname+"\t"+rowCount+"\t"+flag);
		 var table = document.getElementById(tableID);
			if(!($("#"+artistID+"_exchange").val()=='0')){
			var row;
			if(flag=='1'){
			row = table.insertRow(rowCount+rowIncrement);
            
			}else{
			 row = table.insertRow(rowCount); 
			}
			rowIncrement++;
			//alert(artistname);
			row.id = artistID+"_row";
			
			
			row.class="rowClass";
            var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
			
			element1.name=artistID+"_select";
			//element.id=artistID+"_select";
			element1.setAttribute("id", artistID+"_select");
			
			
			element1.setAttribute("class", "myClass");
            element1.type = "checkbox";			
            cell1.appendChild(element1);
			
            var cell2 = row.insertCell(1);
			////alert(artistID);
            cell2.innerHTML = artistID;
			var cell21 = row.insertCell(2);
            //cell21.innerHTML = "'"+artistName;
			//alert(artistName);
			var cell3 = row.insertCell(3);
            //var element2 = document.createElement("option");
            //element2.type = "select-one";
			//var items = {"3":"Three","1":"One","2":"Two"};
			var items= ${siteList};			
			var elem2 = document.createElement("select");
			
			elem2.name=artistID+"_exchange";
			elem2.id=artistID+"_exchange";
					
			var ov = document.createElement("option");
				ov.value = "0"; 
				ov.appendChild(document.createTextNode("--Select--"))
				elem2.appendChild(ov);
			//elem2.setAttribute("name", ""+artistID+"_exchange");
			for ( var key in items) {
				var ov = document.createElement("option");
				ov.value = key; 
				ov.appendChild(document.createTextNode(items[key]))
				elem2.appendChild(ov);
			}
			elem2.setAttribute("onchange", "javascript:validateDataForNew(this,'"+artistID+"')");			
            cell3.appendChild(elem2);
			var cell4 = row.insertCell(4);
			var tickettypes= ${ticketTypeList};	
			////alert(tickettypes);
			var elem3 = document.createElement("select");
			//alert(singleData.exchange);
			
			elem3.id=artistID+"_tickettype";
			elem3.name=artistID+"_tickettype";
			elem3.setAttribute("onchange", "javascript:validateDataForNew(this,'"+artistID+"')");		
			
			//elem3.setAttribute("name", ""+artistID+"_tickettype");
			
			var ov1 = document.createElement("option");
				ov1.value = "0"; 
				ov1.appendChild(document.createTextNode("--Select--"))
				elem3.appendChild(ov1);
			for ( var key1 in tickettypes) {			
				
				var ov1 = document.createElement("option");				
				ov1.value = key1; 
				ov1.appendChild(document.createTextNode(tickettypes[key1]))
				elem3.appendChild(ov1);
			}
			
            cell4.appendChild(elem3);
            var cell5 = row.insertCell(5);
            var element4 = document.createElement("input");			
			element4.id=artistID+"_servicefee";
			element4.name=artistID+"_servicefee";
			
			
            element4.type = "text";
			
			var element42 = document.createElement("select");
			var currencyTypes = {"0":"$","1":"%"}
			
			element42.id=artistID+"_currencytype";
			element42.name=artistID+"_currencytype";
			
			for ( var key1 in currencyTypes) {			
				
				var ov1 = document.createElement("option");				
				ov1.value = key1; 
				ov1.appendChild(document.createTextNode(currencyTypes[key1]))
				element42.appendChild(ov1);
			}
			
			
            cell5.appendChild(element4);
			cell5.appendChild(element42);
			var cell6 = row.insertCell(6);
            var element5 = document.createElement("input");
			
			element5.name=artistID+"_shipping"
			element5.id= artistID+"_shipping";
			
			
            element5.type = "text";
			
			
            cell6.appendChild(element5);
			var cell7 = row.insertCell(7);
			//var element6 = document.createElement("input");
 
			//Assign different attributes to the element.
			/*element6.setAttribute("type", "button");
			element6.setAttribute("value", "Save");
			
			element6.setAttribute("name", artistID+"_button");
			element6.setAttribute("id", artistID);
			element6.setAttribute("onclick", "javascript:addNewRow('"+tableID+"','"+artistID+"','"+artistname+"','"+rowCount+"')");
			*/
			
			//cell7.appendChild(element6);
			}else{
			alert("You can't add more than one row at a time");
			}
		 }
        function deleteRow(tableID) {
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
 
            for(var i=0; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            }
            }catch(e) {
                //alert(e);
            }
        }
		function deleteAllRow(tableID) {
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
			////alert(rowCount);
            for(var i=2; i<rowCount; i++) {
                var row = table.rows[i];                
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            
            }catch(e) {
                //alert(e);
            }
        }
		
		function checkData(artistId,exchange){
		if(!(exchange=='')){
		if(document.getElementById(artistId+"_select"+exchange).checked){
		
		$("#"+artistId+"_select"+exchange).attr('checked', false);
		$("#"+artistId+"_row"+exchange).removeAttr("class", "darken");
		$("#"+artistId+"_servicefee"+exchange).attr('readonly', false);
		$("#"+artistId+"_shipping"+exchange).attr('readonly', false);
		}else{
		if($("#"+artistId+"_servicefee"+exchange).val()==''){
		//alert("Please insert service fee");
		return false;
		}
		else if($("#"+artistId+"_shipping"+exchange).val()==''){
		//alert("Please insert shipping fee");
		return false;
		}else{
		$("#"+artistId+"_select"+exchange).attr('checked', true);
		$("#"+artistId+"_row"+exchange).attr("class", "darken");
		$("#"+artistId+"_select"+exchange).attr('readonly', true);
		$("#"+artistId+"_exchange"+exchange).attr('readonly', true);
		$("#"+artistId+"_tickettype"+exchange).attr('readonly', true);
		$("#"+artistId+"_servicefee"+exchange).attr('readonly', true);
		$("#"+artistId+"_shipping"+exchange).attr('readonly', true);
		}
		}
		}else{		
		if(document.getElementById(artistId+"_select").checked){
		
		$("#"+artistId+"_select").attr('checked', false);
		$("#"+artistId+"_row").removeAttr("class", "darken");
		$("#"+artistId+"_servicefee").attr('readonly', false);
		$("#"+artistId+"_shipping").attr('readonly', false);
		}else{
		if($("#"+artistId+"_exchange").val()=="0"){
		alert("Please select exchange");
		return false;
		}else if($("#"+artistId+"_tickettype").val()=="0"){
		alert("Please select tickettype");
		return false;
		}else if($("#"+artistId+"_servicefee").val()==''){
		alert("Please insert service fee");
		return false;
		}
		else if($("#"+artistId+"_shipping").val()==''){
		alert("Please insert shipping fee");
		return false;
		}else{
		$("#"+artistId+"_select").attr('checked', true);
		$("#"+artistId+"_row").attr("class", "darken");
		$("#"+artistId+"_select").attr('readonly', true);
		$("#"+artistId+"_exchange").attr('readonly', true);
		$("#"+artistId+"_tickettype").attr('readonly', true);
		$("#"+artistId+"_servicefee").attr('readonly', true);
		$("#"+artistId+"_shipping").attr('readonly', true);
		}
		}
		}
		}
		function validateDataForNew(select,artistId){
		var str="";
		var exchange_id=artistId+"_exchange";
		var ticket_type_id=artistId+"_tickettype";
		//alert(exchange_id);
		var exchange_type=$('#'+exchange_id+' :selected').val();
		var ticktet_type=$('#'+ticket_type_id+' :selected').val();
		//var cat_txt=$("'#"+artistId+"_tickettype'" :selected').text();
		var check_exchangeid=artistId+"_exchange_"+exchange_type+"_"+ticktet_type;
		var check_typeid=artistId+"_tickettype_"+exchange_type+"_"+ticktet_type;
		if(!($('#'+check_exchangeid+' :selected').text())==''){
		alert("You can't select this exchange and tickettype combination:It is already used for this artist")
		$("#"+ticket_type_id).val("0");
		$("#"+exchange_id).val("0");
		return false;
		}
		
		}
		
		function validateDataForExisting(artistId,exchange,tickettype){
		var str="";
		//alert(exchange);
		var exchange_id=artistId+"_exchange_"+exchange+"_"+tickettype;
		var ticket_type_id=artistId+"_tickettype_"+exchange+"_"+tickettype;
		//alert(exchange_id);
		var exchange_type=$('#'+exchange_id+' :selected').val();
		var ticktet_type=$('#'+ticket_type_id+' :selected').val();
		//var cat_txt=$("'#"+artistId+"_tickettype'" :selected').text();
		var check_exchangeid=artistId+"_exchange_"+exchange_type+"_"+ticktet_type;
		var check_typeid=artistId+"_tickettype_"+exchange_type+"_"+ticktet_type;
		if(!($('#'+check_exchangeid+' :selected').text())==''){
		alert("You can't select this exchange and tickettype combination:It is already used for this artist")
		$("#"+ticket_type_id).val(tickettype);
		$("#"+exchange_id).val(exchange);
		return false;
		}
		
		}
		function selectAll(){
		$(".myClass").each(function(){
			this.checked=true;
		});
		}
		function selectNone(){
			$(".myClass").each(function(){
				this.checked=false;
			});
		}
		
		function copyAllServiceFee(){
		if($("#copyAllServiceFeeCheckbox").attr('checked')){
		selectAll();}
		var isFirst=true;
		var serviceFee;
		var serviceFeeType;		
		var shippingFee;
		var tempServiceFeeId="";
		var tempServiceFeeTypeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllServiceFeeCheckbox").attr('checked')){
				return;
			}
			
			var serviceFeeId = this.id;
			tempServiceFeeId = serviceFeeId.replace("select", "servicefee");
			var serviceFeeTypeId =  this.id+""; 
			tempServiceFeeTypeId = serviceFeeTypeId.replace("select", "currencytype");
			
			if(isFirst){
			
				if(!isFloat($("#"+tempServiceFeeId).val()) && !isInteger($("#"+tempServiceFeeId).val())){
					alert($("#"+tempServiceFeeId).val() + " is not valid.");
					$("#"+tempServiceFeeId).focus();
					return false;
				}				
				isFirst=false;
				serviceFee=$("#"+tempServiceFeeId).val();				
				serviceFeeType=$("#"+tempServiceFeeTypeId).val();	
					
			}
			else{		
				
				$("#"+tempServiceFeeId).val(serviceFee);
				$("#"+tempServiceFeeTypeId).val(serviceFeeType);			

			}
		});
	}
		function copyAllShipping(){
		if($("#copyAllShippingCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var shippingFee;
		var tempShippingId="";		
		$(".myClass").each(function(){
			if(!$("#copyAllShippingCheckbox").attr('checked')){
				return;
			}
			
			var shippingId = this.id;
			tempShippingId = shippingId.replace("select", "shipping");
			
			
			if(isFirst){
			
				if(!isFloat($("#"+tempShippingId).val()) && !isInteger($("#"+tempShippingId).val())){
					alert($("#"+tempShippingId).val() + " is not valid.");
					$("#"+tempShippingId).focus();
					return false;
				}				
				isFirst=false;
				shippingFee=$("#"+tempShippingId).val();								
					
			}
			else{		
				
				$("#"+tempShippingId).val(shippingFee);				

			}
		});
	}
	function copyAllExchange(){
		if($("#copyAllExchangeCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var exchange;
		var tempExchangeId="";
		var tempFirstExchangeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllExchangeCheckbox").attr('checked')){
				return;
			}
			
			var exchangeId = this.id;
			//alert(exchangeId);
			tempFirstExchangeId = exchangeId.replace("select", "exchange");
			tempExchangeId = exchangeId.split("_")[0]+"_exchange";
			
			if(isFirst){		
				if($("#"+tempFirstExchangeId).val()=='0'){
					alert("Please select proper exchange");
					$("#"+tempFirstExchangeId).focus();
					return false;
				}				
				isFirst=false;
				exchange=$("#"+tempFirstExchangeId).val();								
					
			}
			else{		
				
				$("#"+tempExchangeId).val(exchange);				

			}
		});
	}
	
	function copyAllTicketType(){
		if($("#copyAllTicketTypeCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var ticketType;
		var tempTypeId="";
		var tempFirstTypeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllTicketTypeCheckbox").attr('checked')){
				return;
			}
			
			var tickettypeId = this.id;
			//alert(exchangeId);
			tempFirstTypeId = tickettypeId.replace("select", "tickettype");
			tempTypeId = tickettypeId.split("_")[0]+"_tickettype";
			
			if(isFirst){		
				if($("#"+tempFirstTypeId).val()=='0'){
					alert("Please select proper ticket type");
					$("#"+tempFirstTypeId).focus();
					return false;
				}				
				isFirst=false;
				ticketType=$("#"+tempFirstTypeId).val();								
					
			}
			else{		
				
				$("#"+tempTypeId).val(ticketType);				

			}
		});
	}
		
	function isInteger(value){
		var integerExpression=/^\d+$/;
		return integerExpression.test(value);
	}
	
	function isFloat(value){
		var integerExpression=/^\d+.{0,1}\d{0,2}$/;
		return integerExpression.test(value);
	}
	
	function loadData(){
		var selectedArtists="";
		var selectedExchange="";
		var selectedTicketType="";
		rowIncrement=3;
		deleteAllRow("dataTable");		
		$("#artistSelect option:selected").each(function () {
                selectedArtists += $(this).val() + ";";
				////alert($(this).val());
				
              });
		$("#exchangeSelect option:selected").each(function () {
                selectedExchange += $(this).val() + ";";
				////alert($(this).val());
				
              });
		$("#ticketTypeSelect option:selected").each(function () {
                selectedTicketType += $(this).val() + ";";
				///alert($(this).val());
				
              });	
			  alert("1: " + str);
			  
			  addRow("dataTable",selectedArtists,selectedExchange,selectedTicketType);
	}
	function addAllRow(){
	var exchange="";
	var tickettype="";
	var serviceFee="";
	var serviceFeeType="";
	var shippingFee="";
	if(!$("#copyAllRowCheckbox").attr('checked')){
				return;
			}
			var cnt=0;
	$(".myClass").each(function(){
			if(this.checked==true){
			cnt++;		
			}
		});
		if(cnt==0){
		alert("Please select one row");
		$('#copyAllRowCheckbox').attr('checked', false);
		return;
		}
		if(cnt>1){
		alert("Please select only one row");
		$(".myClass").each(function(){
			this.checked=false;
			
		});
		$('#copyAllRowCheckbox').attr('checked', false);
		return;
		}
		var removeId="";
		$(".myClass").each(function(){
			if(this.checked==true){
			var selectId= this.id;
			var str= selectId.split("_")[0];
			removeId=str;
			var exchangeId = selectId.replace("select", "exchange");
			var ticketTypeId = selectId.replace("select", "tickettype");
			var serviceFeeId = selectId.replace("select", "servicefee");			
			var serviceFeeTypeId = selectId.replace("select", "currencytype");	
			var shippingId = selectId.replace("select", "shipping");
			exchange = $('#'+exchangeId).val();
			tickettype = $('#'+ticketTypeId).val();
			serviceFee = $('#'+serviceFeeId).val();
			serviceFeeType = $('#'+serviceFeeTypeId).val();
			shippingFee = $('#'+shippingId).val();
			//alert(exchange+"\t"+tickettype+"\t"+serviceFee+"\t"+serviceFeeType+"\t"+shippingFee);
			return false;
			}
		});
		var table = document.getElementById('dataTable');
		var rowCount = table.rows.length;
		var toutIdsString="";
		$(".myClass").each(function(){		
		var selectId= this.id;
		var temp= selectId.split("_")[0];
		if((toutIdsString.indexOf(temp) >= 0) || (temp == removeId)){
		}else{
		toutIdsString = temp;
		var check_exchangeid = toutIdsString+"_exchange_"+exchange+"_"+tickettype;		
		if(!($('#'+check_exchangeid+' :selected').text())==''){
		$("#"+toutIdsString+"_select_"+exchange+"_"+tickettype).attr('checked','checked');
		$("#"+toutIdsString+"_exchange_"+exchange+"_"+tickettype).val(exchange);
		$("#"+toutIdsString+"_tickettype_"+exchange+"_"+tickettype).val(tickettype);
		$("#"+toutIdsString+"_servicefee_"+exchange+"_"+tickettype).val(serviceFee);
		$("#"+toutIdsString+"_currencytype_"+exchange+"_"+tickettype).val(serviceFeeType);
		$("#"+toutIdsString+"_shipping_"+exchange+"_"+tickettype).val(shippingFee);
		;
		 
		}else{
		//alert(rowCount);
		if($('#'+toutIdsString+"_exchange"+' :selected').text() ==''){
		addNewRow('dataTable',toutIdsString,'',rowCount,'0');
		rowCount++;	
		}
		$("#"+toutIdsString+"_select").attr('checked','checked');
		$("#"+toutIdsString+"_exchange").val(exchange);
		$("#"+toutIdsString+"_tickettype").val(tickettype);
		$("#"+toutIdsString+"_servicefee").val(serviceFee);
		$("#"+toutIdsString+"_currencytype").val(serviceFeeType);
		$("#"+toutIdsString+"_shipping").val(shippingFee);
				
		}
		}
		});
		var answer = confirm("Do you want to save this?")
	if (answer){
		$('#saveButton').trigger('click');
	}
	else{
		$('#getData').trigger('click');		
	}
		
	}
</script>
<style>
table#dataTable {
    
	border-color: #600;
    border-width: 1px 1px 1px 1px;
    border-style: solid;	
 }
 table#dataTable td {
    
   
	border-color: #600;
    border-width: 1px 1px 0 0;
    border-style: solid;
    margin: 0;
    padding: 4px;
   
 } 
  table#dataTable tr {
    
   
	border-color: #600;
    border-width: 1px 1px 1px 1px;
    border-style: solid;
 }
 .darken { 
border: 2px solid; 
     background-color: #FFC;
 }
 .tabelRow{
	display: table-header-group;
	vertical-align: middle;
	border-color: inherit;
}
.tableCell{
	background-color: #850001;
	background-repeat: no-repeat;
	border-left: 2px solid #C10F0F;
	border-right: 2px solid #4D0101;
	padding-top: 4px;
	padding-bottom: 4px;
	color:white;
}
 .rowClass {
    border: 1px solid;
 }
 .leftcol {
    font-weight: bold;
    text-align: left;
    width: 150px;
    background-color: #CCCCCC;
 }
</style>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> &gt;
Price Management
</div>

<c:if test="${not empty info}">
	<div class="info"><div class="infoText">${info}</div></div>
</c:if>

<c:if test="${not empty error}">
	<div class="error">
		<div class="errorText">
			${error}
		</div>
	</div>
</c:if>


<h1>Price Management</h1>
<body>
<form id="priceManagementForm" action="EditorPriceManagement" method="Post">
<table>
<tr>
<td align="center"><b>Parent Category :</b> Check  All: <input type="checkbox" name="isAllParent" <c:if test="${isAllParentSelected == 'on'}">checked</c:if> id="isAllParent"/>
</td>
<td align="center"><b>Child Category :</b> Check  All: <input type="checkbox" name="isAllChild" <c:if test="${isAllChildSelected == 'on'}">checked</c:if> id="isAllChild" />
</td>
<td align="center"><b>Grand Child Category :</b> Check  All: <input type="checkbox" name="isAllGrandChild" <c:if test="${isAllGrandChildSelected == 'on'}">checked</c:if> id="isAllGrandChild"/>
</td>
</tr>
<tr><td>
<select name="parnetName" id="parentSelect" style="width: 300px" multiple size="5" >
 <c:forEach var="parent" items="${parentList}">
 <c:set var="a1" value="false" />
 <c:forEach var="selected" items="${parentSelected}">
  <c:if test="${parent.id == selected}">
  <c:set var="a1" value="true" />
  </c:if>
  </c:forEach>
    <option value="${parent.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${parent.name}</option>
  </c:forEach>
</select> 
</td>
<td> 
<select name="childName" id="childSelect" style="width: 300px" multiple size="5">
<c:forEach var="child" items="${childList}">
<c:set var="a2" value="false" />  
  <c:forEach var="selected1" items="${childSelected}">
  <c:if test="${child.id == selected1}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
    <option value="${child.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${child.name}</option>
  </c:forEach>

</select>  
</td>
<td> 
<select name="grandchildName" id="grandChildSelect" style="width: 300px" multiple size="5">
<c:forEach var="grandChild" items="${grandChildList}">
<c:set var="a3" value="false" />  
  <c:forEach var="selected2" items="${grandChildSelected}">
  <c:if test="${grandChild.id == selected2}">
  <c:set var="a3" value="true" />
  </c:if>
  </c:forEach>
    <option value="${grandChild.id}" <c:if test="${a3 == 'true'}">selected</c:if>>${grandChild.name}</option>
  </c:forEach>
  
</select>  
</td>
</tr>
<tr><td colspan="3" align="center">
</td></tr>
<tr><td colspan="3" align="center">
<table width="100%">
<tr>
	<td align="center"><b>Artist :</b>  Check  All: <input type="checkbox" name="isAllArtist" <c:if test="${isAllArtistSelected == 'on'}">checked</c:if> id="isAllArtist" /></td>
	<td align="center"><b>Event :</b>  Check  All: <input type="checkbox" name="isAllEvent" <c:if test="${isAllEventSelected == 'on'}">checked</c:if> id="isAllEvent" /></td>
</tr>
<tr>
<td align="center">
<select name="artistName" id="artistSelect" style="width: 400px" multiple size="10" >
	<c:forEach var="artist" items="${artistList}">
		<c:set var="a4" value="false" />  
		<c:forEach var="selected3" items="${artistSelected}">
			<c:if test="${artist.id == selected3}">
				<c:set var="a4" value="true" />
			</c:if>
		</c:forEach>
		<option value="${artist.id}" <c:if test="${a4 == 'true'}">selected</c:if>>${artist.name}</option>
	</c:forEach>
 
</select> 

</td>
<td align="center">
<select name="eventSelect" id="eventSelect" style="width: 400px" multiple size="10" >
<c:forEach var="event" items="${eventList}">
  <c:set var="a5" value="false" />  
  <c:forEach var="selected3" items="${eventSelected}">
	  <c:if test="${event.id == selected3}">
		<c:set var="a5" value="true" />
	  </c:if>
  </c:forEach>
	<option value="${event.id}" <c:if test="${a5 == 'true'}">selected</c:if>>${event.name}</option>
  </c:forEach>
  
</select> 

</td>
</tr>
<tr>
	<td><b>Exchange :</b>  <!--Check  All: <input type="checkbox" name="isAllExchange" <c:if test="${isAllExchangeSelected == 'on'}">checked</c:if> id="isAllExchange" />--></td>
	<td><b>Ticket Type :</b>  <!--Check  All: <input type="checkbox" name="isAllTicketType" <c:if test="${isAllTicketTypeSelected == 'on'}">checked</c:if> id="isAllTicketType" />--></td>
</tr>
<tr>
<td>

			<select name="exchangeName" id="exchangeSelect" style="width: 200px">
			<option value="--">--Select--</option>
			<c:forEach var="exchange" items="${exchangeList}">
			  <option value="${exchange}" <c:if test="${exchangeSelected == exchange}">selected</c:if>>${exchange}</option>
			</c:forEach>	
			</select> 
		</td>
		<td>
		
			<select name="ticketTypeName" id="ticketTypeSelect" style="width: 200px">
			<option value="--">--Select--</option>
		<!-- 	<c:forEach var="tickettype" items="${ticketTypes}">
			  <option value="${tickettype}" <c:if test="${ticketTypeSelected == tickettype}">selected</c:if>>${tickettype}</option>
			</c:forEach>
		 -->
			</select>  
		</td> 
</tr>
</table>
</td>
</tr>
<tr><td colspan="3" align="center"><input id="artistSelectorAction" type="hidden" name="action" value="${action}" /><input type="button" class="medButton" value="Get Data" id="getData" /> &nbsp;<br/></td></tr>

<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3"></td></tr>
<tr><td colspan="3" align='center'><table><tr><td colspan="8" align="right"><input type="button" class="medButton" value="Save Selected" id="saveButton" onclick="$('#artistSelectorAction').val('save'); $('#priceManagementForm').submit()"/><input type="button" class="medButton" value="Delete Selected" onclick="$('#artistSelectorAction').val('delete'); $('#priceManagementForm').submit()"/></td></tr></table></td></tr>

 <tr><td colspan="3" align='center'>   
 
    <TABLE id="dataTable" width="100%"  align="center">
<tr><td colspan='3'>Select : <a href="javascript:selectAll();">All</a>&nbsp;|
	<a href="javascript:selectNone()">None</a>&nbsp;|&nbsp;<a href="javascript:loadData()">Undo Copy All</a></td><td></td><td></td><td><input onClick="javascript:copyAllServiceFee()" type="checkbox" id="copyAllServiceFeeCheckbox" > Copy All</td><td><input onClick="javascript:copyAllShipping()" type="checkbox" id="copyAllShippingCheckbox" > Copy All</td></tr>
     <tr class="tableRow"><td class="tableCell"><b>Select</b></td><td class="tableCell"><b>Event Id</b></td><td class="tableCell"><b>Event Name</b></td> <td class="tableCell"><b>Event Date</b></td><td class="tableCell"><b>Event Time</b></td><td class="tableCell"><b>Event Venue</b></td><td class="tableCell"><b>Artist Name</b></td><td class="tableCell"><b>Exchange</b></td><td class="tableCell"><b>Ticket Type</b></td><td class="tableCell"><b>Service Fees</b></td><td class="tableCell"><b>Shipping</b></td><td class="tableCell"></td></tr>  
    </TABLE></td></tr>

</table>
<span id="processing" style="position:fixed;bottom:280px;right:600px;cursor:pointer"><img src="../images/process-running.gif" align="absbottom" />Loading...</span>
<script type="text/javascript">
$('#processing').hide();

</script>
</form>

</body>
