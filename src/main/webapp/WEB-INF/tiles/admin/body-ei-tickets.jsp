<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; EITickets
</div>

<c:if test="${not empty error}">
  <div class="error">
    <div class="errorText">
      ${error}
    </div>
  </div>
</c:if>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<h1> EI Tickets </h1>		
<form action="EITickets" method="POST">
  List of Event IDs of events to be included for EI tickets (separated by a comma):<br/>
  <textarea name="eventIds" rows="4" cols="120">${eventIds}</textarea><br/>
  <input type="submit" class="medButton" name="action" value="update"/>
</form>


<div class="info">
	<div class=infoText">
		The CSV file is in the following format: <br/>
		Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId
	</div>	
</div>
<br/>
Please click on the link to download the EIMarketPlace EITickets file in csv format:<br/>
<a href="DownloadEITicketCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download EI Ticket CSV</a>
		