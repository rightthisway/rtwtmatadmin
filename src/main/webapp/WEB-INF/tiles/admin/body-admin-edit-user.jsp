<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> &gt;
  <a href="AdminManageUsers">Manage Users</a> 
  &gt; Edit User
</div>

<c:if test="${not empty error}">
  <div class="info"><div class="infoText">${error}</div></div>
</c:if>


<h1>Edit user</h1>

<h2>Edit User Information</h2>

<c:if test="${not empty userInfoMessage}">
  <div class="info"><div class="infoText">${userInfoMessage}</div></div>
</c:if>

<c:if test="${not empty userInfoError}">
  <div class="info"><div class="infoText">${userInfoError}</div></div>
</c:if>

<c:if test="${not empty userRolesMessage}">
  <div class="info"><div class="infoText">${userRolesMessage}</div></div>
</c:if>


<b>Username: ${user.username}</b>

<form action="AdminEditUser" method="POST">
<input type="hidden" name="action" value="updateInfo" />
<input type="hidden" name="username" value="${user.username}" />
<table>
	<tr>
		<td>E-Mail</td>
		<td><input type="text" name="email" value="<c:out value='${user.email}' />" style="width: 220px;" /></td>
	</tr>
	<tr>
		<td>First Name</td>
		<td><input type="text" name="firstName" value="<c:out value='${user.firstName}' />" /></td>
	</tr>
	<tr>
		<td>Last Name</td>
		<td><input type="text" name="lastName" value="<c:out value='${user.lastName}' />" /></td>
	</tr>
	<tr>
		<td>Phone</td>
		<td><input type="text" name="phone" value="<c:out value='${user.phone}' />" /></td>
	</tr>
	<tr>
		<td>
			Role
		</td>
		<td>
		<c:set var="userRole" value="" />
		<c:forEach var="r" items="${user.roles}">
			<c:forEach var="role" items="${roles}"> 
				<c:if test="${r.id == role.id}">
					<c:set var="userRole" value="${role.name}" />
				</c:if>
			</c:forEach>
		</c:forEach>
	
			<input type="radio" name="rolesStrList" value="ROLE_SUPER_ADMIN" <c:if test="${'ROLE_SUPER_ADMIN'==userRole}">checked</c:if> />Super Admin
			<input type="radio" name="rolesStrList" value="ROLE_EDITOR" <c:if test="${'ROLE_EDITOR'==userRole}">checked</c:if> />Editor
			<input type="radio" name="rolesStrList" value="ROLE_USER"  <c:if test="${'ROLE_USER'==userRole}">checked</c:if> />User
			<input type="radio" name="rolesStrList" value="ROLE_OFFSHORE"  <c:if test="${'ROLE_OFFSHORE'==userRole}">checked=true</c:if> />Offshore Admin
			<input type="radio" name="rolesStrList" value="ROLE_BROKER_USER"  <c:if test="${'ROLE_BROKER_USER'==userRole}">checked=true</c:if> />Broker User
			<input type="radio" name="rolesStrList" value="ROLE_LITE_ADMIN"  <c:if test="${'ROLE_LITE_ADMIN'==userRole}">checked=true</c:if> />Lite Admin
			<input type="radio" name="rolesStrList" value="ROLE_LITE_BROWSE"  <c:if test="${'ROLE_LITE_BROWSE'==userRole}">checked=true</c:if> />Lite Browse
			<input type="radio" name="rolesStrList" value="ROLE_LITE_USER"  <c:if test="${'ROLE_LITE_USER'==userRole}">checked=true</c:if> />Lite User
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="Update" class="medButton" />
		</td>
	</tr>
</table>
</form>

<h2>Change password</h2>

<c:if test="${not empty userPasswordMessage}">
  <div class="info"><div class="infoText">${userPasswordMessage}</div></div>
</c:if>

<c:if test="${not empty userPasswordError}">
  <div class="error"><div class="errorText">${userPasswordError}</div></div>
</c:if>

<form action="AdminEditUser" method="POST">
	<input type="hidden" name="action" value="updatePassword" />
	<input type="hidden" name="username" value="${user.username}" />
	<table>
		<tr>
			<td>New Password</td>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td>Confirm Password</td>
			<td><input type="password" name="confirmPassword" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Update" class="medButton" />
			</td>
		</tr>
	</table>
</form>


<h2>Misc</h2>

<c:if test="${not empty userMiscMessage}">
  <div class="info"><div class="infoText">${userMiscMessage}</div></div>
</c:if>

<form action="AdminEditUser" method="POST">
	<input type="hidden" name="action" value="updateMisc" />
	<input type="hidden" name="username" value="${user.username}" />
	
	<input type="checkbox" id="emailCrawlErrorCreatorEnabled" name="emailCrawlErrorCreatorEnabled" <c:if test="${user.emailCrawlErrorCreatorEnabled}">checked</c:if> /><label for="emailCrawlErrorCreatorEnabled">Receive emails for errors on crawl I created</label><br />
	<input type="checkbox" id="emailCrawlErrorLastUpdaterEnabled" name="emailCrawlErrorLastUpdaterEnabled" <c:if test="${user.emailCrawlErrorLastUpdaterEnabled}">checked</c:if> /><label for="emailCrawlErrorLastUpdaterEnabled">Receive emails for errors on crawl I last updated</label><br />
	<input type="checkbox" name="locked" id="locked" <c:if test="${user.locked}">checked</c:if> /><label for="locked">Locked (Disabled)</label>
	
	<br /><br />
	
	<input type="submit" name="submit" value="Update" class="medButton" />
</form>

<h2>Actions</h2>
<c:if test="${not empty userActionsMessage}">
  <div class="info"><div class="infoText">${userActionsMessage}</div></div>
</c:if>
<form action="AdminEditUser" method="POST">
	<input type="hidden" name="action" value="logout" />
	<input type="hidden" name="username" value="${user.username}" />
	
	<br /><br />
	
	<input type="submit" name="submit" value="Logout User" class="medButton" />
</form>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("INPUT[@name='email']").focus();
	
});
function showbrokers()
{
	if($("#radio5").attr('checked'))
{
/*
$("#brokerList").css('display','');*/
}
	
	if($("#radio6").attr('checked'))
	{

	$("#brokerList").css('display','');
	}
	
}
window.onload = showbrokers;
	
	var isBrowseLiteSelected = false;
	/*
	function processBroker(brokerValue){
		if(brokerValue == "role_tmat_lite_user"){
			$('#brokerList').show();
			$('#liteUserMessage').hide();
			$('#tmatLiteUserMessage').show();
			isBrowseLiteSelected=true;
		}else if(brokerValue != "role_user"){
			$('#brokerList').hide();
			$('#liteUserMessage').hide();
			isBrowseLiteSelected=false;
		}else{
			$('#brokerList').show();
			$('#liteUserMessage').show();
			isBrowseLiteSelected=false;
		}
	}
	*/
	function updateRoles(){
		if(isBrowseLiteSelected){
			var liteRole = $('input[name=liteRolesStrList]:checked').val();
			if(liteRole == "" || liteRole == undefined){
				alert("Please select a TMAT Lite role.");
				return false;
			}
			
			var brokerName = $('#brokername').val();
			if(brokerName == "" || brokerName == null){
				alert("Please select a broker.");
				return false;
			}
		}
		return true;
	}
</script>