<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Edit EI Instant Events
</div>

<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${info}
  </div>
</div>
</c:if>

<form id="eventSelector" method="GET" action="EditEIInstantEvents" onsubmit="$('#eventSubmit').hide(); $('#loadingSpan').show()">

Tour: 
<select name="tourId" id="tourIdSelect">
<option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
</select>

<c:choose>
  <c:when test="${tourId == 'ALL'}">
    <c:set var="maxTitleChars" value="10"/>
  </c:when>
  <c:otherwise>
    <c:set var="maxTitleChars" value="22"/>
  </c:otherwise>
</c:choose>

<c:if test="${tourId != 'ALL'}">
Event: 
<select name="eventId" id="eventIdSelect">
<option value="ALL" <c:if test="${eventId == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="event" items="${events}">
    <option value="${event.id}" <c:if test="${eventId == event.id}">selected</c:if>>${event.name} - <fmt:formatDate value="${event.localDate}" pattern="MM/dd/yyyy" />
      <c:choose>
        <c:when test="${not empty event.localTime}">
          <fmt:formatDate value="${event.localTime}" pattern="HH:mm" />
        </c:when>
        <c:otherwise>
          TBD
        </c:otherwise>
      </c:choose>
    </option>
  </c:forEach>
</select>
<c:if test="${eventId != 'ALL'}"> 
</c:if> 
</c:if>

<input id="eventSelectorAction" type="hidden" name="action" value="" /> 
<input id="eventSubmit" type="submit" value="EDIT" class="medButton"/>
<span id="loadingSpan" style="display:none">
  <img src="../images/process-running.gif" align="absbottom"/> Loading...
</span>

<br/>


<table width="100%">
<tr>
<td valign="top" width="50%">
<c:if test="${not empty tourId && eventId == 'ALL' && tourId != 'ALL'}">
  <h3>Tour</h3>

  <b>Expiry: </b>
  <select id="tourExpirySelect" name="tourExpiry" onchange="$('#updateTourExpiryButtons').show()">
    <c:forEach var="hr" begin="12" end="60" step="12">
      <option value="${hr}" <c:if test="${hr == tourExpiry}">selected</c:if>>${hr}hr</option>
    </c:forEach> 
  </select>
  <span id="updateTourExpiryButtons" style="display:none">
    <input id="updateTourExpiryButton"  type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('updateTourExpiry'); $('#eventSelector').submit()"/>
    <input type="button" class="medButton" value="Cancel" onclick="$('#updateTourExpiryButtons').hide(); $('#tourExpirySelect').val(${tourExpiry})"/>
  </span><br/>
  <br/>
  
   <b>Markup Percent: </b>
   <span id="tourMarkupPercentSpan">
    ${tourMarkupPercent}% &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#tourMarkupPercentSpan').hide(); $('#editTourMarkupPercentSpan').show();return false;">Edit</a> 
  </span>
  <span id="editTourMarkupPercentSpan" style="display:none">
    <input type="text" size="4" name="tourMarkupPercent" value="${tourMarkupPercent}"/>%
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeTourMarkupPercent'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#tourMarkupPercentSpan').show(); $('#editTourMarkupPercentSpan').hide()" />
  </span><br/>
  <br/>
  
  <b>Additional Markup Percent: </b>
   <span id="addlTourMarkupPercentSpan">
    ${addlTourMarkupPercent}% &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addlTourMarkupPercentSpan').hide(); $('#editAddlTourMarkupPercentSpan').show();return false;">Edit</a> 
  </span>
  <span id="editAddlTourMarkupPercentSpan" style="display:none">
    <input type="text" size="4" name="addlTourMarkupPercent" value="${addlTourMarkupPercent}"/>%
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeAddlTourMarkupPercent'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#addlTourMarkupPercentSpan').show(); $('#editAddlTourMarkupPercentSpan').hide()" />
  </span><br/>
  <br/>
  
  <b>Shipping Fee: </b>
   <span id="tourShippingFeeSpan">
    ${tourShippingFee} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#tourShippingFeeSpan').hide(); $('#editTourShippingFeeSpan').show();return false;">Edit</a> 
  </span>
  <span id="editTourShippingFeeSpan" style="display:none">
    <input type="text" size="4" name="tourShippingFee" value="${tourShippingFee}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeTourShippingFee'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#tourShippingFeeSpan').show(); $('#editTourShippingFeeSpan').hide()" />
  </span><br/>
  <br/>
  
   <b>Min Threshold: </b>
  <span id="minTourThresholdSpan">
    ${minTourThreshold} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#minTourThresholdSpan').hide(); $('#editMinTourThresholdSpan').show();return false;">Edit</a> 
  </span>
  <span id="editMinTourThresholdSpan" style="display:none">
    <input type="text" size="4" name="minTourThreshold" value="${minTourThreshold}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeMinTourThreshold'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#minTourThresholdSpan').show(); $('#editMinTourThresholdSpan').hide()" />
  </span><br/>
  <br/>
  
   <b>Max Threshold: </b>
  <span id="maxTourThresholdSpan">
    ${maxTourThreshold} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#maxTourThresholdSpan').hide(); $('#editmaxTourThresholdSpan').show();return false;">Edit</a> 
  </span>
  <span id="editmaxTourThresholdSpan" style="display:none">
    <input type="text" size="5" name="maxTourThreshold" value="${maxTourThreshold}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeMaxTourThreshold'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#maxTourThresholdSpan').show(); $('#editmaxTourThresholdSpan').hide()" />
  </span><br/>
  <br/>
   <input type="button" class="medButton" style="width: 130px" value="RemoveTour" onclick="$('#eventSelectorAction').val('RemoveTour'); $('#eventSelector').submit()"/><br/>
  <br/>
  
</td>

</c:if>

<td valign="top">
<c:if test="${not empty eventId && not empty tourId && eventId != 'ALL' && tourId != 'ALL'}">
  <h3>Event</h3>

  <b>Expiration:</b>
  <select id="eventExpirySelect" name="eventExpiry" onchange="$('#updateEventExpiryButtons').show()">
    <c:forEach var="hr" begin="12" end="60" step="12">
      <option value="${hr}" <c:if test="${hr == eventExpiry}">selected</c:if>>${hr}hr</option>
    </c:forEach> 
  </select>
  <span id="updateEventExpiryButtons" style="display:none">
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('updateEventExpiry'); $('#eventSelector').submit()"/>
    <input type="button" class="medButton" value="Cancel" onclick="$('#updateEventExpiryButtons').hide(); $('#eventExpirySelect').val(${eventExpiry})"/>
  </span><br/>
  <br/>
  
  <b>Markup Percent: </b>
   <span id="markupPercentSpan">
    ${markupPercent}% &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#markupPercentSpan').hide(); $('#editmarkupPercentSpan').show();return false;">Edit</a> 
  </span>
  <span id="editmarkupPercentSpan" style="display:none">
    <input type="text" size="4" name="markupPercent" value="${markupPercent}"/>%
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeMarkupPercent'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#markupPercentSpan').show(); $('#editmarkupPercentSpan').hide()" />
  </span><br/>
  <br/>
  
  <b>Additional Markup Percent: </b>
   <span id="addlMarkupPercentSpan">
    ${addlMarkupPercent}% &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addlMarkupPercentSpan').hide(); $('#editAddlMarkupPercentSpan').show();return false;">Edit</a> 
  </span>
  <span id="editAddlMarkupPercentSpan" style="display:none">
    <input type="text" size="4" name="addlMarkupPercent" value="${addlMarkupPercent}"/>%
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeAddlMarkupPercent'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#addlMarkupPercentSpan').show(); $('#editAddlMarkupPercentSpan').hide()" />
  </span><br/>
  <br/>
  
    <b>Shipping Fee: </b>
   <span id="shippingFeeSpan">
    ${shippingFee} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#shippingFeeSpan').hide(); $('#editShippingFeeSpan').show();return false;">Edit</a> 
  </span>
  <span id="editShippingFeeSpan" style="display:none">
    <input type="text" size="4" name="shippingFee" value="${shippingFee}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeShippingFee'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#shippingFeeSpan').show(); $('#editShippingFeeSpan').hide()" />
  </span><br/>
  <br/>
  
   <b>Min Threshold: </b>
  <span id="minThresholdSpan">
    ${minThreshold} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#minThresholdSpan').hide(); $('#editminThresholdSpan').show();return false;">Edit</a> 
  </span>
  <span id="editminThresholdSpan" style="display:none">
    <input type="text" size="4" name="minThreshold" value="${minThreshold}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeMinThreshold'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#minThresholdSpan').show(); $('#editminThresholdSpan').hide()" />
  </span><br/>
  <br/>
  
   <b>Max Threshold: </b>
  <span id="maxThresholdSpan">
    ${maxThreshold} &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#maxThresholdSpan').hide(); $('#editmaxThresholdSpan').show();return false;">Edit</a> 
  </span>
  <span id="editmaxThresholdSpan" style="display:none">
    <input type="text" size="5" name="maxThreshold" value="${maxThreshold}"/>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeMaxThreshold'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#maxThresholdSpan').show(); $('#editmaxThresholdSpan').hide()" />
  </span><br/>
  <br/>
  <input type="button" class="medButton" style="width: 130px" value="RemoveEvent" onclick="$('#eventSelectorAction').val('RemoveEvent'); $('#eventSelector').submit()"/></br>
  </br>
</c:if>
</td></tr></table>
</form>


 
<script type="text/javascript">

$(document).ready(function(){
$('#tourIdSelect').change(function(){ 
if ($(this).val()== $('#tourIdSelect').val())         
      document.location.href="EditEIInstantEvents?tourId=" + $(this).val();
		});
      	    });

</script>
