<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Quotes
</div>

<h1>Quotes</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<form:form commandName="mailerPropertiesCommand" method="post">
	<fieldset>
	 	<legend>Settings</legend>
		<table>
			<tr>
				<td>SMTP Host</td>
				<td>
					<form:input path="smtpHost" cssClass="longInput" />
					<form:errors path="smtpHost" cssClass="error" />					
				</td>
			</tr>
			<tr>
				<td>SMTP Port</td>
				<td>
					<form:input path="smtpPort" cssClass="shortInput"/><span id="defaultSmtpPort"></span>
					<form:errors path="smtpPort" cssClass="error" />									
				</td>
			</tr>
			<tr>
				<td>Email From</td>
				<td>
					<form:input path="smtpFrom" cssClass="longInput" />
					<form:errors path="smtpFrom" cssClass="error" />	
				</td>
			</tr>

		</table>
	</fieldset>
	<br />
	<fieldset>
	 	<legend>Security and Authentication</legend>
		<table>
			<tr>
				<td colspan="2">
					<form:checkbox path="smtpAuth" id="smtpAuth"/><label for="useNameAndPasswordCheckbox">Use name and password</label>
				</td>
			</tr>
			<tbody id="smtpUsernameAndPasswordTBody">
				<tr>
					<td>SMTP Username</td>
					<td>
						<form:input path="smtpUsername" cssClass="longInput" />
						<form:errors path="smtpUsername" cssClass="error" />									
					</td>
				</tr>
				<tr>
					<td>SMTP Password</td>
					<td>
						<form:password path="smtpPassword" cssClass="longInput" />
						<form:errors path="smtpPassword" cssClass="error" />									
					</td>
				</tr>
			</tbody>
			<tr>
				<td colspan="2">
					Use secure connection:
					<form:radiobutton path="smtpSecureConnection" value="no"/>No
					<form:radiobutton path="smtpSecureConnection" value="tls"/>TLS
					<form:radiobutton path="smtpSecureConnection" value="ssl"/>SSL
				</td>
			</tr>
		</table>
	</fieldset>
	
	<br />
	<a href="javascript:void(0)" onclick="testEmailSettings()">Test email settings</a>
	<br /><br />
	<b>Default header:</b><br/>
	<form:textarea path="header" rows="3" cols="80" /><br/>
	<i>(You can use the macro %CUSTOMER% to denote the customer name)</i>
	<br/><br/>
	<b>Default footer:</b><br/>
	<form:textarea path="footer" rows="3" cols="80" />
	<br/><br/>
	<input type="submit" name="submit" value="Save Changes" class="medButton" />
	<input type="button" value="Cancel" class="medButton" onclick="document.location.href='AdmAdminMaintenancein'" />
      
</form:form>
<script type="text/javascript">
	$(document).ready(function() {
		$("INPUT[name='smtpHost']").focus();
		$('#smtpAuth').click(refreshSmtpUsernameAndPasswordTBody);
		refreshSmtpUsernameAndPasswordTBody();

		$("INPUT[name='smtpSecureConnection']").click(refreshSmtpSecureConnection);
		refreshSmtpSecureConnection();
	});
	
	function testEmailSettings() {
		// check parameters
		var smtpHost = $.trim($('#smtpHost').val());
		if (smtpHost == "") {
			alert("Please enter a SMTP Host");
			return;
		}

		var smtpPortString = $.trim($('#smtpPort').val());
		var smtpPort = parseInt(smtpPortString, 10);
		if (isNaN(smtpPort) || isNaN(smtpPortString*1) || smtpPort < 0 || smtpPort > 65535) {
			alert("Please enter a valid SMTP Port");
			return;
		}

		var smtpFrom = $.trim($('#smtpFrom').val());
		if (smtpFrom == "") {
			alert("Please enter an email from");
			return;
		}
		
		var smtpAuth = $('#smtpAuth').attr('checked');
		var smtpUsername = $('#smtpUsername').val();
		var smtpPassword = $('#smtpPassword').val();
		if (smtpAuth) { 
			if (smtpUsername == "") {
				alert("Please enter a smtp username");
				return;

			}
		}
		
		var emailTo = prompt('Enter the email address where you want the test email to be sent to', '${userEmail}');
		if (emailTo != null && emailTo != "") {
			DataDwr.sendTestEmail(
				smtpHost,
				smtpPort,
				smtpAuth,
				smtpUsername,
				smtpPassword,
				$("INPUT[name='smtpSecureConnection']:checked").val(),
				smtpFrom,
				emailTo,
				function(msg) {
					alert(msg);
				}
			);
		}
	};
	
	
	function refreshSmtpUsernameAndPasswordTBody() {
		if ($('#smtpAuth').attr('checked')) {
			$('#smtpUsernameAndPasswordTBody').show();
		} else {
			$('#smtpUsernameAndPasswordTBody').hide();
		}
	};
	
	function refreshSmtpSecureConnection() {
		var s = $("INPUT[name='smtpSecureConnection']:checked").val();
		if (s == "ssl") {
			$('#defaultSmtpPort').text("Default: 465");
		} else {
			$('#defaultSmtpPort').text("Default: 25");
		}
	};
	
</script>
