<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="CompareFeedToCrawl">Compare Feed to Crawl</a> 
  &gt; Compare Feed to Crawl
</div>

<script type="text/javascript">
var eventId = 0;
var tourId = 0;

function changeFilterEvent(eventIdVal) {
	eventId = eventIdVal;
};
	
function changeFilterTour(tourIdVal) {
	tourId = tourIdVal;
	document.location.href = "CompareFeedToCrawl?tourId=" + tourId;
};
	
function changeFilterSource(sourceId) {
	document.location.href = "CompareFeedToCrawl?tourId=" + tourId +"&eventId=" + eventId + "&sourceId=" + sourceId;
};

function gotoEimpEiBoxCompare(){
	document.location.href = "CompareEIMPToEIBOX";
};

</script>

<h1>Compare Feed to Crawl</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<br/>
If you want to comapre EIMarketplace with EIBOX data click <a href="#" onclick="gotoEimpEiBoxCompare()">here</a>
<br/>


<b>Filter:</b> 
<br/>

by Tour
<select onchange="changeFilterTour(this.value);">
	<option value="">Select</option>
	<c:forEach var="tour" items="${tours}">
		<option value="${tour.id}" style="height:20px;" <c:if test="${tour.id == editorCompareTourId}">selected</c:if>>${tour.name}</option>
	</c:forEach>
</select>
<br/>
by Event
<select onchange="changeFilterEvent(this.value);">
	<option value="">Select</option>
	<c:forEach var="event" items="${events}">
		<option value="${event.id}" style="height:20px;" <c:if test="${event.id == editorCompareEventId}">selected</c:if>>${event.name}-${event.localDate}</option>
	</c:forEach>
</select>
<br/>
by Source
<select onchange="changeFilterSource(this.value);">
	<option value="">Select</option>
	<option value="1" style="height:20px;" >Stubhub Feed vs Stubhub Crawl</option>
	<option value="2" style="height:20px;" >Ticket Network Direct vs Ticket Network Crawl</option>
</select>

<br/>
<br/>
<b>TICKET INDEXED FROM FEED:</b> ${feedAndCrawlComparator.ticketIndexedFromFeed}
<br/>
<b>TICKET INDEXED FROM Web:</b> ${feedAndCrawlComparator.ticketIndexedFromWeb}
<br/>
<br/>
EXTRA TICKET IN FEED : <br/>

<display:table class="list" name="${feedAndCrawlComparator.extraTicketInFeed}" id="extratickethit" requestURI="CompareFeedToCrawl" pagesize="50">
	<display:column title="Section"  sortable="true" property="section"/>
	<display:column title="Row"  sortable="true" property="row"/>
    <display:column title="Quantity"  sortable="true" property="quantity"/>
    <display:column title="Current Price"  sortable="true" property="currentPrice" />
</display:table>


<br/>
<br/>
MISSING TICKET IN FEED : <br/>

<display:table class="list" name="${feedAndCrawlComparator.missingTicketInFeed}" id="missingtickethit" requestURI="CompareFeedToCrawl" pagesize="50">

	<display:column title="Section"  sortable="true" property="section"/>
	<display:column title="Row"  sortable="true" property="row"/>
    <display:column title="Quantity"  sortable="true" property="quantity"/>
    <display:column title="Current Price"  sortable="true" property="currentPrice" />
</display:table>
<br />