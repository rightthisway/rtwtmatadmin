<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Edit EiMarketPlace Properties 
</div>

<c:if test="${not empty error}">
  <div class="info"><div class="infoText">${error}</div></div>
</c:if>

<h1>Edit EiMarketPlace Properties</h1>

<c:if test="${not empty userInfoMessage}">
	<div class="info"><div class="infoText">${userInfoMessage}</div></div>
</c:if>

<c:if test="${not empty userInfoError}">
	<div class="info"><div class="infoText">${userInfoError}</div></div>
</c:if>

<form action="AdminEditEiMarketPlaceProperties" method="POST">
<input type="hidden" name="action" value="update" />
<table>
	<tr>
		<td>EiMarketPlace Username</td>
		<td><input type="text" id="eiMarketPlaceUsername" name="eiMarketPlaceUsername" value="<c:out value='${eiMarketPlaceUsername}' />" /></td>
	</tr>
	<tr>
		<td>EiMarketPlace Password</td>
		<td><input type="password" id="eiMarketPlacePassword" name="eiMarketPlacePassword" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<a href="javascript:void(0)" onclick="checkEiMarketPlaceCredentials()">Check credentials</a>
			<div id="checkEiMarketPlaceCredentialsResult" style="margin-top: 3px;font-weight: bold"></div>
			<br />
		
			<input type="submit" value="Update" class="medButton" />
			<input type="button" value="Cancel" class="medButton" onclick="document.location.href='AdminMaintenance'" />
		</td>
	</tr>
</table>
</form>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("INPUT[@name='eiMarketPlaceUsername']").focus();
	});
	
	function checkEiMarketPlaceCredentials() {
		$('#checkEiMarketPlaceCredentialsResult').empty();
		$('#checkEiMarketPlaceCredentialsResult').append(
			$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
			"Checking..."
		);
		DataDwr.checkEiMarketPlaceCredentials(
			$('#eiMarketPlaceUsername').val(),
			$('#eiMarketPlacePassword').val(),
			function(result) {
				if (result == "OK") {
					$('#checkEiMarketPlaceCredentialsResult').css('color', 'green');
					$('#checkEiMarketPlaceCredentialsResult').text(result);
				} else {
					$('#checkEiMarketPlaceCredentialsResult').css('color', 'red');
					$('#checkEiMarketPlaceCredentialsResult').text(result);
				}
			});
	};
</script>