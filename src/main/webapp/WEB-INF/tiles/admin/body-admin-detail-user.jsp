<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="AdminManageUsers">Manage Users</a> 
  &gt; User Details
</div>

<style>
	TABLE TH {text-align: left;}
</style>

<h1>User Information</h1>

<h2>User Details</h2>

<table>
	<tr>
		<th>Username</th>
		<td>${user.username}</td>
	</tr>
	<tr>
		<th>E-Mail</th>
		<td>${user.email}</td>
	</tr>
	<tr>
		<th>First Name</th>
		<td>${user.firstName}</td>
	</tr>
	<tr>
		<th>Last Name</th>
		<td>${user.lastName}</td>
	</tr>
	<tr>
		<th>Logged In</th>
		<td>${user.loggedIn}</td>
	</tr>
	<tr>
		<th>Phone</th>
		<td>${user.phone}</td>
	</tr>
</table>

<h2>User Rights</h2>
<table>
	<tr>
		
		<th>Roles</th>
		<td>
			<c:forEach var="role" items="${user.roles}">
				${role.name},
			</c:forEach>
		</td>
	</tr>
	
	<tr>
		<th>Broker </th>
		
		<c:if test ="${user.broker.id eq '1'}" > <td>MZTix</td> </c:if> 
		<c:if test ="${user.broker.id eq '2'}" > <td>JoeKnownTicket</td> </c:if>
		<c:if test ="${user.broker.id eq '3'}" > <td>ReverseOneTicket</td> </c:if>
		<c:if test ="${user.broker.id eq '4'}" > <td>Manhattan</td> </c:if>
	</tr>
	
	<tr>
		<th>Locked (Disabled)</th>
		<td>${user.locked}</td>
	</tr>
	
</table>

<h2>Emails</h2>
<table>
	<tr>
		<th>Receive emails for errors on crawl I created</th>
		<td>
			<c:choose>
				<c:when test="${user.emailCrawlErrorCreatorEnabled}">true</c:when>
				<c:otherwise>false</c:otherwise>
			</c:choose>
		</td>
	</tr>
	<tr>
		<th>Receive emails for errors on crawl I last updated</th>
		<td>
			<c:choose>
				<c:when test="${user.emailCrawlErrorLastUpdaterEnabled}">true</c:when>
				<c:otherwise>false</c:otherwise>
			</c:choose>
		</td>
	</tr>
</table>

<h2>Created Crawls</h2>
<display:table class="list" name="${createdCrawls}" id="crawl" requestURI="#" defaultsort="2">
    <display:column title="Name" sortable="true">
    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}"/>
    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}">${crawl.name}</a>
    </display:column>
    <display:column title="Created" sortable="true">
		<fmt:formatDate value="${crawl.created}" pattern="yyyy/MM/dd HH:mm:ss" />
    </display:column>
    <%-- <display:column title="Tour" property="tour.name" sortable="true" href="BrowseTours" paramId="id" paramProperty="id"/> --%>
    <display:column title="Event" sortable="true">
    	<c:choose>
    		<c:when test="${empty crawl.eventId}">
    			AUTO
    		</c:when>
    		<c:otherwise>
			     <a href="BrowseTickets?eventId=${crawl.eventId}">
						<fmt:formatDate value="${crawl.event.date}" pattern="yyyy/MM/dd" />
						${crawl.event.venue.building}
			     </a>
    		</c:otherwise>
    	</c:choose>
    </display:column>

</display:table>

<h2>Last Updated Crawls</h2>
<display:table class="list" name="${lastUpdatedCrawls}" id="crawl" requestURI="#" defaultsort="2">
    <display:column title="Name" sortable="true">
    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}"/>
    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}">${crawl.name}</a>
    </display:column>
    <display:column title="Last Updated" sortable="true">
		<fmt:formatDate value="${crawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" />
    </display:column>
    <%-- <display:column title="Tour" property="tour.name" sortable="true" href="BrowseTours" paramId="id" paramProperty="id"/> --%>
    <display:column title="Event" sortable="true">
    	<c:choose>
    		<c:when test="${empty crawl.eventId}">
    			AUTO
    		</c:when>
    		<c:otherwise>
			     <a href="BrowseTickets?eventId=${crawl.eventId}">
						<fmt:formatDate value="${crawl.event.date}" pattern="yyyy/MM/dd" />
						${crawl.event.venue.building}
			     </a>
    		</c:otherwise>
    	</c:choose>
    </display:column>
    
</display:table>	
