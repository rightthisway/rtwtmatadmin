<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; View and Remove Zone Events
</div>

<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${info}
  </div>
</div>
</c:if>

<form id="browseZoneViewEventsForm" action="BrowseZoneViewEvents" method="GET">
<input id="action" type="hidden" name="action" value="" />
Tour:
<select name="tourId" onchange="location.href='BrowseZoneViewEvents?tourId=' + $(this).val()">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
</select>

<b>Select:</b>
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<b>Action:</b>
<input type="button" class="medButton" style="width: 160px" value="Remove Zone Event" onclick="removeSelectedEvents()"/>&nbsp;&nbsp;
<br/>
<br/>


<display:table class="list" name="${events}" id="event" requestURI="BrowseZoneViewEvents?tourId=${tourId}" pagesize="1000">
 
  <display:column>
    <input type="checkbox" class="event_checkbox" name="event_${event.id}"/>
  </display:column>
  <c:if test="${tourId == 'ALL'}">
    <display:column title="Tour" sortable="true">${event.artist.name}</display:column>
  </c:if>
  <display:column title="Event" sortable="true"><c:if test="${event.zoneEvent != true}">${event.name}</c:if><c:if test="${event.zoneEvent == true}"><span style="color:green">${event.name}</span></c:if></display:column>
  <display:column title="Date" sortable="true">
    <fmt:formatDate pattern="MM/dd/yy" value="${event.localDate}" />
    <c:choose>
      <c:when test="${event.localTime == null}">
        TBD
      </c:when>
      <c:otherwise>
        <fmt:formatDate pattern="HH:mm" value="${event.localTime}" />
      </c:otherwise>
    </c:choose>
  </display:column>
  <display:column title="Venue" sortable="true">
   ${event.venue.building}
  </display:column>
</display:table>   

</form>
<br/>


<script type="text/javascript">
 
  function selectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }

  function unselectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }
  
  function removeSelectedEvents() {
    if ($('.event_checkbox:checked').length == 0) {
      alert("Please select at least one event");
      return;
    }
    
    var answer = confirm("Are you sure you want to remove the selected events from zone events?");
    if (answer) {
       $('#action').val('removeEventsFromZone');
       $('#browseZoneViewEventsForm').submit();
    }
  }

</script>