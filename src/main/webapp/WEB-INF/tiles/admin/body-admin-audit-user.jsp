<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Audit Users
</div>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<h1>User Audit</h1>
<form action="AdminUserAudit">
	<div align ="center">
	<table>
		<tr>
			<td>
				Date :
			</td>
			<td>
				<input type ="text" id="startDate" name="startDate" value="${startDate}" />
			</td>
			<td>
				<input type ="text" id="endDate" name="endDate" value="${endDate}"/>
			</td>
		</tr>
		
		<tr>
		<td>
				Broker :
		</td>
		<td>
		<select id="brokerId" name="brokerId" >
					<option value="">---ALL---</option>
					<c:forEach items="${brokers }" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${broker ne null and brokerObj.id eq broker}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>										
				</select>
				
				
				
				
		</td>
		</tr>
		<tr>
			<td>
				User :
			</td>
			<td colspan="2">
				
				<%-- <c:choose>
				<c:when test="${not empty broker or broker eq null}">  --%>
						<select  id="users" name="userName" name="userName">
						<option value="all" <c:if test="${'all' == userName}"> selected</c:if>>-- All --</option>
					
					
						<c:forEach var="user" items="${users}">
						
						<option value="${user.username}" <c:if test="${user.username == userName}"> selected</c:if> >${user.firstName} ${user.lastName}</option>
						</c:forEach>
					<%--  </c:when>
						
					<c:otherwise>
						<select id = with_brok name="userName" name="userName">
						<option value="all" <c:if test="${'all' == userName}"> selected</c:if>>-- All --</option>
					
						<c:forEach var="user" items="${brok_user}">
						<option value="${user.username}" <c:if test="${user.username == userName}"> selected</c:if> >${user.firstName} ${user.lastName}</option>
						</c:forEach>
					
					
					</c:otherwise> 
					
				</c:choose>	 --%>	
					</select>
			</td>
		</tr>
		
		
		
		<tr>
			<td colspan="2">
				<input type ="submit" id="submit" value="Submit" class="medButton"/>
			</td>
		</tr>
	</table>
	
	<display:table class="list" name="${actionList}" id="userAction" requestURI="AdminUserAudit">
		 <display:column title="IP Address" property="ipAddress" sortable="true"/>
		 <display:column title="FirstName" property="user.firstName" sortable="true"/>
		 <display:column title="LastName" property="user.lastName" sortable="true"/>
		 <display:column title="Action" property="action" sortable="true"/>
		 <display:column title="Time" sortable="true" >
			<fmt:formatDate pattern="MMM dd yyyy hh:mm aa" value="${userAction.timeStamp}" />
		 </display:column>
	</display:table>
	</div>
</form>
<script type="text/javascript">

		$('INPUT[name=startDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		$('INPUT[name=endDate]').datepicker({
	        dateFormat:'mm/dd/yy',
			showOn: 'button',
			buttonImage: '../images/ico-calendar.gif',
			buttonImageOnly: true        
		});
		
		$(document).ready(function(){
			
		$("#brokerId" ).change(function() {
				
				var brokerId = $("#brokerId").val();
        url = "GetUsernameByBroker?brokerId="+brokerId;;
				$.ajax({
				url:url,
				dataType:'text',
				
				cache:false,
				success: function(res){
					//jsonData = jQuery.parseJSON(res);
					var jsonData = JSON.parse(res);
					
					$('#users').empty();
					for (var i = 0; i < jsonData.length; i++) {
		                var  data= jsonData[i]; 
						var rowText = "<option value="+data.username+ ">"+ data.firstName+" "+data.lastName+"</option>"
						$('#users').append(rowText);
		            }	
				}
				});
				});
			
		});
</script>