<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu"
	scope="request" />

<div id="adminSubMenu">
	<c:choose>
		<c:when test="${selectedSubMenu == 'Manage Users'}">
			<b>Manage Users</b>
		</c:when>
		<c:otherwise>
			<a href="AdminManageUsers">Manage Users</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Tnd'}">
			<b>Tnd</b>
		</c:when>
		<c:otherwise>
			<a href="TndSalesReport">Tnd</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Crawler Counter'}">
			<b>Crawler Counter</b>
		</c:when>
		<c:otherwise>
			<a href="EditorCrawlerCounter">Crawler Counter</a>
		</c:otherwise>
	</c:choose>

	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'System Stats'}">
			<b>System Stats</b>
		</c:when>
		<c:otherwise>
			<a href="AdminSystemStats">System Stats</a>
		</c:otherwise>
	</c:choose>

	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Settings'}">
			<b>Settings</b>
		</c:when>
		<c:otherwise>
			<a href="AdminEditMailerProperties">Settings</a>
		</c:otherwise>
	</c:choose>
	|
	
	<c:choose>
		<c:when test="${selectedSubMenu == 'Maintenance'}">
			<b>Maintenance</b>
		</c:when>
		<c:otherwise>
			<a href="AdminMaintenance">Maintenance</a>
		</c:otherwise>
	</c:choose>
	<%-- |
	<c:choose>
	  <c:when test="${selectedSubMenu == 'InstantFileSettings'}">
   	    <b>Instant File Settings</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="AdminInstantFileSettings">Instant File Settings</a>
	  </c:otherwise>
	</c:choose> --%>
	
	<%-- |
 <c:choose>
    <c:when test="${selectedSubMenu == 'EiMarketPlace'}">
      <b>EiMarketPlace</b>
    </c:when>
    <c:otherwise>
      <a href="AdminEditEiMarketPlaceProperties">EiMarketPlace</a>
    </c:otherwise>
  </c:choose>
  |
  <c:choose>
    <c:when test="${selectedSubMenu == 'AdminEbay'}">
      <b>Ebay Properties</b>
    </c:when>
    <c:otherwise>
      <a href="AdminEbay">Ebay Properties</a>
    </c:otherwise>
  </c:choose> --%>
	
	
	<%--  |
  <c:choose>
			<c:when test="${selectedSubMenu == 'InstantTickets'}">
			<b>InstantTickets</b>
			</c:when>
			<c:otherwise>
			<a href="InstantTickets">InstantTickets</a>
			</c:otherwise>
	</c:choose>	
|
  <c:choose>
			<c:when test="${selectedSubMenu == 'MercuryTickets'}">
			<b>MercuryTickets</b>
			</c:when>
			<c:otherwise>
			<a href="MercuryTickets">MercuryTickets</a>
			</c:otherwise>
	</c:choose>	 
|
  <c:choose>
			<c:when test="${selectedSubMenu == 'EITickets'}">
			<b>EITickets</b>
			</c:when>
			<c:otherwise>
			<a href="EITickets">EITickets</a>
			</c:otherwise>
	</c:choose>		
|	
  <c:choose>
			<c:when test="${selectedSubMenu == 'Compare Feed to Crawl'}">
			<b>Compare Feed to Crawl</b>
			</c:when>
			<c:otherwise>
			<a href="CompareFeedToCrawl">Compare Feed to Crawl</a>
			</c:otherwise>
	</c:choose>		
|		
  <c:choose>
			<c:when test="${selectedSubMenu == 'BrowseInstantEvents'}">
			<b>Add Instant Events</b>
			</c:when>
			<c:otherwise>
			<a href="BrowseInstantEvents">Add Instant Events</a>
			</c:otherwise>
	</c:choose>	
|		
  <c:choose>
			<c:when test="${selectedSubMenu == 'EditInstantEvents'}">
			<b>Edit SH Instant Events</b>
			</c:when>
			<c:otherwise>
			<a href="EditInstantEvents">Edit SH Instant Events</a>
			</c:otherwise>
	</c:choose>				
|
	 <c:choose>
			<c:when test="${selectedSubMenu == 'EditEIInstantEvents'}">
			<b>Edit EI Instant Events</b>
			</c:when>
			<c:otherwise>
			<a href="EditEIInstantEvents">Edit EI Instant Events</a>
			</c:otherwise>
	</c:choose>	
|
	 <c:choose>
			<c:when test="${selectedSubMenu == 'EditTNInstantEvents'}">
			<b>Edit TN Instant Events</b>
			</c:when>
			<c:otherwise>
			<a href="EditTNInstantEvents">Edit TN Instant Events</a>
			</c:otherwise>
	</c:choose>	
|
	<c:choose>
			<c:when test="${selectedSubMenu == 'EditTNowInstantEvents'}">
			<b>Edit TNow Instant Events</b>
			</c:when>
			<c:otherwise>
			<a href="EditTNowInstantEvents">Edit TNow Instant Events</a>
			</c:otherwise>
	</c:choose>	
|
  	<c:choose>
    	<c:when test="${selectedSubMenu == 'InstantEvent Settings'}">
      		<b>InstantEvent Settings</b>
    	</c:when>
    	<c:otherwise>
      		<a href="AdminEditInstantEventSettings">InstantEvent Settings</a>
    	</c:otherwise>
  	</c:choose>
|
	<c:choose>
			<c:when test="${selectedSubMenu == 'BrowseZoneEvents'}">
				<b>Add Zone Events</b>
			</c:when>
			<c:otherwise>
			<a href="BrowseZoneEvents">Add Zone Events</a>
			</c:otherwise>
	</c:choose> 
|
	<c:choose>
			<c:when test="${selectedSubMenu == 'BrowseZoneViewEvents'}">
				<b>View & Remove Zone Events</b>
			</c:when>
			<c:otherwise>
				<a href="BrowseZoneViewEvents">View & Remove Zone Events</a>
			</c:otherwise>
	</c:choose>    --%>

	

	
	

</div>
<tiles:insertAttribute name="subBody" />