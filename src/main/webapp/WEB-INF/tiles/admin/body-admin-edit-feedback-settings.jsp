<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; Edit Feedback Settings
</div>

<h1>Feedback Settings</h1>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<form action="AdminEditFeedbackSettings">
	<input type="hidden" name="action" value="update" />

	Recipients of feedbacks (separate emails by comma)<br /> 
	<input name="feedbackEmailTo" value="${feedbackEmailTo}" style="width:500px"><br />
	<br />
	Recipients of error feedbacks (separate emails by comma)<br /> 
	<input name="feedbackErrorEmailTo" value="${feedbackErrorEmailTo}" style="width:500px"><br />
	<br />	
	
	<input type="checkbox" id="jiraEnabled" name="jiraEnabled" <c:if test="${jiraEnabled}">checked</c:if> onchange="toggleJiraTableVisibility(this.checked)"/><label for="jiraEnabled">Post error feedbacks to JIRA</label>
	<table id="jiraTable" style="<c:if test="${!jiraEnabled}">display:none;</c:if>;margin-left:40px;padding-left:10px;border-left: 2px  solid #850001" >
		<tr>
			<td>JIRA URI</td>
			<td><input name="jiraUri" value="${jiraUri}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA RPC Path</td>
			<td><input name="jiraRpcPath" value="${jiraRpcPath}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA SOAP Path</td>
			<td><input name="jiraSoapPath" value="${jiraSoapPath}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA Project</td>
			<td><input name="jiraProject" value="${jiraProject}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA Components (separate components by comma)</td>
			<td><input name="jiraComponents" value="${jiraComponents}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA Affects Versions (separate components by comma)</td>
			<td><input name="jiraAffectsVersions" value="${jiraAffectsVersions}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA Username</td>
			<td><input name="jiraUsername" value="${jiraUsername}" style="width:200px"></td>
		</tr>
		<tr>
			<td>JIRA Password</td>
			<td><input name="jiraPassword" value="${jiraPassword}" style="width:200px"></td>
		</tr>
	</table>
	<br />

	<input type="checkbox" id="redmineEnabled" name="redmineEnabled" <c:if test="${redmineEnabled}">checked</c:if> onchange="toggleRedmineTableVisibility(this.checked)"/><label for="redmineEnabled">Post error feedbacks to Redmine</label>
	<table id="redmineTable" style="<c:if test="${!redmineEnabled}">display:none;</c:if>;margin-left:40px;padding-left:10px;border-left: 2px  solid #850001" >
		<tr>
			<td>Redmine URI</td>
			<td><input name="redmineUri" value="${redmineUri}" style="width:200px"></td>
		</tr>
		<tr>
			<td>Redmine TMAT Project</td>
			<td><input name="redmineProject" value="${redmineProject}" style="width:200px"></td>
		</tr>
		<tr>
			<td>Redmine EBAY Project</td>
			<td><input name="redmineEbayProject" value="${redmineEbayProject}" style="width:200px"></td>
		</tr>
		<tr>
			<td>Redmine Username</td>
			<td><input name="redmineUsername" value="${redmineUsername}" style="width:200px"></td>
		</tr>
		<tr>
			<td>Redmine Password</td>
			<td><input name="redminePassword" value="${redminePassword}" style="width:200px"></td>
		</tr>
	</table>
	<br />
		
	<input type="submit" value="Update" class="medButton" />		
	
</form>

<script type="text/javascript">
	function toggleJiraTableVisibility(checked) {
		if (checked) {
			$('#jiraTable').show();
		} else {
			$('#jiraTable').hide();		
		}
	};

	function toggleRedmineTableVisibility(checked) {
		if (checked) {
			$('#redmineTable').show();
		} else {
			$('#redmineTable').hide();		
		}
	};

</script>