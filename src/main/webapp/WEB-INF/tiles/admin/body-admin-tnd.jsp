<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:set var="username"><authz:authentication operation="username"/></c:set>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Maintenance
</div>

<h1>TND Sales Report</h1>

<form action="DownloadTndSalesReport" method="post">

	<table align="center" width="300px;">
		<tr>
			<td colspan="2"><label id="msg"></label></td>
		</tr>
		<tr><td><b>Order Start Date: </b></td>
			<td>
			

<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>
		
		<tr>
    <td><b>Order End Date: </b></td>
<td>

<select name="toMonth" id="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay" >
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="toYear" id="toYear" >
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
</td>
</tr>
		<tr>
			<td><b>Parent : </b></td>
			<td>
			<select id="parent" name="parent" onchange="getChild()">
			<option value="ALL">ALL</option>
			<option value="sports">Sports</option>
			<option value="concerts">Concerts</option>
			<option value="theatre">Theatre</option>
			<option value="other">Other</option>
			</select>
			</td>
		</tr>
		
		<tr>
			<td><b>Child : </b></td>
			<td>
			<select id="child" name="child">
			<option value="ALL">ALL</option>
			</select>
			</td>
		</tr>
		
		<tr>
			<td><b>Venue Name : </b></td>
			<td>
			
			<span id="venueLabelSpanId" style="display: none;">
				<span id="venueNameDiv" >

					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" style="display: block;">
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
						<input type="hidden" id="venue" name="venue"/>
			
			<%-- <select name="venue">
			<option value="ALL">ALL</option>
			<c:forEach items="${venues}" var="venue">
			<option value="${venue}">${venue}</option>
			</c:forEach>
			</select> --%>
			</td>
		</tr>
		
		<tr>
			<td><b>Accept Status : </b></td>
			<td>
			<select name="acceptStatus">
			<option value="ALL">ALL</option>
			<option value="accepted">Accepted</option>
			<option value="rejected">Rejected</option>
			</select>
			</td>
		</tr>
		
		<tr>
			<td><b>Ticket Type : </b></td>
			<td>
			<select name="ticketType">
			<option value="ALL">ALL</option>
			<option value="real">Real</option>
			<option value="category">Category</option>
			</select>
			</td>
		</tr>
		
		<%-- <tr>
			<td><b>Company Name : </b></td>
			<td>
			<select name="cmpName">
			<option value="ALL">ALL</option>
			<c:forEach items="${cmpNames}" var="type">
			<option value="${type}">${type}</option>
			</c:forEach>
			</select>
			</td>
		</tr> --%>
		
		<tr>
			<td><b>Stock Type : </b></td>
			<td>
			<select name="stockType">
			<option value="ALL">ALL</option>
			<c:forEach items="${stockTypes}" var="type">
			<option value="${type}">${type}</option>
			</c:forEach>
			</select>
			</td>
		</tr>
		
		
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><input id="downloadReportButton" style="width:160px;margin-left:60px;" name="action" type="submit" class="medButton" value="Export Csv"/></td>
		</tr>	
	</table>
</form>

<script type="text/javascript">

$(document).ready(function() {
	$('.calendar').datepicker({
		'buttonImageOnly': true,
        buttonImage: '../images/ico-calendar.gif',
        showOn: 'button',
        showButtonPanel: true,
		changeMonth: true,
		changeYear: true,	        
        dateFormat:'yy-mm-dd',
        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
         maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)
	});
	
	$('#fromCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
        }
    );

	$('#fromCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#fromYear').val(tokens[0]*1);
			$('#fromMonth').val(parseInt(tokens[1]*1));
			$('#fromDay').val(parseInt(tokens[2]*1));
		}
	);
	
	$('#toCalendar').datepicker('option', 'beforeShow',
        function(input) {
        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
        }
    );
	
	$('#toCalendar').datepicker('option', 'onSelect',
        function(dateText) {
			var tokens = dateText.split("-");
			$('#toYear').val(tokens[0]*1);
			$('#toMonth').val(parseInt(tokens[1])*1);
			$('#toDay').val(parseInt(tokens[2])*1);
		}
	);			
});

	function getChild(){
	var select = document.getElementById("parent");
	var parent = select.options[select.selectedIndex].value;
	
		if(parent != 'ALL'){
		
		$.ajax({
			dataType:'text',
			url:"TndGetChildByParent?parent="+parent,
			cache:false,
			success: function(res){
				//jsonData = jQuery.parseJSON(res);
				var jsonData = JSON.parse(res);
				
				$('#child').children().remove();
				$('#child').append("<option value=ALL>ALL</option>");
				for (var i = 0; i < jsonData.length; i++) {
	                var  data= jsonData[i]; 
					
					var rowText = "<option value="+data.child+ ">"+ data.child +"</option>"
					$('#child').append(rowText);
	            }	
			}
		});
		}
	}

	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val('');
		$("#venue").val(row[0]);
		$('#venueNameDiv').text(row[1]);
			
		$('#venueLabelSpanId').show();
		$('#venueSelectSpanId').hide();
	});
	
	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#venue').val('');
	}
	
</script>