<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="AdminManageUsers">Manage Users </a> 
  &gt; ${user.firstName } ${user.lastName} Audit
</div>


<h1>${user.firstName } ${user.lastName} Audit</h1>

<display:table class="list" name="${actions}" id="action" requestURI="AuditUser">
	<display:column property="action"/>
	<display:column property="timeStamp" format="{0,date,MMM dd, yyyy hh:mm aa}"/>
</display:table>