<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:set var="username"><authz:authentication operation="username"/></c:set>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Maintenance
</div>

<div id="info" class="info" style="display:none">
  <div id="infoText" class="infoText" >
  </div>
</div>

<h1>Maintenance</h1>

<div id="pleaseWait" style="display:none">
	<img src="../images/process-running.gif" align="absbottom" />Please wait...
	<br /><br />
</div>

<h2>Data Cleaning</h2>
<img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return deleteEmptyEvents()">Delete empty events</a> (events with no ticket, crawler and shorts)<br/>
<!-- <img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return deleteEmptyTours()">Delete empty tours</a> (tours with no event and crawler and zones)<br/> -->
<img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return deleteEmptyArtists()">Delete empty artists</a> (artists with no event)<br/>
<!--  <img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return deleteEmptyVenues()">Delete empty venues</a> (venues with no event)<br/> -->
<!-- <img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return cleanCategoryMappings()">Clean Zones</a> (zone mappings associated to no event)<br/> -->
<img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return deleteUnusedTicketListingCrawl()">Delete unused crawlers</a> (crawlers not associated to any tour/event)<br/>
<img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return cleanBookmarks()">Clean bookmarks</a> (bookmarks not associated to anything)<br/>
<img src="../images/ico-delete.gif" align="absbottom"/> Reset preferences for <img src="../images/ico-user.gif" align="absbottom"/>
<select id="preferenceUser">
  <c:forEach var="user" items="${users}">
    <option value="${user.username}" <c:if test="${username == user.username}">selected</c:if>>${user.username}</option>
  </c:forEach>
</select>
<input type="button" class="medButton" value="reset" onclick="resetPreferencesForUser($('#preferenceUser').val())"/><br/>
<img src="../images/ico-delete.gif" align="absbottom"/> <a href="#" onclick="return resetPreferences()">Reset preferences for all the users</a>

<h2>Data Recalculation</h2>
<img src="../images/ico-start.gif" align="absbottom"/> <a href="#" onclick="return recomputeAllEventTicketValues()">Recompute event ticket values (count and quantity sum)</a><br/>
<img src="../images/ico-start.gif" align="absbottom"/> <a href="#" onclick="return recomputeTicketRemainingQuantities()">Recompute Ticket Remaining Quantities</a><br/>
<!-- <img src="../images/ico-start.gif" align="absbottom"/> <a href="#" onclick="return recomputeTourEventCounts()">Recompute All Tour Event Counts</a><br/> -->

<h2>Misc</h2>
<img src="../images/ico-start.gif" align="absbottom"/> Fetch SeatWave Events (about 20 mins)
<c:choose>
	<c:when test="${seatWaveEventPopulatorRunning}">
		<img src="../images/process-running.gif" align="absbottom" />(${seatWaveEventPopulatorFetchedEventCount} events fetched (about 39,000 in total))				
	</c:when>
	<c:otherwise>
		<a id="fetchSeatWaveEventsLink" href="#" onclick="return fetchSeatWaveEvents()">Start</a>
	</c:otherwise>	
</c:choose>
<br/>
<img src="../images/ico-start.gif" align="absbottom"/> Fetch EiMarketPlace Events (about 20 mins)
<c:choose>
	<c:when test="${eiMarketPlaceEventPopulatorRunning}">
		<img src="../images/process-running.gif" align="absbottom" />(${eiMarketPlaceEventPopulatorFetchedEventCount} events fetched (of about 27,000 events))				
	</c:when>
	<c:otherwise>
		<a id="fetchEiMarketPlaceEventsLink" href="#" onclick="return fetchEiMarketPlaceEvents()">Start</a>
	</c:otherwise>	
</c:choose>
<br/>
<img src="../images/ico-start.gif" align="absbottom"/> Fetch StubHub Autocrawler Events...
<c:choose>
	<c:when test="${refreshAutoCrawlerRunning}">
	    <jsp:useBean id="now" class="java.util.Date"/>
		<img src="../images/process-running.gif" align="absbottom" />Fetching...
	</c:when>
	<c:otherwise>
		<a href="#" onclick="refreshAutoCrawler(); return false;">Start</a> (about 15 mins)
	</c:otherwise>	
</c:choose>


<script language="javascript" type="text/javascript">

function refreshAutoCrawler() {
  $('#pleaseWait').show();
  location.href='AdminMaintenance?action=refreshAutoCrawler';
}


//  function cleanCategoryMappings() {
//	$('#pleaseWait').show();
//	/* DataDwr.cleanCategoryMappings( */
//		function(response) {
//			if (response == 'OK') {
//				$('#infoText').html("All unassociated zone mappings have been deleted!");
//				$('#info').show();
//			} else {
//				alert(response);
//			}
//			$('#pleaseWait').hide();
//		});
//};

function recomputeAllEventTicketValues() {
	$('#pleaseWait').show();
	DataDwr.recomputeAllEventTicketValues(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("All the events have been updated!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		});
}

function deleteUnusedTicketListingCrawl() {
    var answer = confirm("Are you sure you want to delete unused crawls?");
    if (!answer) {
      return false;
    }
	$('#pleaseWait').show();
	DataDwr.deleteUnusedTicketListingCrawl(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Unused crawlers deleted!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
};

function fetchSeatWaveEvents() {
	$('#fetchSeatWaveEventsLink').hide();
	$('#pleaseWait').show();
	DataDwr.fetchSeatWaveEvents(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Fetching SeatWave Events!");
				$('#info').show();
			} else {
				alert(response);
			}
		}
	);
};

function fetchEiMarketPlaceEvents() {
	$('#eiMarketPlaceEventsLink').hide();
	$('#pleaseWait').show();
	DataDwr.fetchEiMarketPlaceEvents(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Fetching EiMarketPlace Events!");
				$('#info').show();
			} else {
				alert(response);
			}
		}
	);
};

function deleteEmptyEvents() {
    var answer = confirm("Are you sure you want to delete empty events?");
    if (!answer) {
      return false;
    }
    
	$('#pleaseWait').show();
	DataDwr.deleteEmptyEvents(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Empty events deleted!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
	return false;
};

function deleteEmptyTours() {
    var answer = confirm("Are you sure you want to delete empty tours?");
    if (!answer) {
      return false;
    }
	$('#pleaseWait').show();
	DataDwr.deleteEmptyTours(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Empty tours deleted!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
};

function deleteEmptyArtists() {
    var answer = confirm("Are you sure you want to delete empty artists?");
    if (!answer) {
      return false;
    }
	$('#pleaseWait').show();
	DataDwr.deleteEmptyArtists(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Empty artists deleted!");
				$('#info').show();
			} else {
				alert(response);
			}

			$('#pleaseWait').hide();
		}
	);
};

function deleteEmptyVenues() {
    var answer = confirm("Are you sure you want to delete empty venues?");
    if (!answer) {
      return false;
    }

	$('#pleaseWait').show();
	DataDwr.deleteEmptyVenues(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Empty venues deleted!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
};

function cleanBookmarks() {
    var answer = confirm("Are you sure you want to delete bogus bookmarks?");
    if (!answer) {
      return false;
    }
	$('#pleaseWait').show();
	DataDwr.cleanBookmarks(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Bookmarks cleaned!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
};

function recomputeTicketRemainingQuantities() {
	$('#pleaseWait').show();
	DataDwr.recomputeTicketRemainingQuantities(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Ticket Remaining Quantities recomputed!");
				$('#info').show();
			} else {
				alert(response);
			}
			$('#pleaseWait').hide();
		}
	);
};
/*
 function recomputeTourEventCounts() {
	$('#pleaseWait').show();
	DataDwr.recomputeTourEventCounts(
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Tour Event Counts recomputed!");
				$('#info').show();
			} else {
				alert(response);
			}

			$('#pleaseWait').hide();
		}
	);
}; */

function resetPreferencesForUser(username) {
  var answer = confirm("Are you sure you want to delete the preferences for the user " + username + "?");
  if (!answer) {
    return false;
  }
  
	$('#pleaseWait').show();
	DataDwr.deletePreferencesForUser(username, 
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("Preferences for user " + username + " deleted!");
				$('#info').show();
			} else {
				alert(response);
			}

			$('#pleaseWait').hide();
		}
	);  
}

function resetPreferences() {
  var answer = confirm("Are you sure you want to delete all the preferences?");
  if (!answer) {
    return false;
  }
  
	$('#pleaseWait').show();
	DataDwr.deletePreferences( 
		function(response) {
			if (response == 'OK') {
				$('#infoText').html("All the Preferences are deleted!");
				$('#info').show();
			} else {
				alert(response);
			}

			$('#pleaseWait').hide();
		}
	);  
}
</script>