<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> 
  &gt; <a href="CompareEIMPToEIBOX">Compare EIMP to EIBOX</a> 
  &gt; Compare Eimp to Eibox
</div>


<h1>Compare EIMP to EIBOX</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<br/>
If you want to comapre Stubhub Web with Stubhub Feed or Ticket Network with Ticket Network Feed data click <a href="CompareFeedToCrawl">here</a>
<br/>


<form method="GET" action="CompareEIMPToEIBOX">
	<table>
		<tr>
			<td>
				<b>Filter Events:</b>
			</td>
			<td >
				<input name="eventFilter" id="eventFilter" type="text" style="width: 240px" value="${eventFilter.name}" />
			</td>
		</tr>
		<tr>
			<td >
				<b>Event Date :</b><fmt:formatDate value="${eventFilter.localDate}" type="date" dateStyle="long"/>
			</td>
			
			<td>
				<b>Event Time :</b><fmt:formatDate value="${eventFilter.localDateTime}" type="time" timeStyle="long"/>
			</td>
			
		</tr>
	</table>
</form>
<br/>
<br/>
<br/>
<b>Missing Tickets in EIBOX:</b> ${eiboxMissingTicketCount}
<br/>
<b>Missing Tickets in EIMP:</b> ${eimpMissingTicketCount}
<br/>
<br/>
<div align="center">
<display:table class="list" name="${tickets}" id="ticket" requestURI="CompareEIMPAndEIBOX" pagesize="50">
	<display:column title="Section"  sortable="true" property="section"/>
	<display:column title="Row"  sortable="true" property="row"/>
    <display:column title="Quantity"  sortable="true" property="quantity"/>
    <display:column title="Seat"  sortable="true" property="seat" />
	<display:column title="Current Price"  sortable="true" property="price" />
	<display:column title="MissingInEIBOX"  sortable="true">
		<c:choose>
			<c:when test="${ticket.EIBOXmissing==true}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title="MissingInEIMP"  sortable="true">
	<c:choose>
			<c:when test="${ticket.EIMPmissing==true}">
				Yes
			</c:when>
			<c:otherwise>
				No
			</c:otherwise>
		</c:choose>
	</display:column>

</display:table>
</div>

<br/>


<script type="text/javascript">
	var eventId = 0;
	var tourId = 0;
	$('#eventFilter').autocomplete("GetAutoCompleteEvents", {
  		width: 550,
		max: 1000,
		minChars: 2,
		formatItem:function(row,i,max){
			return "<div  class='searchEventTag'>EVENT</div>" + row[0] + " - <font color='#943400'>" +  formatDate(new Date(row[1]*1)) + "</font> - " + row[2] ;
		}
	});
$('#eventFilter').result(function(event,row,formatted){
	document.location.href="CompareEIMPAndEIBOX?eventId="+row[2];
	return;
});
</script>
