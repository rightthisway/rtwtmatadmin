<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEbay">
  <a href="..">Home</a>
  &gt; eBay
</div>

<h1>Ebay Download</h1>
<b>Select:</b>
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a> |
<a href="#" onclick="invertAll(); return false">Invert</a>
<br/>

<form id="ebayDownloadForm" method="POST" action="EbayDownload">

<input type="hidden" name="action" value="download"/>

<c:forEach var="eventContainer" items="${eventContainers}">
  <h3>${eventContainer.event.name} (<fmt:formatDate pattern="MM/dd/yyyy" value="${eventContainer.event.localDate}"/> <fmt:formatDate pattern="hh:mm aa" value="${eventContainer.event.localTime}"/>) @ ${eventContainer.venue.building}, ${eventContainer.venue.city}, ${eventContainer.venue.state}, ${eventContainer.venue.country}</h3>
<!--
  <b>Event:</b> ${eventContainer.event.name}<br/>
  <b>Date:</b> <fmt:formatDate pattern="MM/dd/yyyy" value="${eventContainer.event.localDate}"/><br/>
  <b>Time:</b> <fmt:formatDate pattern="hh:mm aa" value="${eventContainer.event.localTime}"/><br/>
  <b>Venue:</b> ${eventContainer.venue.building}, ${eventContainer.venue.city}, ${eventContainer.venue.state}, ${eventContainer.venue.country}
-->  
  <display:table class="list" name="${eventContainer.ticketGroups}" id="ticketGroup">
    <display:column>
      <input type="checkbox" class="ticket_checkbox" name="group_${ticketGroup.id}" checked/>
    </display:column>
    <display:column title="Rem. Qty" sortable="true" property="ticket1.remainingQuantity" sortName="remainingQuantity" class="text-align-center" />
    <display:column title="Zone" sortable="true" property="ticket1.category.symbol" sortName="ticket1.category.symbol" class="text-align-center" />
    <display:column title="Section" sortable="true" sortName="ticket1.section" class="text-align-center">
      ${ticketGroup.ticket3.section}<br/>
      ${ticketGroup.ticket2.section}<br/>
      ${ticketGroup.ticket1.section}<br/>
    </display:column>
    <display:column title="Row" sortable="true" sortName="ticket1.row" class="text-align-center">
      ${ticketGroup.ticket3.row}<br/>
      ${ticketGroup.ticket2.row}<br/>
      ${ticketGroup.ticket1.row}<br/>
    </display:column>
    <display:column title="Wholesale" sortable="true" sortName="ticket1.wholesalePrice" class="text-align-center">
      $<fmt:formatNumber value="${ticketGroup.ticket3.currentPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket2.currentPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket1.currentPrice}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="Online" sortable="true" sortName="ticket1.adjustedCurrentPrice" format="{0,number,currency}" class="text-align-center">
      $<fmt:formatNumber value="${ticketGroup.ticket3.adjustedCurrentPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket2.adjustedCurrentPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket1.adjustedCurrentPrice}" type="currency" pattern="0.00"/>     
    </display:column>
    <display:column title="Title" sortable="true" sortName="title" class="text-align-center">
		<c:choose>
		    <c:when test="${empty ticketGroup.ticket3.title}">
		    &nbsp;
		    </c:when>
			<c:when test="${fn:length(ticketGroup.ticket3.title) > 42}">
				<span tooltip="${ticketGroup.ticket3.title}" style="white-space:nowrap">
					${fn:substring(ticketGroup.ticket3.title, 0,  42)}...
				</span>
			</c:when>
			<c:otherwise>
				${ticketGroup.ticket3.title}
			</c:otherwise>
		</c:choose>
		<br/>
		<c:choose>
		    <c:when test="${empty ticketGroup.ticket2.title}">
		    &nbsp;
		    </c:when>
			<c:when test="${fn:length(ticketGroup.ticket2.title) > 42}">
				<span tooltip="${ticketGroup.ticket2.title}" style="white-space:nowrap">
					${fn:substring(ticketGroup.ticket2.title, 0,  42)}...
				</span>
			</c:when>
			<c:otherwise>
				${ticketGroup.ticket2.title}
			</c:otherwise>
		</c:choose>
		<br/>
		<c:choose>
		    <c:when test="${empty ticketGroup.ticket1.title}">
		    &nbsp;
		    </c:when>
			<c:when test="${fn:length(ticketGroup.ticket1.title) > 42}">
				<span tooltip="${ticketGroup.ticket1.title}" style="white-space:nowrap">
					${fn:substring(ticketGroup.ticket1.title, 0,  42)}...
				</span>
			</c:when>
			<c:otherwise>
				${ticketGroup.ticket1.title}
			</c:otherwise>
		</c:choose>
    </display:column>
    <display:column title="Seller" sortable="true" sortName="siteId" class="text-align-left">
      <img src="../images/ico-${ticketGroup.ticket3.siteId}.gif" align="absbottom" tooltip="${ticketGroup.ticket3.siteId}"/> ${ticketGroup.ticket3.seller}<br/>
      <img src="../images/ico-${ticketGroup.ticket2.siteId}.gif" align="absbottom" tooltip="${ticketGroup.ticket2.siteId}"/> ${ticketGroup.ticket2.seller}<br/>
      <img src="../images/ico-${ticketGroup.ticket1.siteId}.gif" align="absbottom" tooltip="${ticketGroup.ticket1.siteId}"/> ${ticketGroup.ticket1.seller}
    </display:column>
    <display:column title="RPT" sortable="true" sortName="ticket1.rpt" class="text-align-center">
      $<fmt:formatNumber value="${ticketGroup.ticket3.rpt}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket2.rpt}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket1.rpt}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="Bin" sortable="true" sortName="ticket1.binPrice" format="{0,number,currency}" class="text-align-center">
      $<fmt:formatNumber value="${ticketGroup.ticket3.binPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket2.binPrice}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket1.binPrice}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="Net profit" sortable="true" sortName="ticket1.netProfit" format="{0,number,currency}" class="text-align-center">
      $<fmt:formatNumber value="${ticketGroup.ticket3.netProfit}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket2.netProfit}" type="currency" pattern="0.00"/><br/>
      $<fmt:formatNumber value="${ticketGroup.ticket1.netProfit}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="2Xp" sortable="true" sortName="ticket1.tg2" class="text-align-center">
       <br/><br/>
       $<fmt:formatNumber value="${ticketGroup.ticket1.tg2}" type="currency" pattern="0.00"/> 
    </display:column>    
    <display:column title="3Xp" sortable="true" sortName="ticket1.tg3" class="text-align-center">
       <br/><br/>
       $<fmt:formatNumber value="${ticketGroup.ticket1.tg3}" type="currency" pattern="0.00"/> 
    </display:column>
  </display:table>
  <br/><br/>
</c:forEach>

  <input type="submit" value="Download" class="medButton" />
</form>

<script type="text/javascript">
  function selectAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }
  
  function unselectAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }

  function invertAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      if ($(item).is(':checked')) {
        $(item).removeAttr('checked');
      } else {
        $(item).attr('checked', true);
      }
    });
  }
</script>