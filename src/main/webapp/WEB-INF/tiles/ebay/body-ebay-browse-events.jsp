<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:if test="${not empty info}">
<div class="info">
  <div class="infoText">
    ${info}
  </div>
</div>
</c:if>

<div id="breadCrumbPath" class="breadCrumbPathEbay">
  <a href="..">Home</a>
  &gt; eBay browse events
</div>

<div id="quickLinks">
  <c:if test="${tourId != 'ALL'}">
 	<a href="BrowseEvents?tourId=${tourId}">Browse Events</a>
	  <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	  |
	  <%-- </authz:authorize> --%>

  </c:if>
 <%--  <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	  <a href="AdminEbay">Admin</a>
  <%-- </authz:authorize> --%>

</div>
<div style="clear:both"></div>

<form id="browseEventsForm" action="EbayBrowseEvents" method="GET">
<input id="action" type="hidden" name="action" value="" />
Tour:
<select name="tourId" onchange="$('#browseEventsForm').submit()">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
</select>

Managed By: <img src="../images/ico-user.gif" align="absbottom" />
<select name="manager" onchange="$('#browseEventsForm').submit()">
  <c:forEach var="user" items="${users}">
    <option value="${user.username}" <c:if test="${user.username == manager}">selected</c:if>>${user.username}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${manager == 'ALL'}">selected</c:if>>ALL</option>
</select>
<br/>
<input id="hideNoPending" name="hideNoPending" type="checkbox" onchange="$('#browseEventsForm').submit()" <c:if test="${hideNoPending == 'on'}">checked</c:if>><label for="hideNoPending">Hide events with no pending groups</label>
<br/>

<b>Select:</b>
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<b>Action:</b>
<input type="button" class="medButton" style="width: 80px" value="Remove" onclick="removeSelectedEvents()"/> 
<br/>
<br/>

<display:table class="list" name="${ebayInventoryEvents}" id="ebayInventoryEvent" requestURI="EbayBrowseEvents?tourId=${tourId}&username=${username}" decorator="ebayEventTableDecorator">
  <c:set var="event" value="${ebayInventoryEvent.event}" />
  <c:set var="venue" value="${event.venue}" />
  <display:column>
    <input type="checkbox" class="event_checkbox" name="event_${ebayInventoryEvent.id}"/>
  </display:column>
  <c:if test="${tourId == 'ALL'}">
    <display:column title="Tour" sortable="true">${event.artist.name}</display:column>
  </c:if>
  <display:column title="Event" sortable="true">${event.name}</display:column>
  <display:column title="Date" sortable="true">
    <fmt:formatDate pattern="MM/dd/yy" value="${event.localDate}" />
    <c:choose>
      <c:when test="${event.localTime == null}">
        TBD
      </c:when>
      <c:otherwise>
        <fmt:formatDate pattern="HH:mm" value="${event.localTime}" />
      </c:otherwise>
    </c:choose>
  </display:column>
  <display:column title="Venue" sortable="true">
    ${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}    
  </display:column>
  <c:if test="${manager == 'ALL'}">
    <display:column title="User">
      <span style="white-space:nowrap"><img src="../images/ico-user.gif" align="absbottom" /> ${ebayInventoryEvent.username}</span>
    </display:column>
  </c:if>
  <display:column title="Active/Pending" sortable="true">
    ${ebayInventoryEvent.numActiveGroups}/${ebayInventoryEvent.numPendingGroups}
  </display:column>
  <display:column>
    <a href="EbayInventory?tourId=${event.artistId}&eventId=${event.id}&username=${ebayInventoryEvent.username}">View</a>
  </display:column>
</display:table>
</form>
<br/>
<img src="../images/ico-download.gif" align="absbottom"/> <a href="EbayDownload">Download full eBay Inventory (with invalid/deleted groups with price increased by ${invalidAddition}%)</a> 
<c:if test="${not empty lastDownloadDate}">
  | last downloaded on ${lastDownloadDate} by <img src="../images/ico-user.gif" align="absbottom"/> ${lastDownloadUsername}
</c:if>
<br/>
<img src="../images/ico-reload.gif" align="absbottom" /> <a href="#" onclick="forceRefresh(); return false">Force Refresh</a> (Last refreshed on ${lastRefresh})<br/>

<script type="text/javascript">
  function forceRefresh() {
    $('#action').val("refresh");
    $('#browseEventsForm').submit();
  }
  
  function selectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }

  function unselectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }
  
  function removeSelectedEvents() {
    if ($('.event_checkbox:checked').length == 0) {
      alert("Please select at least one event");
      return;
    }
    
    var answer = confirm("Are you sure you want to remove the selected events?");
    if (answer) {
       $('#action').val('remove');
       $('#browseEventsForm').submit();
    }
  }
</script>