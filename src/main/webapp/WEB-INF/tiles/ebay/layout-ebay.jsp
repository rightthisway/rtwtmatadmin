<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="ebaySubMenu">
  <c:choose>
    <c:when  test="${selectedSubMenu == 'Browse Events'}">
      <b>Browse Events</b>
    </c:when>
    <c:otherwise>
      <a href="EbayBrowseEvents">Browse Events</a>
    </c:otherwise>
  </c:choose>
  | 
  <c:choose>
    <c:when  test="${selectedSubMenu == 'Browse Inventories'}">
      <b>Browse Inventories</b>
    </c:when>
    <c:otherwise>
      <a href="EbayInventory">Browse Inventories</a>
    </c:otherwise>
  </c:choose>
</div>

<tiles:insertAttribute name="subBody" />