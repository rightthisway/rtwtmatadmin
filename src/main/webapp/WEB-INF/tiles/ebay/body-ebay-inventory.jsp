<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEbay">
  <a href="..">Home</a>
  &gt; eBay inventory
</div>

<div id="quickLinks">
  <c:if test="${tourId != 'ALL'}">
 	<a href="BrowseEvents?tourId=${tourId}">Browse Events</a>
    <c:if test="${eventId != 'ALL'}">
	  | <a href="BrowseTickets?eventId=${eventId}">Browse Tickets</a>
	</c:if>
  </c:if>
  <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	  | <a href="AdminEbay">Admin</a>
  <%-- </authz:authorize> --%>
</div>
<div style="clear:both"></div>

<c:if test="${not empty error || not empty param.error}">
	<div id="error" class="error">
	  <div id="errorText" class="errorText">
	    ${error} ${param.error}
	  </div>
	</div>
</c:if>

<div id="preview" style="position:fixed;right:10px;top:10px;background:white;border: solid 2px #009900;display:none">
</div>

<div id="info" class="info" <c:if test="${empty info && empty param.info}">style="display:none"</c:if>>
  <div id="infoText" class="infoText">
    ${info} ${param.info}
  </div>
</div>

<form id="eventSelector" method="GET" action="EbayInventory" onsubmit="$('#eventSubmit').hide(); $('#loadingSpan').show()">
<input id="eventSelectorAction" type="hidden" name="action" value="" />
Tour: 
<select name="tourId">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tourId == tour.id}">selected</c:if>>${tour.name}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${tourId == 'ALL'}">selected</c:if>>ALL</option>
</select>

<c:choose>
  <c:when test="${tourId == 'ALL'}">
    <c:set var="maxTitleChars" value="10"/>
  </c:when>
  <c:otherwise>
    <c:set var="maxTitleChars" value="22"/>
  </c:otherwise>
</c:choose>

<c:if test="${tourId != 'ALL'}">
Event: 
<select name="eventId" id="eventIdSelect"">
  <c:forEach var="event" items="${events}">
    <option value="${event.id}" <c:if test="${eventId == event.id}">selected</c:if>>${event.name} - <fmt:formatDate value="${event.localDate}" pattern="MM/dd/yyyy" />
      <c:choose>
        <c:when test="${not empty event.localTime}">
          <fmt:formatDate value="${event.localTime}" pattern="HH:mm" />
        </c:when>
        <c:otherwise>
          TBD
        </c:otherwise>
      </c:choose>
      @ ${venueMap[event.venueId].building}
    </option>
  </c:forEach>
  <option value="ALL" <c:if test="${eventId == 'ALL'}">selected</c:if>>ALL</option>
</select>
<c:if test="${eventId != 'ALL'}"> 
</c:if> 
</c:if>
 Managed by: <img src="../images/ico-user.gif" align="absbottom"/> 
 
<select name="username">
  <c:forEach var="user" items="${users}">
    <option value="${user.username}" <c:if test="${user.username == username}">selected</c:if>>${user.username}</option>
  </c:forEach>
  <option value="ALL" <c:if test="${username == 'ALL'}">selected</c:if>>ALL</option>
</select>
<input id="eventSubmit" type="submit" value="GO" class="medButton"/>
<span id="loadingSpan" style="display:none">
  <img src="../images/process-running.gif" align="absbottom"/> Loading...
</span>

<br/>
<input type="checkbox" id="showRemovedGroup" name="showRemovedGroup" <c:if test="${showRemovedGroup == 'on'}">checked</c:if>/> <label for="showRemovedGroup">Show removed ticket groups</label> &nbsp;&nbsp;&nbsp; <input type="checkbox" id="hideValidGroup" name="hideValidGroup" <c:if test="${hideValidGroup == 'on'}">checked</c:if> /> <label for="hideValidGroup">Hide valid ticket groups</label>

<table width="100%">
<tr>
<td valign="top" width="50%">
<c:if test="${not empty eventId && not empty tourId && eventId != 'ALL' && tourId != 'ALL'}">
  <h3>Tour</h3>
  <b>Map:</b> 
  <span id="addTourNotesSpan">
  	  <c:choose>
	    <c:when test="${empty tourNotes}">
	     <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addTourNotesSpan').hide(); $('#editTourNotesDiv').show();return false;">Add</a>
	    </c:when>
	    <c:otherwise>
  		  <span id="tourNotesSpan" style="white-space:nowrap;color:#666666">
  		    ${tourNotes}
	      </span>
		  &nbsp; <i>(zone map <c:choose><c:when test="enableEventZoneMaps">enabled</c:when><c:otherwise>disabled</c:otherwise></c:choose>)</i> [<a href="#" id="tourNotesSpanPreview">preview</a>] <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addTourNotesSpan').hide(); $('#editTourNotesDiv').show();return false;">Edit</a>
		  <script type="text/javascript">
			  $('#tourNotesSpanPreview').mouseover(function() {
			    $('#preview').html('<b>Click map for larger view</b><br /><img src="${imageDirUrl}/${tourNotes}.gif" width="200px"/>');
			    $('#preview').show();
			  });
			  
			  $('#tourNotesSpanPreview').mouseleave(function() {
			    $('#preview').html('');
			    $('#preview').hide();
			  });
			</script>
	    </c:otherwise>
	 </c:choose>
  </span>
  <span id="editTourNotesDiv" style="display:none">
    <input name="tourNotes" style="width:100px" value="${tourNotes}" /><br/>
    <input id="enableTourZoneMaps" type="checkbox" name="enableTourZoneMaps" <c:if test="${enableTourZoneMaps}">checked</c:if>/> <label for="enableTourZoneMaps">enable zone maps</label>
    <br/>
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeNotes'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#addTourNotesSpan').show(); $('#editTourNotesDiv').hide()" />
    <br/>
  </span><br/>
  <b>Exposure:</b>
  <select id="tourExposureSelect" name="tourExposure" onchange="$('#updateTourExposureButtons').show()">
    <c:forEach var="xp" begin="1" end="3">
      <option value="${xp}" <c:if test="${xp == tourExposure}">selected</c:if>>${xp}xp</option>
    </c:forEach> 
  </select>
  <span id="updateTourExposureButtons" style="display:none">
    <input id="updateTourExposureButton"  type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('updateTourExposure'); $('#eventSelector').submit()"/>
    <input type="button" class="medButton" value="Cancel" onclick="$('#updateTourExposureButtons').hide(); $('#tourExposureSelect').val(${tourExposure})"/>
  </span>
</td>
<td valign="top">
  <h3>Event</h3>
  <b>Map: </b>
  <span id="addEventNotesSpan">
  	  <c:choose>
	    <c:when test="${empty eventNotes}">
	     <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addEventNotesSpan').hide(); $('#editEventNotesDiv').show();return false;">Add</a>
	    </c:when>
	    <c:otherwise>
  		  <span id="eventNotesSpan" style="white-space:nowrap;color:#666666">
  		    ${eventNotes}
	      </span>
		  &nbsp; <i>(zone map <c:choose><c:when test="enableEventZoneMaps">enabled</c:when><c:otherwise>disabled</c:otherwise></c:choose>)</i> [<a href="#" id="eventNotesSpanPreview">preview</a>] <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#addEventNotesSpan').hide(); $('#editEventNotesDiv').show();return false;">Edit</a>
		  <script type="text/javascript">
			  $('#eventNotesSpanPreview').mouseover(function() {
			    $('#preview').html('<b>Click map for larger view</b><br /><img src="${imageDirUrl}/${eventNotes}.gif" width="200px"/>');
			    $('#preview').show();
			  });
			  
			  $('#eventNotesSpanPreview').mouseleave(function() {
			    $('#preview').html('');
			    $('#preview').hide();
			  });
		  </script>
	    </c:otherwise>
	 </c:choose>
  </span>
  <span id="editEventNotesDiv" style="display:none">
    <input name="eventNotes" style="width:100px" value="${eventNotes}" /> <br/>
    <input id="enableEventZoneMaps" type="checkbox" name="enableEventZoneMaps" <c:if test="${enableEventZoneMaps}">checked</c:if>/> <label for="enableEventZoneMaps">enable zone maps</label>
    <br/>
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeNotes'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#addEventNotesSpan').show(); $('#editEventNotesDiv').hide()" />
    <br/>
  </span><br/>
  <b>Exposure:</b>
  <select id="eventExposureSelect" name="eventExposure" onchange="$('#updateEventExposureButtons').show()">
    <c:forEach var="xp" begin="1" end="3">
      <option value="${xp}" <c:if test="${xp == eventExposure}">selected</c:if>>${xp}xp</option>
    </c:forEach> 
  </select>
  <span id="updateEventExposureButtons" style="display:none">
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('updateEventExposure'); $('#eventSelector').submit()"/>
    <input type="button" class="medButton" value="Cancel" onclick="$('#updateEventExposureButtons').hide(); $('#eventExposureSelect').val(${eventExposure})"/>
  </span><br/>
  <b>RPT Factor: </b>
  <span id="rptFactorSpan">
    ${rptFactor}% &nbsp; <img src="../images/ico-edit.gif" align="absbottom"/> <a href="#" onclick="$('#rptFactorSpan').hide(); $('#editRptFactorSpan').show();return false;">Edit</a> 
  </span>
  <span id="editRptFactorSpan" style="display:none">
    <input type="text" size="4" name="rptFactor" value="${rptFactor}"/>%<i>(percentage to add to the online price, e.g, 25)</i>
    &nbsp;&nbsp;
    <input type="button" class="medButton" value="Update" onclick="$('#eventSelectorAction').val('changeRptFactor'); $('#eventSelector').submit()"/> &nbsp;
    <input type="button" class="medButton" value="Cancel" onclick="$('#rptFactorSpan').show(); $('#editRptFactorSpan').hide()" />
  </span><br/>
  <br/>
</c:if>
</td></tr></table>
</form>

<h1>Ebay Inventory</h1>

<form id="ticketGroupManagerForm" method="POST" action="EbayInventory">

<b>Legends: </b>
<c:if test="${hideValidGroup != 'on'}">
  <span class="validEbayInventoryGroupRow" style="border: solid 1px #000000; padding-left: 8px;padding-right: 8px"></span> &nbsp; <span tooltip="Groups for which tickets still exists">Valid groups</span> &nbsp;&nbsp;
</c:if>
<c:if test="${showRemovedGroup == 'on'}">
  <span class="disabledEbayInventoryGroupRow" style="border: solid 1px #000000; padding-left: 8px;padding-right: 8px"></span> &nbsp; <span tooltip="Groups deleted by a market maker">Removed groups</span> &nbsp;&nbsp;
</c:if>
<span class="invalidEbayInventoryGroupRow" style="border: solid 1px #000000; padding-left: 8px;padding-right: 8px"></span> &nbsp; <span tooltip="Groups that have been detected to invalid because tickets are sold/expired">Invalid groups</span> &nbsp;&nbsp;
<span class="suggestedEbayInventoryGroupRow" style="border: solid 1px #000000; padding-left: 8px;padding-right: 8px"></span> &nbsp; <span tooltip="Groups found by the system which are suggested to be added">Suggested to be added</span> &nbsp;&nbsp;
<span class="replacementEbayInventoryGroupRow" style="border: solid 1px #000000; padding-left: 8px;padding-right: 8px"></span> &nbsp; <span tooltip="Groups suggested to replace an existing groups (found to be cheaper or to replace an invalid group)">Suggested replacement</span> &nbsp;&nbsp;
<br/><br/>

<b>Select:</b>
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a> |
<a href="#" onclick="selectInvalid(); return false">Invalid groups</a> |
<a href="#" onclick="selectSuggested(); return false">Suggested groups</a> |
<a href="#" onclick="selectReplacement(); return false">Suggested replacements</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<b>Action:</b>
<input type="button" class="medButton" style="width: 80px" value="Remove" onclick="removeSelectedGroups()"/> 
&nbsp;<input type="button" class="medButton" style="width: 80px" value="Add" onclick="addSelectedGroups()"/>
<br/>
<br/>

  <input type="hidden" name="username" value="${username}"/>
  <input type="hidden" name="tourId" value="${tourId}"/>
  <input type="hidden" name="eventId" value="${eventId}"/>
  <input type="hidden" name="showRemovedGroup" value="${showRemovedGroup}"/>
  <input type="hidden" name="hideValidGroup" value="${hideValidGroup}"/>
  <input type="hidden" id="actionInput" name="action" value=""/>
  
  <display:table class="list" name="${ebayInventoryGroups}" id="ebayInventoryGroup" requestURI="EbayInventory?tourId=${tourId}&eventId=${eventId}&username=${username}" decorator="ebayGroupTableDecorator">
    <c:set var="ebayInventory1" value="${ebayInventoryGroup.ebayInventory1}" />
    <c:set var="ebayInventory2" value="${ebayInventoryGroup.ebayInventory2}" />
    <c:set var="ebayInventory3" value="${ebayInventoryGroup.ebayInventory3}" />
    
    <c:set var="ticket1" value="${ebayInventory1.ticket}" />
    <c:set var="ticket2" value="${ebayInventory2.ticket}" />
    <c:set var="ticket3" value="${ebayInventory3.ticket}" />

    <c:set var="ebayInventoryEvent" value="${ebayInventoryGroup.ebayInventoryEvent}" />
    <c:set var="event" value="${eventMap[ebayInventoryEvent.eventId]}" />
    <c:set var="tour" value="${tourMap[event.artistId]}" />
    <c:set var="venue" value="${venueMap[event.venueId]}" />
    
    <display:column>
      <input type="checkbox" class="ticket_checkbox" name="group_${ebayInventoryGroup.id}"/>
      <c:if test="${ebayInventoryGroup.replacement}">
        <c:set var="replacedEbayInventoryGroup" value="${ebayInventoryGroup.replacedEbayInventoryGroup}" /><br/>
          <c:if test="${replacedEbayInventoryGroup.status != 'DELETED'}">
	        <span tooltip="Replacement for group_id: ${replacedEbayInventoryGroup.id} / rpt: $${replacedEbayInventoryGroup.ebayInventory1.rpt} / bin: <b>$${replacedEbayInventoryGroup.ebayInventory1.binPrice}</b> / net: $<fmt:formatNumber pattern="0.00" value="${replacedEbayInventoryGroup.ebayInventory1.netProfit}"/>"><a href="#" onclick="return false;">(R)</a></span>
          </c:if>
      </c:if>
    </display:column>
    
    <c:if test="${tourId == 'ALL'}">
      <display:column title="Tour">
        <a href="BrowseEvents?tourId=${event.artistId}">
          <c:set var="tourName" value="${tour.name}"/>
      	  <c:choose>
		    <c:when test="${empty tourName}">
		    &nbsp;
		    </c:when>
			<c:when test="${fn:length(tourName) > 16}">
				<span tooltip="${tourName}" style="white-space:nowrap">
					${fn:substring(tourName, 0,  16)}...
				</span>
			</c:when>
			<c:otherwise>
				${tourName}
			</c:otherwise>
		 </c:choose>
       </a>
      </display:column>
    </c:if>
    
    <c:if test="${tourId == 'ALL' || eventId == 'ALL'}">
      <display:column title="Event">
		<a href="BrowseTickets?eventId=${event.id}">
		  <fmt:formatDate value="${event.localDate}" pattern="MM/dd/yy" />
	      <c:choose>
	        <c:when test="${not empty event.localTime}">
	          <fmt:formatDate value="${event.localTime}" pattern="HH:mm" />
	        </c:when>
	        <c:otherwise>
	          TBD
	        </c:otherwise>
	      </c:choose>
		</a>  
		<br/>
		<c:set var="venueStr" value="${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}" />
		<c:choose>
		    <c:when test="${empty venueStr}">
		    &nbsp;
		    </c:when>
			<c:when test="${fn:length(venueStr) > 12}">
				<span tooltip="${venueStr}" style="white-space:nowrap">
					${fn:substring(venueStr, 0,  12)}...
				</span>
			</c:when>
			<c:otherwise>
				${venueStr}
			</c:otherwise>
		</c:choose>
		<br/>
		<img src="../images/ico-user.gif" align="absbottom"/> ${ebayInventoryEvent.username} 
      </display:column>
    </c:if>
    <display:column title="Qty" sortable="true" property="quantity" sortName="remainingQuantity" class="text-align-center" />
    <display:column title="Zone" sortable="true" property="category" sortName="ebayInventoryGroup.category" class="text-align-center" />
    <display:column title="Section" sortable="true" sortName="ticket1.section" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        ${ticket3.section}<br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        ${ticket2.section}<br/>
      </c:if>
      ${ticket1.section}
    </display:column>
    <display:column title="Row" sortable="true" sortName="ticket1.row" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        ${ticket3.row}<br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        ${ticket2.row}<br/>
      </c:if>
      ${ticket1.row}
    </display:column>
    <display:column title="Tix Qty">
      <c:if test="${not empty ebayInventory3}">
        ${ticket3.remainingQuantity}<br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        ${ticket2.remainingQuantity}<br/>
      </c:if>
      ${ticket1.remainingQuantity}<br/>    	
    </display:column>
    <display:column title="Online" sortable="true" sortName="ebayInventory1.onlinePrice" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        $<fmt:formatNumber value="${ebayInventory3.onlinePrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        $<fmt:formatNumber value="${ebayInventory2.onlinePrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      $<fmt:formatNumber value="${ebayInventory1.onlinePrice}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="Wholesale" sortable="true" sortName="ticket1.adjustedCurrentPrice" format="{0,number,currency}" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        $<fmt:formatNumber value="${ebayInventory3.wholesalePrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        $<fmt:formatNumber value="${ebayInventory2.wholesalePrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      $<fmt:formatNumber value="${ebayInventory1.wholesalePrice}" type="currency" pattern="0.00"/>     
    </display:column>
    <display:column title="Seller" sortable="true" sortName="siteId" class="text-align-left">
      <c:if test="${not empty ebayInventory3}">
        <span style="white-space:nowrap"><img src="../images/ico-${ticket3.siteId}.gif" align="absbottom" tooltip="${ticket3.siteId}"/> <a href="RedirectToItemPage?id=${ticket3.id}" target="_BLANK">${ticket3.seller}</a></span><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        <span style="white-space:nowrap"><img src="../images/ico-${ticket2.siteId}.gif" align="absbottom" tooltip="${ticket2.siteId}"/> <a href="RedirectToItemPage?id=${ticket2.id}" target="_BLANK">${ticket2.seller}</a></span><br/>
      </c:if>
      <span style="white-space:nowrap"><img src="../images/ico-${ticket1.siteId}.gif" align="absbottom" tooltip="${ticket1.siteId}"/> <a href="RedirectToItemPage?id=${ticket1.id}" target="_BLANK">${ticket1.seller}</a></span>
    </display:column>
    <display:column title="RPT" sortable="true" sortName="ticket1.rpt" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        $<fmt:formatNumber value="${ebayInventory3.rpt}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        $<fmt:formatNumber value="${ebayInventory2.rpt}" type="currency" pattern="0.00"/><br/>
      </c:if>
      $<fmt:formatNumber value="${ebayInventory1.rpt}" type="currency" pattern="0.00"/> 
    </display:column>
    <display:column title="Bin" sortable="true" sortName="ticket1.binPrice" format="{0,number,currency}" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        $<fmt:formatNumber value="${ebayInventory3.binPrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        $<fmt:formatNumber value="${ebayInventory2.binPrice}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <b>$<fmt:formatNumber value="${ebayInventory1.binPrice}" type="currency" pattern="0.00"/></b> 
    </display:column>
    <display:column title="Net profit" sortable="true" sortName="ticket1.netProfit" format="{0,number,currency}" class="text-align-center">
      <c:if test="${not empty ebayInventory3}">
        $<fmt:formatNumber value="${ebayInventory3.netProfit}" type="currency" pattern="0.00"/><br/>
      </c:if>
      <c:if test="${not empty ebayInventory2}">
        $<fmt:formatNumber value="${ebayInventory2.netProfit}" type="currency" pattern="0.00"/><br/>
      </c:if>
      $<fmt:formatNumber value="${ebayInventory1.netProfit}" type="currency" pattern="0.00"/> 
    </display:column>
    <c:if test="${not empty ebayInventory2}">
      <display:column title="2Xp" sortable="true" sortName="ticket1.tg2" class="text-align-center">
       <c:if test="${not empty ebayInventory3}">
         <br/>
       </c:if>
       <br/>
       $<fmt:formatNumber value="${ebayInventoryGroup.xp2}" type="currency" pattern="0.00"/> 
      </display:column>    
    </c:if>
    <c:if test="${not empty ebayInventory3}">
      <display:column title="3Xp" sortable="true" sortName="xp3" class="text-align-center">
       <br/><br/>
       $<fmt:formatNumber value="${ebayInventoryGroup.xp3}" type="currency" pattern="0.00"/> 
      </display:column>
    </c:if>
  </display:table>
  <br/>
  ${fn:length(ebayInventoryGroups)} entries.
<br/><br/>
<img src="../images/ico-download.gif" align="absbottom"/> <a href="EbayDownload">Download full eBay Inventory (with invalid/deleted groups with price increased by ${invalidAddition}%)</a> 
<c:if test="${not empty lastDownloadDate}">
  | last downloaded on ${lastDownloadDate} by <img src="../images/ico-user.gif" align="absbottom"/> ${lastDownloadUsername}
</c:if>
<br/>
<c:if test="${not empty eventId && not empty tourId && eventId != 'ALL' && tourId != 'ALL'}">
  <img src="../images/ico-delete.gif" align="absbottom" /> <a href="#" onclick="deleteEvent(); return false">Delete Event From Ebay Inventory</a><br/>
  <img src="../images/ico-edit.gif" align="absbottom"/> This event is currently managed by: <img src="../images/ico-user.gif" align="absbottom" />
  <select name="manager" onchange="$('#actionInput').val('changeManager');$('#ticketGroupManagerForm').submit()">
    <c:forEach var="user" items="${users}">
      <option value="${user.username}" <c:if test="${user.username == manager}">selected</c:if>>${user.username}</option>
    </c:forEach>
  </select>
  <br/>
</c:if>
<img src="../images/ico-reload.gif" align="absbottom" /> <a href="#" onclick="forceRefresh(); return false">Force Refresh</a> (Last refreshed on ${lastRefresh})<br/>


<c:if test="${not empty eventId && not empty tourId && eventId != 'ALL' && tourId != 'ALL'}">
<h2>Tests</h2>
<a href="EbayTestInventory?tourId=${tourId}&eventId=${eventId}&username=${username}" >Compute groups</a><br/>
<!--
<a href="EbayUpload?tourId=${tourId}&eventId=${eventId}&username=${username}" >Upload</a>
-->
</c:if>
<script type="text/javascript">

  function selectAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }

  function selectInvalid() {
    $.each($('.ticket_checkbox'), function(index, item) {
      if ($($(item).parent().parent()).hasClass('invalidEbayInventoryGroupRow')) {
        $(item).attr('checked', true);
      } else {
        $(item).removeAttr('checked');
      }
    });
  }

  function selectSuggested() {
    $.each($('.ticket_checkbox'), function(index, item) {
      if ($($(item).parent().parent()).hasClass('suggestedEbayInventoryGroupRow')) {
        $(item).attr('checked', true);
      } else {
        $(item).removeAttr('checked');
      }
    });
  }

  function selectReplacement() {
    $.each($('.ticket_checkbox'), function(index, item) {
      if ($($(item).parent().parent()).hasClass('replacementEbayInventoryGroupRow')) {
        $(item).attr('checked', true);
      } else {
        $(item).removeAttr('checked');
      }
    });
  }
  
  function unselectAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }

  function invertAll() {
    $.each($('.ticket_checkbox'), function(index, item) {
      if ($(item).is(':checked')) {
        $(item).removeAttr('checked');
      } else {
        $(item).attr('checked', true);
      }
    });
  }
  
  function removeSelectedGroups() {
    if ($('.ticket_checkbox:checked').length == 0) {
      alert('Please select at least a ticket group');
      return false;
    }

	var answer = confirm("Are you sure you want to remove the selected groups?");
	if (!answer) {
	  return false;
	}

    $('#actionInput').val("remove");
    $('#ticketGroupManagerForm').submit();
  }
  
  function addSelectedGroups() {
    if ($('.ticket_checkbox:checked').length == 0) {
      alert('Please select at least a ticket group');
      return false;
    }

	var answer = confirm("Are you sure you want to add the selected groups?");
	if (!answer) {
	  return false;
	}

    $('#actionInput').val("add");
    $('#ticketGroupManagerForm').submit();
  }

  function forceRefresh() {
    $('#actionInput').val("refresh");
    $('#ticketGroupManagerForm').submit();
  }
  
  function deleteEvent() {
    var answer = confirm("Are you sure you want to delete the event from the eBay inventory?");
    if (answer) {
      location.href = 'EbayInventory?tourId=${tourId}&eventId=${eventId}&action=deleteEvent';
    }
    
    return false;
  }
</script>