<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<h1>Artist/Team Price Adjustment</h1>
<form action="EditArtistPriceAdjustment" method="post" onsubmit="return checkArguments();">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="artistId" value="${artist.id}" />
	<input type="hidden" name="siteId" value="${param.siteId}" />
	
	<table>
		<tr>
			<td>Artist/Team</td>
			<td>${artist.name}</td>
			<td></td>
		</tr>
		<tr>
			<td>Site</td>
			<td>${param.siteId}</td>
			<td></td>
		</tr>
		<tr>
			<c:choose>
				<c:when test="${empty artistPriceAdjustment.percentAdjustment}">
					<c:set var="artistPriceAdjustment" value="0.00" />
				</c:when>
				<c:otherwise>
					<c:set var="artistPriceAdjustment" value="${artistPriceAdjustment.percentAdjustment}" />
				</c:otherwise>
			</c:choose>

			<td>
				Artist/Team Price Adjustment
				<small><i>(was <a href="javascript:$('#artistPriceAdjustment').val('<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${artistPriceAdjustment}" />');void(0)"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${artistPriceAdjustment}" />%)</a></i></small>				
			</td>
			<td>
				<input type="text" id="artistPriceAdjustment" name="artistPriceAdjustment" value="<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${artistPriceAdjustment}" />" style="width: 60px;" />%
				<c:choose>
					<c:when test="${empty site || site.priceRounding == 'NONE'}">
					</c:when>								
					<c:when test="${site.priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
					</c:when>
					<c:when test="${site.priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
					</c:when>
				</c:choose>
				
			</td>
			<td>
				<span style="color:red" id="artistPriceAdjustment_error"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="javascript:updateTourPriceAdjustments()"><small style="font-size: 10px;">Set all tour price adjustments to this value</small></a>
				-
				<a href="javascript:updateTourAndEventPriceAdjustments()"><small style="font-size: 10px;">Set all tour and event price adjustments to this value</small></a>
			</td>
		</tr>

		<tr>
			<td colspan="2"><br /><b>Tour Price Adjustments</b><br /></td>
		</tr>
		<c:forEach var="tour" items="${artist.tours}">
			<tr>
				<c:choose>
					<c:when test="${empty tourPriceAdjustments[tour.id].percentAdjustment}">
						<c:set var="percentAdjustment" value="0.00" />
					</c:when>
					<c:otherwise>
						<c:set var="percentAdjustment" value="${tourPriceAdjustments[tour.id].percentAdjustment}" />
					</c:otherwise>
				</c:choose>

				<td>
					<b>${tour.name}</b>
					<small><i>(was <a href="javascript:$('#tourPriceAdjustment_${tour.id}').val('<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />');void(0)"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />%</a>)</i></small>
				</td>
				<td>
					<input type="text" id="tourPriceAdjustment_${tour.id}" name="tourPriceAdjustment_${tour.id}" class="tourPriceAdjustment" value='<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />'  style="width: 60px;" />%
					<c:choose>
						<c:when test="${empty site || site.priceRounding == 'NONE'}">
						</c:when>								
						<c:when test="${site.priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
						</c:when>
						<c:when test="${site.priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
						</c:when>
					</c:choose>
					
				</td>
				<td>				
					<span style="color:red" id="tourPriceAdjustment_${tour.id}_error"></span>
				</td>
			</tr>			
			<tr>			
				<td colspan="2">
					<a href="javascript:updateEventPriceAdjustments('${tour.id}')"><small style="font-size: 10px;">Set all event price adjustments to this value</small></a>
				</td>
			</tr>			
			<c:forEach var="event" items="${tour.events}">
				<c:choose>
					<c:when test="${empty eventPriceAdjustments[event.id].percentAdjustment}">
						<c:set var="percentAdjustment" value="0.00" />
					</c:when>
					<c:otherwise>
						<c:set var="percentAdjustment" value="${eventPriceAdjustments[event.id].percentAdjustment}" />
					</c:otherwise>
				</c:choose>
				<tr>
					<td>
						<li>
							<fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />			
							<small><i>(was <a href="javascript:$('#eventPriceAdjustment_${event.id}').val('<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />');void(0)"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />%</a>)</i></small>
						</li>
					</td>
					<td>
						<input type="text" id="eventPriceAdjustment_${event.id}" name="eventPriceAdjustment_${event.id}" class="eventPriceAdjustment eventInTour${tour.id}" value='<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />'  style="width: 60px;" />%
						<c:choose>
							<c:when test="${empty site || site.priceRounding == 'NONE'}">
							</c:when>								
							<c:when test="${site.priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
								<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
							</c:when>
							<c:when test="${site.priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
								<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
							</c:when>
						</c:choose>
						
					</td>
					<td>				
						<span style="color:red" id="eventPriceAdjustment_${event.id}_error"></span>
					</td>
				</tr>
			</c:forEach>
		</c:forEach>
	</table>

	<input type="submit" class="medbutton" value="Save" />
	<input type="button" class="medbutton" value="Cancel" onclick="document.location.href='BrowseTours?artistId=${param.artistId}'" />

</form>

<script type="text/javascript">
	function updateTourPriceAdjustments() {
		var percentValue = formatFloat($('#artistPriceAdjustment').val());
		$('#artistPriceAdjustment').val("" + percentValue);
		$('.tourPriceAdjustment').val("" + percentValue);
	};

	function updateTourAndEventPriceAdjustments() {
		var percentValue = formatFloat($('#artistPriceAdjustment').val());
		$('#artistPriceAdjustment').val("" + percentValue);
		$('.tourPriceAdjustment').val("" + percentValue);
		$('.eventPriceAdjustment').val("" + percentValue);
	};
	
	function updateEventPriceAdjustments(tourId) {
		var percentValue = formatFloat($('#tourPriceAdjustment_' + tourId).val());
		$('#tourPriceAdjustment_' + tourId).val("" + percentValue);	
		$('.eventInTour' + tourId).val("" + percentValue);	
	};
	
	function checkArguments() {
	
		var error = false;

		if ($.trim($('#artistPriceAdjustment').val()) == "" || isNaN($('#artistPriceAdjustment').val() * 1)) {
			$('#artistPriceAdjustment_error').text("Invalid price adjustment for artist");
			error = true;
		}

		for (var i = 0 ; i < $('.tourPriceAdjustment').length ; i++) {
			var elt = $('.tourPriceAdjustment').get(i);
			if ($.trim($(elt).val()) == "" || isNaN($(elt).val() * 1)) {
				var tourId = $(elt).attr('id').split("_")[1]; 				
				$('#tourPriceAdjustment_' + tourId + '_error').text("Invalid price adjustment " + $(elt).val() + " for tour.");
				error = true;
			}			
		}
		
		for (var i = 0 ; i < $('.eventPriceAdjustment').length ; i++) {
			var elt = $('.eventPriceAdjustment').get(i);
			if ($.trim($(elt).val()) == "" || isNaN($(elt).val() * 1)) {
				var eventId = $(elt).attr('id').split("_")[1]; 				
				$('#eventPriceAdjustment_' + eventId + '_error').text("Invalid price adjustment " + $(elt).val() + " for event.");
				error = true;
			}			
		}
		return !error;
	};
</script>