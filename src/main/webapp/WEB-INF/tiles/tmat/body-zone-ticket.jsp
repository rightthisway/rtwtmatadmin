<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<%@page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseShorts">Browse Shorts</a>
  	&gt; Browse Broadcast 
</div>

<div id="quickLinks">
  <c:if test="${not empty eventId}">
    <a href="BrowseTickets?eventId=${eventId}">Browse Tickets</a> |
    <a href="TMATBrowseShorts?eventId=${eventId}">Browse Shorts</a> |
    <a href="EditorEditCategories?tourId=${event.artist.id}">Browse Zones</a>
  </c:if>
</div>
<div style="clear:both"></div>
<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate pattern="yyyy" value="${now}" var="currentYear" />

<fmt:formatDate pattern="yyyy" value="${fromDate}" var="fromYear" />
<fmt:formatDate pattern="M" value="${fromDate}" var="fromMonth" />
<fmt:formatDate pattern="d" value="${fromDate}" var="fromDay" />

<fmt:formatDate pattern="yyyy" value="${toDate}" var="toYear" />
<fmt:formatDate pattern="M" value="${toDate}" var="toMonth" />
<fmt:formatDate pattern="d" value="${toDate}" var="toDay" />
<h1>Zone Ticket</h1>
<form id="browseZoneForm" action="TMATZoneTicket" method="Post">
<table>
<tr>
<td>
<input id="tourName" type="hidden" name="tourName" value="${zoneTour}" />
Search By Tour:  Check  All: <input type="checkbox" name="isAllTour" <c:if test="${isAllTourSelected == 'true'}">checked</c:if> id="isAllTour" value="true" />
Uncheck  All: <input type="checkbox" name="isUncheckAllTour" id="isUncheckAllTour"/>
</td>
<td></td>
<td>
Search By Venue:  Check  All: <input type="checkbox" name="isAllVenue" <c:if test="${isAllVenueSelected == 'true'}">checked</c:if> id="isAllVenue" value="true" />
Uncheck  All: <input type="checkbox" name="isUncheckAllVenue" id="isUncheckAllVenue"/>
</td>
</tr>
<tr><td>
<select name="tourName" id="tourSelect" style="width: 400px" multiple size="10" >
  <c:forEach var="tour" items="${tours}">
  <c:set var="a1" value="false" />
  <c:forEach var="selected" items="${tourSelected}">
  <c:if test="${tour == selected}">
  <c:set var="a1" value="true" /> 
  </c:if>  
  </c:forEach>
  <option value="${tour}" <c:if test="${a1 =='true'}">selected</c:if>>${tour}</option>
  </c:forEach>
</select> 
</td><td>Or</td><td> 
<select name="venue" id="venueSelect" style="width: 400px" multiple size="10">
  <c:forEach var="venue" items="${venues}">
  <c:set var="a2" value="false" />  
  <c:forEach var="selected" items="${venueSelected}">
  <c:if test="${venue == selected}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
	<option value="${venue}" <c:if test="${a2 == 'true'}">selected</c:if>>${venue}</option>
  </c:forEach>
</select>  
</td>
</tr>
</table>
<br /><br/>
From Date: 
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="${currentYear}" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> /
<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDay}">selected</c:if>>${day}</option>
  </c:forEach>
</select>
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />

     To: 

<select name="toYear" id="toYear">
  <c:forEach var="year" begin="2009" end="${currentYear}" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> /
<select name="toMonth" id="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDay}">selected</c:if>>${day}</option>
  </c:forEach>
</select>
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
<br/><br/>
Day: 
<select name="day" id="daySelect">
<option value="ALL" <c:if test="${daySelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="day" items="${days}">
    <option value="${day}" <c:if test="${daySelected == day}">selected</c:if>>${day}</option>
  </c:forEach>
</select>
Quantity: 
<select name="quantity" id="quantitySelect">
<option value="ALL" <c:if test="${quantitySelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="quantity" items="${quantities}">
    <option value="${quantity}" <c:if test="${quantitySelected == quantity}">selected</c:if>>${quantity}</option>
  </c:forEach>
</select>
Zone: 
<select name="zone" id="zoneSelect">
<option value="ALL" <c:if test="${zoneSelected == 'ALL'}">selected</c:if>>ALL</option>
  <c:forEach var="zone" items="${zones}">
    <option value="${zone}" <c:if test="${zoneSelected == zone}">selected</c:if>>${zone}</option>
  </c:forEach>
</select>

	Broadcast Filter:<select id="showOnWeb" name="showOnWeb">
   		<option value="">ALL</option>
   		<option value="Y" <c:if test="${showOnWeb == 'Y'}">selected</c:if>>Y</option>
   		<option value="N" <c:if test="${showOnWeb == 'N'}">selected</c:if>>N</option>
 	</select> 
	<br/><br/>
	<input id="eventSelectorAction" type="hidden" name="action" value="" /> 
 <input type="button" class="medButton" value="View" onclick="$('#eventSelectorAction').val('search'); $('#browseZoneForm').submit()"/> &nbsp;<br/>

	


<br />
<c:if test="${fn:length(zoneTickets) > 0}"><br/><b>Total Number Of Records Found: <c:out value="${fn:length(zoneTickets)}"/></b></c:if><br/>
<display:table class="list" name="${zoneTickets}" id="zonesTicket" requestURI="TMATZoneTicket?tourName==${zoneTour}" export="true" >
<display:setProperty name="export.banner"><div id="exportTypes"><span class="label">Export:</span> {0}</div></display:setProperty>
   <display:setProperty name="export.csv.filename">CSV</display:setProperty>
	<display:setProperty name="export.types">csv</display:setProperty>   
	<display:setProperty name="export.csv.filename" value="TMATZoneTickets.csv"/>
	<display:column title="Event"  sortable="true" >${zonesTicket.event_name}</display:column>
	<display:column title="Venue" media="html" >
	<img title="${zonesTicket.venue_name}" id="show-venue-image" src="../images/question.jpg" />
	</display:column>
	<display:column title="Date/Time" >${zonesTicket.event_date}</display:column>
	<display:column title="Day" sortable="true">${zonesTicket.day_of_week}</display:column>
	<display:column title="Quantity" sortable="true">${zonesTicket.ticket}</display:column>
	<display:column title="Zone" sortable="true">${zonesTicket.zone}</display:column>
	<display:column title="Your Price" sortable="true">${zonesTicket.your_price}</display:column>
	<display:column title="Lowest Price" sortable="true">${zonesTicket.lowest_price}</display:column>
	<display:column title="Broadcast" sortable="true">${zonesTicket.will_show}</display:column>
	<display:setProperty name="export.banner.placement" value="top" />
</display:table>
<c:if test="${empty zoneTickets}">
	No Broadcasts Selected or None Found
</c:if>
</form>
<script type="text/javascript">
$(document).ready(function() {


    $("#isAllTour").click(function() {
	$("#venueSelect").each(function(){
            $("#venueSelect option").removeAttr("selected"); });
	$('#isUncheckAllTour').attr('checked', false);
	$('#isAllVenue').attr('checked', false);
	$('#isUncheckAllVenue').attr('checked', false);
	if((document.getElementById("isAllTour").checked)){
			$("#tourSelect").each(function(){
            $("#tourSelect option").attr("selected","selected"); });
			}else{
			
			$("#tourSelect").each(function(){
            $("#tourSelect option").removeAttr("selected"); });
			}
    }) ; 
$("#isAllVenue").click(function() {
$("#tourSelect").each(function(){
            $("#tourSelect option").removeAttr("selected"); });
$('#isAllTour').attr('checked', false);
$('#isUncheckAllTour').attr('checked', false);
$('#isUncheckAllVenue').attr('checked', false);
	if((document.getElementById("isAllVenue").checked)){
			$("#venueSelect").each(function(){
            $("#venueSelect option").attr("selected","selected"); });
			}else{
			
			$("#venueSelect").each(function(){
            $("#venueSelect option").removeAttr("selected"); });
			}
    }) ; 
	$("#isUncheckAllVenue").click(function() {
	if((document.getElementById("isUncheckAllVenue").checked)){
	$('#isAllTour').attr('checked', false);
	$('#isUncheckAllTour').attr('checked', false);
	$('#isAllVenue').attr('checked', false);
	$("#venueSelect").each(function(){
            $("#venueSelect option").removeAttr("selected"); });
	}
	 }) ;
	 $("#isUncheckAllTour").click(function() {
	if((document.getElementById("isUncheckAllTour").checked)){
	$('#isAllTour').attr('checked', false);
	$("#tourSelect").each(function(){
            $("#tourSelect option").removeAttr("selected"); });
	}
	 }) ;
	$("#tourSelect").click(function() {
		$('#isAllTour').attr('checked', false);
		$('#isUncheckAllTour').attr('checked', false);
		$('#isAllVenue').attr('checked', false);
		$('#isUncheckAllVenue').attr('checked', false);
		$("#venueSelect").each(function(){
            $("#venueSelect option").removeAttr("selected"); });
	 }) ;
	 $("#venueSelect").click(function() {
		$('#isAllTour').attr('checked', false);
		$('#isUncheckAllTour').attr('checked', false);
		$('#isAllVenue').attr('checked', false);
		$('#isUncheckAllVenue').attr('checked', false);
		$("#tourSelect").each(function(){
            $("#tourSelect option").removeAttr("selected"); });
	 }) ;
		$('#daySelect').change(function(){ 
		$('#browseZoneForm').submit();
		});
		$('#quantitySelect').change(function(){ 
		$('#browseZoneForm').submit();
		});
		/*$('#zoneSelect').change(function(){ 
		$('#browseZoneForm').submit();
		});*/

$('.calendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
	        maxDate: new Date((new Date()).getTime())
		});
		
		$('#fromCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
	        }
	    );

		$('#fromCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#fromYear').val(tokens[0]*1);
				$('#fromMonth').val(parseInt(tokens[1]*1));
				$('#fromDay').val(parseInt(tokens[2]*1));
			}
		);
		
		$('#toCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
	        }
	    );
		
		$('#toCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#toYear').val(tokens[0]*1);
				$('#toMonth').val(parseInt(tokens[1])*1);
				$('#toDay').val(parseInt(tokens[2])*1);
			}
		);	
 
});

	</script>

