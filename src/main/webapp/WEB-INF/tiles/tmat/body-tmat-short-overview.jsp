<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="TMATBrowseShorts">Browse Inventory</a>
  &gt; Browse Tour Inventory
</div>
<h1>Browse Tour Inventory</h1>

  <div id="tmatHeaderGoTo">
  	<span id="goToTextFieldTMATContainer">
  		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
  		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
  	</span>
  	<span id="tmatRedirectingContainer" style="display:none">
  		<div>					
  			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
  			<div id="tmatRedirectingPage" style="float:left"></div>
  		</div>
  	</span>
  	<c:if test="${param.eventId != 'ALL'}">
  	  or <a href="TMATShortOverview?eventId=ALL">Show All</a>
  	</c:if>
  </div>        
<div class="info">
	<div class="infoText">
		Only tours/seasons that have inventory are shown.
	</div>
</div> 

<table id="theTable" class="list" >
	<thead>
	<tr>
		<c:forTokens items="Event Name,Date,Covered,Total Qty,Exposure,GM%,PGM%" var="field" delims=",">
			<th>
				${field}
			</th>
		</c:forTokens>
	</tr>	
	</thead>

<%--
TOUR LEVEL
--%>
	<c:forEach var="status" items="${statuses}">
		<tbody>
			<tr>
				<td>
					<a onclick="toggleStatusTBody(${status.tourId}, ${tourToEventMap[status.tourId]})" style="cursor: pointer">
						<img id="event-expand-img-${status.tourId}" src="../images/ico-expand.gif" />
						${status.description}
					</a>  					
				</td>
				<td>
				${status.date}
				</td>
				<td>
				${status.qtyCovered}
				</td>
				<td>
				${status.qtyOpen}
				</td>
				<td>
				<fmt:formatNumber value="${status.exposure}" type="currency" />
				</td>
				<td>
				<fmt:formatNumber value="${status.grossMargin}" pattern=".##" />
				</td>	

				<c:choose>
						<c:when test="${status.projGrossMargin > .25}">
							<td style="background-color:green">
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" />
							</td>
						</c:when>
					<c:otherwise>
					<c:choose> 
						<c:when test="${status.projGrossMargin > .19}"> 
							<td style="background-color:#99ff66"> 
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" /> 
							</td> 
						</c:when> 
					<c:otherwise>
					<c:choose> 
						<c:when test="${status.projGrossMargin > .15}"> 
							<td style="background-color:yellow"> 
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" /> 
							</td> 
						</c:when> 
					<c:otherwise>
					<c:choose> 
						<c:when test="${status.projGrossMargin > .10}"> 
							<td style="background-color:#ffffcc"> 
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" /> 
							</td> 
						</c:when> 
					<c:otherwise>
					<c:choose> 
						<c:when test="${status.projGrossMargin >= 0}"> 
							<td> 
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" /> 
							</td> 
						</c:when> 
					<c:otherwise>
					<c:choose>  
						<c:when test="${status.projGrossMargin > -.11}">  
							<td style="background-color:#ff66ff">
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" />
							</td>
						</c:when>  
					<c:otherwise>
					<c:choose> 
						<c:when test="${status.projGrossMargin > -.26}"> 
							<td style="background-color:#ff3366">
							<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" />
							</td>
						</c:when> 
					<c:otherwise>
						<td style="background-color:red">
						<fmt:formatNumber value="${status.projGrossMargin}" pattern=".##" />
						</td>
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
					</c:choose> 
					</c:otherwise>
				</c:choose>

			</tr>
		</tbody>

<%--
EVENT LEVEL
--%>

		<c:forEach var="statusChild" items="${status.children}">		
			<tbody id="status-tbody-${statusChild.eventId}" style="display:none">
				<tr>
					<td>
					<img id="event-shorts-img-${statusChild.eventId}" src="../images/ico_arrow_right.gif" /><a href="TMATBrowseShorts?eventId=${statusChild.eventId}">${statusChild.description} (Browse Shorts)</a>					
					</td>
					<td>
					${statusChild.date}
					</td>
					<td>
					${statusChild.qtyCovered}
					</td>
					<td>
					${statusChild.qtyOpen}
					</td>
					<td>
					<fmt:formatNumber value="${statusChild.exposure}" type="currency" />
					</td>
					<td>
					<fmt:formatNumber value="${statusChild.grossMargin}" pattern=".##" />
					</td>
					
					<c:choose>
							<c:when test="${statusChild.projGrossMargin > .25}">
								<td style="background-color:green">
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" />
								</td>
							</c:when>
						<c:otherwise>
						<c:choose> 
							<c:when test="${statusChild.projGrossMargin > .19}"> 
								<td style="background-color:#99ff66"> 
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" /> 
								</td> 
							</c:when> 
						<c:otherwise>
						<c:choose> 
							<c:when test="${statusChild.projGrossMargin > .15}"> 
								<td style="background-color:yellow"> 
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" /> 
								</td> 
							</c:when> 
						<c:otherwise>
						<c:choose> 
							<c:when test="${statusChild.projGrossMargin > .10}"> 
								<td style="background-color:#ffffcc"> 
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" /> 
								</td> 
							</c:when> 
						<c:otherwise>
						<c:choose> 
							<c:when test="${statusChild.projGrossMargin >= 0}"> 
								<td> 
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" /> 
								</td> 
							</c:when> 
						<c:otherwise>
						<c:choose>  
							<c:when test="${statusChild.projGrossMargin > -.11}">  
								<td style="background-color:#ff66ff">
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" />
								</td>
							</c:when>  
						<c:otherwise>
						<c:choose> 
							<c:when test="${statusChild.projGrossMargin > -.26}"> 
								<td style="background-color:#ff3366">
								<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" />
								</td>
							</c:when> 
						<c:otherwise>
							<td style="background-color:red">
							<fmt:formatNumber value="${statusChild.projGrossMargin}" pattern=".##" />
							</td>
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
						</c:choose> 
						</c:otherwise>
					</c:choose>
						
				</tr>
			</tbody>	
		</c:forEach>
	</c:forEach>
</table>

<br /><br />

<script type="text/javascript">

  //
  // Nested Status Display
  //
  
	function toggleStatusTBody(tourId, eIds) {
		$.each(eIds, function(j, eId) {
			if ($('#status-tbody-' + eId).css('display') == 'none') {
				$('#status-tbody-' + eId).show();
				$('#event-expand-img-' + tourId).attr('src', '../images/ico-collapse.gif');
			} else {
				$('#status-tbody-' + eId).hide();
				$('#event-expand-img-' + tourId).attr('src', '../images/ico-expand.gif');
			}
		});
	};
	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Sold');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Sold');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (row[3] != "null" || row[4] != "null") {
				 	tourRow += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
  	
  	$('#tmatRedirectingPage').html(pageDescription);
  	
  	if (row[0] == "ARTIST") {
  		document.location.href = "TMATBrowseEvents?artistId=" + row[2];
  		return;
  	} else if (row[0] == "TOUR") {
  		document.location.href = "TMATShortOverview?tourId=" + row[2];
  		return;
  	} else if (row[0] == "EVENT") {
  		document.location.href = "TMATBrowseShorts?eventId=" + row[2];
  		return;
  	}
  });
</script>

