<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseShorts">Browse Shorts</a>
  	&gt; Browse Broadcast 
</div>

<div id="quickLinks">
  <c:if test="${not empty eventId}">
    <a href="BrowseTickets?eventId=${eventId}">Browse Tickets</a> |
    <a href="TMATBrowseShorts?eventId=${eventId}">Browse Shorts</a> |
    <a href="EditorEditCategories?tourId=${event.artist.id}">Browse Zones</a>
  </c:if>
</div>
<div style="clear:both"></div>

<h1>Browse Broadcast</h1>

<div id="tmatHeaderGoTo">
  	<span id="goToTextFieldTMATContainer">
  		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
  		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
  	</span>
  	<span id="tmatRedirectingContainer" style="display:none">
  		<div>					
  			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
  			<div id="tmatRedirectingPage" style="float:left"></div>
  		</div>
  	</span>
  	<c:if test="${param.eventId != 'ALL'}">
  	  or <a href="TMATBrowseBroadcast?eventId=ALL">Browse ALL</a>
  	</c:if> 
  </div>        
  
<div class="info">
	<div class="infoText">
		These are the shorts that are displayed for sale by AdmitOne, but have not yet been sold.
		<br/>
		THESE NUMBERS ARE NOT 100% ACCURATE
	</div>
</div> 
<c:if test="${not empty eventId}">
	<c:if test="${eventId != 'ALL'}">
	<!-- 
		Zone Group:
		<select id="catScheme">
			<c:forEach var="catg" items="${catGroups}">
				<option value="${catg}">${catg}</option>
			</c:forEach>
		</select><br/><br/>  -->
	</c:if>
</c:if>

<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />

<table id="statusTable" class="list">
	<thead>
		<tr>		
			<th>Name</th>
			<th>venue</th>	
			<th>Potential Exposure</th>
			<th>Avg Price</th>
			<th>Broadcast Tickets</th>
			<c:forEach var="sid" items="${constants.siteIds}">
			    <c:set var="safeSiteId" value="[${sid}]" />
			    <c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
					<th>
						<img src="../images/ico-${sid}.gif" border="0" tooltip="${sid}"/>
					</th>
				</c:if>
			</c:forEach>
		</tr>	
	</thead>
	
	<%--
		EVENT LEVEL 
	--%>
	<c:forEach var="eventStatus" items="${eventStatuses}">
		<tbody>
			<tr class="eventRow">
				<%-- Name --%>
				<td>
					<a onclick="toggleCastTBody(${eventStatus.eventId})" style="cursor: pointer">
						<img id="event-expand-img-${eventStatus.eventId}" class="event-expand-img" src="../images/ico-expand.gif" />
						${eventStatus.eventName}, ${eventStatus.date}
					</a>  					
				</td>
				<td><img title="${eventStatus.venue}" id="show-venue-image" src="../images/question.jpg" /></td>
				<%-- Exposure --%>
				<td align="right"><fmt:formatNumber value="${eventStatus.exposure}" type="currency" /></td>
				<%-- Avg Price --%>
				<td align="right"><fmt:formatNumber value="${eventStatus.price}" type="currency" /></td>
				<%-- Broadcast Tickets --%>
				<td align="right">${eventStatus.quantity}</td>
				<%-- site --%>
				<c:forEach var="sid" items="${constants.siteIds}">
				    <c:set var="safeSiteId" value="[${sid}]" />
				    <c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
						<td>
							<c:choose><c:when test="${eventStatus.totalQtyBySite[sid] == 0 || eventStatus.qtyBySite[sid]==0}">-</c:when><c:otherwise>${eventStatus.qtyBySite[sid]} / ${eventStatus.totalQtyBySite[sid]}</c:otherwise></c:choose>
						</td>
					</c:if>
				</c:forEach>
			</tr>
		</tbody>
		
		<%-- 
			CATEGORY LEVEL 
		--%>		
		<c:forEach var="categoryStatus" items="${eventStatus.children}">		
			<tbody class="status-child status-child-tbody-${eventStatus.eventId}" style="display:none">
				<tr class="catRow">
					<td>
						<img src="../images/ico-branch.gif" />
						<a onclick="toggleCastTDoubleBody(${eventStatus.eventId},${categoryStatus.categoryId})" style="cursor: pointer">
							<img id="event-expand-img-${eventStatus.eventId}-${categoryStatus.categoryId}" class="category-expand-img" src="../images/ico-expand.gif" />
							${categoryStatus.groupName}
						</a>  					
					</td>
					<td>&nbsp;</td>
					<td align="right"><fmt:formatNumber value="${categoryStatus.exposure}" type="currency" /></td>
					<td align="right"><fmt:formatNumber value="${categoryStatus.price}" type="currency" /></td>
					<td align="right">${categoryStatus.quantity}</td>
					<c:forEach var="sid" items="${constants.siteIds}">
					    <c:set var="safeSiteId" value="[${sid}]" />
					    <c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
							<td>							
								<c:choose><c:when test="${categoryStatus.totalQtyBySite[sid] == 0 || categoryStatus.qtyBySite[sid]==0}">-</c:when><c:otherwise>${categoryStatus.qtyBySite[sid]} / ${categoryStatus.totalQtyBySite[sid]}</c:otherwise></c:choose> 
							</td>
						</c:if>
					</c:forEach>
				</tr>
			</tbody>	
			
			<%--
				SHORT BROADCAST LEVEL
			--%>					
			<tbody id="status-tbody-${eventStatus.eventId}-${categoryStatus.categoryId}" class="status-grandchild status-child-tbody-${eventStatus.eventId}" style="display:none" >
				<c:forEach var="ticketGroupStatus" items="${categoryStatus.children}">	
					<tr>
						<td>
							<img src="../images/ico-branch.gif" />
							<img src="../images/ico-branch.gif" />
							${ticketGroupStatus.groupName}
						</td>
						<td>&nbsp;</td>
						<td align="right"><fmt:formatNumber value="${ticketGroupStatus.exposure}" type="currency" /></td>
						<td align="right"><fmt:formatNumber value="${ticketGroupStatus.price}" type="currency" /></td>
						<td align="right">${ticketGroupStatus.quantity}</td>
						<c:forEach var="sid" items="${constants.siteIds}">
						    <c:set var="safeSiteId" value="[${sid}]" />
						    <c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
								<td>
									<c:choose><c:when test="${ticketGroupStatus.totalQtyBySite[sid] == 0}">-</c:when><c:otherwise>${ticketGroupStatus.qtyBySite[sid]} / ${ticketGroupStatus.totalQtyBySite[sid]}</c:otherwise></c:choose>
								</td>
							</c:if>
						</c:forEach>
					</tr>
				</c:forEach>
			</tbody>
		</c:forEach>
	</c:forEach>
</table>
	
<c:if test="${empty eventStatuses}">
	No Broadcasts Selected or None Found
</c:if>
	  
<%--	  
<h2>Upload Short Broadcasts</h2>
<div class="info">
	<div class="infoText">
		The CSV file should be in the following format:<br/>
		eventId, pricePerTicket, section, row, quantity<br/>
		<b>Important Note:</b> The first line will be skipped.
	</div>
</div>

<c:if test="${not empty error}">
	<div class="error">
		<div class="errorText">${error}</div>
	</div>
</c:if>
<br />
<form enctype="multipart/form-data" action="EditorUploadShortBroadcastCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
<b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" value="Upload"/>
</form>
<br />
<c:if test="${not empty eventId}">
	<a href="TMATBrowseShorts?eventId=${eventId}">Return to Browse Shorts</a> 
</c:if>

--%>
<script type="text/javascript">

	function changeGroup() {
	  var catGroup = $.trim($('#catScheme').val());;
	  if(${tourId != null}) {
	  	document.location.href = "TMATShortBroadcast?tourId=" + ${tourId} + "&catScheme=" + catGroup;
	  }
	  if(${eventId != null}) {
	  	document.location.href = "TMATShortBroadcast?eventId=" + ${eventId} + "&catScheme=" + catGroup;
	  }
	  if(${artistId != null}) {
	  	document.location.href = "TMATShortBroadcast?artistId=" + ${artistId} + "&catScheme=" + catGroup;
	  }
	};
    
	function toggleCastTBody(eId) {
		if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId).show();
			
			$.each($('.status-child-tbody-' + eId), function(i, elt) {
				if ($(elt).hasClass("status-child")) {
					$(elt).show();
					$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif')
				} else {
					$(elt).hide();
				}
				
			});
			$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId).hide();
			$('.status-child-tbody-' + eId).hide();
			$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
		}
	};
	    
	function toggleCastTDoubleBody(eId,cId) {
		if ($('#event-expand-img-' + eId + '-' + cId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId + '-' + cId).show();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId + '-' + cId).hide();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-expand.gif');
		}
	};

	function expandAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.status-child').show();
		$('#statusTable').find('.status-grandchild').show();
	};

	function collapseAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.status-child').hide();
		$('#statusTable').find('.status-grandchild').hide();
	};

	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Broadcast');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Broadcast');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
  	
  	$('#tmatRedirectingPage').html(pageDescription);
  	
  	if (row[0] == "ARTIST") {
  		document.location.href = "TMATBrowseBroadcast?artistId=" + row[2];
  		return;
  	} else if (row[0] == "TOUR") {
  		document.location.href = "TMATBrowseBroadcast?tourId=" + row[2];
  		return;
  	} else if (row[0] == "EVENT") {
  		document.location.href = "TMATBrowseBroadcast?eventId=" + row[2];
  		return;
  	}
  	
  	
  });
</script>

