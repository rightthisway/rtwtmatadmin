<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<%@page contentType="text/html;charset=UTF-8"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseShorts">Browse Shorts</a>
  	&gt; Browse Broadcast 
</div>

<div id="quickLinks">
  <c:if test="${not empty eventId}">
    <a href="BrowseTickets?eventId=${eventId}">Browse Tickets</a> |
    <a href="TMATBrowseShorts?eventId=${eventId}">Browse Shorts</a> |
    <a href="EditorEditCategories?tourId=${event.artist.id}">Browse Zones</a>
  </c:if>
</div>
<div style="clear:both"></div>

<h1>AO Zone Ticket Analysis</h1>
<div>
<form method="get" action="TMATAOZoneTicket">
	Date: <input type="text" id="dateRange1" name="dateRange1" class="date-pick" style="width: 240px" value="${dateRange1}"/> to <input type="text" name="dateRange2" id="dateRange2" value="${dateRange2}" class="date-pick" style="width: 240px"/>
	<input id="changeGroupButton" type="submit" class="medButton" value="GET AOInventory"/><br/><br/>
</form>
</div>
<div id="tmatHeaderGoTo">
  	<span id="goToTextFieldTMATContainer">
  		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
  		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
  	</span>
  	<span id="tmatRedirectingContainer" style="display:none">
  		<div>					
  			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
  			<div id="tmatRedirectingPage" style="float:left"></div>
  		</div>
  	</span>
  	<c:if test="${param.eventId != 'ALL'}">
  	  or <a href="TMATAOZoneTicket?eventId=ALL">Browse ALL</a>
  	</c:if>
  </div>        
  
<div class="info">
	<div class="infoText">
		These are the shorts that are displayed for sale by AdmitOne, but have not yet been sold.
		<br/>
		THESE NUMBERS ARE NOT 100% ACCURATE
	</div>
</div> 
<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />

<table id="statusTable" class="list">
	<thead>
		<tr>		
			<th align="centre" width="200" class="header-left-align">Name</th>
			<th align="centre" width="80" >Venue</th>
			<th align="centre" width="80">Date</th>
			<th align="centre" width="80">Time</th>
			<th align="centre" width="80">Qty</th>
			<th align="centre" width="80">AO ZONE Price</th>
			<th align="centre" width="80">+/-</th>
			<th align="centre" width="80">Low Zone</th>
			<th align="centre" width="80">Low ESL</th>
			<th align="centre" width="80">Total tickets in CAT</th>
			<th align="centre" width="80">Tix <= AO Zone</th>
			<th align="centre" width="80">% Market</th>
			<th align="centre" width="80">% Market <= AO Price</th>
			<th align="centre" width="80">Days to Expiration</th>
		</tr>	
	</thead>
	
	<%--
		EVENT LEVEL 
	--%>
	<c:forEach var="aoZoneTicket" items="${aoZoneTickets}">
		<tbody>
			<tr class="eventRow">
				<%-- Name --%>
				<td class="content-left-align">
					<a onclick="toggleCastTBody(${aoZoneTicket.eventid})" style="cursor: pointer">
						<img id="event-expand-img-${aoZoneTicket.eventid}" class="event-expand-img" src="../images/ico-expand.gif" />
						<b><a href="BrowseTickets?eventId=${aoZoneTicket.eventid}">${aoZoneTicket.eventName}</a></b>
					</a>  					
				</td>
				<td align="centre"><img title="${aoZoneTicket.venue}" id="show-venue-image" src="../images/question.jpg" /></td>
				<td align="centre">${aoZoneTicket.date}</td>
				<td align="centre">${aoZoneTicket.time}</td>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
				<td/>
			</tr>
		</tbody>
		
		<c:forEach var="aotix" items="${aoZoneTicket.children}">		
			<tbody class="status-child status-child-tbody-${aoZoneTicket.eventid}" style="display:none">
				<tr class="catRow">
					<td>
						<img src="../images/ico-branch.gif" />
						${aotix.section}
					</td>
					<td align="centre"></td>
					<td align="centre"></td>
					<td align="centre"></td>
					<td align="centre">${aotix.quantity}</td>
					<td align="centre">${aotix.broadcastPrice}</td>
					<td align="centre">${aotix.PGM}</td>
					<td align="centre">${aotix.marketPrice}</td>
					<td align="centre">${aotix.lowESL}</td>
					<td align="centre">${aotix.tixInZone}</td>
					<td align="centre">${aotix.tixBelowAOZone}</td>
					<c:choose>
						<c:when test="${aotix.aoInfusedTix}">
							<td align="centre">-</td>
							<td align="centre">0</td>
						</c:when>
						<c:otherwise>
							<td align="centre">${aotix.marketPercentage}</td>
							<td align="centre">${aotix.marketPerBelowAO}</td>
						</c:otherwise>
					</c:choose>
					<td align="centre">${aotix.daysOfExpiration}</td>
				</tr>
			</tbody>	
		</c:forEach>
	</c:forEach>
</table>
	
<c:if test="${empty aoZoneTickets}">
	No Broadcasts Selected or None Found
</c:if>

<script type="text/javascript">

$(function() {
	
	var date1 = new Date(); 
	$('#dateRange1').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr1 = dateText;}});
	$('#dateRange2').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr2 = dateText;}});
});

	function changeGroup() {
	  var catGroup = $.trim($('#catScheme').val());;
	  if(${tourId != null}) {
	  	document.location.href = "TMATShortBroadcast?tourId=" + ${tourId} + "&catScheme=" + catGroup;
	  }
	  if(${eventId != null}) {
	  	document.location.href = "TMATShortBroadcast?eventId=" + ${eventId} + "&catScheme=" + catGroup;
	  }
	  if(${artistId != null}) {
	  	document.location.href = "TMATShortBroadcast?artistId=" + ${artistId} + "&catScheme=" + catGroup;
	  }
	};
    
	function toggleCastTBody(eId) {
		if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId).show();
			
			$.each($('.status-child-tbody-' + eId), function(i, elt) {
				if ($(elt).hasClass("status-child")) {
					$(elt).show();
					$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif')
				} else {
					$(elt).hide();
				}
				
			});
			$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId).hide();
			$('.status-child-tbody-' + eId).hide();
			$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
		}
	};
	    
	function toggleCastTDoubleBody(eId,cId) {
		if ($('#event-expand-img-' + eId + '-' + cId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId + '-' + cId).show();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId + '-' + cId).hide();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-expand.gif');
		}
	};

	function expandAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.status-child').show();
	};

	function collapseAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.status-child').hide();
	};

	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Broadcast');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Broadcast');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,	
		extraParams: {t:'tour&Events'},	
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
  	
  	$('#tmatRedirectingPage').html(pageDescription);
  	
  	if (row[0] == "ARTIST") {
  		document.location.href = "TMATAOZoneTicket?artistId=" + row[2];
  		return;
  	} else if (row[0] == "TOUR") {
  		document.location.href = "TMATAOZoneTicket?tourId=" + row[2];
  		return;
  	} else if (row[0] == "EVENT") {
  		document.location.href = "TMATAOZoneTicket?eventId=" + row[2];
  		return;
  	}
  	
  	
  });
</script>

