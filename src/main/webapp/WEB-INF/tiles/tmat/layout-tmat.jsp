<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="editorSubMenu">
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Browse Inventory'}">
   	    <b>Browse Inventory</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATBrowseShorts">Browse Inventory</a>
	  </c:otherwise>
	</c:choose>
	|
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Browse Uncategorized Short Inventory'}">
   	    <b>Browse Uncategorized Short Inventory</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATBrowseUncategorizedShorts">Browse Uncategorized Short Inventory</a>
	  </c:otherwise>
	</c:choose>
	|	
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Browse Sales'}">
   	    <b>Browse Sales</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATBrowseSales">Browse Sales</a>
	  </c:otherwise>
	</c:choose>
	|
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Broadcast Overview'}">
   	    <b>Broadcast Overview</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATBrowseBroadcast">Broadcast Overview</a>
	  </c:otherwise>
	</c:choose>
	|	
	<c:choose>
	  <c:when test="${selectedSubMenu == 'AO Zone Ticket'}">
   	    <b>AO Zone Ticket</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATAOZoneTicket">AO Zone Ticket</a>
	  </c:otherwise>
	</c:choose>
	|	
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Zone Ticket'}">
   	    <b>Zone Ticket</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="TMATZoneTicket">Zone Ticket</a>
	  </c:otherwise>
	</c:choose>
	
</div>

<tiles:insertAttribute name="subBody" />
