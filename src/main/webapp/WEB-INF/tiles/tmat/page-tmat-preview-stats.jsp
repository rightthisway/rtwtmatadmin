<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<display:table class="list" name="${stats}" id="stat" requestURI="BrowseShorts">
	<display:column title="Seller" sortable="false" property="siteId" />
	<display:column title="Qty" sortable="false" property="totalTicketCount" style="text-align:right" />
    <display:column title="Max Lot Size" sortable="false" property="maxQuantity" style="text-align:right" />
    <display:column title="Mean" sortable="false" property="average" format="{0,number,###,##0.00}" style="text-align:right" />
    <display:column title="Median" sortable="false" property="median" format="{0,number,###,##0.00}" style="text-align:right" />
    <display:column title="Low Price" sortable="false" property="minPrice" format="{0,number,###,##0.00}" style="text-align:right" />
    <display:column title="+/-" sortable="false" property="variance" format="{0,number,###,##0.00}" style="text-align:right" />
    <display:column title="Low Ticket" sortable="true">
        <a href="RedirectToItemPage?id=${stat.ticketId}">
        	<img src="../images/ico-${stat.siteId}.gif" />
        	Go to ticket on ${stat.siteId}
        </a>
    </display:column>
</display:table>