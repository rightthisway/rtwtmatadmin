<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
var dateRangeStr1 = null;
var dateRangeStr2 = null;


</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="TMATBrowseShorts">Browse Inventory</a>
	<c:choose>
		<c:when test="${eventId == 'ALL'}">
			&gt; Browse All Inventory
		</c:when>
		<c:when test="${not empty eventId}">
			&gt; <a href="TMATBrowseShorts?tourId=${event.artistId}">Browse Tour Inventory</a> 
			&gt; Browse Event Inventory
		</c:when>
		<c:otherwise>
			&gt; Browse Tour Inventory
		</c:otherwise>
	</c:choose>
</div>

<div id="quickLinks">
	<c:if test="${not empty eventId}">
		<a href="BrowseTickets?eventId=${eventId}">Browse Tickets</a> |
		<a href="TMATBrowseBroadcast?eventId=${eventId}">Browse Broadcast</a> |
		<a href="EditorEditCategories?tourId=${event.artist.id}">Browse Zones</a>
	</c:if>
</div>
<div style="clear:both"></div>


<c:choose>
	<c:when test="${eventId == 'ALL'}">
		<h1>Browse All Inventory</h1>
	</c:when>
	<c:when test="${not empty eventId}">
		<h1>Browse Event Inventory</h1>
	</c:when>
	<c:otherwise>
		<h1>Browse Tour Inventory</h1>
	</c:otherwise>
</c:choose>

Date: <input type="text" id="dateRange1" name="dateRange1" class="date-pick" val ="${fromDateRange}" style="width: 240px"/>
 to <input type="text" id="dateRange2" name="dateRange2" class="date-pick" val="${toDateRange}" style="width: 240px"/> 
<div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<input id="tourId" name ="tourId" type="hidden" value="${tourId}">
	<input id="eventId" name ="eventId" type="hidden" value="${eventId}">
	<input id="iraEventStatusId" name ="iraEventStatusId" type="hidden" value="${iraEventStatusId}">
	<span class="refreshLink" style="display:none">
	
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>			
	</span>
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>		
	</span>
</div>

<div style="clear: both;float: right;">
	<span>
		<span><u><b>Legends For PGM </b></u></span>
		<br>
		<br>
		White 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 : 0&nbsp;&nbsp; - &nbsp;&nbsp;0.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>|</b>
		Light Yellow : 0.11 &nbsp;-&nbsp; 0.15 
		<br>
		Yellow	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 0.16&nbsp;&nbsp; -&nbsp;&nbsp; 0.19
		<b>|</b>
		Light Green	 : 0.20 &nbsp;-&nbsp; 0.25
		<br>
		Green	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 :   >   0.25&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<b>|</b>
		Pink		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 0.0 &nbsp;&nbsp;-&nbsp;&nbsp; -0.11
		<br>
		Dark Pink	 : 0.12 &nbsp;&nbsp;-&nbsp;&nbsp; -0.26
		<b>|</b>
		Red	 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 :  >&nbsp;&nbsp;&nbsp;&nbsp;0.26  
	</span>
</div>

<br/>
<br/>

<div id="tmatHeaderGoTo">
	<b>Category Type: </b>
	<select id="selectCat">
		<option value="-">--Select--</option>
		<option value="" <c:if test="${catType==''}">selected</c:if>>All</option>
		<option value="cat" <c:if test="${catType=='cat'}">selected</c:if>>Categorized</option>
		<option value="uncat" <c:if test="${catType=='uncat'}">selected</c:if>>Uncategorized</option>
	</select>

	<b>Inventory Type: </b>
	<select id="selectInventory">
		<option value="-">--Select--</option>
		<option value="" <c:if test="${filter==''}">selected</c:if>>All</option>
		<option value="short" <c:if test="${filter=='short'}">selected</c:if>>Short</option>
		<option value="inventory" <c:if test="${filter=='inventory'}">selected</c:if>>Long Inventory</option>
	</select>
	<b>View : </b>
	<select id="selectView">
		<option value="html" <c:if test="${view=='html'}">selected</c:if>>HTML</option>
		<option value="csv" <c:if test="${view=='csv'}">selected</c:if>>CSV</option>
	</select>
	<br/><br/>
	<span id="goToTextFieldTMATContainer">
		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
	</span>
	<span id="tmatRedirectingContainer" style="display:none">
  		<div>
			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
			<div id="tmatRedirectingPage" style="float:left"></div>
		</div>
	</span>

	or <!--<a href="javascript: return null;" onclick="browseAllEvents();">Browse ALL</a> -->
	Parent Type:
	<select id="browseEvents" >
		<option value="">--Select--</option>
		<option value="sports" <c:if test='${eventType=="sports"}'>selected </c:if>>Browse All Sports</option>
		<option value="concerts" <c:if test='${eventType=="concerts"}'>selected </c:if>>Browse All Concerts</option>
		<option value="theaters" <c:if test='${eventType=="theaters"}'>selected </c:if>>Browse All Theaters</option>
		<option value="all" <c:if test='${eventType=="all"}'>selected </c:if> >Browse All</option>
	</select>
	<input id="changeGroupButton" type="button" class="medButton" onclick="browseAllEvents($('#browseEvents'));"  value="GET Inventory"/><br/><br/>
</div>        
<br/>
<div class="info">
	<div class="infoText">
		A seating zone is required for determining PGM%.<br/>
		If zones are not mapped for this event, the PGM% will always be 0.00.<br/>
		If zones are mapped for this event and the PGM is 0.00, that means TMAT could not find any tickets to fill.			
	</div>
</div> 
<br/>
<br/>

<a href="javascript:expandAll()"><img src="../images/ico-expand.gif" />Expand All</a> -
<a href="javascript:collapseAll()"><img src="../images/ico-collapse.gif" />Collapse All</a>
<br /><br />
<table id="statusTable" class="list">
<%-- <c:set var="qty" value="0"></c:set>
<c:set var="revenue" value="0"></c:set>
<c:set var="exposure" value="0"></c:set>
<c:set var="pgm" value="0"></c:set>
<c:set var="totalMarketPrice" value="0"></c:set>
<c:set var="marketPrice" value="0"></c:set>
<c:set var="marketMover" value="0"></c:set> --%>
	<thead>
		<tr>
			<th>Event Name</th>
			<th>Event Date</th>
			<th>Event Time</th>
			<th>Venue</th>		
			<th>Qty</th>
			<c:choose>
				<c:when test="${param.filter == 'short'}">
					<th>Revenue</th>				
				</c:when>
				<c:when test="${param.filter == 'inventory'}">
					<th>Cost</th>				
				</c:when>
				<c:otherwise>
					<th>Cost/Revenue</th>
				</c:otherwise>
			</c:choose>
			<th>Exposure</th>
			<th>PGM</th>
			<th>Total Market Price</th>
			<th>Market Price</th>
			<th>Market Mover</th>
			<th>Section</th>
			<th>Row</th>
			<th>Invoice</th>
			<th>Customer</th>
		</tr>	
	</thead>
<c:if test="${not empty eventStatuses }">
	<c:forEach var="eventStatus" items="${eventStatuses}">
		<%--
			EVENT STATUS LEVEL
		--%>
		<%-- <c:set var="qty" value="${qty + eventStatus.qtySold }"></c:set>
		<c:set var="revenue" value="${revenue + eventStatus.priceSold}" ></c:set>
		<c:set var="exposure" value="${exposure + eventStatus.exposure}"></c:set>
		<c:set var="pgm" value="${pgm + eventStatus.projGrossMargin}"></c:set>
		<c:set var="totalMarketPrice" value="${totalMarketPrice + eventStatus.totalMinPrice}"></c:set>
		<c:set var="marketPrice" value="${marketPrice + eventStatus.minPrice}"></c:set>
		<c:set var="marketMover" value="${marketMover + eventStatus.marketMover}"></c:set> --%>
		
		<tbody>
			<tr class="eventRow">
				<td style='text-align:left'>
					<a onclick="toggleStatusTBody(${eventStatus.eventId})" style="cursor: pointer">
						<img id="event-expand-img-${eventStatus.eventId}" class="event-expand-img" src="../images/ico-expand.gif" />
						${eventStatus.description}
					</a>  					
				</td>
				<td align="right">${eventStatus.date}</td>
				<td align="right">${eventStatus.time}</td>
				<td style='text-align:center'><c:if test="${eventStatus.venue != null}">
					<img title="${eventStatus.venue}" id="show-venue-image" src="../images/question.jpg"></c:if>
				</td>
				<td align="right">${eventStatus.qtySold}</td>
				<td align="right"><fmt:formatNumber value="${eventStatus.priceSold}" type="currency" /></td>
				<td align="right"><fmt:formatNumber value="${eventStatus.exposure}" type="currency" /></td>
				<td align="right" style="background-color:${ao:getMarginBackgroundColor(eventStatus.projGrossMargin)}">
					<span style='color:black'><fmt:formatNumber value="${eventStatus.projGrossMargin}" pattern="0.00" /></span>
				</td>
				<td align="right">
					<c:choose>
						<c:when test="${eventStatus.totalMinPrice > 0}">
							<fmt:formatNumber value="${eventStatus.totalMinPrice}" type="currency" />
						</c:when>
						<c:otherwise>
							-
						</c:otherwise>
					</c:choose>
				</td>
				
				<td align="right">
					<c:choose>
						<c:when test="${eventStatus.minPrice > 0}">
							<fmt:formatNumber value="${eventStatus.minPrice}" type="currency" />
						</c:when>
						<c:otherwise>
							-
						</c:otherwise>
					</c:choose>
				</td>
				<td align="right">
				<c:choose>
					<c:when test="${eventStatus.totalMarketMover > 0}">
						<fmt:formatNumber value="${eventStatus.totalMarketMover}" type="currency" />
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${eventStatus.marketMover > 0}">  
								<fmt:formatNumber value="${eventStatus.marketMover}" type="currency" />
							</c:when> 
							<c:otherwise>
								-
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>					
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
		<c:forEach var="categoryStatus" items="${eventStatus.children}">		
			<%--
				CATEGORY STATUS LEVEL
			--%>
			<tbody class="status-child status-child-tbody-${eventStatus.eventId}" style="display:none">
				<tr class="catRow">
					<%-- event name --%>
					<td style='text-align:left'>
						<img src="../images/ico-branch.gif" />
						<a onclick="toggleStatusTDoubleBody(${eventStatus.eventId}, ${categoryStatus.categoryId})" style="cursor: pointer">
							<img id="event-expand-img-${eventStatus.eventId}-${categoryStatus.categoryId}" class="category-expand-img" src="../images/ico-expand.gif" />
							${categoryStatus.category}							
						</a>  					
					</td>
					<%-- Empty Date --%>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<td> &nbsp; </td>
					<%-- Qty Sold --%>
					<td align="right">${categoryStatus.qtySold}</td>

					<%-- avg price --%>
					<td align="right"><fmt:formatNumber value="${categoryStatus.priceSold}" type="currency" /></td>

					<%-- Exposure --%>
					<td align="right"><fmt:formatNumber value="${categoryStatus.exposure}" type="currency" /></td>

					<%-- PGM --%>
					<td align="right" style="background-color:${ao:getMarginBackgroundColor(categoryStatus.projGrossMargin)}"> 
						<span style='color:black'><fmt:formatNumber value="${categoryStatus.projGrossMargin}" pattern="0.00" /></span>
					</td> 

					<%--Total Market Price --%>
					<td align="right">
						<c:choose>
							<c:when test="${categoryStatus.totalMinPrice > 0}">
								<fmt:formatNumber value="${categoryStatus.totalMinPrice}" type="currency" />
							</c:when>
							<c:otherwise>
								-
							</c:otherwise>
						</c:choose>
					</td>
				
					<%-- Lowest Price --%>
					<td align="right">
						<c:choose>
							<c:when test="${categoryStatus.minPrice > 0}">
								<fmt:formatNumber value="${categoryStatus.minPrice}" type="currency" />
							</c:when>
							<c:otherwise>
								-
							</c:otherwise>
						</c:choose>
					</td>

					<%-- Market Mover --%>
					<td align="right">
						<c:choose>
							<c:when test="${categoryStatus.marketMover > 0.00}">  
								<fmt:formatNumber value="${categoryStatus.marketMover}" type="currency" />
							</c:when> 
							<c:otherwise>
								-
							</c:otherwise>
						</c:choose>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>				
			<%--
				TICKET GROUP STATUS LEVEL
			--%>
			<tbody id="status-tbody-${eventStatus.eventId}-${categoryStatus.categoryId}" class="status-grandchild status-child-tbody-${eventStatus.eventId}" style="display:none" >
				<c:forEach var="ticketGroupStatus" items="${categoryStatus.children}">	
					<tr style='text-align:left'>
						<%-- event name --%>
						<td>
							<img src="../images/ico-branch.gif" />
							<img src="../images/ico-branch.gif" />

							${ticketGroupStatus.description} Quantity : ${ticketGroupStatus.qtySold}
							<input id="getCatStatsButton" type="button" class="smallButton" onclick="getStats(${ticketGroupStatus.eventId}, '${ticketGroupStatus.category}', ${ticketGroupStatus.categoryId},${ticketGroupStatus.qtySold})"  value="Tix" tooltip="Get tickets for category ${categoryStatus.category}" />		
						</td>
						<%-- Empty Date --%>
						<td> &nbsp; </td>
						<td> &nbsp; </td>
						<td> &nbsp; </td>
						<%-- Qty --%>
						<td align="right">${ticketGroupStatus.qtySold}</td>
						
						<%-- Avg Price --%>
						<td align="right">
							<fmt:formatNumber value="${ticketGroupStatus.priceSold}" type="currency" />
						</td>
						
						<%-- Exposure --%>
						<td align="right">
							<fmt:formatNumber value="${ticketGroupStatus.exposure}" type="currency" />
						</td>

						<%-- PGM --%>
						<td align="right" style="background-color:${ao:getMarginBackgroundColor(ticketGroupStatus.projGrossMargin)}">
							<span style='color:black'><fmt:formatNumber value="${ticketGroupStatus.projGrossMargin}" pattern="0.00" /></span>
						</td>		
						
						<%-- Total Lowest Price --%>
						<td align="right" style="white-space:nowrap">
							<c:choose>
								<c:when test="${ticketGroupStatus.totalMinPrice > 0.00}">  
									<fmt:formatNumber value="${ticketGroupStatus.totalMinPrice}" type="currency" />
								</c:when> 
								<c:otherwise>
									-
								</c:otherwise>
							</c:choose>
						</td>									

						<%-- Lowest Price --%>
						<td align="right" style="white-space:nowrap">
							<c:choose>
								<c:when test="${ticketGroupStatus.minPrice > 0}">
									<a href="RedirectToItemPage?id=${ticketGroupStatus.ticketId}" tooltip="${ticketGroupStatus.siteId}/Section:${ticketGroupStatus.ticketSection}/Row:${ticketGroupStatus.ticketRow}">
										<img src="../images/ico-${ticketGroupStatus.siteId}.gif" border="0" align="absbottom" />
									</a>
									<%--
									<a href="TMATChangeShortPrice?id=${ticketGroupStatus.ticketId}&eventId=${ticketGroupStatus.eventId}" tooltip="+/- Fees to this short's price - ${ticketGroupStatus.minPrice}">(+)</a>
									<a href="TMATExcludeTicket?ticketId=${ticketGroupStatus.ticketId}&eventId=${ticketGroupStatus.eventId}&shortId=${ticketGroupStatus.transactionId}" tooltip="Exclude this ticket from this or all shorts">(-)</a>
									--%>
									<span id="excludeTicketUpdate-${ticketGroupStatus.transactionId }">
										<fmt:formatNumber value="${ticketGroupStatus.minPrice}" type="currency" />
									</span>
									<a href="#" onclick="showPreviewTickets(${ticketGroupStatus.eventId}, ${ticketGroupStatus.qtySold}, ${ticketGroupStatus.categoryId}, ${ticketGroupStatus.transactionId}, <c:choose><c:when test="${ticketGroupStatus.longTransaction}">'long'</c:when><c:otherwise>'short'</c:otherwise></c:choose>, '${ticketGroupStatus.ticketId}'); return false;">(excl.)</a>
								</c:when>
								<c:otherwise>
									<span style='color:red'>No Tickets</span>
								</c:otherwise>
							</c:choose>
						</td>

						<%-- Market Mover --%>
						<td align="right">
							<c:choose>
								<c:when test="${ticketGroupStatus.marketMover > 0.00}">  
									<fmt:formatNumber value="${ticketGroupStatus.marketMover}" type="currency" />
								</c:when> 
								<c:otherwise>
									-
								</c:otherwise>
							</c:choose>
						</td>

						<%-- Section --%>
						<td>
							<span tooltip="${ticketGroupStatus.section}">${ticketGroupStatus.normalizedSection}</span>
							<input id="exStatsButton" type="button" class="smallButton" onclick="getExactStats(${ticketGroupStatus.eventId}, ${ticketGroupStatus.transactionId})"  value="Tix" tooltip="Get tickets for section ${ticketGroupStatus.section}" />
						</td>

						<%-- Row --%>
						<td tooltip="${ticketGroupStatus.row}">${ticketGroupStatus.normalizedRow}</td>

						<%-- Invoice --%>
						<td>
							${ticketGroupStatus.invoice}
						</td>

						<%-- Customer --%>
						<td style="background-color:${ticketGroupStatus.siteColor}">
							<span style='color:black'>${ticketGroupStatus.customer}</span>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</c:forEach>
	</c:forEach>
</c:if>
<%-- <c:if test="${not empty eventStatuses }">
	<tbody>
		<tr style="background-color: #E0E0E0 ">
			<td><b>Total:-</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td><b>${qty }</b></td>
			<td><b><fmt:formatNumber value="${revenue }" type="currency" /></b></td>
			<td><b><fmt:formatNumber value="${exposure }" type="currency" /></b></td>
			<td><b><fmt:formatNumber value="${pgm }" pattern="00.00" /></b></td>
			<td><b><fmt:formatNumber value="${totalMarketPrice }" type="currency" /></b></td>
			<td><b><fmt:formatNumber value="${marketPrice }" type="currency" /></b></td>
			<td><b><fmt:formatNumber value="${marketMover }" type="currency" /></b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</c:if> --%>
</table>
<br /><br />

<div style="border-top: 1px dashed black; border-bottom: 1px dashed black;padding-top: 5px; padding-bottom: 5px;">
    Show tickets for the following sites: <a href="javascript:checkAllSites()" style="font-size:11px">check all</a> - <a href="javascript:uncheckAllSites()" style="font-size:11px">uncheck all</a> 
    <br />
	<div style="float:left">
		<c:forEach var="sid" items="${constants.siteIds}">
			<div style="float: left; width: 280px;height: 24px;">
				<c:set var="safeSid" value="[${sid}]" />
				
				<input type="hidden" id="${sid}-checkbox-input" name="${sid}_checkbox" value=""/> 
				<input type="checkbox" id="${sid}-checkbox" class="site-checkbox"
					<c:if test="${not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
					<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>
				>
				<label for="${sid}_checkbox">
			    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
					<c:choose>
					  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
					    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
						  ${sid}
					    </span>
					  </c:when>
					  <c:otherwise>
					    ${sid}
					  </c:otherwise>
					</c:choose>
				</label>
			</div>
		</c:forEach>
	</div>
	<div style="clear:both"></div>
</div>

<br /><br />

<div id="statsDiv"></div>

<script type="text/javascript">
	/* Manually sorting on table header code */
	$(document).ready(function() {
		$('#statusTable tr:first').css('cursor', 'pointer');
	  	$('#statusTable th').click(function(){	
	  		
	  		var sortableType = $(this).text();
	  		var sortingType = '${sortingType}';
	  		if(sortingType != null && sortingType != ""){
	  			if(sortingType == "asc")
	  				sortingType = "desc";
	  			else
	  				sortingType = "asc";
	  		}else{
	  			sortingType = "asc";
	  		}
	  		
	  		dateRangeStr1 = $('#dateRange1').val();
	  	  	dateRangeStr2 = $('#dateRange2').val();
	  	 	var eventType = $('#browseEvents :selected').val();
		  	var flag=true;
		  	if(eventType==''){
		  		flag=false;
		  	}
	  		var cat = $('#selectCat').val();
	  		if(cat=='-'){
	  			flag=false;
	  			alert('Select Category Type..');
	  		}
	  		var inventoryType = $('#selectInventory').val();
	  		if(inventoryType=='-'){
	  			flag=false;
	  			alert('Select Inventory Type..');
	  		}
	  		var view = $('#selectView').val();
	  		
	  		if(!flag){
	  			return;
	  		}
	  		
	  		document.location.href = "TMATBrowseShorts?dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&view="+view+"&eventType="+eventType+"&sortBy="+sortableType+"&sortingType="+sortingType;
	  		
	  	});
	});
	
	var lastUpdated=new Date(parseInt($('#reload').val()));
	
	function showRefreshedRecently() {
		refreshRecentlySpanFirstTime();
	/*
	$('.refreshLink').hide();
	$('.refreshedRecently').show();
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
	if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
		refreshRecentlySpanFirstTime();
	} else {
	  setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
	}
	*/
	}

	function refreshRecentlySpanFirstTime() {
		$('.refreshedRecently').hide();
		$('.refreshLink').show();
		refreshRecentlySpan();
		setInterval("refreshRecentlySpan()", 60000);
	}

	function refreshRecentlySpan() {
		var now = new Date();
		var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
		if (numMins == 0) {
		  $('#numMinsSinceLastUpdate').text("just now");
		} else {
		  $('#numMinsSinceLastUpdate').text(numMins + " mn ago");
		}
	}

	
    function changeGroup(eventId) {
        var catGroup = $.trim($('#catScheme').val());
		if(eventId == undefined) {
	        document.location.href = "TMATBrowseShorts?catScheme=" + catGroup;
		} else {
			document.location.href = "TMATBrowseShorts?eventId=" + eventId + "&catScheme=" + catGroup;
		}
    }

    function getAll() {
        var catGroup = $.trim($('#catScheme').val());;
		document.location.href = "TMATBrowseShorts?eventId=ALL&catScheme=" + catGroup;
    }

	function expandAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-collapse.gif');
		$('#statusTable').find('.status-child').show();
		$('#statusTable').find('.status-grandchild').show();
	};

	function collapseAll() {
		$('#statusTable').find('.event-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.category-expand-img').attr('src', '../images/ico-expand.gif');
		$('#statusTable').find('.status-child').hide();
		$('#statusTable').find('.status-grandchild').hide();
	};

	function toggleStatusTBody(eId) {
		if ($('#event-expand-img-' + eId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId).show();
			$.each($('.status-child-tbody-' + eId), function(i, elt) {
				if ($(elt).hasClass("status-child")) {
					$(elt).show();
					$(elt).find('.category-expand-img').attr('src', '../images/ico-expand.gif');
				} else {
					$(elt).hide();
				}
				
			});
			$('#event-expand-img-' + eId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId).hide();
			$('.status-child-tbody-' + eId).hide();
			$('#event-expand-img-' + eId).attr('src', '../images/ico-expand.gif');
		}
	};
	    
	function toggleStatusTDoubleBody(eId, cId) {
		if ($('#event-expand-img-' + eId + '-' + cId).attr('src').indexOf('expand') >= 0) {
			$('#status-tbody-' + eId + '-' + cId).show();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#status-tbody-' + eId + '-' + cId).hide();
			$('#event-expand-img-' + eId + '-' + cId).attr('src', '../images/ico-expand.gif');
		}
	};
	
	function getStats(eId, category, categoryId, lotSize) {

 		if(category != 'Not Set') {

			var url;		

			url = "TMATPreviewStats?eventId=" + eId
					+ "&categoryId=" + categoryId
					+ "&lotSize=" + lotSize
					+ "&catScheme=${catGroupName}";
					
			 $.each($('.site-checkbox'), function(i, elt) {
    			var siteId = $(elt).attr('id').split("-")[0];
    			url += "&" + siteId + "_checkbox=" + $(elt).attr('checked');  	
    		});
			
			
			$('#statsDiv').empty();
			$('#statsDiv').append(
				$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
				"Fetching preview. Please wait..."		
			);		
			
	
			$.get(url, function(data){
				$('#statsDiv').html(data);
	 		});

		} else {
	 		alert("Zone Not Mapped for Event!");
			return;
		} 
	};

	function getExactStats(eId, tId) {
		var url;		

		url = "TMATPreviewStats?transactionId=" + tId + "&eventId=" + eId + "&catScheme=${catGroupName}";
				
		 $.each($('.site-checkbox'), function(i, elt) {
			var siteId = $(elt).attr('id').split("-")[0];
			url += "&" + siteId + "_checkbox=" + $(elt).attr('checked');  	
		});
		
		$('#statsDiv').empty();
		$('#statsDiv').append(
			$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
			"Fetching preview. Please wait..."		
		);		
	
		$.get(url, function(data){
			$('#statsDiv').html(data);
 		});
	};
	
	
  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Shorts Sold');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Sold');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  $(function() {
		//alert("tourId"+${tourId});
		//alert("eventId"+${eventId});
		//alert('${tourId}');
		//alert('${eventId}');
		/*if('${tourId}' != '' ||  '${eventId}' != '' ){
			//alert("hi");
			
		}*/
		var date1 = new Date(); 
		$('#dateRange1').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr1 = dateText;}});
		$('#dateRange2').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr2 = dateText;}});
		
		if('${fromDateRange}'==''){
			var prettyDate =(date1.getMonth()+1) + '/' + date1.getDate() + '/' +date1.getFullYear();
			$('#dateRange1').val(prettyDate);
		}else{
			$('#dateRange1').val('${fromDateRange}');
		}
		
		if('${toDateRange}'==''){
			var prettyDate =(date1.getMonth()+2) + '/' + date1.getDate() + '/' +date1.getFullYear();
			$('#dateRange2').val(prettyDate);
		}else{
			$('#dateRange2').val('${toDateRange}');
		}
		
		var noCrawlVal= $('#nocrawl').val();
		if(noCrawlVal=='true'){
			getCrawlerStatus();
		}else{
			refreshRecentlySpanFirstTime();
		}
  });
	var totalCrawls="${totalCrawl}";
	var eventIdstr = "${totalEvetnStr}";
	function getCrawlerStatus(){
		CrawlerDwr.getCrawlsExecutedForEventsCount(eventIdstr, function(response) {
			var temp = response.split(":");
			var count = parseInt(temp[0]);
			var eventString = temp[1];
			$('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
			$('.refreshCrawlInfoSpan').show();
			if(count < (totalCrawls)) {
				setTimeout("getCrawlerStatus()", 10000);
			}else {
				// force reload (avoid caching by adding a dummy parameter)
				$('.refreshCrawlInfo').html("100% updated! Reloading page...");
				
				var cat = $('#selectCat').val();
				var inventoryType = $('#selectInventory').val();
				var view = $('#selectView').val();
				if(view == 'csv'){
					$('.refreshCrawlInfoSpan').hide();
				}
				var noCrawl = $('#nocrawl').val();
				var iraEventStatusId = $('#iraEventStatusId').val();
				dateRangeStr1= $('#dateRange1').val();
				dateRangeStr2= $('#dateRange2').val();
				if('${tourId}' != '' && '${tourId}' != 'ALL'){
					if($('#selectView').val()=='html'){
						location.href = "TMATBrowseShorts?tourId=${tourId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
					}else{
						document.location.href = "TMATGetBrowseShortCSV?tourId=${tourId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
//						location.href = 'TMATGetBrowseShortCSV?eventId=${eventId}&nocrawl=true&dateRange1=${dateRangeStr1}&dateRange2=${dateRangeStr2}'+'&iraEventStatusId='+iraEventStatusId;
					}
					//location.href = 'TMATBrowseShorts?tourId=${tourId}&nocrawl=true&dateRange1=${dateRangeStr1}&dateRange2=${dateRangeStr2}';
					//tourId=20883&dateRange1=[object HTMLInputElement]&dateRange2=[object HTMLInputElement]
				} else  if('${eventId}' != '' && '${eventId}' != 'ALL'){
					if($('#selectView').val()=='html'){
						location.href = "TMATBrowseShorts?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
					}else{
						document.location.href = "TMATGetBrowseShortCSV?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
//						location.href = 'TMATGetBrowseShortCSV?eventId=${eventId}&nocrawl=true&dateRange1=${dateRangeStr1}&dateRange2=${dateRangeStr2}'+'&iraEventStatusId='+iraEventStatusId;
					}
				}else if('${venueId}' != '' && '${venueId}' != 'ALL'){
					if($('#selectView').val()=='html'){
						location.href = "TMATBrowseShorts?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
					}else{
						document.location.href = "TMATGetBrowseShortCSV?eventId=${eventId}&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
					}
				}
				else{
					browseAllEvents($('#browseEvents'));
				}
			}
		});
		
	}
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}else if(row[0] == "VENUE"){
				return "<div  class='searchVenueTag'>VENUE</div>" + row[1];
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  function checkAllSites() {
  	$('.site-checkbox').attr('checked', true);
  };

  function uncheckAllSites() {
  	$('.site-checkbox').attr('checked', false);
  };
  
  function browseAllEvents(obj) {
	dateRangeStr1 = $('#dateRange1').val();
	dateRangeStr2 = $('#dateRange2').val();
	var eventType = $(obj).val();
	var flag=true;
	if(eventType==''){
		flag=false;
	}
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
		$(obj).val('');
	}
	
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
		$(obj).val('');
	}
	var view = $('#selectView').val();
	var noCrawl = $('#nocrawl').val();
	var iraEventStatusId = $('#iraEventStatusId').val();
	if(flag){
		if(noCrawl=='true'){
			if(view=='html'){
				document.location.href = "TMATBrowseShorts?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view+"&sortBy=${sortBy}&sortingType=${sortingType}";
			}else{
				$('#nocrawl').val('false');
				document.location.href = "TMATGetBrowseShortCSV?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
			}
		}else{
			document.location.href = "TMATBrowseShorts?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat+"&nocrawl="+noCrawl + "&iraEventStatusId=" + iraEventStatusId + "&view="+view;
		}
		
	}else{
		alert('Select Parent Type..');
	}
  };
  
  function showPreviewTickets(eventId, quantity, categoryId, transactionId, type, ticketId) {
	$('#statsDiv').empty();
	$('#statsDiv').append(
		$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
		"Fetching tickets. Please wait..."		
	);		

    var url = "TMATPreviewTickets?eventId=" + eventId
    		+ "&quantity=" + quantity
    		+ "&categoryId=" + categoryId
    		+ "&transactionId=" + transactionId
    		+ "&type=" + type
    		+ "&ticketId=" + ticketId;
	$.get(url, function(data){
		$('#statsDiv').html(data);
	});
  }
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	} else if(row[0] == "VENUE"){
		pageDescription = "<div class='searchVenueTag'>ARTIST</div>" + row[1];
	}
  	
	dateRangeStr1 = $('#dateRange1').val();
  	dateRangeStr2 = $('#dateRange2').val();
	
  	var flag=true;
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
	}
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
	}
	var view = $('#selectView').val();
	if(flag){
		$('#tmatRedirectingPage').html(pageDescription);
		if (row[0] == "ARTIST") {
			document.location.href = "TMATBrowseEvents?artistId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "TOUR") {
			document.location.href = "TMATBrowseShorts?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&view="+view;
	//document.location.href = "TMATBrowseShorts?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "EVENT") {
			document.location.href = "TMATBrowseShorts?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&view="+view;
	//document.location.href = "TMATBrowseShorts?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		}else if(row[0] == "VENUE"){
			document.location.href = "TMATBrowseShorts?venueId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat+"&view="+view;
		}
	}else{
		$('#goToTextFieldTMATContainer').show();
		$('#tmatRedirectingContainer').hide();
	}
  });
</script>
