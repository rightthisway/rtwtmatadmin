<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script>
var dateRangeStr1 = null;
var dateRangeStr2 = null;
</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Browse Inventory
</div>

<h1>Browse Inventory</h1>

Date: <input type="text" id="dateRange1" class="date-pick" style="width: 240px"/> to <input type="text" id="dateRange2" class="date-pick" style="width: 240px"/> 
<div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<span class="refreshLink" style="display:none">
	
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>
	</span>
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>
	</span>
</div>
<br/>
<br/>

<div id="tmatHeaderGoTo">
	Category Type: 
	<select id="selectCat">
		<option value="-">--Select--</option>
		<option value="">All</option>
		<option value="cat">Categorized</option>
		<option value="uncat">Uncategorized</option>
	</select>
	Inventory Type: 
	<select id="selectInventory">
		<option value="-">--Select--</option>
		<option value="">All</option>
		<option value="short">Short</option>
		<option value="inventory">Long</option>
	</select>
	View : 
	<select id="selectView">
		<option value="html">HTML</option>
		<option value="csv">CSV</option>
	</select>
	<br/><br/>
	<span id="goToTextFieldTMATContainer">
		Go to <input type="text" id="goToTextTMATField" style="width: 240px" />
		<span id="goToTextTMATFieldStatus" style="color:#888888;font-style:italic"></span>
	</span>
	<span id="tmatRedirectingContainer" style="display:none">
  		<div>
			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
			<div id="tmatRedirectingPage" style="float:left"></div>
		</div>
	</span>

	or <!--<a href="javascript: return null;" onclick="browseAllEvents();">Browse ALL</a> -->
	Event Type:
	<select id="browseEvents" onchange="browseAllEvents(this);">
		<option value="">--Select--</option>
		<option value="sport">Browse All Sports</option>
		<option value="concert">Browse All Concerts</option>
		<option value="theater">Browse All Theaters</option>
		<option value="all">Browse All</option>
	</select>
	
</div>        

  
<c:if test="${not empty tourId}">
<h3><a href="TMATChangeTourPrice?tourId=${tourId}">Adjust Tour Fees</a></h3>
</c:if>
<br /><br />
<display:table class="list" name="${events}" id="event" requestURI="TMATBrowseShorts" pagesize="20">
    <display:column title="Date" sortable="true" property="date" format="{0,date,yyyy/MM/dd}" href="TMATBrowseShorts" paramId="eventId" paramProperty="id" />
    <display:column title="Event" sortable="false" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Venue" media="html">
    <img title="${event.venue.location}" id="show-venue-image" src="../images/question.jpg" />
    </display:column>
    <display:column title="Listing" sortable="false" property="ticketCount" />
    <display:column title="Available Ticket Quantity" sortable="false" >	
    ${event.ticketQuantitySum}
	</display:column>
	<display:column sortable="false" >
	    <a href="TMATBrowseBroadcast?eventId=${event.id}" >
			(Event Broadcast Shorts)
		</a>
	</display:column>
	<display:column sortable="false" >
	    <a href="#" onclick="deleteShorts(${event.id})" >
			(Delete All Event Shorts)
		</a>
	</display:column>
</display:table>

<c:if test="${not empty error}">
<div class="error">
<div class="errorText">
${error}
</div>
</div>
</c:if>

<script type="text/javascript">

  //
  // Autocomplete
  //
  
  $('#goToTextTMATField').val('Search Inventory');
  $('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextTMATField').focus(function() {
	$('#goToTextTMATField').val("");
	$('#goToTextTMATField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextTMATFieldStatus').empty();
  });

  $('#goToTextTMATField').blur(function() {
	$('#goToTextTMATField').val('Search Shorts Sold');
	$('#goToTextTMATField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextTMATFieldStatus').empty();
  });
  
  
  $(function() {
  		var date1 = new Date(); 
		$('#dateRange1').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr1 = dateText;}});
		$('#dateRange2').datepicker({changeMonth:true,changeYear:true,minDate:new Date(),maxDate:'+1y',showMonthAfterYear: true,onSelect: function(dateText,inst){dateRangeStr2 = dateText;}});
  });
  
  function browseAllEvents(obj) {
	var eventType = $(obj).val();
	var flag=true;
	if(eventType==''){
		flag=false;
	}
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
	}
	
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
	}
	var view = $('#selectView').val();
	if(flag){
		if(view=='html'){
			document.location.href = "TMATBrowseShorts?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat;
		}else{
			document.location.href = "TMATGetBrowseShortCSV?eventId=ALL&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&eventType="+eventType+"&catType="+cat;
		}
	}
  };
  
  $('#goToTextTMATField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextTMATFieldStatus').empty();
			$('#goToTextTMATFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextTMATFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextTMATFieldStatus').text("No results");
			} else {
				$('#goToTextTMATFieldStatus').text("Found " + rows.length + " results");
			}
		}
  });
  
  $('#goToTextTMATField').result(function(event, row, formatted) {
  	$('#goToTextFieldTMATContainer').hide();
  	$('#tmatRedirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
  
  	var dateRangeStr1 = document.getElementById('dateRange1');
  	var dateRangeStr2 = document.getElementById('dateRange2');
  	
  	var flag=true;
	var cat = $('#selectCat').val();
	if(cat=='-'){
		flag=false;
		alert('Select Category Type..');
	}
	var inventoryType = $('#selectInventory').val();
	if(inventoryType=='-'){
		flag=false;
		alert('Select Inventory Type..');
	}
	var view = $('#selectView').val();
	if(flag){
		$('#tmatRedirectingPage').html(pageDescription);
		if (row[0] == "ARTIST") {
			document.location.href = "TMATBrowseShorts?artistId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "TOUR") {
			if(view=='html'){
				document.location.href = "TMATBrowseShorts?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}else{
				$('#goToTextFieldTMATContainer').show();
				$('#tmatRedirectingContainer').hide();
				document.location.href = "TMATGetBrowseShortCSV?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}
	//document.location.href = "TMATBrowseShorts?tourId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		} else if (row[0] == "EVENT") {
			if(view=='html'){
				document.location.href = "TMATBrowseShorts?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}else{
				$('#goToTextFieldTMATContainer').show();
				$('#tmatRedirectingContainer').hide();
				document.location.href = "TMATGetBrowseShortCSV?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2+"&filter="+inventoryType+"&catType="+cat;
			}
	//document.location.href = "TMATBrowseShorts?eventId="+row[2]+"&dateRange1="+dateRangeStr1+"&dateRange2="+dateRangeStr2;
			return;
		}
	}else{
		$('#goToTextFieldTMATContainer').show();
		$('#tmatRedirectingContainer').hide();
	}
  });

  //delete event shorts
   function deleteShorts(id) {

    ShortTransactionDwr.deleteEventShorts(id, function(response) {
    		alert(response);
            });
  }
   
</script>
