<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
  function uncheckOther(name, id) {  
	  if($('input[id='+id+']').is(':checked')){
		var ticket = name.replace("scope_","");
		  $('span[id='+ticket+']').attr('id',id);
	  }else{
		  var ticket = name.replace("scope_","");
		  $('span#'+id).attr('id',ticket);
	  }
    $.each($('input[name=' + name + ']'), function(index, input) {

        if (input.id != id) {
    		$(input).removeAttr('checked');
    	}
    });
  }
  
  function submitUpdates() {
    var request = "";
    var flag = true;  
	var nextPrice = "";	
  	$.each($("#excludeTicketTable > tbody > tr > td > span"), function(index, span) {
  		if(span.id.indexOf("scope") != -1){
			request += span.id.replace("scope-", "") + ",";
		}else{
			if(flag){
				nextPrice = $('#'+span.id).text();
			}
			flag = false;
		}
  	});
  	DataDwr.updateExcludeTicketMap(${transactionId}, '${transactionType}', '${catScheme}', request,parseInt(nextPrice),
  								   function(response) {
  								   		if (response != "") {  								   		
								   			var arrayList = response.split("-");
								   			document.getElementById("excludeTicketUpdate-"+arrayList[0]).innerHTML = '$'+parseFloat(arrayList[1]);								   			  								   			
  								   		} else { 
  								   			alert("Sorry! Ticket(s) Not Excluded.");
  								   		}
  								   });
  }
  
</script>

Please select the tickets you want to exclude.
<br/><br/>
<input type="button" class="medButton" value="Cancel" onclick="$('#statsDiv').html('')"/>&nbsp;&nbsp;
<input type="button" class="medButton" value="Update" onclick="submitUpdates()"/>
<br/><br/>
<table class="list" id="excludeTicketTable">
  <thead>
    <tr>
      <th colspan="3">Exclude from</th>
      <th rowspan="2">Qty</td>
      <th rowspan="2">Cat</td>
      <th rowspan="2">Section</th>
      <th rowspan="2">Row</th>
      <th rowspan="2">Seat</th>
      <th rowspan="2">Wholesale</th>
	  <th rowspan="2">Online</th>
	  <th rowspan="2">Seller</th>
    </tr>
    <tr>
      <th>Event</td>
      <th>Cat</td>
      <th>Ticket</td>
	</tr>    
  </thead>
  <tbody>
    <c:forEach var="ticket" items="${tickets}">
      <tr <c:if test="${ticket.id == ticketId}">style="background:#99ff99"</c:if>>
        <td>
          <input id="scope-${ticket.id}-0" class="scope" name="scope_${ticket.id}" type="checkbox" onclick="uncheckOther('scope_${ticket.id}', this.id)"/>
        </td>
        <td>
          <input id="scope-${ticket.id}-1" class="scope" name="scope_${ticket.id}" type="checkbox" onclick="uncheckOther('scope_${ticket.id}', this.id)"/>
        </td>
        <td>
          <input id="scope-${ticket.id}-2" class="scope" name="scope_${ticket.id}" type="checkbox" onclick="uncheckOther('scope_${ticket.id}', this.id)"/>
        </td>
        <td>${ticket.remainingQuantity}</td>
        <td>${ticket.category.symbol}</td>
        <td>${ticket.normalizedSection}</td>
        <td>${ticket.normalizedRow}</td>
        <td>${ticket.seat}</td>
        <td><fmt:formatNumber pattern="#,##0.00;-#,##0.00" value="${ticket.adjustedCurrentPrice}"/></td>
        <td>
        	<span id="${ticket.id}">
        		<fmt:formatNumber pattern="#,##0.00;-#,##0.00" value="${ticket.currentPrice}"/>
        	</span>     
        </td>   
        <td>
	        <a href="RedirectToItemPage?id=${ticket.id}">
	        	<img src="../images/ico-${ticket.siteId}.gif" />
	        	Go to ticket on ${ticket.siteId}
	        </a>
        </td>
      </tr>
    </c:forEach>
  </tbody>
</table>
<br/>
<input type="button" class="medButton" value="Cancel" onclick="$('#statsDiv').html('')"/>&nbsp;&nbsp;
<input type="button" class="medButton" value="Update" onclick="submitUpdates()"/>

<script type="text/javascript">
  <c:forEach var="excludeTicketMap" items="${excludeTicketMaps}">
    $('#scope-${excludeTicketMap.ticketId}-${excludeTicketMap.scope}').attr('checked', 'true');
    $('#${excludeTicketMap.ticketId}').attr('id','scope-${excludeTicketMap.ticketId}-${excludeTicketMap.scope}');
  </c:forEach>
</script>