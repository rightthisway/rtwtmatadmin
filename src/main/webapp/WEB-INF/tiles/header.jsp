<%@include file="/WEB-INF/tiles/taglibs.jsp"%>


<jsp:directive.page import="com.admitone.tmat.data.Preference" />
<jsp:directive.page import="com.admitone.tmat.data.Event" />
<jsp:directive.page import="com.admitone.tmat.dao.DAORegistry" />

<c:set var="username"><authz:authentication operation="username"/></c:set>

<%
	// code to show last browsed events in the selector in the header	
	String username = (String)pageContext.getAttribute("username");
	Preference lastBrowsedEventIds = DAORegistry.getPreferenceDAO().getPreference(username, "lastBrowsedEventIds");
	if (lastBrowsedEventIds != null && lastBrowsedEventIds.getValue() != null) {
		String[] eventIds = lastBrowsedEventIds.getValue().split(",");
		Event[] events = new Event[eventIds.length];
		int i = 0;
		for(String eventId: eventIds) {
			events[i++] = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		}
		pageContext.setAttribute("lastBrowsedEvents", events); 
	}			
%>		

<div id="header">
	<div style="float:left;margin-left: 5px;margin-right: 20px;margin-bottom: 10px;">
		<a href="..">
		
		
	<%--
			<c:choose>
				<c:when test="${constants.ipAddress == '172.31.255.12'}"><img src="../images/logo-admit-one.gif" border="0"/></c:when>
				<c:when test="${constants.ipAddress == '192.168.0.176'}"><img src="../images/logo-admit-one.gif" border="0"/></c:when>
				<c:otherwise><img src="../images/logo-admit-one-test.gif" border="0"/></c:otherwise>
			</c:choose>
	--%>
	<img src="../images/Admin Logo.jpg" border="0"/>
		</a>
	</div>
      
      <div style="float:left">
	      <div>
	       <!--  
	        	<div class="highlightRoleRetail">
	            	RETAIL
		      	</div>
	          -->
	       
	          <!-- <div class="highlightRoleAdmin">
	            ADMIN
		      </div> -->
		       <div class="highlightRoleEditor">
	            <a style="cursor:hand;text-decoration:none;color:white" href="SwitchToAnalytics">EDITOR</a>
		      </div>
		      <div class="highlightRoleUser">
	            <a style="cursor:hand;text-decoration:none;color:white" href="SwitchToBrowse">USER</a>
		      </div>
	        <!--
	          <div class="highlightRoleEditor">
	            <a href="..">EDITOR</a>
		      </div>
		      <div class="highlightRoleUser">
	            <a href="..">USER</a>
		      </div>
	       
	          <div class="highlightRoleUser">
	            <a href="..">USER</a>
		      </div>
	        -->
	       
			<!-- 
	        
	          <div class="highlightRoleEbay">
	            EBAY
		      </div>
			-->
	      </div>
	   </div>

      <c:if test="${username != 'anonymousUser'}">
	      <div style="text-align: right; margin-right: 5px">
	      	<div style="height: 40px">
	      	<authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR">
		        <img src="../images/ico-user.gif" align="absbottom" /><a href="MyAccount">${username}</a> | 
                <a href="Bookmarks"><img src="../images/ico-star-on.gif" align="absbottom" />Starred</a>
             </authz:authorize>
             <authz:authorize ifAnyGranted="PRIV_OFFSHORE">
		        <img src="../images/ico-user.gif" align="absbottom" />${username} 
             </authz:authorize>
             |          
             <a href="Logout"><img src="../images/ico-delete.gif" align="absbottom" /> Log out</a>
		        
		        <div style="margin-top:3px">
				  <img src="../images/ico-clock.gif" align="absbottom"/>
				  <span id="headerTime"></span><br/>
			      <span id="headerDate" style="font-weight: bold"></span>
			    </div>
		    </div>
	      </div>
      </c:if>
      
      <div style="clear:both"></div>
      <div>
      	<div style="float:left;width:10px">&nbsp;</div>
       <!-- <a id="tab_home" class="tabHeaderOff" href=".."><img src="../images/ico-home.gif" align="absmiddle" style="margin-right: 5px;" />Home</a>
            
	        <a id="tab_browse" class="tabHeaderOff" href="BrowseTours"><img src="../images/ico-browse.gif" align="absmiddle" style="margin-right: 5px;" />Browse</a>
	        <a id="tab_short" class="tabHeaderEditorOff" href="TMATBrowseEvents"><img src="../images/ico-shorts.gif" align="absmiddle" style="margin-right: 5px;" />IRA</a>
	   -->
        <a id="tab_editor_manage_data" class="tabHeaderEditorOff" href="EditorTCAP"><img src="../images/ico-data.gif" align="absmiddle" style="margin-right: 5px;" />Data</a>
     	<authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR">
          <a id="tab_editor_categories" class="tabHeaderEditorOff" href="EditorEditCategories"><img src="../images/ico-category.gif" align="absmiddle" style="margin-right: 2px;" />Zone</a>
          <a id="tab_editor_crawler" class="tabHeaderEditorOff" href="EditorCrawlerControls"><img src="../images/ico-crawler.gif" align="absmiddle" style="margin-right: 2px;" />Mapping</a>
          <a id="tab_editor_auto_pricing" class="tabHeaderEditorOff" href="ExchangeEventAutoPricing">AutoPricing</a>
          </authz:authorize>
          <authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN">
          	<a id="tab_admin" class="tabHeaderAdminOff" href="AdminMaintenance"><img src="../images/ico-admin.gif" align="absmiddle" style="margin-right: 5px;" />Admin</a>
          </authz:authorize>
          <authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR">
          	<a id="tab_editor_report" class="tabHeaderEditorOff" href="tmatReport">Report</a>
          </authz:authorize>
        <div style="clear: both"></div>
      </div>
</div>

<script type="text/javascript">
	var __serverTime = <% out.println(new java.util.Date().getTime()); %>;
	var __clientTime = new Date().getTime();
	
	refreshHeaderDateTime();
	
	function goToEvent(eventId) {
		$('#lastBrowsedEventsDiv').html("<img src='../images/process-running.gif' align='absbottom' />Redirecting...");
	
		document.location.href = "BrowseTickets?eventId=" + eventId;
	}
	
	function refreshHeaderDateTime() {
		var currentDate = new Date(new Date().getTime() - __clientTime + __serverTime);
		$('#headerDate').text(formatShortDate(currentDate));
		$('#headerTime').text(formatTime(currentDate, true));
		setTimeout(refreshHeaderDateTime, 1000);
	};

  <c:choose>
    <c:when test="${fn:contains(selectedTab, 'admin') || fn:contains(selectedTab, 'report') || fn:contains(selectedTab, 'stats')}">
      $('#tab_${selectedTab}').attr('class', 'tabHeaderAdminOn');
    </c:when>
    <c:when test="${fn:contains(selectedTab, 'editor') || fn:contains(selectedTab, 'short')}">
      $('#tab_${selectedTab}').attr('class', 'tabHeaderEditorOn');
    </c:when>
    <c:when test="${fn:contains(selectedTab, 'ebay')}">
      $('#tab_${selectedTab}').attr('class', 'tabHeaderEbayOn');
    </c:when>
    <c:otherwise>
      $('#tab_${selectedTab}').attr('class', 'tabHeaderOn');
    </c:otherwise>
  </c:choose>
  
  //
  // Autocomplete
  //
  
  $('#goToTextField').val('Search artists, tours or events');
  $('#goToTextField').css({color: '#aaaaaa', 'font-style': 'italic'});
  
  $('#goToTextField').focus(function() {
	$('#goToTextField').val("");
	$('#goToTextField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextFieldStatus').empty();
  });

  $('#goToTextField').blur(function() {
	$('#goToTextField').val('Search artists, tours or events');
	$('#goToTextField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextFieldStatus').empty();
  });
  
  $('#goToTextField').autocomplete("AutoCompleteSearch", {
  		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
				 	tourRow += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
			if (!isNaN(row[3]*1)) {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
				}else{
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'> TBD </font>";
				}				
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextFieldStatus').empty();
			$('#goToTextFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextFieldStatus').text("No results");
			} else {
				$('#goToTextFieldStatus').text("Found " + rows.length + " results");
			}
		},
		highlight: function(value, term) {
			return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.trim(term.replace(/ +/g, ' ')).replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");		
		}
  });
  
  $('#goToTextField').result(function(event, row, formatted) {
  	$('#goToTextFieldContainer').hide();
  	$('#redirectingContainer').show();
  	
  	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (!isNaN(row[3]*1) && !isNaN(row[4] * 1)) {
		 	pageDescription += " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "-" + formatShortDate(new Date(row[4]*1)) + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
	if (!isNaN(row[3]*1)) {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatDate(new Date(row[3]*1)) + "</font>";
		}else{
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'> TBD </font>";
		}
	}
  	
  	$('#redirectingPage').html(pageDescription);
  	
  	if (row[0] == "ARTIST") {
  		document.location.href = "BrowseTours?artistId=" + row[2];
  		return;
  	} else if (row[0] == "TOUR") {
  		document.location.href = "BrowseEvents?tourId=" + row[2];
  		return;
  	} else if (row[0] == "EVENT") {
  		document.location.href = "BrowseTickets?eventId=" + row[2];
  		return;
  	}
  });

</script>
