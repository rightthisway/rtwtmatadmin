<%
try {
%>

<%

String referrerUrl = request.getHeader("referer");
if (referrerUrl != null) {
	referrerUrl = referrerUrl.replace("\n", "");
	pageContext.setAttribute("referrerUrl", referrerUrl);
}

String userAgent = request.getHeader("user-agent");
if (userAgent != null) {
	userAgent = userAgent.replace("\n", "").replace("\"", "\\\"");
	pageContext.setAttribute("userAgent", userAgent);
}

String clientIpAddress = request.getRemoteAddr();
if (clientIpAddress != null) {
	clientIpAddress = clientIpAddress.replace("\n", "");
	pageContext.setAttribute("clientIpAddress", clientIpAddress);
}
%>


<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="decorators.jsp" />
<jsp:useBean id="constants" class="com.admitone.tmat.web.Constants" scope="request" />
<%-- <jsp:useBean id="global" class="com.admitone.tmat.web.Global" scope="request" /> removed by Chirag--%>
<tiles:useAttribute id="selectedTab" name="selectedTab" scope="request" />

<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title><tiles:getAsString name="title"/></title>
        <!-- CSS -->
        <style type="text/css">
            @import url(../css/common.css);
		</style>
		<!--[if gte IE 7]>
        <style type="text/css">
            @import url(../css/common-ie7.css);
		</style>
		<![endif]-->	
		
        <!-- FAVICON -->
        <link rel="shortcut icon" href="../favicon.ico" />
        
		<script type='text/javascript' src='../js/date.js'></script>
		<script type='text/javascript' src='../js/utils.js'></script>

		<script type="text/javascript" src="../js/FABridge.js"></script>
		<script type="text/javascript" src="../js/swfobject.js"></script>
        
		<!-- jQuery + Plugins -->

<!--		
		<script type='text/javascript' src='../js/jquery/jquery.js'></script>
		<script type='text/javascript' src='../js/jquery/jquery-dom.js'></script>
		<script type='text/javascript' src='../js/jquery/jquery-cookie.js'></script>
		<script type='text/javascript' src='../js/jquery/jquery-tooltip.js'></script>
		<script type='text/javascript' src='../js/jquery/jquery.tabs.js'></script>
		<script type='text/javascript' src='../js/jquery/jquery-autocomplete.js'></script>
		<script type="text/javascript" src="../js/jquery/jquery-ui-1.7.1.custom.min.js"></script>
		<style type="text/css">
		    @import url(../css/jquery/jquery-autocomplete.css);
		    @import url(../css/jquery/jquery-tooltip.css);
            @import url(../css/jquery/jquery.tabs.css);
            @import url(../css/jquery/jquery.tabs-ie.css);
		    @import url(../css/smoothness/jquery-ui-1.7.1.custom.css);
		</style>
-->		
		
		<script type='text/javascript' src='../js/jquery-all.js'></script>
		<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>

        <!-- DWR -->
        <authz:authorize ifAnyGranted="PRIV_LOGGED_IN">
	      <script type='text/javascript' src='../js/scripts.js'></script>         
          <script type='text/javascript' src='engine.js'></script>         
          <script type='text/javascript' src='interface/BookmarkDwr.js'></script>
          <script type='text/javascript' src='interface/CategoryDwr.js'></script>
          <script type='text/javascript' src='interface/CategoryMappingDwr.js'></script>
          <script type='text/javascript' src='interface/CrawlerDwr.js'></script>
          <script type='text/javascript' src='interface/DataDwr.js'></script>
          <script type='text/javascript' src='interface/BrowseTicketDwr.js'></script>
        </authz:authorize>                                           
    </head>
    <body>
	    <div id='headerFrame' style="padding-top: 5px;padding-left: 10px; padding-right: 10px;">
			<tiles:insertAttribute name="header" />
	    </div>
	    <div id='bodyFrame' style="padding-left: 10px; padding-right: 10px;">
			<tiles:insertAttribute name="body" />
	    </div>
		<tiles:insertAttribute name="footer" />
	        
		<script type="text/javascript" language="javascript">
			$(document).ready(function () {
				$('*[tooltip]').tooltip({
					showURL: false,
					bodyHandler: function() { 
						return $(this).attr("tooltip"); 
				    } 			
				});
				
			});
			
			var __referrerUrl = "${referrerUrl}";
			var __userAgent = "${userAgent}";
			var __clientIpAddress = "${clientIpAddress}";
		</script>
		
        
			<img src="../images/bar-feedback.gif" style="position:fixed;bottom:30px;right:0px;cursor:pointer" onclick="showFeedbackDialog()" tooltip="Send a feedback" />
				                
    </body>
</html>


<%
} catch (Exception e) {
%>

<h1>Error while displaying this page</h1>

<div id="feedbackForm">

<h2>Error Stack Trace</h2>
<textarea id="errorStackTrace" style="width:100%;height:140px" readonly><%
	out.println("class: " + e.getClass().getName());
	out.println(e.getMessage());

	for (StackTraceElement element : e.getStackTrace()) {
		out.println(element.toString());
	}
%></textarea>

<h2>Error Feedback Form</h2>

You can help us to make this application better by sending us an error feedback.<br />
<table>
	<tr>
		<td>Current URL</td>
		<td><input type="text" id="currentUrl" value="" style="width:600px" readonly /></td>
	</tr>
	<tr>
		<td>Referrer URL</td>
		<td><input type="text" id="referrerUrl" value="${referrerUrl}"  style="width:600px" readonly /></td>
	</tr>
	<tr>
		<td>User Agent</td>
		<td><input type="text" id="userAgent" value="${userAgent}"  style="width:600px" readonly /></td>
	</tr>
</table>
<br />
Describe the action that leads to this error.<br /><br />
Project: <select id="errorFeedbackProject">
	<option value='TMATT'>TMAT-T</option>
	<option value='TMATV'>TMAT-V</option>
	<option value='TMATIV'>TMAT-IV</option>
	<option value='CATEGORYSUPPORT'>Zone Support</option>
	<option value='CHEAPTIX'>Cheap Tix</option>
	<option value='CIRCLES'>Circles</option>
	<option value='EBAY'>eBay</option>
	<option value='INTRANET'>Intranet</option>
	<option value='ONSALE'>Onsale</option>
	<option value='PORTAL'>Portal</option>
	<option value='TMATII'>TMAT-II</option>
</select>
<br />
Summary: <input type="text" id="errorFeedbackSummary" style="width:100%;" /><br />
<br/>
Priority:
<br/>
<select id="errorFeedbackPriority">
  <option value="4">Normal</option>
  <option value="5">High</option>
  <option value="6">Urgent</option>
  <option value="7">Immediate</option>
  <option value="10">Blocker</option>
</select>
<br />
Message<br />
<textarea id="errorFeedbackMessage" style="width:100%;height: 140px"></textarea>
<br /><br />
By clicking on the Send button, an email will be sent to the developers with your message and the exception stack trace shown above.<br />
<div id="feedbackButtonContainer">
	<input type="button" value="Send" class="medbutton" onclick="sendErrorFeedback()" />
	<input type="button" value="Back" class="medbutton" onclick="history.go(-1)" />
</div>
<div id="feedbackLoading" style="display:none">
	<img src="../images/process-running.gif" align="absbottom" />Submitting feedback...
</div>

<%
}
%>
</div>

<div id="feedbackThankYou" style="display:none">
	<div class="info">
		<div class="infoText">
			Your feedback has been sent. Thank you!
		</div>
	</div>
	<input type="button" value="Back" class="medbutton" onclick="history.go(-1)" />
</div>

<script type="text/javascript">
	$('#currentUrl').val(document.location.href);

	function sendErrorFeedback() {
		$('#feedbackButtonContainer').hide();	
		$('#feedbackLoading').show();	
		DataDwr.sendErrorFeedback(
				$('#errorFeedbackProject').val(),		
				$('#errorFeedbackSummary').val(),
				$('#errorFeedbackPriority').val(),
				$('#errorFeedbackMessage').val(),
				$('#errorStackTrace').val(),
				document.location.href,
				$('#referrerUrl').val(),
				document.cookie,
				$('#userAgent').val(),
				"${clientIpAddress}",
				function(result) {
					if (result == "OK") {
						$('#feedbackForm').hide();
						$('#feedbackThankYou').show();
					} else {
						alert(result);
						$('#feedbackButtonContainer').show();	
						$('#feedbackLoading').hide();	
					}
				}
		);
				
	};
</script>