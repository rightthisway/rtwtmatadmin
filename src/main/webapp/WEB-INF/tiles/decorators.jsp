<jsp:directive.page import="java.util.Date"/>
<jsp:directive.page import="java.text.DateFormat"/>
<jsp:directive.page import="java.text.SimpleDateFormat"/>
<jsp:directive.page import="javax.servlet.jsp.PageContext"/>
<jsp:directive.page import="org.displaytag.properties.MediaTypeEnum"/>
<jsp:directive.page import="com.admitone.tmat.crawler.TicketListingCrawl"/>
<jsp:directive.page import="com.admitone.tmat.crawler.TicketListingCrawler"/>
<jsp:directive.page import="com.admitone.tmat.data.Event"/>
<jsp:directive.page import="com.admitone.tmat.eventcreation.CreationEventHit"/>
<jsp:directive.page import="com.admitone.tmat.enums.EventStatus"/>
<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketRow"/>
<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketListingCrawlRow"/>
<jsp:directive.page import="com.admitone.tmat.enums.CrawlState"/>
<jsp:directive.page import="com.admitone.tmat.data.EbayInventoryGroup"/>
<jsp:directive.page import="com.admitone.tmat.data.EbayInventoryEvent"/>
<jsp:directive.page import="com.admitone.tmat.enums.EbayInventoryStatus"/>
<jsp:directive.page import="com.admitone.tmat.data.UserAlert"/>
<jsp:directive.page import="com.admitone.tmat.enums.UserAlertStatus"/>
<jsp:directive.page import="com.admitone.tmat.enums.AlertFor"/>

<%
	request.setAttribute("userAlertTableRowDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			String classNames = "";
			UserAlert userAlert = (UserAlert)getCurrentRowObject();
			if (userAlert.getUserAlertStatus().equals(UserAlertStatus.DISABLED)) {
				classNames += " disabledRow";
			} else if (userAlert.getAlertFor().equals(AlertFor.TMAT)) {
				classNames += " tmatAlertRow";
			} else {
				classNames += " circlesAlertRow";
			}
			
			if (userAlert.getLastEmailDate() != null) {
				classNames += " alertFulfilledRow";
			}
			
			return classNames;
		}
		
		public String addRowId() {
			return "myrow" + evaluate("id");
		}
	});


	request.setAttribute("crawlTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			return ((TicketListingCrawl)getCurrentRowObject()).isEnabled()?"":"disabledRow";
		}
		
		public String addRowId() {
			return "myrow" + evaluate("id");
		}
	});

	request.setAttribute("webCrawlTableRowDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			WebTicketListingCrawlRow crawlRow = (WebTicketListingCrawlRow)getCurrentRowObject();
			if (!crawlRow.isEnabled()) {
				return "disabledRow";
			}
			if (crawlRow.isBroken()) {
				return "brokenRow";
			}
			return "";
		}
		
		public String addRowId() {
			return "myrow" + evaluate("id");
		}
	});
	
	
	final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	final javax.servlet.http.HttpServletRequest httpServletRequest = request;

	request.setAttribute("crawlStatusColumnDecorator", new org.displaytag.decorator.DisplaytagColumnDecorator() {
	
		public Object decorate(Object value, PageContext pageContext, MediaTypeEnum media) {
			boolean expandedCrawlView = "1".equals((String)httpServletRequest.getAttribute("expandedCrawlView"));
			TicketListingCrawler ticketListingCrawler = (TicketListingCrawler)httpServletRequest.getAttribute("ticketListingCrawler");

			int crawlId;
			CrawlState crawlState;
			String crawlName;
			Date startCrawl;
			Date endCrawl;
			Integer nextCrawlWaitingTime;
			String errorMessage;

			if (value instanceof TicketListingCrawl) {
				TicketListingCrawl crawl = (TicketListingCrawl)value;
				crawlId = crawl.getId();
				crawlName = crawl.getName();
				crawlState = crawl.getCrawlState();			
				startCrawl = crawl.getStartCrawl();			
				endCrawl = crawl.getEndCrawl();			
				nextCrawlWaitingTime = crawl.getNextCrawlWaitingTime();			
				errorMessage = crawl.getErrorMessage();			
			} else if (value instanceof WebTicketListingCrawlRow) {
				WebTicketListingCrawlRow crawl = (WebTicketListingCrawlRow)value;			
				crawlId = crawl.getId();
				crawlName = crawl.getName();
				crawlState = crawl.getCrawlState();			
				startCrawl = crawl.getStartCrawl();			
				endCrawl = crawl.getEndCrawl();			
				nextCrawlWaitingTime = crawl.getNextCrawlWaitingTime();			
				errorMessage = crawl.getErrorMessage();			
			} else {
				return null;
			}
			
			String content = "";
			
			if (crawlState == com.admitone.tmat.enums.CrawlState.NO_TIX || (errorMessage != null && errorMessage.equals("No tickets found"))) {
				content = "<span class='crawlError' " + ((endCrawl == null)?"":("tooltip='last crawl ended at " + timeFormat.format(endCrawl) + "'")) + ">NO TIX</span>";
			} else if (crawlState.isError()) {
				if (expandedCrawlView) {
					content = "<span class='crawlError'>" + crawlState + ":" + errorMessage + "'></span>";
				} else {
					content = "<span class='crawlError' tooltip='ERROR: " + errorMessage + "'>" + crawlState + "</span>";
				}
				content = "<a href='javascript:showCrawlException(" + crawlId + ")'>" + content + "</a>";
			}
			
			if (!ticketListingCrawler.isRunning() || nextCrawlWaitingTime == -1) {
				return content + "<span class='crawlStopped'><img src='../images/ico-stopped.gif' align='absbottom'/> STOPPED</span>"; 
			} else if (nextCrawlWaitingTime == 0) {
				return content + ((startCrawl == null)?"":"<span " + ((startCrawl == null)?"":("tooltip='crawl started at " + timeFormat.format(startCrawl) + "'")) + "'>")
					+ "<img src='../images/process-running.gif' align='absbottom'/> <font color='green' />RUNNING</font>"
					+ ((startCrawl == null)?"":"</span>");
			}
			
			String waitingTime;

			if (nextCrawlWaitingTime < 60) {
//				waitingTime = "<= 1mn";
				waitingTime = "READY";
			} else {
				waitingTime = "IN " + (int)(nextCrawlWaitingTime / 60) + "mn";
			}

			return content + "<span class='crawlTime' " + ((endCrawl == null)?"":("tooltip='last crawl ended at " + timeFormat.format(endCrawl) + "'")) + ">"
				+ waitingTime + ""
				+ "</span>";
			
		}				
	});

	
	request.setAttribute("expiredEventTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			return (((Event)getCurrentRowObject()).getEventStatus() == EventStatus.EXPIRED)?"disabledRow":"";
		}
	});
	
	request.setAttribute("ticketTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			String classes = "";
			WebTicketRow ticket = (WebTicketRow)getCurrentRowObject();
			if (ticket.isRemovedDuplicateTicket()) {
				classes += " removedDuplicateTicket";
			}
			if (ticket.isAdmitOneTicket()) {
				classes += " admit1Row ";
			}
			if (ticket.isAdmitOneNonTMTicket()) {
				classes += " admit1RowNonTM ";
			}
			
			return classes;
		}
		
		public String addRowId() {
			return "myrow" + evaluate("id");
		}
	});
	
	request.setAttribute("creationEventHitTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			String classes = "";
			CreationEventHit creationEventHit = (CreationEventHit)getCurrentRowObject();
			if (creationEventHit.isExists()) {
				classes += " existingEventRow";
			}
			return classes;
		}
	});

	request.setAttribute("ebayGroupTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			String classes = "";
			EbayInventoryGroup group = (EbayInventoryGroup)getCurrentRowObject();
			if (group.isDisabled()) {
				classes += " disabledEbayInventoryGroupRow";
			} else if (group.getStatus().equals(EbayInventoryStatus.INVALID)) {
			    classes += " invalidEbayInventoryGroupRow";
			} else if (group.getStatus().equals(EbayInventoryStatus.SUGGESTED)) {
			    classes += " suggestedEbayInventoryGroupRow";
			} else if (group.getStatus().equals(EbayInventoryStatus.REPLACEMENT)) {
			    classes += " replacementEbayInventoryGroupRow";
			}
			
			return classes;
		}
	});

	/* request.setAttribute("ebayEventTableDecorator", new org.displaytag.decorator.TableDecorator() {
		public String addRowClass() {
			String classes = "";
			EbayInventoryEvent event = (EbayInventoryEvent)getCurrentRowObject();
			if (event.getNumPendingGroups() > 0) {
				classes += " suggestedEbayInventoryEventRow";
			}
			
			return classes;
		}
	}); */
	
%>