<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="..">Home</a> 
  &gt; My Account 
</div>
<c:if test="${not empty error}">
<div class="error">
	<div class="errorText">
		${error}
	</div>
</div>
</c:if>

<h1>My Account</h1>

<c:if test="${not empty info}">
<div class="info">
	<div class="infoText">
		${info}
	</div>
</div>
</c:if>

<h2>Update my Information</h2>

<form action="MyAccount" method="POST">
<table>
	<tr>
		<td>E-Mail</td>
		<td><input type="text" name="email" class="longInput" value="<c:out value='${user.email}' />" /></td>
	</tr>
	<tr>
		<td>First Name</td>
		<td><input type="text" name="firstName" class="longInput" value="<c:out value='${user.firstName}' />" /></td>
	</tr>
	<tr>
		<td>Last Name</td>
		<td><input type="text" name="lastName" class="longInput" value="<c:out value='${user.lastName}' />" /></td>
	</tr>
	<tr>
		<td>Phone</td>
		<td><input type="text" name="phone" class="longInput" value="<c:out value='${user.phone}' />" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" name="updateInfo" value="Update" class="medButton" />
		</td>
	</tr>
</table>
</form>

<h2>Change password</h2>
<form action="MyAccount" method="POST">
<table>
	<tr>
		<td>Password</td>
		<td><input type="password" name="password" /></td>
	</tr>
	<tr>
		<td>Confirm Password</td>
		<td><input type="password" name="password2" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" name="updatePassword" value="Update" class="medButton" />
		</td>
	</tr>
</table>
</form>

<%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
	
	<h2>Change Email Settings</h2>
	<form action="MyAccount" method="POST">
		<table>
			<tr>
				<td><input type="checkbox" id="emailCrawlErrorCreatorEnabled" name="emailCrawlErrorCreatorEnabled" <c:if test="${user.emailCrawlErrorCreatorEnabled}">checked</c:if> /><label for="emailCrawlErrorCreatorEnabled">Receive emails for errors on crawl I created</label></td>
			</tr>
			<tr> 
				<td><input type="checkbox" id="emailCrawlErrorLastUpdaterEnabled" name="emailCrawlErrorLastUpdaterEnabled" <c:if test="${user.emailCrawlErrorLastUpdaterEnabled}">checked</c:if> /><label for="emailCrawlErrorLastUpdaterEnabled">Receive emails for errors on crawl I last updated</label></td>
			</tr>
			<tr>
				<td><input type="submit" name="updateEmailSettings" value="Update" class="medButton" /></td>
			</tr>
		</table>
	</form>
<%-- </authz:authorize> --%>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$("INPUT[@name='email']").focus();
	});
</script>
