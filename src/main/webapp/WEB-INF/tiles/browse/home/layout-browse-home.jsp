<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="userSubMenu">
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Home'}">
			<b>Home</b>
		</c:when>
		<c:otherwise>
			<a href="./">Home</a>
		</c:otherwise>
	</c:choose>
	
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Upcoming Events'}">
			<b>Upcoming Events</b>
		</c:when>
		<c:otherwise>
			<a href="UpcomingEvents">Upcoming Events</a>
		</c:otherwise>
	</c:choose> 
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Top Ticket Count Events'}">
			<b>Events With Most Tickets</b>
		</c:when>
		<c:otherwise>
			<a href="TopTicketCountEvents">Events With Most Tickets</a>
		</c:otherwise>
	</c:choose> 
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Starred'}">
			<b>Starred</b>
		</c:when>
		<c:otherwise>
			<a href="Bookmarks">Starred</a>
		</c:otherwise>
	</c:choose> 
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Account Settings'}">
			<b>Account Settings</b>
		</c:when>
		<c:otherwise>
			<a href="MyAccount">Account Settings</a>
		</c:otherwise>
	</c:choose> 
	|
	<c:choose>
		<c:when  test="${selectedSubMenu == 'Useful Links'}">
			<b>Useful Links</b>
		</c:when>
		<c:otherwise>
			<a href="UsefulLinks">Useful Links</a>
		</c:otherwise>
	</c:choose> 
</div>

<tiles:insertAttribute name="subBody" />