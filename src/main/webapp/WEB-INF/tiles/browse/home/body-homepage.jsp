<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	Home
</div>

<%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	<div class="info">
		<div class="infoText">
			You are logged on with the <b>ADMINISTRATOR</b> role.
		</div>
	</div>
<%-- </authz:authorize> --%>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
		<div class="infoText">${param.info}</div>
	</div>
</c:if>

<%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
	<c:if test="${!ticketListingCrawler.running}">
		<div class="error">
			<div class="errorText">
				CRAWLER IS NOT RUNNING
				You can start it by going to the <a href="EditorCrawlerControls">crawler control page</a>.
			</div>
		</div>
	</c:if>

	<c:if test="${not empty clockError}">
		<div class="error">
			<div class="errorText">
				${clockError}
			</div>
		</div>
	</c:if>
	<c:if test="${ticketListingCrawler.running && expectedCrawlThreadCount != activeCrawlThreadCount}">
		<div class="error"><div class="errorText">SOME CRAWL THREADS MIGHT HAVE DIED. ${activeCrawlThreadCount} THREADS ARE RUNNING BUT ${expectedCrawlThreadCount} WERE EXPECTED</div></div>
	</c:if>
<%-- </authz:authorize> --%>

<h1>Welcome to TMAT</h1>
<br />

<%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
	<c:if test="${fn:length(eventsToExpire) > 0}">
		<img src="../images/ico-warning.gif" align="absbottom" />There are ${fn:length(eventsToExpire)} events that may need to be expired. <a href="EditorManageExpiredEvents">Go to manage expired events page</a>. <br />
	</c:if>

	<c:if test="${fn:length(failedCrawls) > 0}">
		<img src="../images/ico-warning.gif" align="absbottom" /> There are ${fn:length(failedCrawls)} crawls with errors. <a href="EditorCrawlErrors">Go to crawl errors page</a>. <br />
	</c:if>
	
	<img src="../images/ico-warning.gif" align="absbottom" /> <a href="MyAlerts">Manage User Alerts for ${username}</a>
<%-- </authz:authorize>

<authz:authorize ifAnyGranted="PRIV_EBAY"> --%>
  <c:if test="${numEbayInventoryGroupToTakeAction > 0}">
    <br/><img src="../images/ico-warning.gif" align="absbottom" /> There are ${numEbayInventoryGroupToTakeAction} ebay ticket groups to take action. <a href="EbayInventory?tourId=ALL">Go to the eBay page</a>.<br/> 
  </c:if>
<%-- </authz:authorize> --%>

<h2>Next 10 Upcoming events</h2>
<display:table class="list" name="${events}" id="event" requestURI="." defaultsort="2">
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
    <display:column value="View" href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold"  />
    <display:column title="Date (Local Time)">
    	<b>${event.formattedEventDate}</b>
    </display:column>
    <display:column title="Event Name" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Venue" property="venue.building" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="State" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
</display:table>
<br />
<a href="UpcomingEvents">More upcoming events</a>

<h2>Top 10 Events With Most Tickets</h2>
<display:table class="list" name="${topTicketCountEvents}" id="event" requestURI="." defaultsort="8" defaultorder="descending">
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
    <display:column value="View" href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold"  />
    <display:column title="Date (Local Time)">
    	<b>${event.formattedEventDate}</b>
    </display:column>
    <display:column title="Event Name" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Venue" property="venue.building" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="State" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    <display:column title="Ticket Count" property="eventTicketStat.ticketCount" style="align:right" />
</display:table>
<br />
<a href="TopTicketCountEvents">More Events With Most Tickets</a>


<%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>

	<h2>My created crawls</h2>
	<display:table class="list" name="${createdCrawls}" id="crawl" requestURI="./" decorator="crawlTableDecorator">
	    <display:column title="Name" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}"/>
	    	<c:choose>
	    		<c:when test="${fn:length(crawl.name) < 40 }">
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=./"><b>${crawl.name}</b></a>
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawl.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawl.name, fn:length(crawl.name) - 20, fn:length(crawl.name))}" />
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=./" tooltip="${crawl.name}"><b>${part1}...${part2}</b></a>
	    		</c:otherwise>
	    	</c:choose>    	    	
	    </display:column>
	    <display:column title="Status" sortable="true" style="width: 90px;" decorator="crawlStatusColumnDecorator" value="${crawl}" />
	    <display:column title="Created" sortable="true">
			<fmt:formatDate value="${crawl.created}" pattern="yyyy/MM/dd HH:mm:ss" />
	    </display:column>
	    <display:column title="Tour" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<c:set var="crawlTour" value="${crawl.tour}" />
	    	<c:choose>    	
	    		<c:when test="${fn:length(crawlTour.name) < 40 }">
			    	<a href="BrowseEvents?tourId=${crawlTour.id}">${crawlTour.name}</a>    
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawlTour.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawlTour.name, fn:length(crawlTour.name) - 20, fn:length(crawlTour.name))}" />
			    	<a href="BrowseEvents?id=${crawlTour.id}" tooltip="${crawlTour.name}">${part1}...${part2}</a>    
	    		</c:otherwise>
	    	</c:choose>    
	    </display:column>
	    <display:column title="Event" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<c:set var="crawlEvent" value="${crawl.event}" />
	    	<c:choose>
	    		<c:when test="${empty crawlEvent}">
	    			AUTO
	    		</c:when>
	    		<c:otherwise>
				     <a href="BrowseTickets?eventId=${crawlEvent.id}" tooltip="${crawlEvent.name} - <fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" /> - ${crawlEvent.venue.building}">
							<fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" />
				     </a>
	    		</c:otherwise>
	    	</c:choose>
	    </display:column>
	    <display:column sortable="true">
			<a href="RedirectToListingPage?id=${crawl.id}"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a>
    	</display:column>

	</display:table>

	<h2>My last updated crawls</h2>
	<display:table class="list" name="${lastUpdatedCrawls}" id="crawl" requestURI="./" decorator="crawlTableDecorator">
	    <display:column title="Name" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}" align="absbottom" />
	    	<c:choose>
	    		<c:when test="${fn:length(crawl.name) < 40 }">
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=./"><b>${crawl.name}</b></a>
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawl.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawl.name, fn:length(crawl.name) - 20, fn:length(crawl.name))}" />
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=./" tooltip="${crawl.name}"><b>${part1}...${part2}</b></a>
	    		</c:otherwise>
	    	</c:choose>    	    	
	    </display:column>
	    <display:column title="Status" sortable="true" style="width: 90px;" decorator="crawlStatusColumnDecorator" value="${crawl}" />
	    <display:column title="Last Updated" sortable="true">
			<fmt:formatDate value="${crawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" />
	    </display:column>
	    <display:column title="Tour" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<c:set var="crawlTour" value="${crawl.tour}" />
	    	<c:choose>    	
	    		<c:when test="${fn:length(crawlTour.name) < 40 }">
			    	<a href="BrowseEvents?tourId=${crawlTour.id}">${crawlTour.name}</a>    
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawlTour.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawlTour.name, fn:length(crawlTour.name) - 20, fn:length(crawlTour.name))}" />
			    	<a href="BrowseEvents?id=${crawlTour.id}" tooltip="${crawlTour.name}">${part1}...${part2}</a>    
	    		</c:otherwise>
	    	</c:choose>    
	    </display:column>
	    <display:column title="Event" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<c:set var="crawlEvent" value="${crawl.event}" />
	    	<c:choose>
	    		<c:when test="${empty crawlEvent}">
	    			AUTO
	    		</c:when>
	    		<c:otherwise>
				     <a href="BrowseTickets?eventId=${crawlEvent.id}" tooltip="${crawlEvent.name} - <fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" /> - ${crawlEvent.venue.building}">
							<fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" />
				     </a>
	    		</c:otherwise>
	    	</c:choose>
	    </display:column>
	    <display:column sortable="true">
			<a href="RedirectToListingPage?id=${crawl.id}"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a>
    	</display:column>
	</display:table>	
	
<%-- </authz:authorize> --%>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${eventBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_' + eventId).attr('class', 'star' + response);  							       
			 if (response == "On") {
			 	$('#star_' + eventId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + eventId).parent().parent().css('background-color', null);
			 }				   							      
		   });
  };
</script>
