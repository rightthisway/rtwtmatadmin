<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	Home &gt; Useful Links
</div>

<h1>Useful Links</h1>

<div class="info">
	<div class="infoText">If you want to add a link on this page, please send us <a href="javascript:void(0)" onclick="showFeedbackDialog()">a feedback</a>.</div>
</div>

<h2>Merchant Sites</h2>
<table>
	<colgroup>
		<col width="200" />
		<col width="200" />
	</colgroup>
	<tr>
		<td><img src="../images/ico-ebay.gif" align="absbottom" /> <a href="http://tickets.ebay.com">eBay</a></td>
		<td><img src="../images/ico-eimarketplace.gif" align="absbottom" /> <a href="http://www.eimarketplace.com">EI Market Place</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-eventinventory.gif" align="absbottom" /> <a href="http://www.eventinventory.com/search/byevent.cfm?restart=yes&client=1337">Event Inventory</a></td>
		<td><img src="../images/ico-fansnap.gif" align="absbottom" /> <a href="http://www.fansnap.com">Fan Snap</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-getmein.gif" align="absbottom" /> <a href="http://www.getmein.com">GetMeIn</a></td>
		<td><img src="../images/ico-razorgator.gif" align="absbottom" /> <a href="http://www.razorgator.com">RazorGator</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-seatwave.gif" align="absbottom" /> <a href="http://www.seatwave.com">SeatWave</a></td>
		<td><img src="../images/ico-stubhub.gif" align="absbottom" /> <a href="http://www.stubhub.com">StubHub</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-ticketmaster.gif" align="absbottom" /> <a href="http://teamexchange.ticketmaster.com">Ticket Master</a></td>
		<td><img src="../images/ico-ticketnetwork.gif" align="absbottom" /> <a href="http://www.ticketnetwork.com">Ticket Network</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-ticketsnow.gif" align="absbottom" /> <a href="http://www.ticketsnow.com">Tickets Now</a></td>
		<td><img src="../images/ico-ticketnetwork.gif" align="absbottom" /> <a href="http://www.zeromarkup.com">ZeroMarkup (Ticket Network)</a></td>
	</tr>
	<tr>
		<td><img src="../images/ico-viagogo.gif" align="absbottom" /> <a href="http://www.viagogo.com">Viagogo</a></td>
		<td><img src="../images/ico-wstickets.gif" align="absbottom" /> <a href="http://www.wstickets.com">wstickets</a></td>
	</tr>
</table>