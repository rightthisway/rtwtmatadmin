<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="..">Home</a> 
  &gt; Starred
</div>

<h1>Starred</h1>

<table width="100%">
  <tr>
    <td style="border-right-style:solid; border-width:1px; padding-right: 5px;" valign="top">
	  <h2>Artists/Teams</h2>
	<display:table class="list" name="${artistBookmarks}" id="artistBookmark" requestURI="Bookmarks">
	    <display:column style="width:20px"><div id="artist_star_${artistBookmark.artist.id}" class="starOn" onclick="toggleBookmarkArtist(${artistBookmark.artist.id})"></div></display:column>
	    <display:column title="Artist" sortable="true" property="artist.name" href="BrowseArtist" paramId="artistId" paramProperty="artist.id" />
	</display:table>
    </td>
    <td style="border-right-style:solid; border-width:1px; padding-right: 5px" valign="top">
	  <h2>Artist/Seasons</h2>
	  <%-- <display:table class="list" name="${tourBookmarks}" id="tourBookmark" requestURI="Bookmarks">
        <display:column style="width:20px"><div id="tour_star_${tourBookmark.tour.id}" class="starOn" onclick="toggleBookmarkTour(${tourBookmark.tour.id})"></div></display:column>
        <display:column title="Dates" sortable="true">
          <fmt:formatDate pattern="MM/dd/yyyy" value="${tourBookmark.tour.startDate}"/>
          -
          <fmt:formatDate pattern="MM/dd/yyyy" value="${tourBookmark.tour.endDate}"/>
        </display:column>
        <display:column title="Name"  sortable="true" property="tour.name" href="BrowseEvents" paramId="tourId" paramProperty="tour.id" />
        <display:column title="Location" sortable="true" property="tour.location" />
		
	  </display:table> --%>
    </td>
    <td valign="top">
	  <h2>Events</h2>
	<display:table class="list" name="${eventBookmarks}" id="eventBookmark" requestURI="Bookmarks">
	    <display:column style="width:20px"><div id="event_star_${eventBookmark.event.id}" class="starOn" onclick="toggleBookmarkEvent(${eventBookmark.event.id})"></div></display:column>
	    <display:column title="Date" sortable="true" property="event.date" format="{0,date,yyyy/MM/dd}" href="BrowseTickets" paramId="eventId" paramProperty="event.id" />
	    <display:column title="Artist/Team" sortable="true" property="event.artist.artist.name" />
	    <display:column title="Location" sortable="true">${eventBookmark.event.venue.building}, ${eventBookmark.event.venue.city}, ${eventBookmark.event.venue.state}, ${eventBookmark.event.venue.country}</display:column>
	</display:table>

    </td>
  </tr>
  <tr>
    <td valign="top" colspan="3">
	  <h2>Tickets</h2>
<fmt:formatDate pattern="MM/dd/yyyy" value="<%=new java.util.Date()%>" var="today" />	  
<display:table class="list" name="${ticketBookmarks}" id="ticketBookmark" requestURI="Bookmarks" pagesize="50">

    <display:column>
    	<div id="ticket_star_${ticketBookmark.ticket.id}" class="starOn" onclick="toggleBookmarkTicket('${ticketBookmark.ticket.id}')"></div>
    </display:column>
	<display:column title="Event" sortable="true" sortProperty="ticket.event.date.time" class="text-align-center">
	  <a href="BrowseTickets?eventId=${ticketBookmark.ticket.event.id}">
	    <fmt:formatDate value="${ticketBookmark.ticket.event.date}" pattern="yyyy/MM/dd" />
	    <fmt:formatDate value="${ticketBookmark.ticket.event.time}" pattern="HH:mm" />
	  </a>
	</display:column>
	<display:column title="Artist" sortable="true" property="ticket.event.artist.name" sortName="ticket.event.artist.name" class="text-align-center" />
    <display:column title="Rem. Qty" sortable="true" sortName="remainingQuantity" class="bold text-align-center">
    	<span tooltip="Lot Size: ${ticketBookmark.ticket.lotSize}">${ticketBookmark.ticket.remainingQuantity}</span>
    </display:column>
	<display:column title="Zone" sortable="true" property="ticket.category.symbol" sortName="category" class="bold text-align-center" />
    <display:column title="Section" sortable="true" sortName="section" class="bold text-align-center"  decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="${artist.tourType == 'SPORT'}">
	      <c:choose>
	        <c:when test="{$ticketBookmark.ticket.section == ticketBookmark.ticket.normalizedSection}">
	          ${ticketBookmark.ticket.section}
	        </c:when>
	        <c:otherwise>
	          <span class="grayCell">
	            <c:set var="formattedSection" value="<font color='#000000'>${ticketBookmark.ticket.normalizedSection}</font>"/>
	            ${fn:replace(ticketBookmark.ticket.section, ticketBookmark.ticket.normalizedSection, formattedSection)}
	          </span>
	        </c:otherwise>
	      </c:choose>
        </c:when>

        <c:when test="${artist.tourType == 'THEATER'}">
	      <c:choose>
	        <c:when test="${ticketBookmark.ticket.section == ticketBookmark.ticket.normalizedSection}">
	          ${ticketBookmark.ticket.section}
	        </c:when>
	        <c:otherwise>
	          ${ticketBookmark.ticket.normalizedSection}
	          <span class="grayCell">
	            (${ticketBookmark.ticket.section})
	          </span>
	        </c:otherwise>
	      </c:choose>        
		</c:when>
        
        <c:otherwise>
          ${ticketBookmark.ticket.section}
		</c:otherwise>        
      </c:choose>


    </display:column>
    <display:column title="Row" sortable="true" sortName="row" class="bold text-align-center" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="{$ticketBookmark.ticket.row == ticketBookmark.ticket.normalizedRow}">
          ${row}
        </c:when>
        <c:otherwise>
          <span class="grayCell">
            <c:set var="formattedRow" value="<font color='#000000'>${ticketBookmark.ticket.normalizedRow}</font>"/>
            ${fn:replace(ticketBookmark.ticket.row, ticketBookmark.ticket.normalizedRow, formattedRow)}
          </span>
        </c:otherwise>
      </c:choose>
    </display:column>
    <display:column title="Wholesale Price" sortable="true" property="ticket.adjustedCurrentPrice" sortName="currentPrice" format="{0,number,currency}" class="bold text-align-right" />
    <display:column title="Online Price" sortable="true" sortName="buyItNowPrice" class="text-align-right" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
      <c:choose>
        <c:when test="${ticketBookmark.ticket.buyItNowPrice > 0}">
          <fmt:formatNumber value="${ticketBookmark.ticket.buyItNowPrice}"
               type="currency" minFractionDigits="2"/>
        </c:when>
        <c:otherwise>
        </c:otherwise>
      </c:choose>
        <c:set var="timeLeft">
	    <c:choose>
	      <c:when test="${empty ticketBookmark.ticket.numSecondsBeforeEndDate}">
	      </c:when>
	      <c:when test="${ticketBookmark.ticket.numSecondsBeforeEndDate < 0}">
	         (expired)
	      </c:when>
	      <c:when test="${ticketBookmark.ticket.numSecondsBeforeEndDate > 24 * 3600 * 1000}">
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${(ticketBookmark.ticket.numSecondsBeforeEndDate / (24 * 3600 * 1000))}"/> days
	      </c:when>
	      <c:when test="${ticketBookmark.ticket.numSecondsBeforeEndDate > 3600 * 1000}">
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticketBookmark.ticket.numSecondsBeforeEndDate % (24 * 3600 * 1000)) / (3600 * 1000))}"/> hours
	      </c:when>
	      <c:otherwise>
	         Ending in <fmt:formatNumber type="number" maxFractionDigits="0" value="${((ticketBookmark.ticket.numSecondsBeforeEndDate % (3600 * 1000)) / (60 * 1000))}"/> minutes
	      </c:otherwise>
	    </c:choose>	          
        </c:set>
	       <c:choose>
	         <c:when test="${ticketBookmark.ticket.ticketType == 'AUCTION'}">
 	           <span tooltip="${timeLeft}">
 	             <c:choose>
 	               <c:when test="${ticketBookmark.ticket.numSecondsBeforeEndDate > 3600 * 24}">
 	                 <img src="../images/ico-auction.gif" align="absbottom"/>
 	               </c:when>
 	               <c:otherwise>
 	                 <img src="../images/ico-auction-ending.gif" align="absbottom"/>
 	               </c:otherwise>
 	             </c:choose>
  	    	   </span>
	         </c:when>
	       </c:choose>
    </display:column>

	<display:column title="Eval" class="text-align-right" sortProperty="ticket.valuationFactor"><c:choose><c:when test="${not empty ticketBookmark.ticket.valuationFactor}">${ticketBookmark.ticket.valuationFactor}</c:when><c:otherwise>-</c:otherwise></c:choose></display:column>
	<display:column title="AI">n/a</display:column>		
		
    <display:column media="html" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper" title="Seller">
       <span style="white-space:nowrap">
  	    <a href="RedirectToItemPage?id=${ticketBookmark.ticket.id}" tooltip="${ticketBookmark.ticket.siteId} - ${ticketBookmark.ticket.itemId}"><img src="../images/ico-${ticketBookmark.ticket.siteId}.gif" border="0" align="absbottom"/>
	         ${ticketBookmark.ticket.seller}
	    </a>
  	    	 <c:if test="${ticketBookmark.ticket.ticketType != 'AUCTION' and not empty ticketBookmark.ticket.numSecondsBeforeEndDate}">
 	           <span tooltip="${timeLeft}">
 	             <c:choose>
 	               <c:when test="${ticketBookmark.ticket.numSecondsBeforeEndDate > 3600 * 24}">
 	                 <img src="../images/ico-clock.gif" align="absbottom"/>
 	               </c:when>
 	               <c:otherwise>
 	                 <img src="../images/ico-clock-ending.gif" align="absbottom"/>
 	               </c:otherwise>
 	             </c:choose>
    	       </span>
	         </c:if>

		    <c:choose>
		    	<c:when test="${ticketBookmark.ticket.ticketDeliveryType == 'EDELIVERY'}">
			    	<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
		    	</c:when>
		    	<c:when test="${ticketBookmark.ticket.ticketDeliveryType == 'INSTANT'}">
			    	<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
		    	</c:when>
		    </c:choose>			    	
	         
     </span>
    </display:column>
</display:table>
      
    </td>
  </tr>
</table>

<script type="text/javascript">
  function toggleBookmarkArtist(artistId) {
    	BookmarkDwr.toggleBookmarkArtist(artistId, 
  							   function(response) {
								 $('#artist_star_' + artistId).attr('class', 'star' + response);  							       
  							   });
  }

  function toggleBookmarkTour(tourId) {
    	BookmarkDwr.toggleBookmarkTour(tourId, 
  							   function(response) {
								 $('#tour_star_' + tourId).attr('class', 'star' + response);  							       
  							   });
  }

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
  							   function(response) {
								 $('#event_star_' + eventId).attr('class', 'star' + response);  							       
  							   });
  }
  
  function toggleBookmarkTicket(ticketId) {
    	BookmarkDwr.toggleBookmarkTicket(ticketId, 
  							   function(response) {
								 $('#ticket_star_' + ticketId).attr('class', 'star' + response);  							       
  							   });
  }
  

</script>