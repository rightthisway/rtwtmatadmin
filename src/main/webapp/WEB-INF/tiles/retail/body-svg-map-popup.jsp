<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery.svg.pan.zoom.js"></script>


<script type="text/javascript">
	$(document).ready(function(){
	$('#zone_letters').show();
	$('#section_letters').show();
	//var zone = 'h';
	var obj = {};
	<c:forEach var="country" items="${colorsMap}">
		obj["${country.key}"] = "${country.value}";
	</c:forEach>
	//alert(obj['a']);
		var rects = $('#shapes').children().each(function(){
		var zone = $(this).attr('id').split('-:-')[0];
		if(obj[zone] == null){
			$(this).css('fill','#F4ED6D');
			$(this).css('fill-opacity','0.7');
		}else{
		//alert(${colorsMap[h]});
		//alert("${colorsMap["+zone+"]}");
			$(this).css('fill',obj[zone]);
			$(this).css('fill-opacity','0.7');
		}
		});
		
		// svg zoom in and zoom out
	
		var svgZoomClicks = 0;
		var example = $("#svg1").svgPanZoom();
		$("#svgZoomIn").click(function(){
				if(svgZoomClicks < 10){
					example.zoomIn();
					svgZoomClicks++;
				}else{
					J('#svgZoomIn').prop('disabled','disabled');
				}
			
		});
				
		$("#svgZoomOut").click(function(){
				if(svgZoomClicks > 0){
					example.zoomOut();
					svgZoomClicks--;
				}else{
					$('#svgZoomOut').prop('disabled','disabled');
				}
			
		});
		
	});
</script>

<div>
	<img id="svgZoomIn" style="width:45px;height:45px;float:left;margin-left:1%;" src="../images/zoomIn.jpg" /> &nbsp <img style="width:45px;height:45px;float:left;margin-left:1%;" id="svgZoomOut" src="../images/zoomOut.jpg" />
	
		<svg id="svg1" height="545" version="1.1" width="681" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
  		style="overflow: hidden; position: relative;" viewBox="0 0 2560 2048" preserveAspectRatio="xMinYMin">
  		<desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Rapha�l 2.1.4</desc>
  		<defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
  		
  		<image id="mapImg" x="0" y="0" width="2560" height="2048" preserveAspectRatio="none" xmlns:xlink="http://www.w3.org/1999/xlink" 
  		xlink:href="${tmatSvgMapDetails.svgMapPath}" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);" stroke-width="1"></image>
  		
  		${tmatSvgMapDetails.svgText}
  		</svg>
		</div>
  	@rtwsvg
  </div>