<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<fmt:formatDate pattern="MM/dd/yyyy" value="<%=new java.util.Date()%>" var="today" />

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 

	<c:if test="${not empty event}">
		&gt; <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
		&gt; <a href="BrowseEvents?tourId=${tour.id}">${tour.name}</a>
		&gt; ${event.formattedEventDate}
	</c:if>
</div>

<div>
	<div id="eventLinks">
		<b>Event:</b>	

        <span id="LoadingEventSpan"><img src="../images/process-running.gif" align="absbottom" />Loading...</span>
        <span id="eventNavigationSpan" style="display:none">
	        <c:choose>
			  <c:when test="${not empty previousEvent}">
				<a href="#" onclick="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); location.href='${ticketsPage}?eventId=${previousEvent.id}' + anchor" tooltip="${previousEvent.name} ${previousEvent.formattedEventDate} ${previousEvent.formattedVenueDescription}" style="color: #850001">Previous</a>
			  </c:when>
			  <c:otherwise>
				<font color="#cccccc">Previous</font>
			  </c:otherwise>
	        </c:choose>		
			
			<select onchange="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); document.location.href='${ticketsPage}?eventId=' + this.value + anchor">
				<c:forEach var="e" items="${tour.events}">
					<option value="${e.id}" <c:if test="${e.id == event.id}">selected</c:if> >${e.name}&nbsp;${e.formattedEventDate}&nbsp;${e.formattedVenueDescription}				
					</option>
				</c:forEach>
			</select>
			
			<c:choose>
			  <c:when test="${not empty nextEvent}">
				<a href="#" onclick="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); location.href='${ticketsPage}?eventId=${nextEvent.id}' + anchor" tooltip="${nextEvent.name} ${nextEvent.formattedEventDate} ${nextEvent.formattedVenueDescription}" style="color: #850001">Next</a>
			  </c:when>
			  <c:otherwise>
				<font color="#cccccc">Next</font>
			  </c:otherwise>
			</c:choose>
		</span>	
	</div>
	
	<div id="quickLinks">
	<c:choose>
	  <%-- <authz:authorize ifAnyGranted="PRIV_ANALYTIC"> --%>
		  <c:when test="${view == 'compact'}">
	        <a href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">Browse Tickets Analytic</a>
		  </c:when>
		  <c:otherwise>
	        <a href="BrowseTickets?eventId=${event.id}&view=compact">Browse Tickets Normal</a>
		  </c:otherwise>
		</c:choose>
	  <%-- </authz:authorize>
	  <authz:authorize ifAnyGranted="PRIV_ANALYTIC"> --%>	  
	  	| <a href="MyAlerts?eventId=${event.id}">Create Alert</a>
	 <%--  </authz:authorize>
	  
	  <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
		| <a href="EditorEditEvent?id=${event.id}&action=edit">Edit Event</a>
	  <%-- </authz:authorize>
	  <authz:authorize ifAnyGranted="PRIV_CATEGORY"> --%>
		| <a href="#" onclick="location.href='EditorEditEventCategories?eventId=${event.id}'">Categorization</a>
	  <%-- </authz:authorize>
	  <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
		| <a href="#" onclick="location.href='EditorValuationFactors?tourId=${tour.id}&eventId=${event.id}'">Valuation Factors</a>
		|
	  <%-- </authz:authorize>
	  <authz:authorize ifAnyGranted="PRIV_SHORTS"> --%>
	    <a href="TMATBrowseShorts?eventId=${event.id}">Shorts</a>
	 <%--  </authz:authorize> --%>
	</div>
</div>
<div style="clear: both"></div>

<div id="wholePageDiv" style="display:none">
<h1>Browse Tickets</h1>

<div style="float: right">
  <span class="refreshLink" style="display:none">
    <a href="#" onclick="return updateCrawls()">
      <img src="../images/ico-reload.gif" align="absbottom"/>
      Refresh
    </a>
  </span>
  <span class="refreshedRecently" style="display:none">
      <img src="../images/ico-reload.gif" align="absbottom"/>
      <i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
  </span>
  </span>
  <span class="refreshCrawlInfoSpan" style="display:none">
    <img src="../images/process-running.gif" align="absbottom"/>
      <span class="refreshCrawlInfo" style="color:green">
      </span>
  </span>
</div>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<h2><a onclick="toggleEventDetails()" style="cursor: pointer">Event Details <img id="event-details-expand-img" src="../images/ico-expand.gif" /></a></h2>
  <div id="event-details-info">
	<table width="100%">
	  <tr>
	    <td width="100px">
			<c:if test="${event.eventStatus == 'EXPIRED'}"><font color="red"><b>EXPIRED</b></font><br /></c:if>
	    </td>
	    <td>

			<b>${tour.tourLabel}:</b> <a href="BrowseEvents?tourId=${tour.id}">${tour.name}</a> <br />
			<b>Event:</b> ${event.name}<br />
			<b>${tour.artistLabel}:</b> <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
			<div>
				<div style="float:left"><b>Event Date:</b></div>
				<div id="star_event_${param.eventId}" class="starOff" style="margin-top: -4px" onclick="toggleBookmarkEvent(${param.eventId})"></div>
				<div style="float:left">
					${event.formattedEventDate}
				</div>
			</div>
	    </td>
	    <td>
			<b>Venue:</b> ${venue.building}<br />
			<b>Venue City:</b> ${venue.city}<br />
			<b>Venue State:</b> ${venue.state}<br />
			<b>Venue Country:</b> <c:if test="${not empty venue.country && venue.country != 'null' }"><img src="../images/flags/${venue.country}.gif" />${venue.country}</c:if><br />
		    <b>Last Full Update:</b>
		   <c:choose>
			  <c:when test="${not empty lastFullUpdate}">
			    <fmt:formatDate value="${lastFullUpdate}" pattern="yyyy/MM/dd HH:mm:ss" />
			  </c:when>
			  <c:otherwise>
			    <i>(crawl queued)</i>
			  </c:otherwise>
			</c:choose>
	    </td>	    
	    <td style="text-align: right">
	<c:set var="eventTicketCount" value="${event.ticketCount}" />
	<c:choose>
		<c:when test="${eventTicketCount > 0}">
			<c:set var="nonDuplicateEventTicketCount" value="${event.nonDuplicateTicketCount}" />
			<c:set var="nonDuplicateEventTicketQuantitySum" value="${event.nonDuplicateTicketQuantitySum}" />
			
			<b>Total number of entries (excl. duplicates):</b> <fmt:formatNumber value="${nonDuplicateEventTicketCount}" /><br />
			<b>Total number of duplicate entries:</b> <fmt:formatNumber value="${eventTicketCount - nonDuplicateEventTicketCount}" /><br />	
			<b>Total number of tickets(excl. duplicates):</b> <fmt:formatNumber value="${nonDuplicateEventTicketQuantitySum}" /><br />	
		    <b>Filtered number of entries:</b> ${fn:length(webTickets)}<br />
	 	 	<b>Filtered number of tickets:</b> ${ticketCount}<br />
			<b>Tickets Offered / Seats Available:</b> <fmt:formatNumber value="${qtyOffered}" /> / <fmt:formatNumber value="${qtyAvailable}" /><br />					
		</c:when>
		<c:otherwise>
			<b>No tickets</b>
		</c:otherwise>
	</c:choose>
	    </td>
	  </tr>
	</table>

<c:choose>
	<c:when test="${removeDuplicatePolicy == 'NONE'}">
		<c:set var="statEventTicketCount" value="${eventTicketCount}" />
		<c:set var="statEventTicketQuantitySum" value="${event.ticketQuantitySum}" />
	</c:when>
	<c:otherwise>
		<c:set var="statEventTicketCount" value="${nonDuplicateEventTicketCount}" />
		<c:set var="statEventTicketQuantitySum" value="${nonDuplicateEventTicketQuantitySum}" />
	</c:otherwise>
</c:choose>
</div>
<div id="container-tabs" style="margin-top: 5px">
  <ul>
    <li><a href="#fragment-tickets"><span>Tickets</span></a></li>
    <c:if test="${view != 'compact'}">
	    <li><a href="#fragment-history"><span>History</span></a></li>
	    <li><a href="#fragment-chart"><span>Charts</span></a></li>
	    <li><a href="#fragment-stats"><span>Stats</span></a></li>
	</c:if>
	<%-- <authz:authorize ifAnyGranted="PRIV_CRAWL"> --%>
      <li><a href="#fragment-crawls"><span>Crawls</span></a></li>
   <%--  </authz:authorize> --%>
    <li><a href="#fragment-filters"><span>Filters</span></a></li>
    
  </ul>
  
  <div id="fragment-tickets">
  
 
<c:if test="${view == 'analytic'}">
	<a href="${ticketsPage}?eventId=${event.id}&liqcalc=1&nocrawl=true">Calculate Liquidity</a>
  	<br />
  	<c:if test="${liquidityCalculation}">
	<div class="info">
		<div class="infoText">
			IN is the number of tickets that were put on the exchange during the last 24 hours.<br />
			OUT is the number of tickets that were put off (expired or sold) the exchange during the last 24 hours.<br />			
			Liquidity ratio is computed as follow: <i>liquidity_ratio = 100 * (OUT - IN) / START</i><br />
			with START the number of active tickets 24 hours ago.
			<br /><br />			
			Note that the figures in the tables below depend on the filters that you select.
		</div>
	</div>
	
	<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<th><img src="../images/ico-${siteId}.gif"/></th>
				</c:forEach>
				<th class="total">Total incl. dup.</th>
				<th class="total">Total excl. dup.</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Start Quantity</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${startTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${startTicketCountBySiteId[siteId]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCount}" /></td>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>IN tickets</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${inTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<font color="green"><fmt:formatNumber pattern="0" value="${inTicketCountBySiteId[siteId]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><font color="green">${inTicketCount}</font></td>
				<td class="total" align="right"><font color="green">${inTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>OUT tickets</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${outTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<font color="red"><fmt:formatNumber pattern="0" value="${outTicketCountBySiteId[siteId]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>				
				</c:forEach>
				<td class="total" align="right"><font color="red">${outTicketCount}</font></td>
				<td class="total" align="right"><font color="red">${outTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>End Quantity</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${endTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${endTicketCountBySiteId[siteId]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCount}" /></td>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>Liquidity ratio</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">				
					<td align="right">
						<c:choose>
							<c:when test="${liquidityRatioBySiteId[siteId] == 0}">-</c:when>
							<c:when test="${liquidityRatioBySiteId[siteId] > 0}">
								+<fmt:formatNumber pattern="0.00" value="${liquidityRatioBySiteId[siteId]}" />
							</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0.00" value="${liquidityRatioBySiteId[siteId]}" />							
							</c:otherwise>
						</c:choose>
					</td>
				</c:forEach>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatio == 0}">-</c:when>
						<c:when test="${liquidityRatio > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatio}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatio}" />							
						</c:otherwise>
					</c:choose>
				</td>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy == 0}">-</c:when>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />							
						</c:otherwise>
					</c:choose>
				</td>

			</tr>
			<tr>
				<td>% of Market</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentBySiteId[siteId]}" />%</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercent}" />%</td>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentWithDuplicateRemovalPolicy}" />%</td>
			</tr>
		</tbody>
	</table>
	<br />

	<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<th>${catSymbol}</th>
				</c:forEach>
				<th class="total">Total</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Start Quantity</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${startTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${startTicketCountByCategory[catSymbol]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>IN tickets</td>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${inTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<font color="green"><fmt:formatNumber pattern="0" value="${inTicketCountByCategory[catSymbol]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><font color="green">${inTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>OUT tickets</td>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${outTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<font color="red"><fmt:formatNumber pattern="0" value="${outTicketCountByCategory[catSymbol]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>				
				</c:forEach>
				<td class="total" align="right"><font color="red">${outTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>End Quantity</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${endTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${endTicketCountByCategory[catSymbol]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>Liquidity ratio</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${liquidityRatioByCategory[catSymbol] == 0}">-</c:when>
							<c:when test="${liquidityRatioByCategory[catSymbol] > 0}">
								+<fmt:formatNumber pattern="0.00" value="${liquidityRatioByCategory[catSymbol]}" />
							</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0.00" value="${liquidityRatioByCategory[catSymbol]}" />							
							</c:otherwise>
						</c:choose>
					</td>
				</c:forEach>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy == 0}">-</c:when>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />							
						</c:otherwise>
					</c:choose>
				</td>

			</tr>
			<tr>
				<td>% of Market</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentByCategory[catSymbol]}" />%</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentWithDuplicateRemovalPolicy}" />%</td>
			</tr>
			
		</tbody>
	</table>
	<br />
		<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<th>${catSymbol}</th>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Tickets Offered / Seats Available</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${offeredTicketsByCat[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${offeredTicketsByCat[catSymbol]}" />
							</c:otherwise>
						</c:choose>		
						/
						<c:choose>
							<c:when test="${availableTicketsByCat[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${availableTicketsByCat[catSymbol]}" />
							</c:otherwise>
						</c:choose>					
					</td>
				</c:forEach>
			</tr>		
		</tbody>
	</table>
	
</c:if>	
</c:if>	
  
   <div id="ticket-filter-toggle">
   <a onclick="toggleTicketFilter()" style="cursor: pointer">
	<b>Ticket Filter</b><img id="ticket-filter-expand-img" src="../images/ico-collapse.gif" />
   </a>  
   </div>	
  <div id="ticket-filter-info">
	  <span>
		Zone:
		<input type="checkbox" id="cat-ALL" class="cat-checkbox" onChange="changeCatFilter()" <c:if test="${catFilters['ALL']}">checked</c:if> />
		ALL	
		<input type="checkbox" id="cat-CATS" class="cat-checkbox" onChange="changeCatFilter()" <c:if test="${catFilters['CAT']}">checked</c:if> />
		Categorized	
		<input type="checkbox" id="cat-UNCATS" class="cat-checkbox" onChange="changeCatFilter()" <c:if test="${catFilters['UNCAT']}">checked</c:if> />
		Uncategorized	
		<c:forTokens items="${categorySymbols}" var="cat" delims=",">
			<input type="checkbox" id="cat-${cat}" class="cat-checkbox" onChange="changeCatFilter()" <c:if test="${catFilters[cat]}">checked</c:if> />
			${cat}	
			&nbsp;			
		</c:forTokens>
	  </span>
	  <br />
	  <span style="margin-left: 10px">
		Quantity:
		<input type="checkbox" id="quantity-ALL" class="quantity-checkbox" onChange="changeQuantityFilter()" <c:if test="${qtyFilters['ALL']}">checked</c:if> />
		ALL	
		<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
			<input type="checkbox" id="quantity-${qty}" class="quantity-checkbox" onChange="changeQuantityFilter()" <c:if test="${qtyFilters[qty]}">checked</c:if> />
			<c:choose>
				<c:when test="${11 == qty}">${qty}+</c:when>
				<c:otherwise>${qty}</c:otherwise>
			</c:choose>		
			&nbsp;			
		</c:forTokens>
	  </span>
	  <br/>
	  <span style="margin-left: 10px">
		<c:if test="${empty sectionFilter and empty rowFilter}">
		  <a href="#" id="expandAdvancedFilteringLink" onclick="$(this).hide(); $('#advancedFilteringDiv').show(); return false;" <c:if test="${not empty param.section or not empty param.row}">style="display:none"</c:if>>Filter by section and row&gt;</a>
		</c:if>
	  </span>
		<div id="advancedFilteringDiv" <c:if test="${empty sectionFilter and empty rowFilter}">style="display:none"</c:if>>
  		  <a href="${ticketsPage}?eventId=${event.id}&section=&row=">&lt;Discard Filtering by Section and Row</a>
		  <br/>
		  <form action="${ticketsPage}">
   		    Filter by:
		    <input type="hidden" name="eventId" value="${event.id}"/>
		    <b>Section:</b> <input type="text" name="section" value="${sectionFilter}"/>
		    <b>Row:</b> <input type="text" name="row" value="${rowFilter}"/>
		    <input type="submit" class="medButton" value="filter"/>
		  </form>
		</div>
<!-- 
  Category Group:
  ${catGroupName}
<select id="catScheme">
  <c:forEach var="catg" items="${catGroups}">
    <option value="${catg}">${catg}</option>
  </c:forEach>
</select> -->
   <input id="changeGroupButton" type="button" class="medButton" onclick="changeGroup()"  value="Change"/><br/><br/>
<div style="border-top:1px  dashed black;margin-top:4px;padding-top:4px; padding-bottom:5px">
	<br />
	<input type="checkbox" id="multisort-checkbox" onchange="changeMutisortCheckBox(this.checked)" <c:if test="${multisort}">checked</c:if> /> 
	<label for="multisort-checkbox">Multisort:</label>
	Sort by <select id="multisort_selector0" class="multisort_selector"></select>
	then by <select id="multisort_selector1" class="multisort_selector"></select>
	then by <select id="multisort_selector2" class="multisort_selector"></select>
	then by <select id="multisort_selector3" class="multisort_selector"></select>
</div>
</div>
<c:if test="${removeDuplicatePolicy == 'MANUAL'}">
  <b>Selected tickets:</b> <a href="#" onclick="return markAsDuplicate()"> Mark as duplicate</a> | <a href="#" onclick="return unmarkAsDuplicate()"> Unmark as duplicate</a> | <a href="#" onclick="untickAll(); return false;"> Untick tickets</a> | <a href="#" onclick="return markAsAdmitOne()"> Mark as AdmitOne</a> | <a href="#" onclick="return unmarkAsAdmitOne()"> Unmark as AdmitOne</a>
  <br/><br/>
</c:if>

<div id="ticketTableContainerLoading"><img src="../images/process-running.gif" align="absbottom" /> Loading <span id="ticketTableContainerLoadingPercent"></span><br /><br /></div>
<div id="ticketTableContainer"></div>

<br />

<c:choose>
    <c:when test="${not empty webTickets}">
      <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
        <img src="../images/ico-add.gif" align="absbottom" /> <a href="EbayAddInventory?eventId=${event.id}">Add to ZSP Inventory</a>
      <%-- </authz:authorize> --%>
    </c:when>
  <c:otherwise>
	<c:choose>
		<c:when test="${eventTicketCount == 0}">
			This event has no tickets. This event might have expired or does not have crawls.
		</c:when>
		<c:otherwise>
			This event has tickets but no tickets match the filters. You can try to <a href="${ticketsPage}?eventId=${event.id}&reset=1">Reset the filters</a>
			or change your filter settings.			
		</c:otherwise>
	</c:choose>
  </c:otherwise>
</c:choose>
  </div>

  <c:if test="${view != 'compact'}">

  <div id="fragment-history">
  
	<div class="info">
		<div class="infoText">
			IN is the number of tickets that were put on the exchange during the given day.<br />
			OUT is the number of tickets that were put off (expired or sold) the exchange during the given day.<br /><br />
			Liquidity ratio and % of Market are computed as follow: <br />
			<ul>
			<i>liquidity_ratio = 100 * (OUT - IN) / START</i><br />
			<i>% of Market = 100 * OUT / START</i><br />
			</ul>
			with START the number of active tickets at the beginning of the given day.						
		</div>
	</div>
  
    <display:table class="list" name="${stats}" id="stat" requestURI="${ticketsPage}" pagesize="${pageSize}">

		<display:column title="Date" property="date" format="{0,date,yyyy/MM/dd}" class="bold" />
		<display:column title="Start Quantity" property="startTicketCount" class="text-align-right" />
		<display:column title="End Quantity" property="endTicketCount" class="text-align-right" />
		<display:column title="IN" property="inTicketCount" class="text-align-right" style="color:green" />
		<display:column title="OUT" property="outTicketCount" class="text-align-right" style="color:red" />
		<display:column title="Liquidity Ratio" class="text-align-right">
			<fmt:formatNumber pattern="0.00" value="${stat.liquidityRatio}" />
		</display:column>
		<display:column title="% of Market" class="text-align-right">
			<fmt:formatNumber pattern="0.00" value="${stat.marketPercent}" />%
		</display:column>
		<display:column title="Min Price" property="minPrice" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Max Price" property="maxPrice" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Min Qty" property="minQuantity" class="text-align-right" />
		<display:column title="Max Qty" property="maxQuantity" class="text-align-right" />
		<display:column title="Average" property="average" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Median" property="median" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Deviation" property="deviation" format="{0,number,###,##0.00}" class="text-align-right" />
	</display:table>
  </div>
        
  <div id="fragment-chart">  
  </div>
  
  <div id="fragment-stats">
    <a name="eventStat"/>
    <h3>Number of tickets per category and site</h3>
    <c:choose>
	  <c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	  </c:when>
	  <c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forEach var="siteId" items="${constants.siteIds}">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1">
							<img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" />
						</a>
					</th>
				</c:forEach>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="category" items="${categories}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
				
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1"><b>Cat. ${category.symbol}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forEach var="siteId" items="${constants.siteIds}">
							<c:set var="total" value="${total + siteStatByCategory[category.symbol][siteId]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty siteStatByCategory[category.symbol][siteId]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&cat=${category.id}&reset=1&uncheckSiteFilters=1">
											${siteStatByCategory[category.symbol][siteId]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forEach>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketQuantitySum}" />%</b>
						</td>
					</tr>
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
		
				<c:choose>
					<c:when test="${(row % 2) == 0}">
						<c:set var="rowClass" value="even" />
					</c:when>
					<c:otherwise>
						<c:set var="rowClass" value="odd" />
					</c:otherwise>				
				</c:choose>
		
				<tr class="${rowClass}">
					<td>
						<a href="${ticketsPage}?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
					</td>
					<c:set var="total" value="0" />
					<c:forEach var="siteId" items="${constants.siteIds}">
						<c:set var="total" value="${total + siteStatByCategory['UNCAT'][siteId]}" />
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&cat=UNCAT&reset=1">
								${siteStatByCategory['UNCAT'][siteId]}
							</a>
						</td>
					</c:forEach>
					<td align="right">
						<a href="${ticketsPage}?eventId=${event.id}&reset=1">							
							<b>${total}</b>
						</a>
					</td>
					<td align="right">
						<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketQuantitySum}" />%</b>
					</td>
				</tr>
		
			</tbody>
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">			
							<c:set var="total" value="${total + totalStatBySite[siteId]}" />					
							${totalStatBySite[siteId]}
						</td>				
					</c:forEach>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatBySite[siteId] * 100 / statEventTicketQuantitySum}" />%
						</td>				
					</c:forEach>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
			
		</table>
	</c:otherwise>
  </c:choose>

  <h3>Number of ticket entries per site and quantity</h3>

  <c:choose>
	<c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	</c:when>
	<c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&reset=1">${quantity}</a>
					</th>
				</c:forTokens>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="siteId" items="${constants.siteIds}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1"><img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" /><b>${siteId}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
							<c:set var="total" value="${total + quantityStatBySiteId[siteId][quantity]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty quantityStatBySiteId[siteId][quantity]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&quantities=${quantity}&reset=1&uncheckSiteFilters=1">
											${quantityStatBySiteId[siteId][quantity]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forTokens>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantities}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
						</td>
					</tr>
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
			</tbody>
		
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td align="right">			
							<c:set var="total" value="${total + totalStatByQuantity[quantity]}" />					
							${totalStatByQuantity[quantity]}
						</td>				
					</c:forTokens>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / statEventTicketCount}" />%
						</td>				
					</c:forTokens>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
		
		</table>
	  </c:otherwise>
    </c:choose>

   <h3>Number of ticket entries per category and quantity</h3>

  <c:choose>
	<c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	</c:when>
	<c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&reset=1">${quantity}</a>
					</th>
				</c:forTokens>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="category" items="${categories}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1"><b>Cat. ${category.symbol}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
							<c:set var="total" value="${total + quantityStatByCategory[category.symbol][quantity]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty quantityStatByCategory[category.symbol][quantity]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&quantities=${quantity}&reset=1">
											${quantityStatByCategory[category.symbol][quantity]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forTokens>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
						</td>
					</tr>			
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
		
				<c:choose>
					<c:when test="${(row % 2) == 0}">
						<c:set var="rowClass" value="even" />
					</c:when>
					<c:otherwise>
						<c:set var="rowClass" value="odd" />
					</c:otherwise>				
				</c:choose>
				<tr class="${rowClass}">
					<td>
						<a href="${ticketsPage}?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
					</td>
					<c:set var="total" value="0" />
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<c:set var="total" value="${total + quantityStatByCategory['UNCAT'][quantity]}" />
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&cat=UNCAT&reset=1">
								${quantityStatByCategory['UNCAT'][quantity]}
							</a>
						</td>
					</c:forTokens>
					<td align="right">
						<a href="${ticketsPage}?eventId=${event.id}&reset=1">							
							<b>${total}</b>
						</a>
					</td>
					<td align="right">
						<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
					</td>
				</tr>
		
			</tbody>
		
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td align="right">			
							<c:set var="total" value="${total + totalStatByQuantity[quantity]}" />					
							${totalStatByQuantity[quantity]}
						</td>				
					</c:forTokens>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / statEventTicketCount}" />%
						</td>				
					</c:forTokens>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
		
		</table>
	 </c:otherwise>
    </c:choose>
  </div>
 </c:if>
  <div id="fragment-crawls">
	<%-- <authz:authorize ifAnyGranted="ROLE_EDITOR"> --%>
	
	<div style="margin-top:16px;margin-bottom:8px;">
		<div style="float: left"><b>Legends:</b></div>
	
		<div style="margin-left:20px;width:10px;height:10px;background-color: #EB9E6F;border: 2px solid #EB9E6F;margin-right: 6px;float: left"></div>  
		<div style="float: left">Disabled crawl</div>
	
		<div style="margin-left:20px;width:10px;height:10px;background: #748C20;border: 2px solid #748C20;margin-right: 6px;float: left"></div>  
		<div style="float: left">Selected crawl</div>
		
		<div style="margin-left:20px;width:10px;height:10px;background: #ff4444;border: 2px solid #ff4444;margin-right: 6px;float: left"></div>  
		<div style="float: left">Broken crawl</div>		
	
		<div style="clear:both"></div>
	</div>
	
	<table>
		<tr>
			<td>
				<select class="addCrawlSelector" onchange="changeAddCrawler($(this).val())">
					<option selected>-- Create new crawl --</option>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<c:set var="safeSiteId" value="[${siteId}]" />
					
					    <c:choose>
					      <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
						    <option style="text-decoration:line-through;margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}" disabled>${siteId}</option>
					      </c:when>
					      <c:otherwise>
						    <option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}">${siteId}</option>
					      </c:otherwise>
					    </c:choose>
					</c:forEach>
				</select>
			</td>
			<c:if test="${not empty crawls}">
				<td width="30">&nbsp;</td>
				<td>
					<b>Select: </b>
					<a href="javascript:void(0)" onclick="selectAllTicketListingCrawls()">All</a> -
					<a href="javascript:void(0)" onclick="unselectAllTicketListingCrawls()">None</a>			
				</td>
				<td width="30">&nbsp;</td>
				<td>
					<b>Action: </b>
					<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedTicketListingCrawls()">Enable</a> -
					<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedTicketListingCrawls()">Disable</a> -
					<img src="../images/ico-reload.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="forceRecrawlSelectedTicketListingCrawls()">Force Recrawl</a>
				</td>
			</c:if>
		</tr>
	</table>
	
	<display:table class="list" name="${crawls}" id="crawl" requestURI="${ticketsPage}" decorator="crawlTableDecorator">
	    <display:column sortable="false" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	   	  <input type="checkbox" id="ticket-listing-crawl-${crawl.id}" class="ticket-listing-crawl" onChange="toggleTicketListingCrawlRow($(this).attr('id').split('-')[3])" />
	    </display:column>
	    <display:column title="Name" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<img src="../images/ico-${crawl.siteId}.gif" align="absmiddle" title="${crawl.siteId}"/>
	    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=${ticketsPage}?eventId=${event.id}"><b>${crawl.name}</b></a>
	    </display:column>
	    <display:column title="Status" sortable="true" style="width: 90px;" decorator="crawlStatusColumnDecorator" value="${crawl}" />
	    <display:column title="Last crawl" sortable="true" style="width: 90px;" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper"><span tooltip="<fmt:formatDate value="${crawl.startCrawl}" pattern="yyyy/MM/dd HH:mm:ss" />"><fmt:formatDate value="${crawl.startCrawl}" pattern="HH:mm:ss" /></span></display:column>    
	    <display:column title="Creator" sortable="true" property="creator" />
	    <display:column title="Created" sortable="true"><fmt:formatDate value="${crawl.created}" pattern="yyyy/MM/dd HH:mm:ss" /></display:column>
	    <display:column title="Last Updater" sortable="true" property="lastUpdater" />
	    <display:column title="Last Updated" sortable="true"><fmt:formatDate value="${crawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" /></display:column>
		<display:column sortable="true"><a href="RedirectToListingPage?id=${crawl.id}" target="_blank"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a></display:column>
	</display:table>
	<br/><br/>
	<a href="EditorManageCrawls?tourId=${event.artistId}&eventId=${event.id}">Go to crawler page</a>
	<%-- </authz:authorize> --%>
  </div>
  <div id="fragment-filters">
	<a href="${ticketsPage}?eventId=${event.id}&reset=1">Reset filter</a>

    <div>	
	<div style="font-size: 12px">
		<div>
			<div style="float: left;width: 260px;">
				View:

				<c:choose>
					<c:when test="${view == 'compact'}">
						<b>Compact</b>
					</c:when>
					<c:otherwise>
						<a href="${ticketsPage}?eventId=${event.id}&view=compact">Compact</a>
					</c:otherwise>
				</c:choose>
				|				
				<c:choose>
					<c:when test="${view == 'expanded'}">
						<b>Expanded</b>
					</c:when>
					<c:otherwise>
						<a href="${ticketsPage}?eventId=${event.id}&view=expanded">Expanded</a>
					</c:otherwise>
				</c:choose>	
				|
				<c:choose>
					<c:when test="${view == 'analytic'}">
						<b>Analytic</b>
					</c:when>
					<c:otherwise>
						<a href="${ticketsPage}?eventId=${event.id}&view=analytic">Analytic</a>
					</c:otherwise>
				</c:choose>	
					
			</div>
			
			<div style="float: left;width: 400px;">
				<label for="removeDuplicatePolicySelector">Remove Duplicate Policy</label>
				<select id="removeDuplicatePolicySelector" onchange="changeRemoveDuplicatePolicy(this.value)">
					<option value="NONE" <c:if test="${removeDuplicatePolicy == 'NONE'}">selected</c:if> >NONE</option>
					<option value="REMOVE_NON_WORD_CHARACTERS" <c:if test="${removeDuplicatePolicy == 'REMOVE_NON_WORD_CHARACTERS'}">selected</c:if>>STANDARD</option>
					<option value="MANUAL" <c:if test="${removeDuplicatePolicy == 'MANUAL'}">selected</c:if>>MANUAL</option>
				</select>
				<br/><input type="checkbox" name="showDuplicates" <c:if test="${showDuplicates}">checked</c:if> onclick="location.href='${ticketsPage}?eventId=${event.id}&showDuplicates=${not showDuplicates}'"/> Show duplicates
				<c:if test="${removeDuplicatePolicy != 'NONE'}">
					<i>(${removedDuplicateCount} duplicate entries were removed)</i>
				</c:if>
			</div>
		</div>
		<div style="clear: both"></div>		
		<div style="border-top: 1px dashed black; border-bottom: 1px dashed black;padding-top: 5px; padding-bottom: 5px;margin-top: 5px;">
			Site:
			(
			<a href="javascript:checkAllSiteFilter()"><small>check all</small></a> -
			<a href="javascript:uncheckAllSiteFilter()"><small>uncheck all</small></a>
			)
			<small>note: prices will be adjusted as follow: <i>new price = price * (1.0 + adjustment / 100)</i></small><br />			
			<div style="float:left;">
				<c:forEach var="sid" items="${constants.siteIds}">
					<c:set var="safeSiteId" value="[${sid}]" />
				
					<div style="float: left; width: 280px;height: 24px;">
						<input type="checkbox" id="${sid}-checkbox" class="site-checkbox" onchange="changeSiteFilter()"
							<c:if test="${siteFilters[sid] and not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">checked</c:if>
							<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">disabled</c:if>
						>
						<label for="${sid}-checkbox">
					    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
							<c:choose>
							  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
							    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
								  ${sid}
							    </span>
							  </c:when>
							  <c:otherwise>
							    ${sid}
							  </c:otherwise>
							</c:choose>
						</label>
						<span id="price-adjustment-${sid}" style="padding: 2px;">
							(
							<c:set var="priceAdjustment" value="${priceAdjustments[sid]}" />																									
							<c:choose>
								<c:when test="${empty priceAdjustment || priceAdjustment.percentAdjustment == 0}">
									<span id="price-adjustment-span-${sid}">+0.00%</span>
								</c:when>
								<c:when test="${priceAdjustment.percentAdjustment < 0}">
									<span id="price-adjustment-span-${sid}" style="color: #aa2222; font-weight: bold">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
									</span>
								</c:when>
								<c:otherwise>
									<span id="price-adjustment-span-${sid}" style="color: #22aa22; font-weight: bold">
										+<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
									</span>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${empty siteById[sid] || siteById[sid].priceRounding == 'NONE'}">
								</c:when>								
								<c:when test="${siteById[sid].priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
									<img id="price-adjustment-price-rounding-icon-${sid}" src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
								</c:when>
								<c:when test="${siteById[sid].priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
									<img id="price-adjustment-price-rounding-icon-${sid}" src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
								</c:when>
							</c:choose>
							<a id="price-adjustment-link-edit-${sid}" href="javascript:void(0);" onclick="editPriceAdjustment('${sid}')" style="font-size:10px">edit</a>
							<span id="price-adjustment-edit-${sid}" style="display:none">
								<input id="price-adjustment-textfield-${sid}" type="text" style="font-size:10px;width:40px;">
								<span id="price-adjustment-textfield-percent-${sid}">%</span>
								<a id="price-adjustment-link-save-${sid}" href="javascript:void(0);" onclick="savePriceAdjustment('${sid}')" style="font-size:10px;">ok</a>
								<a id="price-adjustment-link-cancel-${sid}" href="javascript:void(0);" onclick="cancelPriceAdjustment('${sid}')" style="font-size:10px;">cancel</a>
							</span>
							<span id="price-adjustment-saving-${sid}" style="color:red;font-size: 10px;display:none">
								<img src="../images/process-running.gif" align="absbottom" /> Saving...
							</span>
							)
						</span>
					</div>
				</c:forEach>
			</div>
			<div style="clear:both"></div>
			<%-- <authz:authorize ifAnyGranted="ROLE_EDITOR"> --%>			
				<a href="EditorEditSites" style="font-size:10px">Change price rounding</a>
			<%-- </authz:authorize> --%>
		</div>
	  </div>
    </div>
  </div>
</div>


<div id="chartDiv" style="opacity:0">
    <c:choose>
     <c:when test="${not empty stats}">
	  <br />
	  <div id="flashContent">
	    <div id="flashContentNotLoad" style="display:none">
	      This Flash movie cannot load. You are viewing this because:
	      <ol>
	        <li>You have no Flash Plug-In (<a href="https://www.macromedia.com/go/getflashplayer">Click here to download</a>)</li>
	        <li>
	          Your Flash Plug-in is older than the necessary version required to play this movie
	          (<a href="javascript: window.reload();">Click here to reload the movie</a> and click "Yes" when asked to upgrade your plug-in.)
  	        </li>
	      </ol>
	    </div>  
	  </div>
     </c:when>
     <c:otherwise>
       Nothing found to display
     </c:otherwise>
    </c:choose>
</div>

</div> 

<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketRow" />
<jsp:directive.page import="com.admitone.tmat.data.TicketBookmark" />

<script type="text/javascript">
var io;
    $('#container-tabs').tabs({
    select: function(event, ui) {
      var url = ui.tab.href;
      var posAnchor = url.indexOf('#');
      anchor = url.substr(posAnchor, url.length - posAnchor);
      if (url.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('opacity', 1);
        $('#chartDiv').css('height', 400);
	  } else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
	  }
    }
    });
    
    if (location.href.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('height', 400);
        $('#chartDiv').css('opacity', 1);
    }  else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
    }

    <c:choose>
      <c:when test="${param.nocrawl == true}">
        showRefreshedRecently();
      </c:when>
      <c:otherwise>
        updateCrawls();
      </c:otherwise>
    </c:choose>

    $('#event-details-info').hide();
	$('#LoadingEventSpan').hide();
	$('#eventNavigationSpan').show();
	$('#wholePageDiv').show();
	
	<c:if test="${not empty param.scroll}">
	  window.scrollTo(0, ${param.scroll});
	</c:if>

  var anchor = "";

  <c:if test="${not empty eventBookmark.objectId}">
    $('#star_event_' + ${eventBookmark.objectId}).attr('class', 'starOn');
  </c:if>

//fancy ticket loading
var tickets = [
<c:forEach var="ticket" items="${webTickets}"><%
    WebTicketRow ticket = (WebTicketRow)pageContext.getAttribute("ticket");
	
%>[${ticket.id},${ticket.hasDuplicate},${ticket.removedDuplicateTicket},${ticket.remainingQuantity},${ticket.lotSize},'${ticketToCat[ticket.id]}','${ticket.section}','${ticket.normalizedSection}','${ticket.row}','${ticket.normalizedRow}',${ticket.adjustedCurrentPrice}, ${ticket.buyItNowPrice},${ticket.numSecondsBeforeEndDate},'${ticket.ticketType}','${ticket.seat}','${ticket.lastUpdate}','${ticket.seller}','${ticket.siteId}','${ticket.itemId}','${ticket.ticketDeliveryType}'<c:choose><c:when test="${view == 'analytic' && not empty actions[ticket.id]}">,${ticket.valuationFactor},${actions[ticket.id].tgCheaper},${actions[ticket.id].overprice},${actions[ticket.id].overpriceNext},${actions[ticket.id].underprice},${actions[ticket.id].underpriceNext}</c:when><c:otherwise>,null,null,null,null,null,null</c:otherwise></c:choose><c:if test="${view == 'expanded'}">,${ticket.soldQuantity},'${ticket.endDate}','${ticket.insertionDate}','${ticket.ticketStatus}'</c:if>],
</c:forEach>
null
];

var bookmarks = [
<c:forEach var="bookmark" items="${ticketBookmarks}"><%
    TicketBookmark bookmark = (TicketBookmark)pageContext.getAttribute("bookmark");
	
%>['${bookmark.objectId}'],
</c:forEach>
null
];

// remove last element which is null
tickets = tickets.slice(0, tickets.length - 1);
bookmarks = bookmarks.slice(0, bookmarks.length - 1);

// INDEX contants
var TICKET_ID_INDEX = 0;
var HAS_DUP_INDEX = 1;
var DUP_TICKET_INDEX = 2;
var REM_QTY_INDEX = 3;
var LOT_SIZE_INDEX = 4;
var TICKET_CAT_INDEX = 5;
var TICKET_SECTION_INDEX = 6;
var NORM_SECTION_INDEX = 7;
var TICKET_ROW_INDEX = 8;
var NORM_ROW_INDEX = 9;
var ADJ_PRICE_INDEX = 10;
var NOW_PRICE_INDEX = 11;
var NUM_SECS_INDEX = 12;
var TICKET_TYPE_INDEX = 13;
var TICKET_SEAT_INDEX = 14;
var LAST_UPDATE_INDEX = 15;
var TICKET_SELLER_INDEX = 16;
var SITE_ID_INDEX = 17;
var ITEM_ID_INDEX = 18;
var TICKET_DELIV_INDEX = 19;
var E_VAL_INDEX = 20;
var TG_CHEAP_INDEX = 21;
var OVER_PRICE_INDEX = 22;
var OVER_NEXT_INDEX = 23;
var UNDER_PRICE_INDEX = 24;
var UNDER_NEXT_INDEX = 25;
var SOLD_QTY_INDEX = 26;
var END_DATE_INDEX = 27;
var INS_DATE_INDEX = 28;
var TICKET_STATUS_INDEX = 29;

//BOOKMARK contants
var TICKET_BOOKMARK_INDEX = 0;

var ticketView = "${view}";
var dupPolicy = "${removeDuplicatePolicy}";

//there are some handlers associated when exiting the page that make the page slow, so remove them.
$(window).unbind("unload");

$(document).ready(function() {

	
	$('#ticketTableContainer').empty();

	if (tickets.length == 0) {
		$('#ticketTableContainerLoading').hide();		
		$('#ticketTableContainer').html("<br />No tickets to display");
		return;
	}
	
	buildTicketTable();
	
	
	// initialize multisort selector
	var options = [
		[0, "None"],
		[1, "Remaining Quantity"],
		[2, "Category"],
		[3, "Section"],
		[4, "Row"],
		[5, "Wholesale Price (asc)"],
		[105, "Wholesale Price (desc)"],
		[6, "Online Price (asc)"],
		[106, "Online Price (desc)"],
		[7, "Title"],
		[8, "Seller"],
		[10, "Sold"],
		[9, "Lot Size"],
		[11, "Item Id"],
		[12, "End Date"],
		[13, "Insertion Date"],
		[14, "Last Update"],
		[15, "Status"],
	    [18, "Action Indicator"],
		[19, "eVal (asc)"],
		[119, "eVal (desc)"]
	];	
	
	$.each($('.multisort_selector'), function(i, selector) {
		$.each(options, function(j, option) {
			$(selector).append($.OPTION({value: option[0]}, option[1]));
		});
	});	

	$('#multisort_selector0').val("${multisortField0}");
	$('#multisort_selector0').change(function() {changeMutisortSelector(0, $(this).val())});

	$('#multisort_selector1').val("${multisortField1}");
	$('#multisort_selector1').change(function() {changeMutisortSelector(1, $(this).val())});

	$('#multisort_selector2').val("${multisortField2}");
	$('#multisort_selector2').change(function() {changeMutisortSelector(2, $(this).val())});

	$('#multisort_selector3').val("${multisortField3}");
	$('#multisort_selector3').change(function() {changeMutisortSelector(3, $(this).val())});
	
	$('.ticket-listing-crawl').attr('checked', false);

	$('table#crawl TBODY tr').click(
		function() {
			var checkbox = $($(this).children('TD').get(0)).children('INPUT');
			$(checkbox).attr('checked', !$(checkbox).attr('checked'));
			updateTicketListingCrawlRow($(checkbox).attr('id').split("-")[3]);
		}
	);

	if ($('#flashContent').length == 0) {
		return;
	}
	
	var tzName = "<%=java.util.Calendar.getInstance().getTimeZone().getDisplayName(true, java.util.TimeZone.SHORT)%>";
	var tzOffset = "<%=-(java.util.Calendar.getInstance().get(java.util.Calendar.ZONE_OFFSET) + java.util.Calendar.getInstance().get(java.util.Calendar.DST_OFFSET)) / (60 * 1000) %>";
	var bridgeName = "chartBridge";
	var chartWidth = $('body').width() - 10;
	var chartHeight = 400;
	var noChartMessageBoxEnabled = "true";			
	 
	var so = new SWFObject("../flash/chart.swf", "flexApp", chartWidth, chartHeight, "9", "#000000");
	so.useExpressInstall('../flash/expressinstall.swf');
	so.addParam("wmode", "transparent");    
	so.addParam("pluginspage", "https://www.adobe.com/go/getflashplayer");
	so.addVariable("bridgeName", bridgeName);
	so.addVariable("tzOffset", tzOffset);    
	so.addVariable("tzName", tzName);
	so.addVariable("noChartMessageBoxEnabled", noChartMessageBoxEnabled);
	so.write("flashContent");

	// do that to wait FABridge to be initialized
	initializeChart();	

});


var __rowCount;

function buildTicketTable() {
	__rowCount = 0;

	$('#ticketTableContainerLoading').show();
	$('#ticketTableContainerLoadingPercent').text('');

	$('#ticketTableContainer').empty();
	$('#ticketTableContainer').append(
		$.TABLE({'id': 'ticket', 'class': 'list'},
			$.THEAD({'display': 'none'},
				$.TR({},
					$.TH({}),
					$.TH({}),
					$.TH({'id': 'ticketTableHeader' + REM_QTY_INDEX}, "Rem Qty"),
					$.TH({'id': 'ticketTableHeader' + TICKET_CAT_INDEX}, "Cat"),
					$.TH({'id': 'ticketTableHeader' + NORM_SECTION_INDEX}, "Section"),
					$.TH({'id': 'ticketTableHeader' + NORM_ROW_INDEX}, "Row"),
					$.TH({'id': 'ticketTableHeader' + TICKET_SEAT_INDEX}, "Seats"),
					$.TH({'id': 'ticketTableHeader' + ADJ_PRICE_INDEX}, "Wholesale"),
					$.TH({'id': 'ticketTableHeader' + NOW_PRICE_INDEX}, "Online Price"),
					((ticketView == 'analytic')
							?[$.TH({'id': 'ticketTableHeader' + E_VAL_INDEX}, "EVal"),$.TH({}, "Action")]
							:null
					),
					((ticketView == 'expanded')
						?[$.TH({'id': 'ticketTableHeader' + LOT_SIZE_INDEX}, "Lot Size"),
						  $.TH({'id': 'ticketTableHeader' + SOLD_QTY_INDEX}, "Sold Qty"),
						  $.TH({'id': 'ticketTableHeader' + ITEM_ID_INDEX}, "Item ID"),
						  $.TH({'id': 'ticketTableHeader' + END_DATE_INDEX}, "End Time"),
						  $.TH({'id': 'ticketTableHeader' + INS_DATE_INDEX}, "Date Added"),
						  $.TH({'id': 'ticketTableHeader' + TICKET_STATUS_INDEX}, "Status")]
						:null
					),
					$.TH({'id': 'ticketTableHeader' + TICKET_SELLER_INDEX}, "Seller"),
					$.TH({'id': 'ticketTableHeader' + LAST_UPDATE_INDEX}, "Last Update")
				)
			),
			$.TBODY({'id': 'ticketTableBody'})
		)
	);			
	
	$('#ticket THEAD').hide();

	setTimeout(buildTicketTableContent, 10);
};

/**
 * Build the ticket table by block of 50 (so that it does not block the browser while doing its work).
 */
function buildTicketTableContent() {
	$('#ticketTableContainerLoadingPercent').text('(' + parseInt((__rowCount * 100) / tickets.length) + '%)');
	if (__rowCount >= tickets.length) {
		
		$.each(bookmarks, function(j, bookmark) {
		    $('#star_' + bookmark[TICKET_BOOKMARK_INDEX]).attr('class', 'starOn');
		    $('#star_' + bookmark[TICKET_BOOKMARK_INDEX]).parent().parent().css('background-color', '#eeee44');  
		});
			
		$('#ticketTableContainerLoading').hide();
		$('#ticketTableBody *').filter(function() { return $(this).attr('ticketTooltip'); }).tooltip({
			showURL: false,
			bodyHandler: function() {
			return $(this).attr("ticketTooltip");
			}});
		return;
	}

	subTix = tickets.slice(__rowCount, Math.min(tickets.length, __rowCount + 50));

	$('#ticketTableBody').append(
		$.map(subTix, function(ticket) {
			__rowCount++;

			var ticketId = ticket[TICKET_ID_INDEX];
			var hasDup = ticket[HAS_DUP_INDEX];
			var dupTicket = ticket[DUP_TICKET_INDEX];				
			var remQty = ticket[REM_QTY_INDEX];
			var lotSize = ticket[LOT_SIZE_INDEX];
			var category = ticket[TICKET_CAT_INDEX];				
			var section = ticket[TICKET_SECTION_INDEX];
			var normSection = ticket[NORM_SECTION_INDEX];
			var row = ticket[TICKET_ROW_INDEX];
			var normRow = ticket[NORM_ROW_INDEX];
			var seat = ticket[TICKET_SEAT_INDEX];
			var wholesale = "$" + formatFloat(ticket[ADJ_PRICE_INDEX]);
			var online = "$" + formatFloat(ticket[NOW_PRICE_INDEX]);
			var numSecs = ticket[NUM_SECS_INDEX];
			var ticketType = ticket[TICKET_TYPE_INDEX];
			var itemId = ticket[ITEM_ID_INDEX];
			var lastUpdate = ticket[LAST_UPDATE_INDEX];
			var seller = ticket[TICKET_SELLER_INDEX];
			var siteId = ticket[SITE_ID_INDEX];
			var delivery = ticket[TICKET_DELIV_INDEX];
						
			var eval = null;
			var tgCheap = null;
			var overprice = null;
			var overpriceNext = null;
			if (ticketView == "analytic") {
				eval = ticket[E_VAL_INDEX];	
				tgCheap = ticket[TG_CHEAP_INDEX];
				overprice = "$" + formatFloat(ticket[OVER_PRICE_INDEX]);
				overpriceNext = "$" + formatFloat(ticket[OVER_NEXT_INDEX]);	
				underprice = "$" + formatFloat(ticket[UNDER_PRICE_INDEX]);
				underpriceNext = "$" + formatFloat(ticket[UNDER_NEXT_INDEX]);				
			}

			var qtySold = null;
			var endDate = null;
			var insDate = null;	
			var status = null;
			
			if (ticketView == "expanded") {
				qtySold = ticket[SOLD_QTY_INDEX];		
				endDate = ticket[END_DATE_INDEX];	
				insDate = ticket[INS_DATE_INDEX];	
				status = ticket[TICKET_STATUS_INDEX];					
			}
			
			var formattedEndDate;
			if (endDate != null) {
				formattedEndDate = endDate.split(" ")[0];
			}
			var formattedInsDate;
			if (insDate != null) {
				formattedInsDate = insDate.split(" ")[0];
			}
			var formattedUpDate;
			if (lastUpdate != null) {
				formattedUpDate = lastUpdate.split(" ")[0] + " " + lastUpdate.split(" ")[1].split(":")[0] + ":" + lastUpdate.split(" ")[1].split(":")[1];
			}
			var formattedItemId;
			if (itemId.length < 40) {
				formattedItemId = itemId;
			} else {
				formattedItemId = itemId.substring(0, 20) + "..." + itemId.substring(itemId.length - 20, itemId.length);
			}
			var formattedSection;
			if (normSection.length < 12) {
				formattedSection = normSection;
			} else {
				formattedSection = normSection.substring(0, 9) + "...";
			}
			var formattedRow;
			if (normRow.length < 10) {
				formattedRow = normRow;
			} else {
				formattedRow = normRow.substring(0, 7) + "...";
			}
			var formattedSeat;
			if (seat.length < 10) {
				formattedSeat = seat;
			} else {
				formattedSeat = seat.substring(0, 7) + "...";
			}

			var sellerContent = null;
			if (ticketType != "AUCTION" && numSecs != null) {
				if(numSecs > (3600 * 24)) {
					if(delivery == "EDELIVERY"){
						sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': '', 'ticketTooltip':'Ending in ' + numSecs}, seller),$.IMG({'src': '../images/ico-clock.gif', align: 'absbottom'}),$.IMG({'src': '../images/ico-edelivery.gif', align: 'absbottom'})];
					} else if(delivery == "INSTANT") {
						sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': '', 'ticketTooltip':'Ending in ' + numSecs}, seller),$.IMG({'src': '../images/ico-clock.gif', align: 'absbottom'}),$.IMG({'src': '../images/ico-instant-edelivery.gif', align: 'absbottom'})];
					} else {
						sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': '', 'ticketTooltip':'Ending in ' + numSecs}, seller),$.IMG({'src': '../images/ico-clock.gif', align: 'absbottom'})];
					}
				} else {
					sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': '', 'ticketTooltip':'Ending in ' + numSecs}, seller),$.IMG({'src': '../images/ico-clock-ending.gif', align: 'absbottom'})];
				}
			} else {
				if(delivery == "EDELIVERY"){
					sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': ''}, seller),$.IMG({'src': '../images/ico-edelivery.gif', align: 'absbottom'})];
				} else if(delivery == "INSTANT") {
					sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': ''}, seller),$.IMG({'src': '../images/ico-instant-edelivery.gif', align: 'absbottom'})];
				} else {
					sellerContent = [$.IMG({'src': '../images/ico-' + siteId + '.gif', align: 'absbottom'}),$.SPAN({'class': ''}, seller)];
				}
			}

			var aiContent = null;
			if(ticketView == "analytic"){
				if(tgCheap >= 7){
					aiContent = $.DIV({'class':'highOverAction'}, 'TC#: ' + tgCheap +' OP$: ' + overprice + ' ' + overpriceNext);
				} else {
					if(tgCheap >= 4){
						aiContent = $.DIV({'class':'medOverAction'}, 'TC#: ' + tgCheap +' OP$: ' + overprice + ' ' + overpriceNext);
					} else {
						if(tgCheap >= 2){
							aiContent = $.DIV({'class':'lowOverAction'}, 'TC#: ' + tgCheap +' OP$: ' + overprice + ' ' + overpriceNext);
						} else {
							if(tgCheap != 0){
								aiContent = $.DIV({'class':'basic'}, 'TC#: ' + tgCheap +' OP$: ' + overprice + ' ' + overpriceNext);
							} else {
								aiContent = $.DIV({'class':'basic'}, 'UP$: ' + underprice + ' ' + underpriceNext);
							}
						}
					}
				}
			}

			var evalContent = $.SPAN({'class': ''}, eval);
			
			var onlineContent = null;
			if(ticketType == "AUCTION"){
				if(numSecs > (3600 * 24)) {
					onlineContent = [$.IMG({'src': '../images/ico-auction.gif', align: 'absbottom'}), $.SPAN({'class': ''}, online)];
				} else {
					onlineContent = [$.IMG({'src': '../images/ico-auction-ending.gif', align: 'absbottom'}), $.SPAN({'class': ''}, online)];
				}
			} else {
				onlineContent = $.SPAN({'class': ''}, online);
			}

			var wholeContent = $.SPAN({'class': ''}, wholesale);
			
			var seatContent = $.SPAN({'class': '', 'ticketTooltip': seat}, formattedSeat);
			
			var rowContent = $.SPAN({'class': '', 'ticketTooltip': row}, formattedRow);

			var sectionContent = $.SPAN({'class': '', 'ticketTooltip': section}, formattedSection);

			var catContent = $.SPAN({'class': ''}, category);
			
			var qtyContent = null;
			if(hasDup){
				qtyContent = $.SPAN({'class': ''}, remQty + 'dup');
			} else {
				qtyContent = $.SPAN({'class': ''}, remQty);
			}
			
			var bookmarkContent = $.DIV({'id':'star_' + ticketId, 'class':'starOff', 'ticketId':ticketId})
			$(bookmarkContent).click(function() {
			  toggleBookmarkTicket($(bookmarkContent).attr('ticketId'));
			});
			
			var checkbox = $.INPUT({'id': 'ticket_' + ticketId, 'type': 'checkbox', 'class': 'ticket_checkbox'});
			$(checkbox).attr('checked', false);
			
			$(checkbox).change(function() {tickDuplicateTicket($(this).attr('id').replace('ticket_',''));});
			
			var rowClassName = ((__rowCount % 2) == 0)?"even":"odd";

			if(dupTicket){
				rowClassName = "removedDuplicateTicket";
			} 
			if(seller == "ADMT1"){
				rowClassName = "admit1Row";
			}
			
			var tr = $.TR({'class': rowClassName},
				$.TD({'align' : 'center'}, checkbox),
				$.TD({'align' : 'center'}, bookmarkContent),
				$.TD({'align' : 'center'}, qtyContent),
				$.TD({'align' : 'center'}, catContent),
				$.TD({'align' : 'center'}, sectionContent),
				$.TD({'align' : 'center'}, rowContent),
				$.TD({'align' : 'center'}, seatContent),
				$.TD({'align' : 'right'}, wholeContent),
				$.TD({'align' : 'right'}, onlineContent),
				((ticketView == 'analytic')
						?[$.TD({'align' : 'right'}, evalContent), $.TD({'align' : 'right'}, aiContent)] 
					:null
				),
				((ticketView == 'expanded')
						?[$.TD({'align' : 'center'}, lotSize), $.TD({'align' : 'center'}, qtySold), $.TD({'align' : 'left'}, formattedItemId), $.TD({'align' : 'left'}, formattedEndDate), $.TD({'align' : 'left'}, formattedInsDate), $.TD({'align' : 'right'}, status)] 
					:null
				),
				$.TD({'align' : 'left'}, $.A({'href': 'RedirectToItemPage?id=' + ticketId}, sellerContent)),
				$.TD({'align' : 'left'}, formattedUpDate)
			);
									
			return tr;
		})
	);
	
	
	// show only header after the first element is shown
	$('#ticket THEAD').show();
	setTimeout(buildTicketTableContent, 10);
};

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_event_' + eventId).attr('class', 'star' + response);
		   });
  };

  function toggleBookmarkTicket(ticketId) {
    	BookmarkDwr.toggleBookmarkTicket(ticketId, 
		   function(response) {
				 $('#star_' + ticketId).attr('class', 'star' + response);
				 if (response == "On") {
				 	$('#star_' + ticketId).parent().parent().css('background-color', '#eeee44');
				 } else {
				 	$('#star_' + ticketId).parent().parent().css('background-color', null);
				 }				   							      
  		   });
  };
  
  function changeCatFilter() {
    var url = "${ticketsPage}?eventId=${event.id}";
    url += "&category=";
    $.each($('.cat-checkbox'), function(i, elt) {
    	var cat = $(elt).attr('id').split("-")[1];  	
    	if ($(elt).attr('checked') == true) {
    	url += cat + ",";
    	}
    });
    
    //alert(url + anchor);
    location.href = url + anchor;       	
  };
  
  function changeQuantityFilter() {
    var url = "${ticketsPage}?eventId=${event.id}";
    url += "&quantity=";
    $.each($('.quantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[1];
    //	url += "&qty_" + qty + "=" + $(elt).attr('checked');  	
    	if ($(elt).attr('checked') == true) {
    	url += qty + ",";
    	}
    });
    
    //alert(url + anchor);
    location.href = url + anchor;       	
  };
  
  function changeSiteFilter() {
    var url = "${ticketsPage}?eventId=${event.id}";
    $.each($('.site-checkbox'), function(i, elt) {
    	var siteId = $(elt).attr('id').split("-")[0];
    	url += "&" + siteId + "_checkbox=" + $(elt).attr('checked');  	
    });
    location.href = url + anchor;       	
  };
  
  function checkAllSiteFilter() {
	$.each($('.site-checkbox'), function(intIndex, input) {
	  if (!$(input).attr('disabled')) {
	  	$(input).attr('checked', 'true').change();
	  }
	});
	return false;
    location.href = "${ticketsPage}?eventId=${event.id}"
		<c:forEach var="siteId" items="${constants.siteIds}">
    		+ "&${siteId}_checkbox=true"
    	</c:forEach> + anchor;    
  };

  function uncheckAllSiteFilter() {
  	$('.site-checkbox').attr('checked', false);
    location.href = "${ticketsPage}?eventId=${event.id}"
		<c:forEach var="siteId" items="${constants.siteIds}">
    		+ "&${siteId}_checkbox=false"
    	</c:forEach> + anchor;  	
  };
  
	function changeCategoryFilter(val) {
		location.href = "${ticketsPage}?eventId=${event.id}&cat=" + val + anchor;
	};

	// called by flex chart
	function storeFlexChartSelectedFieldNames(selectedFieldNames) {
		$.cookie("flexChart", selectedFieldNames, {expires: 7});
	};

	function loadFlexChartSelectedFieldNames() {
		return $.cookie("flexChart");
	};
	
function changeGroup() {
    var catGroup = $.trim($('#catScheme').val());;
    document.location.href = "${ticketsPage}?eventId=${event.id}"
        + "&catScheme=" + catGroup + anchor;
}

function changeMutisortCheckBox(checked) {
	document.location.href = "${ticketsPage}?eventId=${event.id}&multisort="
		+ (checked?"1":"0") + anchor;
};

function changeMutisortSelector(id, value) {
	// refresh page
	document.location.href="${ticketsPage}?eventId=${event.id}&multisort=1"
		+ "&multisort_field0=" + $('#multisort_selector0').val()
		+ "&multisort_field1=" + $('#multisort_selector1').val()
		+ "&multisort_field2=" + $('#multisort_selector2').val()
		+ "&multisort_field3=" + $('#multisort_selector3').val()
		+ anchor; 				
};

function changeRemoveDuplicatePolicy(value) {
	// refresh page
	document.location.href="${ticketsPage}?eventId=${event.id}&removeDuplicatePolicy=" + value + anchor;
};

function initializeChart() {
	if (FABridge && FABridge.chartBridge) {
		var urlTokens = document.location.href.split("/");
		urlTokens.pop();
		var historyURL = urlTokens.join("/") + "/IndexHistory?eventId=${event.id}" + "&catScheme=${catGroupName}";
		FABridge.chartBridge.root().createIndex("Price", "chart", "price", 0x313AF9, historyURL, false);
	} else {
		setTimeout(initializeChart, 1000);
	}
};

function editPriceAdjustment(siteId) {
  	var backElement = $($.DIV({id: 'back-element'})).css({
  		'top': '0px', 
  		'left': '0px', 
  		'position': 'absolute', 
  		'z-index': 10000, 
  		'opacity': '0',
  		 'background': 'black',
		'width': $('body').width(),
		'height': $('body').height()
  	});
  	$('body').append(backElement);

 	$('#price-adjustment-' + siteId).css({
 		zIndex: 10001,
 		position: 'absolute',
 		'background': '#ffffaa'
 	});

	$('#price-adjustment-span-' + siteId).hide();
	$('#price-adjustment-price-rounding-icon-' + siteId).hide();
	
 	$('#price-adjustment-link-edit-' + siteId).hide();

 	$('#price-adjustment-link-save-' + siteId).show();
 	$('#price-adjustment-link-cancel-' + siteId).show();
 	
	
	var s = $.trim($('#price-adjustment-span-' + siteId).text());
	var priceAdjustment = s.substring(0, s.length - 1);
	$('#price-adjustment-textfield-' + siteId).val(formatFloat(priceAdjustment));

	$('#price-adjustment-edit-' + siteId).show();
	$('#price-adjustment-textfield-' + siteId).focus();

	$('#price-adjustment-textfield-' + siteId).keyup(function(e) {
		if (e.keyCode == 13) {
			savePriceAdjustment(siteId);
		}
	});		

};

function savePriceAdjustment(siteId) {
	var priceAdjustment = $('#price-adjustment-textfield-' + siteId).val() * 1;
	if (isNaN(priceAdjustment)) {
		alert("Invalid price adjustment");
		return;
	}
	if (priceAdjustment < 0) {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#aa2222', 'font-weight': 'bold'});
		$('#price-adjustment-span-' + siteId).text(priceAdjustment + "%");
	} else if (priceAdjustment > 0) {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#22aa22', 'font-weight': 'bold'});
		$('#price-adjustment-span-' + siteId).text('+' + priceAdjustment + "%");
	} else {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#000000', 'font-weight': 'normal'});
		$('#price-adjustment-span-' + siteId).text(priceAdjustment + "%");
	}

	$('#price-adjustment-edit-' + siteId).hide();
 	
	$('#price-adjustment-price-rounding-icon-' + siteId).show();
	$('#price-adjustment-span-' + siteId).show();
	$('#price-adjustment-saving-' + siteId).show();
	DataDwr.changePriceAdjustment(
		${event.id}, siteId, priceAdjustment,
			function(result) {
				// refresh page
				document.location.href="${ticketsPage}?eventId=${event.id}" + anchor;				
			},
			function(error) {
				alert("error: " + error);
			}
	);
};

function cancelPriceAdjustment(siteId) {
	$('#back-element').remove();
 	$('#price-adjustment-link-edit-' + siteId).show();
 	$('#price-adjustment-link-save-' + siteId).hide();
 	$('#price-adjustment-link-cancel-' + siteId).hide();
 	
	$('#price-adjustment-price-rounding-icon-' + siteId).show();
	$('#price-adjustment-span-' + siteId).show();

	$('#price-adjustment-edit-' + siteId).hide();
	$('#price-adjustment-textfield-' + siteId).blur();	

 	$('#price-adjustment-' + siteId).css({
 		zIndex: 1,
 		position: 'static',
 		background:'#ffffff'
 	});
};

function toggleTicketListingCrawlRow(crawlId) {
	$('#ticket-listing-crawl-' + crawlId).attr('checked', !$('#ticket-listing-crawl-' + crawlId).attr('checked'));
	updateTicketListingCrawlRow(crawlId);
};

function updateTicketListingCrawlRow(crawlId) {
	if ($('#ticket-listing-crawl-' + crawlId).attr('checked')) {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', '#748C20');
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', '#ffffff');
	} else {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', null);
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', null);
	}
};

//function updateTicketQuantity(qty) {
//	if ($('#quantity-' + qty).attr('checked')) {
//		alert('hi' + qty);
//	} else {
//		alert('bye' + qty);
//	}
//};

function enableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=enableCrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};
	
function disableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=disableCrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};

function forceRecrawlSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href = "EditorManageCrawls?action=forceRecrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};

function __getSelectedTicketListingCrawlIds() {
	var ids = "";
	$.each($('.ticket-listing-crawl'), function(i, elt) {
		if ($(elt).attr('checked')) {
			var id = $(elt).attr('id').split("-")[3]; 
			ids += id + ",";
		}
	});
	
	if (ids == "") {
		return null;
	}
	
	// remove the final comma
	ids = ids.substring(0, ids.length - 1)
	return ids;		
};

function selectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', true);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function unselectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', false);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function changeAddCrawler(siteId) {
	if (siteId) {
		document.location.href = "EditorEditTicketListingCrawl?action=create&siteId=" + siteId + "&tourId=${tour.id}&eventId=${event.id}&backUrl=" + escape("${ticketsPage}?eventId=${event.id}");
	}
};

function showRefreshedRecently() {
	$('.refreshLink').hide();
    $('.refreshedRecently').show();
    setTimeout("$('.refreshedRecently').hide(); $('.refreshLink').show();", ${1000 * ticketListingCrawler.minCrawlPeriod});
}

var totalCrawls;

function updateCrawls() {
    CrawlerDwr.forceRecrawlForEvent(${event.id}, function(response) {
      var tokens = response.split('|');
      if (tokens[0] != 'OK') {
        showRefreshedRecently();
      } else {
        totalCrawls = parseInt(tokens[1]);
        var runningCrawls = parseInt(tokens[2]);
        if (totalCrawls == 0) {
          showRefreshedRecently();
          return;
        }
      	$('.refreshLink').hide();
        $('.refreshedRecently').hide();
      	$('.refreshCrawlInfoSpan').show();
      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
        setTimeout("checkCrawlsFinished()", 10000);
      }
    });
  	return false;
}

function checkCrawlsFinished() {
    CrawlerDwr.getCrawlsExecutedForEventCount(${event.id}, function(response) {
      var count = parseInt(response);
      $('.refreshCrawlInfo').html(parseInt(100 * (count / totalCrawls)) + "% updated");
      if(count < totalCrawls) {
        setTimeout("checkCrawlsFinished()", 10000);
      } else {
        // force reload (avoid caching by adding a dummy parameter)
        $('.refreshCrawlInfo').html("100% updated! Reloading page...");
        location.href = '${ticketsPage}?eventId=${event.id}&nocrawl=true&reload=' + (new Date().getTime()) + "&scroll=" + window.scrollY + anchor;
      }
    });
}

  function markAsDuplicate() {
    if ($('.ticket_checkbox:checked').length < 2) {
      alert("You must select at least 2 tickets");
      return false;
    }
  
    var answer = confirm("Are you sure you want to mark the selected tickets as duplicate?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each($('.ticket_checkbox'), function(intIndex, obj) {
      if($(obj).attr('checked') + '' == 'true') {
        params += $(obj).attr('id').replace('ticket_', '') + ','
      };
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=create&dups=' + params;
	return false;
  }

  function unmarkAsDuplicate() {
    if ($('.ticket_checkbox:checked').length < 1) {
      alert("You must select at least 1 ticket");
      return false;
    }
  
    var answer = confirm("Are you sure you want to unmark the selected tickets as duplicate?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each($('.ticket_checkbox'), function(intIndex, obj) {
      if($(obj).attr('checked') + '' == 'true') {
        params += $(obj).attr('id').replace('ticket_', '') + ','
      };
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=delete&dups=' + params;
	return false;
  }
  
  function markAsAdmitOne() {
    if ($('.ticket_checkbox:checked').length < 1) {
      alert("You must select at least 1 tickets");
      return false;
    }
  
    var answer = confirm("Are you sure you want to mark the selected tickets as AdmitOne tickets?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each($('.ticket_checkbox'), function(intIndex, obj) {
      if($(obj).attr('checked') + '' == 'true') {
        params += $(obj).attr('id').replace('ticket_', '') + ','
      };
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=admit&dups=' + params;
	return false;
  }

  function unmarkAsAdmitOne() {
    if ($('.ticket_checkbox:checked').length < 1) {
      alert("You must select at least 1 ticket");
      return false;
    }
  
    var answer = confirm("Are you sure you want to unmark the selected tickets as AdmitOne tickets?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each($('.ticket_checkbox'), function(intIndex, obj) {
      if($(obj).attr('checked') + '' == 'true') {
        params += $(obj).attr('id').replace('ticket_', '') + ','
      };
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=unadmit&dups=' + params;
	return false;
  }
  
  var duplicateTicketMap = [
    <c:forEach var="duplicateTicket" items="${duplicateTicketMap}">
      [${duplicateTicket.ticketId}, ${duplicateTicket.groupId}],
    </c:forEach>
  ];
  
  function getGroupId(ticketId) {
	   for (var i = 0; i < duplicateTicketMap.length; i++) {
	     if (duplicateTicketMap[i][0] == ticketId) {
	       return duplicateTicketMap[i][1];
	     }
	   }
  }

  function getNumDuplicateTicketsByGroupId(groupId) {
       var numDups = 0;
	   for (var i = 0; i < duplicateTicketMap.length; i++) {
	     if (duplicateTicketMap[i][1] == groupId) {
	       numDups++;
	     }
	   }
  }

  function getNumTickedDuplicateTicketsByGroupId(groupId) {
       var numDups = 0;
	   for (var i = 0; i < duplicateTicketMap.length; i++) {
	     if (duplicateTicketMap[i][1] == groupId) {
	       if ($('#ticket_' + duplicateTicketMap[i][0]).is(':checked')) {
	         numDups++;
	       }
	     }
	   }
	   return numDups;
  }
  
  function tickDuplicateTicket(ticketId) {
     // if you just tick a ticket which has duplicate, tick the other tickets
     var groupId = getGroupId(ticketId);
     if (getNumTickedDuplicateTicketsByGroupId(groupId) == 1) {
	   groupId = getGroupId(ticketId);
	   if (groupId == null) {
	     return;
	   }
	   for (var i = 0; i < duplicateTicketMap.length; i++) {
	     if (duplicateTicketMap[i][1] == groupId) {
	       $('#ticket_' + duplicateTicketMap[i][0]).attr('checked', true);
	     }
	   }
	 }    
  }
  
  function untickAll() {
    $('.ticket_checkbox').removeAttr('checked');
  }

	function toggleTicketFilter() {
		if ($('#ticket-filter-info').css('display') == 'none') {
			$('#ticket-filter-info').show();
			$('#ticket-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		} else {
			$('#ticket-filter-info').hide();
			$('#ticket-filter-expand-img').attr('src', '../images/ico-expand.gif');
		}
	};
	  
	function toggleEventDetails() {
		if ($('#event-details-info').css('display') == 'none') {
			$('#event-details-info').show();
			$('#event-details-expand-img').attr('src', '../images/ico-collapse.gif');
		} else {
			$('#event-details-info').hide();
			$('#event-details-expand-img').attr('src', '../images/ico-expand.gif');
		}
	};
	    
  
</script>
