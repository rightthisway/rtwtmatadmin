<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 
	&gt; Quotes
</div>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<c:choose>
  <c:when test="${empty customers}">
    <i>No quotes defined</i>
  </c:when>
  <c:otherwise>
  <form enctype="multipart/form-data" action="Quotes" id="quotesForm" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<!--
    <img src="../images/ico-user.gif" align="bottom"/><b>User:</b>
	<select name="creator" onchange="$('#quotesForm').submit()">
	  <c:forEach var="iCreator" items="${creators}">
	    <option value="${iCreator}" <c:if test="${creator == iCreator}">selected</c:if>>${iCreator}</option>
	  </c:forEach>
	</select>
    
	<b>Customer:</b>
	<select name="customerId" onchange="$('#quotesForm').submit()">
	  <c:forEach var="iCustomer" items="${customers}">
	    <option value="${iCustomer.id}" <c:if test="${iCustomer.id == customer.id}">selected</c:if>>${iCustomer.name}</option>
	  </c:forEach>
	</select>
	
	<b>Date:</b>
	<select name="date" onchange="$('#quotesForm').submit()">
	  <c:forEach var="date" items="${quoteDates}">
	    <fmt:formatDate var="formattedDate" pattern="MM/dd/yyyy" value="${date}"/>
	    <option value="${formattedDate}" <c:if test="${date == quote.quoteDate}">selected</c:if>>${formattedDate}</option>
	  </c:forEach>
	</select>
	
	<b>Sent on:</b>
	<select name="quoteId" onchange="$('#quotesForm').submit()">
	  <c:forEach var="curQuote" items="${quotes}">
	    <fmt:formatDate var="formattedDate" pattern="MM/dd/yyyy HH:mm" value="${curQuote.sentDate}"/>
	    <c:choose>
	      <c:when test="${not empty curQuote.sentDate}">
	       <option value="${curQuote.id}" <c:if test="${quote.id == curQuote.id}">selected</c:if>>${formattedDate}</option>
	      </c:when>
	      <c:otherwise>
	       <option value="${curQuote.id}" <c:if test="${quote.id == curQuote.id}">selected</c:if>>OPEN</option>
	      </c:otherwise>
	    </c:choose>
	  </c:forEach>
	</select>
	<br/>
	-->
	<h2>Quotes</h2>
	
	<table>
	  <tr>
	    <td><b>From: </b></td>
	    <td>${from}</td>
	  </tr>
	  <tr>
	    <td><b>To: </b></td>
	    <td><input type="text" name="to" size="80" value="${customer.email}"></td>
	  </tr>
	  <tr>
	    <td><b>BCC: </b></td>
	    <td><input type="text" name="bcc" size="80" value="${user.email}"></td>
	  </tr>
	  <tr class="editableContent" <c:if test="${not empty quote.sentDate}">style="display:none"</c:if>>
	    <td><b>Subject: </b></td>
	    <td><input type="text" name="subject" size="80" value="Quote Requested"></td>
	  </tr>
	</table>
	<br/>
	<c:if test="${not empty quote.sentDate}">
	  <a href="#" onclick="$('.editableContent').show(); $(this).hide(); return false;">Show Editable Content</a>
	</c:if>
    <hr/>
    <div class="editableContent" <c:if test="${not empty quote.sentDate}">style="display:none"</c:if>>
      <b style="color:#777777">${quotesHeader}</b><br/>
      
      <br/>
    </div>

	<c:forEach var="quotesEvent" items="${quotesEvents}">
	  <b>Event:</b> ${quotesEvent.event.name}<br/>
	  <b>Date: </b> <fmt:formatDate pattern="MM/dd/yyyy" value="${quotesEvent.event.localDate}"/>
	  <c:choose>
	    <c:when test="${not empty quotesEvent.event.localTime}">
	      <fmt:formatDate pattern="hh:mm aa" value="${quotesEvent.event.localTime}"/>
	    </c:when>
	    <c:otherwise>
	      TBD
	    </c:otherwise>
	  </c:choose>
	  <br/>
	  <b>Venue:</b> ${quotesEvent.event.venue.building}, ${quotesEvent.event.venue.city}, ${quotesEvent.event.venue.state}<br/><br/>  
	  
	  <table border="1px" cellspacing="0px">
	    <thead>
	      <th>Quantity</th>
	      <th>Section</th>
	      <th>Row</th>
	      <th>Price</th>	      
	    </thead>
	    <tbody>
	      <c:forEach var="quote" items="${quotesEvent.quotes}">
	        <tr>
	          <td>${quote.ticket.remainingQuantity}</td>
	          <td>${quote.ticket.section}</td>
	          <td>${quote.ticket.row}</td>
	          <td><fmt:formatNumber type="currency" value="${quote.markedPrice}"/></td>
	        </tr>
	      </c:forEach>  
	    </tbody>	      
	  </table>
	  <br />
	  <c:choose>
	    <c:when test="${quotesEvent.map != ''}">			
		${quotesEvent.map}		
	    </c:when>
	    <c:otherwise>		 
	     <img src="../images/ico-upload.gif" align="absmiddle" /> Upload Map <i>(optional)</i>: <input type="file" name="map_${quotesEvent.event.id}"/>
	    </c:otherwise>
	  </c:choose>
	  
  	  <br/><br/>
	</c:forEach>
	
    
	  <b style="color:#777777">${quotesFooter}</b><br/>
      
    <br />

  Please note: None of these seats are on hold and are subject to availability.<br/>
  If you are looking for something I have not listed please let me know.<br/><br/>
  ${user.firstName} ${user.lastName}<br/> 
  Right This Way, LLC<br/>
  1441 Broadway<br/>
  Suite 2402<br/>
  New York, NY 10018<br/>
  212-383-2800 - Main<br/>
  212-382-2140 - Fax<br/>
  <a href="http://www.rightthisway.com/">www.rightthisway.com</a><br/>
  <a href="mailto:${user.email}">${user.email}</a><br/>
  <img src="../images/rightthisway_logo.gif"/><br/>
  <br/><br/>
  <input id="send" onclick="submit();" type="button" class="medButton" value="Send"/>
  <input type="hidden" name = "action" id="action" value=""/>
  	
	<h2>Admin</h2>
	<img src="../images/ico-delete.gif" align="absbottom"/><a href="#" onclick="deleteQuotes(); return false">Delete Quotes for this customer for ${formattedDate}</a><br/>
	<img src="../images/ico-delete.gif" align="absbottom"/><a href="#" onclick="deleteCustomer(); return false">Delete Customer (and associated quotes)</a><br/>
   </form>	  
  </c:otherwise>
</c:choose>

<script type="text/javascript">
function submitForm() {
$("#action").val("preview");	
	$('#quotesForm').submit();
}
function submit() {
$("#action").val("send");	
	$('#quotesForm').submit();
}
  function deleteQuotes() {
    if (confirm('Are you sure you want to delete quotes for this customer for this date?')) {
       location.href = 'Quotes?action=deleteQuotes&customerId=${customer.id}&date=${formattedDate}';
    }
  }

  function deleteCustomer() {
    if (confirm('Are you sure you want to delete this customer?')) {
       location.href = 'Quotes?action=deleteCustomer&customerId=${customer.id}&date=${formattedDate}';
    }
  }
  
  function updateEmail() {
       location.href = 'Quotes?action=updateEmail&email=' + $('#email').val() + '&customerId=${customer.id}&date=${formattedDate}';
  }
  
  function swapFooter(value) {
    if (!value) {
      $('#footer').attr('disabled', 'disabled');
    } else {
      $('#footer').removeAttr('disabled');
    }
  }
  
    var ticketQuotes = new Array();
    <c:forEach var="quotesEvent" items="${quotesEvents}">
      ticketQuotes[${quotesEvent.event.id}] = new Array();
      <c:forEach var="quote" items="${quotesEvent.quotes}">
        ticketQuotes[${quotesEvent.event.id}][${quote.id}] = [${quote.ticket.remainingQuantity}, ${quote.markedPrice}];
      </c:forEach>
    </c:forEach>
    
  function updateQuotes(eventId, increment) {
    if ($.trim(increment) == '') {
      alert("Please type a number");
      return;
    }
  
    for (var key in ticketQuotes[eventId]) {
      var amount = ticketQuotes[eventId][key][1];
      $('#markedPrice_' + key).val(Math.ceil(amount * (100 + parseFloat(increment)) / 100));
      updatePercentIncrement(eventId, key);
    }
    recalculateTotal(eventId);
  }
  
  function updatePercentIncrement(eventId, quoteId) {   
    var basePrice = ticketQuotes[eventId][quoteId][1];
    var newVal = parseFloat($('#markedPrice_' + quoteId).val());
    if (newVal == undefined) {
      newVal = 0;
    }
    var percent = parseInt(10000 * ((newVal - basePrice) / basePrice)) / 100
    $('#quote_percent_inc_' + quoteId).text(percent);
  }

  function recalculateTotal(eventId) {
    var total = 0;
    for (var quoteId in ticketQuotes[eventId]) {
      var basePrice = parseFloat($('#markedPrice_' + quoteId).val());
      var amount = ticketQuotes[eventId][quoteId][0] * basePrice;
      total += amount; 
    }
    $('#total_' + eventId).text(total);
    $('#total_' + eventId).text(Math.ceil(parseFloat($('#total_' + eventId).text())));
    $('#total_' + eventId).formatCurrency();
  }

    <c:forEach var="quotesEvent" items="${quotesEvents}">
      recalculateTotal(${quotesEvent.event.id});
    </c:forEach>

</script>