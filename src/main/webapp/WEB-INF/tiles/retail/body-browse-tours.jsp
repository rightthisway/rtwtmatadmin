<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="BrowseArtists">Browse Artists/Teams</a> 
  &gt; Browse Tours/Seasons
</div>

<div id="quickLinks">		 	
 <%-- <authz:authorize ifAnyGranted="ROLE_EDITOR"> --%>
	<img src="../images/ico-edit.gif" align="absbottom" /> <a href="EditorManageTours">Edit Tours/Seasons</a>
  <%-- </authz:authorize> --%>
</div>

<div style="clear:both"></div>

<h1>Browse Tours/Seasons</h1>

<div style="float:left"><b>Artist/Team: </b></div>
<c:if test="${not empty param.artistId}">
  <div id="star_artist_${param.artistId}" class="starOff" onclick="toggleBookmarkArtist(${param.artistId})"></div>
</c:if>
<div style="float: left">
<select name="artistId" onchange="if ($(this).val() != '') location.href='BrowseTours?artistId=' + $(this).val(); else location.href='BrowseTours'">
  <option value="">ALL</option>
  <c:forEach var="artist" items="${artists}">
    <option value="${artist.id}" <c:if test="${artist.id == param.artistId}">selected</c:if>>${artist.name}</option>
  </c:forEach>
</select>
</div>
<div style="clear:both"></div>

<%-- <authz:authorize ifAnyGranted="PRIV_DATA">		 --%>	
	
	<c:if test="${not empty param.artistId}">
		<h2>Price Adjustments</h2>
		<div>
			<c:forEach var="siteId" items="${constants.siteIds}">
				<div style="float: left; width: 280px; height: 24px;">
					<img src="../images/ico-${siteId}.gif" border="0" align="absmiddle"/>						
					${siteId}
					(
					<c:set var="priceAdjustment" value="${priceAdjustments[siteId]}" />
					<c:choose>
						<c:when test="${empty priceAdjustment || priceAdjustment.percentAdjustment == 0}">
							<span id="price-adjustment-span-${siteId}">+0.00%</span>
						</c:when>
						<c:when test="${priceAdjustment.percentAdjustment < 0}">
							<span id="price-adjustment-span-${siteId}" style="color: #aa2222; font-weight: bold">
								<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
							</span>
						</c:when>
						<c:otherwise>
							<span id="price-adjustment-span-${siteId}" style="color: #22aa22; font-weight: bold">
								+<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
							</span>
						</c:otherwise>
					</c:choose>
	
					<c:choose>
						<c:when test="${empty siteById[siteId] || siteById[siteId].priceRounding == 'NONE'}">
						</c:when>								
						<c:when test="${siteById[siteId].priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
						</c:when>
						<c:when test="${siteById[siteId].priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
						</c:when>
					</c:choose>
	
					<a href="EditArtistPriceAdjustment?artistId=${param.artistId}&siteId=${siteId}">change</a>				
					)
				</div>
			</c:forEach>
		</div>
		<div style="clear:both"></div>
	
		<%-- <authz:authorize ifAnyGranted="ROLE_EDITOR"> --%>			
			<a href="EditorEditSites" style="font-size:10px">Change price rounding</a>
		<%-- </authz:authorize> --%>
	</c:if>
<%-- </authz:authorize> --%>
<br/>
<form method="GET" action="BrowseTours">
<b>Filter Tours/Seasons:</b> <input id="filter" name="filter" type="text" value="${param.filter}" />
  <c:if test="${not empty param.artistId}">
    <input type="hidden" name="artistId" value="${param.artistId}" />
  </c:if>
  <input class="medButton" type="submit" value="Filter" onclick="return ValidateFilter()"/>
  <c:if test="${not empty param.filter}"> <a href="BrowseTours<c:if test="${not empty param.artistId}">?artistId=${param.artistId}</c:if>">Clear Filter</a></c:if>
</form>
<br />
<display:table class="list" name="${tours}" id="tour" requestURI="BrowseTours" pagesize="20" defaultsort="5" defaultorder="descending">

    <display:column style="width:20px"><div id="star_${tour.id}" class="starOff" onclick="toggleBookmarkTour(${tour.id})"></div></display:column>
     <display:column  value="View" href="BrowseEvents" paramId="tourId" paramProperty="id" class="bold" ></display:column>
   
    <display:column title="Artist/Team"  sortable="true" property="artist.name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Tour/Season Name" sortable="true" class="content-left-align" headerClass="header-left-align">
      <img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}"/> ${tour.name}
    </display:column>
   
    <display:column title="Event Count" sortable="true" class="text-align-center" property="eventCount" />
    <display:column title="Dates" sortable="true" sortProperty="startDate">
      <span style="white-space:nowrap">
        <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
        -
        <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
      </span>
    </display:column>
</display:table>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${tourBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.tour.id}').parent().parent().css('background-color', '#eeee44');    
  </c:forEach>

  <c:if test="${not empty artistBookmark.objectId}">
    $('#star_artist_' + ${artistBookmark.objectId}).attr('class', 'starOn');
  </c:if>
  
  function toggleBookmarkArtist(artistId) {
    	BookmarkDwr.toggleBookmarkArtist(artistId, 
		   function(response) {
			 $('#star_artist_' + artistId).attr('class', 'star' + response);
		   });
  }

  function toggleBookmarkTour(tourId) {
	BookmarkDwr.toggleBookmarkTour(tourId, 
		   function(response) {
			 $('#star_' + tourId).attr('class', 'star' + response);
			if (response == "On") {
			 	$('#star_' + tourId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + tourId).parent().parent().css('background-color', null);
			 }				   							      
			   							       
		   });
  }
function ValidateFilter()
{


if(document.getElementById('filter').value=="")
{

alert("Please enter the name of Tours/Season in order to filter");
return false;
}

}
</script>