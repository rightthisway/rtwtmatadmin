<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  <a href="BrowseArtists">Browse Artists</a> 
  &gt; Browse Tours
</div>

<a href="BrowseTours?view=normal">Normal View</a> | <b>Quick View</b>

<h1>Browse Tours</h1>

<div style="float:left"><b>Artist:</b></div>
<c:if test="${not empty param.artistId}">
  <div id="star_artist_${param.artistId}" class="starOff" onclick="toggleBookmarkArtist(${param.artistId})"></div>
</c:if>
<div style="float: left">
<select name="artistId" onchange="location.href='BrowseTours?artistId=' + $(this).val()">
  <option value="">ALL</option>
  <c:forEach var="artist" items="${artists}">
    <option value="${artist.id}" <c:if test="${artist.id == param.artistId}">selected</c:if>>${artist.name}</option>
  </c:forEach>
</select>
</div>
<div style="clear:both"></div>
<br/><br/>
<form method="GET" action="BrowseTours">
<b>Filter Tours:</b> <input name="filter" type="text" value="${param.filter}" />
<input class="medButton" type="submit" value="Filter"/>
</form>

<c:forEach var="tour" items="${tours}">
  <div class="tourGridDiv">
    <div style="display:inline" id="star_${tour.id}" class="starOff" onclick="toggleBookmarkTour(${tour.id})"></div>
    <a href="BrowseEvents?tourId=${tour.id}"><b>${tour.name}</b></a> (${fn:length(tour.events)})

   <div style="clear:both"></div>
   
   <div style="text-align:center">
    <a href="BrowseTours?artistId=${tour.artist.id}" style="font-style:italic">${tour.artist.name}</a><br/><br/>
    <b>Event: </b><select id="event_tour_${tour.id}" onchange="location.href='BrowseTickets?eventId=' + $(this).val()">
    <c:forEach var="event" items="${tour.events}">
  	  <option value="${event.id}"><fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" /></option>
    </c:forEach>
    </select>
    <img align="bottom" src="../images/ico_arrow_right.gif" onclick="location.href='BrowseTickets?eventId=' + $('#event_tour_${tour.id}').val()"/>
   </div>
  </div>
</c:forEach>
  <div style="clear:both"></div>

<%-- <authz:authorize ifAnyGranted="ROLE_EDITOR"> --%>
	<h2>Administration</h2>
	<c:choose>
		<c:when test="${empty tour.id}">
			<a href="EditorManageTours">Manage Tours</a>
		</c:when>
		<c:otherwise>
			<a href="EditorDetailTour?tourId=${tour.id}">Edit Tour</a>
		</c:otherwise>
	</c:choose>
<%-- </authz:authorize> --%>


<script type="text/javascript">
  <c:forEach var="bookmark" items="${tourBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.tour.id}').parent().css('background-color', '#eeee44');    
  </c:forEach>

  <c:if test="${not empty artistBookmark.objectId}">
    $('#star_artist_' + ${artistBookmark.objectId}).attr('class', 'starOn');
  </c:if>
  
  function toggleBookmarkArtist(artistId) {
    	BookmarkDwr.toggleBookmarkArtist(artistId, 
		   function(response) {
			 $('#star_artist_' + artistId).attr('class', 'star' + response);
		   });
  }

  function toggleBookmarkTour(tourId) {
	BookmarkDwr.toggleBookmarkTour(tourId, 
		   function(response) {
			 $('#star_' + tourId).attr('class', 'star' + response);
			if (response == "On") {
			 	$('#star_' + tourId).parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + tourId).parent().css('background-color', null);
			 }				   							      
			   							       
		   });
  }
  
</script>