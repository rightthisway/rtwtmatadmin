<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>
<script type="text/javascript" src="../js/jquery-all.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 
	&gt; View Quotes
</div>
<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate pattern="yyyy" value="${now}" var="currentYear" />
<fmt:formatDate pattern="M" value="${now}" var="currentMonth" />
<fmt:formatDate pattern="d" value="${now}" var="currentDate" />
<style type="text/css">
            @import url(../css/common.css);
		</style>
<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
  <form enctype="multipart/form-data" action="ListQuotes" id="quotesListForm" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />    
	<input type="hidden" id ="fetch" name="fetch" value="" /> 
	<input type="hidden" id ="type" name="type" value="zone" /> 
	<h2>Quotes</h2>
	<b>
	From Date: </b>


<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> 
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />

     <b>To: </b>
<select name="toMonth" id="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDate}">selected</c:if>>${day}</option>
  </c:forEach>
</select> /
<select name="toYear" id="toYear">
  <c:forEach var="year" begin="2009" end="2020" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select>
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
<br /><br />
	<img src="../images/ico-user.gif" align="bottom"/><b>User:</b>
	
	<select name="creator" onchange="$('#quotesListForm').submit()">
	<option value="ALL"> -- All --</option>
		<c:forEach var="iCreator" items="${creators}">
			<option value="${iCreator}" <c:if test="${creator == iCreator}">selected</c:if>>${iCreator}</option>
		</c:forEach>
	</select>    	
	<b>Customer:</b> <!-- <input type="text" id="quoteCustomer" size="12"/> -->
	<select name="customerId" onchange="$('#quotesListForm').submit()">
	<option value="ALL"> -- All --</option>
	  <c:forEach var="iCustomer" items="${customers}">
	    <option value="${iCustomer.id}" <c:if test="${iCustomer.id == customer.id}">selected</c:if>>${iCustomer.name}</option>
	  </c:forEach>
	</select>
	<br /><br />
E-mail :
<select name="email" id="email">
<option value="ALL" <c:if test="${email == 'ALL'}">selected</c:if>> -- All --</option>
 <c:forEach var="iemail" items="${emails}">
		<c:if test="${iemail != null}">
	    <option value="${iemail}" <c:if test="${iemail == email}">selected</c:if>>${iemail}</option>
		</c:if>
	  </c:forEach> 
</select>  
status:
<select name="status" id="status">
<option value="ALL" <c:if test="${status == 'ALL'}">selected</c:if>> -- All --</option>
	<option value="1" <c:if test="${status == '1'}">selected</c:if>>Unsent</option>
	<option value="-1" <c:if test="${status == '-1'}">selected</c:if>>Sent</option>  
  
</select>
<br /><br />
<input id="getQuoteDataButton" type="button" name="action" onclick = "submitForm();" class="medButton" value="Fetch Quotes"/> 
</form>
<br />


<c:choose>
<c:when test="${not empty ticketQuotes}">
<display:table class="list" name="${ticketQuotes}" id="quote"  requestURI="ViewQuotes" pagesize="50" export="true">	
	<display:column title="Id" group="1" sortable="true"  property="quote.id" />
	<display:column title="Customer Name" group="2" sortable="true"  property="quote.customer.name" />
	<display:column title="User Name" group="3"   sortable="true" property="quote.username"/>    
	<display:column title="Event Name" property="ticket.event.name"/>
	<display:column title="event Date & Time" property="ticket.event.formatedDate"/>
	<display:column title="Venue"  property="ticket.event.venue.building"/>
	<<display:column title="Section" sortable="true">
		<c:if test="${quote.zone == null || quote.zone==''}">
			${quote.ticket.section}
		</c:if>
	</display:column>
	<display:column title="Row" sortable="true">
		<c:if test="${quote.zone == null || quote.zone==''}">
			${quote.ticket.row}
		</c:if>
	</display:column>
	<display:column title="Zone"  property="zone" sortable="true"/>
	<display:column title="Quantity" sortable="true">
		<c:if test="${quote.zone == null || quote.zone==''}">
			${quote.qty}
		</c:if>
		<c:if test="${quote.zone != null && quote.zone!=''}">
			${quote.zoneQty}
		</c:if>
	</display:column>
	
	<display:column title="Price"  property="zonePrice" sortable="true"/>	
	<display:column title="Quote Date" >
	<fmt:formatDate var="formattedDate" pattern="MM/dd/yyyy hh:mm:ss" value="${quote.quote.creationDate}"/>
	${formattedDate}
	</display:column>
    <display:column title="Customer E-mail"  property="quote.sentTo" />
	<display:column title="Link" group = "11" sortable="true">
	<input type="button" class="medButton" onclick="sendToappend(${quote.quote.id},'${quote.quote.customer.name}','zone');" value="Append">
	</display:column>	
	</display:table>
</c:when >
<c:otherwise>
There is no Quote to display.
</c:otherwise>
</c:choose>

<script type="text/javascript">
function sendToappend(quoteId,customer,type){

	var r= confirm("Are you sure to append ?");		
	if(r == true){	
	markupPercentAddition = '${markupPercentAddition}';		
	window.opener.sendAppendRequest(quoteId,customer,type);
	window.close();
	}else{
	window.colse();
	}

};
function viewQuote(custId,quoteId){
	var url = 'Quotes?customerId='+custId+'&quoteId='+quoteId;	
	window.location = url;
}
var customers = [
	<c:forEach var="customer" items="${customers}">
		'${customer.name}', 
	</c:forEach>
	''
];
$('#quoteCustomer').autocomplete(customers);
$('#quoteCustomer').val('${customer.name}');
 $(document).ready(function() {
		$('.calendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
	         maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)
		});
		
		$('#fromCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
	        }
	    );

		$('#fromCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#fromYear').val(tokens[0]*1);
				$('#fromMonth').val(parseInt(tokens[1]*1));
				$('#fromDay').val(parseInt(tokens[2]*1));
			}
		);
		
		$('#toCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
	        }
	    );
		
		$('#toCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#toYear').val(tokens[0]*1);
				$('#toMonth').val(parseInt(tokens[1])*1);
				$('#toDay').val(parseInt(tokens[2])*1);
			}
		);			
	});	
	function submitForm(){	
	$("#fetch").val("fetch");	
	$('#quotesListForm').submit();
	}	

</script>