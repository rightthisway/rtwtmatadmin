<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath"  class="breadCrumbPathUser">
	<a href="BrowseArtists">Browse</a> 
	
	<c:if test="${not empty tour}">
		 &gt; <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
		 &gt; ${tour.name}
	</c:if>
</div>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
	  <div class="infoText">${param.info}</div>
	</div>
</c:if>

<div id="quickLinks">
 <a href="MyAlerts?filterTourId=${event.artist.id}">Create alert</a>
 <%-- <authz:authorize ifAnyGranted="PRIV_SHORT"> --%>
 	 <c:if test="${tour != null}">
	 	| <a href="TMATBrowseEvents?tourId=${tour.id}">Go to Shorts</a>
	 </c:if>
 <%-- </authz:authorize>	 
 <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
 	 <c:if test="${tour != null}">
		| <img src="../images/ico-edit.gif" align="absbottom" /> <a href="EditorDetailTour?tourId=${tour.id}">Edit Tour</a>
	</c:if>
 <%-- </authz:authorize>
 <authz:authorize ifAnyGranted="PRIV_CATEGORY"> --%>
  	<c:if test="${tour != null}">
		| <a href="EditorEditCategories?tourId=${tour.id}">Go to Categorization</a>
	</c:if>
  <%-- </authz:authorize>
 <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
	| <a href="EditorManageCrawls?tourId=${tour.id}">Go to Crawler</a>
  <%-- </authz:authorize> --%>
</div>
<div style="clear:both"></div>

<h1>Browse Events</h1>
<c:choose>
  <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
    <c:if test="${tour != null}">(<a href="BrowseEvents?tourId=${tour.id}">Switch to normal view</a>)</c:if>
     <c:if test="${venue != null}">(<a href="BrowseEvents?venueId=${venue.id}">Switch to normal view</a>)</c:if>
  </c:when>
  <c:otherwise>
	 <%-- <authz:authorize ifAnyGranted="PRIV_ANALYTIC"> --%>
	   <c:if test="${tour != null}">(<a href="BrowseEventsAnalytic?tourId=${tour.id}">Switch to analytic view</a>)</c:if>
	    <c:if test="${venue != null}">(<a href="BrowseEventsAnalytic?venueId=${venue.id}">Switch to analytic view</a>)</c:if>
	 <%-- </authz:authorize> --%>
  </c:otherwise>
</c:choose>

<c:if test="${not empty tour}">
	<div style="float: left">
		<div style="float:left">
			<div style="float:left"><b>Tour:</b></div>
		    <div id="star_tour_${param.tourId}" class="starOff" style="margin-top: -4px;" onclick="toggleBookmarkTour(${param.tourId})"></div>
			<div style="float:left"><img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}"/>${tour.name}</div>
		</div>
		<div style="float:left; clear: left;">
			<b>Type:</b> ${tour.tourType}<br />
			<b>${tour.artistLabel}:</b> <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a><br />
			<b>Location:</b> ${tour.location}<br /><br />
			<b>Start Date:</b> <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/><br />
			<b>End Date:</b> <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/><br /><br />
			<b>Event Count:</b> ${fn:length(events)}<br />			
		</div>
	</div>
</c:if>
<c:if test="${not empty venue}">
	<div style="float: left">
		<div style="float:left">
			<div style="float:left"><b>Venue:</b></div>
			<b>${venue.building}</b><br/>
		</div>
		<div style="float:left; clear: left;">
		<br/>
			<b>City :</b> ${venue.city}<br/>
			<b>State : </b>${venue.city}<br/>
			<b>Country :</b> ${venue.country}<br/>
			<b>Type:</b> ${venue.venueType}<br />
			<b>Event Count:</b> ${fn:length(events)}<br />			
		</div>
	</div>
</c:if>

<div style="clear:both"></div>

<%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
	
	<h2>Price Adjustments</h2>
	<div>
		<c:forEach var="siteId" items="${constants.siteIds}">
			<div style="float: left; width: 280px; height: 24px;">
				<img src="../images/ico-${siteId}.gif" border="0" align="absmiddle"/>						
				${siteId}
				(
				<c:set var="priceAdjustment" value="${priceAdjustments[siteId]}" />
				<c:choose>
					<c:when test="${empty priceAdjustment || priceAdjustment.percentAdjustment == 0}">
						<span id="price-adjustment-span-${siteId}">+0.00%</span>
					</c:when>
					<c:when test="${priceAdjustment.percentAdjustment < 0}">
						<span id="price-adjustment-span-${siteId}" style="color: #aa2222; font-weight: bold">
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
						</span>
					</c:when>
					<c:otherwise>
						<span id="price-adjustment-span-${siteId}" style="color: #22aa22; font-weight: bold">
							+<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
						</span>
					</c:otherwise>
				</c:choose>
	
				<c:choose>
					<c:when test="${empty siteById[siteId] || siteById[siteId].priceRounding == 'NONE'}">
					</c:when>								
					<c:when test="${siteById[siteId].priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
					</c:when>
					<c:when test="${siteById[siteId].priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
					</c:when>
				</c:choose>
	
				<a href="EditTourPriceAdjustment?tourId=${tour.id}&siteId=${siteId}">change</a>
				)
			</div>
		</c:forEach>
	</div>
<%-- </authz:authorize> --%>

<div style="clear:both"></div>
<%-- <authz:authorize ifAnyGranted="PRIV_DATA">	 --%>		
	<a href="EditorEditSites?backUrl=BrowseEvents?tourId=${tour.id}" style="font-size:10px">Change price rounding</a>
<%-- </authz:authorize> --%>

<h2>Events</h2>

<%
java.util.Date startDate = new java.util.Date();
%>
<form id="event_form" method="POST" action="EbayAddInventory">

<c:if test="${not empty events}">
<%-- <authz:authorize ifAnyGranted="PRIV_EBAY,PRIV_DATA"> --%>
Select:
<a href="#" onclick="selectAll(); return false">All</a> |
<a href="#" onclick="unselectAll(); return false">None</a> |
<a href="#" onclick="invertAll(); return false">Invert</a>
<br/><br/>
<%-- </authz:authorize> --%>
</c:if>

<display:table class="list" name="${events}" id="event" requestURI="BrowseEvents">
   <%--  <authz:authorize ifAnyGranted="PRIV_EBAY,PRIV_DATA"> --%>
      <display:column>
        <input type="checkbox" id="event_${event.id}" name="event_${event.id}" class="event_checkbox"/>
      </display:column>
   <%--  </authz:authorize> --%>
    <display:column style="width:20px"><div id="star_${event.id}" class="starOff" onclick="toggleBookmarkEvent(${event.id})"></div></display:column>
	    <c:choose>
	      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
	        <display:column>
	          <b>
	            <a href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">View</a>
	          </b>
	        </display:column>    
	      </c:when>
	      <c:otherwise>
	        <display:column value="View"  href="BrowseTickets" paramId="eventId" paramProperty="id" class="bold" />
	      </c:otherwise>
	    </c:choose>
    <display:column title="Date" sortable="true">
    	<b>
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
			<c:choose>
			  <c:when test="${not empty event.time}">
			    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
			  </c:when>
			  <c:otherwise>
			    TBD
			  </c:otherwise>
			</c:choose>		
		</b>
    </display:column>
    <display:column title="Event Name" sortable="true" sortProperty="name">
      <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/> ${event.name}
    </display:column>
    <display:column title="Venue" sortable="true" >${eventToVenue[event.id].building}</display:column>
    <display:column title="State" sortable="true" >${eventToVenue[event.id].state}</display:column>
    <display:column title="Country">
    	<c:if test="${not empty eventToVenue[event.id].country && eventToVenue[event.id].country != 'null' }">
    		<img src="../images/flags/${eventToVenue[event.id].country}.gif" />${eventToVenue[event.id].country}
    	</c:if>
    </display:column>
    
    <c:choose>
      <c:when test="${browseEventsPage == 'BrowseEventsAnalytic'}">
        <display:column title="OPHigh">
        	${eventToActions[event.id]['OH']}
        </display:column>
        <display:column title="OPMed">
        	${eventToActions[event.id]['OM']}
        </display:column>
        <display:column title="OPLow">
        	${eventToActions[event.id]['OL']}
        </display:column>
        <display:column title="Correct">
        	${eventToActions[event.id]['CO']}
        </display:column>
        <display:column title="UPLow">
        	${eventToActions[event.id]['UL']}
        </display:column>
        <display:column title="UPMed">
        	${eventToActions[event.id]['UM']}
        </display:column>
        <display:column title="UPHigh">
        	${eventToActions[event.id]['UH']}
        </display:column>
      </c:when>
    </c:choose>    
    <display:column title="Managed By" sortable="true" sortProperty="marketMakerManager" class="text-align-center">
      <c:if test="${event.marketMakerManager != 'DESK'}">
      <img src="../images/ico-user.gif" align="absbottom"/>
      </c:if>
      ${event.marketMakerManager}
    </display:column>
  
  	<display:column title="CATS Count" class="text-align-center">
  		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">${fn:length(event.category)}</a>
  	</display:column>
  	
  	<display:column title="CATS Group" class="text-align-center">
		<a href="javascript: return null;" onclick="$('#displaycats${event.id}').dialog('open');">
			${event.uniqueCategoryGroupName}
		</a>	 			
  	</display:column>
  	
    <%--

    <display:column title="Listing" sortable="true" property="ticketCount" class="text-align-center" />
    <display:column title="Ticket" sortable="true" property="ticketQuantitySum" class="text-align-center" />        

    <display:column title="Listing" sortable="true" property="nonDuplicateTicketCount" class="text-align-center" />
    <display:column title="Ticket" sortable="true" property="nonDuplicateTicketQuantitySum" class="text-align-center" />

	<c:set var="eventTicketStat" value="${event.nonDuplicateEventTicketStat}" />
    <display:column title="Listing" sortable="true" value="${eventTicketStat.ticketListingCount}" class="text-align-center" />
    <display:column title="Ticket" sortable="true" value="${eventTicketStat.ticketCount}" class="text-align-center" />

    --%>    
</display:table>

<br/>
 	<c:forEach var="event" items="${events}">
		<div id="displaycats${event.id}" title="CATS" style="display:none">
			<c:forEach var="categories" items="${event.category}">
				${categories.symbol} - ${categories.description}<br/>
			</c:forEach>
		</div>
	</c:forEach>	
  
  <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
    <c:if test="${not empty events}">
      <img src="../images/ico-add.gif" align="absbottom" /> <a href="#" onclick="return downloadTickets()">Add to ZSP inventory</a>
    </c:if>
  <%-- </authz:authorize> 
  
  <authz:authorize ifAnyGranted="PRIV_DATA">--%>
    <br/>
    <c:if test="${not empty events}">
      Assign selected events to: <img src="../images/ico-user.gif" align="absbottom" />
      <select id="marketMaker" name="marketMaker">
        <c:forEach var="curMarketMaker" items="${marketMakers}">
          <option value="${curMarketMaker.username}">${curMarketMaker.username}</option>
        </c:forEach>
        <option value="DESK" selected>DESK</option>
      </select>
      <input type="button" class="medButton" onclick="assignEventsToMarketMaker()" value="Assign"/>
    </c:if>
 <%--  </authz:authorize>   --%>    
</form>

<%
	java.util.Date endDate = new java.util.Date();
	out.println("<!-- Took " + ((endDate.getTime() - startDate.getTime()) /  1000) + "s -->");
%>

<script type="text/javascript">

	<c:forEach var="event" items="${events}">
	$("#displaycats${event.id}").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 300,
		height:200,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	</c:forEach>
  <c:if test="${not empty tourBookmark.objectId}">
    $('#star_tour_' + ${tourBookmark.objectId}).attr('class', 'starOn');
  </c:if>

  <c:forEach var="bookmark" items="${eventBookmarks}">
    $('#star_' + ${bookmark.objectId}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkTour(tourId) {
    	BookmarkDwr.toggleBookmarkTour(tourId, 
		   function(response) {
			 $('#star_tour_' + tourId).attr('class', 'star' + response);
		   }
		);
  }

  function toggleBookmarkEvent(eventId) {
    	BookmarkDwr.toggleBookmarkEvent(eventId, 
		   function(response) {
			 $('#star_' + eventId).attr('class', 'star' + response);  							       
			 if (response == "On") {
			 	$('#star_' + eventId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + eventId).parent().parent().css('background-color', null);
			 }				   							      
		   });
  };
  
  function downloadTickets() {
    if ($('.event_checkbox:checked').length == 0) {
      alert('Please select at least an event');
      return false;
    }
    
    $('#event_form').submit()
  }
  
  function selectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).attr('checked', true);
    });
  }
  
  function unselectAll() {
    $.each($('.event_checkbox'), function(index, item) {
      $(item).removeAttr('checked');
    });
  }

  function invertAll() {
    $.each($('.event_checkbox'), function(index, item) {
      if ($(item).is(':checked')) {
        $(item).removeAttr('checked');
      } else {
        $(item).attr('checked', true);
      }
    });
  }
  
  function assignEventsToMarketMaker() {
    if ($('.event_checkbox:checked').length == 0) {
      alert('Please select at least an event');
      return false;
    }
    
    var eventIdsStr = "";
    $.each($('.event_checkbox:checked'), function(index, elt) {
    	eventIdsStr += elt.id.replace(/event_/, '') + ",";
    });
    location.href='MarketMakerEventTrackingUpdate?marketMaker=' + $('#marketMaker').val() + "&eventId=" + eventIdsStr;
  }
  
</script>