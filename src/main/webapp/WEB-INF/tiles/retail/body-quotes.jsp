<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript" src="../js/jquery/jquery.formatCurrency-1.3.0.min.js"></script>

<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 
	&gt; Quotes
</div>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<c:choose>
  <c:when test="${empty customers}">
    <i>No quotes defined</i>
  </c:when>
  <c:otherwise>
  <form enctype="multipart/form-data" action="Quotes" id="quotesForm" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <img src="../images/ico-user.gif" align="bottom"/><b>User:</b>
	<select name="creator" onchange="$('#quotesForm').submit()">
	  <c:forEach var="iCreator" items="${creators}">
	    <option value="${iCreator}" <c:if test="${creator == iCreator}">selected</c:if>>${iCreator}</option>
	  </c:forEach>
	</select>
    
	<b>Customer:</b>
	<select name="customerId" onchange="$('#quotesForm').submit()">
	  <c:forEach var="iCustomer" items="${customers}">
	    <option value="${iCustomer.id}" <c:if test="${iCustomer.id == customer.id}">selected</c:if>>${iCustomer.name}</option>
	  </c:forEach>
	</select>
	
	<b>Date:</b>
	<select name="date" onchange="$('#quotesForm').submit()">
	  <c:forEach var="date" items="${quoteDates}">
	    <fmt:formatDate var="formattedDate" pattern="MM/dd/yyyy" value="${date}"/>
	    <option value="${formattedDate}" <c:if test="${date == quote.quoteDate}">selected</c:if>>${formattedDate}</option>
	  </c:forEach>
	</select>
	
	<b>Sent on:</b>
	<select name="quoteId" onchange="$('#quotesForm').submit()">
	  <c:forEach var="curQuote" items="${quotes}">
	    <fmt:formatDate var="formattedDate" pattern="MM/dd/yyyy HH:mm" value="${curQuote.sentDate}"/>
	    <c:choose>
	      <c:when test="${not empty curQuote.sentDate}">
	       <option value="${curQuote.id}" <c:if test="${quote.id == curQuote.id}">selected</c:if>>${formattedDate}</option>
	      </c:when>
	      <c:otherwise>
	       <option value="${curQuote.id}" <c:if test="${quote.id == curQuote.id}">selected</c:if>>OPEN</option>
	      </c:otherwise>
	    </c:choose>
	  </c:forEach>
	</select>
	<br/>

	<h2>Quotes
	<c:if test="${quote.sentDate != null}">
			  (Sent)
	</c:if></h2>
	
	<table>
	  <tr>
	    <td><b>From: </b></td>
	    <td>${from}</td>
	  </tr>
	  <tr>
	    <td><b>To: </b></td>
	    <td>
		<c:choose>
			<c:when test="${quote.sentDate == null}">
				<input type="text" name="to" size="80" value="${customer.email}">
			</c:when>
			<c:otherwise>
			<input type="text" name="to" size="80" value="${quote.sentTo}">				
			</c:otherwise>
		</c:choose>
		</td>
	  </tr>
	  <tr>
	    <td><b>BCC: </b></td>
	    <td>
		 <c:choose>		
			<c:when test="${quote.sentDate == null}">
			  <input type="text" name="bcc" size="80" value="${user.email}">
			</c:when>
			<c:otherwise>
				<input type="text" name="bcc" size="80" value="${user.email}">
			</c:otherwise>
		  </c:choose>
		</td>
	  </tr>
	  <c:if test="${quote.sentDate != null}">
	  <tr>
	    <td><b>Subject: </b></td>
	    <td>
		<input type="hidden" name="subject" size="80" value="${subject}">${subject}</td>
	  </tr>
	  </c:if>
	  <tr class="editableContent" <c:if test="${not empty quote.sentDate}">style="display:none"</c:if>>
	    <td><b>Subject: </b></td>
	    <td>
		<input type="text" name="subject" size="80" value="${subject}"></td>
	  </tr>
	  <tr >
	    		
	    <td  colspan="2">
	    Exact Seat Locations		
		<input type="checkbox" name="sendingTicketData" onclick="showTicketData(this);" checked>
		Zone Locations <input type="checkbox" name="sendingZoneData" onclick="showZoneData(this);" checked></td>
	  </tr>
	</table>
	<br/>
	<c:choose>
		<c:when test="${quote.sentDate == null}">
			<a href="#" onclick="$('.editableContent').show(); $(this).hide(); return false;">Show Editable Content</a>
	    </c:when>
		<c:otherwise>
	    
	    </c:otherwise>
	 </c:choose>	
    <hr/>
	
		<input name="action" type="submit" class="medButton" value="Send"/><br /><br />
	
	<c:choose>
			<c:when test="${quote.sentDate != null}">
				<div>
				  <b style="color:#777777">Header:</b><br/>
				  <input type="hidden" name="header" size="80" value="${quotesHeader}">${quotesHeader}				 
				  <br/><br/>
				</div>
		    </c:when>
			<c:otherwise>
			<div class="editableContent" <c:if test="${quote.sentDate == null}">style="display:none"</c:if>>
			  <b style="color:#777777">Header:</b><br/>
			  <textarea name="header" cols="80" rows="5">${quotesHeader}</textarea>
			  <br/>
			</div>
			 </c:otherwise>
	 </c:choose>
	 <b>Reference:</b>${quote.referral} <br/> <br/>
	<c:forEach var="quotesEvent" items="${quotesEvents}">
	  <b>Event:</b> ${quotesEvent.event.name}<br/>
	  <b>Date: </b> <fmt:formatDate pattern="MM/dd/yyyy" value="${quotesEvent.event.localDate}"/>
	  <c:choose>
	    <c:when test="${not empty quotesEvent.event.localTime}">
	      <fmt:formatDate pattern="hh:mm aa" value="${quotesEvent.event.localTime}"/>
	    </c:when>
	    <c:otherwise>
	      TBD
	    </c:otherwise>
	  </c:choose>
	  <br/>
	  <b>Venue:</b> ${quotesEvent.event.venue.building}, ${quotesEvent.event.venue.city}, ${quotesEvent.event.venue.state}<br/>
	  <b>Map:</b> ${quotesEvent.event.venue.id}/${quotesEvent.event.catGroupName}
	  <br/><br/>
	  <c:set var="isZone" value="false"/>
	   <c:set var="isTicket" value="false"/>
	   <c:forEach var="quote1" items="${quotesEvent.quotes}">
	   <c:if test="${!isZone}">
	   <c:if test="${quote1.zone !=null && quote1.zone != ''}">
	   <c:set var="isZone" value="true"/>
	   </c:if>	   
	   </c:if>
	    <c:if test="${!isTicket}">
	   <c:if test="${quote1.zone ==null || quote1.zone == ''}">
	   <c:set var="isTicket" value="true"/>
	   </c:if>	   
	   </c:if>
	  </c:forEach>
	  <br/>
	   <!--start-->
	  
	  <c:if test="${isZone}">	
	   <span class="zoneData">	
	  
	  <b>Percent Markup: </b> <input id="zone_percent_increment_${quotesEvent.event.id}"   type="text" size="4" value='0'/> <input type="button" class="medButton" value="Update" onclick="updateZoneQuotes(${quotesEvent.event.id}, $('#zone_percent_increment_${quotesEvent.event.id}').val())"/>
	  
	  <u><b>Zone Locations</b></u>
	 	</span>
	  <table border="1px" cellspacing="0px" class="zoneData" width="40%">
	    <thead>
	      <th>Quote Qty</th>		 
	      <th>Zone</th>	 
		  <th>Price per ticket</th>	 		
	      <th>Total Price</th>
	      <th>Marked Price</th>
		  <c:if test="${quote.sentDate == null}">
			  <th>Update</th>
			  <th>Delete</th>
		  </c:if>
	    </thead>
		 <c:forEach var="quote" items="${quotesEvent.quotes}">
		 
	    <tbody>
	     <c:if test="${quote.zone !=null && quote.zone != ''}">
	        <tr align="center">
	        	<td style="padding:5px;">
					<input type="hidden" id="remqty_${quote.id}" value="${quote.ticket.remainingQuantity}">
	           		<input type="text" size ="4"  id="zoneQuote_${quote.id}" name="zoneQuote_${quote.id}" value="${quote.zoneQty}"/>
	           	</td>	
	          <td style="padding:5px;"> ${quote.zone}</td>
			 <td style="padding:5px;" >
			  <fmt:formatNumber type="currency" value="${quote.zonePrice/quote.zoneQty}"/></td>
	          
	          <td style="padding:5px;" >
			  <input type="hidden" value="${quote.zonePrice}" id="zoneBasePrice_${quote.id}"/>
			  <fmt:formatNumber type="currency" value="${quote.zonePrice}"/></td>
	          <td style="padding:5px;">
			  
					$<input type="text" size="4" name="zoneMarkedPrice_${quote.id}" id="zoneMarkedPrice_${quote.id}" onKeyUp="updateZonePercentIncrement(${quotesEvent.event.id}, ${quote.id}); recalculateZoneTotal(${quotesEvent.event.id});" value="<fmt:formatNumber type='number' minFractionDigits='2' maxFractionDigits='2' value='${quote.zonePrice}'/>" class="currency" />
(+<span id="zone_quote_percent_inc_${quote.id}"><fmt:formatNumber type="number" minFractionDigits="" maxFractionDigits="1" value="0" /></span>%)
						  
</td>
		<c:if test="${quote.quote.sentDate == null}">
			<td style="padding:5px;">
			<a href="#" onclick ="updateTicketQuote(${quote.id},1)">Update</a>
			</td>
			<td style="padding:5px;">
			<a href="#" onclick ="deleteTicketQuote(${quote.id})">Delete</a>
			</td>
		</c:if>	
	        </tr>
	      </c:if>
	    </tbody>	
     </c:forEach>  	
    <br/>	 
	  </table>
	  </c:if>
	 
	  <!--end-->
	  <br/>
	  
	  <c:if test="${isTicket}">
	  <span class="ticketData">
	  	  
	  <b>Percent Markup: </b> <input id="percent_increment_${quotesEvent.event.id}"  type="text" size="4"  value='0'/> <input type="button" class="medButton" value="Update" onclick="updateQuotes(${quotesEvent.event.id}, $('#percent_increment_${quotesEvent.event.id}').val())"/>
	 	
	   <u><b>Exact Seat Locations</b></u>
	   </span>
	  <table border="1px" cellspacing="0px" class="ticketData" width="40%">
	    <thead>
	      <th>Quote Qty</th>
		  <!--<c:if test="${quote.sentDate == null}">
		  	<th>Remaining Qty</th>
		  </c:if> -->
	      <th style="padding:5px">Section</th>
	      <th>Row</th>
	      <th>Per Ticket Price</th>
	      <th>Marked Price</th>
		  <c:if test="${quote.sentDate == null}">
		  <th>Update</th>
		  </c:if>
		  <c:if test="${quote.sentDate == null}">
		  <th>Delete</th>
		  </c:if>
	    </thead>
	    <tbody>
	      <c:forEach var="quote" items="${quotesEvent.quotes}">
		     <c:if test="${quote.zone ==null || quote.zone == ''}">
	        	<tr  align="center">
	          		<td style="padding:5px;">
	          			<input type="hidden" id="remqty_${quote.id}" value="${quote.ticket.remainingQuantity}">
						<input type="text" size ="4" id="zoneQuote_${quote.id}" value="${quote.qty}">
					</td>
			<c:if test="${quote.quote.sentDate == null}">
			  <td style="padding:5px;">${quote.ticket.remainingQuantity}</td>
			  </c:if>
	          <td style="padding:5px;">${quote.ticket.section}</td>
	          <td style="padding:5px;">${quote.ticket.row}</td>
	          <td style="padding:5px;" >
			  <input type="hidden" id="basePrice_${quote.id}" value="${quote.ticket.buyItNowPrice}"/>
			  	  <fmt:formatNumber type="currency" value="${quote.ticket.buyItNowPrice}"/></td>
	          <td>
			  	$<input type="text" size="6" name="markedPrice_${quote.id}" id="markedPrice_${quote.id}" onKeyUp="updatePercentIncrement(${quotesEvent.event.id}, ${quote.id}); " value="<fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${quote.markedPrice}"/>" class="currency" />
(+<span id="quote_percent_inc_${quote.id}"><fmt:formatNumber type="number" minFractionDigits="" maxFractionDigits="1" value="0"/></span>%)
				
			  
</td>
		<c:if test="${quote.quote.sentDate == null}">
			<td style="padding:5px;">
			<a href="#" onclick ="updateTicketQuote(${quote.id},0)">Update</a>
			</td>
		</c:if>	
		<c:if test="${quote.quote.sentDate == null}">
			<td style="padding:5px;">
			<a href="#" onclick ="deleteTicketQuote(${quote.id})">Delete</a>
			</td>
		</c:if>	
	        </tr>
			</c:if>	
	      </c:forEach>  
	    </tbody>	      
	  </table>
	  
	  </c:if>
	  
	          
			<c:choose>
			<c:when test="${quotesEvent.savedFile != null}">
			<br /><table><tr><td><a href="javascript:loadImageFile('${quotesEvent.savedFile}')">
					<img src="../images/000_THUMB_AO.gif" width="50" height="50" style="float:left; margin: 0px 10px 10px 10px;border:1px solid black; padding:10px;" />
				</a></td>			
			<td>${quotesEvent.savedFile}</td></tr></table><br />
			<input type="hidden" name="map_${quotesEvent.event.id}" value="${quotesEvent.savedFile}"/>			
			</c:when>
			<c:otherwise>
			<br /><table><tr><td><a href="javascript:loadImage(${quotesEvent.event.id}, '${quotesEvent.event.catGroupName}')">
					<img src="../images/000_THUMB_AO.gif" width="50" height="50" style="float:left; margin: 0px 10px 10px 10px;border:1px solid black; padding:10px;" />
				</a></td>			
		<td>${quotesEvent.map},</td></tr></table><br />
		<input type="hidden" name="map_${quotesEvent.event.id}" value="${quotesEvent.map}"/>			
			
			</c:otherwise>
			</c:choose>
			
	     <img src="../images/ico-upload.gif" align="absmiddle" /> Upload Map <i>(optional)</i>: <input type="file" name="map_${quotesEvent.event.id}"/>
		 
	    
  	  <br/><br/>
	</c:forEach>
	<br /><br />
	<!-- 
	<c:choose>
			<c:when test="${quote.sentDate != null}">
				<div>
				  <b style="color:#777777">Footer:</b><br/>
				  ${quotesFooter}
				  <br/><br/>
				</div>
		    </c:when>
			<c:otherwise>
			<div class="editableContent" <c:if test="${not empty quote.sentDate}">style="display:none"</c:if>>
			  <input type="checkbox" id="includeFooter" onchange="swapFooter($(this).val())" checked/>
			  <b style="color:#777777">Footer:</b><br/>
			  <textarea id="footer" name="footer" cols="80" rows="5">${quotesFooter}</textarea><br/>
			</div>
			 </c:otherwise>
	 </c:choose>
     -->

  Please note: These seats are not hold and are subject to availability.<br/><br/>
  ${user.firstName} ${user.lastName}<br/> 
  Right This Way, LLC<br/>
  1441 Broadway<br/>
  Suite 2402<br/>
  New York, NY 10018<br/>
  212-383-2800 - Main<br/>
  212-382-2140 - Fax<br/>
  <a href="http://www.rightthisway.com/">www.rightthisway.com</a><br/>
  <a href="mailto:${user.email}">${user.email}</a><br/>
  <img src="../images/rightthisway_logo.gif"/><br/>
  <br/><br/>
  
  <input name="action" type="submit" class="medButton" value="Send"/><br />
  
	<h2>Admin</h2>
	<img src="../images/ico-delete.gif" align="absbottom"/><a href="#" onclick="deleteQuotes(); return false">Delete Quotes for this customer for ${formattedDate}</a><br/>
	<img src="../images/ico-delete.gif" align="absbottom"/><a href="#" onclick="deleteCustomer(); return false">Delete Customer (and associated quotes)</a><br/>
   </form>	  
  </c:otherwise>
</c:choose>

<script type="text/javascript">
  function deleteQuotes() {
    if (confirm('Are you sure you want to delete quotes for this customer for this date?')) {
       location.href = 'Quotes?action=deleteQuotes&customerId=${customer.id}&date=${formattedDate}';
    }
  }

  function deleteCustomer() {
    if (confirm('Are you sure you want to delete this customer?')) {
       location.href = 'Quotes?action=deleteCustomer&customerId=${customer.id}&date=${formattedDate}';
    }
  }
  
  function updateEmail() {
       location.href = 'Quotes?action=updateEmail&email=' + $('#email').val() + '&customerId=${customer.id}&date=${formattedDate}';
  }
  
  function swapFooter(value) {
    if (!value) {
      $('#footer').attr('disabled', 'disabled');
    } else {
      $('#footer').removeAttr('disabled');
    }
  }
  
    var ticketQuotes = new Array();
    <c:forEach var="quotesEvent" items="${quotesEvents}">
      ticketQuotes[${quotesEvent.event.id}] = new Array();
      <c:forEach var="quote" items="${quotesEvent.quotes}">
        ticketQuotes[${quotesEvent.event.id}][${quote.id}] = [${quote.qty}, ${quote.markedPrice},${quote.zoneQty},${quote.zoneMarkedPrice}];
      </c:forEach>
    </c:forEach>
    
  function updateQuotes(eventId, increment) {
    if ($.trim(increment) == '') {
      alert("Please type a number");
      return;
    }
	
  
    for (var key in ticketQuotes[eventId]) {
      var  amount=$('#basePrice_' + key).val();
      $('#markedPrice_' + key).val(Math.ceil(amount * (100 + parseFloat(increment)) / 100));
      updatePercentIncrement(eventId, key,increment);
    }
    recalculateTotal(eventId);
  }
  function updateZoneQuotes(eventId, increment) {
    if ($.trim(increment) == '') {
      alert("Please type a number");
      return;
    }
	for (var key in ticketQuotes[eventId]) {
      //var amount = ticketQuotes[eventId][key][3];
	  var amount=$('#zoneBasePrice_' + key).val();
      $('#zoneMarkedPrice_' + key).val(Math.ceil(amount * (100 + parseFloat(increment)) / 100));
      updateZonePercentIncrement(eventId, key,increment);
    }
    recalculateZoneTotal(eventId);
  }
	
  function updatePercentIncrement(eventId, quoteId,increment) {  
  
    //var basePrice = ticketQuotes[eventId][quoteId][1];
	
	var basePrice = parseFloat($('#basePrice_' + quoteId).val()) ;
    var newVal = parseFloat($('#markedPrice_' + quoteId).val()) ;
    if (newVal == undefined) {
      newVal = 0;
    }
    if(increment == undefined){
		var percent = parseInt(10000 * ((newVal - basePrice) / basePrice)) / 100
		$('#quote_percent_inc_' + quoteId).text(percent);
	}else{
		$('#quote_percent_inc_' + quoteId).text(increment);
	}
    
  }
function updateZonePercentIncrement(eventId, quoteId,increment) {
   
    //var basePrice = ticketQuotes[eventId][quoteId][1];
	var basePrice = parseFloat($('#zoneBasePrice_' + quoteId).val()) ;
    var newVal = parseFloat($('#zoneMarkedPrice_' + quoteId).val());
    if (newVal == undefined) {
      newVal = 0;
    }
    //var percent = parseInt(10000 * ((newVal - basePrice) / basePrice)) / 100
	if(increment == undefined){
		var percent = parseInt(10000 * ((newVal - basePrice) / basePrice)) / 100
		$('#zone_quote_percent_inc_' + quoteId).text(percent);
	}else{
		$('#zone_quote_percent_inc_' + quoteId).text(increment);
	}
  }

  function recalculateTotal(eventId) {
    var total = 0;
    for (var quoteId in ticketQuotes[eventId]) {
      var basePrice = parseFloat($('#markedPrice_' + quoteId).val());
      var amount = ticketQuotes[eventId][quoteId][0] * basePrice;
      total += amount; 
    }
    $('#total_' + eventId).text(total);
    $('#total_' + eventId).text(Math.ceil(parseFloat($('#total_' + eventId).text())));
    $('#total_' + eventId).formatCurrency();
  }
   function recalculateZoneTotal(eventId) {
   
    var total = 0;
    for (var quoteId in ticketQuotes[eventId]) {
      var basePrice = parseFloat($('#zoneMarkedPrice_' + quoteId).val());
      var amount = ticketQuotes[eventId][quoteId][0] * basePrice;
      total += amount; 
    }
    $('#total_' + eventId).text(total);
    $('#total_' + eventId).text(Math.ceil(parseFloat($('#total_' + eventId).text())));
    $('#total_' + eventId).formatCurrency();
  }


    <c:forEach var="quotesEvent" items="${quotesEvents}">
      recalculateTotal(${quotesEvent.event.id});
    </c:forEach>
function loadImage(eventId, catScheme) {
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};
function loadImageFile(fileName) {
	var eventId = 0;
	var catScheme = '';
	//alert(fileName);
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme + '&fileName=' + fileName,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};

function deleteTicketQuote(ticketOuoteId){

	var url = "DeleteTicketQuote?ticketOuoteId="+ticketOuoteId;	
	var r= confirm("Are you want to delete?");		
		if(r == true){	
			if(ticketOuoteId!=''){
				$.ajax({
					url:url,
					success: function(res){
						 alert(res);
						 location.reload();
						}
					
					
				}); 
			}
		}	 
}


function updateTicketQuote(ticketQuoteId,zone){
	var ticketQty=0;
	ticketQty = $("#zoneQuote_" + ticketQuoteId).val();
	var remQty =  $("#remqty_" + ticketQuoteId).val();
	if(ticketQty>remQty){
		alert('Can not increase Quote Qty.');
		return false;
	}
	var url = "UpdateTicketQuote?ticketQuoteId=" + ticketQuoteId + "&ticketQty=" + ticketQty + "&zone=" + zone;	
	var r= confirm("Are you want to update quantity?");		
		if(r == true){	
			if(ticketQuoteId!='' && !isNaN(ticketQty)){
				$.ajax({
					url:url,
					success: function(res){
						 alert(res);
						 location.reload();
						}
				}); 
			}else{
				alert('Quantity must be a number. ');
			}
		}	 
}

function checkQty(id){
	var remqty = $('#remqty_'+id).val();
	var qty = $('#qty_'+id).val();
	
	if(qty > remqty){
	alert("you can't add more than "+remqty);
	$('#qty_'+id).val(remqty);	
	}	
}
function showTicketData(checkBox){   
	if(checkBox.checked){
		$('.ticketData').show()
	}else{
		$('.ticketData').hide()
	}
}
function showZoneData(checkBox){    
	if(checkBox.checked){
		$('.zoneData').show()
	}else{
		$('.zoneData').hide()
	}
}
</script>