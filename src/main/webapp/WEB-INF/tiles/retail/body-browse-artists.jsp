<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathUser">
  Browse
</div>

<div id="quickLinks">		 	
 <%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
	<a href="EditorManageArtists">Edit Artists/Teams</a>
  <%-- </authz:authorize> --%>
</div>
<div style="clear:both"></div>

<h1>Browse Artists/Teams</h1>
<form method="GET" action="BrowseArtists">
	<b>Filter Artists/Teams:</b> <input id="filter" name="filter" type="text" value="${param.filter}" />
	<input class="medButton" type="submit" value="Filter" onclick="return ValidateFilter()"/>
	<c:if test="${not empty param.filter}"> <a href="BrowseArtists">Clear Filter</a></c:if>	
</form>

<display:table class="list" name="${artists}" id="artist"  pagesize="20" requestURI="BrowseArtists" defaultsort="3" defaultorder="descending">
    <display:column style="width:20px"><div id="star_${artist.id}" class="starOff" onclick="toggleBookmarkArtist(${artist.id})"></div></display:column>
    <display:column title="Artist/Team Name" sortable="true" property="name" href="BrowseTours" paramId="artistId" paramProperty="id" />
    <display:column title="Tour Count" sortable="true" property="tourCount" style="width: 80px" class="text-align-right"/>
</display:table>

<script type="text/javascript">
  <c:forEach var="bookmark" items="${artistBookmarks}">
    $('#star_' + ${bookmark.artist.id}).attr('class', 'starOn');
    $('#star_${bookmark.objectId}').attr('class', 'starOn');
    $('#star_${bookmark.objectId}').parent().parent().css('background-color', '#eeee44');
  </c:forEach>

  function toggleBookmarkArtist(artistId) {
    	BookmarkDwr.toggleBookmarkArtist(artistId, 
		   function(response) {
			 $('#star_' + artistId).attr('class', 'star' + response);
			 if (response == "On") {
			 	$('#star_' + artistId).parent().parent().css('background-color', '#eeee44');
			 } else {
			 	$('#star_' + artistId).parent().parent().css('background-color', null);
			 }				   							      
			   							       
		   });
  }

// FIXME: please indent the following. Thanks.  
  function ValidateFilter()
  {
  
  if(document.getElementById('filter').value=="")
  {
  
  alert("Please enter the name of Artist/Teams in order to filter. ");
  return false;
  }
  }
  
</script>