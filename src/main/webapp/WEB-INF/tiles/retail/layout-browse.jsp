<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="userSubMenu">
	<div style="float:left">
		<c:choose>
			<c:when test="${selectedSubMenu == 'Browse Tours/Seasons'}">
				<b>Browse Tours/Seasons</b>
			</c:when>
			<c:otherwise>
				<a href="BrowseTours">Browse Tours/Seasons</a>
			</c:otherwise>
		</c:choose>
		|
		<c:choose>
			<c:when test="${selectedSubMenu == 'Quotes'}">
				<b>Quotes</b>
			</c:when>
			<c:otherwise>
				<a href="Quotes">Quotes</a>
			</c:otherwise>
		</c:choose>
		|
		<c:choose>
			<c:when test="${selectedSubMenu == 'ViewQuotes'}">
				<b>View Quotes</b>
			</c:when>
			<c:otherwise>
				<a href="ViewQuotes">View Quotes</a>
			</c:otherwise>
		</c:choose>	
	</div>
	
	<div style="float:right">
		<img src="../images/ico-admin.gif" align="absbottom"><a href="javascript:void(0)" onclick="resetUserPreferences()">Auto fix</a>
	</div>
	
	<div style="clear:both"></div>
</div>

<script type="text/javascript">
function resetUserPreferences() {
	var result = confirm("This button will reset all of your filter preferences. Do not proceed if you wish to save your filter settings for this current session.");
	if (!result) {
		return;
	}
	DataDwr.deleteUserPreferences( 
		function(response) {
			if (response == 'OK') {
				alert("Your preferences have been reseted");
				window.location.href = window.location.href;
			} else {
				alert(response);
			}
		}
	);  
};
</script>

<tiles:insertAttribute name="subBody" />
