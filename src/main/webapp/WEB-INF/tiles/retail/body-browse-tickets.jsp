<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<script type="text/javascript" src="../js/highChart/highcharts.js"></script>
<script type="text/javascript" src="../js/highChart/exporting.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />

<fmt:formatDate pattern="MM/dd/yyyy" value="<%=new java.util.Date()%>" var="today" />
<script type="text/javascript">

var createChartTemplateForPrice = function() {
    return {
        chart: new Highcharts.Chart({

            chart: {
                renderTo: 'containerForPrice',
				zoomType: 'x',
                spacingRight: 20
            },
			title: {

                text: 'Price Chart',
				style: {
					color: '#000',
					font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
				}

            },
			subtitle: {

                text: document.ontouchstart === undefined ?

                    'Click and drag in the plot area to zoom in' :

                    'Drag your finger over the plot to zoom in'

            },
			xAxis: {

                type: 'datetime',

                maxZoom: 14 * 24 * 3600000, // fourteen days

                title: {

                    text: 'Historical Range ',
					style: {
					color: '#000',
					font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
					}

                }

            },
			yAxis: {

                title: {

                    text: 'Ticket Price',
					style: {
					color: '#000',
					font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
					}

                },

                min: 0.0,

                startOnTick: false,

                showFirstLabel: true

            },

            tooltip: {

                shared: false

            },

            legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100,
			borderWidth: 0
			},

            plotOptions: {

                line: {

                                       
					lineWidth: 2,
					
                    marker: {

                        enabled: false,

                        states: {

                            hover: {

                                enabled: true,

                                radius: 1

                            }

                        }

                    },

                    shadow: false,

                    states: {

                        hover: {

                            lineWidth: 1

                        }

                    }

                }

            },
			exporting :{
				enabled:false
			},
			credits: {
				enabled: false
			},
            series: []
        }),
        addSeries: function(name) {
            var seriesIndex = this.seriesExists(name);
			if ((seriesIndex === false)) {			
				this.chart.addSeries({
					name: name,
					data: [],
					id: Math.floor(Math.random()*1000),
					type:'line',
					pointInterval: 24 * 3600 * 1000,
					pointStart: Date.UTC(${chartStartDate})				
					
				});
			}
			
        },
		removeSeries:function(){
			while(this.chart.series.length > 0)
				this.chart.series[0].remove(false);
				this.chart.redraw();
			
		},
        addPoint: function(data, series) {
            var seriesIndex = this.seriesExists(series);	
			var existsData=this.chart.series[seriesIndex].data;
			if(existsData.length == 0){				
				if (!(seriesIndex === false)) {							
					this.chart.series[seriesIndex].setData(data);
										
				}
			}
			
            
        },
		toggleSeries:function(series){
			$.each(this.chart.series, function(index, item) {				
                if ($.trim(item.name) == $.trim(series)) {					
                    if(item.visible){
						item.hide();
					}else{						
						item.show();					
					}	
                   
                }
            });
			
		},
        seriesExists: function(series) {
            var seriesIndex = false;
            $.each(this.chart.series, function(index, item) {
                if ($.trim(item.name) == $.trim(series)) {
					
                    seriesIndex = index;
					
                    return false;
                }
            });
            return seriesIndex;
        }
    }
}
var createChartTemplateForQuantity = function() {
    return {
        chart: new Highcharts.Chart({

            chart: {
                renderTo: 'containerForQuantity',
				zoomType: 'x',
                spacingRight: 20
            },
			title: {

                text: 'Quantity Chart',
				style: {
					color: '#000',
					font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
				}

            },
			subtitle: {

                text: document.ontouchstart === undefined ?

                    'Click and drag in the plot area to zoom in' :

                    'Drag your finger over the plot to zoom in'

            },
			xAxis: {

                type: 'datetime',

                maxZoom: 14 * 24 * 3600000, // fourteen days

                title: {

                    text: 'Historical Range ',
					style: {
					color: '#000',
					font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
					}

                }

            },
			yAxis: {

                title: {

                    text: 'Ticket Quantity',
					style: {
					color: '#000',
					font: 'bold 14px "Trebuchet MS", Verdana, sans-serif'
					}

                },

                min: 0.0,

                startOnTick: false,

                showFirstLabel: true

            },

            tooltip: {

                shared: false

            },

            legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -10,
			y: 100,
			borderWidth: 0
			},

            plotOptions: {

                line: {                    
                    
					lineWidth: 2,
					
                    marker: {

                        enabled: false,

                        states: {

                            hover: {

                                enabled: true,

                                radius: 1

                            }

                        }

                    },

                    shadow: false,

                    states: {

                        hover: {

                            lineWidth: 2

                        }

                    }

                }

            },
			exporting :{
				enabled:false
			},
			credits: {
				enabled: false
			},
            series: []
        }),
        addSeries: function(name) {
            this.chart.addSeries({
                name: name,
                data: [],
                id: Math.floor(Math.random()*1000),
				type:'line',
				pointInterval: 24 * 3600 * 1000,
				pointStart: Date.UTC(${chartStartDate})				
				
            });
			
        },
		removeSeries:function(){
			while(this.chart.series.length > 0)
				this.chart.series[0].remove(false);
				this.chart.redraw();
		},
        addPoint: function(data, series) {
            var seriesIndex = this.seriesExists(series);	
			var existsData=this.chart.series[seriesIndex].data;
			if(existsData.length == 0){				
				if (!(seriesIndex === false)) {					
					this.chart.series[seriesIndex].setData(data);
					
					}
				}
			           
        },
		toggleSeries:function(series){
			$.each(this.chart.series, function(index, item) {				
                if ($.trim(item.name) == $.trim(series)) {					
                    if(item.visible){
						item.hide();
					}else{						
						item.show();					
					}	
                   
                }
            });
			
		},
        seriesExists: function(series) {
            var seriesIndex = false;
            $.each(this.chart.series, function(index, item) {
                if ($.trim(item.name) == $.trim(series)) {
					
                    seriesIndex = index;
					
                    return false;
                }
            });
            return seriesIndex;
        }
    }
}

function loadOnStartUp() {
	<c:if test="${tabName eq 'zone'}">
	   $("#tab-zone").click();			
     </c:if>
	<c:if test="${tabName eq 'liquidity'}">
		$("#tab-liquidity").click();			
	</c:if>
	<c:if test="${tabName eq 'history'}">
		$("#tab-history").click();			
	</c:if>
	<c:if test="${tabName eq 'stats'}">
		$("#tab-stats").click();	
	</c:if>
	$("#zoneFilterUpdateButton").click(function(){
		$('#zoneFilterCancelButton').hide();
		$('#zoneFilterUpdateButton').hide();
		$('#zoneFilterUpdateWait').show();
		$('#zoneCatSchemeHidden').val($('#zoneCatScheme').val());
		$('#zoneStartPriceHidden').val($('#zoneStartPrice').val());
		$('#zoneEndPriceHidden').val($('#zoneEndPrice').val());
		$("#selectedTab").val("zone");
		$("#ticketFilterUpdateButton").click();
	});
	
	$("#historyFilterUpdateButton").click(function(){
		$('#historyFilterCancelButton').hide();
		$('#historyFilterUpdateButton').hide();
		$('#historyFilterUpdateWait').show();
		
		$("#selectedTab").val("history");
		
		$("#ticketFilterUpdateButton").click();
	});
	
	$("#liquidityFilterUpdateButton").click(function(){
		$('#liquidityFilterCancelButton').hide();
		$('#liquidityFilterUpdateButton').hide();
		$('#liquidityFilterUpdateWait').show();
		
		$("#selectedTab").val("liquidity");
		
		$("#ticketFilterUpdateButton").click();
	});
	
	$("#statsFilterUpdateButton").click(function(){
		$('#statsFilterCancelButton').hide();
		$('#statsFilterUpdateButton').hide();
		$('#statsFilterUpdateWait').show();
		
		$("#selectedTab").val("stats");
		
		$("#ticketFilterUpdateButton").click();
	});
	
	
}

	
		
$(function() {
	
	var chartMap;
	var chartQtyMap;
	if(typeof chartMap == 'undefined'){
		
		chartMap=new Object();
		<c:forEach var="filter" items="${chartData}">				
		chartMap.${filter.key} = [${filter.value}];			
		</c:forEach>
	}
	if(typeof chartQtyMap == 'undefined'){
		
		chartQtyMap=new Object();
		<c:forEach var="filterQty" items="${chartQtyData}">				
		chartQtyMap.${filterQty.key} = [${filterQty.value}];			
		</c:forEach>
	}
	
	var chartForPrice;
	var chartForQuantity;
	
	<c:if test="${view != 'compact'}">
		chartForPrice = createChartTemplateForPrice();
		chartForQuantity = createChartTemplateForQuantity();
	</c:if>

	
	function renderPriceChart(data,seriesName){
		
		chartForPrice.addSeries(seriesName);			
		
		chartForPrice.addPoint(data, seriesName);
		
	}
	function renderQunatityChart(data,seriesName){
		
		chartForQuantity.addSeries(seriesName);				
		chartForQuantity.addPoint(data,seriesName);
		
	}
	//Chart Filter
  $("#chartFilterUpdateButton").click(function(){
		
		
		changeCatFiltersForChart();
		changeQuantityFiltersForChart();
		
		if($('#categoryInputForChart').val() != '' && $('#quantityInputForChart').val() == ''){
			chartForPrice.removeSeries();
			chartForQuantity.removeSeries();
			alert('please select at least one category and quantity to see chart.');
			
		}
		else{
			var arrayCategories=$('#categoryInputForChart').val().split(",");
			var arrayQuantity=$('#quantityInputForChart').val().split(",");			
			if(arrayCategories.length > 1 && arrayQuantity.length > 1){
					
				chartForPrice.removeSeries();
				chartForQuantity.removeSeries();
				
				for(var i=0; i<(arrayCategories.length - 1); i++){					
					for(var j=0; j<(arrayQuantity.length - 1); j++){
						var data=[];
						var qtyData=[];
						var seriesName=arrayCategories[i]+"_"+arrayQuantity[j];
						data = chartMap[seriesName];
						qtyData = chartQtyMap[seriesName];
						
						renderPriceChart(data,seriesName);
						renderQunatityChart(qtyData,seriesName);	
						
						
					}
					
				}		
				
			}
			
			if($('#avg-For-Chart').attr('checked') == true){
				
				if(arrayQuantity.length > 11){
					if(arrayCategories.length > 1 && arrayQuantity.length > 1){
						for(var i=0; i<(arrayCategories.length - 1); i++){
							var avgPrice=[];
							var avgQty=[];
							var numOfFilter=0;
							var n=0;
							for(var j=0; j<(arrayQuantity.length - 1); j++){
								numOfFilter+=1;
								var seriesName=arrayCategories[i]+"_"+arrayQuantity[j];
								var price = chartMap[seriesName];							
								var Qty = chartQtyMap[seriesName];
								
								n=price.length;	
													
								for(var k=0; k<price.length; k++){
									if(typeof avgPrice[k] == 'undefined'){									
										avgPrice[k]=price[k];
										avgQty[k]=Qty[k];									
									}else{
										
										avgPrice[k]+=price[k];
										avgQty[k]+=Qty[k];
									}
								}
								
							}
							for(var l=0; l<n; l++){
								if(avgPrice[l] > 0){
									avgPrice[l] = avgPrice[l] / numOfFilter;
									avgPrice[l] = avgPrice[l].toFixed(2);																		
								}else{
									avgPrice[l] = avgPrice[l].toFixed(2);
								}
								avgPrice[l]=avgPrice[l]*1;
								if(avgQty[l] > 0){
									avgQty[l] = Math.round(avgQty[l] / numOfFilter);
								}
							}
							
							var sName= "Aevrage_"+arrayCategories[i];
							renderPriceChart(avgPrice,sName);
							renderQunatityChart(avgQty,sName);
						}
							
					}				
					
				}else{
					alert("Select All Quantity to view Aevrage");
				}
			}	
			
			
			
		}
	});	
	
	
	
});


 
 
</script>
<div id="breadCrumbPath" class="breadCrumbPathUser">
	<a href="..">Home</a> 
	&gt; <a href="BrowseArtists">Browse</a> 

	<c:if test="${not empty event}">
		&gt; <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
		&gt; <a href="BrowseEvents?tourId=${tour.id}">${tour.name}</a>
		&gt; ${event.formattedEventDate}
	</c:if>
</div>

<div>
	<div id="eventLinks">
		<b>Event:</b>	

        <span id="LoadingEventSpan"><img src="../images/process-running.gif" align="absbottom" />Loading...</span>
        <span id="eventNavigationSpan" style="display:none">
	        <c:choose>
			  <c:when test="${not empty previousEvent}">
				<a href="#" onclick="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); location.href='${ticketsPage}?eventId=${previousEvent.id}' + anchor" tooltip="${previousEvent.name} ${previousEvent.formattedEventDate} ${previousEvent.formattedVenueDescription}" style="color: #850001">Previous</a>
			  </c:when>
			  <c:otherwise>
				<font color="#cccccc">Previous</font>
			  </c:otherwise>
	        </c:choose>		
			
			<select onchange="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); document.location.href='${ticketsPage}?eventId=' + this.value + anchor">
				<c:forEach var="e" items="${tour.events}">
					<option value="${e.id}" <c:if test="${e.id == event.id}">selected</c:if> >${e.name}&nbsp;${e.formattedEventDate}&nbsp;${e.formattedVenueDescription}				
					</option>
				</c:forEach>
			</select>
			
			<c:choose>
			  <c:when test="${not empty nextEvent}">
				<a href="#" onclick="$('#eventNavigationSpan').hide();$('#LoadingEventSpan').show(); location.href='${ticketsPage}?eventId=${nextEvent.id}' + anchor" tooltip="${nextEvent.name} ${nextEvent.formattedEventDate} ${nextEvent.formattedVenueDescription}" style="color: #850001">Next</a>
			  </c:when>
			  <c:otherwise>
				<font color="#cccccc">Next</font>
			  </c:otherwise>
			</c:choose>
		</span>	
	</div>
	
	<div id="quickLinks">
		<%-- <authz:authorize ifAnyGranted="PRIV_ANALYTIC"> --%>
			<c:choose>
				<c:when test="${view == 'compact'}">
					<a href="BrowseTicketsAnalytic?eventId=${event.id}&view=analytic">Browse Tickets Analytic</a>
				</c:when>
				<c:otherwise>
					<a href="BrowseTickets?eventId=${event.id}&view=compact">Browse Tickets Normal</a>
				</c:otherwise>
			</c:choose>
		<%-- </authz:authorize>
		<authz:authorize ifAnyGranted="PRIV_ALERT"> --%>
			| <a href="MyAlerts?eventId=${event.id}">Create Alert</a>
		<%-- </authz:authorize>
		<authz:authorize ifAnyGranted="PRIV_DATA"> --%>
			| <a href="EditorEditEvent?id=${event.id}&action=edit">Edit Event</a>
		<%-- </authz:authorize>
		<authz:authorize ifAnyGranted="PRIV_CATEGORY"> --%>
			| <a href="#" onclick="location.href='EditorEditEventCategories?eventId=${event.id}'">Categorization</a>
		<%-- </authz:authorize>
		<authz:authorize ifAnyGranted="PRIV_DATA"> --%>
			| <a href="#" onclick="location.href='EditorValuationFactors?tourId=${tour.id}&eventId=${event.id}'">Valuation Factors</a>
		<%-- </authz:authorize>
		<authz:authorize ifAnyGranted="PRIV_SHORTS"> --%>
			|
			<a href="TMATBrowseShorts?eventId=${event.id}">Shorts</a>
		<%-- </authz:authorize> --%>
	</div>
</div>
<div style="clear: both"></div>

<div id="wholePageDiv" style="display:none">
<h1>Browse Tickets</h1>

<div style="float: right">
	<input id="nocrawl" name ="nocrawl" type="hidden" value="${noCrawl}">
	<input id="reload" name ="reload" type="hidden" value="${reload}">
	<span class="refreshLink" style="display:none">
	
		<img src="../images/ico-reload.gif" align="absbottom"/>
		Page updated <span id="numMinsSinceLastUpdate"></span>
	</span>
	<span class="refreshedRecently" style="display:none">
		<img src="../images/ico-reload.gif" align="absbottom"/>
		<i>(updated less than <fmt:formatNumber pattern="0" value="${ticketListingCrawler.minCrawlPeriod / 60}"/>mn ago)</i>
	</span>
	<span class="refreshCrawlInfoSpan" style="display:none">
		<img src="../images/process-running.gif" align="absbottom"/>
		<span class="refreshCrawlInfo" style="color:green"></span>
	</span>
</div>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
	<div>
		<form id="marketMakerEventTrackingUpdateForm" action="MarketMakerEventTrackingUpdate">
			<input type="hidden" name="eventId" value="${event.id}"/>
			Managed by: <img src="../images/ico-user.gif" align="bottom"/>
			<select name="marketMaker" onchange="$('#marketMakerEventTrackingUpdateForm').submit()">
				<c:forEach var="curMarketMaker" items="${marketMakers}">
					<option value="${curMarketMaker.username}" <c:if test="${marketMaker.username == curMarketMaker.username}">selected</c:if>>${curMarketMaker.username}</option>
				</c:forEach>
				<option value="DESK" <c:if test="${empty marketMaker}">selected</c:if>>DESK</option>
			</select>
		</form>
	</div>
<%-- </authz:authorize> --%>

<a onclick="toggleEventDetails()" style="cursor: pointer">Event Details <img id="event-details-expand-img" src="../images/ico-expand.gif" /></a>
<div id="event-details-info">
	<table width="100%">
		<tr>
			<td width="100px" style="padding-right:10px">
				<a href="javascript:loadImage(${event.id}, '${catGroupName}')">
					<img src="../images/000_THUMB_AO.gif" width="50" height="50" style="float:left; margin: 0px 10px 10px 10px;border:1px solid black; padding:10px;" />
				</a>
				<c:if test="${event.eventStatus == 'EXPIRED'}"><font color="red"><b>EXPIRED</b></font><br /></c:if>
			</td>
			<td>
				<b>${tour.tourLabel}:</b> <a href="BrowseEvents?tourId=${tour.id}">${tour.name}</a> <br />
				<b>Event:</b> <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/>${event.name}
				<%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>			
					(<a href="EditorLinkAdmitone?eventId=${event.id}">link to admitone</a>)<c:if test="${not empty event}"><c:if test="${empty event.admitoneId}"><font color="red">(Not Linked)</font></c:if><c:if test="${not empty event.admitoneId}"><font color="green" title="${event.admitoneId}">(Linked)</font><br /> <b>Linked to:</b>${admitoneEventName}<c:if test='${admitoneEventDate != null}'> (${admitoneEventDate}) </c:if> </c:if></c:if><br />	
				<%-- </authz:authorize> --%>
				<b>${tour.artistLabel}:</b> <a href="BrowseTours?artistId=${artist.id}">${artist.name}</a>
				<div>
					<div style="float:left"><b>Event Date:</b></div>
					<div id="star_event_${param.eventId}" class="starOff" style="margin-top: -4px" onclick="toggleBookmarkEvent(${param.eventId})"></div>
					<div style="float:left">
						${event.formattedEventDate}
					</div>
				</div>
			</td>
			<td>
				<b>Venue Id:</b> ${venue.id}<br />
				<b>Venue:</b> ${venue.building}<br />
				<b>Venue City:</b> ${venue.city}<br />
				<b>Venue State:</b> ${venue.state}<br />
				<b>Venue Country:</b> <c:if test="${not empty venue.country && venue.country != 'null' }"><img src="../images/flags/${venue.country}.gif" />${venue.country}</c:if><br />
				<b>Last Full Update:</b>
				<c:choose>
					<c:when test="${not empty lastFullUpdate}">
						<fmt:formatDate value="${lastFullUpdate}" pattern="yyyy/MM/dd HH:mm:ss" />
					</c:when>
					<c:otherwise>
						<i>(crawl queued)</i>
					</c:otherwise>
				</c:choose>
			</td>	    
			<td style="text-align: right">
				<c:set var="eventTicketCount" value="${event.ticketCount}" />
				<c:choose>
					<c:when test="${eventTicketCount > 0}">
						<c:set var="nonDuplicateEventTicketCount" value="${event.nonDuplicateTicketCount}" />
						<c:set var="nonDuplicateEventTicketQuantitySum" value="${event.nonDuplicateTicketQuantitySum}" />			
						<b>Total number of entries (excl. duplicates):</b> <fmt:formatNumber value="${nonDuplicateEventTicketCount}" /><br />
						<b>Total number of duplicate entries:</b> <fmt:formatNumber value="${eventTicketCount - nonDuplicateEventTicketCount}" /><br />	
						<b>Total number of tickets(excl. duplicates):</b> <fmt:formatNumber value="${nonDuplicateEventTicketQuantitySum}" /><br />	
						<b>Filtered number of entries:</b> ${fn:length(webTickets)}<br />
						<b>Filtered number of tickets:</b> ${ticketCount}<br />
						<b>AO tickets / Market / Est. capacity:</b> <fmt:formatNumber value="${adminOneTicketsCount}" /> / <fmt:formatNumber value="${qtyOffered}" /> / <fmt:formatNumber value="${qtyAvailable}" /><br />					
					</c:when>
					<c:otherwise>
						<b>No tickets</b>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</table>

	<c:choose>
		<c:when test="${removeDuplicatePolicy == 'NONE'}">
			<c:set var="statEventTicketCount" value="${fn:length(webTickets)}" />
			<c:set var="statEventTicketQuantitySum" value="${ticketCount}" />
		</c:when>
		<c:otherwise>
			<c:set var="statEventTicketCount" value="${fn:length(webTickets)}" />
			<c:set var="statEventTicketQuantitySum" value="${ticketCount}" />
		</c:otherwise>
	</c:choose>
</div>

<div id="container-tabs" style="margin-top: 5px">
	<ul>
		<li><a href="#fragment-tickets" id="tab-tickets"><span>Tickets</span></a></li>
		<li><a href="#fragment-zone" id="tab-zone" ><span>Zone</span></a></li>
		<c:if test="${view != 'compact'}">
			<li><a href="#fragment-liquidity" id="tab-liquidity"><span>Liquidity</span></a></li>
		    <li><a href="#fragment-history" id="tab-history"><span>History</span></a></li>
		    <li><a href="#fragment-chart" id="tab-chart"><span>Charts</span></a></li>
		    <li><a href="#fragment-stats" id="tab-stats"><span>Stats</span></a></li>
		</c:if>
		<%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
			<li><a href="#fragment-crawls" id="tab-crawls"><span>Crawls</span></a></li>
		<%-- </authz:authorize> --%>
		<li><a href="#fragment-filters" id="tab-filters"><span>Filters</span></a></li>    
  	</ul>
	
	<div id="fragment-tickets">
		<c:set var="filterIcon" value="../images/ico-collapse.gif" />
		<c:set var="filterDisplay" value="block" />
	
		<%-- <authz:authorize ifAnyGranted="ROLE_USER">	 --%>		
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			<c:set var="filterDisplay" value="none" />
		<%-- </authz:authorize> --%>
		
		<c:choose>
			<c:when test="${browseTicketFilter == '1'}">
			<c:set var = "filterDisplay" value="block" />
			<c:set var="filterIcon" value="../images/ico-collapse.gif" />
			</c:when>
			<c:otherwise>
			<c:set var = "filterDisplay" value="none" />
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			</c:otherwise>
	</c:choose>
		<div id="ticket-filter-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleTicketFilter()" style="cursor: pointer">
				<b>Ticket Filter</b><img id="ticket-filter-expand-img" src="${filterIcon}" />
			</a>  
		</div>			
		<div id="ticket-filter-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplay}">
			<form action="${ticketsPage}" method="GET" onsubmit="changeCatFilters(); changeQuantityFilters(); changeCatScheme(); changeWholesaleCheckBox(); changeMutisortCheckBox();  $('#ticketFilterCancelButton').hide(); $('#ticketFilterUpdateButton').hide(); $('#ticketFilterUpdateWait').show()">
				<input id="multisortInput" type="hidden" name="multisort" value=""/>
				<input id="quantityInput" type="hidden" name="quantity" value=""/>
				<input id="categoryInput" type="hidden" name="category" value=""/>
				<input id="wholesaleInput" type="hidden" name="wholesale" value=""/>				
				<input id="selectedTab" type="hidden" name="selectedTab" value=""/>
	
				<input id="zoneCatSchemeHidden" type="hidden" name="zoneCatScheme" value=""/>	
				<input id="zoneQuantityInput" type="hidden" name="zoneQuantity" value=""/>				
				<input id="zoneCategoryInput" type="hidden" name="zoneCategory" value=""/>	
				<input id="zoneStartPriceHidden"  type="hidden" name="zoneStartPrice" value="">	
				<input id="zoneEndPriceHidden"  type="hidden" name="zoneEndPrice" value="">				
				<input id="historyCategoryInput" type="hidden" name="historyCategory" value=""/>
				<input id="historyQuantityInput" type="hidden" name="historyQuantity" value=""/>
				<input id="historyCatScheme" type="hidden" name="historyCatScheme" value=""/>
				<input id="liquidityCategoryInput" type="hidden" name="liquidityCategory" value=""/>
				<input id="liquidityQuantityInput" type="hidden" name="liquidityQuantity" value=""/>
				<input id="liquidityCatScheme" type="hidden" name="liquidityCatScheme" value=""/>
				<input id="statsCategoryInput" type="hidden" name="statsCategory" value=""/>
				<input id="statsQuantityInput" type="hidden" name="statsQuantity" value=""/>
				<input id="statsCatScheme" type="hidden" name="statsCatScheme" value=""/>				
				<input type="hidden" name="eventId" value="${event.id}"/>
				<table>
		<tr>
         <td width="100px"><b>Zone Group:</b></td>
          <td>
			<select name="catScheme" id="catScheme">
			  <c:forEach var="catg" items="${catGroups}">
			    <option value="${catg}" <c:if test="${catg == catGroupName}">selected</c:if>>${catg}</option>
			  </c:forEach>
			</select>
          </td> 
        </tr>
        <tr>
          <td><b>Zone:</b></td>
          <td>
			<input type="checkbox" id="cat-ALL" class="cat-checkbox"  <c:if test="${catFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#cat-UNCATS').removeAttr('checked'); $('#cat-CATS').removeAttr('checked'); $('.cat-specific').removeAttr('checked');}"/>
			ALL	
			<input type="checkbox" id="cat-CATS" class="cat-checkbox"  <c:if test="${catFilters['CAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#cat-ALL').removeAttr('checked');$('#cat-UNCATS').removeAttr('checked');$('.cat-specific').removeAttr('checked');}"/>
			Categorized
			<input type="checkbox" id="cat-UNCATS" class="cat-checkbox"  <c:if test="${catFilters['UNCAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#cat-ALL').removeAttr('checked'); $('#cat-CATS').removeAttr('checked'); $('.cat-specific').removeAttr('checked');}"/>
			Uncategorized	
			<c:forTokens items="${categorySymbols}" var="cat" delims=",">
				<input type="checkbox" id="cat-${cat}" class="cat-checkbox cat-specific" <c:if test="${catFilters[cat]}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#cat-ALL').removeAttr('checked'); $('#cat-CATS').removeAttr('checked');$('#cat-UNCATS').removeAttr('checked')}"/>
				${cat}	
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>			
			<a href="javascript: selectCategoryNone()" style="font-size:10px;">None</small></a> -
			<a href="javascript: selectUncategorized()" style="font-size:10px;">Uncategorized</small></a>-
			<a href="javascript: selectCategorized()" style="font-size:10px;">Categorized</small></a>- 			
			<a href="javascript: selectCategoryAll()" style="font-size:10px;">All</small></a> 			
          </td>
        </tr>
        <tr>
          <td><b>Quantity:</b></td>
          <td>
			<input type="checkbox" id="quantity-ALL" class="quantity-checkbox" <c:if test="${qtyFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('.quantity-specific').removeAttr('checked');}"/>
			ALL	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
				<input type="checkbox" id="quantity-${qty}" class="quantity-checkbox quantity-specific" <c:if test="${qtyFilters[qty]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#quantity-ALL').removeAttr('checked')" />
				<c:choose>
					<c:when test="${11 == qty}">${qty}+</c:when>
					<c:otherwise>${qty}</c:otherwise>
				</c:choose>		
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>
			<a href="javascript: selectQuantityNone()" style="font-size:10px;">None</small></a> -  
			<a href="javascript: selectQuantityGreaterThan4()" style="font-size:10px;">4+</small></a> -  
			<a href="javascript: selectQuantityAll()" style="font-size:10px;">All</small></a> 
          </td>
        </tr>
        <tr id="tr-location">
          <td><b>Location:</b></td>
          <td>
		    <b>Section:</b> <input type="text" id="section" name="section" value="${sectionFilter}"/>
		    <b>Row:</b> <input type="text" id="row" name="row" value="${rowFilter}"/>
          </td>
        </tr>
        <tr id="tr-price">
          <td><b>Price:</b></td>
          <td>
		    <b>From:</b> <input type="text" name="startPrice" value="${startPriceFilter}"/>
		    <b>To:</b> <input type="text" name="endPrice" value="${endPriceFilter}"/>
		    <b>Wholesale instead:</b>
		     <input type="checkbox" id="wholesale-checkbox" <c:if test="${wholesale == '1'}">checked</c:if>/> 
          </td>
        </tr>
      </table>
	<br />
	
	<div id="div-mulitsort">
		<input type="checkbox" id="multisort-checkbox"  <c:if test="${multisort}">checked</c:if> /> 
		<label for="multisort-checkbox">Multisort:</label>
		Sort by <select name="multisort_field0" id="multisort_selector0" class="multisort_selector"></select>
		then by <select name="multisort_field1" id="multisort_selector1" class="multisort_selector"></select>
		then by <select name="multisort_field2" id="multisort_selector2" class="multisort_selector"></select>
		then by <select name="multisort_field3" id="multisort_selector3" class="multisort_selector"></select>
		<br/><br/>
	</div>
    <div style="text-align:center" style="margin-top: 20px">
	<input id="ticketFilterUpdateButton" type="submit" class="medButton" name="submit" value="Update"/>  
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="ticketFilterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.form.reset(); resetMultisort()"/>&nbsp;
      <span id="ticketFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
	</form>
	</div>
   


<%-- <authz:authorize ifAnyGranted="PRIV_DATA"> --%>
	<b>Selected tickets:</b> <a href="#" onclick="return markAsDuplicate()"> Mark as duplicate</a> | <a href="#" onclick="return unmarkAsDuplicate()"> Unmark as duplicate</a> | <a href="#" onclick="untickAll(); return false;"> Untick tickets</a> | <a href="#" onclick="return markAsAdmitOne()"> Mark as AdmitOne</a> | <a href="#" onclick="return unmarkAsAdmitOne()"> Unmark as AdmitOne</a>
  <br/><br/>
<%-- </authz:authorize> --%>

<%-- <authz:authorize ifAnyGranted="PRIV_RETAIL"> --%>
<b>Markup Percent Addition:</b> <input name="markupPercent" id="markupPercent" type="text" value="" size="3" value="20"/>% <input type="button" class="medButton" value="Update" onclick="updateMarkupPrice($('#markupPercent').val())"/>
<br/>    
<%-- </authz:authorize> --%>
<b>Add to quotes:</b><input type="hidden" id="quoteDate" value="${today}"/>
<input type="button" class="medButton" id="quoteAdd" value="Add New Quote" onclick="addSelectedTicketsToQuotes()"/>
<input type="button" class="medButton" style ="width:200px" value="Append Existing Quote" onclick="addSelectedTicketsToAppendQuotes()"/>
<br />
	
<c:if test="${not empty webTickets}">
<div id="ticket-grid" style="height:420;width:600" ></div>
</c:if>

<br />
<img src="../images/ico-download.gif" align="absbottom" /> <a href="DownloadTicketsCsv?eventId=${event.id}">Download CSV</a>

<c:choose>
    <c:when test="${not empty webTickets}">
      <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
        <img src="../images/ico-add.gif" align="absbottom" /> <a href="EbayAddInventory?eventId=${event.id}">Add to ZSP Inventory</a>
      <%-- </authz:authorize> --%>
    </c:when>
  <c:otherwise>
	<c:choose>
		<c:when test="${eventTicketCount == 0}">
			This event has no tickets. This event might have expired or does not have crawls.
		</c:when>
		<c:otherwise>
			This event has tickets but no tickets match the filters. You can try to <a href="${ticketsPage}?eventId=${event.id}&reset=1">Reset the filters</a>
			or change your filter settings.			
		</c:otherwise>
	</c:choose>
  </c:otherwise>
</c:choose>
  </div>
 <div id="fragment-zone">
 <div id="zone-filter-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleZoneFilter()" style="cursor: pointer">
				<b>Zone Filter</b><img id="zone-filter-expand-img" src="${filterIcon}" />
			</a>  
	</div>	
	<div id="zone-filter-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplay}">
			<form id="zoneForm" name="zoneForm"  method="Get" onsubmit="changeCatFilters(); changeQuantityFilters();  $('#zoneFilterCancelButton').hide(); $('#zoneFilterUpdateButton').hide(); $('#zoneFilterUpdateWait').show()">				
				
				<table>
		
		<tr>
         <td width="100px"><b>Zone Group:</b></td>
          <td>
			<select name="zoneCatSchemeSelect" id="zoneCatScheme">
			  <c:forEach var="catg" items="${zoneCatGroups}">
			    <option value="${catg}" <c:if test="${catg == zoneCatGroupName}">selected</c:if>>${catg}</option>
			  </c:forEach>
			</select>
          </td> 
        </tr>
		<tr>
          <td><b>Zone:</b></td>
          <td>
			<input type="checkbox" id="zoneCat-ALL" class="zoneCat-checkbox"  <c:if test="${zoneCatFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) $('.zoneCat-specific').removeAttr('checked');" />
			ALL							
			<c:forTokens items="${zoneCategorySymbols}" var="cat" delims=",">
				<input type="checkbox" id="zoneCat-${cat}" class="zoneCat-checkbox zoneCat-specific" <c:if test="${zoneCatFilters[cat]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#zoneCat-ALL').removeAttr('checked'); "/>
				${cat}	
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>			
			<a href="javascript: selectCategoryNone()" style="font-size:10px;">None</small></a>-			 			
			<a href="javascript: selectCategoryAll()" style="font-size:10px;">All</small></a> 			
          </td>
        </tr>
        		
        <tr>
          <td><b>Quantity:</b></td>
          <td>
			<input type="checkbox" id="zoneQuantity-ALL" class="zoneQuantity-checkbox" <c:if test="${zoneQtyFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) $('.zoneQuantity-specific').removeAttr('checked')" />
			ALL	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
				<input type="checkbox" id="zoneQuantity-${qty}" class="zoneQuantity-checkbox zoneQuantity-specific" <c:if test="${zoneQtyFilters[qty]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#zoneQuantity-ALL').removeAttr('checked')" />
				<c:choose>
					<c:when test="${11 == qty}">${qty}+</c:when>
					<c:otherwise>${qty}</c:otherwise>
				</c:choose>		
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>
			<a href="javascript: selectQuantityNone()" style="font-size:10px;">None</small></a> -  
			<a href="javascript: selectQuantityGreaterThan4()" style="font-size:10px;">4+</small></a> -  
			<a href="javascript: selectQuantityAll()" style="font-size:10px;">All</small></a> 
          </td>
        </tr>
        
        <tr>
          <td><b>Price:</b></td>
          <td>
		    <b>From:</b> <input type="text" id="zoneStartPrice" name="zoneStartPriceTxt" value="${zoneStartPrice}" onkeyup="JavaScript:checkPrice('zoneStartPrice');"/>
		    <b>To:</b> <input type="text" id="zoneEndPrice" name="zoneEndPriceTxt" value="${zoneEndPrice}" onkeyup="JavaScript: checkPrice('zoneEndPrice');"/>
		    
          </td>
        </tr>
      </table>
	<br />
	
	<div>
		
	</div>
    <div style="text-align:center" style="margin-top: 20px">
	<input id="zoneFilterUpdateButton" type="button" class="medButton" name="submit" value="Update"/>  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="zoneFilterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.zoneForm.reset(); "/>&nbsp;
      <span id="zoneFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
  </form>
</div>

	
 <%-- <authz:authorize ifAnyGranted="PRIV_RETAIL"> --%>
<b>Markup Percent Addition:</b> 
<input name="markupPercentForZone" id="markupPercentForZone" type="text" value="" size="3" value="20"/>% <input type="button" class="medButton" value="Update" onclick="updateMarkupPrice($('#markupPercent').val())"/>
<br/>
<%-- </authz:authorize> --%>
<b>Add to quotes:</b><input type="hidden" id="zoneQuoteDate" value="${today}"/>
<input type="button" class="medButton" value="Add New Quote" onclick="addSelectedTicketsToQuotes()"/>
<input type="button" class="medButton" style ="width:200px" value="Append Existing Quote" onclick="addSelectedTicketsToAppendQuotes()"/>

<br />
  <c:if test="${not empty ticketgroups}">	
	<display:table list="${ticketgroups}" id="group" class="list" pagesize="100" requestURI="${ticketsPage}" >
	                          <display:column
					title="<input type='checkbox' id='selectAll' name='selectAll'" >
					<div align="center"><input type="checkbox"
						value="${group.ebayInventory1.ticket.id}--${group.ebayInventory1.ticket.category.symbol}--${group.ebayInventoryPrice}--${group.quantity}" 
						name="select" /></div>
				</display:column> 
													
							<display:column title="Qty" >${group.quantity}</display:column>
							<display:column title="Category" sortable="true" >
							${group.ebayInventory1.ticket.category.symbol}</display:column>	
							<display:column title="Section" sortable="true" >
							Zone ${group.ebayInventory1.ticket.category.symbol}</display:column>	
							<display:column title="Per Ticket Price" sortable="true" property="ebayInventoryPrice" format="{0,number,currency}">
							</display:column>
							<display:column title="Total" sortable="true" property="zonePrice" format="{0,number,currency}">
							</display:column>
														
							
						</display:table>
	</c:if>
	
	</div>
  <c:if test="${view != 'compact'}">

  <div id="fragment-liquidity">
	<c:set var="filterIcon" value="../images/ico-collapse.gif" />
		<c:set var="filterDisplay" value="block" />
	
		<%-- <authz:authorize ifAnyGranted="ROLE_USER">		 --%>	
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			<c:set var="filterDisplay" value="none" />
		<%-- </authz:authorize> --%>
		
		<c:choose>
			<c:when test="${browseTicketFilter == '1'}">
			<c:set var = "filterDisplay" value="block" />
			<c:set var="filterIcon" value="../images/ico-collapse.gif" />
			</c:when>
			<c:otherwise>
			<c:set var = "filterDisplay" value="none" />
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			</c:otherwise>
	</c:choose>
		<div id="liquidity-filter-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleLiquidityFilter()" style="cursor: pointer">
				<b>Ticket Filter</b><img id="liquidity-filter-expand-img" src="${filterIcon}" />
			</a>  
		</div>			
		<div id="liquidity-filter-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplay}">
			<form method="GET">		
				
			<table>
		<tr>
         <td width="100px"><b>Category Group:</b></td>
          <td>
			<select name="liquidity-catScheme" id="liquidity_catScheme">
			  <c:forEach var="catg" items="${catGroups}">
			    <option value="${catg}" <c:if test="${catg == liquidityCatScheme}">selected</c:if>>${catg}</option>
			  </c:forEach>
			</select>
          </td> 
        </tr>
        <tr>
          <td><b>Zone:</b></td>
          <td>
			<input type="checkbox" id="liquidity-cat-ALL" class="liquidity-cat-checkbox"  <c:if test="${liquidityCatFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#liquidity-cat-CATS').removeAttr('checked');$('#liquidity-cat-UNCATS').removeAttr('checked');$('.liquidity-cat-specific').removeAttr('checked');}"/>
			ALL	
			<input type="checkbox" id="liquidity-cat-CATS" class="liquidity-cat-checkbox"  <c:if test="${liquidityCatFilters['CAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#liquidity-cat-ALL').removeAttr('checked');$('#liquidity-cat-UNCATS').removeAttr('checked');$('.liquidity-cat-specific').removeAttr('checked');}"/>
			Categorized
			<input type="checkbox" id="liquidity-cat-UNCATS" class="liquidity-cat-checkbox"  <c:if test="${liquidityCatFilters['UNCAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#liquidity-cat-ALL').removeAttr('checked');$('#liquidity-cat-CATS').removeAttr('checked');$('.liquidity-cat-specific').removeAttr('checked');}"/>
			Uncategorized	
			<c:forTokens items="${liquidityCategorySymbols}" var="cat" delims=",">
				<input type="checkbox" id="liquidity-cat-${cat}" class="liquidity-cat-checkbox liquidity-cat-specific" <c:if test="${liquidityCatFilters[cat]}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#liquidity-cat-ALL').removeAttr('checked'); $('#liquidity-cat-CATS').removeAttr('checked');$('#liquidity-cat-UNCATS').removeAttr('checked');}"/>
				${cat}	
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>			
			<a href="javascript: selectLiquidityCategoryNone()" style="font-size:10px;">None</small></a> -
			<a href="javascript: selectLiquidityUncategorized()" style="font-size:10px;">Uncategorized</small></a>-
			<a href="javascript: selectLiquidityCategorized()" style="font-size:10px;">Categorized</small></a>- 			
			<a href="javascript: selectLiquidityCategoryAll()" style="font-size:10px;">All</small></a> 			
          </td>
        </tr>
        <tr>
          <td><b>Quantity:</b></td>
          <td>
			<input type="checkbox" id="liquidity-quantity-ALL" class="liquidity-quantity-checkbox" <c:if test="${liquidityQtyFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) $('.liquidity-cat-specific').removeAttr('checked')"/>
			ALL	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
				<input type="checkbox" id="liquidity-quantity-${qty}" class="liquidity-quantity-checkbox liquidity-cat-specific" <c:if test="${liquidityQtyFilters[qty]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#liquidity-quantity-ALL').removeAttr('checked')" />
				<c:choose>
					<c:when test="${11 == qty}">${qty}+</c:when>
					<c:otherwise>${qty}</c:otherwise>
				</c:choose>		
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>
			<a href="javascript: selectLiquidityQuantityNone()" style="font-size:10px;">None</small></a> -  
			<a href="javascript: selectLiquidityQuantityGreaterThan4()" style="font-size:10px;">4+</small></a> -  
			<a href="javascript: selectLiquidityQuantityAll()" style="font-size:10px;">All</small></a> 
          </td>
        </tr>          
      </table>	
    <div style="text-align:center" style="margin-top: 20px">
	<input id="liquidityFilterUpdateButton" type="button" class="medButton" name="submit" value="Update"/>  
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="liquidityFilterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.form.reset();"/>&nbsp;
      <span id="liquidityFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
	</form>
	</div>
	<div class="info">
		<div class="infoText">
			IN is the number of tickets that were put on the exchange during the last 24 hours.<br />
			OUT is the number of tickets that were put off (expired or sold) the exchange during the last 24 hours.<br />			
			Liquidity ratio is computed as follow: <i>liquidity_ratio = 100 * (OUT - IN) / START</i><br />
			with START the number of active tickets 24 hours ago.
			<br /><br />			
			Note that the figures in the tables below depend on the filters that you select.
		</div>
	</div>
	
	<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<th><img src="../images/ico-${siteId}.gif"/></th>
				</c:forEach>
				<th class="total">Total incl. dup.</th>
				<th class="total">Total excl. dup.</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Start Quantity</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${startTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${startTicketCountBySiteId[siteId]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCount}" /></td>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>IN tickets</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${inTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<font color="green"><fmt:formatNumber pattern="0" value="${inTicketCountBySiteId[siteId]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><font color="green">${inTicketCount}</font></td>
				<td class="total" align="right"><font color="green">${inTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>OUT tickets</td>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${outTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<font color="red"><fmt:formatNumber pattern="0" value="${outTicketCountBySiteId[siteId]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>				
				</c:forEach>
				<td class="total" align="right"><font color="red">${outTicketCount}</font></td>
				<td class="total" align="right"><font color="red">${outTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>End Quantity</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right">
						<c:choose>
							<c:when test="${endTicketCountBySiteId[siteId] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${endTicketCountBySiteId[siteId]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCount}" /></td>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>Liquidity ratio</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">				
					<td align="right">
						<c:choose>
							<c:when test="${liquidityRatioBySiteId[siteId] == 0}">-</c:when>
							<c:when test="${liquidityRatioBySiteId[siteId] > 0}">
								+<fmt:formatNumber pattern="0.00" value="${liquidityRatioBySiteId[siteId]}" />
							</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0.00" value="${liquidityRatioBySiteId[siteId]}" />							
							</c:otherwise>
						</c:choose>
					</td>
				</c:forEach>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatio == 0}">-</c:when>
						<c:when test="${liquidityRatio > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatio}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatio}" />							
						</c:otherwise>
					</c:choose>
				</td>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy == 0}">-</c:when>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />							
						</c:otherwise>
					</c:choose>
				</td>

			</tr>
			<tr>
				<td>% of Market</td>			
				<c:forEach var="siteId" items="${constants.siteIds}">
					<td align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentBySiteId[siteId]}" />%</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercent}" />%</td>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentWithDuplicateRemovalPolicy}" />%</td>
			</tr>
		</tbody>
	</table>
	<br />

	<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<th>${catSymbol}</th>
				</c:forEach>
				<th class="total">Total</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Start Quantity</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${startTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${startTicketCountByCategory[catSymbol]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${startTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>IN tickets</td>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${inTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<font color="green"><fmt:formatNumber pattern="0" value="${inTicketCountByCategory[catSymbol]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><font color="green">${inTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>OUT tickets</td>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${outTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<font color="red"><fmt:formatNumber pattern="0" value="${outTicketCountByCategory[catSymbol]}" /></font>								
							</c:otherwise>
						</c:choose>						
					</td>				
				</c:forEach>
				<td class="total" align="right"><font color="red">${outTicketCountWithDuplicateRemovalPolicy}</font></td>
			</tr>
			<tr>
				<td>End Quantity</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${endTicketCountByCategory[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${endTicketCountByCategory[catSymbol]}" />
							</c:otherwise>
						</c:choose>						
					</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0" value="${endTicketCountWithDuplicateRemovalPolicy}" /></td>
			</tr>
			<tr>
				<td>Liquidity ratio</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${liquidityRatioByCategory[catSymbol] == 0}">-</c:when>
							<c:when test="${liquidityRatioByCategory[catSymbol] > 0}">
								+<fmt:formatNumber pattern="0.00" value="${liquidityRatioByCategory[catSymbol]}" />
							</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0.00" value="${liquidityRatioByCategory[catSymbol]}" />							
							</c:otherwise>
						</c:choose>
					</td>
				</c:forEach>
				<td class="total" align="right">
					<c:choose>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy == 0}">-</c:when>
						<c:when test="${liquidityRatioWithDuplicateRemovalPolicy > 0}">
							+<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />
						</c:when>
						<c:otherwise>
							<fmt:formatNumber pattern="0.00" value="${liquidityRatioWithDuplicateRemovalPolicy}" />							
						</c:otherwise>
					</c:choose>
				</td>

			</tr>
			<tr>
				<td>% of Market</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentByCategory[catSymbol]}" />%</td>
				</c:forEach>
				<td class="total" align="right"><fmt:formatNumber pattern="0.00" value="${marketPercentWithDuplicateRemovalPolicy}" />%</td>
			</tr>
			
		</tbody>
	</table>
	<br />
		<table class="list">
		<thead>
			<tr>
				<th></th>
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<th>${catSymbol}</th>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Tickets Offered / Seats Available</td>			
				<c:forEach var="catSymbol" items="${categorySymbolList}">
					<td align="right">
						<c:choose>
							<c:when test="${offeredTicketsByCat[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${offeredTicketsByCat[catSymbol]}" />
							</c:otherwise>
						</c:choose>		
						/
						<c:choose>
							<c:when test="${empty availableTicketsByCat[catSymbol] || availableTicketsByCat[catSymbol] == 0}">-</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${availableTicketsByCat[catSymbol]}" />
							</c:otherwise>
						</c:choose>					
					</td>
				</c:forEach>
			</tr>		
		</tbody>
	</table>
	  
  </div>

  <div id="fragment-history">
	<c:set var="filterIcon" value="../images/ico-collapse.gif" />
		<c:set var="filterDisplay" value="block" />
	
		<%-- <authz:authorize ifAnyGranted="ROLE_USER">	 --%>		
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			<c:set var="filterDisplay" value="none" />
		<%-- </authz:authorize> --%>
		
		<c:choose>
			<c:when test="${browseTicketFilter == '1'}">
			<c:set var = "filterDisplay" value="block" />
			<c:set var="filterIcon" value="../images/ico-collapse.gif" />
			</c:when>
			<c:otherwise>
			<c:set var = "filterDisplay" value="none" />
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			</c:otherwise>
	</c:choose>
		<div id="history-filter-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleHistoryFilter()" style="cursor: pointer">
				<b>Ticket Filter</b><img id="history-filter-expand-img" src="${filterIcon}" />
			</a>  
		</div>			
		<div id="history-filter-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplay}">
			<form method="GET">		
				
			<table>
		<tr>
         <td width="100px"><b>Category Group:</b></td>
          <td>
			<select name="history-catScheme" id="history_catScheme">
			  <c:forEach var="catg" items="${catGroups}">
			    <option value="${catg}" <c:if test="${catg == historyCatScheme}">selected</c:if>>${catg}</option>
			  </c:forEach>
			</select>
          </td> 
        </tr>
        <tr>
          <td><b>Zone:</b></td>
          <td>
			<input type="checkbox" id="history-cat-ALL" class="history-cat-checkbox"  <c:if test="${historyCatFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#history-cat-CATS').removeAttr('checked');$('#history-cat-UNCATS').removeAttr('checked');$('.history-cat-specific').removeAttr('checked');}"/>
			ALL	
			<input type="checkbox" id="history-cat-CATS" class="history-cat-checkbox"  <c:if test="${historyCatFilters['CAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#history-cat-ALL').removeAttr('checked');$('#history-cat-UNCATS').removeAttr('checked');$('.history-cat-specific').removeAttr('checked');}"/>
			Categorized
			<input type="checkbox" id="history-cat-UNCATS" class="history-cat-checkbox"  <c:if test="${historyCatFilters['UNCAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#history-cat-ALL').removeAttr('checked');$('#history-cat-CATS').removeAttr('checked');$('.history-cat-specific').removeAttr('checked');}"/>
			Uncategorized	
			<c:forTokens items="${historyCategorySymbols}" var="cat" delims=",">
				<input type="checkbox" id="history-cat-${cat}" class="history-cat-checkbox history-cat-specific" <c:if test="${historyCatFilters[cat]}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#history-cat-ALL').removeAttr('checked');$('.history-cat-specific').removeAttr('checked'); $(this).attr('checked',true); $('#history-cat-CATS').removeAttr('checked');$('#history-cat-UNCATS').removeAttr('checked');}"/>
				${cat}	
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>			
			<a href="javascript: selectHistoryCategoryNone()" style="font-size:10px;">None</small></a> -
			<a href="javascript: selectHistoryUncategorized()" style="font-size:10px;">Uncategorized</small></a>-
			<a href="javascript: selectHistoryCategorized()" style="font-size:10px;">Categorized</small></a>- 			
			<a href="javascript: selectHistoryCategoryAll()" style="font-size:10px;">All</small></a> 			
          </td>
        </tr>
        <tr>
          <td><b>Quantity:</b></td>
          <td>
			<input type="checkbox" id="history-quantity-ALL" class="history-quantity-checkbox" <c:if test="${historyQtyFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) $('.history-quantity-specific').removeAttr('checked')"/>
			ALL	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
				<input type="checkbox" id="history-quantity-${qty}" class="history-quantity-checkbox history-quantity-specific" <c:if test="${historyQtyFilters[qty]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#history-quantity-ALL').removeAttr('checked')" />
				<c:choose>
					<c:when test="${11 == qty}">${qty}+</c:when>
					<c:otherwise>${qty}</c:otherwise>
				</c:choose>		
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>
			<a href="javascript: selectHistoryQuantityNone()" style="font-size:10px;">None</small></a> -  
			<a href="javascript: selectHistoryQuantityGreaterThan4()" style="font-size:10px;">4+</small></a> -  
			<a href="javascript: selectHistoryQuantityAll()" style="font-size:10px;">All</small></a> 
          </td>
        </tr>          
      </table>	
    <div style="text-align:center" style="margin-top: 20px">
	<input id="historyFilterUpdateButton" type="button" class="medButton" name="submit" value="Update"/>  
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="historyFilterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.form.reset();"/>&nbsp;
      <span id="historyFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
	</form>
	</div>
<%
   long printHistoryStartTime = System.currentTimeMillis();
%>
  
	<div class="info">
		<div class="infoText">
			IN is the number of tickets that were put on the exchange during the given day.<br />
			OUT is the number of tickets that were put off (expired or sold) the exchange during the given day.<br /><br />
			Liquidity ratio and % of Market are computed as follow: <br />
			<ul>
			<i>liquidity_ratio = 100 * (OUT - IN) / START</i><br />
			<i>% of Market = 100 * OUT / START</i><br />
			</ul>
			with START the number of active tickets at the beginning of the given day.						
		</div>
	</div>
  
    <display:table class="list" name="${stats}" id="stat" requestURI="${ticketsPage}" pagesize="20">

		<display:column title="Date" property="date" format="{0,date,yyyy/MM/dd}" class="bold" />
		<display:column title="Start Quantity" property="startTicketCount" class="text-align-right" />
		<display:column title="End Quantity" property="endTicketCount" class="text-align-right" />
		<display:column title="TIX IN" property="inTicketCount" class="text-align-right" style="color:green" />
		<display:column title="TIX OUT" property="outTicketCount" class="text-align-right" style="color:red" />
		<display:column title="Min Qty" property="minQuantity" class="text-align-right" />
		<display:column title="Max Qty" property="maxQuantity" class="text-align-right" />		
		<display:column title="% of Market" class="text-align-right">
			<fmt:formatNumber pattern="0.00" value="${stat.marketPercent}" />%
		</display:column>		
		<display:column title="Min Price1" property="minPrice1" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Min Price2" property="minPrice2" format="{0,number, currency}" class="text-align-right" />		
		<display:column title="Median" property="median" format="{0,number, currency}" class="text-align-right" />
		<display:column title="Market Close" property="marketClosed" format="{0,number, currency}" class="text-align-right" />		
	</display:table>
  </div>

<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printHistory", System.currentTimeMillis() - printHistoryStartTime);
%>
        
  <div id="fragment-chart">
  	  <c:set var="filterIconForChart" value="../images/ico-collapse.gif" />
		<c:set var="filterDisplayForChart" value="block" />
	
		<%-- <authz:authorize ifAnyGranted="ROLE_USER">	 --%>		
			<c:set var="filterIconForChart" value="../images/ico-expand.gif" />
			<c:set var="filterDisplayForChart" value="none" />
		<%-- </authz:authorize> --%>
		
		<c:choose>
			<c:when test="${browseTicketFilter == '1'}">
			<c:set var = "filterDisplayForChart" value="block" />
			<c:set var="filterIconForChart" value="../images/ico-collapse.gif" />
			</c:when>
			<c:otherwise>
			<c:set var = "filterDisplayForChart" value="none" />
			<c:set var="filterIconForChart" value="../images/ico-expand.gif" />
			</c:otherwise>
		</c:choose>
   
		<div id="ticket-filter-chart-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleTicketFilterForChart()" style="cursor: pointer">
				<b>Ticket Filter</b><img id="ticket-filter-chart-expand-img" src="${filterIconForChart}" />
			</a>  
		</div>	
		
		<div id="ticket-filter-chart-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplayForChart}">
			<form method="GET" onsubmit="changeCatFiltersForChart(); changeQuantityFiltersForChart();">
				
				<input id="quantityInputForChart" type="hidden" name="quantity" value=""/>
				<input id="categoryInputForChart" type="hidden" name="category" value=""/>				
				<input type="hidden" name="eventIdForChart" value="${event.id}"/>
				<table>
					<div id="common-div">
					<tr>
					 <td width="100px"><b>Zone Group:</b></td>
					  <td>
						<select name="catScheme" id="catSchemeForChart">
						  <c:forEach var="catg" items="${catGroups}">
							<option value="${catg}" <c:if test="${catg == catGroupName}">selected</c:if>>${catg}</option>
						  </c:forEach>
						</select>
					  </td> 
					</tr>
					<tr>
					  <td><b>Zone:</b></td>
					  <td>
						<input type="checkbox" id="cat-ALL-For-Chart" class="cat-checkbox-For-Chart" onclick="if ($(this).attr('checked')) $('.cat-specific-For-Chart').removeAttr('checked')"/>
						ALL					
						<c:forTokens items="${categorySymbols}" var="cat" delims=",">
							<input type="checkbox" id="cat-${cat}-For-Chart" class="cat-checkbox-For-Chart cat-specific-For-Chart" onclick="if ($(this).attr('checked')) $('#cat-ALL-For-Chart').removeAttr('checked')"/>
							${cat}	
							&nbsp;			
						</c:forTokens>
						<span style="font-weight: bold;font-size: 10px;">Select:</span>			
						<a href="javascript: selectCategoryNoneForChart()" style="font-size:10px;">None</small></a> -					
						<a href="javascript: selectCategoryAllForChart()" style="font-size:10px;">All</small></a> 			
					  </td>		  
					</tr>
					<tr>
					  <td><b>Quantity:</b></td>
					  <td>
						<input type="checkbox" id="quantity-ALL-For-Chart" class="quantity-checkbox-For-Chart" onclick="if ($(this).attr('checked')) $('.quantity-specific-For-Chart').removeAttr('checked')"/>
						ALL	
						<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
							<input type="checkbox" id="quantity-${qty}-For-Chart" class="quantity-checkbox-For-Chart quantity-specific-For-Chart" onclick="if ($(this).attr('checked')) $('#quantity-ALL-For-Chart').removeAttr('checked')"/>
							<c:choose>
								<c:when test="${11 == qty}">${qty}+</c:when>
								<c:otherwise>${qty}</c:otherwise>
							</c:choose>		
							&nbsp;			
						</c:forTokens>
						<span style="font-weight: bold;font-size: 10px;">Select:</span>
						<a href="javascript: selectQuantityNoneForChart()" style="font-size:10px;">None</small></a> -  
						<a href="javascript: selectQuantityGreaterThan4ForChart()" style="font-size:10px;">4+</small></a> -  
						<a href="javascript: selectQuantityAllForChart()" style="font-size:10px;">All</small></a> 
					  </td>		  
					</tr>
					</div>
		<tr>
			<td></td>
			<td>			
			<input type="checkbox" id="avg-For-Chart"/>Average
		  </td>
		</tr>
		<tr><td colspan="2">
					<div style="text-align:center;margin-top: 20px">
							<input id="chartFilterUpdateButton" type="button" class="medButton" name="submitChart" value="Update"/>  
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;							
							<span id="ChartFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
					</div>
			</td>
		</tr>
      </table>
	  
	<br />    
  </form>
</div>
	  
	  
	  
  	<div id="containerForPrice" style="min-width:100%; height: 400px; margin: 0 auto" ></div>
	<hr>
	<div id="containerForQuantity" style="min-width:100%; height: 400px; margin: 0 auto" ></div> 
  	
  		
  </div>
  
  <div id="fragment-stats">
	 <c:set var="filterIcon" value="../images/ico-collapse.gif" />
		<c:set var="filterDisplay" value="block" />
	
		<%-- <authz:authorize ifAnyGranted="ROLE_USER">	 --%>		
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			<c:set var="filterDisplay" value="none" />
		<%-- </authz:authorize> --%>
		
		<c:choose>
			<c:when test="${browseTicketFilter == '1'}">
			<c:set var = "filterDisplay" value="block" />
			<c:set var="filterIcon" value="../images/ico-collapse.gif" />
			</c:when>
			<c:otherwise>
			<c:set var = "filterDisplay" value="none" />
			<c:set var="filterIcon" value="../images/ico-expand.gif" />
			</c:otherwise>
	</c:choose>
		<div id="stats-filter-toggle" style="background:#a1a8ff; padding: 5px">
			<a onclick="toggleStatsFilter()" style="cursor: pointer">
				<b>Ticket Filter</b><img id="stats-filter-expand-img" src="${filterIcon}" />
			</a>  
		</div>			
		<div id="stats-filter-info" style="background:#a1a8ff; padding: 5px; margin-bottom: 5px;display:${filterDisplay}">
			<form method="GET">		
				
			<table>
		<tr>
         <td width="100px"><b>Zone Group:</b></td>
          <td>
			<select name="stats-catScheme" id="stats_catScheme">
			  <c:forEach var="catg" items="${catGroups}">
			    <option value="${catg}" <c:if test="${catg == statsCatScheme}">selected</c:if>>${catg}</option>
			  </c:forEach>
			</select>
          </td> 
        </tr>
        <tr>
          <td><b>Zone:</b></td>
          <td>
			<input type="checkbox" id="stats-cat-ALL" class="stats-cat-checkbox"  <c:if test="${statsCatFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('.stats-cat-specific').removeAttr('checked');$('#stats-cat-CATS').removeAttr('checked');$('#stats-cat-UNCATS').removeAttr('checked');}"/>
			ALL	
			<input type="checkbox" id="stats-cat-CATS" class="stats-cat-checkbox"  <c:if test="${statsCatFilters['CAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#stats-cat-ALL').removeAttr('checked');$('.stats-cat-specific').removeAttr('checked');$('#stats-cat-UNCATS').removeAttr('checked');}"/>
			Categorized
			<input type="checkbox" id="stats-cat-UNCATS" class="stats-cat-checkbox"  <c:if test="${statsCatFilters['UNCAT']}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#stats-cat-ALL').removeAttr('checked');$('.stats-cat-specific').removeAttr('checked');$('#stats-cat-CATS').removeAttr('checked');}"/>
			Uncategorized	
			<c:forTokens items="${statsCategorySymbols}" var="cat" delims=",">
				<input type="checkbox" id="stats-cat-${cat}" class="stats-cat-checkbox stats-cat-specific" <c:if test="${statsCatFilters[cat]}">checked</c:if> onclick="if ($(this).attr('checked')) {$('#stats-cat-ALL').removeAttr('checked'); $('#stats-cat-CATS').removeAttr('checked');$('#stats-cat-UNCATS').removeAttr('checked');}"/>
				${cat}	
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>			
			<a href="javascript: selectStatsCategoryNone()" style="font-size:10px;">None</small></a> -
			<a href="javascript: selectStatsUncategorized()" style="font-size:10px;">Uncategorized</small></a>-
			<a href="javascript: selectStatsCategorized()" style="font-size:10px;">Categorized</small></a>- 			
			<a href="javascript: selectStatsCategoryAll()" style="font-size:10px;">All</small></a> 			
          </td>
        </tr>
        <tr>
          <td><b>Quantity:</b></td>
          <td>
			<input type="checkbox" id="stats-quantity-ALL" class="stats-quantity-checkbox" <c:if test="${statsQtyFilters['ALL']}">checked</c:if> onclick="if ($(this).attr('checked')) $('.stats-quantity-specific').removeAttr('checked')" />
			ALL	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11" var="qty" delims=",">
				<input type="checkbox" id="stats-quantity-${qty}" class="stats-quantity-checkbox stats-quantity-specific" <c:if test="${statsQtyFilters[qty]}">checked</c:if> onclick="if ($(this).attr('checked')) $('#stats-quantity-ALL').removeAttr('checked')" />
				<c:choose>
					<c:when test="${11 == qty}">${qty}+</c:when>
					<c:otherwise>${qty}</c:otherwise>
				</c:choose>		
				&nbsp;			
			</c:forTokens>
			<span style="font-weight: bold;font-size: 10px;">Select:</span>
			<a href="javascript: selectStatsQuantityNone()" style="font-size:10px;">None</small></a> -  
			<a href="javascript: selectStatsQuantityGreaterThan4()" style="font-size:10px;">4+</small></a> -  
			<a href="javascript: selectStatsQuantityAll()" style="font-size:10px;">All</small></a> 
          </td>
        </tr>          
      </table>	
    <div style="text-align:center" style="margin-top: 20px">
	<input id="statsFilterUpdateButton" type="button" class="medButton" name="submit" value="Update"/>  
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input id="statsFilterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.form.reset();"/>&nbsp;
      <span id="statsFilterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
	</form>
	</div>
    <a name="eventStat"/>
    <h3>Number of tickets per zone and site</h3>

<%
   long printStatsStartTime = System.currentTimeMillis();
%>

    <c:choose>
	  <c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	  </c:when>
	  <c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forEach var="siteId" items="${constants.siteIds}">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1">
							<img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" />
						</a>
					</th>
				</c:forEach>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="category" items="${statsCategories}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
				
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1"><b>Cat. ${category.symbol}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forEach var="siteId" items="${constants.siteIds}">
							<c:set var="total" value="${total + siteStatByCategory[category.symbol][siteId]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty siteStatByCategory[category.symbol][siteId]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&cat=${category.id}&reset=1&uncheckSiteFilters=1">
											${siteStatByCategory[category.symbol][siteId]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forEach>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketQuantitySum}" />%</b>
						</td>
					</tr>
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
		
				<c:choose>
					<c:when test="${(row % 2) == 0}">
						<c:set var="rowClass" value="even" />
					</c:when>
					<c:otherwise>
						<c:set var="rowClass" value="odd" />
					</c:otherwise>				
				</c:choose>
		
				<tr class="${rowClass}">
					<td>
						<a href="${ticketsPage}?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
					</td>
					<c:set var="total" value="0" />
					<c:forEach var="siteId" items="${constants.siteIds}">
						<c:set var="total" value="${total + siteStatByCategory['UNCAT'][siteId]}" />
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&cat=UNCAT&reset=1">
								${siteStatByCategory['UNCAT'][siteId]}
							</a>
						</td>
					</c:forEach>
					<td align="right">
						<a href="${ticketsPage}?eventId=${event.id}&reset=1">							
							<b>${total}</b>
						</a>
					</td>
					<td align="right">
						<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketQuantitySum}" />%</b>
					</td>
				</tr>
		
			</tbody>
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">			
							<c:set var="total" value="${total + totalStatBySite[siteId]}" />					
							${totalStatBySite[siteId]}
						</td>				
					</c:forEach>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatBySite[siteId] * 100 / statEventTicketQuantitySum}" />%
						</td>				
					</c:forEach>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
			
		</table>
	</c:otherwise>
  </c:choose>

  <h3>Number of ticket entries per site and quantity</h3>

  <c:choose>
	<c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	</c:when>
	<c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&reset=1">${quantity}</a>
					</th>
				</c:forTokens>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="siteId" items="${constants.siteIds}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1"><img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" /><b>${siteId}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
							<c:set var="total" value="${total + quantityStatBySiteId[siteId][quantity]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty quantityStatBySiteId[siteId][quantity]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&${siteId}_checkbox=true&quantities=${quantity}&reset=1&uncheckSiteFilters=1">
											${quantityStatBySiteId[siteId][quantity]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forTokens>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantities}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
						</td>
					</tr>
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
			</tbody>
		
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td align="right">			
							<c:set var="total" value="${total + totalStatByQuantity[quantity]}" />					
							${totalStatByQuantity[quantity]}
						</td>				
					</c:forTokens>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / statEventTicketCount}" />%
						</td>				
					</c:forTokens>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
		
		</table>
	  </c:otherwise>
    </c:choose>

   <h3>Number of ticket entries per zone and quantity</h3>

  <c:choose>
	<c:when test="${eventTicketCount == 0}">
		Nothing found to display.		
	</c:when>
	<c:otherwise>
		<table class="list">
			<thead>
			<tr>
				<th></th>		
				<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
					<th>
						<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&reset=1">${quantity}</a>
					</th>
				</c:forTokens>
				<th>Total</th>
				<th>%</th>
			</tr>	
			</thead>
			<tbody>
				<c:set var="row" value="1" />					
				<c:forEach var="category" items="${statsCategories}">
					<c:choose>
						<c:when test="${(row % 2) == 0}">
							<c:set var="rowClass" value="even" />
						</c:when>
						<c:otherwise>
							<c:set var="rowClass" value="odd" />
						</c:otherwise>				
					</c:choose>
					<tr class="${rowClass}">
						<td>
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1"><b>Cat. ${category.symbol}</b></a>
						</td>
						<c:set var="total" value="0" />
						<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
							<c:set var="total" value="${total + quantityStatByCategory[category.symbol][quantity]}" />
							<td align="right">
								<c:choose>
									<c:when test="${empty quantityStatByCategory[category.symbol][quantity]}">
										-
									</c:when>
									<c:otherwise>
										<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&quantities=${quantity}&reset=1">
											${quantityStatByCategory[category.symbol][quantity]}
										</a>								
									</c:otherwise>								
								</c:choose>							
							</td>
						</c:forTokens>
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&cat=${category.id}&reset=1">				
								<b>${total}</b>
							</a>
						</td>
						<td align="right">
							<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
						</td>
					</tr>			
					<c:set var="row" value="${row + 1}" />					
				</c:forEach>
		
				<c:choose>
					<c:when test="${(row % 2) == 0}">
						<c:set var="rowClass" value="even" />
					</c:when>
					<c:otherwise>
						<c:set var="rowClass" value="odd" />
					</c:otherwise>				
				</c:choose>
				<tr class="${rowClass}">
					<td>
						<a href="${ticketsPage}?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
					</td>
					<c:set var="total" value="0" />
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<c:set var="total" value="${total + quantityStatByCategory['UNCAT'][quantity]}" />
						<td align="right">
							<a href="${ticketsPage}?eventId=${event.id}&quantities=${quantity}&cat=UNCAT&reset=1">
								${quantityStatByCategory['UNCAT'][quantity]}
							</a>
						</td>
					</c:forTokens>
					<td align="right">
						<a href="${ticketsPage}?eventId=${event.id}&reset=1">							
							<b>${total}</b>
						</a>
					</td>
					<td align="right">
						<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / statEventTicketCount}" />%</b>
					</td>
				</tr>
		
			</tbody>
		
			<tfoot>
				<tr>
					<c:set var="total" value="${0}" />					
					<td align="right">Total</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td align="right">			
							<c:set var="total" value="${total + totalStatByQuantity[quantity]}" />					
							${totalStatByQuantity[quantity]}
						</td>				
					</c:forTokens>
					<td align="right">${total}</td>
					<td align="center">-</td>
				</tr>
				<tr>
					<td align="right">%</td>
					<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
						<td>			
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / statEventTicketCount}" />%
						</td>				
					</c:forTokens>
					<td align="center">-</td>
					<td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${100}" />%</td>
				</tr>
			</tfoot>
		
		</table>
	 </c:otherwise>
    </c:choose>
    
<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printStats", System.currentTimeMillis() - printStatsStartTime);
%>
    
  </div>
 </c:if>
  <div id="fragment-crawls">
	
	<%-- <authz:authorize ifAnyGranted="PRIV_CRAWLER"> --%>
<%
   long printCrawlsStartTime = System.currentTimeMillis();
%>
	
	<div style="margin-top:16px;margin-bottom:8px;">
		<div style="float: left"><b>Legends:</b></div>
	
		<div style="margin-left:20px;width:10px;height:10px;background-color: #EB9E6F;border: 2px solid #EB9E6F;margin-right: 6px;float: left"></div>  
		<div style="float: left">Disabled crawl</div>
	
		<div style="margin-left:20px;width:10px;height:10px;background: #748C20;border: 2px solid #748C20;margin-right: 6px;float: left"></div>  
		<div style="float: left">Selected crawl</div>
				
		<div style="margin-left:20px;width:10px;height:10px;background: #666666;border: 2px solid #666666;margin-right: 6px;float: left"></div>  
		<div style="float: left">Broken crawl</div>		
	
		<div style="clear:both"></div>
	</div>
	
	<table>
		<tr>
			<td>
				<select class="addCrawlSelector" onchange="changeAddCrawler($(this).val())">
					<option selected>-- Create new crawl --</option>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<c:set var="safeSiteId" value="[${siteId}]" />
					
					    <c:choose>
					      <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
						    <option style="text-decoration:line-through;margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}" disabled>${siteId}</option>
					      </c:when>
					      <c:otherwise>
						    <option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}">${siteId}</option>
					      </c:otherwise>
					    </c:choose>
					</c:forEach>
				</select>
			</td>
			<c:if test="${not empty crawls}">
				<td width="30">&nbsp;</td>
				<td>
					<b>Select: </b>
					<a href="javascript:void(0)" onclick="selectAllTicketListingCrawls()">All</a> -
					<a href="javascript:void(0)" onclick="unselectAllTicketListingCrawls()">None</a>			
				</td>
				<td width="30">&nbsp;</td>
				<td>
					<b>Action: </b>
					<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedTicketListingCrawls()">Enable</a> -
					<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedTicketListingCrawls()">Disable</a> -
					<img src="../images/ico-reload.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="forceRecrawlSelectedTicketListingCrawls()">Force Recrawl</a>
				</td>
			</c:if>
		</tr>
	</table>
	
	<display:table class="list" name="${crawls}" id="crawl" requestURI="${ticketsPage}" decorator="crawlTableDecorator">
	    <display:column sortable="false" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	   	  <input type="checkbox" id="ticket-listing-crawl-${crawl.id}" class="ticket-listing-crawl" onChange="toggleTicketListingCrawlRow($(this).attr('id').split('-')[3])" />
	    </display:column>
	    <display:column title="Name" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper" class="content-left-align" headerClass="header-left-align">
	    	<img src="../images/ico-${crawl.siteId}.gif" align="absmiddle" title="${crawl.siteId}"/>
	    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=${ticketsPage}?eventId=${event.id}"><b>${crawl.name}</b></a>
	    </display:column>
	    <display:column title="Status" sortable="true" style="width: 90px;" decorator="crawlStatusColumnDecorator" value="${crawl}" />
	    <display:column title="Last crawl" sortable="true" style="width: 90px;" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper"><span tooltip="<fmt:formatDate value="${crawl.startCrawl}" pattern="yyyy/MM/dd HH:mm:ss" />"><fmt:formatDate value="${crawl.startCrawl}" pattern="HH:mm:ss" /></span></display:column>    
	    <display:column title="Creator" sortable="true" property="creator" />
	    <display:column title="Created" sortable="true"><fmt:formatDate value="${crawl.created}" pattern="yyyy/MM/dd HH:mm:ss" /></display:column>
	    <display:column title="Last Updater" sortable="true" property="lastUpdater" />
	    <display:column title="Last Updated" sortable="true"><fmt:formatDate value="${crawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" /></display:column>
		<display:column sortable="true"><a href="RedirectToListingPage?id=${crawl.id}" target="_blank"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a></display:column>
	</display:table>
	<br/><br/>
	<a href="EditorManageCrawls?tourId=${event.artistId}&eventId=${event.id}">Go to crawler page</a>
	
<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printCrawls", System.currentTimeMillis() - printCrawlsStartTime);
%>	
	<%-- </authz:authorize> --%>
  </div>
  
<%
   long printFiltersStartTime = System.currentTimeMillis();
%>  
  <div id="fragment-filters">
	
	<a href="${ticketsPage}?eventId=${event.id}&reset=1">Reset filter</a>
   <form action="${pageName}" method="get" onsubmit="$('#filterUpdateButton').hide(); $('#filterCancelButton').hide(); $('#filterUpdateWait').show(); changeSiteFilters(); changeShowDuplicateFilter(); ">
    <input id="showDuplicatesInput" type="hidden" name="showDuplicates" value=""/>
    <input type="hidden" name="eventId" value="${event.id}"/>
    <div>	
	<div style="font-size: 12px">
		<div>
			<div style="float: left;width: 260px;">
				View:
				<select name="view">
				  <option value="compact" <c:if test="${view == 'compact'}">selected</c:if>>Compact</option>
				  <option value="expanded" <c:if test="${view == 'expanded'}">selected</c:if>>Expanded</option>
				  <option value="analytic" <c:if test="${view == 'analytic'}">selected</c:if>>Analytic</option>
				</select>
			</div>
			
			<div style="float: left;width: 400px;">
				<label for="removeDuplicatePolicy">Remove Duplicate Policy</label>
				<select name="removeDuplicatePolicy" id="removeDuplicatePolicy" >
					<option value="NONE" <c:if test="${removeDuplicatePolicy == 'NONE'}">selected</c:if> >NONE</option>
					<option value="SUPER_SMART" <c:if test="${removeDuplicatePolicy == 'SUPER_SMART'}">selected</c:if>>SCRUB</option>
				</select>
				<br/><input type="checkbox" id="showDuplicates" <c:if test="${showDuplicates}">checked</c:if> /> Show duplicates
				<c:if test="${removeDuplicatePolicy != 'NONE'}">
					<i>(${removedDuplicateCount} duplicate entries were removed)</i>
				</c:if>
			</div>
		</div>
		<div style="clear: both"></div>		
		<div style="border-top: 1px dashed black; border-bottom: 1px dashed black;padding-top: 5px; padding-bottom: 5px;margin-top: 5px;">
			Site:
			(
			<a href="#" onclick="javascript:checkAllSiteFilter(); return false"><small>check all</small></a> -
			<a href="#" onclick="javascript:uncheckAllSiteFilter(); return false"><small>uncheck all</small></a>
			)
			<small>note: prices will be adjusted as follow: <i>new price = price * (1.0 + adjustment / 100)</i></small><br />			
			<div style="float:left;">
				<c:forEach var="sid" items="${constants.siteIds}">
					<c:set var="safeSid" value="[${sid}]" />
					<div style="float: left; width: 280px;height: 24px;">
						
						<input type="hidden" id="${sid}-checkbox-input" name="${sid}_checkbox" value=""/> 
						<input type="checkbox" id="${sid}-checkbox" class="site-checkbox"
							<c:if test="${siteFilters[sid] and not fn:contains(constants.disabledSiteIdsString, safeSid)}">checked</c:if>
							<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">disabled</c:if>
						>
						<label for="${sid}_checkbox">
					    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
							<c:choose>
							  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSid)}">
							    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
								  ${sid}
							    </span>
							  </c:when>
							  <c:otherwise>
							    ${sid}
							  </c:otherwise>
							</c:choose>
						</label>

						<%-- <authz:authorize ifAnyGranted="PRIV_DATA">	 --%>									
	
							<span id="price-adjustment-${sid}" style="padding: 2px;">
								(
								<c:set var="priceAdjustment" value="${priceAdjustments[sid]}" />																									
								<c:choose>
									<c:when test="${empty priceAdjustment || priceAdjustment.percentAdjustment == 0}">
										<span id="price-adjustment-span-${sid}">+0.00%</span>
									</c:when>
									<c:when test="${priceAdjustment.percentAdjustment < 0}">
										<span id="price-adjustment-span-${sid}" style="color: #aa2222; font-weight: bold">
											<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
										</span>
									</c:when>
									<c:otherwise>
										<span id="price-adjustment-span-${sid}" style="color: #22aa22; font-weight: bold">
											+<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${priceAdjustment.percentAdjustment}" />%
										</span>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty siteById[sid] || siteById[sid].priceRounding == 'NONE'}">
									</c:when>								
									<c:when test="${siteById[sid].priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
										<img id="price-adjustment-price-rounding-icon-${sid}" src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
									</c:when>
									<c:when test="${siteById[sid].priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
										<img id="price-adjustment-price-rounding-icon-${sid}" src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
									</c:when>
								</c:choose>
								
								<a id="price-adjustment-link-edit-${sid}" href="javascript:void(0);" onclick="editPriceAdjustment('${sid}')" style="font-size:10px">edit</a>
								<span id="price-adjustment-edit-${sid}" style="display:none">
									<input id="price-adjustment-textfield-${sid}" type="text" style="font-size:10px;width:40px;">
									<span id="price-adjustment-textfield-percent-${sid}">%</span>
									<a id="price-adjustment-link-save-${sid}" href="javascript:void(0);" onclick="savePriceAdjustment('${sid}')" style="font-size:10px;">ok</a>
									<a id="price-adjustment-link-cancel-${sid}" href="javascript:void(0);" onclick="cancelPriceAdjustment('${sid}')" style="font-size:10px;">cancel</a>
								</span>
								<span id="price-adjustment-saving-${sid}" style="color:red;font-size: 10px;display:none">
									<img src="../images/process-running.gif" align="absbottom" /> Saving...
								</span>
								)
							</span>
						<%-- </authz:authorize> --%>
					</div>
				</c:forEach>
			</div>
			<div style="clear:both"></div>
			<%-- <authz:authorize ifAnyGranted="PRIV_DATA">	 --%>		
				<a href="EditorEditSites" style="font-size:10px">Change price rounding</a>
			<%-- </authz:authorize> --%>
		</div>
	  </div>
    </div>
<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printFilters", System.currentTimeMillis() - printFiltersStartTime);
%>
    
    Number of visible ticket rows to display on the screen:
    <select name="numTicketRows" id="numTicketRows">
      <c:forEach var="i" begin="10" end="40" step="2">
        <option value="${i}" <c:if test="${i == numTicketRows}">selected</c:if>>${i}</option>
      </c:forEach>
    </select>
    <br/>
    <div style="text-align:center">
     <input id="filterUpdateButton" type="submit" class="medButton" name="submit" value="Update"/>&nbsp;
      <input id="filterCancelButton" type="button" class="medButton" value="Cancel" onClick="this.form.reset()"/>
     <span id="filterUpdateWait" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Updating...</span>
    </div>
  </div>
 </form>
</div>

<div id="markAsDuplicateDialog" title="" style="display:none">
	<span id="markAsDuplicateDialogMessage" align="center"></span>
</div>
<div id="dialogAdd" title="Select Customer">
	
	<p>Please select customer.</p>
	<table><tr><td>
	<b>Customer:</b></td><td> <input type="text" id="quoteCustomer" name="quoteCustomer" size="12"/><br />	
	</td></tr>
	<tr><td>
	<b>Reference:</b></td><td> <input type="text" id="refferal" name="refferal" size="12"/></td></tr></table>
</div>
<script type="text/javascript">

<%
   long printBookmarksStartTime = System.currentTimeMillis();
%>

var bookmarks = [
<c:forEach var="bookmark" items="${ticketBookmarks}"><%
    TicketBookmark bookmark = (TicketBookmark)pageContext.getAttribute("bookmark");
	
%>${bookmark.objectId},
</c:forEach>
null
];

<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printBookmarks", System.currentTimeMillis() - printBookmarksStartTime);
%>

<%
   long printTicketsStartTime = System.currentTimeMillis();
%>

//fancy ticket loading
var tickets = [
<c:forEach var="ticket" items="${webTickets}">
  <c:set var="action">
    <c:if test="${not empty actions[ticket.id]}">
      <c:choose>
        <c:when test="${actions[ticket.id].tgCheaper > 7}">
          <div class="highOverAction">TC#: ${actions[ticket.id].tgCheaper} OP$: ${actions[ticket.id].overprice} ${actions[ticket.id].overpriceNext}</div>
        </c:when>
        <c:when test="${actions[ticket.id].tgCheaper > 4}">
          <div class="medOverAction">TC#: ${actions[ticket.id].tgCheaper} OP$: ${actions[ticket.id].overprice} ${actions[ticket.id].overpriceNext}</div>
        </c:when>
        <c:when test="${actions[ticket.id].tgCheaper > 2}">
          <div class="lowOverAction">TC#: ${actions[ticket.id].tgCheaper} OP$: ${actions[ticket.id].overprice} ${actions[ticket.id].overpriceNext}</div>
        </c:when>
        <c:when test="${actions[ticket.id].tgCheaper > 0}">
          <div class="basic">TC#: ${actions[ticket.id].tgCheaper} OP$: ${actions[ticket.id].overprice} ${actions[ticket.id].overpriceNext}</div>
        </c:when>
        <c:otherwise>
          <div class="basic">UP#: ${actions[ticket.id].underprice} ${actions[ticket.id].underpriceNext}</div>
        </c:otherwise>
      </c:choose>
    </c:if>
  </c:set>
[${ticket.id},${ticket.admitOneTicket},${ticket.admitOneNonTMTicket},${ticket.hasDuplicate},${ticket.removedDuplicateTicket},${ticket.remainingQuantity},${ticket.lotSize},'${ticket.categorySymbol}','${ticket.section}','${ticket.normalizedSection}','${ticket.row}','${ticket.normalizedRow}',${ticket.adjustedCurrentPrice}, ${ticket.buyItNowPrice},${ticket.purchasePrice},${ticket.numSecondsBeforeEndDate},'${ticket.ticketType}','${ticket.seat}','<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${ticket.lastUpdate}"/>','${ticket.siteId}','${ticket.seller}','${ticket.itemId}',${ticket.numerizedSection},'${ticket.ticketDeliveryType}'<c:choose><c:when test="${view == 'analytic' && not empty actions[ticket.id]}">,${ticket.eval},'${action}'</c:when><c:when test="${view == 'analytic' && empty actions[ticket.id]}">,${ticket.eval},null</c:when><c:otherwise>,null,null</c:otherwise></c:choose>,null,null,${ticket.dupRefTicketId}<c:choose><c:when test="${view == 'expanded'}">,${ticket.soldQuantity},'<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${ticket.endDate}"/>','<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${ticket.insertionDate}"/>','${ticket.ticketStatus}'</c:when><c:otherwise>,null,null,null,null</c:otherwise></c:choose>,'${ticket.priceByQuantity}'],
</c:forEach>
null
];

<%
   ((java.util.Map)pageContext.getAttribute("timeStats", javax.servlet.jsp.PageContext.REQUEST_SCOPE)).put("printTickets", System.currentTimeMillis() - printTicketsStartTime);
%>
</script>

<%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
<br />
<a onclick="toggleSystemStats()" style="cursor: pointer"><img id="system-stats-expand-img" src="../images/ico-expand.gif" /> System Stats</a>
<div id="system-stats-info" style="display:none">
	Time to perform the following actions:<br /><br />	
	Handle Preferences: <c:choose><c:when test="${empty timeStats.handlePreferences}">-</c:when><c:otherwise>${timeStats.handlePreferences} ms</c:otherwise></c:choose><br />
	Filtered Tickets: <c:choose><c:when test="${empty timeStats.filteredTickets}">-</c:when><c:otherwise>${timeStats.filteredTickets} ms</c:otherwise></c:choose><br />
	<ul>
	  <li>Load Zones: <c:choose><c:when test="${empty timeStats.loadCategories}">-</c:when><c:otherwise>${timeStats.loadCategories} ms</c:otherwise></c:choose><br /></li>
	  <li>Load Tickets: <c:choose><c:when test="${empty timeStats.loadTickets}">-</c:when><c:otherwise>${timeStats.loadTickets} ms</c:otherwise></c:choose><br /></li>
	  <li>Preassign Zones: <c:choose><c:when test="${empty timeStats.preAssignCategories}">-</c:when><c:otherwise>${timeStats.preAssignCategories} ms</c:otherwise></c:choose><br /></li>
	  <li>Filter Categorized: <c:choose><c:when test="${empty timeStats.filterCategorized}">-</c:when><c:otherwise>${timeStats.filterCategorized} ms</c:otherwise></c:choose><br /></li>
	  <li>Filter Tickets: <c:choose><c:when test="${empty timeStats.filterTickets}">-</c:when><c:otherwise>${timeStats.filterTickets} ms</c:otherwise></c:choose><br /></li>
	  <li>Load Historical Tickets: <c:choose><c:when test="${empty timeStats.loadHistoricalTickets}">-</c:when><c:otherwise>${timeStats.loadHistoricalTickets} ms</c:otherwise></c:choose><br /></li>
	  <li>Filter Historical Tickets: <c:choose><c:when test="${empty timeStats.filterHistoricalTickets}">-</c:when><c:otherwise>${timeStats.filterHistoricalTickets} ms</c:otherwise></c:choose><br /></li>
	</ul>
	Handle Mark Duplicates: <c:choose><c:when test="${empty timeStats.handleMarkDuplicates}">-</c:when><c:otherwise>${timeStats.handleMarkDuplicates} ms</c:otherwise></c:choose><br />
	Liquidity Stats: <c:choose><c:when test="${empty timeStats.liquidityStats}">-</c:when><c:otherwise>${timeStats.liquidityStats} ms</c:otherwise></c:choose><br />
	Remove Duplicate Tickets: <c:choose><c:when test="${empty timeStats.removeDuplicateTickets}">-</c:when><c:otherwise>${timeStats.removeDuplicateTickets} ms</c:otherwise></c:choose><br />
	Remove Duplicate Historical Tickets: <c:choose><c:when test="${empty timeStats.removeDuplicateHistoricalTickets}">-</c:when><c:otherwise>${timeStats.removeDuplicateHistoricalTickets} ms</c:otherwise></c:choose><br />
	Event Stats: <c:choose><c:when test="${empty timeStats.eventStats}">-</c:when><c:otherwise>${timeStats.eventStats} ms</c:otherwise></c:choose><br />
	History Stats: <c:choose><c:when test="${empty timeStats.historyStats}">-</c:when><c:otherwise>${timeStats.historyStats} ms</c:otherwise></c:choose><br />
	Action For Tickets: <c:choose><c:when test="${empty timeStats.actionsForTickets}">-</c:when><c:otherwise>${timeStats.actionsForTickets} ms</c:otherwise></c:choose><br />	
	Print Tickets: <c:choose><c:when test="${empty timeStats.printTickets}">-</c:when><c:otherwise>${timeStats.printTickets} ms</c:otherwise></c:choose><br />
	Print Bookmarks: <c:choose><c:when test="${empty timeStats.printBookmarks}">-</c:when><c:otherwise>${timeStats.printBookmarks} ms</c:otherwise></c:choose><br />	
	Print History: <c:choose><c:when test="${empty timeStats.printHistory}">-</c:when><c:otherwise>${timeStats.printHistory} ms</c:otherwise></c:choose><br />	
	Print Stats: <c:choose><c:when test="${empty timeStats.printStats}">-</c:when><c:otherwise>${timeStats.printStats} ms</c:otherwise></c:choose><br />	
	Print Filters: <c:choose><c:when test="${empty timeStats.printFilters}">-</c:when><c:otherwise>${timeStats.printFilters} ms</c:otherwise></c:choose><br />	
	Print Crawls: <c:choose><c:when test="${empty timeStats.printCrawls}">-</c:when><c:otherwise>${timeStats.printCrawls} ms</c:otherwise></c:choose><br />	
</div>
<%-- </authz:authorize> --%>



</div> 



<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketRow" />
<jsp:directive.page import="com.admitone.tmat.data.TicketBookmark" />

<script type="text/javascript">
	$("#markAsDuplicateDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 600,
		height:400,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	$("#dialogAdd").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 300,
		height:200,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

    $('#container-tabs').tabs({
    select: function(event, ui) {
      var url = ui.tab.href;
      var posAnchor = url.indexOf('#');
  	//anchor = url.substr(posAnchor, url.length - posAnchor);
  	anchor = "";   
  	
      if (url.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('opacity', 1);
        $('#chartDiv').css('height', 400);
	  } else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
	  }
    }
    });
    
    if (location.href.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('height', 400);
        $('#chartDiv').css('opacity', 1);
    }  else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
    }
    updateCrawls($('#nocrawl').val());
    $('#event-details-info').hide();
	$('#LoadingEventSpan').hide();
	$('#eventNavigationSpan').show();
	$('#wholePageDiv').show();
	
	<c:if test="${not empty param.scroll}">
	  window.scrollTo(0, ${param.scroll});
	</c:if>

  var anchor = "";

  <c:if test="${not empty eventBookmark.objectId}">
    $('#star_event_' + ${eventBookmark.objectId}).attr('class', 'starOn');
  </c:if>



// remove last element which is null
tickets = tickets.slice(0, tickets.length - 1);
bookmarks = bookmarks.slice(0, bookmarks.length - 1);

// INDEX contants
var TICKET_ID_INDEX = 0;
var IS_ADM_TICKET_INDEX = 1;
var HAS_DUP_INDEX = 2;
var DUP_TICKET_INDEX = 3;
var REM_QTY_INDEX = 4;
var LOT_SIZE_INDEX = 5;
var TICKET_CAT_INDEX = 6;
var TICKET_SECTION_INDEX = 7;
var NORM_SECTION_INDEX = 8;
var TICKET_ROW_INDEX = 9;
var NORM_ROW_INDEX = 10;
var ADJ_PRICE_INDEX = 11;
var CURR_PRICE_INDEX = 12;
var PUR_PRICE_INDEX = 13;
var NUM_SECS_INDEX = 14;
var TICKET_TYPE_INDEX = 15;
var TICKET_SEAT_INDEX = 16;
var LAST_UPDATE_INDEX = 17;
var SITE_ID_INDEX = 18;
var TICKET_SELLER_INDEX = 19;
var ITEM_ID_INDEX = 20;
var NUMERIZED_SECTION_INDEX = 21;
var TICKET_DELIV_INDEX = 22;
var E_VAL_INDEX = 23;
var TG_EVAL = 24;
//var OVER_PRICE_INDEX = 23;
//var OVER_NEXT_INDEX = 24;
//var UNDER_PRICE_INDEX = 25;
//var UNDER_NEXT_INDEX = 26;
var BOOKMARK_INDEX = 25;
var CHECK_INDEX = 26;
var TICKET_DUP_REF_ID_INDEX = 28;
var SOLD_QTY_INDEX = 29;
var END_DATE_INDEX = 30;
var INS_DATE_INDEX = 31;
var TICKET_STATUS_INDEX = 32;
var PRICE_BY_QUANTITY = 33;
 
//BOOKMARK contants
//var TICKET_BOOKMARK_INDEX = 32;

$.each(tickets, function(i, ticket) {
	if($.inArray(ticket[TICKET_ID_INDEX], bookmarks) > -1) {
		ticket[BOOKMARK_INDEX ] = 'On';
	} else {
		ticket[BOOKMARK_INDEX ] = 'Off'
	}
});

  var duplicateTicketMap = Array();
  
  <c:forEach var="duplicateTicket" items="${duplicateTicketMap}">
      duplicateTicketMap[${duplicateTicket.ticketId}] = ${duplicateTicket.groupId};
  </c:forEach>
  
  var explicitlyMarkedDisplayGroupMap = Array();
  var displayGroupMap = Array();
  
  var groupCounter = 1;
  for (var i in duplicateTicketMap) {
    var groupId = duplicateTicketMap[i];
    if (explicitlyMarkedDisplayGroupMap[groupId] == undefined) {
      explicitlyMarkedDisplayGroupMap[groupId] = groupCounter++;
    }
  } 

  groupCounter = 1;
  for (var i = 0; i < tickets.length; i++) {
    var ticket = tickets[i];
    var dupRefTicketId = ticket[TICKET_DUP_REF_ID_INDEX];
    if (dupRefTicketId == undefined || dupRefTicketId == '') {
      continue;
    }
    if (displayGroupMap[dupRefTicketId] == undefined) {
      displayGroupMap[dupRefTicketId] = groupCounter++;
    }    
  } 
  
var ticketView = "${view}";
var dupPolicy = "${removeDuplicatePolicy}";

//there are some handlers associated when exiting the page that make the page slow, so remove them.
$(window).unbind("unload");

$(document).ready(function() {
	loadOnStartUp();
	// initialize multisort selector
	var options = [
		[0, "None"],
		[1, "Remaining Quantity"],
		[2, "Category"],
		[3, "Section"],
		[4, "Row"],
		[5, "Wholesale Price (asc)"],
		[105, "Wholesale Price (desc)"],
		[6, "Online Price (asc)"],
		[106, "Online Price (desc)"],
		[7, "Purchase Price (asc)"],
		[107, "Purchase Price (desc)"],
		[8, "Seller"],
		[10, "Sold"],
		[9, "Lot Size"],
		[11, "Item Id"],
		[12, "End Date"],
		[13, "Insertion Date"],
		[14, "Last Update"],
		[15, "Status"],
	    [18, "Action Indicator"],
		[19, "eVal (asc)"],
		[119, "eVal (desc)"]
	];	
	
	$.each($('.multisort_selector'), function(i, selector) {
		$.each(options, function(j, option) {
			$(selector).append($.OPTION({value: option[0]}, option[1]));
		});
	});	
	
	resetMultisort();
	
	$('.ticket-listing-crawl').attr('checked', false);

	$('table#crawl TBODY tr').click(
		function() {
			var checkbox = $($(this).children('TD').get(0)).children('INPUT');
			$(checkbox).attr('checked', !$(checkbox).attr('checked'));
			updateTicketListingCrawlRow($(checkbox).attr('id').split("-")[3]);
		}
	);

	

	
	
});


//var __rowCount;

var markupPercentAddition = 20;
var checkedTickets = new Array();

function resetMultisort() {
	
	$('#multisort_selector0').val("${multisortField0}");
	$('#multisort_selector1').val("${multisortField1}");
	$('#multisort_selector2').val("${multisortField2}");
	$('#multisort_selector3').val("${multisortField3}");
}

function checkTicket(ticketId, record) {
  var pos = $.inArray(ticketId, checkedTickets);
  if (pos < 0) {
	  record.set('checked', true);
      checkedTickets.push(ticketId);
  } else if (pos > -1) {
	record.set('checked', false);
  	checkedTickets.splice(pos, 1);
  }
}

function quantityRenderer(val, cell, record) {
	return "<span title='lot size:" + record.data.lotSize + "'>" + record.data.quantity + "</span>";
};


function sellerRenderer(val, cell, record) {
  var sellerContent = "";
  var auction = "";
  var delivery = "";
  if (record.data.ticketType == 'AUCTION') {
     var remainingTime = '';
     if (record.data.numSecs > 24 * 3600) {
     	remainingTime = parseInt(record.data.numSecs / (24 * 3600)) + ' days';
     } else if (record.data.numSecs > 3600) {
     	remainingTime = parseInt(record.data.numSecs / (3600)) + ' hours';
     } else if (record.data.numSecs > 60) {
     	remainingTime = parseInt(record.data.numSecs / (3600)) + ' minutes';
     } else {
     	remainingTime = record.data.numSecs + ' seconds';
     }

     if (record.data.numSecs < 24 * 3600) {
  	   auction = '<img src="../images/ico-clock-ending.gif" title="Ending in ' + remainingTime + '"/>';
  	 } else {
  	   auction = '<img src="../images/ico-clock.gif" title="Ending in ' + remainingTime + '"/>';
  	 }
  }

  if (record.data.deliv == 'EDELIVERY') {
  	delivery = '<img src="../images/ico-edelivery.gif"/>';
  } else if (record.data.deliv == 'INSTANT') {
  	delivery = '<img src="../images/ico-instant-edelivery.gif"/>';
  }else if (record.data.deliv == 'ETICKETS') {
  	delivery = '<img src="../images/ico-eticket.gif"/>';
  }else if (record.data.deliv == 'MERCURYPOS') {
  	delivery = '<img src="../images/ico-ticketnetworkmercury.gif"/>';
  }
  
  var info = "";
  if (record.data.site == 'eimarketplace') {
  	  info = '<img onclick="window.open(\'RedirectToSellerPage?id=' + record.data.tid + '\', \'' + record.data.seller + '\', \'width=450,height=450\')" src="../images/ico-info.gif"/>';
  }

  return '<a target="_blank" href="RedirectToItemPage?id=' + record.data.tid + '"><img src="../images/ico-' + record.data.site + '.gif"/>' + auction + record.data.seller + '</a>' + info + delivery;
}

function markupRenderer(val, cell, record) {
	if (record.data.admitOneTicket) {
		return '$' + Ext.util.Format.number(record.data.pPrice, '?0,000.00');
	}

	return '$' + Ext.util.Format.number(Math.ceil((record.data.pPrice * (100 + markupPercentAddition)) / 100), '?0,000.00');
}

function moneyRenderer(val) {
  return '$' + Ext.util.Format.number(val, '?0,000.00');
}

function purchaseRenderer(val, cell, record) {
	return '<span title="' + record.data.priceByQty + '">' + '$' + Ext.util.Format.number(val, '?0,000.00') + '</span>';
}

function sectionRenderer(val, cell, record) {
  return '<span title="' + record.data.section + '">' + record.data.normSection + '</span>';
}

function rowRenderer(val, cell, record) {
  return '<span title="' + record.data.row + '">' + record.data.normRow + '</span>';
}

var store = null;
Ext.onReady(function(){
	store = new Ext.data.Store({
		<c:if test="${multisort}">remoteSort: true,</c:if>
		bufferSize: 2000,
		bufferRange : 2000,
		reader: new Ext.data.ArrayReader({}, [
			{name: 'tid', type: 'int'},
			{name: 'admitOneTicket', type: 'boolean'},
			{name: 'admitOneNonTMTicket', type: 'boolean'},
			{name: 'dupe', type: 'boolean'},
			{name: 'remDupe', type: 'boolean'},
			{name: 'quantity', type: 'int'},
			{name: 'lotSize', type: 'int'},
			{name: 'cat'},
			{name: 'section'},
			{name: 'normSection'},
			{name: 'row', type: 'int'},
			{name: 'normRow'},
			{name: 'aPrice', type: 'float'},
			{name: 'oPrice', type: 'float'},
			{name: 'pPrice', type: 'float'},			
			{name: 'numSecs', type: 'float'},
			{name: 'ticketType'},
			{name: 'seat'},
			{name: 'lastChange'},
			{name: 'site'},
			{name: 'seller'},
			{name: 'item'},
			{name: 'numerizedSection'},
			{name: 'deliv'},
			{name: 'eval', type: 'int'},
			{name: 'action'},
			{name: 'starStatus'},
			{name: 'checked', type: 'boolean'},
			{name: 'dupRefTicketId', type: 'int'},
			{name: 'soldQty', type: 'int'},
			{name: 'endDate'},
			{name: 'insDate'},
			{name: 'ticketStatus'},	
			{name: 'priceByQty'},							
		])
	});
    
	<c:if test="${not multisort}">
		//store.setDefaultSort('oPrice', 'desc');
	</c:if>
	    
	// manually load local data
	store.loadData(tickets);
	
	myView = new Ext.ux.grid.BufferView({
		rowHeight: 24,
		scrollDelay: 1,
		getRowClass : function(record, index) {
			if (record.data.remDupe) {
				return "removedDuplicateTicket";
			}

			if (record.data.admitOneNonTMTicket) {
				return "admit1RowNonTM";
			}
			
			if (record.data.admitOneTicket) {
				return "admit1Row";
			}
	
			if (record.data.starStatus == 'On') {
				return "starOnRow";
			}
		}
	});
	Ext.data.Store.prototype.sortData = function(f, direction){
		direction = direction || 'ASC';
		var st = this.fields.get(f).sortType;
		//alert(f);
		var fn = function(r1, r2) {
			var v1='';
			var v2='';
			var flag=false;
			if(f=='numerizedSection'){
				if(isNaN(r1.data.section)){
					v1= (r1.data.section).toLowerCase().charCodeAt(0);
					v2= (r2.data.section).toLowerCase().charCodeAt(0);
					flag=true;
				}else{
					v1 = parseInt(r1.data.section);
				}
				if(!flag){
					if(isNaN(r2.data.section)){
						v1= (r1.data.section).toLowerCase().charCodeAt(0);
						v2= (r2.data.section).toLowerCase().charCodeAt(0);
					}else{
						v2 = parseInt(r2.data.section);
					}
				}
			}else if(f=='normRow'){
				if(isNaN(r1.data.row)){
					v1= (r1.data.normRow).toLowerCase().charCodeAt(0);
					v2= (r2.data.normRow).toLowerCase().charCodeAt(0);
					flag=true;
				}else{
					v1 = parseInt(r1.data.row);
				}
				if(!flag){
					if(isNaN(r2.data.row)){
						v1= (r1.data.normRow).charCodeAt(0);
						v2= (r2.data.normRow).charCodeAt(0);
					}else{
						v2 = parseInt(r2.data.row);
					}
				}
			}else if(f=='seat'){
				v1 = st(r1.data.seat);
				v2 = st(r2.data.seat);
			}else if(f=='cat'){
				v1 = st(r1.data.cat);
				v2 = st(r2.data.cat);
			}else if(f=='aPrice'){
				v1 = st(r1.data.aPrice);
				v2 = st(r2.data.aPrice);
			}else if(f=='pPrice'){
				v1 = st(r1.data.pPrice);
				v2 = st(r2.data.pPrice);
			}else if(f=='oPrice'){
				v1 = st(r1.data.oPrice);
				v2 = st(r2.data.oPrice);
			}else if(f=='quantity'){
				v1 = st(r1.data.quantity);
				v2 = st(r2.data.quantity);
			}else if(f=='seller'){
				v1 = st(r1.data.seller);
				v2 = st(r2.data.seller);
			}
			
			return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
			
		};
		this.data.sort(direction, fn);
		if (this.snapshot && this.snapshot != this.data) {
			this.snapshot.sort(direction, fn);
		}
	};
	Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
	var grid = new Ext.ux.grid.livegrid.GridPanel({
		stateful: true,
		stateId: 'objectGrid',
		stateEvents: ['columnresize', 'show', 'hide', 'columnmove'],
		enableDragDrop : false,
		width:900,
		height:${24 + numTicketRows * 26},
		frame: false,
		store: store,
		disableSelection: true,
		cm : new Ext.grid.ColumnModel([
			{
				id: 'tBox',
				header: "X",
				width: 40,
				align: 'left',
				renderer: renderBox,
				dataIndex: ['tid', 'dupe'],
				sortable:false
			},{
				id: 'tStar',
				header: " *",
				width: 25,
				align: 'left',
				renderer: renderStar,
				sortable: false,
				resizable: false
			},{
				id: 'tQty',
				header: "<b>Qty</b>",
				dataIndex: 'quantity',
				width: 40,
				align: 'center',
				sortable:true,
				renderer: quantityRenderer
			},{
				id: 'tCat',
				header: "Cat",
				dataIndex: 'cat',
				width: 40,
				align: 'center',
				sortable:true
			},{
				id: 'tSec',
				header: "<b>Section</b>",
				dataIndex: 'numerizedSection',
				width: 100,
				align: 'center',
				sortable:true,
				renderer: sectionRenderer
			},{
				id: 'tRow',
				header: "<b>Row</b>",
				dataIndex: 'normRow',
				width: 75,
				align: 'center',
				sortable:true,
				renderer: rowRenderer            
			},{
				id: 'tSea',
				header: "Seat",
				dataIndex: 'seat',
				width: 75,
				align: 'center',
				sortable:true
			},
			/* <authz:authorize ifAnyGranted="PRIV_RETAIL"> */
				{
					id: 'tOnl1',
					header: "Online Price",
					dataIndex: 'oPrice',
					width: 75,
					align: 'right',
					renderer: moneyRenderer,
					sortable:true
				},
				{
					id: 'tPur1',
					header: "Purchase Price",
					dataIndex: 'pPrice',
					width: 75,
					align: 'right',
					renderer: purchaseRenderer,
					sortable:true
				},{
					id: 'tMarkup',
					header: "<b>Markup</b>",
					dataIndex: 'oPrice',
					width: 75,
					align: 'right',
					renderer: markupRenderer,
					sortable:true
				},
			/* </authz:authorize>
			<authz:authorize ifNotGranted="PRIV_RETAIL"> */
				{
					id: 'tWho',
					header: "Wholesale",
					dataIndex: 'aPrice',
					width: 75,
					align: 'right',
					renderer: moneyRenderer,
					sortable:true
				},
				
				{
					id: 'tOnl',
					header: "<b>Online Price</b>",
					dataIndex: 'oPrice',
					width: 75,
					align: 'right',
					renderer: moneyRenderer,
					sortable:true
				},
				{
					id: 'tPur1',
					header: "<b>Purchase Price</b>",
					dataIndex: 'pPrice',
					width: 75,
					align: 'right',
					renderer: purchaseRenderer,
					sortable:true
				},
			/* </authz:authorize> */
			<c:if test="${view == 'analytic'}">
				{
					id: 'tEval',
					header: "<b>Eval</b>",
					dataIndex: 'eval',
					width: 50,
					align: 'left',
					sortable:true
				},{
					id: 'tAction',
					header: "<b>Action</b>",
					dataIndex: 'action',
					width: 50,
					align: 'left',
					sortable:true
				},
			</c:if>
			<c:if test="${view == 'expanded'}">
				{
					id: 'tSoldQty',
					header: "<b>Sold Qty</b>",
					dataIndex: 'soldQty',
					width: 30,
					align: 'left',
					sortable:true
			    },{
					id: 'tEndDate',
					header: "<b>End Date</b>",
					dataIndex: 'endDate',
					width: 50,
					align: 'left',
					sortable:true
			    },{
					id: 'tInsDate',
					header: "<b>Ins Date</b>",
					dataIndex: 'insDate',
					width: 50,
					align: 'left',
					sortable:true
			    },{
					id: 'tTicketStatus',
					header: "<b>Status</b>",
					dataIndex: 'ticketStatus',
					width: 50,
					align: 'left',
					sortable:true
			    },
			</c:if>
			{
				id: 'tSel',
				header: "Seller",
				dataIndex: 'seller',
				width: 75,
				align: 'left',
				sortable:true,
				renderer: sellerRenderer,
			},{
				id: 'tLas',
				header: "Last Update",
				dataIndex: 'lastChange',
				width: 75,
				align: 'center',
				sortable:true
			}
		]),
		loadMask: {
			msg : 'Loading...'
		},
		view: myView
	});
	
	grid.render('ticket-grid');
	
	grid.on({
		"cellclick":{fn: onCellClick }
	});
	
	<c:if test="${multisort}">
		setTimeout("store.remoteSort = false", 2000);
	</c:if>
	$("#tab-tickets").click(function(){
		//store.loadData(tickets);
		//grid.render('ticket-grid');
		myView.refresh(true);
	});
	

});

function onCellClick(grid, rowIndex, columnIndex, e) {
	var record = this.store.getAt(rowIndex);
	if(columnIndex == 0) {
		//cell is checkbox
		e.stopEvent(); // Stops the browser context menu from showing.
		checkTicket(record.get('tid'), record);
	} else if (columnIndex == 1) {
		//cells is start
        e.stopEvent(); // Stops the browser context menu from showing.
		toggleBookmarkTicket(record.get('tid'), record);
	}
};
    
function renderBox(value, p, record){
	var explicitlyDupeSymbol = "";
	if (getGroupId(record.data.tid) != undefined) {
		explicitlyDupeSymbol = "<span style=\"color:#770\">(d" + explicitlyMarkedDisplayGroupMap[getGroupId(record.data.tid)] + ")</span>";
	} 
	  
	var dupeSymbol = "";
	if (displayGroupMap[record.data.tid] != undefined) {
		dupeSymbol = "<span style=\"color:#770\">(g" + displayGroupMap[record.data.tid] + ")</span>";
	} else if (displayGroupMap[record.data.dupRefTicketId] != undefined) {
		dupeSymbol = "<span style=\"color:#770\">(g" + displayGroupMap[record.data.dupRefTicketId] + ")</span>";
	}
	  
	if(record.data.checked) {
		return '<img class="checkbox" src="../images/checkbox-ticked.gif"/>' + dupeSymbol + explicitlyDupeSymbol;
	} else {
		return '<img class="checkbox" src="../images/checkbox-empty.gif"/>' + dupeSymbol + explicitlyDupeSymbol;
	}
};
	  
function renderStar(value, p, record){
	var starState = record.get('starStatus');
	var ticketId = record.get('tid');

	return "<div id=\"star_" + ticketId + "\" class=\"star" + starState + "\" style=\"margin-top: -4px\"></div>";
};

function toggleBookmarkEvent(eventId) {
	BookmarkDwr.toggleBookmarkEvent(eventId, 
		function(response) {
			$('#star_event_' + eventId).attr('class', 'star' + response);
		});
};

function toggleBookmarkTicket(ticketId, record) {
    	BookmarkDwr.toggleBookmarkTicket(ticketId, 
		   function(starState) {
			record.set('starStatus', starState);
			if (starState) {
				bookmarks.push(ticketId);
			} else {
				var pos = $.inArray(ticketId, bookmarks);
				if (pos >= 0) {
					bookmarks.splice(pos, 1);
				}
			}
  		   });
  };
  
  function changeShowDuplicateFilter() {
    $('#showDuplicatesInput').val($('#showDuplicates').attr('checked'));
  }
  
  
  function ajaxCallForChart()
  {
		alert("in ajax call");
		$.ajax({
			dataType:'text',
			url:'ChartData',
			cache:false,
			success: function(res){
				jsonData = jQuery.parseJSON(res);
				for (var i = 0; i < jsonData.length; i++) {
                          alert("in success");          
					var data = jsonData[i];
					var seriesName="series"+i;
					renderPriceChart(data,seriesName);
					
				} 
                                    
					
				
			
		
			 
			}
		}); 
  }
  function changeCatScheme(){
	$("#historyCatScheme").val($("#history_catScheme").val());
	$("#liquidityCatScheme").val($("#liquidity_catScheme").val());
	$("#statsCatScheme").val($("#stats_catScheme").val());
	$("#crawlsCatScheme").val($("#crawls_catScheme").val());
	$("#filtersCatScheme").val($("#filters_catScheme").val());
	
  };
  function changeCatFilters() {
    //var url = "${ticketsPage}?eventId=${event.id}";
    var categoryInput = ""
    $.each($('.cat-checkbox'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[1];  	
    		if ($(elt).attr('checked') == true) {
    		categoryInput += cat + ",";
    	}
    });    
    $('#categoryInput').val(categoryInput); 
    
    var zonecategoryInput = ""
        $.each($('.zoneCat-checkbox'), function(i, elt) {
        		var cat = $(elt).attr('id').split("-")[1];  	
        		if ($(elt).attr('checked') == true) {
        			zonecategoryInput += cat + ",";
        	}
        }); 
    $('#zoneCategoryInput').val(zonecategoryInput);

	var historyCategoryInput = ""
    $.each($('.history-cat-checkbox'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[2];  	
    		if ($(elt).attr('checked') == true) {
    		historyCategoryInput += cat + ",";
    	}
    });    
    $('#historyCategoryInput').val(historyCategoryInput);  
	//alert($('#historyCategoryInput').val());
	
	var liquidityCategoryInput = ""
    $.each($('.liquidity-cat-checkbox'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[2];  	
    		if ($(elt).attr('checked') == true) {
    		liquidityCategoryInput += cat + ",";
    	}
    });    
    $('#liquidityCategoryInput').val(liquidityCategoryInput);  
	//alert($('#liquidityCategoryInput').val());
	   
	var statsCategoryInput = ""
    $.each($('.stats-cat-checkbox'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[2];  	
    		if ($(elt).attr('checked') == true) {
    		statsCategoryInput += cat + ",";
    	}
    });    
    $('#statsCategoryInput').val(statsCategoryInput);  
	//alert($('#statsCategoryInput').val());
	
	
	
  };
   function changeCatFiltersForChart() {
	if($('#cat-ALL-For-Chart').attr('checked') == true){
		var categoryInput = ""
		$.each($('.cat-checkbox-For-Chart'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[1];  	
    		if ($(elt).attr('checked') == true || $(elt).attr('checked') == false ) {
			if(cat != "ALL"){
				categoryInput += cat + ",";
			}
    	}
		});    
		$('#categoryInputForChart').val(categoryInput); 
	}else{
		var categoryInput = ""
		$.each($('.cat-checkbox-For-Chart'), function(i, elt) {
    		var cat = $(elt).attr('id').split("-")[1];  	
    		if ($(elt).attr('checked') == true) {
    		categoryInput += cat + ",";
    	}
		});    
		$('#categoryInputForChart').val(categoryInput); 
	}
	
	
     
	//alert($('#categoryInputForChart').val());	
  };
  
  function changeQuantityFilters() {	
    var quantityString = "";
    $.each($('.quantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[1];
    	if ($(elt).attr('checked') == true) {
    	quantityString += qty + ",";
    	}
    });    
    $('#quantityInput').val(quantityString);  
    var zoneQuantityString = "";
    $.each($('.zoneQuantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[1];
    	if ($(elt).attr('checked') == true) {
    		zoneQuantityString += qty + ",";
    	}
    });
    $('#zoneQuantityInput').val(zoneQuantityString); 

	var historyQuantityString = "";
    $.each($('.history-quantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[2];
    	if ($(elt).attr('checked') == true) {
    	historyQuantityString += qty + ",";
    	}
    });    
    $('#historyQuantityInput').val(historyQuantityString);  
	//alert($('#historyQuantityInput').val());

	var liquidityQuantityInput = "";
    $.each($('.liquidity-quantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[2];
    	if ($(elt).attr('checked') == true) {
    	liquidityQuantityInput += qty + ",";
    	}
    });    
    $('#liquidityQuantityInput').val(liquidityQuantityInput);  
	//alert($('#liquidityQuantityInput').val());
	
	var statsQuantityInput = "";
    $.each($('.stats-quantity-checkbox'), function(i, elt) {
    	var qty = $(elt).attr('id').split("-")[2];
    	if ($(elt).attr('checked') == true) {
    	statsQuantityInput += qty + ",";
    	}
    });    
    $('#statsQuantityInput').val(statsQuantityInput);  
	//alert($('#statsQuantityInput').val());
	
	
	
  };
   function changeQuantityFiltersForChart() {
   if($('#quantity-ALL-For-Chart').attr('checked') == true){
		var quantityString = "";
		$.each($('.quantity-checkbox-For-Chart'), function(i, elt) {
			var qty = $(elt).attr('id').split("-")[1];
			if ($(elt).attr('checked') == true || $(elt).attr('checked') == false) {
			if(qty != "ALL"){
				quantityString += qty + ",";
			}
			
			}
		});
		
		$('#quantityInputForChart').val(quantityString);
   }else{
		var quantityString = "";
		$.each($('.quantity-checkbox-For-Chart'), function(i, elt) {
			var qty = $(elt).attr('id').split("-")[1];
			if ($(elt).attr('checked') == true) {
			quantityString += qty + ",";
			}
		});
		
		$('#quantityInputForChart').val(quantityString);
   }
    
	//alert($('#quantityInputForChart').val());
	
  };

  function checkAllSiteFilter() {
	$.each($('.site-checkbox'), function(intIndex, input) {
	  if (!$(input).attr('disabled')) {
	  	$(input).attr('checked', 'true').change();
	  }
	});
  };

  function uncheckAllSiteFilter() {
  	$('.site-checkbox').attr('checked', false);
  };
  
  function changeSiteFilters() {
      $.each($('.site-checkbox'), function(i, elt) {
    	var siteId = $(elt).attr('id').split("-")[0];
    	$('#' + siteId + "-checkbox-input").val($(elt).attr('checked'));  	
    });
  }
  
	function changeCategoryFilter(val) {
		location.href = "${ticketsPage}?eventId=${event.id}&cat=" + val + anchor;
	};

	// called by flex chart
	function storeFlexChartSelectedFieldNames(selectedFieldNames) {
		$.cookie("flexChart", selectedFieldNames, {expires: 7});
	};

	function loadFlexChartSelectedFieldNames() {
		return $.cookie("flexChart");
	};
	
function changeGroup() {
    var catGroup = $.trim($('#catScheme').val());;
    document.location.href = "${ticketsPage}?eventId=${event.id}"
        + "&catScheme=" + catGroup + anchor;
}

function changeMutisortCheckBox() {
    if ($('#multisort-checkbox').is(':checked')) {
	  $('#multisortInput').val("1");
    } else {
      $('#multisortInput').val("0");
    }
};

function changeWholesaleCheckBox() {
    if ($('#wholesale-checkbox').is(':checked')) {
	  $('#wholesaleInput').val("1");
    } else {
      $('#wholesaleInput').val("0");
    }
};



function editPriceAdjustment(siteId) {
  	var backElement = $($.DIV({id: 'back-element'})).css({
  		'top': '0px', 
  		'left': '0px', 
  		'position': 'absolute', 
  		'z-index': 10000, 
  		'opacity': '0',
  		 'background': 'black',
		'width': $('body').width(),
		'height': $('body').height()
  	});
  	$('body').append(backElement);

 	$('#price-adjustment-' + siteId).css({
 		zIndex: 10001,
 		position: 'absolute',
 		'background': '#ffffaa'
 	});

	$('#price-adjustment-span-' + siteId).hide();
	$('#price-adjustment-price-rounding-icon-' + siteId).hide();
	
 	$('#price-adjustment-link-edit-' + siteId).hide();

 	$('#price-adjustment-link-save-' + siteId).show();
 	$('#price-adjustment-link-cancel-' + siteId).show();
 	
	
	var s = $.trim($('#price-adjustment-span-' + siteId).text());
	var priceAdjustment = s.substring(0, s.length - 1);
	$('#price-adjustment-textfield-' + siteId).val(formatFloat(priceAdjustment));

	$('#price-adjustment-edit-' + siteId).show();
	$('#price-adjustment-textfield-' + siteId).focus();

	$('#price-adjustment-textfield-' + siteId).keyup(function(e) {
		if (e.keyCode == 13) {
			savePriceAdjustment(siteId);
		}
	});		

};

function savePriceAdjustment(siteId) {
	var priceAdjustment = $('#price-adjustment-textfield-' + siteId).val() * 1;
	if (isNaN(priceAdjustment)) {
		alert("Invalid price adjustment");
		return;
	}
	if (priceAdjustment < 0) {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#aa2222', 'font-weight': 'bold'});
		$('#price-adjustment-span-' + siteId).text(priceAdjustment + "%");
	} else if (priceAdjustment > 0) {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#22aa22', 'font-weight': 'bold'});
		$('#price-adjustment-span-' + siteId).text('+' + priceAdjustment + "%");
	} else {
		priceAdjustment = formatFloat(priceAdjustment);
		$('#price-adjustment-span-' + siteId).css({color: '#000000', 'font-weight': 'normal'});
		$('#price-adjustment-span-' + siteId).text(priceAdjustment + "%");
	}

	$('#price-adjustment-edit-' + siteId).hide();
 	
	$('#price-adjustment-price-rounding-icon-' + siteId).show();
	$('#price-adjustment-span-' + siteId).show();
	$('#price-adjustment-saving-' + siteId).show();
	DataDwr.changePriceAdjustment(
		${event.id}, siteId, priceAdjustment,
			function(result) {
				// refresh page
				document.location.href="${ticketsPage}?eventId=${event.id}" + anchor;				
			},
			function(error) {
				alert("error: " + error);
			}
	);
};

function cancelPriceAdjustment(siteId) {
	$('#back-element').remove();
 	$('#price-adjustment-link-edit-' + siteId).show();
 	$('#price-adjustment-link-save-' + siteId).hide();
 	$('#price-adjustment-link-cancel-' + siteId).hide();
 	
	$('#price-adjustment-price-rounding-icon-' + siteId).show();
	$('#price-adjustment-span-' + siteId).show();

	$('#price-adjustment-edit-' + siteId).hide();
	$('#price-adjustment-textfield-' + siteId).blur();	

 	$('#price-adjustment-' + siteId).css({
 		zIndex: 1,
 		position: 'static',
 		background:'#ffffff'
 	});
};

function toggleTicketListingCrawlRow(crawlId) {
	$('#ticket-listing-crawl-' + crawlId).attr('checked', !$('#ticket-listing-crawl-' + crawlId).attr('checked'));
	updateTicketListingCrawlRow(crawlId);
};

function updateTicketListingCrawlRow(crawlId) {
	if ($('#ticket-listing-crawl-' + crawlId).attr('checked')) {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', '#748C20');
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', '#ffffff');
	} else {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', null);
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', null);
	}
};

function enableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=enableCrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};
	
function disableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=disableCrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};

function forceRecrawlSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href = "EditorManageCrawls?action=forceRecrawl&crawlIds=" + ids + "&backUrl=${ticketsPage}?eventId=${event.id}";
};

function __getSelectedTicketListingCrawlIds() {
	var ids = "";
	$.each($('.ticket-listing-crawl'), function(i, elt) {
		if ($(elt).attr('checked')) {
			var id = $(elt).attr('id').split("-")[3]; 
			ids += id + ",";
		}
	});
	
	if (ids == "") {
		return null;
	}
	
	// remove the final comma
	ids = ids.substring(0, ids.length - 1)
	return ids;		
};

function selectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', true);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function unselectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', false);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function changeAddCrawler(siteId) {
	if (siteId) {
		document.location.href = "EditorEditTicketListingCrawl?action=create&siteId=" + siteId + "&tourId=${tour.id}&eventId=${event.id}&backUrl=" + escape("${ticketsPage}?eventId=${event.id}");
	}
};
var lastUpdated=new Date(parseInt($('#reload').val()));

function showRefreshedRecently() {
		refreshRecentlySpanFirstTime();
/*
	$('.refreshLink').hide();
    $('.refreshedRecently').show();
    var now = new Date();
    var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
    if (numMins >= ${1000 * ticketListingCrawler.minCrawlPeriod}) {
        refreshRecentlySpanFirstTime();
    } else {
      setTimeout(refreshRecentlySpanFirstTime, ${1000 * ticketListingCrawler.minCrawlPeriod} - numMins);
    }
    */
}

function refreshRecentlySpanFirstTime() {
	$('.refreshedRecently').hide();
    $('.refreshLink').show();
    refreshRecentlySpan();
    setInterval("refreshRecentlySpan()", 60000);
}


function refreshRecentlySpan() {
	var now = new Date();
	var numMins = parseInt((now.getTime() - lastUpdated.getTime()) / 60000);
    if (numMins == 0) {
      $('#numMinsSinceLastUpdate').text("just now");
    } else {
	  $('#numMinsSinceLastUpdate').text(numMins + "mn ago");
    }
}

var totalCrawls;

function updateCrawls(isNoCrawl) {
	if(isNoCrawl=='true'){
		var time= parseInt($('#reload').val());
		  lastUpdated = new Date(time);
          showRefreshedRecently();
	      return;
	}
    CrawlerDwr.forceRecrawlForEvent(${event.id}, function(response) {
      var tokens = response.split('|');
	  if (tokens[0] != 'OK') {
		lastUpdated = new Date();
        showRefreshedRecently();
	 } else {
	    totalCrawls = parseInt(tokens[1]);
        var runningCrawls = parseInt(tokens[2]);
	    if (totalCrawls == 0) {
		  lastUpdated = new Date(tokens[3]);
          showRefreshedRecently();
          return;
        }
      	$('.refreshLink').hide();
        $('.refreshedRecently').hide();
      	$('.refreshCrawlInfoSpan').show();
      	$('.refreshCrawlInfo').html(parseInt(100 * (runningCrawls / totalCrawls)) + "% updated");
        setTimeout("checkCrawlsFinished()", 10000);
      }
    });
  	return false;
}

function checkCrawlsFinished() {
    CrawlerDwr.getCrawlsExecutedForEventCount(${event.id}, function(response) {
      var result= response.split(":");
      var count = parseInt(result[0]) + parseInt(result[1]);
	  $('.refreshCrawlInfo').html(parseInt(100 * (count / (totalCrawls))) + "% updated");
      if(count < (totalCrawls)) {
        setTimeout("checkCrawlsFinished()", 10000);
      } else {
        // force reload (avoid caching by adding a dummy parameter)
        $('.refreshCrawlInfo').html("100% updated! Reloading page...");
        location.href = '${ticketsPage}?eventId=${event.id}&nocrawl=true&reload=' + (new Date().getTime()) + "&scroll=" + window.scrollY + anchor;
      }
    });
}

  function createTableFromTickets(ticketIdList) {
	var result = "<table class='list'>"
						+ "<thead>"
						+ "<th>Seller</th><th>qty</th><th>Section</th><th>Row</th><th>Wholesale</th><th>Online</th>"
						+ "</thead>"
						+ "<tbody>";
						
	$.each(ticketIdList, function(intIndex, ticketId) {
				  var row = getTicketRow(ticketId);
      			  result +=   '<tr>' 
      					 + '<td><img src="../images/ico-' + row[SITE_ID_INDEX] + '.gif" align="absbottom"/>' + row[TICKET_SELLER_INDEX] + '</td>'
      					 + '<td>' + row[REM_QTY_INDEX] + '</td>'
      					 + '<td>' + row[NORM_SECTION_INDEX] + '</td>'
      					 + '<td>' + row[NORM_ROW_INDEX] + '</td>'
      					 + '<td>$' + Ext.util.Format.number(row[ADJ_PRICE_INDEX], '?0,000.00') + '</td>'
      					 + '<td>$' + Ext.util.Format.number(row[CURR_PRICE_INDEX], '?0,000.00') + '</td>'
      					 + '</tr>';
	});
													
	result += "</tbody>"
	        + "</table>";
	        
	return result;
  }
  
  function markAsDuplicate() {
    if (checkedTickets.length < 2) {
      alert("You must select at least 2 tickets");
      return false;
    }
    
    var dupMap = new Array();
	
	var selectedNotDupeIdList = new Array();
	var selectedDominantDupeIdList = new Array();
	var dominatedDupeIdList = new Array();	
	
    var params = "";
    $.each(checkedTickets, function(intIndex, ticketId) {
   	  var group = getTicketRowIdsByDupRefTicketId(ticketId);
      if (group.length > 0) {
          selectedDominantDupeIdList.push(ticketId);
          dominatedDupeIdList = $.merge(dominatedDupeIdList, group);
      } else {
          selectedNotDupeIdList.push(ticketId);
      }
      
      params += ticketId + ',';
    });
    
    /*
    $.unique(dupTicketIdList);
      	// remove ticketId from tmpTickets
		dupTicketIdList = $.grep(dupTicketIdList, function(value, index) {
        	return ($.inArray(value, checkedTickets) == -1);
      	});
    */
    
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
    
    if (selectedNotDupeIdList.length == 0) {
    	alert('The tickets you selected are already the dominant tickets of duplicate tickets');
    	return false;
    }

	var selectedNotDupeListStr = createTableFromTickets(selectedNotDupeIdList);
	var selectedDominantDupeListStr = createTableFromTickets(selectedDominantDupeIdList);
	// create list of already marked duplicates
	var dominatedDupeListStr = createTableFromTickets(dominatedDupeIdList);

    var content = "<b>ARE YOU SURE YOU WANT THESE TICKETS:</b><br />"
	    	+ "<center>" + selectedNotDupeListStr + "</center><br/>";  // added tickets
	    	
	if (selectedDominantDupeIdList.length > 0) {
		    content += "<b>TO BE DUPLICATES OF THESE TICKETS:</b><br/>"
		    + "<center>" + selectedDominantDupeListStr + "</center><br/>"  // best ticket
		    
		    if (selectedDominantDupeIdList.length > 0) {
			    content += "<b>WHICH ARE DUPLICATES OF:</b></b>"
			    + "<center>" + dominatedDupeListStr + "</center><br/><br/>"  // dupTickets
		    }
			+ "<b>DO YOU REALLY WANT TO MARK THEM AS DUPLICATE?</b>";
	} else {
		content += "<b>AS DUPLICATES?</b>";
	}
	$('#markAsDuplicateDialog').dialog('option', 'title', 'Mark Tickets as Duplicate?');
	$('#markAsDuplicateDialogMessage').html(content);
	$("#markAsDuplicateDialog").dialog('open');
	$("#markAsDuplicateDialog").dialog('option', 'buttons', {
		'OK': function() {
			location.href = '${ticketsPage}?eventId=${event.id}&dupAction=create&dups=' + params;
		},
		Cancel: function() {
			$(this).dialog('close');
		}
	});
  
	return false;
  }

  function unmarkAsDuplicate() {
    if (checkedTickets.length < 1) {
      alert("You must select at least 1 ticket");
      return false;
    }
  
    var answer = confirm("Are you sure you want to unmark the selected tickets as duplicate?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each(checkedTickets, function(intIndex, ticketId) {
        params += ticketId + ','
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=delete&dups=' + params;
	return false;
  }
  
  function markAsAdmitOne() {
    if (checkedTickets.length < 1) {
      alert("You must select at least 1 tickets");
      return false;
    }
  
    var answer = confirm("Are you sure you want to mark the selected tickets as AdmitOne tickets?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each(checkedTickets, function(intIndex, ticketId) {
        params += ticketId + ','
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=admit&dups=' + params;
	return false;
  }

  function unmarkAsAdmitOne() {
    if (checkedTickets.length < 1) {
      alert("You must select at least 1 ticket");
      return false;
    }
  
    var answer = confirm("Are you sure you want to unmark the selected tickets as AdmitOne tickets?");
    if (!answer) {
      return false;
    }
    
    var params = "";
    $.each(checkedTickets, function(intIndex, ticketId) {
        params += ticketId + ','
    });
    if (params.length > 0) {
      params = params.substring(0, params.length - 1);
    }
	location.href = '${ticketsPage}?eventId=${event.id}&dupAction=unadmit&dups=' + params;
	return false;
  }

  function getGroupId(ticketId) {
       return duplicateTicketMap[ticketId];
  }

  function getDuplicateTicketsByGroupId(groupId) {
  	   var result = new Array();
  	   
       var numDups = 0;
       for (var i in duplicateTicketMap) {
         var value = duplicateTicketMap[i];
         if (value == groupId) {
           result.push(i);
         }
       }
	   
	   return result;
  }

  function getNumDuplicateTicketsByGroupId(groupId) {
       var numDups = 0;
       for (var i in duplicateTicketMap) {
         var value = duplicateTicketMap[i];
         if (value == groupId) {
           numDups++
         }
       }
  }

  function getNumTickedDuplicateTicketsByGroupId(groupId) {
       var numDups = 0;
       for (var i in duplicateTicketMap) {
         var value = duplicateTicketMap[i];
         if (value == groupId) {
	       if ($.inArray(i, checkedTickets) > -1) {
             numDups++
	       }
         }
       }

	   return numDups;
  }

  function getTicketRowIdsByDupRefTicketId(ticketId) {
  
    var result = new Array();
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      if (ticket[TICKET_DUP_REF_ID_INDEX] == ticketId) {
        result.push(ticket[TICKET_ID_INDEX]);
      }
    }
  	return result;
  }
  
  function getTicketRowsByDupRefTicketId(ticketId) {
    var result = new Array();
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      if (ticket[TICKET_DUP_REF_ID_INDEX] == ticketId) {
        result.push(ticket);
      }
    }
  	return result;
  }
  
  function getTicketRowIndex(ticketId) {
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      if (ticket[0] == ticketId) {
        return i;
      }
    }
    return -1;
  }

  function getTicketRow(ticketId) {
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      if (ticket[0] == ticketId) {
        return tickets[i];
      }
    }
    return null;
  }
  
  function tickDuplicateTicket(ticketId) {
     // if you just tick a ticket which has duplicate, tick the other tickets
     var groupId = getGroupId(ticketId);
     if (getNumTickedDuplicateTicketsByGroupId(groupId) == 1) {
	   groupId = getGroupId(ticketId);
	   if (groupId == null) {
	     return;
	   }
	   
	   for (var ticketId in duplicateTicketMap) {
	     if (duplicateTicketMap[ticketId] == groupId) {
	       $('#ticket_' + ticketId).attr('checked', true);
	       if ($.inArray(ticketId, checkedTickets) < 0) {
	         checkedTickets.push(ticketId);
	       }
	       var index =  getTicketRowIndex(ticketId);
	       if (index != 1) {
	         ticketRows[index][0] = "<input id='ticket_" + ticketId + "' type='checkbox' class='ticket_checkbox' onchange='checkTicket(" + ticketId + ")' checked/>";
	       }
	     }
	   }
	 }
  }

function toggleSystemStats() {
	if ($('#system-stats-info').css('display') == 'none') {
		$('#system-stats-info').show();
		$('#system-stats-expand-img').attr('src', '../images/ico-collapse.gif');
	} else {
		$('#system-stats-info').hide();
		$('#system-stats-expand-img').attr('src', '../images/ico-expand.gif');
	}
};

function toggleTicketFilter() {
	var expanded;
	if ($('#ticket-filter-info').css('display') == 'none') {
		$('#ticket-filter-info').show();
		$('#ticket-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#ticket-filter-info').hide();
		$('#ticket-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
function toggleZoneFilter() {
	var expanded;
	if ($('#zone-filter-info').css('display') == 'none') {
		$('#zone-filter-info').show();
		$('#zone-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#zone-filter-info').hide();
		$('#zone-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
function toggleHistoryFilter() {
	var expanded;
	if ($('#history-filter-info').css('display') == 'none') {
		$('#history-filter-info').show();
		$('#history-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#history-filter-info').hide();
		$('#history-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};

function toggleLiquidityFilter() {
	var expanded;
	if ($('#liquidity-filter-info').css('display') == 'none') {
		$('#liquidity-filter-info').show();
		$('#liquidity-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#liquidity-filter-info').hide();
		$('#liquidity-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};

function toggleStatsFilter() {
	var expanded;
	if ($('#stats-filter-info').css('display') == 'none') {
		$('#stats-filter-info').show();
		$('#stats-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#stats-filter-info').hide();
		$('#stats-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
function toggleCrawlsFilter() {
	var expanded;
	if ($('#crawls-filter-info').css('display') == 'none') {
		$('#crawls-filter-info').show();
		$('#crawls-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#crawls-filter-info').hide();
		$('#crawls-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
function togglefiltersFilter() {
	var expanded;
	if ($('#filters-filter-info').css('display') == 'none') {
		$('#filters-filter-info').show();
		$('#filters-filter-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#filters-filter-info').hide();
		$('#filters-filter-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
function toggleTicketFilterForChart() {
	var expanded;
	if ($('#ticket-filter-chart-info').css('display') == 'none') {
		$('#ticket-filter-chart-info').show();
		$('#ticket-filter-chart-expand-img').attr('src', '../images/ico-collapse.gif');
		expanded=true;
	} else {
		$('#ticket-filter-chart-info').hide();
		$('#ticket-filter-chart-expand-img').attr('src', '../images/ico-expand.gif');
		expanded=false;
	}
	BrowseTicketDwr.setTicketMinimizeFilter(expanded);
};
	  
function toggleEventDetails() {
	if ($('#event-details-info').css('display') == 'none') {
		$('#event-details-info').show();
		$('#event-details-expand-img').attr('src', '../images/ico-collapse.gif');
	} else {
		$('#event-details-info').hide();
		$('#event-details-expand-img').attr('src', '../images/ico-expand.gif');
	}
};
var customers = [
	<c:forEach var="customer" items="${customers}">
		'${customer.name}', 
	</c:forEach>
	''
];
$('#quoteCustomer').autocomplete(customers);
function addSelectedTicketsToQuotes() {
	var ticketIds = "";
	$.each(checkedTickets, function(intIndex, ticketId) {
		ticketIds += ticketId + ",";
	});

			
	
	var groupIds = "";
	var all = document.getElementsByName("select");			
			 for(var i= 0;i<all.length;i++){
			 	if(all[i].checked){
			 		groupIds += all[i].value + ","  ;			 					 		
			 	}
			}
			 
	 if (ticketIds == '' && groupIds=='') {
					alert('Please tick at least one ticket or zone');
					return false;
	 }			 
		
	$("#dialogAdd").dialog('open');
	$("#dialogAdd").dialog('option', 'buttons', {
		'OK': function() {
			var customerName = $('#quoteCustomer').val();
			var refferal = $('#refferal').val();
				var quoteDate = $('#quoteDate').val();
				if (customerName == '') {
					alert('Please enter a customer name');
					$( this ).dialog( "close" );
					return false;
				}
				DataDwr.addTicketsAndZonesToQuotes(ticketIds, groupIds, customerName, quoteDate, markupPercentAddition,'true',null,refferal, function(response) {
				checkedTickets = new Array();
					for (var i = 0; i < store.getCount(); i++) {
						store.getAt(i).set('checked', false);
					} 					
					alert(response);
					$("#dialogAdd").dialog('close');
					
					});
		},
		Cancel: function() {
			$(this).dialog('close');
		}
	});
};

function addSelectedTicketsToAppendQuotes() {
	var ticketIds = "";
	$.each(checkedTickets, function(intIndex, ticketId) {
		ticketIds += ticketId + ",";
	});
	var groupIds = "";	
	var all = document.getElementsByName("select");			
			 for(var i= 0;i<all.length;i++){
			 	if(all[i].checked){
			 		groupIds +=  all[i].value + "," ;			 					 		
			 	}
			}

	if (ticketIds == '' && groupIds=='') {
		alert('Please tick at least one ticket or zone');
		return false;
	}		
	
	var quoteDate = $('#quoteDate').val();
	window.open("ListQuotes?ticketIds="+ticketIds+"&groupIds="+groupIds+"&quoteDate="+quoteDate+"&markup="+markupPercentAddition+"&action=append",'','scrollbars=yes,menubar=no,height=800,width=600,resizable=yes,toolbar=no,location=no,status=no');	
	
};
function sendAppendRequest(quoteId,cust,type){
	 
		sendTicketsAndZonesAppendRequest(quoteId,cust);
}
function sendTicketsAndZonesAppendRequest(quoteId,cust){
var ticketIds = "";
	$.each(checkedTickets, function(intIndex, ticketId) {
		ticketIds += ticketId + ",";
	});
	var groupIds = "";	
	var all = document.getElementsByName("select");			
			 for(var i= 0;i<all.length;i++){
			 	if(all[i].checked){
			 		groupIds +=  all[i].value + "," ;			 					 		
			 	}
			}
	if (ticketIds == '' && groupIds=='') {
		alert('Please tick at least one ticket or zone');
		return false;
	}	
	var customerName = cust;
	var quoteDate = $('#quoteDate').val();
DataDwr.addTicketsAndZonesToQuotes(ticketIds,groupIds,customerName,quoteDate, markupPercentAddition,'false',quoteId,'', function(response) {
		
		alert(response);
		});
};

function updateMarkupPrice(percent) {
   if ($.trim(percent).length == 0 || isNaN(percent)) {
     alert("Please provide a number (e.g., 20)");
     return;
   }
   markupPercentAddition = parseFloat(percent);
   myView.refresh(true);
};
	
function selectQuantityGreaterThan4() {
	$('#quantity-ALL').attr('checked', false);
	$('#quantity-1').attr('checked', false);
	$('#quantity-2').attr('checked', false);
	$('#quantity-3').attr('checked', false);
	$('#quantity-4').attr('checked', true);
	$('#quantity-5').attr('checked', true);
	$('#quantity-6').attr('checked', true);
	$('#quantity-7').attr('checked', true);
	$('#quantity-8').attr('checked', true);
	$('#quantity-9').attr('checked', true);
	$('#quantity-10').attr('checked', true);
	$('#quantity-11').attr('checked', true);
	
	$('#zoneQuantity-ALL').attr('checked', false);
	$('#zoneQuantity-1').attr('checked', false);
	$('#zoneQuantity-2').attr('checked', false);
	$('#zoneQuantity-3').attr('checked', false);
	$('#zoneQuantity-4').attr('checked', true);
	$('#zoneQuantity-5').attr('checked', true);
	$('#zoneQuantity-6').attr('checked', true);
	$('#zoneQuantity-7').attr('checked', true);
	$('#zoneQuantity-8').attr('checked', true);
	$('#zoneQuantity-9').attr('checked', true);
	$('#zoneQuantity-10').attr('checked', true);
	$('#zoneQuantity-11').attr('checked', true);
};

function selectHistoryQuantityGreaterThan4() {
	$('#history-quantity-ALL').attr('checked', false);
	$('#history-quantity-1').attr('checked', false);
	$('#history-quantity-2').attr('checked', false);
	$('#history-quantity-3').attr('checked', false);
	$('#history-quantity-4').attr('checked', true);
	$('#history-quantity-5').attr('checked', true);
	$('#history-quantity-6').attr('checked', true);
	$('#history-quantity-7').attr('checked', true);
	$('#history-quantity-8').attr('checked', true);
	$('#history-quantity-9').attr('checked', true);
	$('#history-quantity-10').attr('checked', true);
	$('#history-quantity-11').attr('checked', true);
};

function selectLiquidityQuantityGreaterThan4() {
	$('#liquidity-quantity-ALL').attr('checked', false);
	$('#liquidity-quantity-1').attr('checked', false);
	$('#liquidity-quantity-2').attr('checked', false);
	$('#liquidity-quantity-3').attr('checked', false);
	$('#liquidity-quantity-4').attr('checked', true);
	$('#liquidity-quantity-5').attr('checked', true);
	$('#liquidity-quantity-6').attr('checked', true);
	$('#liquidity-quantity-7').attr('checked', true);
	$('#liquidity-quantity-8').attr('checked', true);
	$('#liquidity-quantity-9').attr('checked', true);
	$('#liquidity-quantity-10').attr('checked', true);
	$('#liquidity-quantity-11').attr('checked', true);
};

function selectStatsQuantityGreaterThan4() {
	$('#stats-quantity-ALL').attr('checked', false);
	$('#stats-quantity-1').attr('checked', false);
	$('#stats-quantity-2').attr('checked', false);
	$('#stats-quantity-3').attr('checked', false);
	$('#stats-quantity-4').attr('checked', true);
	$('#stats-quantity-5').attr('checked', true);
	$('#stats-quantity-6').attr('checked', true);
	$('#stats-quantity-7').attr('checked', true);
	$('#stats-quantity-8').attr('checked', true);
	$('#stats-quantity-9').attr('checked', true);
	$('#stats-quantity-10').attr('checked', true);
	$('#stats-quantity-11').attr('checked', true);
};

function selectCrawlsQuantityGreaterThan4() {
	$('#crawls-quantity-ALL').attr('checked', false);
	$('#crawls-quantity-1').attr('checked', false);
	$('#crawls-quantity-2').attr('checked', false);
	$('#crawls-quantity-3').attr('checked', false);
	$('#crawls-quantity-4').attr('checked', true);
	$('#crawls-quantity-5').attr('checked', true);
	$('#crawls-quantity-6').attr('checked', true);
	$('#crawls-quantity-7').attr('checked', true);
	$('#crawls-quantity-8').attr('checked', true);
	$('#crawls-quantity-9').attr('checked', true);
	$('#crawls-quantity-10').attr('checked', true);
	$('#crawls-quantity-11').attr('checked', true);
};
function selectfiltersQuantityGreaterThan4() {
	$('#filters-quantity-ALL').attr('checked', false);
	$('#filters-quantity-1').attr('checked', false);
	$('#filters-quantity-2').attr('checked', false);
	$('#filters-quantity-3').attr('checked', false);
	$('#filters-quantity-4').attr('checked', true);
	$('#filters-quantity-5').attr('checked', true);
	$('#filters-quantity-6').attr('checked', true);
	$('#filters-quantity-7').attr('checked', true);
	$('#filters-quantity-8').attr('checked', true);
	$('#filters-quantity-9').attr('checked', true);
	$('#filters-quantity-10').attr('checked', true);
	$('#filters-quantity-11').attr('checked', true);
};

function selectQuantityGreaterThan4ForChart() {
	$('#quantity-ALL-For-Chart').attr('checked', false);
	$('#quantity-1-For-Chart').attr('checked', false);
	$('#quantity-2-For-Chart').attr('checked', false);
	$('#quantity-3-For-Chart').attr('checked', false);
	$('#quantity-4-For-Chart').attr('checked', true);
	$('#quantity-5-For-Chart').attr('checked', true);
	$('#quantity-6-For-Chart').attr('checked', true);
	$('#quantity-7-For-Chart').attr('checked', true);
	$('#quantity-8-For-Chart').attr('checked', true);
	$('#quantity-9-For-Chart').attr('checked', true);
	$('#quantity-10-For-Chart').attr('checked', true);
	$('#quantity-11-For-Chart').attr('checked', true);
};

	
function selectQuantityNone() {
	$('#quantity-ALL').attr('checked', false);
	$('#zoneQuantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#quantity-' + i).attr('checked', false);
		$('#zoneQuantity-' + i).attr('checked', false);
	}
};	

function selectHistoryQuantityNone() {
	$('#history-quantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#history-quantity-' + i).attr('checked', false);
	}
};	

function selectLiquidityQuantityNone() {
	$('#liquidity-quantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#liquidity-quantity-' + i).attr('checked', false);
	}
};	

function selectStatsQuantityNone() {
	$('#stats-quantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#stats-quantity-' + i).attr('checked', false);
	}
};	

function selectCrawlsQuantityNone() {
	$('#crawls-quantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#crawls-quantity-' + i).attr('checked', false);
	}
};	
function selectfiltersQuantityNone() {
	$('#filters-quantity-ALL').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#filters-quantity-' + i).attr('checked', false);
	}
};	

function selectQuantityNoneForChart() {
	$('#quantity-ALL-For-Chart').attr('checked', false);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#quantity-' + i + '-For-Chart').attr('checked', false);
	}
};
	
function selectQuantityAll() {
	$('#quantity-ALL').attr('checked', true);
	$('#zoneQuantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#quantity-' + i).attr('checked', true);
		$('#zoneQuantity-' + i).attr('checked', true);
	}
};

function selectHistoryQuantityAll() {
	$('#history-quantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#history-quantity-' + i).attr('checked', true);
	}
};	

function selectLiquidityQuantityAll() {
	$('#liquidity-quantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#liquidity-quantity-' + i).attr('checked', true);
	}
};	

function selectStatsQuantityAll() {
	$('#stats-quantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#stats-quantity-' + i).attr('checked', true);
	}
};	

function selectCrawlsQuantityAll() {
	$('#crawls-quantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#crawls-quantity-' + i).attr('checked', true);
	}
};	
function selectfiltersfiltersQuantityAll() {
	$('#filters-quantity-ALL').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#filters-quantity-' + i).attr('checked', true);
	}
};	

function selectQuantityAllForChart() {
	$('#quantity-ALL-For-Chart').attr('checked', true);
	for(var i = 1 ; i <= 11 ; i++) {
		$('#quantity-' + i + '-For-Chart').attr('checked', true);
	}
};	

function selectCategoryNone() {
	$('.cat-checkbox').attr('checked', false);	
	$('.zoneCat-checkbox').attr('checked', false);
};
function selectHistoryCategoryNone() {
	$('.history-cat-checkbox').attr('checked', false);	
};
function selectLiquidityCategoryNone() {
	$('.liquidity-cat-checkbox').attr('checked', false);	
};
function selectStatsCategoryNone() {
	$('.stats-cat-checkbox').attr('checked', false);	
};
function selectCrawlsCategoryNone() {
	$('.stats-cat-checkbox').attr('checked', false);	
};
function selectFiltersCategoryNone() {
	$('.filters-cat-checkbox').attr('checked', false);	
};

function selectCategoryNoneForChart() {
	$('.cat-checkbox-For-Chart').attr('checked', false);	
};

function selectCategoryAll() {
	$('.cat-checkbox').attr('checked', true);
	$('.zoneCat-checkbox').attr('checked', true);
};

function selectHistoryCategoryAll() {
	$('.history-cat-checkbox').attr('checked', true);	
};

function selectLiquidityCategoryAll() {
	$('.liquidity-cat-checkbox').attr('checked', true);	
};

function selectStatsCategoryAll() {
	$('.stats-cat-checkbox').attr('checked', true);	
};

function selectCrawlsCategoryAll() {
	$('.crawls-cat-checkbox').attr('checked', true);	
};

function selectfiltersCategoryAll() {
	$('.filters-cat-checkbox').attr('checked', true);	
};

function selectCategoryAllForChart() {
	$('.cat-checkbox-For-Chart').attr('checked', true);	
};


function selectCategorized() {
    $('#cat-ALL').removeAttr('checked');
	$('#cat-UNCATS').removeAttr('checked');
	$('#cat-CATS').attr('checked', true);	
	$('.cat-specific').attr('checked', true);
};
function selectHistoryCategorized() {
    $('#history-cat-ALL').removeAttr('checked');
	$('#history-cat-UNCATS').removeAttr('checked');
	$('#history-cat-CATS').attr('checked', true);	
	$('.history-cat-specific').attr('checked', true);
};
function selectLiquidityCategorized() {
    $('#liquidity-cat-ALL').removeAttr('checked');
	$('#liquidity-cat-UNCATS').removeAttr('checked');
	$('#liquidity-cat-CATS').attr('checked', true);	
	$('.liquidity-cat-specific').attr('checked', true);
};
function selectStatsCategorized() {
    $('#stats-cat-ALL').removeAttr('checked');
	$('#stats-cat-UNCATS').removeAttr('checked');
	$('#stats-cat-CATS').attr('checked', true);	
	$('.stats-cat-specific').attr('checked', true);
};
function selectCrawlsCategorized() {
    $('#crawls-cat-ALL').removeAttr('checked');
	$('#crawls-cat-UNCATS').removeAttr('checked');
	$('#crawls-cat-CATS').attr('checked', true);	
	$('.crawls-cat-specific').attr('checked', true);
};
function selectfiltersCategorized() {
    $('#filters-cat-ALL').removeAttr('checked');
	$('#filters-cat-UNCATS').removeAttr('checked');
	$('#filters-cat-CATS').attr('checked', true);	
	$('.filters-cat-specific').attr('checked', true);
};


function selectUncategorized() {
	$('#cat-ALL').removeAttr('checked');
	$('#cat-CATS').removeAttr('checked');
	$('#cat-UNCATS').attr('checked', true);
	$('.cat-specific').removeAttr('checked');	
};

function selectHistoryUncategorized() {
	$('#history-cat-ALL').removeAttr('checked');
	$('#history-cat-CATS').removeAttr('checked');
	$('#history-cat-UNCATS').attr('checked', true);
	$('.history-cat-specific').removeAttr('checked');	
};
function selectLiquidityUncategorized() {
	$('#liquidity-cat-ALL').removeAttr('checked');
	$('#liquidity-cat-CATS').removeAttr('checked');
	$('#liquidity-cat-UNCATS').attr('checked', true);
	$('.liquidity-cat-specific').removeAttr('checked');	
};
function selectStatsUncategorized() {
	$('#stats-cat-ALL').removeAttr('checked');
	$('#stats-cat-CATS').removeAttr('checked');
	$('#stats-cat-UNCATS').attr('checked', true);
	$('.stats-cat-specific').removeAttr('checked');	
};
function selectCrawlsUncategorized() {
	$('#crawls-cat-ALL').removeAttr('checked');
	$('#crawls-cat-CATS').removeAttr('checked');
	$('#crawls-cat-UNCATS').attr('checked', true);
	$('.crawls-cat-specific').removeAttr('checked');	
};
function selectfiltersUncategorized() {
	$('#filters-cat-ALL').removeAttr('checked');
	$('#filters-cat-CATS').removeAttr('checked');
	$('#filters-cat-UNCATS').attr('checked', true);
	$('.filters-cat-specific').removeAttr('checked');	
};
	
var customers = [
	<c:forEach var="customer" items="${customers}">
		'${customer.name}', 
	</c:forEach>
	''
];
	
function loadImage(eventId, catScheme) {
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};

function untickAll() {
  	checkedTickets = [];
  	
  	store.data.items[0].data.checked
  	$.each(store.data.items, function(i, record) {
  		record.data.checked = false;
  	});
	$('.checkbox').attr('src', '../images/checkbox-empty.gif');
};

$('#markupPercent').val(markupPercentAddition);

function isInt(x) {
	  var y=parseInt(x);
	  if (isNaN(y)) return false;
	   return x==y && x.toString()==y.toString();
	}
	function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
	function checkPrice(price) {
	       var val=$.trim($('#'+price).val());	  
		   if(val ==''){
			 return true;
		   }else{
			  var num =isNumber(val);
			  if(!num){
			     alert('Please enter valid price');
				 $('#'+price).val('');
				 return false;
			  }else{
				return true;
			  }
		   }   
	}
</script>

