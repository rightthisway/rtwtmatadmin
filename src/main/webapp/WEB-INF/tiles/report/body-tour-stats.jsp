<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:set var="allSiteIdSet" value="" />
<c:forEach var="siteId" items="${constants.siteIds}">
	<c:set var="allSiteIdSet" value="${allSiteIdSet}&${siteId}_checkbox=true" />
</c:forEach>    

<c:choose>
	<c:when test="${criteria == 'site'}">
		<h1>Number of tickets indexed by the crawler by site</h1>
	</c:when>
	<c:when test="${criteria == 'quantity'}">
		<h1>Number of ticket listing indexed by the crawler by quantity</h1>
	</c:when>
</c:choose>

<script type="text/javascript">
	function changeRemoveDuplicatesFilter(checked) {
		document.location.href="?removeDuplicates=" + (checked?"1":"0"); 	
	};
	
	function refreshCache() {
		document.location.href="?removeDuplicates=${removeDuplicates}&action=refreshCache";
	};
</script>

<c:choose>
	<c:when test="${refreshingTourStatCache}">
		<img src="../images/process-running.gif" align="absbottom" /> Stat is being refreshed (${progressPercent}%).
	</c:when>
	<c:otherwise>
		
		<c:choose>
			<c:when test="${not emptyStatCache}">
				The stats were processed
				<c:choose>
					<c:when test="${lastUpdateDelay < 60 * 1000}"><fmt:formatNumber pattern="0" value="${lastUpdateDelay / 1000}" /> seconds</c:when>
					<c:when test="${lastUpdateDelay < 60 * 60 * 1000}"><fmt:formatNumber pattern="0" value="${lastUpdateDelay / (60 * 1000)}" /> minutes</c:when>
					<c:when test="${lastUpdateDelay < 24 * 60 * 60 * 1000}"><fmt:formatNumber pattern="0" value="${lastUpdateDelay / (60 * 60 * 1000)}" /> hours</c:when>
					<c:otherwise><fmt:formatNumber pattern="0" value="${lastUpdateDelay / (24 * 60 * 60 * 1000)}" /> days</c:otherwise>
				</c:choose>
				ago.
				<%-- display the button after 10mn since the last update to prevent abuses --%>
				<c:if test="${lastUpdateDelay > 10 * 60 * 1000}">
					<input type="button" class="medbutton" value="Refresh stats" onclick="refreshCache()"/>
				</c:if>	
			</c:when>
			<c:otherwise>
				The stat cache is empty.
				<input type="button" class="medbutton" value="Compute stats" onclick="refreshCache()"/>	
			</c:otherwise>	
		</c:choose>
	</c:otherwise>
</c:choose>
<br /><br />
<i>Note: when the stats are being processed, you can browse the other pages and the processing of the stats will not be interrupted.
<br />As the processing of stats is very database intensive, we limit the processing of stats to at most once every 10 minutes.</i>
<br /><br />
<c:choose>
	<c:when test="${emptyStatCache}">
		<c:choose>
			<c:when test="${refreshingTourStatCache}">
				The stat cache is empty and is currently being processed. 
			</c:when>
		</c:choose>
	</c:when>
	<c:otherwise>
	
		<b>Stat computation:</b><input id="removeDuplicatesCheckbox" onchange="changeRemoveDuplicatesFilter(this.checked);" type="checkbox" <c:if test="${removeDuplicates}">checked</c:if> /><label for="removeDuplicatesCheckbox">Remove duplicates</label><br /><br />
		
		<b>Number of tours/seasons: </b>${fn:length(tours)}<br />
		<b>Number of events: </b>${fn:length(eventStats)}<br />
		<br />
		<b>Number of entries: </b>${ticketEntryCount}<br />
		<b>Number of tickets: </b>${ticketCount}
		<br />
		<br />
		<a href="javascript:collapseAllTours();"><img align="absbottom" src="../images/ico-collapse.gif" />Collapse all tours/seasons</a>
		-
		<a href="javascript:expandAllTours();"><img align="absbottom" src="../images/ico-expand.gif" />Expand all tours/seasons</a>
		<br /><br />

    	<c:set var="statsTable" scope="request">	
			<table class="list">
				<thead>
					<tr>
						<th></th>
						<c:forEach var="columnKey" items="${columnKeys}">
							<th>
								<c:choose>
									<c:when test="${criteria == 'site'}">
										<img align="absbottom" src="../images/ico-${columnKey}.gif" title="${columnKey}" />
									</c:when>
									<c:otherwise>
										${columnKey}
									</c:otherwise>
								</c:choose>
							</th>
						</c:forEach>
						<th>Total</th>
					</tr>	
				</thead>
			
				<c:forEach var="tour" items="${tours}">
					<tbody>
						<tr class="odd">
							<td>
								<b>
									<a onclick="toggleTourTBody(${tour.id})" style="cursor: pointer">
										<img id="tour-expand-img-${tour.id}" src="../images/ico-expand.gif" />
										(${fn:length(tour.events)})						
										${tour.name}
									</a>
									<a href="BrowseEvents?tourId=${tour.id}">(go)</a>
								</b>					
							</td>
							<c:set var="total" value="0" />
							<c:forEach var="columnKey" items="${columnKeys}">
								<c:choose>
									<c:when test="${empty tourStats[tour.id][columnKey] or tourStats[tour.id][columnKey] == 0}">
										<td align="right"><b>-</b></td>
									</c:when>
									<c:otherwise>
										<td align="right"><b>${tourStats[tour.id][columnKey]}</b></td>
									</c:otherwise>
								</c:choose>
								<c:set var="total" value="${total + tourStats[tour.id][columnKey]}" />
							</c:forEach>
							<td align="right"><b>${total}</b></td>
						</tr>
					</tbody>
								
					<tbody id="tour-tbody-${tour.id}" style="display:none" class="tour-tbody">
						<c:forEach var="event" items="${tour.events}">
							<c:set var="total" value="0" />
							<tr>
								<td>
									<img src="../images/ico-branch.gif" />
									${event.name} - 
									<fmt:formatDate value="${event.date}" pattern="yyyy/MM/dd" /> - ${event.formattedVenueDescription}
									<a href="EventStat?eventId=${event.id}">(stats)</a>
									<a href="BrowseTickets?eventId=${event.id}">(go)</a>
								</td>
								<c:forEach var="columnKey" items="${columnKeys}">
									<c:choose>
										<c:when test="${empty eventStats[event.id][columnKey] or eventStats[event.id][columnKey] == 0}">
											<td align="right">-</td>
										</c:when>
										<c:otherwise>
											<td align="right">
												<c:choose>
													<c:when test="${criteria == 'site'}">
														<a href="BrowseTickets?eventId=${event.id}&${columnKey}_checkbox=true&reset=1&uncheckSiteFilters=1">
													</c:when>
													<c:otherwise>
														<a href="BrowseTickets?eventId=${event.id}&${columnParamName}=${columnKey}&reset=1">										
													</c:otherwise>
												</c:choose>
													${eventStats[event.id][columnKey]}
												</a>
											</td>
										</c:otherwise>
									</c:choose>
									<c:set var="total" value="${total + eventStats[event.id][columnKey]}" />
								</c:forEach>		
								<td align="right">
									<a href="BrowseTickets?eventId=${event.id}&reset=1${allSiteIdSet}"><b>${total}</b></a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</c:forEach>
				<tfoot>
					<tr>
						<td align="right">
							<b>Total</b>
						</td>
						<c:set var="total" value="0" />
						<c:forEach var="columnKey" items="${columnKeys}">
							<td><b>${totalStats[columnKey]}</b></td>
							<c:set var="total" value="${total + totalStats[columnKey]}" />
						</c:forEach>
						<td><b>${total}</b></td>
					</tr>
				</tfoot>
			</table>
		</c:set>
		
		<%
			// remove extra whitespaces of the table
			request.setAttribute("statsTable", com.admitone.tmat.utils.TextUtil.removeExtraWhitespaces((String)request.getAttribute("statsTable")));			
		%>
		
		${statsTable}
		
		<br />
		<a href="javascript:collapseAllTours();"><img align="absbottom" src="../images/ico-collapse.gif" />Collapse all tours/seasons</a>
		-
		<a href="javascript:expandAllTours();"><img align="absbottom" src="../images/ico-expand.gif" />Expand all tours/seasons</a>
	</c:otherwise>
</c:choose>

<script type="text/javascript">

	function collapseAllTours() {
		$.each($('.tour-tbody'), function(i, elt) {
			var tourId = $(elt).attr("id").split("-")[2];
			$('#tour-tbody-' + tourId).hide();
			$('#tour-expand-img-' + tourId).attr('src', '../images/ico-expand.gif');			
		});
	};

	function expandAllTours() {
		$.each($('.tour-tbody'), function(i, elt) {
			var tourId = $(elt).attr("id").split("-")[2];
			$('#tour-tbody-' + tourId).show();
			$('#tour-expand-img-' + tourId).attr('src', '../images/ico-collapse.gif');			
		});
	};

	function toggleTourTBody(tourId) {
		if ($('#tour-tbody-' + tourId).css('display') == 'none') {
			$('#tour-tbody-' + tourId).show();
			$('#tour-expand-img-' + tourId).attr('src', '../images/ico-collapse.gif');
		} else {
			$('#tour-tbody-' + tourId).hide();
			$('#tour-expand-img-' + tourId).attr('src', '../images/ico-expand.gif');
		}
	};
</script>
