<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:if test="${param.bodyonly != '1'}">
	<div id="breadCrumbPath" class="breadCrumbPathAdmin">
	  <a href="..">Home</a> 
	  &gt; Manage Stats
	</div>
</c:if>

<h1>Event Stat</h1>

<h2>Event details</h2>
<div style="float:left;  margin-bottom:10px;">
	<c:if test="${event.eventStatus == 'EXPIRED'}"><font color="red"><b>EXPIRED</b></font><br /></c:if>
	<b>${event.artist.tourLabel}:</b> <a href="BrowseEvents?tourId=${event.artist.id}">${event.artist.name}</a> <br />
	<b>Name:</b> ${event.name}<br />
	<b>${event.artist.artistLabel}:</b> <a href="BrowseTours?artistId=${event.artist.artist.id}">${event.artist.artist.name}</a>
	<div>
	  <div style="float:left"><b>Event Date:</b></div>
      <div id="star_event_${param.eventId}" class="starOff" style="margin-top: -4px" onclick="toggleBookmarkEvent(${param.eventId})"></div>
	  <div style="float:left">
        <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
		<c:choose>
		  <c:when test="${not empty event.time}">
		    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
		  </c:when>
		  <c:otherwise>
		    TBD
		  </c:otherwise>
		</c:choose>
	  </div>
	</div>
	<div style="clear:both"></div>
	<br />
	<b>Venue:</b> ${event.venue.building}<br />
	<b>Venue City:</b> ${event.venue.city}<br />
	<b>Venue State:</b> ${event.venue.state}<br />
	<b>Venue Country:</b> ${event.venue.country}<br />
</div>

<div style="clear:both"></div>

<h2>Stats</h2>

<script type="text/javascript">
	function changeRemoveDuplicatesFilter(checked) {
		document.location.href="EventStat?eventId=${param.eventId}&removeDuplicates=" + (checked?"1":"0"); 	
	};
</script>

<b>Stat computation:</b><input id="removeDuplicatesCheckbox" onchange="changeRemoveDuplicatesFilter(this.checked);" type="checkbox" <c:if test="${removeDuplicates}">checked</c:if> /><label for="removeDuplicatesCheckbox">Remove duplicates</label><br /><br />

<b>Number of entries: </b>${ticketEntryCount}<br />
<b>Number of tickets: </b>${ticketCount}

<h2>Number of tickets per zone and site</h2>
  Zone Group:
  ${catGroupName}
<select id="catScheme">
  <c:forEach var="catg" items="${catGroups}">
    <option value="${catg}">${catg}</option>
  </c:forEach>
</select>
   <input id="changeGroupButton" type="button" class="medButton" onclick="changeGroup()"  value="Change"/><br/><br/>

<table class="list">
	<thead>
	<tr>
		<th></th>		
		<c:forEach var="siteId" items="${constants.siteIds}">
			<th>
				<a href="BrowseTickets?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1">
					<img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" />
				</a>
			</th>
		</c:forEach>
		<th>Total</th>
		<th>%</th>
	</tr>	
	</thead>
	<tbody>
		<c:set var="row" value="1" />				
		<c:forEach var="category" items="${event.artist.categories}">
			<c:choose>
				<c:when test="${(row % 2) == 0}">
					<c:set var="rowClass" value="even" />
				</c:when>
				<c:otherwise>
					<c:set var="rowClass" value="odd" />
				</c:otherwise>				
			</c:choose>
			<tr class="${rowClass}">
				<td>
					<b><a href="BrowseTickets?eventId=${event.id}&cat=${category.id}&reset=1">Cat. ${category.symbol}</a></b>
				</td>
				<c:set var="total" value="0" />
				<c:forEach var="siteId" items="${constants.siteIds}">
					<c:set var="total" value="${total + siteStatByCategory[category.symbol][siteId]}" />
					<td align="right">
						<c:choose>
							<c:when test="${empty siteStatByCategory[category.symbol][siteId]}">
								-
							</c:when>
							<c:otherwise>
								<a href="BrowseTickets?eventId=${event.id}&${siteId}_checkbox=true&cat=${category.id}&reset=1&uncheckSiteFilters=1">
									${siteStatByCategory[category.symbol][siteId]}
								</a>								
							</c:otherwise>								
						</c:choose>							
					</td>
				</c:forEach>
				<td align="right">
					<a href="BrowseTickets?eventId=${event.id}&cat=${category.id}&reset=1">				
						<b>${total}</b>
					</a>
				</td>
				<td align="right">
					<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / ticketCount}" />%</b>
				</td>
			</tr>
			<c:set var="row" value="${row + 1}" />
		</c:forEach>

		<c:choose>
			<c:when test="${(row % 2) == 0}">
				<c:set var="rowClass" value="even" />
			</c:when>
			<c:otherwise>
				<c:set var="rowClass" value="odd" />
			</c:otherwise>				
		</c:choose>
		<tr class="${rowClass}">
			<td>
				<a href="BrowseTickets?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
			</td>
			<c:set var="total" value="0" />
			<c:forEach var="siteId" items="${constants.siteIds}">
				<c:set var="total" value="${total + siteStatByCategory['UNCAT'][siteId]}" />
				<td align="right">
					<a href="BrowseTickets?eventId=${event.id}&${siteId}_checkbox=true&cat=UNCAT&reset=1">
						${siteStatByCategory['UNCAT'][siteId]}
					</a>
				</td>
			</c:forEach>
			<td align="right">
				<a href="BrowseTickets?eventId=${event.id}&reset=1">							
					<b>${total}</b>
				</a>
			</td>
			<td align="right">
				<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / ticketCount}" />%</b>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td align="right">Total</td>
			<c:forEach var="siteId" items="${constants.siteIds}">
				<td align="right">
					${totalStatBySite[siteId]}</b>
				</td>				
			</c:forEach>
			<td align="right">${ticketCount}</td>
			<td align="center">-</td>
		</tr>
		<tr>
			<td align="right">%</td>
			<c:forEach var="siteId" items="${constants.siteIds}">
				<td align="right">
					<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatBySite[siteId] * 100 / ticketCount}" />%				
				</td>
			</c:forEach>
			<td align="center">-</td>
			<td align="right">100.00%</td>
		</tr>
	</tfoot>
</table>

<h2>Number of ticket entries per site and quantity</h2>

<table class="list">
	<thead>
	<tr>
		<th></th>		
		<c:forTokens items="1,2,3,4,5,6,7,8,9,10,11+" var="quantity" delims=",">
			<th>
				<a href="BrowseTickets?eventId=${event.id}&quantity=${quantity}&reset=1">${quantity}</a>
			</th>
		</c:forTokens>
		<th>Total</th>
		<th>%</th>
	</tr>	
	</thead>
	<tbody>
		<c:set var="row" value="1" />				
		<c:forEach var="siteId" items="${constants.siteIds}">
			<c:choose>
				<c:when test="${(row % 2) == 0}">
					<c:set var="rowClass" value="even" />
				</c:when>
				<c:otherwise>
					<c:set var="rowClass" value="odd" />
				</c:otherwise>				
			</c:choose>
			<tr class="${rowClass}">
				<td>
					<a href="BrowseTickets?eventId=${event.id}&${siteId}_checkbox=true&reset=1&uncheckSiteFilters=1"><img align="absbottom" src="../images/ico-${siteId}.gif" title="${siteId}" /><b>${siteId}</b></a>
				</td>
				<c:set var="total" value="0" />
				<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
					<c:set var="total" value="${total + quantityStatBySiteId[siteId][quantity]}" />
					<td align="right">
						<c:choose>
							<c:when test="${empty quantityStatBySiteId[siteId][quantity]}">
								-
							</c:when>
							<c:otherwise>
								<a href="BrowseTickets?eventId=${event.id}&${siteId}_checkbox=true&quantity=${quantity}&reset=1&uncheckSiteFilters=1">
									${quantityStatBySiteId[siteId][quantity]}
								</a>								
							</c:otherwise>								
						</c:choose>							
					</td>
				</c:forTokens>
				<td align="right">
					<a href="BrowseTickets?eventId=${event.id}&quantity=${quantity}&reset=1">				
						<b>${total}</b>
					</a>
				</td>
				<td align="right">
					<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / ticketEntryCount}" />%</b>
				</td>
			</tr>
			<c:set var="row" value="${row + 1}" />				
		</c:forEach>
	</tbody>

	<tfoot>
		<tr>
			<td align="right">Total</td>
			<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
				<td align="right">			
					${totalStatByQuantity[quantity]}
				</td>				
			</c:forTokens>
			<td align="right">${ticketEntryCount}</td>
			<td align="center">-</td>
		</tr>
		<tr>
			<td align="right">%</td>
			<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
				<td>			
					<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / ticketEntryCount}" />%
				</td>				
			</c:forTokens>
			<td align="center">-</td>
			<td align="right">100.00%</td>
		</tr>
	</tfoot>

</table>

<h2>Number of ticket entries per zone and quantity</h2>

<table class="list">
	<thead>
	<tr>
		<th></th>		
		<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
			<th>
				<a href="BrowseTickets?eventId=${event.id}&quantity=${quantity}&reset=1">${quantity}</a>
			</th>
		</c:forTokens>
		<th>Total</th>
		<th>%</th>
	</tr>	
	</thead>
	<tbody>
		<c:set var="row" value="1" />				
		<c:forEach var="category" items="${event.artist.categories}">
			<c:choose>
				<c:when test="${(row % 2) == 0}">
					<c:set var="rowClass" value="even" />
				</c:when>
				<c:otherwise>
					<c:set var="rowClass" value="odd" />
				</c:otherwise>				
			</c:choose>
			<tr class="${rowClass}">
				<td>
					<a href="BrowseTickets?eventId=${event.id}&cat=${category.id}&reset=1"><b>Cat. ${category.symbol}</b></a>
				</td>
				<c:set var="total" value="0" />
				<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
					<c:set var="total" value="${total + quantityStatByCategory[category.symbol][quantity]}" />
					<td align="right">
						<c:choose>
							<c:when test="${empty quantityStatByCategory[category.symbol][quantity]}">
								-
							</c:when>
							<c:otherwise>
								<a href="BrowseTickets?eventId=${event.id}&cat=${category.id}&quantity=${quantity}&reset=1">
									${quantityStatByCategory[category.symbol][quantity]}
								</a>								
							</c:otherwise>								
						</c:choose>							
					</td>
				</c:forTokens>
				
				<td align="right">
					<a href="BrowseTickets?eventId=${event.id}&cat=${category.id}&reset=1">				
						<b>${total}</b>
					</a>
				</td>
				<td align="right">
					<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / ticketEntryCount}" />%</b>
				</td>
			</tr>
			<c:set var="row" value="${row + 1}" />				
		</c:forEach>

		<c:choose>
			<c:when test="${(row % 2) == 0}">
				<c:set var="rowClass" value="even" />
			</c:when>
			<c:otherwise>
				<c:set var="rowClass" value="odd" />
			</c:otherwise>				
		</c:choose>

		<tr class="${rowClass}">
			<td>
				<a href="BrowseTickets?eventId=${event.id}&cat=UNCAT&reset=1"><b>Uncategorized</b></a>
			</td>
			<c:set var="total" value="0" />
			<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
				<c:set var="total" value="${total + quantityStatByCategory['UNCAT'][quantity]}" />
				<td align="right">
					<a href="BrowseTickets?eventId=${event.id}&quantity=${quantity}&cat=UNCAT&reset=1">
						${quantityStatByCategory['UNCAT'][quantity]}
					</a>
				</td>
			</c:forTokens>
			<td align="right">
				<a href="BrowseTickets?eventId=${event.id}&reset=1">							
					<b>${total}</b>
				</a>
			</td>
			<td align="right">
				<b><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${total * 100 / ticketEntryCount}" />%</b>
			</td>
		</tr>

	</tbody>

	<tfoot>
		<tr>
			<td align="right">Total</td>
			<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
				<td align="right">			
					${totalStatByQuantity[quantity]}
				</td>				
			</c:forTokens>
			<td align="right">${ticketEntryCount}</td>
			<td align="center">-</td>
		</tr>
		<tr>
			<td align="right">%</td>
			<c:forTokens items="1,2,3,4,5,6,7,8,9+" var="quantity" delims=",">
				<td>			
					<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${totalStatByQuantity[quantity] * 100 / ticketEntryCount}" />%
				</td>				
			</c:forTokens>
			<td align="center">-</td>
			<td align="right">100.00%</td>
		</tr>
	</tfoot>

</table>

<h2>Links</h2>
<%-- <authz:authorize ifAnyGranted="PRIV_CATEGORY"> --%>
	<a href="#" onclick="location.href='EditorEditEventCategories?eventId=${event.id}'">Go to Categorization</a><br />
<%-- </authz:authorize> --%>
<a href="TMATBrowseShorts?eventId=${event.id}">Go to Shorts</a><br />
<a href="BrowseTickets?eventId=${event.id}">Go to Browse Tickets</a><br />

  <script type="text/javascript">

  function changeGroup() {
    var catGroup = $.trim($('#catScheme').val());;
	document.location.href = "EventStat?eventId=" + ${eventId} + "&catScheme=" + catGroup + "&transId=" + ${transId};

  }

  </script>
