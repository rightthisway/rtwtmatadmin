<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; User Reports
</div>

<h1>User Reports</h1>

<br /><br />
<display:table class="list" name="${actions}" id="action" requestURI="Report" pagesize="20">
    <display:column title="User" sortable="true" property="user.username" href="Report" paramId="user" paramProperty="user.username" />
    <display:column title="Time" sortable="true" property="timeStamp"  format="{0,date,yyyy/MM/dd, HH:mm.ss}" />
    <display:column title="Action" sortable="false" property="action" />
</display:table>
