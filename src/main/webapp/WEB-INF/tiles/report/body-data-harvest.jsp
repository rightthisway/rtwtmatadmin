<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />

<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a>
  &gt; Data Harvest
</div>

<h1>Data Harvest</h1>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>

<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate pattern="yyyy" value="${now}" var="currentYear" />

<fmt:formatDate pattern="yyyy" value="${fromDate}" var="fromYear" />
<fmt:formatDate pattern="M" value="${fromDate}" var="fromMonth" />
<fmt:formatDate pattern="d" value="${fromDate}" var="fromDay" />

<fmt:formatDate pattern="yyyy" value="${toDate}" var="toYear" />
<fmt:formatDate pattern="M" value="${toDate}" var="toMonth" />
<fmt:formatDate pattern="d" value="${toDate}" var="toDay" />
<br />
<form action="ReportDataHarvest" method="GET">
<h2>Upload Tickets</h2>
From Date: 
<select name="fromYear" id="fromYear">
  <c:forEach var="year" begin="2009" end="${currentYear}" step="1">
    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> /
<select name="fromMonth" id="fromMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="fromDay" id="fromDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == fromDay}">selected</c:if>>${day}</option>
  </c:forEach>
</select>
<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />

     To: 

<select name="toYear" id="toYear">
  <c:forEach var="year" begin="2009" end="${currentYear}" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> /
<select name="toMonth" id="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
  <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay" id="toDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDay}">selected</c:if>>${day}</option>
  </c:forEach>
</select>
<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />

<input id="getEventDataButton" name="action" type="submit" class="medButton" value="Fetch Events"/> 
<br /><br />

<c:choose>
	<c:when test="${empty events}">
		No events
	</c:when>
	<c:otherwise>
		<a href="javascript:selectAll()">Select all</a> - 
		<a href="javascript:unselectAll()">Unselect all</a><br /><br />
		<c:set var="rowNum" value="0" />
	
		<table class="list">
			<thead>
				<th></th>
				<th>Event name</th>
				<th>Event date</th>
				<th>Event location</th>
				<th></th>
			</thead>
			<tbody>
			  	<c:forEach var="event" items="${events}">
					<c:set var="rowNum" value="${rowNum + 1}" />
					<c:set var="rowClass" value="odd" />
					<c:if test="${(rowNum % 2) == 0}"><c:set var="rowClass" value="even" /></c:if>
					<tr class="${rowClass}">
					    <td style="white-space:nowrap">
					    	<input name="event-${event.id}" id="event-${event.id}" class="event-checkbox" type="checkbox" <c:if test="${selectedEventMap[event.id]}">checked</c:if>/>&nbsp;
					    </td>
					    <td>
					    	<img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom"/> ${event.name}
					    </td>
					    <td>
					    	${event.formattedEventDate}
						</td>
					    <td>
					    	${event.formattedVenueDescription}
						</td>
						<td>
							<a href="javascript:void(0)" onclick="showPreview(${event.id})">View</a>
							-
							<img src="../images/ico-download.gif" align="absmiddle" />
							<a href="#" onclick="downloadCsv(${event.id})">CSV</a> 
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br />
		
		Mark selected events as:
		<select name="eventType">
		  <option value="SPORT">SPORT</option>
		  <option value="THEATER">THEATER</option>
		  <option value="CONCERT">CONCERT</option>
		  <option value="OTHER">OTHER</option>
		</select>
		&nbsp;
		<input name="action" type="submit" class="medButton" value="Mark"/> 
		<br/><br/>
		
		<h2>Filters</h2>		
		Remove Duplicates:
		<select id="removeDuplicatePolicy">
		  <option value="NONE">NONE</option>
		  <option value="SCRUB" selected>SCRUB</option>
		</select>

		Normalize Sections and Rows:
		<select id="normalizeSectionsAndRowsPolicy">
		  <option value="NO">NO</option>
		  <option value="YES" selected>YES</option>
		</select>
		<input type="button" class="medButton" value="Update" onclick="if (lastPreviewedEventId > 0) showPreview(lastPreviewedEventId); return false"/> 
		
		<br/><br/>
		<img src="../images/ico-download.gif" align="absmiddle" /> <a href="javascript:downloadExcelFile()">Download tickets of selected events in excel file</a>
		<br /><br />

	</c:otherwise>
</c:choose>
</form>

<h2>Upload Tickets</h2>
You can categorize your own ticket listing in CSV format. To upload them click on the button below. 
<!--
<form enctype="multipart/form-data" action="ReportDataHarvest" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import Ticket csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" class="medButton "value="Upload"/>
</form>
-->
<input type="button" id="uploadTicketsButton" class="medButton" value="Upload Tickets" style="width: 200px" />
<div id="uploadTicketsLoading" style="display:none"><img src="../images/process-running.gif" />Uploading...</div>
<div id="uploadedTicketDiv" style="display:none">
	<img src="../images/ico-download.gif" align="absmiddle" /><a href="ReportDownloadDataHarvestCsvFile?eventId=0">Download CSV</a> -  
	<a href="javascript:showPreview(0)">View Preview</a> 
</div>
<br /><br />

You can upload your zone mappings in CVS format by clicking on the button below.
<input type="button" id="uploadCategoriesButton" class="medButton" value="Upload Zones" style="width: 200px" />
<div id="uploadCategoriesLoading" style="display:none"><img src="../images/process-running.gif" />Uploading...</div>
<br /><br />


<h2>Preview Tickets</h2>
<div id="previewLoading" style="display:none"><img src="../images/process-running.gif" />Loading preview...</div>

<div id="ticket-grid" style="height:750;width:826" ></div>

<br /><br />

<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketRow" />

<form id="reportDownloadXls" action="ReportDownloadDataHarvestXlsFile" method="POST">
	<input type="hidden" id="reportDownloadXlsEventIds" name="eventIds" />
	<input type="hidden" id="duplicatePolicyDownloadPolicyDownload" name="removeDuplicate" />
	<input type="hidden" id="normalizeSectionsAndRowsPolicyDownload" name="normalizeSectionAndRows" />
</form>

<script type="text/javascript">


	$(document).ready(function() {
		$('.calendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime() - 365 * 2 * 24 * 60 * 60 * 1000),
	        maxDate: new Date((new Date()).getTime())
		});
		
		$('#fromCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#fromCalendar').val(($('#fromYear').val()) + "-" + $('#fromMonth').val() + "-" + $('#fromDay').val());
	        }
	    );

		$('#fromCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#fromYear').val(tokens[0]*1);
				$('#fromMonth').val(parseInt(tokens[1]*1));
				$('#fromDay').val(parseInt(tokens[2]*1));
			}
		);
		
		$('#toCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#toCalendar').val(($('#toYear').val()) + "-" + $('#toMonth').val() + "-" + $('#toDay').val());
	        }
	    );
		
		$('#toCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('#toYear').val(tokens[0]*1);
				$('#toMonth').val(parseInt(tokens[1])*1);
				$('#toDay').val(parseInt(tokens[2])*1);
			}
		);			
		
		var uploadId = new Date().getTime();		
		var uploadCategoriesButton = $('#uploadCategoriesButton').upload({
		        action: 'ReportDataHarvestUploadCategories',
		        enctype: 'multipart/form-data',
		        params: {'uploadId': uploadId},
		        autoSubmit: true,
		        onSubmit: function() {
		        	$('#uploadCategoriesButton').hide();
		        	$('#uploadCategoriesLoading').show();
		        },
		        onComplete: function() {
		        	$.get("ReportDataHarvestReturnedMessage", {'uploadId': uploadId}, function(message) {
	        			alert(message)
			        	$('#uploadCategoriesButton').show();
			        	$('#uploadCategoriesLoading').hide();
			        	uploadCategoriesButton.params({'uploadId': new Date().getTime()});
		        	});
		        },
		        onSelect: function() {}
		});

		var uploadTicketsButton = $('#uploadTicketsButton').upload({
		        action: 'ReportDataHarvestUploadTickets',
		        enctype: 'multipart/form-data',
		        params: {'uploadId': uploadId},
		        autoSubmit: true,
		        onSubmit: function() {
		        	$('#uploadTicketsButton').hide();
		        	$('#uploadTicketsLoading').show();
		        },
		        onComplete: function() {
		        	$.get("ReportDataHarvestReturnedMessage", {'uploadId': uploadId}, function(message) {
	        			alert(message);
	        			if (message.indexOf("OK")) {
	        				$('#uploadedTicketDiv').show();
	        			}
			        	$('#uploadTicketsButton').show();
			        	$('#uploadTicketsLoading').hide();
			        	uploadTicketsButton.params({'uploadId': new Date().getTime()});
		        	});
		        },
		        onSelect: function() {}
		});

	});		
	
	function downloadExcelFile() {
		if ($('INPUT.event-checkbox:checked').length == 0) {
		  alert("You have to select at least one event.");
		  return;
		} 
		var ids = "";
		$('INPUT.event-checkbox:checked').each(function(i, elt) {
			ids += elt.id.split("-")[1] + ",";
		});
		ids = ids.substring(0, ids.length - 1);
		$('#reportDownloadXlsEventIds').val(ids);
		$('#duplicatePolicyDownloadPolicyDownload').val($('#removeDuplicatePolicy').val());
		$('#normalizeSectionsAndRowsPolicyDownload').val($('#normalizeSectionsAndRowsPolicy').val());
		$('#reportDownloadXls').submit();
	};


//there are some handlers associated when exiting the page that make the page slow, so remove them.
$(window).unbind("unload");

function sellerRenderer(val, cell, record) {
  var sellerContent = "";
  return record.data.seller;
}

function moneyRenderer(val) {
  return '$' + Ext.util.Format.number(val, '?0,000.00');
}

function sectionRenderer(val, cell, record) {
  return '<span title="' + record.data.section + '">' + record.data.section + '</span>';
}

function rowRenderer(val, cell, record) {
  return '<span title="' + record.data.row + '">' + record.data.row + '</span>';
}


var grid;
var store;

Ext.onReady(function(){

     store = new Ext.data.Store({
	  bufferSize: 2000,
	  bufferRange : 2000,
	  // proxy: new Ext.data.MemoryProxy(tickets),
        reader: new Ext.data.ArrayReader({}, [
           {name: 'quantity', type: 'int'},
           {name: 'cat'},
           {name: 'section'},
           {name: 'row'},
           {name: 'aPrice', type: 'float'},
           {name: 'oPrice', type: 'float'},
           {name: 'seller'},
           {name: 'endDate'},
           {name: 'insDate'}
        ])
    });

    var myView = new Ext.ux.grid.BufferView({
		    // custom row height
		    rowHeight: 24,
		    // render rows as they come into viewable area.
		    scrollDelay: 50,
		    getRowClass : function(record, index){
				if (record.data.seller == 'ADMT1') {
				  return "admit1Row";
				}
        	},
  		constrainScroll : function(y){
        		var max, full = this.ul.setHeight('auto').getHeight();
        		var new_y = y;
		      if(this.floating){
		      	var pe = Ext.fly(this.el.dom.parentNode);
		            var st = pe.getScroll().top;
		            var vh = pe.getViewSize().height;
		            //Normalize y by the scroll position for the parent element.  Need to move it into the coordinate space
		            //of the view.
		            var norm_y = y - st;
		            max = this.maxHeight ? this.maxHeight : vh - norm_y;
            
		            if(full > vh) {
            		    max = vh;
		                //Set new_y equal to (0,0) in view space by reducing y by the value of norm_y
            		    new_y = y - norm_y;
		            } else if(max < full) {
            		    new_y = y - (full - max);
		                max = full;
            		}                        
		      }else{
            		max = this.getHeight();
        		}
                       
        		if(full > max && max > 0){
            	  	this.activeMax = max - this.scrollerHeight * 2 - this.el.getFrameWidth('tb') - Ext.num(this.el.shadowOffset, 0);
            	  	this.ul.setHeight(this.activeMax);
            	  	this.createScrollers();
            	  	this.el.select('.x-menu-scroller').setDisplayed('');
			}else{
            		this.ul.setHeight(full);
            		this.el.select('.x-menu-scroller').setDisplayed('none');
        		}
        		this.ul.dom.scrollTop = 0;
        
        		return new_y;
    		}
    });

	grid = new Ext.ux.grid.livegrid.GridPanel({
		//renderTo: 'ticket-grid',
		stateful: true,
		stateId: 'objectGrid',
		stateEvents: ['columnresize', 'columnmove', 'headermousedown', 'show', 'hide'],
		enableDragDrop : false,
		width:750,
		height:286,
		frame: false,
		disableSelection: true,
		store:store,

        cm : new Ext.grid.ColumnModel([
	{
		id: 'tQty',
            header: "<b>Qty</b>",
            dataIndex: 'quantity',
            width: 40,
			align: 'center',
            sortable:true
        },{
		id: 'tCat',
            header: "Cat",
            dataIndex: 'cat',
            width: 40,
			align: 'center',
            sortable:true
        },{
		id: 'tSec',
            header: "<b>Section</b>",
            dataIndex: 'section',
            width: 100,
            align: 'center',
            sortable:true,
            renderer: sectionRenderer
        },{
		id: 'tRow',
            header: "<b>Row</b>",
            dataIndex: 'row',
            width: 75,
            align: 'center',
            sortable:true,
            renderer: rowRenderer            
        },{
		id: 'tWho',
            header: "Wholesale",
            dataIndex: 'aPrice',
            width: 75,
            align: 'right',
            renderer: moneyRenderer,
            sortable:true
        },{
		id: 'tOnl',
            header: "<b>Online Price</b>",
            dataIndex: 'oPrice',
            width: 75,
            align: 'right',
            renderer: moneyRenderer,
            sortable:true
        },{
			id: 'tInsDate',
            header: "<b>Ins Date</b>",
            dataIndex: 'insDate',
            width: 50,
            align: 'left',
            sortable:true
	},{
		id: 'tLas',
            header: "Last Update",
            dataIndex: 'endDate',
            width: 75,
            align: 'center',
            sortable:true
        },{
		id: 'tSel',
            header: "Seller",
            dataIndex: 'seller',
            width: 75,
            align: 'left',
            sortable:true,
            renderer: sellerRenderer,
        }
	]),
      
	  loadMask       : {
            msg : 'Loading...'
        },

	  //selModel : new Ext.ux.grid.livegrid.RowSelectionModel(),

	    view: myView
    });

	grid.render('ticket-grid');

    grid.on({
        "cellclick":{fn: onCellClick }
    });
});

    function onCellClick(grid, rowIndex, columnIndex, e) {   
    }


  function getEventData() {
    var tYear = $('#toYear').val();
    var tMonth = $('#toMonth').val();
    var tDay = $('#toDay').val();
    var fYear = $('#fromYear').val();
    var fMonth = $('#fromMonth').val();
    var fDay = $('#fromDay').val();
	document.location.href = "ReportDataHarvest?toYear=" + tYear + "&toMonth=" + tMonth + "&toDay=" + tDay + "&fromYear=" + fYear + "&fromMonth=" + fMonth + "&fromDay=" + fDay;
  }

  function changeEventTypes() {
    var tYear = $('#toYear').val();
    var tMonth = $('#toMonth').val();
    var tDay = $('#toDay').val();
    var fYear = $('#fromYear').val();
    var fMonth = $('#fromMonth').val();
    var fDay = $('#fromDay').val();
	document.location.href = "ReportDataHarvest?toYear=" + tYear + "&toMonth=" + tMonth + "&toDay=" + tDay + "&fromYear=" + fYear + "&fromMonth=" + fMonth + "&fromDay=" + fDay;
  }    
    
  function selectAll() {
	$('INPUT.event-checkbox:checked').attr('checked', true); 	
  }
    
  function unselectAll() {
	$('INPUT.event-checkbox:checked').attr('checked', false); 	
  }
  
  var lastPreviewedEventId = 0;
  
  function showPreview(eventId) {
    lastPreviewedEventId = eventId;
  	$('#previewLoading').show();
	$.getJSON("ReportDataHarvestPreview?eventId=" + eventId + "&removeDuplicate=" + $('#removeDuplicatePolicy').val() + "&normalizeSectionAndRows=" + $('#normalizeSectionsAndRowsPolicy').val(), function(tickets) {
	    store.loadData(tickets);
		grid.render();
	  	$('#previewLoading').hide();
	});
  }
  
  function downloadCsv(eventId) {
	document.location.href = "ReportDownloadDataHarvestCsvFile?eventId=" + eventId + "&removeDuplicate=" + $('#removeDuplicatePolicy').val() + "&normalizeSectionAndRows=" + $('#normalizeSectionsAndRowsPolicy').val()
  }

   </script>
