<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="adminSubMenu">
  <c:choose>
    <c:when test="${selectedSubMenu == 'User Reports'}">
      <b>User Reports</b>
    </c:when>
    <c:otherwise>
      <a href="Report">User Reports</a>
    </c:otherwise>
    </c:choose>
    |
  <c:choose>
    <c:when test="${selectedSubMenu == 'Data Harvest'}">
      <b>Data Harvest</b>
    </c:when>
    <c:otherwise>
      <a href="ReportDataHarvest">Data Harvest</a>
    </c:otherwise>
  </c:choose>
	<%-- |
	<c:choose>
		<c:when test="${criteria == 'site'}">
			<b>Stats by Site</b>
		</c:when>
		<c:otherwise>
			<a href="TourStatBySite">Stats by Site</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${criteria == 'quantity'}">
			<b>Stats by Quantity</b>
		</c:when>
		<c:otherwise>
			<a href="TourStatByQuantity">Stats by Quantity</a>
		</c:otherwise>
	</c:choose> --%>

</div>

<tiles:insertAttribute name="subBody" />
