<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Log Files
</div>


<div style="clear:both"></div>

<h1>Log Files</h1>
<script type="text/javascript">

	function update(id,project){
		document.location.href = "TnApiCallLimit?action=update&createCallDay="+$('#'+id+'_1').val()+"&createCallMinute="+$('#'+id+'_2').val()
				+"&deleteCallMinute="+$('#'+id+'_3').val()+"&updateCallMinute="+$('#'+id+'_4').val()+"&project="+project;
	}
	
	function audit(project){
		window.open("TnApiCallLimit?action=audit&project="+project);
	}

</script>

<display:table id="tnApiCallLimit" list="${tnApiCallLimitList }">
		
	<display:column title="Projects" property="projects"></display:column>
	<display:column title="Create Call Per Day">
		<input type="text" name="${tnApiCallLimit.id }_1" id="${tnApiCallLimit.id }_1" value="${tnApiCallLimit.createCallDay }"/>
	</display:column>
	<display:column title="Create Call Per Minute">
		<input type="text" name="${tnApiCallLimit.id }_2" id="${tnApiCallLimit.id }_2" value="${tnApiCallLimit.createCallMinute }"/>
	</display:column>
	<display:column title="Delete Call Per Minute">
		<input type="text" name="${tnApiCallLimit.id }_3" id="${tnApiCallLimit.id}_3" value="${tnApiCallLimit.deleteCallMinute }"/>
	</display:column>
	<display:column title="Update Call Per Minute">
		<input type="text" name="${tnApiCallLimit.id }_4" id="${tnApiCallLimit.id }_4" value="${tnApiCallLimit.updateCallMinute }"/>
	</display:column>
	<display:column>
		<input type="button" name="update" id="update" value="Update" onclick="update('${tnApiCallLimit.id }','${tnApiCallLimit.projects }');"/>
	</display:column>
	<display:column>
		<input type="button" name="audit" id="audit" value="Audit" onclick="audit('${tnApiCallLimit.projects }');"/>
	</display:column>
	
		
</display:table>