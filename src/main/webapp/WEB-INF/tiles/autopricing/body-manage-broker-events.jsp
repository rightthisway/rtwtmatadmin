<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
var otherFrequentEventsCount = 0;
if('${otherFrequentEventsCount}' != null && '${otherFrequentEventsCount}' != '') {
	otherFrequentEventsCount = '${otherFrequentEventsCount}';
}



$(document).ready(function(){
	
	$( "#brokerId" ).change(function() {
		$("#events").html('');
		$("#ebayInventoryTabel").html('');
		$("#selectedValue").html('');

		});

	$('#copyAllBroker').click(function(){
		var brokerVal=$(".brokermethod")[0].value;
		if($('#copyAllBroker').attr('checked')){
			$.each( $(".brokermethod"), function( key, value ) {
  				value.value=brokerVal;
			});
		}
	});
	
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	//copyAll();
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	$('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
			else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
			else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1]);
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1]);
				
			} 
			else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1]);
				
			}
			
					
			else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#child").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('grandChildId',row[1]);
				
			} 
	});

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id){
	/*var InvForm = document.forms.formId;
	var tourList = new Array();
	var flag = false;
	var x = 0; 	
	var i=0;

	for (x=0;x<InvForm.artist.length;x++){
    	if (InvForm.artist[x].selected){
			flag = true;
			tourList[i] =   InvForm.artist[x].value;
			i++;
		}
	}*/
	
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;;
	} 
	
	else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	}
		
	else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;;
	}
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+"</option>"
				$('#events').append(rowText);
            }	
		}
	}); 
} 

function submitForms(action){
		$("#status").val(action);
		$("#formId").submit();
}

/* 
function validateForm(){

	var flag= true;
	$('.selectCheck:checkbox:checked').each(function () {
		
		var id,value;
		
		id = this.id.replace('checkbox','broker');
		value = $.trim($("#"+id).val());
		if(value=='') {
			alert('Broker can not be blank.');
			$("#"+id).focus();
			flag = false;
			return ;
		}
	});
	if(!flag){
		return flag;	
	}
	return true;
} */

function copyAll(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		copyTextField('broker');
		
	}else{
		$('.selectCheck').attr('checked', false);
	}
}

function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}


function popupUpdateTGCatsPricingAudit(eId){
	var url = "ManageBrokerAutoPricingAuditPopup?eId="+eId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Manage Broker Events
</div>
<h1>Manage Broker Events</h1>
<br/>

<form:form action="ManageBrokerEvents" method="post" id="formId">
<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br/>
	<br/>
	<table width="80%">
	
		
		<tr>
			<td><b>Select Broker:</b></td>
			<td>
			
					<select id="brokerId" name="brokerId">
					<option value="" <c:if test="${brokerId eq 0}"> Selected </c:if> >All</option>
					<c:forEach items="${brokers}" var="broker">
			     		<option value="${broker.id}" <c:if test="${broker.id eq brokerId}"> Selected </c:if> >${broker.name}</option>
					</c:forEach>									
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
 
	
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="Child" name="Child" value ="${Child}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue">
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" /> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}">  ${event.name}<fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building} 
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr>
		<tr>
			<td colspan="6" align="center">
			<input type="hidden" id="status" name="status"/>
				<input type="button" value="Get Events" onclick="submitForms('search');" class="medButton">
				<!-- <input type="button" value="Remove Events" onclick="submitForms('DELETED');" class="medButton" -->
			</td>
		</tr>
	</table>
	
	<c:if test="${not empty autoprieventslst}" >
	<div id="ebayInventoryTabel" >
	
	
	
	
	
	
	
<display:table list="${autoprieventslst}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="event"  requestURI="#" class="list">
			
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${event.id}" id="checkbox_${event.id}" <c:if test ="${event.brokerStatus == 'ACTIVE'  }"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			
			<display:column title="EvenName" sortable="true">${event.name}</display:column>
			
			<display:column title="Event Date And Time" sortable="true">

				<fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
						<c:when test="${event.localTime == null}">
							TBD
						</c:when>
						<c:otherwise>
							<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
						</c:otherwise>
					</c:choose>
			</display:column>
			
			<display:column title="Venue" sortable="true">${event.venue.building},${event.venue.state},${event.venue.city},${event.venue.country}</display:column>
		
			<display:column title="BrokerStatus" sortable="true">${event.brokerStatus}</display:column>
			
			<display:column title='Broker<br/><input type="checkbox" name="copyAllBroker" id="copyAllBroker" >' >
    <select name="broker_${event.id}" id="broker_${event.id}" class="brokermethod" onchange="selectRow('${event.id}');" style="width:100px;" >
   
     <c:forEach items="${brokers}" var="eventBroker">
          <option value="${eventBroker.id}" <c:if test="${eventBroker.id eq event.brokerId}"> Selected </c:if> >${eventBroker.name}</option>
     </c:forEach>
    </select>
   </display:column>


			
			
			
			
					<display:column title="Action" >
			 	<input type="button" value="Audit"    class="medButton" id="audit_${event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${event.id}');"/>
			 	
			 </display:column>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:320px;">
				<input type="button" value="Update"    class="medButton" id="update" name="update2"onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty autoprieventslst}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any event details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
</form:form>