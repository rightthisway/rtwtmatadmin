<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Additional Exchange Markup
</div>


<div style="clear:both"></div>

<h1>Additional Exchange Markup</h1>
<script type="text/javascript">
function ebayExposureUpdate(){
	$("#action").val("updateExposure");
	$("#formId").submit();
} 
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function popupDefaultExchangeMarkup(){
	var url = "DefaultExchangeMarkupAudit";
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}

	 </script>
<form:form modelAttribute="exosure" action="AutopricingEditExchangeMarkup" method="post" id="formId">
<div id="ebayInventoryTabel" >
	    <table>
			<tr align="center">
				<td colspan="3" class="successMessage" id="success" ><font face="Tahoma" size="4" color="ff0000">${sportinfo}</font></td>
        	</tr>
		</table>	
		<br/>
</div>
		<div align="left">
<table BORDER=5 BORDERCOLOR=blue width="300">
<tr>
  <th>Exchange</th>
  <th>Markup(%)</th>
</tr>

<tr>
<td>TicketNetwork</td>
  <td><input type="text" id="markup_tn" size="5" name="markup_tn" onkeypress="return isNumber(event)" value="${tnmarkup}"/></td>
 </tr>
<tr>
<td>Vivid Seats</td>
  <td><input type="text" id="markup_vividseat" size="5" name="markup_vividseat" onkeypress="return isNumber(event)" value="${vividmarkup}"/></td>
</tr>
<%-- <tr>
<td>TickPick</td>
  <td><input type="text" id="markup_tickpick" size="5" name="markup_tickpick" onkeypress="return isNumber(event)" value="${tickpickmarkup}"/></td>
</tr> --%>
<tr>
<td>ScoreBig</td>
  	<td><input type="text" id="markup_scorebig" size="5" name="markup_scorebig" onkeypress="return isNumber(event)" value="${scorebigmarkup}"/></td>	
</tr>
<%-- <tr>
<td>Ebay</td>
  <td><input type="text" id="markup_ebay" size="5" name="markup_ebay" onkeypress="return isNumber(event)" value="${ebaymarkup}"/></td>
 </tr> --%>
<tr>
<td>Stubhub</td>
  	<td><input type="text" id="markup_stubhub" size="5" name="markup_stubhub" onkeypress="return isNumber(event)" value="${stubhubmarkup}"/></td>	
</tr>
<tr>
<td>Exchange 3</td>
  	<td><input type="text" id="markup_exchange3" size="5" name="markup_exchange3" onkeypress="return isNumber(event)" value="${exchange3markup}"/></td>	
</tr>
<tr>
<td>Exchange 4</td>
  	<td><input type="text" id="markup_exchange4" size="5" name="markup_exchange4" onkeypress="return isNumber(event)" value="${exchange4markup}"/></td>	
</tr>
<tr>
<td>Exchange 5</td>
  	<td><input type="text" id="markup_exchange5" size="5" name="markup_exchange5" onkeypress="return isNumber(event)" value="${exchange5markup}"/></td>	
</tr>
</br>

</table>
			<br/>
			</div>
<table align="left">
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td align="center"><input type="button" value="Update"  class="medButton" id="update2" name="update2"onclick="javascript:ebayExposureUpdate();"/> </td>
<td align="center"><input type="button" value="Audit"  class="medButton" id="Audit2" name="Audit2"onclick="javascript:popupDefaultExchangeMarkup();"/> </td>
</tr>
</table>
	<input type="hidden" name="action" id="action"/>
	<input type="hidden" name="parentCat" id="parentCat"/>
	<br/>
	<br/>
</form:form>