<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<script type="text/javascript" src="../js/highChart/highcharts.js"></script>
<script type="text/javascript" src="../js/highChart/exporting.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Auto Pricing Ticket Details
</div>


<h1>Auto Pricing Ticket Details</h1>
<br/>
<form:form action="AutopricingTicketDetails" method="post" id="formId">
	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	
	<br/>
	<br/>
	
<table width="80%">
		
		<tr>
			<td><b>Filter: By Tour </b></td>
			<td>
				<select name="tourId" id="tourId" onchange="changeFilter();">
					<option value="">Select</option>
					<c:forEach var="tour" items="${tours}">
						<option value="${tour.id}" style="height:20px;" 
						<c:if test="${tour.id == selectedTourId}">selected</c:if>>${tour.name}</option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		
		<tr>
			<td><b>Or By Venue </b></td>
			<td>
				<select name="venueId" id="venueId"  onchange="changeFilter();">
					<option value="">Select</option>
					<c:forEach var="venue" items="${venues}">
						<option value="${venue.id}" style="height:20px;" 
						<c:if test="${venue.id == selectedVenueId}">selected</c:if>>${venue.building}</option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		<tr>
			<td><b>Events: </b></td>
			<td>
				<select name="eventId" id="eventId"  >
					<option value="">Select</option>
					<c:forEach var="event" items="${events}">
						<option value="${event.id}" style="height:20px;" 
						<c:if test="${event.id == selectedEventId}">selected</c:if>>${event.name} 
						<fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose></option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Ticket Details" onclick="submitForms();" class="medButton">
			</td>
		</tr>
	</table>
	
	
	<div id="container-tabs" style="margin-top: 5px">
	
	<ul>
			<li><a href="#fragment-minicats" id="tab-minicattickets"><span>MiniCat</span></a></li>
			<li><a href="#fragment-autocats" id="tab-autocattickets"><span>AutoCat</span></a></li>
			<li><a href="#fragment-vipautocats" id="tab-vipautocattickets"><span>VipCat</span></a></li>    
   </ul>
   
   		
   		
   		<div id="fragment-minicats">
   		<c:if test="${not empty categoryTickets}" >
		<div id="ebayInventoryTabel" >
		
		
		<display:table list="${categoryTickets}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tgCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${tgCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${tgCatTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${tgCatTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${tgCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${tgCatTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${tgCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${tgCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${tgCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${tgCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${tgCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${tgCatTicket.id}"
				onClick="popupTGCatsTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${tgCatTicket.id}"
				onClick="popupTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.ticketId}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${tgCatTicket.baseTicket1}"
				onClick="popupTMATBaseTicket1Info('${tgCatTicket.eventId}','${tgCatTicket.baseTicket1}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket2" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicket2 ne null and tgCatTicket.baseTicket2 ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${tgCatTicket.baseTicket2}"
						onClick="popupTMATBaseTicket2Info('${tgCatTicket.eventId}','${tgCatTicket.baseTicket2}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</c:if>
	<c:if test="${empty categoryTickets}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	
	<div id="fragment-autocats">
   		<c:if test="${not empty autoCatsCategoryTicketsList}" >
		<div id="ebayInventoryTabel" >
	
	
	<display:table list="${autoCatsCategoryTicketsList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="autoCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${autoCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${autoCatTicket.rowRange}</display:column>
			<display:column title="Last Row" sortable="true">${autoCatTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${autoCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${autoCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${autoCatTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${autoCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${autoCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${autoCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${autoCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${autoCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${autoCatTicket.id}"
				onClick="popupAutoCatsTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${autoCatTicket.id}"
				onClick="popupAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.ticketId}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${autoCatTicket.baseTicket1}"
				onClick="popupAutoTMATBaseTicket1Info('${autoCatTicket.eventId}','${autoCatTicket.baseTicket1}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket2" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicket2 ne null and autoCatTicket.baseTicket2 ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${autoCatTicket.baseTicket2}"
						onClick="popupAutoTMATBaseTicket2Info('${autoCatTicket.eventId}','${autoCatTicket.baseTicket2}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</c:if>
	<c:if test="${empty autoCatsCategoryTicketsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	
	
	<div id="fragment-vipautocats">
	<c:if test="${not empty vipCatsCategoryTicketsList}" >
	
	<div id="ebayInventoryTabel" >
		
		<display:table list="${vipCatsCategoryTicketsList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipCatTicket.section}</display:column>
			<display:column title="Alternate Row" sortable="true">${vipCatTicket.alternateRow}</display:column>
			<display:column title="Quantity" sortable="true">${vipCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipCatTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${vipCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${vipCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipCatTicket.id}"
				onClick="popupVipCatsTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${vipCatTicket.id}"
				onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.ticketId}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipCatTicket.baseTicket1}"
				onClick="popupVipTMATBaseTicket1Info('${vipCatTicket.eventId}','${vipCatTicket.baseTicket1}');"/>
			</display:column>
			
			<display:column title="TMAT Base Ticket2" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicket2 ne null and vipCatTicket.baseTicket2 ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${vipCatTicket.baseTicket2}"
						onClick="popupVipTMATBaseTicket2Info('${vipCatTicket.eventId}','${vipCatTicket.baseTicket2}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</c:if>
	
	<c:if test="${empty vipCatsCategoryTicketsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>
</div>


<script type="text/javascript">

$('#container-tabs').tabs({
    select: function(event, ui) {
      var url = ui.tab.href;
      var posAnchor = url.indexOf('#');
  	//anchor = url.substr(posAnchor, url.length - posAnchor);
  	anchor = "";   
  	
      if (url.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('opacity', 1);
        $('#chartDiv').css('height', 400);
	  } else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
	  }
    }
    });
    
if (location.href.indexOf('#fragment-chart') > 0) {
    $('#chartDiv').css('height', 400);
    $('#chartDiv').css('opacity', 1);
}  else {
    $('#chartDiv').css('opacity', 0);
    $('#chartDiv').css('height', 1);
}

function changeFilter() {
	
	$("#formId").submit();
}

function submitForms(){
	
	$("#action").val(action);
	
	
	if(action=='update'){
		if(validateForm()){
			
			$("#formId").submit();
		}
	}else{
		$("#action").val(action);
		$("#formId").submit();
	}
}
function popupTGCatsTicketInfo(eId,tId){
	var url = "CategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupTMATTicketInfo(eId,ticketId){
	var url = "TMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupTMATBaseTicket1Info(eId,ticketId){
	var url = "TMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupTMATBaseTicket2Info(eId,ticketId){
	var url = "TMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupAutoCatsTicketInfo(eId,tId){
	var url = "AutoCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupAutoTMATTicketInfo(eId,ticketId){
	var url = "AutoTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupAutoTMATBaseTicket1Info(eId,ticketId){
	var url = "AutoTMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupAutoTMATBaseTicket2Info(eId,ticketId){
	var url = "AutoTMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupVipCatsTicketInfo(eId,tId){
	var url = "VipCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipTMATTicketInfo(eId,ticketId){
	var url = "VipTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupVipTMATBaseTicket1Info(eId,ticketId){
	var url = "VipTMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupVipTMATBaseTicket2Info(eId,ticketId){
	var url = "VipTMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayCatsTicketInfo(eId,tId){
	var url = "EbayCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupEbayTMATTicketInfo(eId,ticketId){
	var url = "EbayTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket1Info(eId,ticketId){
	var url = "EbayTMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket2Info(eId,ticketId){
	var url = "EbayTMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
</script>		