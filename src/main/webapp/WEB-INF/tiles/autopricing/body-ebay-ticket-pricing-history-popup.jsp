
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
		<c:if test="${not empty CategoryTicketlist}" >
	<div id="ebayInventoryTabel" >
		
		<display:table list="${CategoryTicketlist}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tgCatTmatTicket"  requestURI="#" class="list">
			
			<display:column title="BuyItNowPrice" sortable="true">${tgCatTmatTicket.buyItNowPrice}</display:column>
			<display:column title="TmatPurchase Price" sortable="true">${tgCatTmatTicket.tmatpurchasePrice}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTmatTicket.lastUpdated}" /></display:column>
			<display:column title="EventId" sortable="true">${tgCatTmatTicket.eventId}</display:column>
		</display:table>
	</c:if>
	<br/>
</div>
<c:if test="${not empty tmatPriceList}" >
	<div id="auditTable" >
	<h2> Price History </h2>
		<display:table list="${tmatPriceList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="TList"  requestURI="#" class="list">			
			<display:column title="Date">
			<c:choose>
			<c:when test="${TList.tPopDate== null}">
			-
			</c:when>
			<c:otherwise>
				${TList.tPopDate}
				</c:otherwise>	
				</c:choose>				
			</display:column>
			
			<display:column  title ="Price">
			
			<c:choose>
			<c:when test="${TList.tPopPrice== null}">
			-
			</c:when>
			<c:otherwise>
				${TList.tPopPrice}
				</c:otherwise>	
				</c:choose>	
			</display:column>
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty CategoryTicketlist}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>