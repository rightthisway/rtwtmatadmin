<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Default Autopricing Properties
</div>

<div style="clear:both"></div>

<h1>Default AutoPricing Properties</h1>
<script type="text/javascript">
function ebayExposureUpdate(){
	
	var errorNoFlag = true;
	
	var miniExposure = $("#mini_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Mini Exposure can not be blank.');
		$("#mini_exposure_sport").focus();
		errorNoFlag = false;
		return ;
	}
    
	miniExposure = $("#auto_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Auto Exposure can not be blank.');
		$("#auto_exposure_sport").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_mini_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure_sport").focus();
		flag = false;
		return ;
	}
	
	
	miniExposure = $("#vip_auto_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure_sport").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastrow_mini_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure_sport").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastfiverow_mini_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure_sport").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#zones_pricing_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Zones Pricing Exposure can not be blank.');
		$("#zones_pricing_exposure_sport").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zoned_lastrowmini_exposure_sport").val();
	if(miniExposure==''){
		alert('Sports Zoned LastRowMini Exposure can not be blank.');
		$("#zoned_lastrowmini_exposure_sport").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#mini_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Mini Exposure can not be blank.');
		$("#mini_exposure_concert").focus();
		errorNoFlag = false;
		return ;
	}
    
	miniExposure = $("#auto_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Auto Exposure can not be blank.');
		$("#auto_exposure_concert").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_mini_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure_concert").focus();
		flag = false;
		return ;
	}
	
	
	miniExposure = $("#vip_auto_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure_concert").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastrow_mini_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure_concert").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastfiverow_mini_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure_concert").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#zones_pricing_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Zones Pricing Exposure can not be blank.');
		$("#zones_pricing_exposure_concert").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zoned_lastrowmini_exposure_concert").val();
	if(miniExposure==''){
		alert('Concerts Zoned LastRowMini Exposure can not be blank.');
		$("#zoned_lastrowmini_exposure_concert").focus();
		flag = false;
		return ;
	}
	
	   
	miniExposure = $("#mini_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Mini Exposure can not be blank.');
		$("#mini_exposure_theater").focus();
		errorNoFlag = false;
		return ;
	}
    
	miniExposure = $("#auto_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Auto Exposure can not be blank.');
		$("#auto_exposure_theater").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_mini_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure_theater").focus();
		flag = false;
		return ;
	}
	
	
	miniExposure = $("#vip_auto_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure_theater").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#lastrow_mini_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure_theater").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastfiverow_mini_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure_theater").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zones_pricing_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Zones Pricing Exposure can not be blank.');
		$("#zones_pricing_exposure_theater").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zoned_lastrowmini_exposure_theater").val();
	if(miniExposure==''){
		alert('Theater Zoned LastRowMini Exposure can not be blank.');
		$("#zoned_lastrowmini_exposure_theater").focus();
		flag = false;
		return ;
	}
	
	  /*  
	
	miniExposure = $("#mini_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas Mini Exposure can not be blank.');
		$("#mini_exposure_vegas").focus();
		errorNoFlag = false;
		return ;
	}
    
	miniExposure = $("#auto_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas Auto Exposure can not be blank.');
		$("#auto_exposure_vegas").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_mini_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure_vegas").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_auto_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure_vegas").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastrow_mini_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure_vegas").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastfiverow_mini_exposure_vegas").val();
	if(miniExposure==''){
		alert('Vegas LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure_vegas").focus();
		flag = false;
		return ;
	} */
	   
	  
	miniExposure = $("#mini_exposure_others").val();
	if(miniExposure==''){
		alert('Others Mini Exposure can not be blank.');
		$("#mini_exposure_others").focus();
		errorNoFlag = false;
		return ;
	}
    
	miniExposure = $("#auto_exposure_others").val();
	if(miniExposure==''){
		alert('Others Auto Exposure can not be blank.');
		$("#auto_exposure_others").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_mini_exposure_others").val();
	if(miniExposure==''){
		alert('Others Vip Mini Exposure can not be blank.');
		$("#vip_mini_exposure_others").focus();
		flag = false;
		return ;
	}
	
	miniExposure = $("#vip_auto_exposure_others").val();
	if(miniExposure==''){
		alert('Others Vip Auto Exposure can not be blank.');
		$("#vip_auto_exposure_others").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastrow_mini_exposure_others").val();
	if(miniExposure==''){
		alert('Others LastRow Mini Exposure can not be blank.');
		$("#lastrow_mini_exposure_others").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#lastfiverow_mini_exposure_others").val();
	if(miniExposure==''){
		alert('Others LastFiveRow Mini Exposure can not be blank.');
		$("#lastfiverow_mini_exposure_others").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zones_pricing_exposure_others").val();
	if(miniExposure==''){
		alert('Others Zones Pricing Exposure can not be blank.');
		$("#zones_pricing_exposure_others").focus();
		flag = false;
		return ;
	}
	miniExposure = $("#zoned_lastrowmini_exposure_others").val();
	if(miniExposure==''){
		alert('Others Zoned LastRowMini Exposure can not be blank.');
		$("#zoned_lastrowmini_exposure_others").focus();
		flag = false;
		return ;
	}
	
	if(errorNoFlag){
		$("#action").val("updateExposure");
		$("#formId").submit();
	}
	
} 
function popupDefaultAutoPricingAudit(exCategory){
	var url = "DefaultAutopricingAudit?exCategory="+exCategory;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
	 </script>
<form:form modelAttribute="exosure" action="DefaultAutopricing" method="post" id="formId">




<span  <c:if test="${store==true}">  style="color:blue;" </c:if> <c:if test="${store!=true}">class="successMessage" </c:if> >${exposureMsg}</span>
<br/>
<br/>
<div id="ebayInventoryTabel" >
	    <table>
			<tr align="center">
				<td colspan="3" class="successMessage" id="success" ><font face="Tahoma" size="4" color="ff0000">${sportinfo}</font></td>
        	</tr>
		</table>	
		<br/>
<table list=""  defaultsort="5" decorator="org.displaytag.decorator.TotalTableDecorator"   id="ebayInventory"  requestURI="#" class="list">
<tr>
  <th>Event Category</th>
  <th>Mini Exposure</th>
  <th>Auto Exposure</th>
  <th>Vip Mini Exposure</th>
  <th>Vip Auto Exposure</th>
  <th>LastRow MiniCats Exposure</th>
  <th>LastFiveRow MiniCats Exposure</th>
  <th>Zones Pricing Exposure</th>
  <th>Zoned LastRowMini Exposure</th>
  <th>Shipping Method</th>
  <th>Near Term Display Option</th>			
  <th>RPT(%)</th>
  <th>Price BreakUp</th>
  <th>Lower Markup(%)</th>
  <th>Upper Markup(%)</th>
  <th>Lower Shipping Fees($)</th>
  <th>Upper Shipping Fees($)</th>
  <th>Section Count Ticket</th>
  <th>Action</th>
</tr>
<tr>
<td>Sports</td>
<td>
<select name="mini_exposure_sport" id="mini_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="auto_exposure_sport" id="auto_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td> 
<select name="vip_mini_exposure_sport" id="vip_mini_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportvipminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportvipminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportvipminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_auto_exposure_sport" id="vip_auto_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportvipautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportvipautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportvipautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td> 
<select name="lastrow_mini_exposure_sport" id="lastrow_mini_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td> 
<select name="lastfiverow_mini_exposure_sport" id="lastfiverow_mini_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportlastfiverowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportlastfiverowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportlastfiverowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td> 
<select name="zones_pricing_exposure_sport" id="zones_pricing_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportzonespricingexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportzonespricingexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportzonespricingexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td> 
<select name="zoned_lastrowmini_exposure_sport" id="zoned_lastrowmini_exposure_sport" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${sportzonedlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${sportzonedlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${sportzonedlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>


<td>
<select name="shipping_method_sport" id="shipping_method_sport" style="width:200px;" >
					<option value="0" <c:if test ="${sportshippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${sportshippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${sportshippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${sportshippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${sportshippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${sportshippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${sportshippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${sportshippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${sportshippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${sportshippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
</td>

<td>
<select name="near_term_display_option_sport" id="near_term_display_option_sport" style="width:200px;" >
					<option value="0" <c:if test ="${sportnearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${sportnearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${sportnearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
</td>
  <td><input type="text"  id="rpt_sport" size="5" name="rpt_sport" value="${sportrpt}" onkeypress="return isNumber(event)"></td>
  <td><input type="text"  id="price_breakup_sport" size="5" name="price_breakup_sport" value="${sportpricebreakup}" onkeypress="return isNumber(event)"></td>
  <td><input type="text" id="lower_markup_sport" size="5" name="lower_markup_sport" onkeypress="return isNumber(event)" value="${sportlowermarkup}"/></td>		
  <td><input type="text" id="upper_markup_sport" size="5" name="upper_markup_sport" onkeypress="return isNumber(event)" value="${sportuppermarkup}"/></td>
  <td><input type="text" id="lower_shippingFees_sport" size="5" name="lower_shippingFees_sport" onkeypress="return isNumber(event)" value="${sportlowershipping}"/></td>
  <td><input type="text" id="upper_shippingFees_sport" size="5" name="upper_shippingFees_sport" onkeypress="return isNumber(event)" value="${sportuppershipping}"/></td>	
 <td><input type="text" id="sectionCount_sport" size="5" name="sectionCount_sport" onkeypress="return isNumber(event)" value="${sportsectioncount}"/></td>
 <td><input type="button" value="Audit"  class="smallButton" id="sportAudit" name="sportAudit" onClick="popupDefaultAutoPricingAudit('sport');"/></td>
</tr>
<tr>
<td>Concert</td>
<td><select name="mini_exposure_concert" id="mini_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select></td>
				
			
				
<td>
<select name="auto_exposure_concert" id="auto_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_mini_exposure_concert" id="vip_mini_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertvipminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertvipminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertvipminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_auto_exposure_concert" id="vip_auto_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertvipautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertvipautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertvipautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastrow_mini_exposure_concert" id="lastrow_mini_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastfiverow_mini_exposure_concert" id="lastfiverow_mini_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertlastfiverowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertlastfiverowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertlastfiverowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>


<td>
<select name="zones_pricing_exposure_concert" id="zones_pricing_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertzonespricingexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertzonespricingexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertzonespricingexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="zoned_lastrowmini_exposure_concert" id="zoned_lastrowmini_exposure_concert" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${concertzonedlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${concertzonedlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${concertzonedlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
				
<select name="shipping_method_concert" id="shipping_method_concert" style="width:200px;" >
					<option value="0" <c:if test ="${concertshippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${concertshippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${concertshippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${concertshippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${concertshippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${concertshippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${concertshippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${concertshippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${concertshippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${concertshippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
</td>

<td>
<select name="near_term_display_option_concert" id="near_term_display_option_concert" style="width:200px;" >
					<option value="0" <c:if test ="${concertnearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${concertnearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${concertnearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
</td>
  <td><input type="text"  id="rpt_concert" size="5" name="rpt_concert" value="${concertrpt}" onkeypress="return isNumber(event)"></td>
  <td><input type="text"  id="price_breakup_concert" size="5" name="price_breakup_concert" value="${concertpricebreakup}" onkeypress="return isNumber(event)"></td>	
  <td><input type="text" id="lower_markup_concert" size="5" name="lower_markup_concert" onkeypress="return isNumber(event)" value="${concertlowermarkup}"/></td>
  <td><input type="text" id="upper_markup_concert" size="5" name="upper_markup_concert" onkeypress="return isNumber(event)" value="${concertuppermarkup}"/></td>
  <td><input type="text" id="lower_shippingFees_concert" size="5" name="lower_shippingFees_concert" onkeypress="return isNumber(event)" value="${concertlowershipping}"/></td>
  <td><input type="text" id="upper_shippingFees_concert" size="5" name="upper_shippingFees_concert" onkeypress="return isNumber(event)" value="${concertuppershipping}"/></td>
 <td><input type="text" id="sectionCount_concert" size="5" name="sectionCount_concert" onkeypress="return isNumber(event)" value="${concertsectioncount}"/></td>
 <td><input type="button" value="Audit"  class="smallButton" id="concertAudit" name="concertAudit"onClick="popupDefaultAutoPricingAudit('concert');"/></td>
 </tr>
<tr>
<td>Theater</td>
<td><select name="mini_exposure_theater" id="mini_exposure_theater"  style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select></td>
				
							
<td>
<select name="auto_exposure_theater" id="auto_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_mini_exposure_theater" id="vip_mini_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theatervipminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theatervipminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theatervipminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>


<td>
<select name="vip_auto_exposure_theater" id="vip_auto_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theatervipautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theatervipautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theatervipautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastrow_mini_exposure_theater" id="lastrow_mini_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastfiverow_mini_exposure_theater" id="lastfiverow_mini_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterlastfiverowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterlastfiverowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterlastfiverowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>


<td>
<select name="zones_pricing_exposure_theater" id="zones_pricing_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterzonespricingexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterzonespricingexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterzonespricingexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="zoned_lastrowmini_exposure_theater" id="zoned_lastrowmini_exposure_theater" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${theaterzonedlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${theaterzonedlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${theaterzonedlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>

<select name="shipping_method_theater" id="shipping_method_theater" style="width:200px;" >
					<option value="0" <c:if test ="${theatershippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${theatershippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${theatershippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${theatershippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${theatershippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${theatershippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${theatershippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${theatershippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${theatershippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${theatershippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
</td>

<td>
<select name="near_term_display_option_theater" id="near_term_display_option_theater" style="width:200px;" >
					<option value="0" <c:if test ="${theaternearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${theaternearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${theaternearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
</td>
  <td><input type="text"  id="rpt_theater" size="5" name="rpt_theater" value="${theaterrpt}" onkeypress="return isNumber(event)"></td>
  <td><input type="text"  id="price_breakup_theater" size="5" name="price_breakup_theater" value="${theaterpricebreakup}" onkeypress="return isNumber(event)"></td>	
  <td><input type="text" id="lower_markup_theater" size="5" name="lower_markup_theater" onkeypress="return isNumber(event)" value="${theaterlowermarkup}"/></td>	
  <td><input type="text" id="upper_markup_theater" size="5" name="upper_markup_theater" onkeypress="return isNumber(event)" value="${theateruppermarkup}"/></td>
  <td><input type="text" id="lower_shippingFees_theater" size="5" name="lower_shippingFees_theater" onkeypress="return isNumber(event)" value="${theaterlowershipping}"/></td>
  <td><input type="text" id="upper_shippingFees_theater" size="5" name="upper_shippingFees_theater" onkeypress="return isNumber(event)" value="${theateruppershipping}"/></td>
 <td><input type="text" id="sectionCount_theater" size="5" name="sectionCount_theater" onkeypress="return isNumber(event)" value="${theatersectioncount}"/></td>	
 <td><input type="button" value="Audit"  class="smallButton" id="theaterAudit" name="theaterAudit"onClick="popupDefaultAutoPricingAudit('theater');"/></td>
 </tr>
<tr>
<!--
<td>Vegas</td>
<td><select name="mini_exposure_vegas" id="mini_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegasminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegasminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegasminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select></td>
<td>
<select name="auto_exposure_vegas" id="auto_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegasautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegasautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegasautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_mini_exposure_vegas" id="vip_mini_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegasvipminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegasvipminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegasvipminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_auto_exposure_vegas" id="vip_auto_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegasvipautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegasvipautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegasvipautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="lastrow_mini_exposure_vegas" id="lastrow_mini_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegaslastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegaslastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegaslastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastfiverow_mini_exposure_vegas" id="lastfiverow_mini_exposure_vegas" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${vegaslastfiverowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${vegaslastfiverowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${vegaslastfiverowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="shipping_method_vegas" id="shipping_method_vegas" style="width:200px;" >
					<option value="0" <c:if test ="${vegasshippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${vegasshippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${vegasshippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${vegasshippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${vegasshippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${vegasshippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${vegasshippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${vegasshippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${vegasshippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${vegasshippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
</td>

<td>
<select name="near_term_display_option_vegas" id="near_term_display_option_vegas" style="width:200px;" >
					<option value="0" <c:if test ="${vegasnearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${vegasnearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${vegasnearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
</td>
  <td><input type="text"  id="rpt_vegas" size="5" name="rpt_vegas" value="${vegasrpt}" onkeypress="return isNumber(event)"></td>
  <td><input type="text"  id="price_breakup_vegas" size="5" name="price_breakup_vegas" value="${vegaspricebreakup}" onkeypress="return isNumber(event)"></td>
  <td><input type="text" id="lower_markup_vegas" size="5" name="lower_markup_vegas" onkeypress="return isNumber(event)" value="${vegaslowermarkup}"/></td>		
  <td><input type="text" id="upper_markup_vegas" size="5" name="upper_markup_vegas" onkeypress="return isNumber(event)" value="${vegasuppermarkup}"/></td>
  <td><input type="text" id="lower_shippingFees_vegas" size="5" name="lower_shippingFees_vegas" onkeypress="return isNumber(event)" value="${vegaslowershipping}"/></td>
  <td><input type="text" id="upper_shippingFees_vegas" size="5" name="upper_shippingFees_vegas" onkeypress="return isNumber(event)" value="${vegasuppershipping}"/></td>
 <td><input type="text" id="sectionCount_vegas" size="5" name="sectionCount_vegas" onkeypress="return isNumber(event)" value="${vegassectioncount}"/></td>
 <td><input type="button" value="Audit"  class="smallButton" id="vegasAudit" name="vegasAudit"onClick="popupDefaultAutoPricingAudit('vegas');"/></td>
</tr>
-->
<tr>
<td>Others</td>
<td><select name="mini_exposure_others" id="mini_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${othersminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${othersminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${othersminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select></td>
<td>
<select name="auto_exposure_others" id="auto_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${othersautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${othersautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${othersautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_mini_exposure_others" id="vip_mini_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${othersvipminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${othersvipminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${othersvipminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="vip_auto_exposure_others" id="vip_auto_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${othersvipautoexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${othersvipautoexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${othersvipautoexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastrow_mini_exposure_others" id="lastrow_mini_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${otherslastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${otherslastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${otherslastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="lastfiverow_mini_exposure_others" id="lastfiverow_mini_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${otherslastfiverowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${otherslastfiverowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${otherslastfiverowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>

<td>
<select name="zones_pricing_exposure_others" id="zones_pricing_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${otherszonespricingexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${otherszonespricingexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${otherszonespricingexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="zoned_lastrowmini_exposure_others" id="zoned_lastrowmini_exposure_others" style="width:200px;" >
					<option value="" >--- Select ---</option>
					<option value="1-OXP" <c:if test ="${otherszonedlastrowminiexposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${otherszonedlastrowminiexposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${otherszonedlastrowminiexposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
</td>
<td>
<select name="shipping_method_others" id="shipping_method_others" style="width:200px;" >
					<option value="0" <c:if test ="${othersshippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${othersshippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${othersshippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${othersshippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${othersshippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${othersshippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${othersshippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${othersshippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${othersshippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${othersshippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
				</select>
</td>

<td>
<select name="near_term_display_option_others" id="near_term_display_option_others" style="width:200px;" >
					<option value="0" <c:if test ="${othersnearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${othersnearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${othersnearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
</td>
  	<td><input type="text"  id="rpt_others" size="5" name="rpt_others" value="${othersrpt}" onkeypress="return isNumber(event)"></td>
  	<td><input type="text"  id="price_breakup_others" size="5" name="price_breakup_others" value="${otherspricebreakup}" onkeypress="return isNumber(event)"></td>	
  	<td><input type="text" id="lower_markup_others" size="5" name="lower_markup_others" onkeypress="return isNumber(event)" value="${otherslowermarkup}"/></td>	
	<td><input type="text" id="upper_markup_others" size="5" name="upper_markup_others" onkeypress="return isNumber(event)" value="${othersuppermarkup}"/></td>
  	<td><input type="text" id="lower_shippingFees_others" size="5" name="lower_shippingFees_others" onkeypress="return isNumber(event)" value="${otherslowershipping}"/></td>
  <td><input type="text" id="upper_shippingFees_others" size="5" name="upper_shippingFees_others" onkeypress="return isNumber(event)" value="${othersuppershipping}"/></td>
 <td><input type="text" id="sectionCount_others" size="5" name="sectionCount_others" onkeypress="return isNumber(event)" value="${otherssectioncount}"/></td>	
 <td><input type="button" value="Audit"  class="smallButton" id="otherAudit" name="otherAudit"onClick="popupDefaultAutoPricingAudit('others');"/></td>
</tr>									
</table>							
<br/>
</div>
<table align ="center">
<td align="center"><input type="button" value="Update"  class="medButton" id="update2" name="update2"onclick="javascript:ebayExposureUpdate();"/></td>
</table>		
	<input type="hidden" name="action" id="action"/>
	<input type="hidden" name="parentCat" id="parentCat"/>
	<br/>
	<br/>
</form:form>