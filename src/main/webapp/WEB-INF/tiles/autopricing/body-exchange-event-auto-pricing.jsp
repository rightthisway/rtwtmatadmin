<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
var otherFrequentEventsCount = 0;
if('${otherFrequentEventsCount}' != null && '${otherFrequentEventsCount}' != '') {
	otherFrequentEventsCount = '${otherFrequentEventsCount}';
}


$(document).ready(function(){
	var allEvents = '${eventsCheckAll}';
	var isUpdate = 'true';
	//copyAll();
	function selectCheckBox(){
		if(allEvents=='true'){
			$('#eventsCheckAll').attr("checked","checked");
		}
		allEvents='false';
	}
	selectCheckBox();
	$('#autoArtistVenue').autocomplete("AutoCompleteGrandChildAndArtistAndVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if(row[0]=='ARTIST'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}else if(row[0]=='VENUE'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else if(row[0]=='CHILD'){
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			} else {
				return "<div class='searchArtistTag'>" +  row[0] + "</div>" + row[2] ;
			}
		}
	}).result(function (event,row,formatted){
			$('#autoArtistVenue').val('');
			$('#selectedValue').text(row[2]);
			if(row[0]=='ARTIST'){
				$('#selectedOption').text('Artist');
				$("#artist").val(row[1]);
				$("#grandChild").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('artistId',row[1]);
				
			} else if(row[0]=='VENUE'){
				$('#selectedOption').text('Venue');
				$("#venue").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#child").val('');
				getEvents('venueId',row[1]);
				
			} else if(row[0]=='CHILD'){
				$('#selectedOption').text('Child');
				$("#child").val(row[1]);
				$("#grandChild").val('');
				$("#artist").val('');
				$("#venue").val('');
				getEvents('childId',row[1]);
				
			} else {
				$('#selectedOption').text('GrandChild');
				$("#grandChild").val(row[1]);
				$("#artist").val('');
				$("#venue").val('');
				$("#child").val('');
				getEvents('grandChildId',row[1]);
				
			} 
	});

	$("#events").change(function () {
		if(isUpdate!='true'){
			$('#ebayInventoryTabel').hide();
			$("#events option:selected").each(function () {
				$('#eventsCheckAll').removeAttr("checked");
			});
		}
		isUpdate='false'; 
	}).trigger('change');
	
	   
	
});
	
function getEvents(isArtist,id){
	/*var InvForm = document.forms.formId;
	var tourList = new Array();
	var flag = false;
	var x = 0; 	
	var i=0;

	for (x=0;x<InvForm.artist.length;x++){
    	if (InvForm.tour[x].selected){
			flag = true;
			tourList[i] =   InvForm.tour[x].value;
			i++;
		}
	}*/
	
	var url = "";
	var brokerId = $("#brokerId").val();
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id+"&brokerId="+brokerId;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id+"&brokerId="+brokerId;
	} else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}
	
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#events').children().remove();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+" "+data.venue+"</option>"
				$('#events').append(rowText);
            }	
		}
	}); 
} 
function validateForm(){
		var flag= true;
		$('.selectCheck:checkbox:checked').each(function () {
			//var id = this.id;
			var tn = this.id.replace('checkbox','isTicketNetworkEvent');
			var vivid = this.id.replace('checkbox','isVividSeatsEvent');
			//var otherExchange = this.id.replace('checkbox','isTickPickEvent');
			//var ebay = this.id.replace('checkbox','isEbayEvent');
			var scorebig = this.id.replace('checkbox','isScoreBigEvent');
			var larryMiniTN = this.id.replace('checkbox','isLarryMiniTNEvent');
			var stubhub = this.id.replace('checkbox','isStubhubEvent');
			var zonesPricingTN = this.id.replace('checkbox','isZonesPricingTNEvent');
			var zonedLastRowMiniTN = this.id.replace('checkbox','isZonedLastRowMiniTNEvent');
			//var exchangeFour = this.id.replace('checkbox','isExchangeFourEvent');
			//var exchangeFive = this.id.replace('checkbox','isExchangeFiveEvent');
			
			var isTN = false,isVividSeat = false,isScorebig=false,isLarryMiniTN = false,isStubHub = false,isMinimamOneExchange=false,isZonesPricingTN=false,isZonedLastRowMiniTN=false;//Currently we are using Stubhub exchange only in LastFiveRowMC project 
			
			
				
					
			if($("#" + tn).attr('checked') ) {
				isTN = true;
				isMinimamOneExchange = true;
			}
			if($("#" + vivid).attr('checked') ) {
				isVividSeat = true;
				isMinimamOneExchange = true;
			} 
			if($("#" + scorebig).attr('checked') ) {
				isScorebig = true;
				isMinimamOneExchange = true;
			}
			if($("#" + larryMiniTN).attr('checked') ) {
				isLarryMiniTN = true;
				isMinimamOneExchange = true;
			}
			if($("#" + stubhub).attr('checked') ) {
				isStubHub = true;
				isMinimamOneExchange = true;
			} 
			if($("#" + zonesPricingTN).attr('checked') ) {
				isZonesPricingTN = true;
				isMinimamOneExchange = true;
			}
			
			if($("#" + zonedLastRowMiniTN).attr('checked') ) {
				isZonedLastRowMiniTN = true;
				isMinimamOneExchange = true;
			}
				
			if(!isMinimamOneExchange){
				alert('Please select minimam one exchanges for every event.');
				$("#"+tn).focus();
				flag = false;
				return ;	
			}
			
			var id,value;
			
			
			id = this.id.replace('checkbox','auto_exposure');
			value = $.trim($("#"+id).val());
			if(value=='') {
				if((isTN || isVividSeat || isScorebig)){
					alert('Auto Exposure can not be blank.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
				
			id = this.id.replace('checkbox','mini_exposure');
			value = $.trim($("#"+id).val());
			if(value=='') {
				if((isTN || isVividSeat || isScorebig)) {
					alert('Mini Exposure can not be blank.');
					$("#"+id).focus();
					flag = false;
					return ;
					
				} else if(isLarryMiniTN) {
					alert('Mini Exposure can not be blank for LarryMini TN Events.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			
			id = this.id.replace('checkbox','vip_mini_exposure');
			value = $.trim($("#"+id).val());
			if(value=='') {
				if((isTN || isVividSeat || isScorebig)){
					alert('Vip Mini Exposure can not be blank.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			id = this.id.replace('checkbox','vip_auto_exposure');
			value = $.trim($("#"+id).val());
			if(value=='') {
				if((isTN || isVividSeat || isScorebig)){
					alert('Vip Auto Exposure can not be blank.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			id = this.id.replace('checkbox','lastrow_mini_exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				if(isTN) {
					alert('LastRow Mini Exposure can not be blank for TicketNetwork Exchange Events.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			id = this.id.replace('checkbox','lastfiverow_mini_exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				if(isStubHub) {
					alert('LastFiveRow Mini Exposure can not be blank for Stubhub Exchange Events.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			
			id = this.id.replace('checkbox','zones_pricing_exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				if(isZonesPricingTN) {
					alert('Zones Pricing Exposure can not be blank for TicketNetwork Exchange Events.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			
			id = this.id.replace('checkbox','zoned_lastrowmini_exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				if(isZonedLastRowMiniTN) {
					alert('Zoned LastrowMini Exposure can not be blank for TicketNetwork Exchange Events.');
					$("#"+id).focus();
					flag = false;
					return ;
				}
			}
			
			id = this.id.replace('checkbox','rptFactor');
			value = $.trim($("#"+id).val());
			if(value=='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid RPT Factor.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','priceBreakup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Price Breakup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperMarkup');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Markup.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','lowerShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Lower Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
			id = this.id.replace('checkbox','upperShippingFees');
			value = $.trim($("#"+id).val());
			if(value =='' || !(/^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/.test(value))){
				alert('Please enter valid Upper Shipping Fees.');
				$("#"+id).focus();
				flag = false;
				return ;
			}
		});
		if(!flag){
			return flag;	
		}
		
		/*
		var count = 0;
		$(".selectCheck:checked").each(function() {
			var id = this.id.replace('checkbox','isFrequentEvent');
			if($("#" + id).attr('checked')) {
				count++;
			}
		});
		
		if(parseFloat(otherFrequentEventsCount) + parseFloat(count) > 200) {
			alert("Frequent Event Exceeds the limit of 200. remove some existing frequent events to add new frequent events.");
			return false;
		}
		*/
		return true;
}
function submitForms(action){
	if(action=='update'){
		if(validateForm()){
			$("#action").val(action);
			$("#formId").submit();
		}
	}else{
		$("#action").val(action);
		$("#formId").submit();
	}
}

$(function () {
	
	$('#copyAllFrequentEvent').click(function(){
		if($('#copyAllFrequentEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isFrequentEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllSectionRange').click(function(){
		if($('#copyAllSectionRange').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('allowSectionRange');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllAutoExposure').click(function(){
		if($('#copyAllAutoExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('auto_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllMiniExposure').click(function(){
		if($('#copyAllMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllVipMiniExposure').click(function(){
		if($('#copyAllVipMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vip_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	}); 
	
	$('#copyAllVipAutoExposure').click(function(){
		if($('#copyAllVipAutoExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('vip_auto_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	}); 
	
	$('#copyAllLastRowMiniExposure').click(function(){
		if($('#copyAllLastRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lastrow_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllLastFiveRowMiniExposure').click(function(){
		if($('#copyAllLastFiveRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lastfiverow_mini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonesPricingExposure').click(function(){
		if($('#copyAllZonesPricingExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zones_pricing_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonedLastRowMiniExposure').click(function(){
		if($('#copyAllZonedLastRowMiniExposure').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('zoned_lastrowmini_exposure');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllShippingMethod').click(function(){
		if($('#copyAllShippingMethod').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_method');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});  
	$('#copyAllNearTermDisplayOption').click(function(){
		if($('#copyAllNearTermDisplayOption').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('near_term_display_option'); 
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyShippingDays').click(function(){
		if($('#copyShippingDays').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('shipping_days'); 
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllLowerMarkup').click(function(){
		if($('#copyAllLowerMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerMarkup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllUpperMarkup').click(function(){
		if($('#copyAllUpperMarkup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperMarkup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllLowerShippingFees').click(function(){
		if($('#copyAllLowerShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('lowerShippingFees');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllUpperShippingFees').click(function(){
		if($('#copyAllUpperShippingFees').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('upperShippingFees');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllRptFactor').click(function(){
		if($('#copyAllRptFactor').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('rptFactor');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllPriceBreakup').click(function(){
		if($('#copyAllPriceBreakup').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyTextField('priceBreakup');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	
	$('#copyAllTicketNetworkEvent').click(function(){
		if($('#copyAllTicketNetworkEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isTicketNetworkEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllVividSeatsEvent').click(function(){
		if($('#copyAllVividSeatsEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isVividSeatsEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	/*$('#copyAllTickPickEvent').click(function(){
		if($('#copyAllTickPickEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isTickPickEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllEbayEvent').click(function(){
		if($('#copyAllEbayEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isEbayEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});*/
	$('#copyAllScoreBigEvent').click(function(){
		if($('#copyAllScoreBigEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isScoreBigEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllLarryMiniTNEvent').click(function(){
		if($('#copyAllLarryMiniTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isLarryMiniTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllStubhubEvent').click(function(){
		if($('#copyAllStubhubEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isStubhubEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	
	$('#copyAllZonesPricingTNEvent').click(function(){
		if($('#copyAllZonesPricingTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isZonesPricingTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllZonedLastRowMiniTNEvent').click(function(){
		if($('#copyAllZonedLastRowMiniTNEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isZonedLastRowMiniTNEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	/*$('#copyAllExchangeFourEvent').click(function(){
		if($('#copyAllExchangeFourEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isExchangeFourEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});
	$('#copyAllExchangeFiveEvent').click(function(){
		if($('#copyAllExchangeFiveEvent').attr('checked')){
			$('.selectCheck').attr('checked', true);
			copyCheckBox('isExchangeFiveEvent');
		}else{
			$('.selectCheck').attr('checked', false);
		}
	});*/
	
	$('#copyAll').click(function(){
		copyAll();
	});
});

function copyAll(){
	if($('#copyAll').attr('checked')){
		$('.selectCheck').attr('checked', true);
		copyCheckBox('isFrequentEvent');
		copyCheckBox('allowSectionRange');
		//copyTextField('exposure');
		copyTextField('auto_exposure');
		copyTextField('mini_exposure');
		copyTextField('vip_mini_exposure');
		copyTextField('vip_auto_exposure');
		copyTextField('lastrow_mini_exposure');
		copyTextField('lastfiverow_mini_exposure');
		copyTextField('zones_pricing_exposure');
		copyTextField('zoned_lastrowmini_exposure');
		
		copyTextField('shipping_method');
		copyTextField('near_term_display_option'); 
		copyTextField('shipping_days');    
		
		copyTextField('lowerMarkup');
		copyTextField('upperMarkup');
		copyTextField('lowerShippingFees');
		copyTextField('upperShippingFees');
		copyTextField('rptFactor');
		copyTextField('priceBreakup');
		
		copyCheckBox('isTicketNetworkEvent');
		copyCheckBox('isVividSeatsEvent');
		//copyCheckBox('isTickPickEvent');
		//copyCheckBox('isEbayEvent');
		
		copyCheckBox('isScoreBigEvent');
		copyCheckBox('isLarryMiniTNEvent');
		copyCheckBox('isStubhubEvent');
		copyCheckBox('isZonesPricingTNEvent');
		copyCheckBox('isZonedLastRowMiniTNEvent');
		//copyCheckBox('isExchangeFourEvent');
		//copyCheckBox('isexchangeFiveEvent');
		
	}else{
		$('.selectCheck').attr('checked', false);
	}
}
function copyCheckBox(fieldName){
	var isFirst = true;
	var firstFieldValue = false;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			if($("#" + id).attr('checked')) {
				firstFieldValue = true;
			}
			isFirst= false;
		}else{
			$("#" + id ).attr('checked', firstFieldValue);
		}
	});
}
function copyTextField(fieldName){
	var isFirst = true;
	var firstFieldValue;
	$(".selectCheck:checked").each(function() {
		var id = this.id.replace('checkbox',fieldName);
		if(isFirst){
			firstFieldValue = $("#" + id ).val();
			isFirst= false;
		}else{
			$("#" + id ).val(firstFieldValue);
		}
	});
}
function selectRow(id) {
	$("#checkbox_" + id ).attr('checked', true);
}

function selectAllEvents(){
	if((document.getElementById("eventsCheckAll").checked)){
		$("#events").each(function(){
			$("#events option").attr("selected","selected"); 
		});

	}
	else{
			$("#events").each(function(){
			$("#events option").removeAttr("selected"); 
		});
	}
}

function popupUpdateTGCatsPricingAudit(eId){
	var url = "ExchangeEventAutoPricingAuditPopup?eId="+eId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
} 
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Manage AutoPricing
</div>
<h1>Manage AutoPricing</h1>
<br/>
<b>Total ${totalFrequentEvents } Frequent Events&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<form action="FrequentEvents">
	<input type="submit" value="Frequent Event" class="medButton"/>
</form>
<hr/>
<form:form action="ExchangeEventAutoPricing" method="post" id="formId">
<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	<br/>
	<br/>
	<table width="80%">
		<tr>
			<td><b>Select Broker:</b></td>
			<td>
				<select id="brokerId" name="brokerId">
					<option value="">---ALL---</option>
					<c:forEach items="${brokers }" var="brokerObj">
						<option value="${brokerObj.id }"
						<c:if test="${broker ne null and brokerObj.id eq broker}"> Selected </c:if>
						>${brokerObj.name}</option>
					</c:forEach>
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Select Child or GrandChild or Artist or Venue:</b></td>
			<td>
				<input type="hidden" id="artist" name="artist" value ="${artist}">
				<input type="hidden" id="venue" name="venue" value ="${venue}">
				<input type="hidden" id="grandChild" name="grandChild" value ="${grandChild}">
				<input type="hidden" id="brokerId" name="brokerId" value ="${brokerId}">
				<input type="text" id="autoArtistVenue" name="autoArtistVenue">
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b><span id="selectedOption" >${selectedOption}</span></b></td>
			<td><b><span id="selectedValue" >${selectedValue}</span></b></td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td><b>Events:</b></td>
			<td >
				<input type="hidden" id="eventStr" name="eventStr" value = ${eventStr}/> 
				<input type="checkbox" id="eventsCheckAll" name="eventsCheckAll" onclick ="selectAllEvents()" /> 
				<label for="eventsCheckAll">Select All</label>
				<br/>
				<select id='events' name="events"  multiple="multiple" size='6' style="width:480px;" >
					<c:forEach items="${events}" var="event">
						<c:set var='temp' value=","/>
						<c:set var="temp2" value="${event.id}${temp}"/>
						<option 
							<c:if test="${event!=null && fn:contains(eventStr,temp2)}"> Selected </c:if>
							value="${event.id}"> ${event.name} <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose>
								${event.venue.building}
						</option>
					</c:forEach> 
				</select>
			</td>
			<td></td>
			<td colspan="3" align="center"></td>
			
		</tr>
		<tr>
			<td colspan="6" align="center">
				<input type="button" value="Get Pricing" onclick="submitForms('search');" class="medButton">
			</td>
		</tr>
	</table>
	
	<c:if test="${not empty categories}" >
	<div id="ebayInventoryTabel" >
	
		
		<div style="float:right; padding-right:320px;">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div>
		<div id="copyCheckBox" style="float:right;">
			<!--<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" >Copy first row  to all remaining rows &nbsp;&nbsp;&nbsp; -->
		</div>
		<br/>
		<br/>
		<br/>
		<display:table list="${categories}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="category"  requestURI="#" class="list">
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${category.event.id}" id="checkbox_${category.event.id}" <c:if test ="${category.lowerMarkup !=null  }"> checked="checked" </c:if> class="selectCheck" />
			</display:column>
			<display:column title="EvenName" sortable="true">${category.event.name}</display:column>
			<display:column title="Event Date And Time" sortable="true">

				<fmt:formatDate pattern="MM/dd/yyyy" value="${category.event.localDate}" />				
					<c:choose>
						<c:when test="${category.event.localTime == null}">
							TBD
						</c:when>
						<c:otherwise>
							<fmt:formatDate pattern="hh:mm aa" value="${category.event.localTime}" />
						</c:otherwise>
					</c:choose>
			</display:column>
			<display:column title="Venue" sortable="true">${category.event.venue.building},${category.event.venue.state},${category.event.venue.city},${category.event.venue.country}</display:column>
			<display:column title='Frequent Event<br/><input type="checkbox" name="copyAllFrequentEvent" id="copyAllFrequentEvent" >' >
				<input type="checkbox" name="isFrequentEvent_${category.event.id}" id="isFrequentEvent_${category.event.id}" onchange="selectRow('${category.event.id}');" <c:if test ="${category.isFrequentEvent !=null && category.isFrequentEvent}"> checked="checked" </c:if> />
			</display:column>
			<display:column title='PreSale<br/><input type="checkbox" name="copyAllSectionRange" id="copyAllSectionRange" >' >
				<input type="checkbox" name="allowSectionRange_${category.event.id}" id="allowSectionRange_${category.event.id}" 
				onchange="selectRow('${category.event.id}');" <c:if test ="${category.allowSectionRange !=null && category.allowSectionRange}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Auto Exposure<br/><input type="checkbox" name="copyAllAutoExposure" id="copyAllAutoExposure" >' >
				<select name="auto_exposure_${category.event.id}" id="auto_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.autoExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.autoExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.autoExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			<display:column title='Mini Exposure<br/><input type="checkbox" name="copyAllMiniExposure" id="copyAllMiniExposure" >' >
				<select name="mini_exposure_${category.event.id}" id="mini_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.miniExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.miniExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.miniExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			<display:column title='VipMini Exposure<br/><input type="checkbox" name="copyAllVipMiniExposure" id="copyAllVipMiniExposure" >' >
				<select name="vip_mini_exposure_${category.event.id}" id="vip_mini_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.vipMiniExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.vipMiniExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.vipMiniExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			<display:column title='VipAuto Exposure<br/><input type="checkbox" name="copyAllVipAutoExposure" id="copyAllVipAutoExposure" >' >
				<select name="vip_auto_exposure_${category.event.id}" id="vip_auto_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.vipAutoExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.vipAutoExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.vipAutoExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			<display:column title='LastRow Mini Exposure<br/><input type="checkbox" name="copyAllLastRowMiniExposure" id="copyAllLastRowMiniExposure" >' >
				<select name="lastrow_mini_exposure_${category.event.id}" id="lastrow_mini_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.lastRowMiniExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.lastRowMiniExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.lastRowMiniExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			<display:column title='LastFiveRow Mini Exposure<br/><input type="checkbox" name="copyAllLastFiveRowMiniExposure" id="copyAllLastFiveRowMiniExposure" >' >
				<select name="lastfiverow_mini_exposure_${category.event.id}" id="lastfiverow_mini_exposure_${category.event.id}" class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.lastFiveRowMiniExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.lastFiveRowMiniExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.lastFiveRowMiniExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			<display:column title='Zones Pricing Exposure<br/><input type="checkbox" name="copyAllZonesPricingExposure" id="copyAllZonesPricingExposure" >' >
				<select name="zones_pricing_exposure_${category.event.id}" id="zones_pricing_exposure_${category.event.id}" 
				class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.zonesPricingExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.zonesPricingExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.zonesPricingExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			<display:column title='Zoned LastRowMini Exposure<br/><input type="checkbox" name="copyAllZonedLastRowMiniExposure" id="copyAllZonedLastRowMiniExposure" >' >
				<select name="zoned_lastrowmini_exposure_${category.event.id}" id="zoned_lastrowmini_exposure_${category.event.id}" 
				class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="">--- Select ---</option>
					<option value="1-OXP" <c:if test ="${category.zonedLastRowMiniExposure eq '1-OXP'}" > selected </c:if> >1-OXP</option>
					<option value="2-OXP" <c:if test ="${category.zonedLastRowMiniExposure eq '2-OXP'}" > selected </c:if> >2-OXP</option>
					<option value="3-OXP" <c:if test ="${category.zonedLastRowMiniExposure eq '3-OXP'}" > selected </c:if> >3-OXP</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Method<br/><input type="checkbox" name="copyAllShippingMethod" id="copyAllShippingMethod" >' >
				<select name="shipping_method_${category.event.id}" id="shipping_method_${category.event.id}" 
						class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="0" <c:if test ="${category.shippingMethod eq '0'}" > selected </c:if> >Default Website Setting</option>
					<option value="1" <c:if test ="${category.shippingMethod eq '1'}" > selected </c:if> >E-Ticket</option>
					<option value="2" <c:if test ="${category.shippingMethod eq '2'}" > selected </c:if> >Will Call</option>
					<option value="3" <c:if test ="${category.shippingMethod eq '3'}" > selected </c:if> >Local Pickup Near Venue</option>
					<option value="4" <c:if test ="${category.shippingMethod eq '4'}" > selected </c:if> >E-Ticket or Will Call</option>
					<option value="5" <c:if test ="${category.shippingMethod eq '5'}" > selected </c:if> >Will Call or Local Pickup Near Venue</option>
					<option value="6" <c:if test ="${category.shippingMethod eq '6'}" > selected </c:if> >E-Ticket or Local Pickup Near Venue</option>
					<option value="7" <c:if test ="${category.shippingMethod eq '7'}" > selected </c:if> >E-Ticket or Will Call or Local Pickup Near Venue</option>
					<option value="8" <c:if test ="${category.shippingMethod eq '8'}" > selected </c:if> >Paperless (Meet Seller at Venue)</option>
					<option value="9" <c:if test ="${category.shippingMethod eq '9'}" > selected </c:if> >Electronic Transfer</option>
					
				</select>
			</display:column> 
			
			<display:column title='Near Term Display Option<br/><input type="checkbox" name="copyAllNearTermDisplayOption" id="copyAllNearTermDisplayOption" >' >
				<select name="near_term_display_option_${category.event.id}" id="near_term_display_option_${category.event.id}" 
						class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="0" <c:if test ="${category.nearTermDisplayOption eq '0'}" > selected </c:if> >Default near-term display options</option>
					<option value="1" <c:if test ="${category.nearTermDisplayOption eq '1'}" > selected </c:if> >Always show near-term shipping</option>
					<option value="2" <c:if test ="${category.nearTermDisplayOption eq '2'}" > selected </c:if> >Only show near-term shipping</option>
				</select>
			</display:column>
			
			<display:column title='Shipping Days<br/><input type="checkbox" name="copyShippingDays" id="copyShippingDays" >' >
				<select name="shipping_days_${category.event.id}" id="shipping_days_${category.event.id}" 
						class="shippingMethodText" onchange="selectRow('${category.event.id}');" style="width:100px;" >
					<option value="2" <c:if test ="${category.shippingDays eq '2'}" > selected </c:if> >2 Days</option>
					<option value="5" <c:if test ="${category.shippingDays eq '5'}" > selected </c:if> >5 Days</option>
				</select>
			</display:column>
			
			<display:column title='RPT Factor<br/><input type="checkbox" name="copyAllRptFactor" id="copyAllRptFactor" >' >
				<input type="text" value="${category.rptFactor}" name="rptFactor_${category.event.id}" id="rptFactor_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Price Breakup<br/><input type="checkbox" name="copyAllPriceBreakup" id="copyAllPriceBreakup" >' >
				<input type="text" value="${category.priceBreakup}" name="priceBreakup_${category.event.id}" id="priceBreakup_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Lower Markup<br/><input type="checkbox" name="copyAllLowerMarkup" id="copyAllLowerMarkup" >' >
				<input type="text" value="${category.lowerMarkup}" name="lowerMarkup_${category.event.id}" id="lowerMarkup_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			<display:column title='Upper Markup<br/><input type="checkbox" name="copyAllUpperMarkup" id="copyAllUpperMarkup" >' >
				<input type="text" value="${category.upperMarkup}" name="upperMarkup_${category.event.id}" id="upperMarkup_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Lower Shipping Fees<br/><input type="checkbox" name="copyAllLowerShippingFees" id="copyAllLowerShippingFees" >' >
				<input type="text" value="${category.lowerShippingFees}" name="lowerShippingFees_${category.event.id}" id="lowerShippingFees_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='Upper Shipping Fees<br/><input type="checkbox" name="copyAllUpperShippingFees" id="copyAllUpperShippingFees" >' >
				<input type="text" value="${category.upperShippingFees}" name="upperShippingFees_${category.event.id}" id="upperShippingFees_${category.event.id}" onchange="selectRow('${category.event.id}');" size="5" class="markupText"/>
			</display:column>
			
			<display:column title='TicketNetwork Event<br/><input type="checkbox" name="copyAllTicketNetworkEvent" id="copyAllTicketNetworkEvent" >' >
				<input type="checkbox" name="isTicketNetworkEvent_${category.event.id}" id="isTicketNetworkEvent_${category.event.id}" <c:if test ="${category.isTicketNetworkEvent !=null && category.isTicketNetworkEvent}"> checked="checked" </c:if> />
			</display:column>
			<display:column title='VividSeats Event<br/><input type="checkbox" name="copyAllVividSeatsEvent" id="copyAllVividSeatsEvent" >' >
				<input type="checkbox" name="isVividSeatsEvent_${category.event.id}" id="isVividSeatsEvent_${category.event.id}" <c:if test ="${category.isVividSeatsEvent !=null && category.isVividSeatsEvent}"> checked="checked" </c:if> />
			</display:column>
			<%-- <display:column title='TickPick Event<br/><input type="checkbox" name="copyAllTickPickEvent" id="copyAllTickPickEvent" >' >
				<input type="checkbox" name="isTickPickEvent_${category.event.id}" id="isTickPickEvent_${category.event.id}" <c:if test ="${category.isTickPickEvent !=null && category.isTickPickEvent}"> checked="checked" </c:if> />
			</display:column> --%>
			<display:column title='ScoreBig Event<br/><input type="checkbox" name="copyAllScoreBigEvent" id="copyAllScoreBigEvent" >' >
				<input type="checkbox" name="isScoreBigEvent_${category.event.id}" id="isScoreBigEvent_${category.event.id}" <c:if test ="${category.isScoreBigEvent !=null && category.isScoreBigEvent}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='LarryLast TN Event<br/><input type="checkbox" name="copyAllLarryMiniTNEvent" id="copyAllLarryMiniTNEvent" >' >
				<input type="checkbox" name="isLarryMiniTNEvent_${category.event.id}" id="isLarryMiniTNEvent_${category.event.id}" 
				<c:if test ="${category.isLarryMiniTNEvent !=null && category.isLarryMiniTNEvent}"> checked="checked" </c:if> />
			</display:column>
			
			
			<display:column title='LastFiveRow Stubhub Event<br/><input type="checkbox" name="copyAllStubhubEvent" id="copyAllStubhubEvent" >' >
				<input type="checkbox" name="isStubhubEvent_${category.event.id}" id="isStubhubEvent_${category.event.id}" 
				<c:if test ="${category.isStubhubEvent !=null && category.isStubhubEvent}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='ZonesPricing TN Event<br/><input type="checkbox" name="copyAllZonesPricingTNEvent" id="copyAllZonesPricingTNEvent" >' >
				<input type="checkbox" name="isZonesPricingTNEvent_${category.event.id}" id="isZonesPricingTNEvent_${category.event.id}" 
				<c:if test ="${category.isZonesPricingTNEvent !=null && category.isZonesPricingTNEvent}"> checked="checked" </c:if> />
			</display:column>
			
			<display:column title='Zoned LastRowMini TN Event<br/><input type="checkbox" name="copyAllZonedLastRowMiniTNEvent" id="copyAllZonedLastRowMiniTNEvent" >' >
				<input type="checkbox" name="isZonedLastRowMiniTNEvent_${category.event.id}" id="isZonedLastRowMiniTNEvent_${category.event.id}" 
				<c:if test ="${category.isZonedLastRowMiniTNEvent !=null && category.isZonedLastRowMiniTNEvent}"> checked="checked" </c:if> />
			</display:column>
			
			<%-- <display:column title='ExchangeFour Event<br/><input type="checkbox" name="copyAllExchangeFourEvent" id="copyAllExchangeFourEvent" >' >
				<input type="checkbox" name="isExchangeFourEvent_${category.event.id}" id="isExchangeFourEvent_${category.event.id}" <c:if test ="${category.isExchangeFourEvent !=null && category.isExchangeFourEvent}"> checked="checked" </c:if> />
			</display:column>
			<display:column title='ExchangeFive Event<br/><input type="checkbox" name="copyAllExchangeFiveEvent" id="copyAllExchangeFiveEvent" >' >
				<input type="checkbox" name="isExchangeFiveEvent_${category.event.id}" id="isExchangeFiveEvent_${category.event.id}" <c:if test ="${category.isExchangeFiveEvent !=null && category.isExchangeFiveEvent}"> checked="checked" </c:if> />
			</display:column>	 --%>		
			
			<display:column title='Broker Event Status' >
				${category.event.brokerStatus}
			</display:column>
			
			<display:column title="Action" >
			 	<input type="button" value="Audit"    class="medButton" id="audit_${category.event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${category.event.id}');"/>
			 	
			 </display:column>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:320px;">
				<input type="button" value="Update"    class="medButton" id="update2" name="update2"onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty categories}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
		<table id="listAudit"></table>
		<br/>
		<br/>
	</c:if>
	<input type="hidden" name="action" id="action"/>
	<br/>
	<br/>
	
</form:form>