
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
	 
</div>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"  id="audit"  requestURI="#" class="list">

			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.createdDate}" />					
			</display:column>
			<display:column title="User">${audit.userName}</display:column> 
			
			<display:column title="Frequent Event"> 
				<c:choose>
						<c:when test="${audit.frequentEvent == null}">
							No
						</c:when>
						<c:when test="${audit.frequentEvent}">
							Yes
						</c:when>
						<c:otherwise>
							No
						</c:otherwise>
					</c:choose>
				
			</display:column>
			<display:column title="PreSale"> 
				<c:choose>
						<c:when test="${audit.allowSectionRange == null}">
							No
						</c:when>
						<c:when test="${audit.allowSectionRange}">
							Yes
						</c:when>
						<c:otherwise>
							No
						</c:otherwise>
					</c:choose>
				
			</display:column>
			<display:column title="AutoExposure">
				<c:choose>
					<c:when test="${audit.autoExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.autoExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="MiniExposure">
				<c:choose>
					<c:when test="${audit.miniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.miniExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="VipMiniExposure">
				<c:choose>
					<c:when test="${audit.vipMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.vipMiniExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="VipAutoExposure">
				<c:choose>
					<c:when test="${audit.vipAutoExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.vipAutoExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="LastRow MiniExposure">
				<c:choose>
					<c:when test="${audit.lastRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.lastRowMiniExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="LastFiveRow Mini MiniExposure">
				<c:choose>
					<c:when test="${audit.lastFiveRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.lastFiveRowMiniExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Zones Pricing Exposure">
				<c:choose>
					<c:when test="${audit.zonesPricingExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.zonesPricingExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Zoned LastRowMini Exposure">
				<c:choose>
					<c:when test="${audit.zonedLastRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.zonedLastRowMiniExposure}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Shipping Method">
				<c:choose>
					<c:when test="${audit.shippingMethod == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.shippingMethod eq '0'}" >Default Website Setting</c:if>
						<c:if test ="${audit.shippingMethod eq '1'}" >E-Ticket</c:if>
						<c:if test ="${audit.shippingMethod eq '2'}" >Will Call</c:if>
						<c:if test ="${audit.shippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
						<c:if test ="${audit.shippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.shippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
						<c:if test ="${audit.shippingMethod eq '9'}" >Electronic Transfer</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Near Term Display option">
				<c:choose>
					<c:when test="${audit.nearTermDisplayOption == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.nearTermDisplayOption eq '0'}" >Default near-term display options</c:if>
						<c:if test ="${audit.nearTermDisplayOption eq '1'}" >Always show near-term shipping</c:if>
						<c:if test ="${audit.nearTermDisplayOption eq '2'}" >Only show near-term shipping</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Shipping Days">
				<c:choose>
					<c:when test="${audit.shippingDays == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.shippingDays eq '2'}" >2 Days</c:if>
						<c:if test ="${audit.shippingDays eq '5'}" >5 Days</c:if>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="RPT Factor">
				<c:choose>
					<c:when test="${audit.rptFactor == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.rptFactor}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			
			<display:column title="Price Breakup">
				<c:choose>
					<c:when test="${audit.priceBreakup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.priceBreakup}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			
		 <display:column title="Lower Markup" sortable="true" headerClass="sortable"> 
				<c:choose>
						<c:when test="${audit.lowerMarkup == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.lowerMarkup}
						</c:otherwise>
					</c:choose>
			</display:column>	
			<display:column title="Upper Markup">
				<c:choose>
					<c:when test="${audit.upperMarkup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.upperMarkup}
					</c:otherwise>
				</c:choose>
				
			</display:column>
			<display:column title="Lower Shipping Fees">
				<c:choose>
					<c:when test="${audit.lowerShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.lowerShippingFees}
					</c:otherwise>
				</c:choose>
			</display:column>	
			<display:column title="Upper Shipping Fees">
				<c:choose>
					<c:when test="${audit.upperShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.upperShippingFees}
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TicketNetwork Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'TNE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="VividSeats Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'Vivid')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<%-- <display:column title="TickPick Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'TickPick')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column> --%>
			
			<display:column title="ScoreBig Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'ScoreBig')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<%-- <display:column title="Ebay Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'EBAY')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column> --%>
			
			<display:column title="LarryMini Theater Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'LarryMini')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="LastFiveRow Stubhub Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'LastFiveRowMCStub')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Zones Pricing TN Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'ZonesPricingTN')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Zoned LastRowMini TN Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'ZonedLastRowTN')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<%-- <display:column title="Exchange Four Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'EXFOUR')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="exchange Five Event">
				<c:choose>
					<c:when test="${audit.exchanges != null && fn:contains(audit.exchanges,'EXFIVE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column> --%>
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit for this Event.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>