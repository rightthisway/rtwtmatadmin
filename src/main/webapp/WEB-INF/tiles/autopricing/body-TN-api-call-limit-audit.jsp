<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<h1>Audit For ${project}</h1>

<display:table id="tnApiCallLimit" list="${auditList }">
		
	<display:column title="Updated By" property="userName" sortable="true"></display:column>
	<display:column title="Updated Date" property="createDate" sortable="true"></display:column>
	<display:column title="Create Call Day" property="createCallDay" sortable="true"></display:column>
	<display:column title="Create Call Minute" property="createCallMinute" sortable="true"></display:column>
	<display:column title="Delete Call Minute" property="deleteCallMinute" sortable="true"></display:column>
	<display:column title="Update Call Minute" property="updateCallMinute" sortable="true"></display:column>	
		
</display:table>
	