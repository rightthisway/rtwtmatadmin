
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

		<display:column title="User">${audit.userName}</display:column>
			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Modified Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.createdDate}" />					
			</display:column>
			
			<display:column title="TicketNetwork Markup">
				<c:choose>
					<c:when test="${audit.oldTicketNetwork == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldTicketNetwork}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newTicketNetwork == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newTicketNetwork}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="VividSeat Markup">
				<c:choose>
					<c:when test="${audit.oldVividSeats == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldVividSeats}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newVividSeats == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newVividSeats}
				</c:otherwise>
				</c:choose>
			</display:column>
			
		<%--  <display:column title="TickPick Markup"> 
				<c:choose>
						<c:when test="${audit.oldOtherExchange == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.oldOtherExchange}
						</c:otherwise>
					</c:choose>
			&nbsp;/&nbsp;
				<c:choose>
						<c:when test="${audit.newOtherExchange == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.newOtherExchange}
						</c:otherwise>
					</c:choose>
				
			</display:column> --%>	
			<display:column title="ScoreBig Markup">
				<c:choose>
					<c:when test="${audit.oldScorebig == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldScorebig}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newScorebig == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newScorebig}
				</c:otherwise>
				</c:choose>
			</display:column>
			<%-- <display:column title="Ebay Markup">
				<c:choose>
					<c:when test="${audit.oldEbay == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldEbay}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newEbay == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newEbay}
				</c:otherwise>
				</c:choose>
			</display:column> --%>
			<display:column title="Stubhub Markup">
				<c:choose>
					<c:when test="${audit.oldStubhub == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldStubhub}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newStubhub == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newStubhub}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Exchange3 Markup">
				<c:choose>
					<c:when test="${audit.oldExchange3 == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldExchange3}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newExchange3 == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newExchange3}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Exchange4 Markup">
				<c:choose>
					<c:when test="${audit.oldExchange4 == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldExchange4}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newExchange4 == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newExchange4}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Exchange5 Markup">
				<c:choose>
					<c:when test="${audit.oldExchange5 == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldExchange5}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newExchange5 == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newExchange5}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>