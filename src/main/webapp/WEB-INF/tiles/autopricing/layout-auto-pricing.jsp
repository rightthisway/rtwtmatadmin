<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu"
	scope="request" />

<div id="editorSubMenu">
	<c:choose>
		<c:when test="${selectedSubMenu == 'AutoPricing'}">
			<b>AutoPricing</b>
		</c:when>
		<c:otherwise>
			<a href="ExchangeEventAutoPricing">AutoPricing</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Global Autopricing'}">
			<b>Global Autopricing</b>
		</c:when>
		<c:otherwise>
			<a href="GlobalAutoPricing">Global Autopricing</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Default Autopricing'}">
			<b>Default Autopricing</b>
		</c:when>
		<c:otherwise>
			<a href="DefaultAutopricing">Default Autopricing</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Exchange Markup'}">
			<b>Exchange Markup</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingEditExchangeMarkup">Exchange Markup</a>
		</c:otherwise>
	</c:choose>
	|
	<%-- <c:choose>
		<c:when test="${selectedSubMenu == 'TN API Call Limit'}">
			<b>TN API Call Limit</b>
		</c:when>
		<c:otherwise>
			<a href="TnApiCallLimit">TN API Call Limit</a>
		</c:otherwise>
	</c:choose>
	| --%>
	<c:choose>
		<c:when test="${selectedSubMenu == 'Log Files'}">
			<b>Log Files</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingEditLogFiles">Log Files</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'AutoPricing Ticket Details'}">
			<b>AutoPricing Tickets Details</b>
		</c:when>
		<c:otherwise>
			<a href="AutopricingTicketDetails">AutoPricing Ticket Details</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubMenu == 'Broker Events'}">
			<b>Manage Broker Events</b>
		</c:when>
		<c:otherwise>
			<a href="ManageBrokerEvents">Manage Broker Events</a>
		</c:otherwise>
	</c:choose>

</div>
<tiles:insertAttribute name="subBody" />