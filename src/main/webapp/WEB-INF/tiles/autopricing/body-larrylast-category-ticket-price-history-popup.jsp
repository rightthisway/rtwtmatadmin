
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
	 
</div>

<c:if test="${categoryTicket ne null}" >

     <b>Section : ${categoryTicket.section}</b>
     <br/>
     <b>LastRow : ${categoryTicket.lastRow}</b>
     <br/>
     <b>Quantity : ${categoryTicket.quantity}</b>
     <br/>
</c:if>
<c:if test="${not empty ticketPriceList}" >
	<div id="auditTable" >
   	<h2> Price History </h2>
		<display:table list="${ticketPriceList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="CTList"  requestURI="#" class="list">			
			<display:column title="Date">
				<c:set var="string1" value="${CTList.popDate}"/>
					<c:set var="string2" value="${fn:replace(string1,' ','&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp')}" />
					<p>${string2}</p>	
			</display:column>
			
			<display:column  title ="Price">
			${CTList.popPrice}
			</display:column>
			
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${categoryTicket eq null}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any larry last pricing details for this selection.</span></h4> 
	</div>
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>