
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

		<display:column title="Category">${audit.category}</display:column>
		<display:column title="User">${audit.userName}</display:column>
			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Modified Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.createdDate}" />					
			</display:column>
			
			<display:column title="Mini Exposure">
				<c:choose>
					<c:when test="${audit.oldMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldMiniExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newMiniExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newMiniExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Auto Exposure">
				<c:choose>
					<c:when test="${audit.oldAutoExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldAutoExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newAutoExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newAutoExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Vip Mini Exposure">
				<c:choose>
					<c:when test="${audit.oldVipMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldVipMiniExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newVipMiniExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newVipMiniExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Vip Auto Exposure">
				<c:choose>
					<c:when test="${audit.oldVipAutoExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldVipAutoExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newVipAutoExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newVipAutoExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="LastRow Mini Exposure">
				<c:choose>
					<c:when test="${audit.oldLastRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldLastRowMiniExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newLastRowMiniExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newLastRowMiniExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="LastFiveRow Mini Exposure">
				<c:choose>
					<c:when test="${audit.oldLastFiveRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldLastFiveRowMiniExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newLastFiveRowMiniExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newLastFiveRowMiniExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Zones Pricing Exposure">
				<c:choose>
					<c:when test="${audit.oldZonesPricingExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldZonesPricingExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newZonesPricingExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newZonesPricingExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Zoned LastRowMini Exposure">
				<c:choose>
					<c:when test="${audit.oldZonedLastRowMiniExposure == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldZonedLastRowMiniExposure}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newZonedLastRowMiniExposure == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newZonedLastRowMiniExposure}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Shipping Method">
				<c:choose>
					<c:when test="${audit.oldShippingMethod == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.oldShippingMethod eq '0'}" >Default Website Setting</c:if>
						<c:if test ="${audit.oldShippingMethod eq '1'}" >E-Ticket</c:if>
						<c:if test ="${audit.oldShippingMethod eq '2'}" >Will Call</c:if>
						<c:if test ="${audit.oldShippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
						<c:if test ="${audit.oldShippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
						<c:if test ="${audit.oldShippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.oldShippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.oldShippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.oldShippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
						<c:if test ="${audit.oldShippingMethod eq '9'}" >Electronic Transfer</c:if>
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newShippingMethod == null}">
					-
				</c:when>
				<c:otherwise>
						<c:if test ="${audit.newShippingMethod eq '0'}" >Default Website Setting</c:if>
						<c:if test ="${audit.newShippingMethod eq '1'}" >E-Ticket</c:if>
						<c:if test ="${audit.newShippingMethod eq '2'}" >Will Call</c:if>
						<c:if test ="${audit.newShippingMethod eq '3'}" >Local Pickup Near Venue</c:if>
						<c:if test ="${audit.newShippingMethod eq '4'}" >E-Ticket or Will Call</c:if>
						<c:if test ="${audit.newShippingMethod eq '5'}" >Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.newShippingMethod eq '6'}" >E-Ticket or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.newShippingMethod eq '7'}" >E-Ticket or Will Call or Local Pickup Near Venue</c:if>
						<c:if test ="${audit.newShippingMethod eq '8'}" >Paperless (Meet Seller at Venue)</c:if>
						<c:if test ="${audit.newShippingMethod eq '9'}" >Electronic Transfer</c:if>
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Near Term Display Option">
				<c:choose>
					<c:when test="${audit.oldNearTermDisplayOption == null}">
						-
					</c:when>
					<c:otherwise>
						<c:if test ="${audit.oldNearTermDisplayOption eq '0'}" >Default near-term display options</c:if>
						<c:if test ="${audit.oldNearTermDisplayOption eq '1'}" >Always show near-term shipping</c:if>
						<c:if test ="${audit.oldNearTermDisplayOption eq '2'}" >Only show near-term shipping</c:if>
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newNearTermDisplayOption == null}">
					-
				</c:when>
				<c:otherwise>
						<c:if test ="${audit.newNearTermDisplayOption eq '0'}" >Default near-term display options</c:if>
						<c:if test ="${audit.newNearTermDisplayOption eq '1'}" >Always show near-term shipping</c:if>
						<c:if test ="${audit.newNearTermDisplayOption eq '2'}" >Only show near-term shipping</c:if>
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="RPT Factor">
				<c:choose>
					<c:when test="${audit.oldRptFactor == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldRptFactor}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newRptFactor == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newRptFactor}
				</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Price Breakup">
				<c:choose>
					<c:when test="${audit.oldPriceBreakup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldPriceBreakup}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newPriceBreakup == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newPriceBreakup}
				</c:otherwise>
				</c:choose>
			</display:column>
			
		 <display:column title="Lower Markup" > 
				<c:choose>
						<c:when test="${audit.oldLowerMarkup == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.oldLowerMarkup}
						</c:otherwise>
					</c:choose>
			&nbsp;/&nbsp;
				<c:choose>
						<c:when test="${audit.newLowerMarkup == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.newLowerMarkup}
						</c:otherwise>
					</c:choose>
				
			</display:column>	
			<display:column title="Upper Markup">
				<c:choose>
					<c:when test="${audit.oldUpperMarkup == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldUpperMarkup}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newUpperMarkup == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newUpperMarkup}
				</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="Lower Shipping Fees">
				<c:choose>
					<c:when test="${audit.oldLowerShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldLowerShippingFees}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newLowerShippingFees == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newLowerShippingFees}
				</c:otherwise>
				</c:choose>
			</display:column>	
			<display:column title="Upper Shipping Fees">
				<c:choose>
					<c:when test="${audit.oldUpperShippingFees == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldUpperShippingFees}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newUpperShippingFees == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newUpperShippingFees}
				</c:otherwise>
				</c:choose>
			</display:column>
			
		<display:column title="Section Count Ticket">
				<c:choose>
					<c:when test="${audit.oldSectionCountTicket == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.oldSectionCountTicket}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newSectionCountTicket == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newSectionCountTicket}
				</c:otherwise>
				</c:choose>
			</display:column>
			
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>