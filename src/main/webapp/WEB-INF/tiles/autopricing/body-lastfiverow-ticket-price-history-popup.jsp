
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
		
		
	<c:if test="${not empty tmatTicketList}" >
	<div id="ebayInventoryTabel" >
		
		<display:table list="${tmatTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tgCatTmatTicket"  requestURI="#" class="list">
			
			<display:column title="History Date" sortable="true">${tgCatTmatTicket.tPopTicDate}</display:column>
			<display:column title="PurchasePrice" sortable="true">${tgCatTmatTicket.tPopTicPur}</display:column>
			<display:column title="Online Price" sortable="true">${tgCatTmatTicket.tPopTicOnilinePrice}</display:column>
			<display:column title="Section" sortable="true">${tgCatTmatTicket.tPopTicSection}</display:column>
			<display:column title="NormalizedSection" sortable="true">${tgCatTmatTicket.tPopTicNormalizedSection}</display:column>
			<display:column title="Row" sortable="true">${tgCatTmatTicket.tPopTicRow}</display:column>
			<display:column title="Quantity" sortable="true">${tgCatTmatTicket.tPopTicquantity}</display:column>
			<display:column title="SiteId" sortable="true">${tgCatTmatTicket.tPopTicSiteId}</display:column>
			<display:column title="Created Date" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTmatTicket.tPopTicCreatedDate}" /></display:column>
			<display:column title="Last Updated" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTmatTicket.tPopTiclastUpdated}" /></display:column>
			<display:column title="TicketStatus" sortable="true">${tgCatTmatTicket.tPopTicTicketStatus}</display:column>

			
			
		
		</display:table>
		</div>
	</c:if>
	<br/>
	
	
	
	
	
	
	<c:if test="${empty tmatTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">No Information Found.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>	
		
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>