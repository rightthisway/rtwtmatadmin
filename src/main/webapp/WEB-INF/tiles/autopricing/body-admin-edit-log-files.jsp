<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Log Files
</div>


<div style="clear:both"></div>

<h1>Log Files</h1>
<script type="text/javascript">
function ebayExposureUpdate(){
	$("#action").val("updateExposure");
	$("#formId").submit();
}
</script>
<script type="text/javascript">

/*function fileDownload(exchangeField,fileLinkField){
	
	var exchange = $("#"+exchangeField).val();
	var fileLink = $("#"+fileLinkField).val();

	if(exchange!= null && exchange != '' && fileLink != null && fileLink != '') {
		document.formId.action=	fileLink;	
		//$("#action").val(fileLink);
		$("#formId").submit();
	} else {
		alert('Please select valid exchange and file.');
		return false;
	}
	
} */

function exchangeOnChange(product,exchange) {
	
	if(exchange =='' ) {
		$('#'+product+'_tn_file').hide();
		$('#'+product+'_vivid_file').hide();
		$('#'+product+'_scoreBig_file').hide();
		$('#'+product+'_stubhub_file').hide();
		$('#'+product+'_empty_file').show();
	} else if(exchange == 'tn') {
		$('#'+product+'_tn_file').show();
		$('#'+product+'_vivid_file').hide();
		$('#'+product+'_scoreBig_file').hide();
		$('#'+product+'_stubhub_file').hide();
		$('#'+product+'_empty_file').hide();
	
	} else if(exchange == 'vivid'){
		$('#'+product+'_vivid_file').show();
		$('#'+product+'_tn_file').hide();
		$('#'+product+'_scoreBig_file').hide();
		$('#'+product+'_stubhub_file').hide();
		$('#'+product+'_empty_file').hide();
		
	} else if(exchange == 'scoreBig'){
		$('#'+product+'_scoreBig_file').show();
		$('#'+product+'_tn_file').hide();
		$('#'+product+'_vivid_file').hide();
		$('#'+product+'_stubhub_file').hide();
		$('#'+product+'_empty_file').hide();
		
	}else if(exchange == 'tn'){
		$('#'+product+'_stubhub_file').show();
		$('#'+product+'_empty_file').hide();
		$('#'+product+'_tn_file').hide();
		$('#'+product+'_vivid_file').hide();
		$('#'+product+'_scoreBig_file').hide();
				
	}
}
function setFileLink(value,FieldName) {
	$("#"+FieldName).val(value);
}

function miniExchangeOnChange(exchange) {
	
	if(exchange =='' || exchange == 'tn') {
		$('#mini_tn_file').show();
		$('#mini_vivid_file').hide();
		$('#mini_scoreBig_file').hide();
	} else if(exchange == 'vivid'){
		$('#mini_vivid_file').show();
		$('#mini_tn_file').hide();
		$('#mini_scoreBig_file').hide();
	} else {
		$('#mini_scoreBig_file').show();
		$('#mini_tn_file').hide();
		$('#mini_vivid_file').hide();
		
	}
}

function downloadOnClick(action){
	$("#action").val(action);
	$("#formId").submit();
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function(){
$("#brokerId").change(function(){
	console.log($(this).val());
	if($(this).val() !=0){
		$("#ebayInventoryTabel").css("visibility","hidden");
	}
}); 
});

	 </script>
<form:form modelAttribute="exosure" action="AutopricingEditLogFiles" method="post" id="formId">
<div id="ebayInventoryTabel" >
	    <table>
			<tr align="center">
				<td colspan="3" class="successMessage" id="success" ><font face="Tahoma" size="4" color="ff0000">${sportinfo}</font> </td>
        	</tr>
		</table>	
		<br/>
</div>
<!-- <table BORDER=5 BORDERCOLOR=BLUE width="700"> -->
<table>
<tr>
<td>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

<td>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

<td>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>

			<td><b>Select Broker:</b></td>
			<td>
					<select id="brokerId" name="brokerId">
					 <option value="" <c:if test="${brokerId eq 0}"> Selected </c:if> >Select</option>
					<!-- <option value="">Select</option> -->
					<c:forEach items="${brokers}" var="broker">
			     		<option value="${broker.id}" <c:if test="${broker.id eq brokerId}"> Selected </c:if> >${broker.name}</option>
					</c:forEach>									
				</select>
			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
</table>		
<tr>
<!-- </table> -->
		
		<div align="left">
		
<br>
<table BORDER=5 BORDERCOLOR=BLUE width="700">
<tr>
 <tr>
  <th align="center">Product</th>
  <th  align="center">Exchange</th>
  <th  align="center">File</th>
  <th align="center" >Download</th>
</tr>

<tr>
 <!-- <td  align="center">Auto Cats</td>
  <td align="center"><select name="auto_exchange" id="auto_exchange" onchange="exchangeOnChange('auto',this.value);">
  <option  value="">Select</option>
  <option value="tn">Ticket Network</option>
  <option value="vivid">Vivid Seats</option>
  <option value="tickPick">Tick Pick</option>
  <option value="scoreBig">Score Big</option>
</select></td>
<td align="center">
<input type="hidden" id="auto_link" name="auto_link" />
<span id="auto_empty_file">
	<select name="auto_tn_link" id="auto_tn_link" onchange="setFileLink(this.value,'auto_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="auto_tn_file">
	<select name="auto_tn_link" id="auto_tn_link" onchange="setFileLink(this.value,'auto_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTndCategoryTickets">Category Tickets</option>
	  <option value="DownloadTndCategoryEvents">Category Events</option>
	  <option value="DownloadTndErrors">Errors</option>
	</select>
</span>
<span id="auto_vivid_file">
	<select name="auto_vivid_link" id="auto_vivid_link" onchange="setFileLink(this.value,'auto_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTndConcertsCategoryTickets">Concerts - Category Tickets</option>
	  <option value="DownloadTndConcertsCategoryEvents">Concerts - Category Events</option>
	  <option value="DownloadTndSportsCategoryTickets">Sports - Category Tickets</option>
	  <option value="DownloadTndSportsCategoryEvents">Sports - Category Events</option>
	  <option value="DownloadTndTheaterCategoryTickets">Theater - Category Tickets</option>
	  <option value="DownloadTndTheaterCategoryEvents">Theater - Category Events</option>
	</select>
</span>
<span id="auto_scoreBig_file">
	<select name="auto_scoreBig_link" id="auto_scoreBig_link" onchange="setFileLink(this.value,'auto_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTndCategoryTickets">Category Tickets</option>
	  <option value="DownloadTndCategoryEvents">Category Events</option>
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('auto_download');"/></td>
</tr> --> 

<td  align="center">Mini Cats</td>
  <td align="center"><select name="mini_exchange" id="mini_exchange" onchange="exchangeOnChange('mini',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
  <option value="vivid">Vivid Seats</option>
   <!-- <option value="tickPick">Tick Pick</option> -->
  <option value="scoreBig">Score Big</option>
</select></td>
<td align="center">
<input type="hidden" id="mini_link" name="mini_link" />
<span id="mini_empty_file">
	<select name="mini_tn_link" id="mini_tn_link" onchange="setFileLink(this.value,'mini_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="mini_tn_file">
	<select name="mini_tn_link" id="mini_tn_link" onchange="setFileLink(this.value,'mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTGCategoryTickets">Category Tickets</option>
	 <!--  <option value="DownloadTGCategoryEvents">Category Events</option>
	  <option value="DownloadTGErrors">Errors</option> -->
	</select>
</span>
<span id="mini_vivid_file">
	<select name="mini_vivid_link" id="mini_vivid_link" onchange="setFileLink(this.value,'mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTGCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadTGConcertsCategoryTickets">Category Tickets</option> -->
	 <!--  <option value="DownloadTGConcertsCategoryEvents">Concerts - Category Events</option>
	  <option value="DownloadTGSportsCategoryTickets">Sports - Category Tickets</option>
	  <option value="DownloadTGSportsCategoryEvents">Sports - Category Events</option>
	  <option value="DownloadTGTheaterCategoryTickets">Theater - Category Tickets</option>
	  <option value="DownloadTGTheaterCategoryEvents">Theater - Category Events</option> -->
	</select>
</span>
<span id="mini_scoreBig_file">
	<select name="mini_scoreBig_link" id="mini_scoreBig_link" onchange="setFileLink(this.value,'mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadTGCategoryTickets">Category Tickets</option>
	<!--   <option value="DownloadTGCategoryEvents">Category Events</option> -->
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('mini_download');"/></td>
</tr>

<tr>
<td  align="center">VIP Mini Cats</td>
  <td align="center"><select name="vip_mini_exchange" id="vip_mini_exchange" onchange="exchangeOnChange('vip_mini',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
  <!-- <option value="vivid">Vivid Seats</option> -->
 <!--  <option value="tickPick">Tick Pick</option> -->
  <!-- <option value="scoreBig">Score Big</option> -->
</select></td>
<td align="center">
<input type="hidden" id="vip_mini_link" name="vip_mini_link" />
<span id="vip_mini_empty_file">
	<select name="vip_mini_tn_link" id="vip_mini_tn_link" onchange="setFileLink(this.value,'vip_mini_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="vip_mini_tn_file">
	<select name="vip_mini_tn_link" id="vip_mini_tn_link" onchange="setFileLink(this.value,'vip_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadVipCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadVipCategoryEvents">Category Events</option>
	  <option value="DownloadVipErrors">Errors</option> -->
	</select>
</span>
<span id="vip_mini_vivid_file">
	<select name="vip_mini_vivid_link" id="vip_mini_vivid_link" onchange="setFileLink(this.value,'vip_mini_link');">
	  <option  value="">Select</option>
	   <option value="DownloadVipCategoryTickets">Category Tickets</option>
	  <option value="DownloadVipCategoryEvents">Category Events</option>
	</select>
</span>
<span id="vip_mini_scoreBig_file">
	<select name="vip_mini_scoreBig_link" id="vip_mini_scoreBig_link" onchange="setFileLink(this.value,'vip_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadVipCategoryTickets">Category Tickets</option>
	  <option value="DownloadVipCategoryEvents">Category Events</option>
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('vip_mini_download');"/></td>
</tr>

 <!-- <tr>
<td  align="center">VIP Auto Cats</td>
  <td align="center"><select name="vip_auto_exchange" id="vip_auto_exchange" onchange="exchangeOnChange('vip_auto',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
  <option value="vivid">Vivid Seats</option>
  <option value="tickPick">Tick Pick</option>
  <option value="scoreBig">Score Big</option>
</select></td>
<td align="center">
<input type="hidden" id="vip_auto_link" name="vip_auto_link" />
<span id="vip_auto_empty_file">
	<select name="vip_auto_tn_link" id="vip_auto_tn_link" onchange="setFileLink(this.value,'vip_auto_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="vip_auto_tn_file">
	<select name="vip_auto_tn_link" id="vip_auto_tn_link" onchange="setFileLink(this.value,'vip_auto_link');">
	  <option  value="">Select</option>
	  <option value="DownloadVipAutoCategoryTickets">Category Tickets</option>
	  <option value="DownloadVipAutoCategoryEvents">Category Events</option>
	  <option value="DownloadVipAutoErrors">Errors</option>
	</select>
</span>
<span id="vip_auto_vivid_file">
	<select name="vip_auto_vivid_link" id="vip_auto_vivid_link" onchange="setFileLink(this.value,'vip_auto_link');">
	  <option  value="">Select</option>
	   <option value="DownloadVipAutoCategoryTickets">Category Tickets</option>
	  <option value="DownloadVipAutoCategoryEvents">Category Events</option>
	</select>
</span>
<span id="vip_auto_scoreBig_file">
	<select name="vip_auto_scoreBig_link" id="vip_auto_scoreBig_link" onchange="setFileLink(this.value,'vip_auto_link');">
	  <option  value="">Select</option>
	  <option value="DownloadVipAutoCategoryTickets">Category Tickets</option>
	  <option value="DownloadVipAutoCategoryEvents">Category Events</option>
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('vip_auto_download');"/></td>
</tr> -->
<tr>
<td  align="center">LastRow Mini Cats</td>
  <td align="center"><select name="lastrow_mini_exchange" id="lastrow_mini_exchange" onchange="exchangeOnChange('lastrow_mini',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
  <option value="vivid">Vivid Seats</option>
  <option value="scoreBig">Score Big</option>
</select></td>
<td align="center">
<input type="hidden" id="lastrow_mini_link" name="lastrow_mini_link" />
<span id="lastrow_mini_empty_file">
	<select name="lastrow_mini_tn_link" id="lastrow_mini_tn_link" onchange="setFileLink(this.value,'lastrow_mini_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="lastrow_mini_tn_file">
	<select name="lastrow_mini_tn_link" id="lastrow_mini_tn_link" onchange="setFileLink(this.value,'lastrow_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadLastRowMiniCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadLastRowMiniCategoryEvents">Category Events</option>
	  <option value="DownloadLastRowMiniErrors">Errors</option> -->
	</select>
</span>
<span id="lastrow_mini_vivid_file">
	<select name="lastrow_mini_vivid_link" id="lastrow_mini_vivid_link" onchange="setFileLink(this.value,'lastrow_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadLastRowMiniCategoryTickets">Category Tickets</option>
	</select>
</span>
<span id="lastrow_mini_scoreBig_file">
	<select name="lastrow_mini_scoreBig_link" id="lastrow_mini_scoreBig_link" onchange="setFileLink(this.value,'lastrow_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadLastRowMiniCategoryTickets">Category Tickets</option>
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('lastrow_mini_download');"/></td>
</tr>
<tr>
<td  align="center">Zones Pricing</td>
  <td align="center"><select name="zones_pric_exchange" id="zones_pric_exchange" onchange="exchangeOnChange('zones_pric',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
</select></td>
<td align="center">
<input type="hidden" id="zones_pric_link" name="zones_pric_link" />
<span id="zones_pric_empty_file">
	<select name="zones_pric_tn_link" id="zones_pric_tn_link" onchange="setFileLink(this.value,'zones_pric_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="zones_pric_tn_file">
	<select name="zones_pric_tn_link" id="zones_pric_tn_link" onchange="setFileLink(this.value,'zones_pric_link');">
	  <option  value="">Select</option>
	  <option value="DownloadZonesPricingCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadLastRowMiniCategoryEvents">Category Events</option>
	  <option value="DownloadLastRowMiniErrors">Errors</option>  -->
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('zones_pric_download');"/></td>
</tr>
<tr>
<td  align="center">Zoned LastRow Minicats</td>
  <td align="center"><select name="zoned_last_exchange" id="zoned_last_exchange" onchange="exchangeOnChange('zoned_last',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
</select></td>
<td align="center">
<input type="hidden" id="zoned_last_link" name="zoned_last_link" />
<span id="zoned_last_empty_file">
	<select name="zoned_last_tn_link" id="zoned_last_tn_link" onchange="setFileLink(this.value,'zoned_last_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="zoned_last_tn_file">
	<select name="zoned_last_tn_link" id="zoned_last_tn_link" onchange="setFileLink(this.value,'zoned_last_link');">
	  <option  value="">Select</option>
	  <option value="DownloadZonedLastRowCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadLastRowMiniCategoryEvents">Category Events</option>
	  <option value="DownloadLastRowMiniErrors">Errors</option>  -->
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('zoned_lastrow_download');"/></td>
</tr>
<tr>
<td  align="center">LarryLast</td>
  <td align="center"><select name="Larry_last_exchange" id="Larry_last_exchange" onchange="exchangeOnChange('Larry_last',this.value);">
  <option value="">Select</option>
  <option value="tn">Ticket Network</option>
</select></td>
<td align="center">
<input type="hidden" id="Larry_last_link" name="Larry_last_link" />
<span id="Larry_last_empty_file">
	<select name="Larry_last_tn_link" id="Larry_last_tn_link" onchange="setFileLink(this.value,'Larry_last_link');">
	  <option  value="">Select</option>
	</select>
</span>
<span id="Larry_last_tn_file">
	<select name="Larry_last_tn_link" id="Larry_last_tn_link" onchange="setFileLink(this.value,'Larry_last_link');">
	  <option  value="">Select</option>
	  <option value="DownloadLarryLastCategoryTickets">Category Tickets</option>
	  <!-- <option value="DownloadLastRowMiniCategoryEvents">Category Events</option>
	  <option value="DownloadLastRowMiniErrors">Errors</option>  -->
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('larry_last_download');"/></td>
</tr>  
<tr>  
<tr>
<!-- <td  align="center">LastFiveRow Mini Cats</td>
  <td align="center"><select name="lastfiverow_mini_exchange" id="lastfiverow_mini_exchange" onchange="exchangeOnChange('lastfiverow_mini',this.value);">
  <option value="">Select</option>
  <option value="stubhub">Stubhub</option>
</select></td>
<td align="center">
<input type="hidden" id="lastfiverow_mini_link" name="lastfiverow_mini_link" />
<span id="lastfiverow_mini_empty_file">
	<select name="lastfiverow_mini_stubhub_link" id="lastfiverow_mini_stubhub_link" onchange="setFileLink(this.value,'lastfiverow_mini_link');">
	  <option  value="">Select</option>
	  
	</select>
</span>
<span id="lastfiverow_mini_stubhub_file">
	<select name="lastfiverow_mini_stubhub_link" id="lastfiverow_mini_stubhub_link" onchange="setFileLink(this.value,'lastfiverow_mini_link');">
	  <option  value="">Select</option>
	  <option value="DownloadLastFiveRowMiniCatTickets">Category Tickets</option>
	  <option value="DownloadLastFiveRowMiniCatEvents">Category Events</option>
	</select>
</span>
</td>
  <td align="center"><input type="button" value="Download"  class="medButton" id="update2" name="update2"onclick="javascript:downloadOnClick('lastfiverow_mini_download');"/></td>
</tr>
 -->
</table>
			<br/>
			<div align ="left" style="float:left; padding-left:320px;">
				 
			</div>
			</div>
	<input type="hidden" name="action" id="action"/>
	<input type="hidden" name="parentCat" id="parentCat"/>
	<br/>
	<br/>
</form:form>

<script type="text/javascript">
exchangeOnChange('auto','');
exchangeOnChange('mini','');
exchangeOnChange('vip_mini','');
exchangeOnChange('vip_auto','');
exchangeOnChange('lastrow_mini','');
exchangeOnChange('lastfiverow_mini','');
exchangeOnChange('zones_pric','');
exchangeOnChange('zoned_last','');
exchangeOnChange('Larry_last','');


</script>