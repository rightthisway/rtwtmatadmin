
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
	 
</div>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}" style="width:530px;"  decorator="org.displaytag.decorator.TotalTableDecorator"  id="audit"  requestURI="#" class="list">

			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.createdDate}" />					
			</display:column>
			
			
			<display:column title="User">${audit.userName}</display:column>

				<%-- <display:column title="Broker_Id">${audit.brokerId}</display:column>  --%>

				<display:column title="Broker_Status">${audit.brokerStatus}</display:column> 
				
				<display:column title="Broker_Name">
					<c:forEach items="${brokers}" var="broker">
				     		<c:if test="${broker.id eq audit.brokerId}"> ${broker.name} </c:if>
					</c:forEach>
			</display:column>
				
				<%-- <display:column title="Broker_Name">${event.broker_status}</display:column>  --%>
				<%-- <display:column title="User">${audit.broker_id}</display:column>  --%>
				
			
			
		
			
			
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit for this Event.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>