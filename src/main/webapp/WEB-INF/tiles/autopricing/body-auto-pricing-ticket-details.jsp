<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<script type="text/javascript" src="../js/highChart/highcharts.js"></script>
<script type="text/javascript" src="../js/highChart/exporting.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
 


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Auto Pricing Ticket Details
</div>


<h1>Auto Pricing Ticket Details</h1>
<br/>
<form:form action="AutopricingTicketDetails" method="post" id="formId">

	<input type="hidden" id="artistId" name="artistId" value="${artistId }"/>
	<input type="hidden" id="venueId" name="venueId" value="${venueId }"/>
	
	<c:if test="${not empty info}">
		<div    style="color:blue;"  >${info}</div>
	</c:if> 
	
	<br/>
	<br/>
	
<table width="80%">
		
		<tr>
			<td><b>Filter: By Artist </b></td>
			<td>
				<span id="artistLabelSpanId"  <c:if test="${not empty artistId }">style="display: black;"</c:if> 
				<c:if test="${empty artistId }">style="display: none;"</c:if>  >
				<span id="artistNameDiv" >
					<c:choose>
						<c:when test="${not empty artistName }">
							${artistName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
				</span>	
					<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
				</span>
				
				
				<span id="artistSelectSpanId" <c:if test="${not empty artistId }">style="display: none;"</c:if> 
					<c:if test="${empty artistId }">style="display: black;"</c:if> >
				
					<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		
		<tr>
			<td><b>Or By Venue </b></td>
			<td>
				<span id="venueLabelSpanId"  <c:if test="${not empty venueId }">style="display: black;"</c:if> 
				
				<c:if test="${empty venueId }">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty venueId }">style="display: none;"</c:if> 
				<c:if test="${empty venueId }">style="display: black;"</c:if> >
				
					<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
					<c:forEach var="venue" items="${venues}">
						<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
					</c:forEach>
				</form:select> --%>
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		<tr>
			<td><b>Events: </b></td>
			<td>
				<select name="eventId" id="eventId"  >
					<option value="">Select</option>
					<c:forEach var="event" items="${events}">
						<option value="${event.id}" style="height:20px;" 
						<c:if test="${event.id == selectedEventId}">selected</c:if>>${event.name} 
						<fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
								<c:choose>
									<c:when test="${event.localTime == null}">
										 TBD
									</c:when>
									<c:otherwise>
										<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
									</c:otherwise>
								</c:choose></option>
					</c:forEach>
				</select>

			</td>
			<td></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		
		
		<tr>
		
			<td><b>Section :</b></td>
			<td>
				<input type="text" id="section" name="section"  value="${param.section}" />
			</td>
		</tr>
		
		
		<tr>
		
			<td><b>Row :</b></td>
			<td>
				<input type="text" id="row" name="row"  value="${param.row}"  />
			</td>
		</tr>
		
		<tr>
		
			<td><b>Quantity : </b></td>
			<td>
				<input type="text" id="quantity" name="quantity" value="${param.quantity}"  />
			</td>
		</tr>
		<tr>
			<td colspan="6" align="center">
				<input type="button"  id="submitbutton"value="Get Ticket Details" onclick="submitForms();" class="medButton">
			</td>
		</tr>
	</table>
	
	<c:if test="${showgrid ==true}">
	<div id="container-tabs" style="margin-top: 5px">
	
	<ul>
			<li><a href="#fragment-minicats" id="tab-minicattickets"><span>MiniCat</span></a></li>
			<li><a href="#fragment-autocats" id="tab-autocattickets"><span>AutoCat</span></a></li>
			<li><a href="#fragment-vipminicats" id="tab-vipminicattickets"><span>VipMiniCat</span></a></li> 
			<li><a href="#fragment-lastrowminicats" id="tab-lastrowminicats"><span>LastRowMiniCat</span></a></li>  
			<li><a href="#fragment-larrylastcats" id="tab-larrylastcats"><span>LarryLast</span></a></li>
			<li><a href="#fragment-zonedLastrowminicats" id="tab-zonedlastrowcats"><span>Zoned LastRow Minicat</span></a></li>
			<li><a href="#fragment-zonesPricingcats" id="tab-zonespricingcats"><span>ZonesPricing</span></a></li>
			<!-- <li><a href="#fragment-vipautocats" id="tab-vipautocats"><span>VipAutoCat</span></a></li>  -->  
			<!-- <li><a href="#fragment-lastfiverowminicats" id="tab-lastfiverowminicats"><span>LastFiveRowsMiniCat</span></a></li> -->
   	</ul>
   		
   		<div id="fragment-minicats">
   		<c:if test="${not empty categoryTickets}" >
		<div id="ebayInventoryTabel" >
		
		
		<display:table list="${categoryTickets}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="tgCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${tgCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${tgCatTicket.rowRange}</display:column>
			<display:column title="Last_Row" sortable="true">${tgCatTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${tgCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${tgCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${tgCatTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${tgCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${tgCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${tgCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${tgCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${tgCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${tgCatTicket.id}"
				onClick="popupTGCatsTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${tgCatTicket.id}"
				onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.ticketId}', '${tgCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketOneHistory ne null and tgCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${tgCatTicket.baseTicket1}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicket1}', '${tgCatTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketTwoHistory ne null and tgCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${tgCatTicket.baseTicket2}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicket2}','${tgCatTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${tgCatTicket.baseTicketThreeHistory ne null and tgCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${tgCatTicket.baseTicket3}"
						onClick="popupTgcatTMATTicketInfo('${tgCatTicket.eventId}','${tgCatTicket.baseTicket3}','${tgCatTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty categoryTickets}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	
	
	
	
	<div id="fragment-lastrowminicats">
   		<c:if test="${not empty lastRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${lastRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="lastRowMiniTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${lastRowMiniTicket.section}</display:column>
			<%-- <display:column title="Row Range" sortable="true">${lastRowMiniTicket.rowRange}</display:column> --%>
			<display:column title="Last_Row" sortable="true">${lastRowMiniTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${lastRowMiniTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${lastRowMiniTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${lastRowMiniTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${lastRowMiniTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${lastRowMiniTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${lastRowMiniTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${lastRowMiniTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${lastRowMiniTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${lastRowMiniTicket.id}"
				onClick="popupLastRowCatsTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${lastRowMiniTicket.id}"
				onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.ticketId}','${lastRowMiniTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketOneHistory ne null and lastRowMiniTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${lastRowMiniTicket.baseTicket1}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicket1}','${lastRowMiniTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketTwoHistory ne null and lastRowMiniTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${lastRowMiniTicket.baseTicket2}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicket2}','${lastRowMiniTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${lastRowMiniTicket.baseTicketThreeHistory ne null and lastRowMiniTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${lastRowMiniTicket.baseTicket3}"
						onClick="popupLastRowTMATTicketInfo('${lastRowMiniTicket.eventId}','${lastRowMiniTicket.baseTicket3}','${lastRowMiniTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty lastRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	
	<!-- Larry Lat Display -->
	
	
	
	<div id="fragment-larrylastcats">
   		<c:if test="${not empty larryLastCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${larryLastCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="larryLastTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${larryLastTicket.section}</display:column>
			
			<display:column title="Last_Row" sortable="true">${larryLastTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${larryLastTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${larryLastTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${larryLastTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${larryLastTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${larryLastTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${larryLastTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${larryLastTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${larryLastTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${larryLastTicket.id}"
				onClick="popupLarryLastCatsTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${larryLastTicket.id}"
				onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.ticketId}','${larryLastTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketOneHistory ne null and larryLastTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${larryLastTicket.baseTicket1}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicket1}','${larryLastTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketTwoHistory ne null and larryLastTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${larryLastTicket.baseTicket2}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicket2}','${larryLastTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${larryLastTicket.baseTicketThreeHistory ne null and larryLastTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${larryLastTicket.baseTicket3}"
						onClick="popupLarryLastTMATTicketInfo('${larryLastTicket.eventId}','${larryLastTicket.baseTicket3}','${larryLastTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty larryLastCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<!-- VipAutoCat -->
	
	<%-- <div id="fragment-vipautocats">
   		<c:if test="${not empty vipAutoCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${vipAutoCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipautoTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipautoTicket.section}</display:column>
			
			<display:column title="Last_Row" sortable="true">${vipautoTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${vipautoTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipautoTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipautoTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${vipautoTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipautoTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipautoTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipautoTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${vipautoTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipautoTicket.id}"
				onClick="popupVipAutoCatsTicketInfo('${vipautoTicket.eventId}','${vipautoTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${vipautoTicket.id}"
				onClick="popupVipAutoTMATTicketInfo('${vipautoTicket.eventId}','${vipautoTicket.ticketId}','${vipautoTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketOneHistory ne null and vipautoTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipautoTicket.baseTicket1}"
						onClick="popupVipAutoTMATBaseTicket1Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicket1}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketTwoHistory ne null and vipautoTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${vipautoTicket.baseTicket2}"
						onClick="popupVipAutoTMATBaseTicket2Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicket2}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${vipautoTicket.baseTicketThreeHistory ne null and vipautoTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${vipautoTicket.baseTicket3}"
						onClick="popupVipAutoTMATBaseTicket3Info('${vipautoTicket.eventId}','${vipautoTicket.baseTicket3}','${vipautoTicket.id}');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty vipAutoCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	
	
	
	
	<div id="fragment-autocats">
   		<c:if test="${not empty autoCatsCategoryTicketsList}" >
		<div id="ebayInventoryTabel" >
	
	
	<display:table list="${autoCatsCategoryTicketsList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="autoCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${autoCatTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${autoCatTicket.rowRange}</display:column>
			<display:column title="Last Row" sortable="true">${autoCatTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${autoCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${autoCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${autoCatTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${autoCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${autoCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${autoCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${autoCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${autoCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${autoCatTicket.id}"
				onClick="popupAutoCatsTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${autoCatTicket.id}"
				onClick="popupAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.ticketId}','${autoCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketOneHistory ne null and autoCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${autoCatTicket.baseTicket1}"
						onClick="popupAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicket1}','${autoCatTicket.id}','baseTicketOne');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketTwoHistory ne null and autoCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${autoCatTicket.baseTicket2}"
						onClick="popupAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicket2}','${autoCatTicket.id}','baseTicketTwo');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${autoCatTicket.baseTicketThreeHistory ne null and autoCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${autoCatTicket.baseTicket3}"
						onClick="popupAutoTMATTicketInfo('${autoCatTicket.eventId}','${autoCatTicket.baseTicket3}','${autoCatTicket.id}','baseTicketThree');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
			</div>
	</c:if>
	<c:if test="${empty autoCatsCategoryTicketsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	
	
	
	
	
	
	
	
	
	<%-- <div id="fragment-lastfiverowminicats">
   		<c:if test="${not empty lastFiveRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	<display:table list="${lastFiveRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="lastfiverowTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${lastfiverowTicket.section}</display:column>
			<display:column title="Row Range" sortable="true">${lastfiverowTicket.rowRange}</display:column>
			<display:column title="Last Row" sortable="true">${lastfiverowTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${lastfiverowTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${lastfiverowTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${lastfiverowTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${lastfiverowTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${lastfiverowTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${lastfiverowTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${lastfiverowTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${lastfiverowTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${lastfiverowTicket.id}"
				onClick="popupLastFiveRowCatsTicketInfo('${lastfiverowTicket.eventId}','${lastfiverowTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${lastfiverowTicket.id}"
				onClick="popupLastFiveRowTMATTicketInfo('${lastfiverowTicket.eventId}','${lastfiverowTicket.ticketId}','${lastfiverowTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketOneHistory ne null and lastfiverowTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${lastfiverowTicket.baseTicket1}"
						onClick="popupLastFiveRowTMATBaseTicket1Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicket1}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketTwoHistory ne null and lastfiverowTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${lastfiverowTicket.baseTicket2}"
						onClick="popupLastFiveRowTMATBaseTicket2Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicket2}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${lastfiverowTicket.baseTicketThreeHistory ne null and lastfiverowTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${lastfiverowTicket.baseTicket3}"
						onClick="popupLastFiveRowTMATBaseTicket3Info('${lastfiverowTicket.eventId}','${lastfiverowTicket.baseTicket3}','${lastfiverowTicket.id}');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
			</div>
	</c:if>
	<c:if test="${empty lastFiveRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div> --%>
	
	
	<div id="fragment-vipminicats">
	<c:if test="${not empty vipCatsCategoryTicketsList}" >
	
		
		<display:table list="${vipCatsCategoryTicketsList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="vipCatTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${vipCatTicket.section}</display:column>
			<display:column title="Alternate Row" sortable="true">${vipCatTicket.alternateRow}</display:column>
			<display:column title="Quantity" sortable="true">${vipCatTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${vipCatTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${vipCatTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${vipCatTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${vipCatTicket.vividPrice}</display:column>
			<display:column title="TickPick Price" sortable="true">${vipCatTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${vipCatTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${vipCatTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${vipCatTicket.id}"
				onClick="popupVipCatsTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"    class="medButton" id="tmatTicketInfo_${vipCatTicket.id}"
				onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.ticketId}','${vipCatTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketOneHistory ne null and vipCatTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${vipCatTicket.baseTicket1}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicket1}','${vipCatTicket.id}','baseTicketOne');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketTwoHistory ne null and vipCatTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${vipCatTicket.baseTicket2}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicket2}','${vipCatTicket.id}','baseTicketTwo');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${vipCatTicket.baseTicketThreeHistory ne null and vipCatTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${vipCatTicket.baseTicket3}"
						onClick="popupVipTMATTicketInfo('${vipCatTicket.eventId}','${vipCatTicket.baseTicket3}','${vipCatTicket.id}','baseTicketThree');"/>
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				</c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</c:if>
	
	<c:if test="${empty vipCatsCategoryTicketsList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<div id="fragment-zonedLastrowminicats">
   		<c:if test="${not empty zonedLastRowMiniCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${zonedLastRowMiniCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zonedLastRowMiniTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zonedLastRowMiniTicket.section}</display:column>
			<%-- <display:column title="Row Range" sortable="true">${zonedLastRowMiniTicket.rowRange}</display:column> --%>
			<display:column title="Last_Row" sortable="true">${zonedLastRowMiniTicket.lastRow}</display:column>
			<display:column title="Quantity" sortable="true">${zonedLastRowMiniTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zonedLastRowMiniTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zonedLastRowMiniTicket.actualPrice}</display:column>
			<display:column title="TN Price" sortable="true">${zonedLastRowMiniTicket.tnPrice}</display:column>
			<display:column title="Vivid Price" sortable="true">${zonedLastRowMiniTicket.vividPrice}</display:column>
			<%-- <display:column title="TickPick Price" sortable="true">${zonedLastRowMiniTicket.tickPickPrice}</display:column> --%>
			<display:column title="ScoreBig Price" sortable="true">${zonedLastRowMiniTicket.scorebigPrice}</display:column>
			<display:column title="Status" sortable="true">${zonedLastRowMiniTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zonedLastRowMiniTicket.id}"
				onClick="popupZonedLastRowCatsTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zonedLastRowMiniTicket.id}"
				onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.ticketId}','${zonedLastRowMiniTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketOneHistory ne null and zonedLastRowMiniTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zonedLastRowMiniTicket.baseTicket1}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicket1}','${zonedLastRowMiniTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketTwoHistory ne null and zonedLastRowMiniTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zonedLastRowMiniTicket.baseTicket2}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicket2}','${zonedLastRowMiniTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zonedLastRowMiniTicket.baseTicketThreeHistory ne null and zonedLastRowMiniTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zonedLastRowMiniTicket.baseTicket3}"
						onClick="popupZonedLastRowTMATTicketInfo('${zonedLastRowMiniTicket.eventId}','${zonedLastRowMiniTicket.baseTicket3}','${zonedLastRowMiniTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty zonedLastRowMiniCategoryTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<div id="fragment-zonesPricingcats">
   		<c:if test="${not empty zonesPricingCategoryTicketList}" >
		<div id="ebayInventoryTabel" >
	
	
	
	
	<display:table list="${zonesPricingCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="zonesPricingTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${zonesPricingTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${zonesPricingTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${zonesPricingTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${zonesPricingTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${zonesPricingTicket.tnPrice}</display:column>
			<%--<display:column title="Vivid Price" sortable="true">${zonesPricingTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${zonesPricingTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zonesPricingTicket.scorebigPrice}</display:column> --%>
			<display:column title="Status" sortable="true">${zonesPricingTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${zonesPricingTicket.id}"
				onClick="popupZonesPricingCatsTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${zonesPricingTicket.id}"
				onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.ticketId}','${zonesPricingTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketOneHistory ne null and zonesPricingTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${zonesPricingTicket.baseTicket1}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicket1}','${zonesPricingTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketTwoHistory ne null and zonesPricingTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${zonesPricingTicket.baseTicket2}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicket2}','${zonesPricingTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${zonesPricingTicket.baseTicketThreeHistory ne null and zonesPricingTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${zonesPricingTicket.baseTicket3}"
						onClick="popupZonesPricingTMATTicketInfo('${zonesPricingTicket.eventId}','${zonesPricingTicket.baseTicket3}','${zonesPricingTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	
		<c:if test="${not empty jktZonesPricingCategoryTicketList}" >
		<div id="ebayInventoryTabel" >

	<display:table list="${jktZonesPricingCategoryTicketList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="jktZonesPricingTicket"  requestURI="#" class="list">
			
			<display:column title="Section" sortable="true">${jktZonesPricingTicket.section}</display:column>
			<display:column title="Quantity" sortable="true">${jktZonesPricingTicket.quantity}</display:column>
			<display:column title="Last Update" sortable="true"><fmt:formatDate type="both" dateStyle="short" timeStyle="short" value="${jktZonesPricingTicket.lastUpdated}" /></display:column>
			<display:column title="Actual Price" sortable="true">${jktZonesPricingTicket.price}</display:column>
			<display:column title="TN Price" sortable="true">${jktZonesPricingTicket.tnPrice}</display:column>
			<display:column title="FloorCap" sortable="true">${jktZonesPricingTicket.floorCap}</display:column>
			<%--<display:column title="Vivid Price" sortable="true">${zonesPricingTicket.vividPrice}</display:column>
			 <display:column title="TickPick Price" sortable="true">${zonesPricingTicket.tickPickPrice}</display:column>
			<display:column title="ScoreBig Price" sortable="true">${zonesPricingTicket.scorebigPrice}</display:column> --%>
			<display:column title="Status" sortable="true">${jktZonesPricingTicket.status}</display:column>
			
			<display:column title="Category Ticket" >
			 	<input type="button" value="Get Info"    class="medButton" id="catTicketInfo_${jktZonesPricingTicket.id}"
				onClick="popupJktZonesPricingCatsTicketInfo('${jktZonesPricingTicket.eventId}','${jktZonesPricingTicket.id}');"/>
			</display:column>
			
			<display:column title="TMAT Ticket1" >
			 	<input type="button" value="Get Info"   class="medButton" id="tmatTicketInfo_${jktZonesPricingTicket.id}"
				onClick="popupJktZonesPricingTMATTicketInfo('${jktZonesPricingTicket.eventId}','${jktZonesPricingTicket.ticketId}','${jktZonesPricingTicket.id}','ticketId');"/>
			</display:column>
			
			<display:column title="TMAT Ticket2" >
				<c:choose>
					<c:when test="${jktZonesPricingTicket.baseTicketOneHistory ne null and jktZonesPricingTicket.baseTicketOneHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket1Info_${jktZonesPricingTicket.baseTicket1}"
						onClick="popupJktZonesPricingTMATTicketInfo('${jktZonesPricingTicket.eventId}','${jktZonesPricingTicket.baseTicket1}','${jktZonesPricingTicket.id}','baseTicketOne');"/>
					</c:when>
			
			    <c:otherwise>
			         -
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket3" >
				<c:choose>
					<c:when test="${jktZonesPricingTicket.baseTicketTwoHistory ne null and jktZonesPricingTicket.baseTicketTwoHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket2Info_${jktZonesPricingTicket.baseTicket2}"
						onClick="popupZonesPricingTMATTicketInfo('${jktZonesPricingTicket.eventId}','${jktZonesPricingTicket.baseTicket2}','${jktZonesPricingTicket.id}','baseTicketTwo');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
			
			<display:column title="TMAT Ticket4" >
				<c:choose>
					<c:when test="${jktZonesPricingTicket.baseTicketThreeHistory ne null and jktZonesPricingTicket.baseTicketThreeHistory ne '' }">
						<input type="button" value="Get Info"    class="medButton" id="tmatBaseTicket3Info_${jktZonesPricingTicket.baseTicket3}"
						onClick="popupZonesPricingTMATTicketInfo('${jktZonesPricingTicket.eventId}','${jktZonesPricingTicket.baseTicket3}','${jktZonesPricingTicket.id}','baseTicketThree');"/>
					</c:when>
			
			    <c:otherwise>
							-
			   </c:otherwise>
		       </c:choose>
			</display:column>
			
		</display:table>
			
			<br/>
	</div>
	</c:if>
	<c:if test="${empty zonesPricingCategoryTicketList and empty jktZonesPricingTicketList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any pricing details for this selection</span></h4> 
	</div>
	</c:if>
	</div>
	
	<br/>
	<br/>
	</div>
</c:if>
	<input type="hidden" name="action" id="action"/>
</form:form>



<script type="text/javascript">



$(document).ready(function () {
	
	
		
	$( "#row" ).change(function() {
	  var section=$("#row").val();
	  $("#row").show();
	});
	
	
	$( "#section" ).change(function() {
		  var section=$("#section").val();
		  $("#section").show();
		});
	
	$( "#quantity" ).change(function() {
		  var section=$("#quantity").val();
		  $("#quantity").show();
		});
	
	});

$('#container-tabs').tabs({
    select: function(event, ui) {
      var url = ui.tab.href;
      var posAnchor = url.indexOf('#');
  	//anchor = url.substr(posAnchor, url.length - posAnchor);
  	anchor = "";   
  	
      if (url.indexOf('#fragment-chart') > 0) {
        $('#chartDiv').css('opacity', 1);
        $('#chartDiv').css('height', 400);
	  } else {
	    $('#chartDiv').css('opacity', 0);
        $('#chartDiv').css('height', 1);
	  }
    }
    });
    
if (location.href.indexOf('#fragment-chart') > 0) {
    $('#chartDiv').css('height', 400);
    $('#chartDiv').css('opacity', 1);
}  else {
    $('#chartDiv').css('opacity', 0);
    $('#chartDiv').css('height', 1);
}

function changeFilter() {
	
	$("#formId").submit();
}

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val('');
		$('#artistNameDiv').text(row[1]);
			
		$('#artistLabelSpanId').show();
		$('#artistSelectSpanId').hide();
		
		$("#artistId").val(row[0]);
		callChangeVenue();
		getEvents('artistId',row[0]);
	});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#tempArtistId').val('');
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#tempVenueId').val('');
}

$('#autoVenue').autocomplete("AutoCompleteVenue", {
	width: 650,
	max: 1000,
	minChars: 2,		
	formatItem: function(row, i, max) {
		return row[1];
	}
	}).result(function (event,row,formatted){
		$('#autoVenue').val('');
		$('#venueNameDiv').text(row[1]);
			
		$('#venueLabelSpanId').show();
		$('#venueSelectSpanId').hide();
		
		$("#venueId").val(row[0]);
		callChangeArtist();
		getEvents('venueId',row[0]);
});

function getEvents(isArtist,id){
	
	if(id == '') {
		alert('Please select valid artist or venue.');
		return false;
	}
	
	var url = "";
	if(isArtist == 'artistId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?artistId="+id;
	}else if(isArtist == 'venueId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?venueId="+id;
	} /*else if(isArtist == 'childId'){
		url = "GetEventsByGrandChildAndArtistAndVenue?childId="+id+"&brokerId="+brokerId;;
	} else{
		url = "GetEventsByGrandChildAndArtistAndVenue?grandChildId="+id+"&brokerId="+brokerId;
	}*/
	$.ajax({
		dataType:'text',
		url:url,
		cache:false,
		success: function(res){
			//jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			
			$('#eventId').empty();
			for (var i = 0; i < jsonData.length; i++) {
                var  data= jsonData[i]; 
				var rowText = "<option value="+data.id+ ">"+ data.name+" "+data.date+" "+data.time+"</option>"
				$('#eventId').append(rowText);
            }
		}
	}); 
} 

function submitForms(){
	
	var eventId = $("#eventId").val(); 
	if(eventId == '') {
		alert('Please select valid event.');
		return false;
	}
	
	$("#formId").submit();
	
}
function popupTGCatsTicketInfo(eId,tId){
	var url = "CategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	} 
function popupTgcatTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "TMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus();}
	return false;
	}

function popupAutoCatsTicketInfo(eId,tId){
	var url = "AutoCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupAutoTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "AutoTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupVipCatsTicketInfo(eId,tId){
	var url = "VipCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "VipTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupLastFiveRowCatsTicketInfo(eId,tId){
	var url = "LastFiveRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLastFiveRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LastFiveRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}


function popupLastRowCatsTicketInfo(eId,tId){
	var url = "LastRowCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLastRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LastRowTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupLarryLastCatsTicketInfo(eId,tId){
	var url = "LarryLastCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupLarryLastTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "LarryLastTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupVipAutoCatsTicketInfo(eId,tId){
	var url = "VipAutoCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupVipAutoTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "VipAutoTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
	

function popupZonedLastRowCatsTicketInfo(eId,tId){
	var url = "ZonedLastrowMiniCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupZonedLastRowTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ZonedLastrowMiniTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}

function popupZonesPricingCatsTicketInfo(eId,tId){
	var url = "ZonesPricingCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupZonesPricingTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "ZonesPricingTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupJktZonesPricingCatsTicketInfo(eId,tId){
	var url = "JKTZonesPricingCatPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupJktZonesPricingTMATTicketInfo(eId,ticketId,tgid,ticketType){
	var url = "JKTZonesPricingTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId+"&tgid="+tgid+"&ticketType="+ticketType;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayCatsTicketInfo(eId,tId){
	var url = "EbayCategoryPriceHistoryPopup?eId="+eId+"&tId="+tId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	} 
function popupEbayTMATTicketInfo(eId,ticketId){
	var url = "EbayTMATTicketPriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket1Info(eId,ticketId){
	var url = "EbayTMATBaseTicket1PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
function popupEbayTMATBaseTicket2Info(eId,ticketId){
	var url = "EbayTMATBaseTicket2PriceHistoryPopup?eId="+eId+"&ticketId="+ticketId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	}
</script>		