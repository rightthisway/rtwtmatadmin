<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<c:choose>
	<c:when test="${fn:length(tickets) > 0}">
	
		<div style="margin-top:8px;margin-bottom:8px;">
			<div style="float: left"><b>Legends:</b></div>
		
			<div style="float: left" style="margin-left:20px;float:left">
			  <div style="padding-left: 20px;">
				<img src="../images/ico-auction.gif" align="absbottom" title="auction" />
				<font color="green">Auction</font>
		      </div>
			</div>
				
			<div style="float: left" style="margin-left:20px; padding-left: 20px; float:left">
			  <div style="padding-left: 20px;">
				<img src="../images/ico-edelivery.gif" align="absbottom" title="eDelivery" />
				eDelivery
			  </div>
			</div>
		
			<div style="float: left" style="margin-left:20px; padding-left: 20px; float:left">
			  <div style="padding-left: 20px;">
				<img src="../images/ico-instant-edelivery.gif" align="absbottom" title="Instant eDelivery" />
				Instant eDelivery
			  </div>
			</div>
		
			<div style="clear:both"></div>
		</div>
	
		<display:table class="list" name="${tickets}" id="ticket" requestURI="BrowseTickets" pagesize="${pageSize}">
		    <display:column title="" style="18px;" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
			    <c:if test="${ticket.ticketType == 'AUCTION'}">
			    	<img src="../images/ico-auction.gif" title="auction" align="absbottom" />
			    </c:if>

			    <c:choose>
			    	<c:when test="${ticket.ticketDeliveryType == 'EDELIVERY'}">
				    	<img src="../images/ico-edelivery.gif" title="eDelivery" align="absbottom" />
			    	</c:when>
			    	<c:when test="${ticket.ticketDeliveryType == 'INSTANT'}">
				    	<img src="../images/ico-instant-edelivery.gif" title="Instant eDelivery" align="absbottom" />
			    	</c:when>
			    </c:choose>			    	

		    </display:column>
		    <display:column title="Lot Size" property="lotSize" class="text-align-right" />
		    <display:column title="Rem. Qty" property="remainingQuantity" class="text-align-right bold" />
		    <display:column title="Sold Qty" property="soldQuantity" class="text-align-right bold" />
			<display:column title="Zone" property="category.symbol" class="bold" />
		    <display:column title="Section" property="section" class="bold" />
		    <display:column title="Row" property="row"  class="bold" />
		    <display:column title="Seats">
		    	<c:if test="${not empty ticket.seat}">${ticket.seat}</c:if>
		    </display:column>
		    <display:column title="Current Price" property="currentPrice" format="{0,number,currency}" class="text-align-right bold "/>
		    <display:column title="Buy It Now Price" format="{0,number,currency}" value="${ticket.buyItNowPrice}" />
		    <display:column title="Event"><span tooltip="${ticket.event.artist.name} - <fmt:formatDate value="${ticket.event.date}" pattern="yy/MM/dd" /> - ${ticket.event.formattedVenueDescription}"><fmt:formatDate value="${ticket.event.date}" pattern="yy/MM/dd" /></span></display:column>
			<display:column title="Item ID" property="itemId" />
		    <display:column title="Seller"><img src="../images/ico-${ticket.siteId}.gif" border="0" align="absbottom"/>${ticket.seller}</display:column>
		    <display:column title="End Time" property="endDate" format="{0,date,yyyy/MM/dd HH:mm:ss}" />
		</display:table>
	</c:when>
	<c:otherwise>
		<img src="../images/ico-warning.gif" align="absbottom" /><font color="red"><b>NO TICKETS FOUND</b></font>
	</c:otherwise>
</c:choose>