<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	<!-- &gt; <a href="EditorTourCrawls">Tour Crawls</a> --> 
	&gt; <a href="EditorManageCrawls">Manage Crawls</a> 
	&gt;
	<c:choose>
		<c:when test="${param.action == 'create'}">New Crawl</c:when>
		<c:otherwise>${ticketListingCrawlCommand.ticketListingCrawl.siteId}</c:otherwise>
	</c:choose>
</div>

<c:if test="${not empty param.id}">
	<div>
		<b>Crawl: </b>
		<c:choose>
			<c:when test="${not empty previousCrawl}">
				<a href="EditorEditTicketListingCrawl?action=edit&id=${previousCrawl.id}" tooltip="${previousCrawl.name}">Previous</a>
			</c:when>
			<c:otherwise>
				<font color="#cccccc">Previous</font>
			</c:otherwise>
		</c:choose>
		<select onchange="redirectToCrawlPage(this.value)">
			<c:forEach var="ticketListingCrawl" items="${ticketListingCrawls}">
				<option value="${ticketListingCrawl.id}" <c:if test="${ticketListingCrawlCommand.ticketListingCrawl.id == ticketListingCrawl.id}">selected</c:if> style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${ticketListingCrawl.siteId}.gif);background-repeat: no-repeat; height: 20px;">${ticketListingCrawl.name}</option>
			</c:forEach>
		</select>
		<c:choose>
			<c:when test="${not empty nextCrawl}">
				<a href="EditorEditTicketListingCrawl?action=edit&id=${nextCrawl.id}" tooltip="${nextCrawl.name}">Next</a>
			</c:when>
			<c:otherwise>
				<font color="#cccccc">Next</font>
			</c:otherwise>
		</c:choose>

	</div>
</c:if>

<h1>
<c:choose>
	<c:when test="${param.action == 'create'}">Add a new  ${ticketListingCrawlCommand.ticketListingCrawl.siteId} crawl</c:when>
	<c:otherwise>Edit ${ticketListingCrawlCommand.ticketListingCrawl.siteId} crawl</c:otherwise>
</c:choose>
</h1>

<div id="container-tabs" style="margin-top: 5px">
	<ul>
    	<li><a href="#fragment-crawl-parameters"><span>Parameters</span></a></li>
		<c:if test="${not empty ticketListingCrawlCommand.originalName}">
	    	<li><a href="#fragment-crawl-info"><span>Info</span></a></li>
	    	<li><a href="#fragment-crawl-maintenance"><span>Maintenance</span></a></li>
	    </c:if>
	</ul>

	<div id="fragment-crawl-parameters">
	
		<h2>Crawl Parameters</h2>
		
		<form:form commandName="ticketListingCrawlCommand" method="POST">
			<table>
				<tr>		
					<td>Name</td>
					<td>
						<form:input path="ticketListingCrawl.name" cssClass="longInput" />
						<form:errors path="ticketListingCrawl.name" cssClass="error" />			
					</td>
				</tr>
				<c:if test="${ticketListingCrawlCommand.ticketListingCrawl.siteId != 'seatwavefeed'}">
					<tr>		
						<td>Artist</td>
						<td>
						
							<span id="artistLabelSpanId">
								<span id="artistNameDiv" >
									${ticketListingCrawlCommand.ticketListingCrawl.event.artist.name}
								</span>	
								<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
							</span>
							
							
							<span id="artistSelectSpanId" style="display: none;">
								<form:input path="ticketListingCrawl.event.artist.name"/>	
							</span>	
							
							<form:hidden path="ticketListingCrawl.event.artistId"/>
							<form:errors path="ticketListingCrawl.event.artistId" cssClass="error" />	
						</td>
					</tr> 
					<tr>		
						<td>Event</td>		
						<td>
							<div id="eventLoading"><img src="../images/process-running.gif" align="absbottom" />Loading events...</div>
							<form:select path="ticketListingCrawl.eventId" cssStyle="display:none" />
							<form:errors path="ticketListingCrawl.eventId" cssClass="error" />			
						</td>
					</tr>
				</c:if>
				<tr>		
					<td>Automatic Crawl Frequency</td>
					<td>
						<form:radiobutton path="automaticCrawlFrequency" value="false"/>False
						<form:radiobutton path="automaticCrawlFrequency" value="true"/>True
					</td>
				</tr>
				<tr>		
					<td>Crawl Frequency</td>
					<td>
						<form:select path="ticketListingCrawl.crawlFrequency">
							<%-- <form:option value="300">5 minute</form:option>
							<form:option value="600">10 minute</form:option>
							<form:option value="900">15 minute</form:option>
							<form:option value="1800">30 minute</form:option> --%>
							<form:option value="3600">1 hour</form:option>
							<form:option value="7200">2 hour</form:option>
							<form:option value="10800">3 hour</form:option>
							<form:option value="14400">4 hour</form:option>
							<form:option value="18000">5 hour</form:option>
							<form:option value="21600">6 hour</form:option>
							<form:option value="25200">7 hour</form:option>
							<form:option value="28800">8 hour</form:option>
							<form:option value="32400">9 hour</form:option>
							<form:option value="36000">10 hour</form:option>
							<form:option value="39600">11 hour</form:option>
							<form:option value="43200">12 hour</form:option>
							<form:option value="86400">1 day</form:option>
							<form:option value="172800">2 day</form:option>							
							<form:option value="1576800000">Idle</form:option>
						</form:select>
						<form:errors path="ticketListingCrawl.crawlFrequency" cssClass="error" />			
					</td>
				</tr>
				<c:choose>
					<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ebay'}">
						<tr>
							<td colspan="2">
								<b>Ebay parameters</b><br/>
								<i>In order to find the query string to input, go to <a href="http://tickets.ebay.com" target="ebay">http://tickets.ebay.com</a>
								and try some queries (e.g. Britney Spears, New York Giants).<br /> 
								When you get the expected results on eBay, copy/paste your query into the textfield below.</i>
							</td>
						</tr>
						<%--
						<tr>
							<td>Query String</td>
							<td>
								<form:input path="queryString" cssClass="longInput" />
								<form:errors path="queryString" cssClass="error" />									
							</td>
						</tr>
						--%>
						<tr>
							<td>Query Url</td>
							<td>
								<form:input path="ticketListingCrawl.queryUrl" cssClass="longInput" />
								<form:errors path="ticketListingCrawl.queryUrl" cssClass="error" />									
							</td>
						</tr>
						<%--
						<tr>
							<td>Query Date</td>
							<td>						
									
								<form:select path="queryMonth">
									<form:option value="" label="None" />
									<c:forEach var="month" begin="1" end="12" step="1">
										<form:option value="${month}" label="${month}" />
									</c:forEach>
								</form:select>
								
								<form:select path="queryDay">
									<form:option value="" label="None" />
									<c:forEach var="day" begin="1" end="31" step="1">
										<form:option value="${day}" label="${day}" />
									</c:forEach>
								</form:select>
		
								<form:select path="queryYear">
									<form:option value="" label="None" />
									<c:forEach var="year" begin="2009" end="2020" step="1">
										<form:option value="${year}" label="${year}" />
									</c:forEach>
								</form:select>
		
							</td>
						</tr>
						--%>
					</c:when>
					<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eibox'}">
						<tr>
							<td>Query Event Id</td>
							<td>
								<form:input path="queryEventId" />
								<form:errors path="queryEventId" cssClass="error" />									
							</td>
						</tr>
						<tr>
							<td>Query Venue Id</td>
							<td>
								<form:input path="queryVenueId" />
								<form:errors path="queryVenueId" cssClass="error" />									
							</td>
						</tr>
						<tr>
							<td>Query Event Date</td>
							<td>
								<form:input path="queryEventDate" />
								<form:errors path="queryEventDate" cssClass="error" />									
							</td>
						</tr>
					</c:when>
					<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eimarketplace'}">
						<tr>
							<td colspan="2">
								<b>EI Market Place parameters</b><br/>
								<i>In order to find the query string to input, go to <a href="http://www.eimarketplace.com" target="eimarketplace">http://www.eimarketplace.com</a>
								and go to the event page. <br />
								To find the event id, you can either have it in the URL of the page (<font color="blue">https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=<b>761682</b></font> or <font color="blue">http://www.eimarketplace.com/MarketPlace/Inventory/TicketList.aspx?PID=<b>829198</b></font>)<br />
								or you can get it by clicking on the link "Add New Ticket". You can get the event id from there. (<font color="blue">https://www.eimarketplace.com/brokers/editticket_vip.cfm?id=0&p=<b>761682</b></font>).
							</td>
						</tr>
						<tr>
							<td>Query Event Id</td>
							<td>
								<form:input path="queryEventId" />
								<form:errors path="queryEventId" cssClass="error" />									
							</td>
						</tr>
					</c:when>
					<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketnetworkdirect'}">
						<tr>
							<td colspan="2">
								<b>Ticket Network Direct parameters</b><br/>
								 <b><font color="red">This crawler uses the TND test site. Only use this crawl for testing purposes. </b></font><br />
							</td>
						</tr>
						<tr>
							<td>Query Event Id</td>
							<td>
								<form:input path="queryEventId" />
								<form:errors path="queryEventId" cssClass="error" />
							</td>					
						</tr>
						<tr>
							<td colspan="2">
								<i>To know the event id to use, use the search below:</i><br />
							</td>
						</tr>				
					</c:when>
					<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId != 'seatwavefeed'}">
						<tr>
							<td colspan="2"><b>${ticketListingCrawlCommand.ticketListingCrawl.siteId} parameters</b></td>
						</tr>
						<tr>
							<td colspan="2">
								<c:choose>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eventinventory'}">
										On the <a href="http://www.eventinventory.com/search/byevent.cfm?restart=yes&client=1337" target="crawler">Event Inventory website</a> go the ticket page and get the URL.<br />
										The url can be on a page with a list of events:
										<i>e.g.</i> <font color="blue">http://www.eventinventory.com/search/pubsearch.cfm?cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&client=1337&e=52&id=10&cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&RefList=</font>
										<br />
										or a list of tickets:
										<i>e.g.</i> <font color="blue">http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=594&v=386&s=1&month=2&day=10&year=2009&p=692415&cfid=101288044&cftoken=4a64c19-49694649-a304-486c-ab3a-3eaa3190835e&cfuser=67440E7B-DC11-427D-860CEA70F4B30B5A&RefList=#search_top</font>
									</c:when>							
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'fansnap'}">							
										On the <a href="http://www.fansnap.com">FanSnap website</a> go the event page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.fansnap.com/arizona-diamondbacks-tickets/san-francisco-giants-vs-arizona-diamondbacks/68921/april-26-2009-68922</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'getmein'}">							
										On the <a href="http://www.getmein.com">GetMeIn website</a> go the event page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.getmein.com/tickets/u2-tickets/london-117831.html</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'razorgator'}">
										On the RazorGator website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.razorgator.com/tickets/concerts/rock-pop/u2-tickets/?performance=7274859|1</font>
									</c:when>							
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'seatwave'}">
										On the SeatWave website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.seatwave.com/beyoncetickets/metro-radio-arena-tickets/22-may-2009/perf/198403</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'stubhub'}">
										On the StubHub website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.stubhub.com/beyonce-tickets-beyonc--foxborough-gillette-stadium-6-3-2016/event/9519795/</font>
										<br />
										 <strong>Target URL:</strong><font color="red">http://www.stubhub.com/?event_id=9519795</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'vividseat'}">
										On the VividSeats website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.vividseats.com/concerts/justin-bieber-tickets/justin-bieber-rexall-place-6-14-1936642.html</font>
										<br />
										 <strong>Target URL:</strong><font color="red">http://www.vividseats.com/concerts/justin-bieber-tickets/justin-bieber-rexall-place-6-14-1936642.html</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'flashseats'}">
										On the FlshSeats website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">https://www.flashseats.com/Default.aspx?pid=18&ec=9000000000013516&ss=0&fss=1482641628</font>
										<br />
										 <strong>Target URL:</strong><font color="red">https://www.flashseats.com/Default.aspx?pid=18&ec=9000000000013516&ss=0&fss=1482641628</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'tickpick'}">
										On the TickPick website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">https://www.tickpick.com/buy-jackson-browne-tickets-vina-robles-amphitheater-8-26-16-7pm/2793629/</font>
										<br />
										 <strong>Target URL:</strong><font color="red">https://www.tickpick.com/buy-jackson-browne-tickets-vina-robles-amphitheater-8-26-16-7pm/2793629/</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketcity'}">
										On the TicketCity website go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">https://www.ticketcity.com/charleston-wv-tickets/charleston-civic-center-tickets/dolly-parton-jun-7-2016-1482412.html</font>
										<br />
										 <strong>Target URL:</strong><font color="red">https://www.ticketcity.com/charleston-wv-tickets/charleston-civic-center-tickets/dolly-parton-jun-7-2016-1482412.html</font>
									</c:when>
									
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketmaster'}">
										The fetcher can fetch pages  from the TicketMaster website:
										<i>e.g.</i> <font color="blue">http://www.ticketmaster.com/exchange/1B00416C7A862395?tm_link=edp_buytix</font><br />
										or from <a href="http://teamexchange.ticketmaster.com/">Team Exchange TicketMaster website</a>:
										<i>e.g.</i> <font color="blue">https://teamexchange.ticketmaster.com/html/seatlist.htmI?l=EN&team=wachovia&EVNT=PEN0410&CNTX=</font><br />
										or <i>e.g.</i> <font color="blue">https://teamexchange.ticketmaster.com/html/postinglist.htmI?l=EN&EVNT=09A3&team=ducks</font><br /><br />
										<a href="javascript: void(0)" onclick="$('#screenshot-ticketmaster').toggle()">See a sample of a ticketmaster web page which is supported by the crawler.</a><br />
										<div id="screenshot-ticketmaster" style="display:none;border-left: 2px  solid #BD662F;margin: 10px;margin-left: 40px;padding-left: 20px;">
											<img src="../images/crawler/screenshot-ticketmaster.gif" />
										</div>
									</c:when>							
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketnetwork'}">
										This crawl can get the ticket listing from <a href="http://www.ticketnetwork.com">TicketNetwork website</a> or <a href="http://www.zeromarkup.com">ZeroMarkup website</a>.<br />
										Enter in the textfield below, either a ticketnetwork url:
										<i>e.g.</i> <font color="blue">http://www.ticketnetwork.com/tix/u2-sunday-8-9-2009-tickets-1065872.aspx</font><br />
										or a ZeroMarkup url:
										<i>e.g.</i> <font color="blue">http://zeromarkup.com/ResultsTicket.aspx?evtid=968346&event=Spring+Training%3a+Boston+Red+Sox+Vs.+Baltimore+Orioles</font><br />
										Note: Preferentially use zeromarkup.								
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketsnow'}">
										On the <a href="http://www.ticketsnow.com">TicketsNow website</a> go the page where you can see the ticket listing and copy/paste the URL into the textfield below.<br />
										<i>e.g.</i> <font color="blue">http://www.ticketsnow.com/InventoryBrowse/Beyonce-Tickets-at-Citi-Field-in-Flushing-6-8-2016?PID=1862712</font>
										<br />
										 <strong>Target URL:</strong><font color="red">http://www.ticketsnow.com/InventoryBrowse/Beyonce-Tickets-at-Citi-Field-in-Flushing-6-8-2016?PID=1862712</font>
										 
										<a href="javascript: void(0);" onclick="$('#screenshot-ticketsnow').toggle()">See a sample of a ticketsnow web page which is supported by the crawler.</a>
										<div id="screenshot-ticketsnow" style="display:none;border-left: 2px  solid #BD662F;margin: 10px;margin-left: 40px;padding-left: 20px;">
											<img src="../images/crawler/screenshot-ticketsnow.gif" />
										</div>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketsolutions'}">
										On the <a href="http://www.ticketsolutions.com">TicketSolutions website</a>, go the page where you can see the ticket listing and copy/paste the URL into the textfield below.<br />
										e.g. <font color="blue">http://go.ticketsolutions.com/tickets.aspx?eventid=818187</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'viagogo'}">
										On the <a href="http://www.viagogo.com">Viagogo website</a>, go the page where you can see the ticket listing and copy/paste the URL into the textfield below.<br />
										e.g. <font color="blue">http://www.viagogo.com/Concert-Tickets/Rock-and-Pop/Britney-Spears-Tickets/E-175028</font>
									</c:when>
									<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'wstickets'}">
										On the <a href="http://www.wstickets.com/" target="crawler">Western States Ticket Service website</a> go the ticket page and get the URL:
										<i>e.g.</i> <font color="blue">http://www.wstickets.com/football/nfl/Super_Bowl_Tickets.html</font><br /><br />
										If you see on the page, buttons like <img src="../images/crawler/button-eventinventory.gif" align="absmiddle"/> or <img src="../images/crawler/button-eventinventory2.gif" align="absmiddle"/>, use the <b>eventinventory crawl</b> instead.<br />
										You are on the correct page if you see rows like that <img src="../images/crawler/row-wstickets.gif" align="absmiddle"/><br />
										
										If you have an error during the crawl or during the preview, get the url of the iframe instead of the url of the page:
										<a href="javascript:void(0)" onclick="$('#error-help-div').toggle()">click here</a> for more information.
										<div id="error-help-div" style="display:none;border-left: 2px  solid #BD662F;margin: 10px;margin-left: 40px;padding-left: 20px;">									
											<img src="../images/crawler/screenshot-getiframesource.gif" />
											<img src="../images/crawler/screenshot-getiframesource2.gif" />
										</div>
									</c:when>
								</c:choose>
							</td>
						</tr>
						<tr>
							<td>Listing URL</td>
							<td>
								<form:input path="ticketListingCrawl.queryUrl" cssClass="longInput" cssStyle="width: 600px;" />
								<form:errors path="ticketListingCrawl.queryUrl" cssClass="error" />
							</td>
						</tr>
					</c:when>
				</c:choose>
					
				<c:if test="${ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eimarketplace'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eimarketplace'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'eventinventory'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'getmein'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'razorgator'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'seatwave'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'stubhub'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketnetwork'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketnetworkdirect'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketsnow'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'ticketsolutions'
						|| ticketListingCrawlCommand.ticketListingCrawl.siteId == 'viagogo'
					}">
					<tr>
						<td colspan="2" style="border-top:1px dashed black;">
							<a href="javascript:void(0)" onclick="$('#crawlSearchForm').show()"><b>Search for ${ticketListingCrawlCommand.ticketListingCrawl.siteId} events</b></a><br />
							<br />
							<div id="crawlSearchForm" style="display:none">						
								Keywords:<input type="text" id="eventSearchKeywords" style="width:240px" />
								&nbsp;&nbsp;&nbsp;&nbsp;
								Location:<input type="text" id="eventSearchLocation" /><br />						
								<div style="display:inline">
									From date:
									<select id="eventSearchFromDateMonthSelector">
										<c:forEach var="month" begin="0" end="11">
											<option value="${month}">${month + 1}</option>
										</c:forEach>
									</select>
									<select id="eventSearchFromDateDaySelector">
										<c:forEach var="day" begin="1" end="31">
											<option value="${day}">${day}</option>
										</c:forEach>
									</select>
									<select id="eventSearchFromDateYearSelector">
										<c:forEach var="year" begin="2009" end="2019">
											<option value="${year}">${year}</option>
										</c:forEach>
									</select>
									To date:
									<select id="eventSearchToDateMonthSelector">
										<c:forEach var="month" begin="0" end="11">
											<option value="${month}">${month + 1}</option>
										</c:forEach>
									</select>
									<select id="eventSearchToDateDaySelector">
										<c:forEach var="day" begin="1" end="31">
											<option value="${day}">${day}</option>
										</c:forEach>						
									</select>
									<select id="eventSearchToDateYearSelector">
										<c:forEach var="year" begin="2009" end="2019">
											<option value="${year}">${year}</option>
										</c:forEach>
									</select>
								</div>
								<br /><br />
								<img src="../images/ico-start.gif" align="absmiddle"/>
								<a href="javascript:void(0)" onclick="searchEvents()">Search</a>
								<br /><br />
								<div id="eventSearchLoading" style="display:none"><img src="../images/process-running.gif" align="absbottom" />Searching events...</div>						
								<div id="eventSearchResult"></div>
							</div>
						</td>
					</tr>
				</c:if>
								
			</table>
			
			<c:if test="${not empty ticketListingCrawlCommand.originalName}">
				<br />
				<small><i>Note: If you had some tickets with wrong information from previous crawls, you can delete them by going at the maintenance section.</i></small>
			</c:if>
		
			<h2>Preview Crawled Tickets</h2>
			<i>If you don't  know which parameters you have to input in the form above. You can try some values and
			run the preview to see if the parameters are correct.<br />
			You can run the preview safely as the results are not stored  in the database.<br /><br /></i>
			<label for="previewLimitSize">Limit the number of tickets fetched for the preview to</label>
			<select id="previewLimitSize">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="">No limit</option>
			</select><br />
			<i><small>(the more you put, the more you'll have to wait to get the preview)</small></i>
			<br /><br />
			<img src="../images/ico-start.gif" align="absmiddle"/><a href="javascript:previewFetchTickets()">Preview Fetched Tickets</a><br /><br />	
			<div id="previewDiv"></div>
			
			<h2>Save Crawl</h2>
		
			<c:if test="${not empty ticketListingCrawlCommand.originalName}">
				<form:checkbox path="forceRecrawl" /><form:label path="forceRecrawl">Force recrawl after this change</form:label><br  />
			</c:if>
		
			<form:checkbox path="ticketListingCrawl.enabled" /><form:label path="ticketListingCrawl.enabled">Enabled this crawl</form:label><br  />
		
			<br />
		
			<input type="submit" name="submit" value="Save Changes" class="medButton" />
				
			<c:set var="backUrl" value="EditorManageCrawls" />
			<c:if  test="${not empty ticketListingCrawlCommand.backUrl}"><c:set var="backUrl" value="${ticketListingCrawlCommand.backUrl}" /></c:if>
			<input type="button" value="Cancel" class="medButton" onclick="document.location.href='${backUrl}'" />
			
		</form:form>
	</div>
	
	
	<c:if test="${not empty ticketListingCrawlCommand.originalName}">
		<div id="fragment-crawl-info">
		
			<h2>Crawl Info</h2>
			<table>
				<tr>
					<td>Crawl Id:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.id}</td> 
				</tr>
				<tr>
					<td>Creator:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.creator}</td> 
				</tr>
				<tr>
					<td>Creation Date:</td>
					<td><fmt:formatDate value="${ticketListingCrawlCommand.ticketListingCrawl.created}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
				</tr>
				<tr>
					<td>Last Updater:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.lastUpdater}</td> 
				</tr>
				<tr>
					<td>Last Update Date:</td>
					<td>
						<fmt:formatDate value="${ticketListingCrawlCommand.ticketListingCrawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" />
						<%--
						<c:if test="${ticketListingCrawlCommand.ticketListingCrawl.broken}">
							<font color="red"><b>BROKEN</b></font>
						</c:if>
						--%>
					</td>
				</tr>
				<tr>
					<td>Merchant Listing URL:</td>
					<td><a href="${ticketListingUrl}">${ticketListingUrl}</a></td>
				</tr>
				<tr>
					<td>Artist:</td>
					<td>
						<c:choose>
							<c:when test="${empty ticketListingCrawlCommand.ticketListingCrawl.event.artistId}">
								AUTO
							</c:when>
							<c:otherwise>
								<a href="BrowseEvents?artistId=${ticketListingCrawlCommand.ticketListingCrawl.event.artistId}">${ticketListingCrawlCommand.ticketListingCrawl.event.artist.name}</a>
							</c:otherwise>
						</c:choose>
					</td>
				</tr> 
				<tr>
					<td>Event:</td>
					<td>
						<c:choose>
							<c:when test="${empty ticketListingCrawlCommand.ticketListingCrawl.eventId}">
								AUTO
							</c:when>
							<c:otherwise>
								<a href="BrowseTickets?eventId=${ticketListingCrawlCommand.ticketListingCrawl.eventId}">${ticketListingCrawlCommand.ticketListingCrawl.event.name}, ${ticketListingCrawlCommand.ticketListingCrawl.event.formattedVenueDescription}, ${ticketListingCrawlCommand.ticketListingCrawl.event.formattedEventDate}</a>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</table>
		
			<h2>Last Crawl</h2>
			<table>
				<tr>
					<td>Enabled:</td>
					<td>
						${ticketListingCrawlCommand.ticketListingCrawl.enabled}
					</td> 
				</tr>
				<tr>
					<td>Start Crawl:</td>
					<td>
						<fmt:formatDate value="${ticketListingCrawlCommand.ticketListingCrawl.startCrawl}" pattern="yyyy/MM/dd HH:mm:ss" />
					</td> 
				</tr>
				<tr>
					<td>End Crawl:</td>
					<td>
						<fmt:formatDate value="${ticketListingCrawlCommand.ticketListingCrawl.endCrawl}" pattern="yyyy/MM/dd HH:mm:ss" />
					</td> 
				</tr>
				<tr>
					<td>Last successful run:</td>
					<td>
						<fmt:formatDate value="${ticketListingCrawlCommand.ticketListingCrawl.lastSuccessfulRun}" pattern="yyyy/MM/dd HH:mm:ss" />
					</td> 
				</tr>
				<tr>
					<td>Crawl  Phase:</td>
					<td>
						${ticketListingCrawlCommand.ticketListingCrawl.crawlPhase}
					</td> 
				</tr>
				<tr>
					<td>Fetched Byte:</td>
					<td>
						<fmt:formatNumber pattern="0.00" value="${ticketListingCrawlCommand.ticketListingCrawl.fetchingTime / 1024}" />kB
					</td> 
				</tr>
				<tr>
					<td>Fetching Time:</td>
					<td>
						<fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.fetchingTime}" />ms
					</td> 
				</tr>
				<tr>
					<td>Indexation Time:</td>
					<td>
						<fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.indexationTime}" />ms
					</td> 
				</tr>
				<tr>
					<td>Extraction Time:</td>
					<td>
						<fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.extractionTime}" />ms
					</td> 
				</tr>
				<tr>
					<td>Expiration Time:</td>
					<td>
						${ticketListingCrawlCommand.ticketListingCrawl.expirationTime}ms
					</td> 
				</tr>
				<tr>
					<td>State:</td>
					<td>
				    	<c:choose>
				    		<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.errorMessage == 'No tickets found'}">
				    			<font color="red">
				    				NO TICKETS FOUND
				    			</font>
				    		</c:when>
				    		<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.crawlState.error}">
				    			<font color="red">
				    				${ticketListingCrawlCommand.ticketListingCrawl.crawlState}
				    			</font>
				    		</c:when>
				    		<c:when test="${!ticketListingCrawler.running || ticketListingCrawlCommand.ticketListingCrawl.nextCrawlWaitingTime == -1}">
				    			<font color="red">STOPPED</font>
				    		</c:when>
				    		<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.nextCrawlWaitingTime == 0}">
			    				<img src="../images/process-running.gif" align="absbottom"/><font color="green" />RUNNING</font>
				    		</c:when>
				    		<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.nextCrawlWaitingTime >= 60}">
				    			<img src="../images/ico-clock.gif" align="absbottom" /> <font color="#26a000">in <fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.nextCrawlWaitingTime / 60}" pattern="0" />mn</font>
				    		</c:when>
				    		<c:when test="${ticketListingCrawlCommand.ticketListingCrawl.nextCrawlWaitingTime < 60}">
				    			<img src="../images/ico-clock.gif" align="absbottom" /> <font color="#26a000">in &lt;= 1mn</font>
				    		</c:when>
				    	</c:choose>
					</td> 
				</tr>		
				<tr>
					<td>Error Message:</td>
					<td><a href='javascript:showCrawlException(${ticketListingCrawlCommand.ticketListingCrawl.id})'>${ticketListingCrawlCommand.ticketListingCrawl.errorMessage}</a></td> 
				</tr>
				<tr>
					<td>New Item Indexed during the last crawl:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.newItemIndexed}</td> 
				</tr>
				<tr>
					<td>Item Indexed during the last crawl:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.itemIndexed}</td> 
				</tr>
				<tr>
					<td>Item Error Indexed during the last crawl:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.itemErrorIndexed}</td> 
				</tr>
				<tr>
					<td>Cancelled:</td>
					<td>${ticketListingCrawlCommand.ticketListingCrawl.cancelled}</td> 
				</tr>
			</table>

			<h2>Crawl Stat</h2>
			<table class="list">
				<thead>
				<tr>
					<th></th>
					<th>ACTIVE</th>
					<th>SOLD</th>
					<th>EXPIRED</th>
					<th>TOTAL</th>
				</tr>	
				</thead>
				<tbody>
					<tr>
						<td>Listing Count</td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedActiveTicketCount}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedSoldTicketCount}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedExpiredTicketCount}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedTicketCount}" /></td>
					</tr>
					<tr>			
						<td>Ticket Count</td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedActiveTicketQuantitySum}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedSoldTicketQuantitySum}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedExpiredTicketQuantitySum}" /></td>
						<td class="text-align-right"><fmt:formatNumber value="${ticketListingCrawlCommand.ticketListingCrawl.totalFetchedTicketQuantitySum}" /></td>
					</tr>
				</tbody>
			</table>

		</div>	
		
		<div id="fragment-crawl-maintenance">
	
			<h2>Maintenance</h2>
		
			Delete tickets crawled by this crawl.<br />
			
			<table>
				<tr>
					<td rowspan="2"><input type="radio" name="delete-crawl" value="selected" /></td>
					<td>From</td>
					<td>
						<select id="startYear">
							<c:forEach var="year" begin="2009" end="2020">
								<option value="${year}">${year}</option>
							</c:forEach>
						</select>
						/
						<select id="startMonth">
							<c:forEach var="month" begin="0" end="11">
								<option value="${month}"><fmt:formatNumber pattern="00" value="${month + 1}" /></option>
							</c:forEach>
						</select>
						/
						<select id="startDay">
							<c:forEach var="day" begin="1" end="31">
								<option value="${day}"><fmt:formatNumber pattern="00" value="${day}" /></option>
							</c:forEach>
						</select>
						-	
						<select id="startHour">
							<c:forEach var="hour" begin="0" end="23" step="1">
								<option value="${hour}"><fmt:formatNumber pattern="00" value="${hour}" /></option>
							</c:forEach>
						</select>
						:
						<select id="startMinute">
							<c:forEach var="minute" begin="0" end="59" step="1">
								<option value="${minute}"><fmt:formatNumber pattern="00" value="${minute}" /></option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>To</td>
					<td>
						<select id="endYear">
							<c:forEach var="year" begin="2009" end="2020">
								<option value="${year}">${year}</option>
							</c:forEach>
						</select>
						/
						<select id="endMonth">
							<c:forEach var="month" begin="1" end="12">
								<option value="${month}"><fmt:formatNumber pattern="00" value="${month + 1}" /></option>
							</c:forEach>
						</select>
						/
						<select id="endDay">
							<c:forEach var="day" begin="1" end="31">
								<option value="${day}"><fmt:formatNumber pattern="00" value="${day}" /></option>
							</c:forEach>
						</select>
						-	
						<select id="endHour">
							<c:forEach var="hour" begin="0" end="23">
								<option value="${hour}"><fmt:formatNumber pattern="00" value="${hour}" /></option>
							</c:forEach>
						</select>
						:
						<select id="endMinute">
							<c:forEach var="minute" begin="0" end="59" step="1">
								<option value="${minute}"><fmt:formatNumber pattern="00" value="${minute}" /></option>
							</c:forEach>
						</select>
		
					</td>
				</tr>
				<tr>
					<td><input type="radio" name="delete-crawl" value="ALL" checked /></td>
					<td>All</td>
					<td></td>
				</tr>
			</table>
		
			<input type="button" class="medButton" value="Delete" onclick="previewDeleteTickets()" />
			
		</div>
	</c:if>
</div>

<script type="text/javascript" language="javascript">	
	var __siteId = "${ticketListingCrawlCommand.ticketListingCrawl.siteId}";

	function previewFetchTickets() {
		// multiply by one to transform to int		
		var tourId = $('#ticketListingCrawl\\.event\\.artistId').val();
		var eventId = $('#ticketListingCrawl\\.eventId').val();
		
		var previewLimitSize = $('#previewLimitSize').val();
		var queryUrl = $('#ticketListingCrawl\\.queryUrl').val();
		
		var url;
		if (__siteId == "ebay") {
			// var queryYear = $('#queryYear').val();
			// var queryMonth = $('#queryMonth').val();
			// var queryDay = $('#queryDay').val();
			// var queryString = $('#queryString').val();
			url = "EditorCrawlerPreviewFetchTickets?siteId=" + __siteId 
					+ "&tourId=" + tourId
					+ "&eventId=" + ((eventId == null)?"":eventId)
					// + "&queryYear=" + queryYear
					// + "&queryMonth=" + queryMonth
					// + "&queryDay=" + queryDay
					// + "&queryString=" + escape(queryString)
					+ "&queryUrl=" + escape(queryUrl)
					+ "&previewLimitSize=" + previewLimitSize;
		} else if (__siteId == "eimarketplace" || __siteId == "ticketnetworkdirect") {
			var queryEventId = $('#queryEventId').val();
			url = "EditorCrawlerPreviewFetchTickets?siteId=" + __siteId 
					+ "&tourId=" + tourId
					+ "&eventId=" + ((eventId == null)?"":eventId)
					+ "&queryEventId=" + queryEventId
					+ "&queryUrl=" + escape(queryUrl)
					+ "&previewLimitSize=" + previewLimitSize;
		} else if (__siteId == "eibox") {
			var queryEventId = $('#queryEventId').val();
			var queryVenueId = $('#queryVenueId').val();
			var queryEventDate = $('#queryEventDate').val();
			url = "EditorCrawlerPreviewFetchTickets?siteId=" + __siteId 
					+ "&tourId=" + tourId
					+ "&eventId=" + ((eventId == null)?"":eventId)
					+ "&queryEventId=" + queryEventId
					+ "&queryVenueId=" + queryVenueId
					+ "&queryEventDate=" + queryEventDate
					+ "&previewLimitSize=" + previewLimitSize;
		} else if (__siteId == "seatwavefeed") {
			url = "EditorCrawlerPreviewFetchTickets?siteId=" + __siteId 
					+ "&previewLimitSize=" + previewLimitSize;		
		} else {
			
			url = "EditorCrawlerPreviewFetchTickets?siteId=" + __siteId
					+ "&tourId=" + tourId
					+ "&eventId=" + eventId
					+ "&queryUrl=" + escape(queryUrl)
					+ "&previewLimitSize=" + previewLimitSize;
		}
				
		$('#previewDiv').empty();
		$('#previewDiv').append(
			$.IMG({src: '../images/process-running.gif', align: 'absbottom'}),
			"Fetching preview. Please wait..."		
		);	
							
		$.get(url, function(data){
			$('#previewDiv').html(data);
			$('#previewDiv *[tooltip]').tooltip({
					showURL: false,
					bodyHandler: function() { 
						return $(this).attr("tooltip"); 
				    } 			
			});						
			
 		});		
	};
	
	function previewDeleteTickets() {
		var startDate;
		var endDate;

		if ($("INPUT[name='delete-crawl']:checked").val() == "selected") {
			startDate = new Date($('#startYear').val(), $('#startMonth').val(), $('#startDay').val(), $('#startHour').val(), $('#startMinute').val());
			endDate = new Date($('#endYear').val(), $('#endMonth').val(), $('#endDay').val(), $('#endHour').val(), $('#endMinute').val());
		} else {
			startDate = null;
			endDate = null;
		}
		
		DataDwr.getTicketCount(
			"${ticketListingCrawlCommand.ticketListingCrawl.id}"*1,
			startDate,
			endDate,
			function(result) {
				if (result == 0) {
					alert("No ticket to delete");
					return;
				}
				
				var msg;				
				if ($("INPUT[name='delete-crawl']:checked").val() == "selected") {
				 	msg = "Delete " + result + " tickets from " + formatDate(startDate) + " to " + formatDate(endDate) + "?"
				} else {
				 	msg = "Delete all " + result + " tickets?"
				}				
				
				var c = confirm(msg);
				if (c) {
					DataDwr.deleteTickets(
						"${ticketListingCrawlCommand.ticketListingCrawl.id}"*1,
						startDate,
						endDate,
						function(result) {
							if (result) {
								alert("Tickets deleted");
								// refresh
								document.location.href = "EditorEditTicketListingCrawl?action=edit&id=${ticketListingCrawlCommand.ticketListingCrawl.id}";
							} else {
								alert("Error while deleting the ticket");
							}
						}
					);
				}
			}			
		);		
	};
	
	function redirectToCrawlPage(crawlId) {
		document.location.href="EditorEditTicketListingCrawl?id=" + crawlId + "&action=edit";
	};
	
	var eventOptGroups = {};
	
	function changeTour(tourId, eventId) {
		$('#ticketListingCrawl\\.eventId').empty();
		$('#ticketListingCrawl\\.eventId').hide();
		$('#eventLoading').show();
		DataDwr.getEventsByTour(tourId, function(result) {
			// sort result by date
			//did sorting @ server side
			/* var date1,date2;
			result.sort(function(event1, event2) {
				var date1Split = event1.date.split("TBD");
				var date1Length = date1Split.length - 1;
				var date2Split = event2.date.split("TBD");
				var date2Length = date2Split.length - 1;
				if(date1Length == 2 || date2Length == 2)
					return 1;
				if(date1Length == 1 || date2Length == 1)
					return 0;
				
				date1 = new Date(event1.date);
				date2 = new Date(event2.date);
				if (date1 < date2) {
					return -1;
				}
				if (date1 > date2) {
					return 1;
				}
				return 0;
			}); */
			
			if (__siteId == 'ebay') {
				$('#ticketListingCrawl\\.eventId').append($.OPTION({value: ''}, "AUTO"));				
			}
			
			$('#ticketListingCrawl\\.eventId').append(				
				$.map(result, function(event) {
					var label;
					if (event.venueBuilding != null) {
						label = "" + event.date + " - "		//formatDate(event.date)
							+ event.name + " - "
							+ event.venueBuilding
							+ ((event.venueCity == null)?"":(", " + event.venueCity))
							+ ((event.venueState == null)?"":(", " + event.venueState));
					} else {
						label = "" + event.date;
					}					
					return $.OPTION({value: event.id}, label);
				})
			);
			$('#eventLoading').hide();
			$('#ticketListingCrawl\\.eventId').show();
			
			if (eventId) {
				$('#ticketListingCrawl\\.eventId').val(eventId);
				changeEvent(eventId);
			} else {
				if (result != null && result.length > 0) {
					changeEvent(result[0].id);
				}
			}

		});

	};
	
	function changeEvent(eventId) {		
		var eventDescription = $('#ticketListingCrawl\\.eventId OPTION[value=' + eventId + ']').text();
		
		var tokens = eventDescription.split("-");
		if (tokens.length > 2) {
					
			$('#eventSearchKeywords').val(tokens[1]);
	
			var dateTokens = $.trim(tokens[0]).split("/");
			$('#eventSearchFromDateMonthSelector').val("" + (dateTokens[0]*1 - 1));
			$('#eventSearchFromDateDaySelector').val("" + (dateTokens[1] * 1));
			$('#eventSearchFromDateYearSelector').val(dateTokens[2].split(" ")[0]);
	
			$('#eventSearchToDateMonthSelector').val("" + (dateTokens[0]*1 - 1));
			$('#eventSearchToDateDaySelector').val("" + (dateTokens[1] * 1));
			$('#eventSearchToDateYearSelector').val(dateTokens[2].split(" ")[0]);
		}
	}

	$(document).ready(function() {
		$('#container-tabs').tabs();
	
		<c:if test="${ticketListingCrawlCommand.ticketListingCrawl.siteId != 'seatwavefeed'}">
	
			$('#ticketListingCrawl\\.event\\.artistId').change(function() {
				var tourId = $('#ticketListingCrawl\\.event\\.artistId').val();		
				changeTour(tourId);			
			});
			
			<c:choose>
				<c:when test="${empty ticketListingCrawlCommand.ticketListingCrawl.eventId}">
					changeTour($('#ticketListingCrawl\\.event\\.artistId').val());				
				</c:when>
				<c:otherwise>
					changeTour(${ticketListingCrawlCommand.ticketListingCrawl.event.artistId}, "${ticketListingCrawlCommand.ticketListingCrawl.eventId}");
				</c:otherwise>
			</c:choose>
		</c:if>
				
		// focus on crawl name textfield
		$('#ticketListingCrawl\\.name').focus();
		
		// by default set start date to today
		var now = new Date();		
		$('#startYear').val("" + now.getYear());
		$('#startMonth').val("" + (now.getMonth()));
		$('#startDay').val("" + (now.getDate()));

		var tomorrow = new Date(now.getTime() + 24 * 60 * 60 * 1000);
		$('#endYear').val("" + tomorrow.getFullYear());
		$('#endMonth').val("" + (tomorrow.getMonth()));
		$('#endDay').val("" + (tomorrow.getDate()));

		$('#ticketListingCrawl\\.eventId').change(function() {
			changeEvent($(this).val());
		});
	});
	
	function searchEvents() {
		var fromDate = new Date($('#eventSearchFromDateYearSelector').val(), $('#eventSearchFromDateMonthSelector').val(), $('#eventSearchFromDateDaySelector').val());
		var toDate = new Date($('#eventSearchToDateYearSelector').val(), $('#eventSearchToDateMonthSelector').val(), $('#eventSearchToDateDaySelector').val());
		
		$('#eventSearchLoading').show();
		$('#eventSearchResult').hide();
		
		var rowCount = 0;
		
		DataDwr.searchEvents($('#eventSearchKeywords').val(), __siteId, $('#eventSearchLocation').val(), fromDate, toDate,
			function(eventListingResult) {
			
				$('#eventSearchLoading').hide();
				$('#eventSearchResult').show();
				$('#eventSearchResult').empty();
				if (eventListingResult.exception != null) {
					$('#eventSearchResult').html("<font color='red'><b>ERROR: " + eventListingResult.exception.javaClassName + ":" + eventListingResult.exception.message + "</b></font>");
					return;
				}

				eventHits = eventListingResult.eventHits;
				
				if (eventHits == null || eventHits.length == 0) {
					$('#eventSearchResult').html("<font color='red'><b>No result found</b></font>");
					return;
				}

				$('#eventSearchResult').append("Found " + eventHits.length + " events", $.BR());

				$('#eventSearchResult').append(
					$.TABLE({'class': 'list'},
						$.THEAD({},
							$.TR({},
								$.TH({}, "Event ID"),
								$.TH({}, "Event Name"),
								$.TH({}, "Date"),
								$.TH({}, "Location"),
								$.TH({})
							)
						),
						$.TBODY({},
							$.map(eventHits, function(eventHit) {
								rowCount++;
								
								return $.TR({'class': ((rowCount % 2) == 0)?"":"odd"},									
									$.TD({},
										$.IMG({'src': '../images/ico-' + __siteId + '.gif', 'align': 'absbottom'}), 
										$.A({'href': "javascript:selectEventHit('" + eventHit.eventId + "', '" + escape(eventHit.url) + "')"}, eventHit.eventId)
									),
									$.TD({},
										$.A({'href': "javascript:selectEventHit('" + eventHit.eventId + "', '" + escape(eventHit.url) + "')"}, eventHit.name)
									),
									$.TD({}, formatShortDate(eventHit.date) + " - " + ((eventHit.time == null)?"TBD":formatTime(eventHit.time))),
									$.TD({}, eventHit.location),
									$.TD({}, $.A({'href': eventHit.url}, "Listing page"))
								);
								
							})
						)
					)
				);
				
			}
		);
	};

	function selectEventHit(eventId, url) {
		$('#queryEventId').val(eventId);
		$('#ticketListingCrawl\\.queryUrl').val(unescape(url));
	};
	
	$(document).ready(function(){
		$('#ticketListingCrawl\\.event\\.artist\\.name').autocomplete("AutoCompleteArtist", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				return row[1];
			}
		}).result(function (event,row,formatted){
			$('#ticketListingCrawl\\.event\\.artist\\.name').val('');
			$('#artistNameDiv').text(row[1]);
			$('#artistLabelSpanId').show();
			$('#artistSelectSpanId').hide();
			
			$('#ticketListingCrawl\\.event\\.artistId').val(row[0]);
			changeTour(row[0]);	
		});
		
	});

	function callChangeArtist(){ 
		$('#artistLabelSpanId').hide();
		$('#artistSelectSpanId').show();
		$('#tempArtistId').val('');
	}
</script>
