<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Tour Crawls 
</div>

<h1>Crawls</h1>

<form method="GET" action="EditorTourCrawls">
  <b>Artist:</b>
  <select name="artistId" onchange="location.href='EditorTourCrawls?artistId=' + $(this).val()">
    <option value="">ALL</option>
    <c:forEach var="artist" items="${artists}">
      <option value="${artist.id}" <c:if test="${artist.id == param.artistId}">selected</c:if>>${artist.name}</option>
    </c:forEach>
  </select>
</form>

<form method="GET" action="EditorTourCrawls">
  <b>Filter:</b>
  <input type="text" name="filter" value="${param.filter}"/>
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form>

<display:table class="list" name="${tours}" id="tour" requestURI="EditorTourCrawls" defaultsort="4" pagesize="20">

    <display:column value="View"   href="EditorManageCrawls" paramId="tourId" paramProperty="id" class="bold" />
    <display:column title="Dates" sortable="true">
      <span style="white-space:nowrap">
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
		  -
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
	  </span>
    </display:column>
    <display:column title="Artist/Team"  sortable="true" property="artist.name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Location" sortable="true" property="location" />
    <display:column title="Tour/Season Name"  sortable="true" property="name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Event Count" sortable="true" sortProperty="eventCount" class="text-align-center">${tour.eventCount}</display:column>  
    <display:column title="Crawler Count" sortable="true" sortProperty="crawlCount" class="text-align-center" >${tour.crawlCount}</display:column>
      
</display:table>