<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorSuperCrawlCreation">Super Crawl/Event Creation</a>
	&gt; Confirmation
</div>

<h1>Automatic Event Confirmation</h1>
The table below present for each event the number of crawls found for each ticket site. In normal circumstances it should be
1 or 0 (denoted as -).

<form action="EditorSuperCrawlFinish" method="POST">
<c:if test="${not empty includeStubhubEvents}">
	<input type="hidden" name="includeStubhubEvents" id="includeStubhubEvents" value="${includeStubhubEvents}"/> 
</c:if>
<c:if test="${not empty includeExistingEvents}">
	<input type="hidden" name="includeExistingEvents" id="includeExistingEvents" value="${includeExistingEvents}"/> 
</c:if>
<c:if test="${not empty includeSeatwaveEvents}">
	<input type="hidden" name="includeSeatwaveEvents" id="includeSeatwaveEvents" value="${includeSeatwaveEvents}"/> 
</c:if>
<c:if test="${not empty includeTicketNetworkEvents}">
	<input type="hidden" name="includeTicketNetworkEvents" id="includeTicketNetworkEvents" value="${includeTicketNetworkEvents}"/> 
</c:if>
<c:if test="${not empty includeTicketNetworkDataFeedEvents}">
	<input type="hidden" name="includeTicketNetworkDataFeedEvents" id="includeTicketNetworkDataFeedEvents" value="${includeTicketNetworkDataFeedEvents}"/> 
</c:if>
<c:if test="${not empty includeTicketNetworkPOSEvents}">
	<input type="hidden" name="includeTicketNetworkPOSEvents" id="includeTicketNetworkPOSEvents" value="${includeTicketNetworkPOSEvents}"/> 
</c:if>
<table class="list">
  <thead>
    <th></th>
    <th></th>
    <th>Event</th>
    <th>Date</th>
    <!--<th>Tour</th>-->
    <th>Location</th>
    <th>VenueCategoryGroup</th>
	<!--
    <c:forEach var="siteId" items="${constants.enabledSiteIds}">
	  <th><img src="../images/ico-${siteId}.gif" title="${siteId}"/></th>
    </c:forEach>
	-->
  </thead>
  <tbody>
    <c:forEach var="eventListingResult" items="${eventListingResults}">
	    <tr <c:if test="${not eventListingResult.enabled}">style="background:#ffaaaa"</c:if>>
		  <td>
		    <c:choose>
		      <c:when test="${not eventListingResult.enabled}">
		        <img src="../images/ico-error.gif" align="absbottom" tooltip="This event cannot be created because it contains 2+ crawls for the same ticket site" />
		      </c:when>
		      <c:otherwise>
		       <input type="checkbox" name="event-${eventListingResult.eventId}" checked />
		      </c:otherwise>
		    </c:choose>
		    
		  </td>
		  <td>
	        <img src="../images/ico-${fn:toLowerCase(eventListingResult.tourType)}.gif" align="absbottom" title="${eventListingResult.tourType}" />
	      </td>
		  <td>
			<c:choose>
				<c:when test="${fn:length(eventListingResult.name) > 42}">
					<span tooltip="${eventListingResult.name}">
						${fn:substring(eventListingResult.name, 0,  42)}...
					</span>
				</c:when>
				<c:otherwise>
					${eventListingResult.name}
				</c:otherwise>
			</c:choose>
		  </td>
		  <td>
		    <span style="white-space:nowrap">
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${eventListingResult.date}" />&nbsp;
			  <c:choose>
				<c:when test="${not empty eventListingResult.time}">
				    <fmt:formatDate pattern="HH:mm" value="${eventListingResult.time}" />
				</c:when>
			    <c:otherwise>
				  TBD
				</c:otherwise>
			  </c:choose>
			</span>
		  </td>
	      <td>
					${eventListingResult.location}	      
	      </td>
	      <td>
					${eventListingResult.venueCategory.categoryGroup}	      
	      </td>
		  <!--
		  <c:forEach var="siteId" items="${constants.enabledSiteIds}">
		    <td>
		        <c:choose>
		          <c:when test="${fn:length(eventListingResult.eventHitMap[siteId]) != 0}">
		              <c:if test="${siteId == 'ebay'}">
		                <img src="../images/ico-warning.gif" align="absbottom" tooltip="Warning! The ebay crawl which is going to be created will get all the tickets with the event keyword for ANY time during the day" /> 
		              </c:if>
		              <a href="${eventListingResult.eventHitMap[siteId][0].url}" target="_BLANK">
				    ${fn:length(eventListingResult.eventHitMap[siteId])}
				  </a>
		          </c:when>
		          <c:otherwise>
		           -
		          </c:otherwise>
		        </c:choose>
		        
		    </td>
		  </c:forEach>
		  -->
	    </tr>
    </c:forEach>
  </tbody>
</table>
<br/><br/>
<input type="button" class="medButton" onclick="history.go(-1)" value="&lt;&lt;Go back">
<input type="submit" class="medButton" id="submitButton" name="submit" value="Finish Creation" onclick="return confirmEventCreation()"/>
</form>

<script type="text/javascript">

function confirmEventCreation() {
  var answer = confirm("Are you sure you want to create the crawls for the selected events?");
  if (answer) {
    return true;
  }
  
  return false;
}
</script>