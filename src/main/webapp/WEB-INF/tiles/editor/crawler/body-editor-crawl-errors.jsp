<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTourCrawls">Tour Crawls</a> 
	&gt; <a href="EditorManageCrawls">Manage Crawls</a> 
	&gt; Crawl Errors
</div>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<h1>Crawls with errors</h1>

<c:if test="${not empty failedCrawls}">
	<table>
		<tr>
			<td>
				<b>Select: </b>
				<a href="javascript:void(0)" onclick="selectAllTicketListingCrawls()">All</a> -
				<a href="javascript:void(0)" onclick="unselectAllTicketListingCrawls()">None</a>			
			</td>
			<td width="30">&nbsp;</td>
			<td>
				<b>Action: </b>
				<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedTicketListingCrawls()">Enable</a> -
				<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedTicketListingCrawls()">Disable</a> -
				<img src="../images/ico-reload.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="forceRecrawlSelectedTicketListingCrawls()">Force Recrawl</a>
			</td>
		</tr>
	</table>
</c:if>

<display:table class="list" name="${failedCrawls}" id="crawl" requestURI="EditorCrawlErrors" decorator="crawlTableDecorator">
    <display:column sortable="false" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
   	  <input type="checkbox" id="ticket-listing-crawl-${crawl.id}" class="ticket-listing-crawl" onChange="toggleTicketListingCrawlRow($(this).attr('id').split('-')[3])" />
    </display:column>
    <display:column title="Name" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}"/>
    	<c:choose>
    		<c:when test="${fn:length(crawl.name) < 40}">
		    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=EditorCrawlErrors"><b>${crawl.name}</b></a>
    		</c:when>
    		<c:otherwise>
    			<c:set var="part1" value="${fn:substring(crawl.name, 0, 20)}" />
    			<c:set var="part2" value="${fn:substring(crawl.name, fn:length(crawl.name) - 20, fn:length(crawl.name))}" />
		    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=EditorCrawlErrors" tooltip="${crawl.name}"><b>${part1}...${part2}</b></a>
    		</c:otherwise>
    	</c:choose>    	    	
    </display:column>
    <display:column title="Status" sortable="true" style="width: 90px;" decorator="crawlStatusColumnDecorator" value="${crawl}" />
    <display:column title="Created" sortable="true">
		<fmt:formatDate value="${crawl.created}" pattern="yyyy/MM/dd HH:mm:ss" />
    </display:column>
    <display:column title="Creator" property="creator" sortable="true" />
    <display:column title="Last Updated" sortable="true">
		<fmt:formatDate value="${crawl.lastUpdated}" pattern="yyyy/MM/dd HH:mm:ss" />
    </display:column>
    <display:column title="Last Updater" property="lastUpdater" sortable="true" />
    <display:column title="Tour/Season" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
    	<c:set var="crawlTour" value="${crawl.tour}" />
    	<c:choose>    	
    		<c:when test="${fn:length(crawlTour.name) < 40}">
		    	<a href="BrowseEvents?tourId=${crawlTour.id}">${crawlTour.name}</a>    
    		</c:when>
    		<c:otherwise>
    			<c:set var="part1" value="${fn:substring(crawlTour.name, 0, 20)}" />
    			<c:set var="part2" value="${fn:substring(crawlTour.name, fn:length(crawlTour.name) - 20, fn:length(crawlTour.name))}" />
		    	<a href="BrowseEvents?id=${crawlTour.id}" tooltip="${crawlTour.name}">${part1}...${part2}</a>    
    		</c:otherwise>
    	</c:choose>    
    </display:column>
    <display:column title="Event Date" sortable="true" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper" sortProperty="event.localDate">
    	<c:set var="crawlEvent" value="${crawl.event}" />
    	<c:choose>
    		<c:when test="${empty crawlEvent}">
    			AUTO
    		</c:when>
    		<c:otherwise>
			     <a href="BrowseTickets?eventId=${crawlEvent.id}" tooltip="${crawlEvent.name} - <fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" /> - ${crawlEvent.venue.building}">
						<fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" />
			     </a>
    		</c:otherwise>
    	</c:choose>
    </display:column>
    <display:column title="Merchant" sortable="true">
		<a href="RedirectToListingPage?id=${crawl.id}"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a>
	</display:column>
</display:table>

<script type="text/javascript">

function selectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', true);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function unselectAllTicketListingCrawls() {
	$.each($('.ticket-listing-crawl'),  function(i,elt) {
		var crawlId = $(elt).attr('id').split("-")[3];
		$('#ticket-listing-crawl-' + crawlId).attr('checked', false);		
		updateTicketListingCrawlRow(crawlId);
	});
};

function toggleTicketListingCrawlRow(crawlId) {
	$('#ticket-listing-crawl-' + crawlId).attr('checked', !$('#ticket-listing-crawl-' + crawlId).attr('checked'));
	updateTicketListingCrawlRow(crawlId);
};

function updateTicketListingCrawlRow(crawlId) {
	if ($('#ticket-listing-crawl-' + crawlId).attr('checked')) {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', '#748C20');
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', '#ffffff');
	} else {
		$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', null);
		$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', null);
	}
};

function forceRecrawlSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href = "EditorManageCrawls?action=forceRecrawl&crawlIds=" + ids + "&backUrl=EditorCrawlErrors";
};


function __getSelectedTicketListingCrawlIds() {
	var ids = "";
	$.each($('.ticket-listing-crawl'), function(i, elt) {
		if ($(elt).attr('checked')) {
			var id = $(elt).attr('id').split("-")[3]; 
			ids += id + ",";
		}
	});
	
	if (ids == "") {
		return null;
	}
	
	// remove the final comma
	ids = ids.substring(0, ids.length - 1)
	return ids;		
};

$(document).ready(function() {
	$('.ticket-listing-crawl').attr('checked', false);

	$('table#crawl TBODY tr').click(
		function() {
			var checkbox = $($(this).children('TD').get(0)).children('INPUT');
			$(checkbox).attr('checked', !$(checkbox).attr('checked'));
			updateTicketListingCrawlRow($(checkbox).attr('id').split("-")[3]);
		}
	);
});

function enableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=enableCrawl&crawlIds=" + ids + "&backUrl=EditorCrawlErrors";
};
	
function disableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	document.location.href="EditorManageCrawls?action=disableCrawl&crawlIds=" + ids + "&backUrl=EditorCrawlErrors";
};


</script>