<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a>
	&gt; <a href="EditorArtistCrawls">Artist Crawls</a>  
	&gt; <a href="EditorManageCrawls">Manage Crawls</a> 
	&gt; Crawler Controls
</div>

<script type="text/javascript">
	
	function stopCrawler() {
		var result = confirm("Do you really want to stop the crawler?");
		if (result) {
			$('#crawlerStatus').html('<img src="../images/process-running.gif" align="absbottom" /><font color="red">STOPPING...</font>');
			document.location.href="EditorCrawlerControls?action=stopCrawler"; 
		}
	};
		
</script>

<div id="quickLinks">		 	
  <%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
    <a href="AdminEditCrawlerProperties">Crawler Settings</a>
  <%-- </authz:authorize> --%>
</div>
<div style="clear:both"></div>

<h1>Crawler Controls</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<c:choose>
	<c:when test="${ticketListingCrawler.running}">		
		<b>Crawler Status:</b>
		<span id="crawlerStatus"> 
			<img src="../images/process-running.gif" align="absbottom" /><font color="green"><b>RUNNING</b></font>
		</span>
		<a href="javascript:void(0)" onclick="stopCrawler()"><img src="../images/ico-stop.gif" align="absbottom" /> Stop Crawler</a>
		(${ticketListingCrawler.activeThreadCount} threads are running)
	</c:when>
	<c:otherwise>
		<b>Crawler Status:</b> <font color="red">NOT RUNNING</font>
		<a href="EditorCrawlerControls?action=startCrawler"><img src="../images/ico-start.gif" align="absbottom" /> Start Crawler</a>
	</c:otherwise>
</c:choose>
<br /><br />
<b>Crawler Phase:</b>
<c:choose>
	<c:when test="${ticketListingCrawler.phase == 'SCHEDULED'}">
		<font color="green"><b>SCHEDULED</b></font> [priority to events occuring soon] <c:if test="${ticketListingCrawler.crawlerSchedulerManager.schedulerMode == 'SCHEDULED_UNSCHEDULED'}"> (from ${ticketListingCrawler.crawlerSchedulerManager.automaticScheduleStartHour}:00 to ${ticketListingCrawler.crawlerSchedulerManager.automaticScheduleStopHour}:00)</c:if>
	</c:when>
	<c:when test="${ticketListingCrawler.phase == 'UNSCHEDULED'}">
		<font color="blue"><b>UNSCHEDULED</b></font> [equal priority for all events] (from ${ticketListingCrawler.crawlerSchedulerManager.automaticScheduleStopHour}:00 to ${ticketListingCrawler.crawlerSchedulerManager.automaticScheduleStartHour}:00)
	</c:when>
	<c:when test="${ticketListingCrawler.phase == 'IDLE'}">
		<font color="red"><b>IDLE</b></font> [no crawlers running for maintenance] <c:if test="${ticketListingCrawler.idlePeriodEnabled}"> (from ${ticketListingCrawler.idlePeriodStartHour}:00 to ${ticketListingCrawler.idlePeriodStopHour}:00)</c:if> 
	</c:when>
	<c:otherwise>
		<font color="red"><b>UNDEFINED</b> [Please contact your administrator]</font> 
	</c:otherwise>
</c:choose>
<br /><br />
<%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	You can edit some crawler settings on the <a href="AdminEditCrawlerProperties">Edit Crawler Properties Page</a>.
<%-- </authz:authorize> --%>


<c:if test="${ticketListingCrawler.running}">
	<h2>Running crawls</h2>	
	Dispatched crawls: ${fn:length(runningTicketListingCrawls)}<br /><br />
	
	<display:table class="list" name="${runningTicketListingCrawls}" id="crawl" requestURI="EditorManageCrawls" decorator="crawlTableDecorator">
	    <display:column title="Name" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<img src="../images/ico-${crawl.siteId}.gif" title="${crawl.siteId}" align="absbottom"/>
	    	<c:choose>
	    		<c:when test="${editorCrawlerView == 'expanded' || fn:length(crawl.name) < 40 }">
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=EditorCrawlerControls"><b>${crawl.name}</b></a>
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawl.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawl.name, fn:length(crawl.name) - 20, fn:length(crawl.name))}" />
			    	<a href="EditorEditTicketListingCrawl?action=edit&id=${crawl.id}&backUrl=EditorCrawlerControls" tooltip="${crawl.name}"><b>${part1}...${part2}</b></a>
	    		</c:otherwise>
	    	</c:choose>    	    	
	    		<c:if test="crawl.suspended">
	    		  <i>(suspended)</i>
	    		</c:if>
	    </display:column>
	    <display:column title="Node">
	      <c:choose>
	        <c:when test="${not empty crawl.nodeId}">
  	          <img src="../images/ico-screen-blue.gif" align="absbottom"/> ${crawl.nodeId} - ${crawl.hostname} 
	        </c:when>
	        <c:otherwise>
	        -
	        </c:otherwise>
	      </c:choose>
	    </display:column>
	    <display:column title="Priority">
	      <c:choose>
	        <c:when test="${crawl.priority == 5}">
  	          NORMAL 
	        </c:when>
	        <c:when test="${crawl.priority == 10}">
  	          <font color="green"><b>HIGH</b></font> 
	        </c:when>
	        <c:otherwise>
  	          <i>LOW</i> 
	        </c:otherwise>
	      </c:choose>	     
	    </display:column>
	    <display:column title="Start time" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
			<fmt:formatDate value="${crawl.startCrawl}" pattern="HH:mm:ss" />		    	    	
	    </display:column>
	    <display:column title="Phase" property="crawlPhase" />
	    <display:column title="Tour" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	       	<c:set var="crawlTour" value="${crawl.tour}" />		    
	    	<c:choose>
	    		<c:when test="${editorCrawlerView == 'expanded' || fn:length(crawlTour.name) < 40 }">
			    	<a href="BrowseEvents?tourId=${crawlTour.id}">${crawlTour.name}</a>    
	    		</c:when>
	    		<c:otherwise>
	    			<c:set var="part1" value="${fn:substring(crawlTour.name, 0, 20)}" />
	    			<c:set var="part2" value="${fn:substring(crawlTour.name, fn:length(crawlTour.name) - 20, fn:length(crawlTour.name))}" />
			    	<a href="BrowseEvents?id=${crawlTour.id}" tooltip="${crawlTour.name}">${part1}...${part2}</a>    
	    		</c:otherwise>
	    	</c:choose>    
	    </display:column>
	    
	    <display:column title="Event" decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">
	    	<c:set var="crawlEvent" value="${crawl.event}" />
	    	<c:choose>
	    		<c:when test="${empty crawlEvent}">
	    			AUTO
	    		</c:when>
	    		<c:otherwise>
	    			<c:choose>
	    				<c:when test="${editorCrawlerView == 'expanded'}">
						     <a href="BrowseTickets?eventId=${crawlEvent.eventId}">
									<fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" />
									<br />
									${crawlEvent.venue.building}
						     </a>
	    				</c:when>
	    				<c:otherwise>
						     <a href="BrowseTickets?eventId=${crawlEvent.id}" tooltip="${crawlEvent.name} - <fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" /> - ${crawlEvent.venue.building}">
								<fmt:formatDate value="${crawlEvent.date}" pattern="yyyy/MM/dd" />
						     </a>
	    				</c:otherwise>
	    			</c:choose>
	    		</c:otherwise>
	    	</c:choose>
	    </display:column>
	    <display:column decorator="com.admitone.tmat.web.decorators.ExtraWhiteSpaceStripWrapper">    
	   		<a href="RedirectToListingPage?id=${crawl.id}"><img src="../images/ico-link.gif" align="absbottom" />Merchant</a>
	   	</display:column>
	</display:table>
	<br />
	<font color="#888888">Crawl cycle:
	  <b tooltip="Crawl is dispatched by the master to be taken by a worker">DISPATCHED</b>
	  &rarr; <b tooltip="The worker is fetching the tickets from a ticket site">EXTRACTION</b>
	  &rarr; <b tooltip="The worker is writing the tickets to the database">FLUSH</b>
	  &rarr; <b tooltip="The worker expires tickets of the crawls which are outdated">EXPIRATION</b>
	  &rarr; <b tooltip="The worker sends back the crawl to the master">REQUEUING</b>
	  &rarr; <b tooltip="The crawl is idle">STOPPED</b></font> 
</c:if>	

<h2>Crawl Stats</h2>
<b>Number of enabled crawls:</b> <fmt:formatNumber value="${totalEnabledTicketListingCrawlCount}" /><br />
<b>Number of disabled crawls:</b> <fmt:formatNumber value="${disabledTicketListingCrawlCount}" /><br />
<b>Number of failed crawls:</b> <a href="EditorCrawlErrors"><fmt:formatNumber value="${failedTicketListingCrawlCount}" /></a><br />
<b>Number of ready crawls:</b> <fmt:formatNumber value="${readyTicketListingCrawlCount}" /><br />
<b>Least recently processed crawl at:</b> <fmt:formatDate value="${leastRecentlyUpdatedCrawl.startCrawl}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
<b>Most recently processed crawl at:</b> <fmt:formatDate value="${mostRecentlyUpdatedCrawl.startCrawl}" pattern="yyyy/MM/dd HH:mm:ss" /><br />

<div id="container-tabs" style="margin-top: 5px">
  <ul>
    <c:forEach var="stat" items="${stats}">
      <li><a href="#fragment-${stat.name}"><img src="../images/ico-screen.gif" align="absbottom"/> ${stat.name}</a></li>
    </c:forEach>
  </ul>

  <c:forEach var="stat" items="${stats}">
    <div id="fragment-${stat.name}">
      <b>Last update:</b> <fmt:formatDate value="${stat.lastUpdate}" pattern="yyyy/MM/dd - HH:mm:ss" />
      <div style="float:right">
      	<a href="EditorCrawlerControls?action=reset&node=${stat.name}" onclick="return confirmResetStat('${stat.name}');"><img src="../images/ico-reload.gif" align="absbottom"/>Reset stats</a>      
      </div>
      <h2>Crawl Processing Stats</h2>
		<input type="checkbox" id="divideTimeByThreadCount" onchange="changeDivideTimeByThreadCount(this.checked)" <c:if test="${divideTimeByThreadCount}">checked</c:if>/><label for="divideTimeByThreadCount">Divide time by running thread count</label>
		<br />
		<i>Note: When the checkbox is unchecked, the estimated time is measured as if there was only one crawler thread running (instead of ${ticketListingCrawler.activeThreadCount}).</i> 
		<br />
		
		<c:choose>
			<c:when test="${divideTimeByThreadCount}">
				<c:choose>
					<c:when test="${ticketListingCrawler.processingCrawlThreadCount == 0}">
						<c:set var="processingCrawlThreadCount" value="${ticketListingCrawler.activeThreadCount}" />
					</c:when>
					<c:otherwise>
						<c:set var="processingCrawlThreadCount" value="${ticketListingCrawler.processingCrawlThreadCount}" />
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<c:set var="processingCrawlThreadCount" value="1" />
			</c:otherwise>
		</c:choose>
		<br />
			
		<table class="list">
			<thead>
				<tr>
					<th></th>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<th><img src="../images/ico-${siteId}.gif" title="${siteId}"/></th>
					</c:forEach>
					<th>All</th>
				</tr>
			</thead>
			<tbody>
				<tr class="odd">
					<td>Least recently processed crawl at</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<span tooltip="<fmt:formatDate value="${leastRecentlyUpdatedCrawlDateBySiteId[siteId]}" pattern="yyyy/MM/dd - HH:mm:ss" />"><fmt:formatDate value="${leastRecentlyUpdatedCrawlDateBySiteId[siteId]}" pattern="HH:mm:ss" /></span>						
						</td>
					</c:forEach>
					<td align="right"><span tooltip="<fmt:formatDate value="${leastRecentlyUpdatedCrawl.startCrawl}" pattern="yyyy/MM/dd - HH:mm:ss" />"><fmt:formatDate value="${leastRecentlyUpdatedCrawl.startCrawl}" pattern="HH:mm:ss" /></span></td>
				</tr>				
				<tr class="odd">
					<td>Most recently processed crawl at</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<span tooltip="<fmt:formatDate value="${mostRecentlyUpdatedCrawlDateBySiteId[siteId]}" pattern="yyyy/MM/dd - HH:mm:ss" />"><fmt:formatDate value="${mostRecentlyUpdatedCrawlDateBySiteId[siteId]}" pattern="HH:mm:ss" /></span>						
						</td>
					</c:forEach>
					<td align="right"><span tooltip="<fmt:formatDate value="${mostRecentlyUpdatedCrawl.startCrawl}" pattern="yyyy/MM/dd - HH:mm:ss" />"><fmt:formatDate value="${mostRecentlyUpdatedCrawl.startCrawl}" pattern="HH:mm:ss" /></span></td>
				</tr>				
				<tr>
					<td>Number of running crawls</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${runningTicketListingCrawlCountBySiteId[siteId] == 0}">-</c:when>						
								<c:otherwise><font color="green">${runningTicketListingCrawlCountBySiteId[siteId]}</font></c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><font color="green">${fn:length(runningTicketListingCrawls)}</font></td>
				</tr>				
				<tr>
					<td>Number of running crawls (verification)</td>
					<c:set var="totalActiveTicketListingCrawlCount" value="0" />
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${activeTicketListingCrawlCountBySiteId[siteId] == 0}">-</c:when>						
								<c:otherwise><font color="green">${activeTicketListingCrawlCountBySiteId[siteId]}</font></c:otherwise>
							</c:choose>
						</td>
						<c:set var="totalActiveTicketListingCrawlCount" value="${totalActiveTicketListingCrawlCount + activeTicketListingCrawlCountBySiteId[siteId]}" />
					</c:forEach>
					<td align="right"><font color="green">${totalActiveTicketListingCrawlCount}</font></td>
				</tr>				
				<tr>
					<td>Number of failed crawls</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${failedTicketListingCrawlCountBySiteId[siteId] == 0}">-</c:when>						
								<c:otherwise><font color="red">${failedTicketListingCrawlCountBySiteId[siteId]}</font></c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right">
						<c:choose>
							<c:when test="${failedTicketListingCrawlCount == 0}">-</c:when>						
							<c:otherwise><font color="red">${failedTicketListingCrawlCount}</font></c:otherwise>
						</c:choose>
					</td>
				</tr>				
				<tr>
					<td>Number of ready crawls</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${readyTicketListingCrawlCountBySiteId[siteId] == 0}">-</c:when>						
								<c:otherwise>${readyTicketListingCrawlCountBySiteId[siteId]}</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right">
						<c:choose>
							<c:when test="${readyTicketListingCrawlCount == 0}">-</c:when>						
							<c:otherwise>${readyTicketListingCrawlCount}</c:otherwise>
						</c:choose>
					</td>
				</tr>				
				<tr class="odd">
					<td>Total number of crawls</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${totalTicketListingCrawlCountBySiteId[siteId] == 0}">-</c:when>						
								<c:otherwise>${totalTicketListingCrawlCountBySiteId[siteId]}</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right">${totalTicketListingCrawlCount - disabledTicketListingCrawlCount}</td>
				</tr>				
				<tr>
					<td>Avg. fetched bytes per crawl</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right"> 
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageFetchedByteCount == 0}">-</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageFetchedByteCount / 1024}" />kB</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalFetchedByteCount  / (totalEnabledTicketListingCrawlCount * 1024 * processingCrawlThreadCount)}" />kB</td>
				</tr>				
				<tr class="odd">
					<td>Bandwidth</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right"> 
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].bandwidth == 0}">-</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].bandwidth / 1024}" />kB/s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalBandwidth  / (1024 * processingCrawlThreadCount)}" />kB/s</td>
				</tr>				
				<tr>
					<td>Avg. number of ticket entries fetched per crawl</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right"> 
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageProcessedTicketCount == 0}">-</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageProcessedTicketCount}" /></c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.averageProcessedTicketCount}" /></td>
				</tr>				
				<tr>
					<td>Avg. dispatching time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageDispatchingTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageDispatchingTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalDispatchingTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr>
					<td>Avg. fetching time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageFetchingTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageFetchingTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalFetchingTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr>
					<td>Avg. extraction time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageExtractionTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageExtractionTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalExtractionTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr>
					<td>Avg. indexation time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageIndexationTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageIndexationTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalIndexationTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr>	
					<td>Avg. flushing time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageFlushingTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageFlushingTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalFlushingTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr>	
					<td>Avg. expiration time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageExpirationTime == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageExpirationTime / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalExpirationTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
				<tr class="odd">
					<td>Avg. total processing crawl time <c:if test="${divideTimeByThreadCount}">/ thread</c:if></td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageTimeToProcessCrawl == 0}">-	</c:when>
								<c:otherwise><fmt:formatNumber pattern="0.00" value="${stat.siteStatMap[siteId].averageTimeToProcessCrawl / (1000 * processingCrawlThreadCount)}" />s</c:otherwise>
							</c:choose>
						</td>
					</c:forEach>
					<td align="right"><fmt:formatNumber pattern="0.00" value="${stat.totalProcessingTime / (totalEnabledTicketListingCrawlCount * 1000 * processingCrawlThreadCount)}" />s</td>
				</tr>
			</tbody>
			<tfoot>				
				<tr>
					<td>Est. total processing time</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:set var ="timeToProcessAllCrawlsInSeconds" value="${totalTicketListingCrawlCountBySiteId[siteId] * stat.siteStatMap[siteId].averageTimeToProcessCrawl / (1000 * processingCrawlThreadCount)}" />
							<c:choose>
								<c:when test="${stat.siteStatMap[siteId].averageTimeToProcessCrawl == 0}">-	</c:when>
								<c:when test="${timeToProcessAllCrawlsInSeconds < 60}"><fmt:formatNumber pattern="0.00" value="${timeToProcessAllCrawlsInSeconds}" />s</c:when>
								<c:when test="${timeToProcessAllCrawlsInSeconds < 3600}">
									<fmt:formatNumber pattern="0" value="${timeToProcessAllCrawlsInSeconds / 60 -  0.49}" />m<fmt:formatNumber pattern="00" value="${(timeToProcessAllCrawlsInSeconds - 0.49) % 60}" />s
								</c:when>
								<c:otherwise>
									<fmt:formatNumber pattern="0" value="${timeToProcessAllCrawlsInSeconds / 3600 - 0.49}" />h<fmt:formatNumber pattern="00" value="${(timeToProcessAllCrawlsInSeconds / 60 - 0.49)  % 60}" />m
								</c:otherwise>
							</c:choose>						
						</td>
					</c:forEach>
					<td align="right">
						<c:set var ="totalTimeToProcessAllCrawlsInSeconds" value="${stat.totalProcessingTime / (1000 * processingCrawlThreadCount)}" />
						<c:choose>
							<c:when test="${totalTimeToProcessAllCrawlsInSeconds < 60}">
								<fmt:formatNumber pattern="0.00" value="${totalTimeToProcessAllCrawlsInSeconds}" />s
							</c:when>
							<c:when test="${totalTimeToProcessAllCrawlsInSeconds < 3600}">
								<fmt:formatNumber pattern="0" value="${totalTimeToProcessAllCrawlsInSeconds / 60 - 0.49}" />m<fmt:formatNumber pattern="00" value="${(totalTimeToProcessAllCrawlsInSeconds - 0.49) % 60}" />s
							</c:when>
							<c:otherwise>
								<fmt:formatNumber pattern="0" value="${totalTimeToProcessAllCrawlsInSeconds / 3600 - 0.49}" />h<fmt:formatNumber pattern="00" value="${(totalTimeToProcessAllCrawlsInSeconds / 60 - 0.49) % 60}" />m
							</c:otherwise>
						</c:choose>					
					</td>
				</tr>				
				<tr>
					<td>Est. total processing time (%)</td>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<td align="right">
							<c:set var ="timeToProcessAllCrawlsInSeconds" value="${totalTicketListingCrawlCountBySiteId[siteId] * stat.siteStatMap[siteId].averageTimeToProcessCrawl / (1000 * processingCrawlThreadCount)}" />
							<fmt:formatNumber pattern="0.00" value="${timeToProcessAllCrawlsInSeconds * 100.0 / totalTimeToProcessAllCrawlsInSeconds}" />%
						</td>
					</c:forEach>
					<td align="right">
						100.00%
					</td>
				</tr>				
			</tfoot>
		</table>

		<h2>Crawler Scheduling Stats</h2>
		<c:choose>
		<c:when test="${not empty stat.delayByFrequencyValues}">
		<table class="list">
		  <thead>
		    <th></th>
		    <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		      <th>
		          <c:choose>
		           <c:when test="${frequencyStat.frequency == -1}">
		              All
		            </c:when>
		            <c:otherwise>
		              <fmt:formatNumber pattern="0" value="${frequencyStat.frequency / 60}"/>mn
		            </c:otherwise>
		          </c:choose>
		      </th>
		    </c:forEach>
		  </thead>
		  <tbody>
		    <tr>
			  <td>Number of Scheduled Crawls</td>
		      <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		        <td align="right">${frequencyStat.totalCount - frequencyStat.proActiveCount}</td>
		      </c:forEach>
		    </tr>
		    <tr>
			  <td>Number of Pro-Active Crawls</td>
		      <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		        <td align="right">${frequencyStat.proActiveCount}</td>
		      </c:forEach>
		    </tr>
		    <tr class="odd">
			  <td>Total Number of Crawls</td>
		      <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		        <td align="right">${frequencyStat.totalCount}</td>
		      </c:forEach>
		    </tr>
		    <tr>
			  <td>Avg. Pro-Active Time</td>
		      <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		        <td align="right">
		          <fmt:formatNumber pattern="0" value="${frequencyStat.proActiveAverageTime / 1000}"/>s
		        </td>
		      </c:forEach>
		    </tr>
		    <tr>
			  <td>Avg. Delay</td>
		      <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		        <td align="right">
		          <fmt:formatNumber pattern="0" value="${frequencyStat.averageDelay / 1000}"/>s
		        </td>
		      </c:forEach>
		    </tr>
		    <tfoot>
		      <tr>
			    <td>Avg. Delay (%)</td>
		        <c:forEach var="frequencyStat" items="${stat.delayByFrequencyValues}">
		          <td align="right"><fmt:formatNumber pattern="0.00" value="${frequencyStat.averageDelayInPercent}"/>%</td>
		        </c:forEach>
		      </tr>
		    </tfoot>
		  </tbody>
		</table>
		<br/>
		</c:when>
		<c:otherwise>
		<i>No information yet (they will appear after a first round of crawls)...</i>
		</c:otherwise>
	  </c:choose>

		<h2>Indexation Stats</h2>
		<table class="list">
		  <thead>
		    <th></th>
		    <th>Update</th>
		    <th>Insert</th>
		    <th>All</th>
		  </thead>
		  <tbody>
		  <tr>
		    <td>Avg. ticket info. extraction time</td>
		    <td align="right">
		    -
		    </td>
		    <td align="right">
		    -
		    </td>
		    <td align="right">
			  <fmt:formatNumber pattern="0.00" value="${stat.ticketHitIndexerStat.averageIndexingTime}" />ms
		    </td>
		  </tr>
		  <tr>
		    <td>Avg. time to load a ticket from the DB</td>
		    <td align="right">
		    -
		    </td>
		    <td align="right">
		    -
		    </td>
		    <td align="right">
		      <fmt:formatNumber type="number" pattern="0.00" value="${stat.ticketHitIndexerStat.averageDBLoadTicketTime}" />ms
		    </td> 
		  </tr>
		  <tr>
		    <td>Avg. time to store a ticket in the DB</td>
		    <td align="right">
		      <fmt:formatNumber type="number" pattern="0.00" value="${stat.ticketHitIndexerStat.averageDBUpdateTicketTime}" />ms    </td>
		    <td align="right">
		      <fmt:formatNumber type="number" pattern="0.00" value="${stat.ticketHitIndexerStat.averageDBInsertTicketTime}" />ms    </td>
		    <td align="right">
		      <fmt:formatNumber type="number" pattern="0.00" value="${stat.ticketHitIndexerStat.averageDBStoreTicketTime}" />ms
		    </td> 
		  </tr>
		    <tr>
		      <td>
		        Number of tickets
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" value="${stat.ticketHitIndexerStat.updatedTicketCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" value="${stat.ticketHitIndexerStat.insertedTicketCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" value="${stat.ticketHitIndexerStat.updatedTicketCount + stat.ticketHitIndexerStat.insertedTicketCount}" />
		      </td>
		    </tr>
		    <tr>
		      <td>
		        Warnings
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.updatedTicketWarningCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.insertedTicketWarningCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.updatedTicketWarningCount + stat.ticketHitIndexerStat.insertedTicketWarningCount}" />
		      </td>
		    </tr>
		    <tr>
		      <td>Errors</td>
		      <td align="right">
		       <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.updatedTicketErrorCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.insertedTicketErrorCount}" />
		      </td>
		      <td align="right">
		        <fmt:formatNumber type="number" pattern="0" value="${stat.ticketHitIndexerStat.updatedTicketErrorCount + stat.ticketHitIndexerStat.insertedTicketErrorCount}" />
		      </td>
		    </tr>
		  </tbody>
		</table>
		<br/>	  
    </div>
  </c:forEach>
</div>

<script type="text/javascript">
  $('#container-tabs').tabs();

function confirmResetStat(nodeId) {
  var answer = confirm("Are you sure you want to reset the stats for node " + nodeId + "?");
  if (answer) {
    return true;
  }
  
  return false;
};

function changeDivideTimeByThreadCount(divideTimeByThreadCount) {
	document.location.href = "EditorCrawlerControls?divideTimeByThreadCount=" + (divideTimeByThreadCount?"1":"0");
};

</script>