
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="../../decorators.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<h1>Existing Crawl Event Details</h1>
<body>
<c:if test="${not empty crawl}" >
	<div>
	<table>
		<tr>
			<td><b>Event </b></td>
			<td> : ${crawl.event.name}</td>
		</tr>
		<tr>
			<td><b>Event Date </b></td>
			<td> : <fmt:formatDate pattern="MM/dd/yyyy" value="${crawl.event.localDate}" />				
				<c:choose>
				<c:when test="${crawl.event.localTime == null}">
							 TBD
				</c:when>
				<c:otherwise>
					<fmt:formatDate pattern="hh:mm aa" value="${crawl.event.localTime}" />
				</c:otherwise>
				</c:choose>	
			</td>
		</tr>
		<tr>
			<td><b>Venue </b></td>
			<td> : ${crawl.event.venue.location}</td>
		</tr>
		<tr>
			<td><b>Site </b></td>
			<td> : ${crawl.siteId}</td>
		</tr>
		<tr>
			<td><b>Url </b></td>
			<td> : ${crawl.queryUrl}</td>
		</tr>
		<tr>
			<td><b>Crawl Created By </b></td>
			<td> : ${crawl.creator}</td>
		</tr>
		<tr>
			<td><b>Crawl Created Date </b></td>
			<td> : <fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${crawl.created}" /></td>
		</tr>
	</table>
		
	</div>
</c:if>

<c:if test="${empty crawl}" >
<div align='center'>
	 <h4><span  id="emptyMsg">There isn't any details for this selection.</span></h4> 
</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>