<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate pattern="yyyy" value="${now}" var="eventYear" />

<fmt:formatDate pattern="yyyy" value="${fromDate}" var="fromYear" />
<fmt:formatDate pattern="M" value="${fromDate}" var="fromMonth" />
<fmt:formatDate pattern="d" value="${fromDate}" var="fromDay" />

<fmt:formatDate pattern="yyyy" value="${toDate}" var="toYear" />
<fmt:formatDate pattern="M" value="${toDate}" var="toMonth" />
<fmt:formatDate pattern="d" value="${toDate}" var="toDay" />

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Automatic Event/Crawl Creation
</div>

<div class="info">
  <div class="infoText">
    This page allows you to create crawls automatically for events.<br/>
    You can choose to display events already in our system as well as events from Stubhub or Seatwave which are not already in our system.
    If you create crawls for an event not already present in the system, it will create it automatically for you.
  </div>
</div>

<c:if test="${not empty results}">
  <div class="info">
    <div class="infoText">
      <c:forEach var="result" items="${results}">
        <c:choose>
          <c:when test="${empty result.errorMessage}">
            <a href="BrowseTickets?eventId=${result.systemEventId}">${result.name}</a>: ${result.message}<br/>
          </c:when>
          <c:otherwise>
            <img src="../images/ico-warning.gif" align="absbottom"/>${result.errorMessage}<br/>
          </c:otherwise>
        </c:choose>
        <c:forEach var="message" items="${result.crawlErrorMessageEntries}">
          <c:choose>
            <c:when test="${empty message.value}">
              <img src="../images/ico-${message.key}.gif" tooltip="${message.key}"/> The crawl for ${message.key} has been created.<br/>
            </c:when>
            <c:otherwise>
              <img src="../images/ico-warning.gif" align="absbottom"/> <img src="../images/ico-${message.key}.gif" tooltip="${message.key}"/> ${message.value}<br/>
            </c:otherwise>
          </c:choose>
        </c:forEach>
        <br/>
      </c:forEach>
    </div>
  </div>
</c:if>

<h1>Automatic Event/Crawl Creation</h1>
<!-- <a href="DownloadLastTwoDaysEventsNotInTmatReport">Events Created in Last Two Days in POS But not in TMAT..</a>
<br/>
<a href="EditorDownloadLessCrawlEvents?latestEvent=false">All Events With Two or Less than Two Crawls..</a>
<br/>
<a href="EditorDownloadLessCrawlEvents?latestEvent=true">Last 15Days Added Events With Two or Less than Two Crawls..</a>
 -->
 <br/><br/><br/>

<form action="EditorSuperCrawlCreation" method="GET">
    Event: <input type="text" name="eventFilter" value="${param.eventFilter}"/>
    <input name="matchAll" type="checkbox" <c:if test="${not empty param.matchAll}">checked</c:if>/> Match All &nbsp; &nbsp; &nbsp; &nbsp;   
    Location: <input type="text" name="locationFilter" value="${param.locationFilter}"/>
    <input name="locationMatchAll" type="checkbox" <c:if test="${not empty param.locationMatchAll}">checked</c:if>/> Match All &nbsp; &nbsp; &nbsp; &nbsp;   
    Type:
      <select name="tourType">
        <option value="" style="margin-left: 5px;padding-left: 20px;" <c:if test="${empty param.tourType or param.tourType == ''}">selected</c:if>>ALL</option>
        <option value="SPORT" style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-sport.gif);background-repeat: no-repeat; height: 20px;" <c:if test="${param.tourType == 'SPORT'}">selected</c:if>>SPORT</option>
        <option value="THEATER" style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-theater.gif);background-repeat: no-repeat; height: 20px;" <c:if test="${param.tourType == 'THEATER'}">selected</c:if>>THEATER</option>
        <option value="CONCERT" style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-concert.gif);background-repeat: no-repeat; height: 20px;" <c:if test="${param.tourType == 'CONCERT'}">selected</c:if>>CONCERT</option>
        <option value="OTHER" style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-other.gif);background-repeat: no-repeat; height: 20px;" <c:if test="${param.tourType == 'OTHER'}">selected</c:if>>OTHER</option>
      </select>

	<input type="submit" name="action" class="medButton" value="Search"/><br/>
	From:
	<select name="fromYear">
	  <c:forEach var="year" begin="${fromYear}" end="${eventYear + 5}" step="1">
	    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
	  </c:forEach>
	</select> /
	<select name="fromMonth">
	  <c:forEach var="month" begin="1" end="12" step="1">
	    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
	  </c:forEach>
	</select> /
	<select name="fromDay">
	  <c:forEach var="day" begin="1" end="31" step="1">
	    <option value="${day}" <c:if test="${day == fromDay}">selected</c:if>>${day}</option>
	  </c:forEach>
	</select>
	
	<input type="text" id="fromCalendar" class="calendar" style="visibility:hidden;width:0px;" />
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	To:
	<select name="toYear">
	  <c:forEach var="year" begin="${eventYear}" end="${eventYear + 5}" step="1">
	    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
	  </c:forEach>
	</select> /
	<select name="toMonth">
	  <c:forEach var="month" begin="1" end="12" step="1">
	    <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
	  </c:forEach>
	</select> /
	<select name="toDay">
	  <c:forEach var="day" begin="1" end="31" step="1">
	    <option value="${day}" <c:if test="${day == toDay}">selected</c:if>>${day}</option>
	  </c:forEach>
	</select>
	
	<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />
	
	 (YYYY/MM/DD)
	<br/>
	
	Show:
	<input type="checkbox" id="includeExistingEvents" name="includeExistingEvents" <c:if test="${includeExistingEvents}">checked</c:if>/><label for="includeExistingEvents">Existing Events</label> &nbsp;
	<input type="checkbox" id="includeTicketNetworkEvents" name="includeTicketNetworkEvents" <c:if test="${includeTicketNetworkEvents}">checked</c:if>/><label for="includeTicketNetworkDataFeedEvents"><img src="../images/ico-ticketnetwork.gif" align="absbottom"/> TicketNetwork Events</label> &nbsp;
	<input type="checkbox" id="includeTicketNetworkDataFeedEvents" name="includeTicketNetworkDataFeedEvents" <c:if test="${includeTicketNetworkDataFeedEvents}">checked</c:if>/><label for="includeTicketNetworkDataFeedEvents"><img src="../images/ico-ticketnetwork.gif" align="absbottom"/> TicketNetwork DataFeed Events</label> &nbsp;
	<input type="checkbox" id="includeTicketNetworkPOSEvents" name="includeTicketNetworkPOSEvents" <c:if test="${includeTicketNetworkPOSEvents}">checked</c:if>/><label for="includeTicketNetworkPOSEvents"><img src="../images/ico-ticketnetwork.gif" align="absbottom"/> TicketNetwork POS Events</label> &nbsp;
	<!-- <input type="checkbox" id="includeStubhubEvents" name="includeStubhubEvents" <c:if test="${includeStubhubEvents}">checked</c:if>/><label for="includeStubhubEvents"><img src="../images/ico-stubhub.gif" align="absbottom"/> StubHub Events (not in our system)</label> &nbsp;
	<input type="checkbox" id="includeStubhubDuplicatedEvents" name="includeStubhubDuplicatedEvents" <c:if test="${includeStubhubDuplicatedEvents}">checked</c:if>/><label for="includeStubhubDuplicatedEvents"><img src="../images/ico-stubhub.gif" align="absbottom"/> StubHub Duplicated Events</label>
	<input type="checkbox" id="includeSeatwaveEvents" name="includeSeatwaveEvents" <c:if test="${includeSeatwaveEvents}">checked</c:if>/><label for="includeSeatwaveEvents"><img src="../images/ico-seatwave.gif" align="absbottom"/> Seatwave Events</label><br/>
	 -->
	 Filter: <input type="checkbox" id="hideEventsWithACrawl" name="hideEventsWithACrawl" <c:if test="${hideEventsWithACrawl}">checked</c:if> />  <label for="hideEventsWithACrawl">Only show events with no crawls created yet</label>

</form>

<!--
Legends:
<span style="width:16px; height: 16px; border-style: solid; border-width: 1px; color: black; background:#888888">&nbsp;&nbsp;&nbsp;&nbsp;</span> Duplicated event &nbsp;
<img src="../images/ico-stubhub.gif" align="absbottom"/> StubHub Event (not in our system)
<img src="../images/ico-seatwave.gif" align="absbottom"/> Seatwave Event (not in our system)
-->
<form id="EditorSuperCrawlConfirmationForm" action="EditorSuperCrawlConfirmation" method="POST" onsubmit="return checkCrawlCreation()">
<br/>
<c:if test="${not empty includeStubhubEvents}">
	<input type="hidden" name="includeStubhubEventsStr" id="includeStubhubEventsStr" value="${includeStubhubEvents}"/>
</c:if>
<c:if test="${not empty includeExistingEvents}">
	<input type="hidden" name="includeExistingEventsStr" id="includeExistingEventsStr" value="${includeExistingEvents}"/>
</c:if>
<c:if test="${not empty includeSeatwaveEvents}">
	<input type="hidden" name="includeSeatwaveEventsStr" id="includeSeatwaveEventsStr" value="${includeSeatwaveEvents}"/>
</c:if>

<c:if test="${not empty includeTicketNetworkEvents}">
	<input type="hidden" name="includeTicketNetworkEventsStr" id="includeTicketNetworkEventsStr" value="${includeTicketNetworkEvents}"/>
</c:if>
<c:if test="${not empty includeTicketNetworkDataFeedEvents}">
	<input type="hidden" name="includeTicketNetworkDataFeedEventsStr" id="includeTicketNetworkDataFeedEventsStr" value="${includeTicketNetworkDataFeedEvents}"/>
</c:if>
<c:if test="${not empty includeTicketNetworkPOSEvents}">
	<input type="hidden" name="includeTicketNetworkPOSEventsStr" id="includeTicketNetworkPOSEventsStr" value="${includeTicketNetworkPOSEvents}"/>
</c:if>
<c:if test="${not empty eventHits}">
<!--<input type="checkbox" name="automated" id="automated" /> <label for="automated"> Use Exchange Tour Name </label> 
<br/>
-->
<br/>
  Select:
  <a href="#" onclick="$('.event_checkbox').attr('checked', true); return false;">All</a>
  | <a href="#" onclick="$('.event_checkbox').removeAttr('checked'); return false;">None</a><br/>
<br/>

	<display:table class="list" name="${eventHits}" pagesize="250" id="eventHit" requestURI="EditorSuperCrawlCreation" decorator="creationEventHitTableDecorator">
	  <display:column>
	   <span style="white-space:nowrap">
	    <input type="checkbox" class="event_checkbox" name="event-${eventHit.eventId}" id="event-${eventHit.eventId}" /><c:if test="${eventHit.siteId != null}"><a target="_blank" href="${eventHit.url}"><img tooltip="${eventHit.siteId} - ${eventHitOriginalId}" src="../images/ico-${eventHit.siteId}.gif"/></a></c:if>
	   </span>
	  </display:column>
	  <display:column title="">
	   	<img src="../images/ico-${fn:toLowerCase(eventHit.tourType)}.gif" align="absbottom" title="${eventHit.tourType}" />
	  </display:column>
	  <display:column sortable="true" title="Event Name">
	    <c:choose>
	      <c:when test="${eventHit.exists}">
	        <a target="_blank" href="BrowseTickets?eventId=${eventHit.eventId}">${eventHit.name}</a>
	      </c:when>
	      <c:otherwise>
	        ${eventHit.name}
	      </c:otherwise>
	    </c:choose>
	  </display:column>
	  <display:column sortable="true" title="Date">
	    <span style="white-space:nowrap">
	    <fmt:formatDate pattern="MMM dd, yyyy" value="${eventHit.date}" />&nbsp;
		  <c:choose>
			<c:when test="${not empty eventHit.time}">
			  <fmt:formatDate pattern="hh:mm aa" value="${eventHit.time}" />
			</c:when>
		    <c:otherwise>
			  TBD
			</c:otherwise>
		  </c:choose>
		</span>
	  </display:column>
	  <display:column sortable="true" title="Artist Name">
	    ${eventHit.artistName}
	  </display:column>
	  <display:column title="<input type='checkbox' name='copyVenueName' id='copyVenueName' onclick='copyAllVenue()'><br/>Venue">
	  
	  	<select name="selectedVenue-${eventHit.eventId}" id="selectedVenue-${eventHit.eventId}" class="venue" style="width:300px" onchange="getVenueCategory(this);">
	  	<option value="0" >Select Venue</option>
	  		<c:forEach items="${venues}" var="venue">
	  			<option value="${eventHit.eventId}-${venue.id}" id="${eventHit.eventId}-${venue.id}" <c:if test="${venue.id == eventHit.venueId}">selected</c:if>>${venue.building}</option>
		    </c:forEach>
	    </select>
	  </display:column>
	  <display:column title="<input type='checkbox' name='copyVenueCategory' id='copyVenueCategory' onclick='callAllVenueCate()'><br/>VenueCategory">
	  	<select name="selectedVenueCategory-${eventHit.eventId}" id="selectedVenueCategory-${eventHit.eventId}" class="venueCategory" style="width:150px" 
	  		onchange = "selectEvent(${eventHit.eventId});callAllVenueCate();">
	  	<option value="0" >--Select--</option>
	  		<c:forEach items="${eventHit.venueCategoryList}" var="venueCategory">
	  			<option value="${venueCategory.categoryGroup}" <c:if test="${fn:length(eventHit.venueCategoryList) eq 1}">selected </c:if>>${venueCategory.categoryGroup}</option>
		    </c:forEach>
	    </select>
	  </display:column>
	 
	  <display:column sortable="true" title="Exchange Location">
	    ${eventHit.building}, ${eventHit.city}, ${eventHit.state}, ${eventHit.country}
	  </display:column>
	  <display:column title="Existing Crawls" sortable="true" sortProperty="siteCount" >
	    <c:forEach var="site" items="${eventHit.sites}">
	      <img src="../images/ico-${site}.gif" tooltip="${site}" align="absbottom"/>
	    </c:forEach>
	  </display:column>
	</display:table>
	<br/><br/>
  <div style="clear:both"></div>
	
	<span id="pleaseWaitSpan" style="display:none"><img src="../images/process-running.gif" align="absbottom" /> Crawling... Please wait</span>
	<input type="submit" class="medButton" id="submitButton" name="action" value="Create Events&gt;&gt;"/> <i>(You will be prompted at the next page to confirm)</i>	
	<input id="linkEventsButton" type="button" class="medButton" onclick="linkToAdmitone()"  value="&gt;&gt;Link Events"/>
</c:if>

</form>

<script type="text/javascript">
	
	function getVenueCategory(obj){
		var venueId = $(obj).val();
		var eventId = $(obj).attr("id")
		selectEvent(eventId.split('-')[1]);
		var temp = venueId.split("-");
		$.ajax({
			url:"GetVenueCategoryByVenue?venueId="+temp[1],
			success:function(result){
				var cats = result.split(",")
				var options = "<option value ='0'>--Select--</option>";
				var selected ="";
				if(cats.length-1==1){
					selected="selected";
				}
				for(i=0;i<cats.length-1;i++){
					var val = cats[i].split("-");
					options = options + "<option value ='" + val[0]+ "'" + selected + ">" + val[0] + "</option>";
				}
				$("#selectedVenueCategory-"+temp[0]).html(options);
			}
		});
		$('#copyVenueCategory').attr('checked',false);
		$('#copyVenueName').attr('checked',false);
	}
	function checkAllSites() {
		$.each($('.site-checkbox'), function(intIndex, input) {
		  if (!$(input).attr('disabled')) {
		  	$(input).attr('checked', 'true').change();
		  }
		});
	}

	function uncheckAllSites() {
		$.each($('.site-checkbox'), function(intIndex, input) {
		  if (!$(input).attr('disabled')) {
		  	$(input).removeAttr('checked').change();
		  }
		});
		return false;
  	}

	function checkCrawlCreation() {
	  if ($('input.event_checkbox:checked').length == 0) {
	    alert("Please select at least one event");
	    return false;
	  }
	  
	  $.each($('input.site-checkbox:checked'), function(intIndex, obj) {
	    var site = $(obj).attr('name').split('-')[1];
	    var hiddenInput = $.INPUT({name: 'site-' + site, 'type':'hidden', 'value':'on'});
	    $('#EditorSuperCrawlConfirmationForm').append(hiddenInput);  
	  });
	  
	  $('#pleaseWaitSpan').show();$('#submitButton').hide()
	  return true;
	}

	  function linkToAdmitone() {
		  if ($('input.event_checkbox:checked').length == 0) {
			    alert("Please select at least one event");
			    return false;
			  }
			  	 
	    var params = "";
	    
	    $.each($('input.event_checkbox:checked'), function(intIndex, obj) {
	    	var eventId = $(obj).attr('name').split('-')[1];
	    	if(eventId != ''){
	        	params += eventId + ',';
	    	}
	    });
	    
	    if (params.length > 0) {
	      params = params.substring(0, params.length - 1);
	    }
		//alert("EditorLinkAdmitone?eventId=" + params);
		location.href = 'EditorLinkAdmitone?eventId=' + params;
		return false;
	  }

	  function copyAllTour() { 
		if(!$('#copyTourName').attr('checked')){
			return false;
		}
		var i=0;
		var isFirst=true;
		var numItems = $('.tour').length
		var tourIndex='';
		
		$('.tour').each(function(index) {
			if(!isFirst){
				$('.tour').removeAttr("selected");
				var temp = ' option[index=' + tourIndex +']';
				$('.tour').find(temp).attr("selected","selected");
				return false;
			}
			isFirst=false;
			tourIndex= $(this).attr("selectedIndex");
		});
	}
	function copyAllVenue() { 
		if(!$('#copyVenueName').attr('checked')){
			$('.event_checkbox').attr('checked',false);
			return false;
		}
		var i=0;
		var isFirst=true;
		var numItems = $('.venue').length
		var venueIndex='';
		
		$('.venue').each(function(index) {
			if(!isFirst){
				$('.venue').removeAttr("selected");
				var temp = ' option[index=' + venueIndex +']';
				$('.venue').find(temp).attr("selected","selected");
				return false;
			}
			isFirst=false;
			venueIndex= $(this).attr("selectedIndex");
		});
		$('.event_checkbox').attr('checked',true);
		copyAllVenueCategory();
	  }

	
	function callAllVenueCate(){
		if($('#copyVenueCategory').attr('checked')){			
		
			var isFirst=true;
			var options = "";
			var selected ='';
			$('.venueCategory').each(function() {
				if(isFirst){
					$("#" + $(this).attr('id') +" option").each(function(){
						if($(this).attr('selected')){
							selected = "selected";
						}else{
							selected = "";
						}
						options += '<option value = ' + $(this).val() + ' ' + selected +'>' + $(this).text() + '</option>';
					});
					isFirst=false;
				}else{					
					$(this).html(options);
				}
			});
			$('#copyVenueName').attr('checked',true);
			$('.event_checkbox').attr('checked',true);
			copyAllVenue();
		}else{
			$('.event_checkbox').attr('checked',false);
		}
	}
	
	
	  function copyAllVenueCategory(){
		var isFirst=true;
		var options = "";
		var selected ='';
		$('.venueCategory').each(function() {
			if(isFirst){
				$("#" + $(this).attr('id') +" option").each(function(){
					if($(this).attr('selected')){
						selected = "selected";
					}else{
						selected = "";
					}
					options += '<option value = ' + $(this).val() + ' ' + selected +'>' + $(this).text() + '</option>';
				});
				isFirst=false;
			}else{
				$(this).html(options);
			}
		});
	  }
	  function selectEvent(val){
		var eventId = val.split('-')[0];
		$('#event-' + eventId).attr('checked','checked');
	  }
</script>