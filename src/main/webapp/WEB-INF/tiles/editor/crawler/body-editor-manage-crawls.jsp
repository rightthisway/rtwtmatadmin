<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Manage Crawls 
</div>

<!-- form used to submit list of crawl ids using POST -->
<form id="crawlForm" action="EditorManageCrawls" method="post">
	<input type="hidden" id="crawlFormAction" name="crawlFormAction" />
	<input type="hidden" id="crawlFormIds" name="crawlFormIds" />
	<!-- <input type="hidden" id="checkCrawlFormFrequency" name="checkCrawlFormFrequency" /> -->
	<input type="hidden" id="checkAutoCrawlFrequencyStatus" name="checkAutoCrawlFrequencyStatus" />
	<input type="hidden" id="crawlCrawlFrequency" name="crawlCrawlFrequency" />
	<!-- <input type="hidden" id="crawlFormStatus" name="crawlFormStatus" /> -->
	<input type="hidden" id="artistId" name="artistId" value="${editorCrawlerArtistId}" />
	<input type="hidden" id="venueId" name="venueId" value="${editorCrawlerVenueId }"/>
	<input type="hidden" id="eventId" name="eventId" value="${editorCrawlerEventId}" />
</form>

<script type="text/javascript">
	
$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val(row[1]);
		changeFilterArtist(row[0]);
	});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#tempArtistId').val('');
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#tempVenueId').val('');
}

$(document).ready(function(){
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		changeFilterVenue(row[0]);
	});
});
	
function changeFilterArtist(artistId) {
	var showDisabledCrawls = $('#showDisabledCrawls').is(':checked');
	var showOnlyPresaleCrawls = $('#showOnlyPresaleCrawls').is(':checked');
	if(${editorCrawlerVenueId!=null}){
		document.location.href = "EditorManageCrawls?artistId=" + artistId +"&venueId="+"${editorCrawlerVenueId}"+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
	}else{    
		document.location.href = "EditorManageCrawls?artistId=" + artistId+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
	}
};
function changeFilterVenue(venueId) {
	var showDisabledCrawls = $('#showDisabledCrawls').is(':checked');
	var showOnlyPresaleCrawls = $('#showOnlyPresaleCrawls').is(':checked');
	if(${editorCrawlerArtistId!=null}){
		document.location.href = "EditorManageCrawls?venueId=" + venueId +"&artistId="+"${editorCrawlerArtistId}"+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
	}else{    
		document.location.href = "EditorManageCrawls?venueId=" + venueId+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
	}
};
function changeFilterEvent(eventId) {
	var showDisabledCrawls = $('#showDisabledCrawls').is(':checked');
	var showOnlyPresaleCrawls = $('#showOnlyPresaleCrawls').is(':checked');
	var artistId = $('#artistId').val();
	var venueId = $('#venueId').val();
	//document.location.href = "EditorManageCrawls?eventId="+eventId+"&artistId="+artistId;
	document.location.href = "EditorManageCrawls?artistId="+artistId+"&eventId="+eventId+"&venueId="+venueId+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
};

function changeFilterSiteId(siteId) {
	var showDisabledCrawls = $('#showDisabledCrawls').is(':checked');
	var showOnlyPresaleCrawls = $('#showOnlyPresaleCrawls').is(':checked');
	var artistId = $('#artistId').val();
	var eventId = $('#eventId').val();
	var venueId = $('#venueId').val();
	//document.location.href = "EditorManageCrawls?siteId=" + siteId+"&eventId="+eventId+"&artistId="+artistId;
	document.location.href = "EditorManageCrawls?artistId="+artistId+"&eventId="+eventId+"&siteId=" + siteId+"&venueId="+venueId;+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
};

function changeShowDisabledCrawls(showDisabledCrawls) {
	var artistId = $('#artistId').val();
	var eventId = $('#eventId').val();
	var siteId = $('#siteId').val();
	var venueId = $('#venueId').val();
	var showOnlyPresaleCrawls =  $('#showOnlyPresaleCrawls').is(':checked');
	//document.location.href = "EditorManageCrawls?showDisabledCrawls=" + (showDisabledCrawls?"on":"off")+"&siteId=" + siteId+"&eventId="+eventId+"&artistId="+artistId;
	document.location.href = "EditorManageCrawls?artistId="+artistId+"&venueId="+venueId+"&eventId="+eventId+"&siteId=" + 
	siteId+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
};

function changeShowOnlyPresaleCrawls(showOnlyPresaleCrawls) {
	var artistId = $('#artistId').val();
	var eventId = $('#eventId').val();
	var siteId = $('#siteId').val();
	var venueId = $('#venueId').val();
	var showDisabledCrawls = $('#showDisabledCrawls').is(':checked');
	//document.location.href = "EditorManageCrawls?showDisabledCrawls=" + (showDisabledCrawls?"on":"off")+"&siteId=" + siteId+"&eventId="+eventId+"&artistId="+artistId;
	document.location.href = "EditorManageCrawls?artistId="+artistId+"&venueId="+venueId+"&eventId="+eventId+"&siteId=" + 
	siteId+"&showOnlyPresaleCrawls=" + (showOnlyPresaleCrawls?"on":"off")+"&showDisabledCrawls=" + (showDisabledCrawls?"on":"off");
};

function __getSelectedTicketListingCrawlIds() {
	var ids = "";
	for (var crawlId in __selectedCrawls) {
		ids += crawlId + ",";
	}
	
	if (ids == "") {
		return null;
	}
	
	// remove the final comma
	ids = ids.substring(0, ids.length - 1)
	return ids;		
};	

function selectAllTicketListingCrawls() {
	for(var i = 0 ; i < crawls.length ; i++) {
		__selectedCrawls[crawls[i][CRAWL_ID_INDEX]] = true;
	}
	$('.checkbox-crawl').attr('src', '../images/checkbox-ticked.gif');
};

function unselectAllTicketListingCrawls() {
	__selectedCrawls = {};
	$('.checkbox-crawl').attr('src', '../images/checkbox-empty.gif');
};

function enableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	
	$('#crawlFormAction').val('enableCrawl');
	$('#crawlFormIds').val(ids);
	$('#crawlForm').submit();
	// document.location.href="EditorManageCrawls?action=enableCrawl&crawlIds=" + ids;
};

function setFrequencyToSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	var auto = false;
	var autoCrawlFrequency= $('input[name=autoCrawlFrequency]:checked').val();
	if(autoCrawlFrequency=='true'){
		$('#checkAutoCrawlFrequencyStatus').val('true');  
		$('#selectFrequency').val('');
	}else if(autoCrawlFrequency=='false'){
		var selectedFrequency= $('#selectFrequency').val();
		$('#crawlCrawlFrequency').val(selectedFrequency);
		
	}else{
		alert('Please select frequency.');
		return;
	}
	$('#crawlFormAction').val('updateCrawl');	
	/* if($('#applyCrawlFrequencyToAll').is(':checked')) {	
		$('#checkCrawlFormFrequency').val('true');    
	}else{
		$('#checkCrawlFormFrequency').val('false'); 
	}
	if($('#applyACRToAll').is(':checked')) {
		   
	}else{
		$('#checkCrawlFormStatus').val('false');  
	} */	
	$('#crawlFormIds').val(ids);
	$('#crawlForm').submit();
	
};

function disableSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	$('#crawlFormAction').val('disableCrawl');
	$('#crawlFormIds').val(ids);
	$('#crawlForm').submit();
	// document.location.href="EditorManageCrawls?action=disableCrawl&crawlIds=" + ids;
};

function disableExpiredTicketListingCrawls() {
	document.location.href="EditorManageCrawls?action=disableExpiredCrawl";
};

function forceRecrawlSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	$('#crawlFormAction').val('forceRecrawl');
	$('#crawlFormIds').val(ids);
	$('#crawlForm').submit();
	// document.location.href = "EditorManageCrawls?action=forceRecrawl&crawlIds=" + ids;
};

function deleteSelectedTicketListingCrawls() {
	var ids = __getSelectedTicketListingCrawlIds();
	if (ids == null) {
		alert("No crawl selected");
		return;
	}
	
	var idCount = ids.split(",").length;
	var r = confirm("Do you really want to delete the " + idCount  + " selected crawl(s)?\nAll the tickets associated to these crawls will be deleted as well.");
	if (r) {
		$('#crawlFormAction').val('removeCrawl');
		$('#crawlFormIds').val(ids);
		$('#crawlForm').submit();
		// document.location.href = "EditorManageCrawls?action=removeCrawl&crawlIds=" + ids;
	}
};

function changeAddCrawler(siteId) {
	if (siteId) {
		document.location.href = "EditorEditTicketListingCrawl?action=create&siteId=" + siteId;
	}
};

</script>

<h1>Crawls</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>
<div align="center">
<table>
	<tr>
		<td colspan="3">
			<b>Filter</b> with <b>AND</b> combination:			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>Artist:</b>			
		</td>
		<td>
			<span id="artistLabelSpanId"  <c:if test="${not empty editorCrawlerArtistId && editorCrawlerArtistId != 0 }">style="display: black;"</c:if> 
					<c:if test="${empty editorCrawlerArtistId || editorCrawlerArtistId == 0 }">style="display: none;"</c:if>  >
					<span id="artistNameDiv" >
						<c:choose>
							<c:when test="${not empty editorCrawlerArtistId && editorCrawlerArtistId != 0}">
								${artistName}
							</c:when>
							<c:otherwise>
									-
							</c:otherwise>
						
						</c:choose>
					</span>	
						<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
					</span>
					
					
					<span id="artistSelectSpanId" <c:if test="${not empty editorCrawlerArtistId && editorCrawlerArtistId != 0 }">style="display: none;"</c:if> 
						<c:if test="${empty editorCrawlerArtistId || editorCrawlerArtistId == 0 }">style="display: black;"</c:if> >
					
						<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>
		</td>
	</tr>
	<tr>
		<td colspan="2"><b>Venue</b></td>
		<td>
			<c:choose>
				<c:when test="${(empty editorCrawlerArtistId || editorCrawlerArtistId == 0) && empty venues}">
					
					<span id="venueLabelSpanId"  <c:if test="${not empty editorCrawlerVenueId && editorCrawlerVenueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty editorCrawlerVenueId || editorCrawlerVenueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName }">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty editorCrawlerVenueId && editorCrawlerVenueId != 0 }">style="display: none;"</c:if> 
				<c:if test="${empty editorCrawlerVenueId || editorCrawlerVenueId == 0 }">style="display: block;"</c:if> >
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
				
				</c:when>
				<c:otherwise>
					<select id="venueId" name="venueId" onchange="changeFilterVenue(this.value);">
						<option value="">Select</option>
						<c:forEach var="venue" items="${venues}">
							<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == editorCrawlerVenueId}">selected</c:if>>${venue.building}</option>
						</c:forEach>
					</select>
				</c:otherwise>
			</c:choose>	
		</td>
	</tr>
	<c:if test="${(editorCrawlerArtistId != null and editorCrawlerArtistId != 0) or (editorCrawlerVenueId !=null and editorCrawlerVenueId !=0)}">
	<tr>
		<td colspan="2">
			<b>Event:</b>		
		</td>
		<td>
			<select id="eventId" name="eventId" onchange="changeFilterEvent(this.value);">
				<option value="">All</option>
				<c:forEach var="event" items="${events}">
					<option style="height: 20px;" value="${event.id}" <c:if test="${event.id == editorCrawlerEventId}">selected</c:if>>
						${event.name} - <fmt:formatDate value="${event.date}" pattern="MM/dd/yyyy" /> 
						<c:choose><c:when test="${event.time == null}"> TBD </c:when>
							<c:otherwise><fmt:formatDate pattern="hh:mm aa" value="${event.time}" /></c:otherwise>
						</c:choose>	- ${event.formattedVenueDescription}
					</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2"><b>Site:</b></td>
		<td> 
			<select id="siteId" name="siteId" onchange="changeFilterSiteId(this.value);">
				<option value="">All</option>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}" <c:if test="${siteId == editorCrawlerSiteId}">selected</c:if>>${siteId}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	
	</c:if>
</table>
<div style="clear: both;"></div>

<div style="line-height: 20px;float: left">
	<b>Show:</b>
	<input type="checkbox" id="showDisabledCrawls" name="showDisabledCrawls" onchange="changeShowDisabledCrawls(this.checked)" 
		<c:if test="${editorCrawlerShowDisabledCrawls == 'on'}">checked</c:if>	
	/> <label for="showDisabledCrawls">Disabled Crawls</label>
	<input type="checkbox" id="showOnlyPresaleCrawls" name="showOnlyPresaleCrawls" onchange="changeShowOnlyPresaleCrawls(this.checked)" 
		<c:if test="${editorCrawlerShowOnlyPresaleCrawls == 'on'}">checked</c:if>	
	/> <label for="onlyPresaleCrawls">Only Presale Crawls</label>
</div>
<div style="clear: both;"></div>

<div align="left">

<b>Status:</b>
<c:choose>
	<c:when test="${statusTime == 'ALL'}"><b>ALL</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=ALL">ALL</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == 'ERROR'}"><b>ERROR</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=ERROR">ERROR</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == '1mn'}"><b>&lt; 1 minute</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=1mn">&lt; 1 minute</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == '30mn'}"><b>1 - 30 minutes</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=30mn">1 - 30 minutes</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == '1hr'}"><b>30 - 60 minutes</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=1hr">30 - 60 minutes</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == '2hr'}"><b>1 - 2 hours</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=2hr">1 - 2 hours</a></c:otherwise>
</c:choose>
-
<c:choose>
	<c:when test="${statusTime == '2+hr'}"><b>&gt; 2+ hours</b></c:when>
	<c:otherwise><a href="EditorManageCrawls?statusTime=2%2Bhr">&gt; 2+ hours</a></c:otherwise>
</c:choose>
 </div>
<br /><br />
  
<div align="left" style="border-top: 1px dashed black;border-bottom: 1px dashed black;padding-top: 4px; padding-bottom: 4px;">
	<b>Number of enabled crawls:</b> <fmt:formatNumber value="${totalTicketListingCrawlCount - disabledTicketListingCrawlCount}" /><br />
	<b>Number of disabled crawls:</b> <fmt:formatNumber value="${disabledTicketListingCrawlCount}" /><br />
	<b>Number of failed crawls:</b> <fmt:formatNumber value="${failedTicketListingCrawlCount}" /><br />
	<b>Number of ready crawls:</b> <fmt:formatNumber value="${readyTicketListingCrawlCount}" /><br />
	<br />
	<b>Number of displayed crawls:</b> <fmt:formatNumber value="${fn:length(ticketListingCrawls)}" /><br />
</div>


</div>
<div style="margin-top:16px;">
	
	<a href="javascript:void(0)" onclick="disableExpiredTicketListingCrawls()">Disable Expired Crawls</a>
	<br /><br />
	<div style="float: left"><b>Legends:</b></div>

	<div style="margin-left:20px;width:10px;height:10px;background-color: #EB9E6F;border: 2px solid #EB9E6F;margin-right: 6px;float: left"></div>  
	<div style="float: left">Disabled crawl</div>

	<div style="margin-left:20px;width:10px;height:10px;background: #748C20;border: 2px solid #748C20;margin-right: 6px;float: left"></div>  
	<div style="float: left">Selected crawl</div>

	<div style="margin-left:20px;width:10px;height:10px;background: #666666;border: 2px solid #666666;margin-right: 6px;float: left"></div>  
	<div style="float: left">Broken crawl</div>

	<div style="clear:both"></div>
</div>

<br />

<table>
	<tr>
		<td>
			<select class="addCrawlSelector" onchange="changeAddCrawler($(this).val())">
				<option selected>-- Create new crawl --</option>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<c:set var="safeSiteId" value="[${siteId}]" />
				
				  <c:choose>
				      <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
					    <option style="text-decoration:line-through;margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}" disabled>${siteId}</option>
				      </c:when>
				      <c:otherwise>
					    <option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}">${siteId}</option>
				      </c:otherwise>
				  </c:choose>
				</c:forEach>
			</select>
		</td>
		<c:if test="${not empty ticketListingCrawls}">
			<td width="30">&nbsp;</td>
			<td>
				<b>Select: </b>
				<a href="javascript:void(0)" onclick="selectAllTicketListingCrawls()">All</a> -
				<a href="javascript:void(0)" onclick="unselectAllTicketListingCrawls()">None</a>			
			</td>
			<td width="30">&nbsp;</td>
			<td>
				<b>Action: </b>
				<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedTicketListingCrawls()">Enable</a> -
				<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedTicketListingCrawls()">Disable</a> -
				<img src="../images/ico-reload.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="forceRecrawlSelectedTicketListingCrawls()">Force Recrawl</a> 
				<!-- <img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedTicketListingCrawls()">Delete</a> -->			
			</td>
		</c:if>
	</tr>
	<tr><tr><td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>
		<table style="border: 1px solid #000;">
			<tr>
				<td>
					<b>Automatic Crawl Frequency :</b>
				</td>
				<td colspan="2">
					<input type="radio" name="autoCrawlFrequency" value="true"  onclick="$('#trCrawlFrequency').hide();"/> True
					<input type="radio" name="autoCrawlFrequency" value="false" onclick="$('#trCrawlFrequency').show();"/> False
				</td>
			</tr>
			<tr id="trCrawlFrequency">
				<td colspan="2" align="right">
					Crawl Frequency
				</td>
				<td >
		  			<select name="selectFrequency" id="selectFrequency">
						<!-- <option value="300">5mn</option>
					  	<option value="600">10mn</option>
					  	<option value="900">15mn</option>
					  	<option value="1800">30mn</option> -->
					  	<option value="3600">1h</option>
					  	<option value="7200">2h</option>
					  	<option value="10800">3h</option>
					  	<option value="14400">4h</option>
					  	<option value="18000">5h</option>
					  	<option value="21600">6h</option>
					  	<option value="25200">7h</option>
					  	<option value="28800">8h</option>
					  	<option value="32400">9h</option>
					  	<option value="36000">10h</option>
					  	<option value="39600">11h</option>
					  	<option value="43200">12h</option>
					  	<option value="86400">1d</option>
					  	<option value="172800">2d</option>
					  	<option value="1576800000">Idle</option>
					</select>
					
				</td>
			</tr>
			<tr >
				<td colspan="3" align="center"><input id="bulkcrawl" type="button" onclick="setFrequencyToSelectedTicketListingCrawls()" class="medButton" value="Update"></td>
			</tr>
		</table>
	</td>
	</tr>
</table>

<div id="crawl-none" style="display:none">No crawls to display</div>
<div id="crawl-grid" style="height:420;width:600"></div>

<c:if test="${not empty ticketListingCrawls and fn:length(ticketListingCrawls) > 5}">
	<table id="crawlBottomBar" style="display:none">
		<tr>
			<td>
				<select class="addCrawlSelector" onchange="changeAddCrawler($(this).val())">
					<option selected>-- Create new crawl --</option>
					<c:forEach var="siteId" items="${constants.siteIds}">
						<option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}">${siteId}</option>
					</c:forEach>
				</select>
			</td>
			<td width="30">&nbsp;</td>
			<td>
				<b>Select: </b>
				<a href="javascript:void(0)" onclick="selectAllTicketListingCrawls()">All</a> -
				<a href="javascript:void(0)" onclick="unselectAllTicketListingCrawls()">None</a>			
			</td>
			<td width="30">&nbsp;</td>
			<td>
				<b>Action: </b>
				<img src="../images/ico-enable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="enableSelectedTicketListingCrawls()">Enable</a> -
				<img src="../images/ico-disable.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="disableSelectedTicketListingCrawls()">Disable</a> -
				<img src="../images/ico-reload.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="forceRecrawlSelectedTicketListingCrawls()">Force Recrawl</a> 
				<!-- <img src="../images/ico-delete.gif" align="absbottom" /> <a href="javascript:void(0)" onclick="deleteSelectedTicketListingCrawls()">Delete</a>	 -->		
			</td>
		</tr>
	</table>
	
	<div style="clear: both"></div>

</c:if>

<jsp:directive.page import="com.admitone.tmat.utils.TextUtil" />
<jsp:directive.page import="com.admitone.tmat.web.pojo.WebTicketListingCrawlRow" />
<jsp:directive.page import="com.admitone.tmat.data.Artist" />
<jsp:directive.page import="org.apache.commons.lang.StringEscapeUtils" />

<script language="javascript">
var crawls = [
<c:forEach var="crawl" items="${ticketListingCrawls}"><%
	WebTicketListingCrawlRow crawl = (WebTicketListingCrawlRow)pageContext.getAttribute("crawl");
	
	String name = crawl.getName();
	if (name != null) {
		name = StringEscapeUtils.escapeJavaScript(TextUtil.removeExtraWhitespaces(name).trim());
	}
	
	String artistName = null;
	/* Tour crawlTour = crawl.getTour();
	if (crawlTour != null) { */
		artistName = crawl.getEvent().getArtist().getName();
		if (artistName != null) {
			artistName = StringEscapeUtils.escapeJavaScript(TextUtil.removeExtraWhitespaces(artistName).trim());
		}
	/* }  */
	
	String errorMessage = crawl.getErrorMessage();
	if (errorMessage != null) {
		errorMessage = StringEscapeUtils.escapeJavaScript(TextUtil.removeExtraWhitespaces(errorMessage).trim());
	}
	
	String formattedVenueDescription = null;
	if (crawl.getEvent() != null && crawl.getEvent().getFormattedVenueDescription() != null) {
		formattedVenueDescription = StringEscapeUtils.escapeJavaScript(TextUtil.removeExtraWhitespaces(crawl.getEvent().getFormattedVenueDescription().trim()));
	}
	
	pageContext.setAttribute("broken", crawl.isBroken()?"true":"false"); 
	pageContext.setAttribute("crawlName", name); 
	pageContext.setAttribute("artistName", artistName);
	pageContext.setAttribute("errorMessage", errorMessage);
	pageContext.setAttribute("formattedVenueDescription", formattedVenueDescription);

	String startCrawlTime = "null";
	if (crawl.getStartCrawl() != null) {
		startCrawlTime = "" + crawl.getStartCrawl().getTime();
	}
	pageContext.setAttribute("startCrawlTime", startCrawlTime);

	String endCrawlTime = "null";
	if (crawl.getEndCrawl() != null) {
		endCrawlTime = "" + crawl.getEndCrawl().getTime();
	}
	pageContext.setAttribute("endCrawlTime", endCrawlTime);

	// id, name, siteId, crawlState, crawlState, startCrawl, endCrawl, nextCrawlWaitingTime, errorMessage, artistId, artistName, eventId, eventDateDescription, eventVenueDescription, crawlFrequency , automaticCrawlFrequency
%>[${crawl.id},'${crawlName}','${crawl.siteId}','${crawl.crawlState}',${crawl.enabled},${startCrawlTime},${endCrawlTime},${crawl.nextCrawlWaitingTime},'${errorMessage}', ${crawl.event.artistId}, '${artistName}',${broken},<c:choose><c:when test="${not empty crawl.event}">${crawl.event.id},${crawl.event.localDate.time}, '${crawl.event.formattedEventDate}','${formattedVenueDescription}'</c:when><c:otherwise>null,null</c:otherwise></c:choose>,${crawl.crawlFrequency},${crawl.automaticCrawlFrequency}],
</c:forEach>
null
];

// remove last element which is null
crawls = crawls.slice(0, crawls.length - 1);

// INDEX contants
var CRAWL_ID_INDEX = 0;
var CRAWL_NAME_INDEX = 1;
var SITE_ID_INDEX = 2;
var CRAWL_STATE_INDEX = 3;
var CRAWL_ENABLED_INDEX = 4;
var START_CRAWL_INDEX = 5;
var END_CRAWL_INDEX = 6;
var NEXT_CRAWL_WAITING_TIME_INDEX = 7;
var ERROR_MESSAGE_INDEX = 8;
var ARTIST_ID_INDEX = 9;
var ARTIST_NAME_INDEX = 10;
var BROKEN_INDEX = 11;
var EVENT_ID_INDEX = 12;
var EVENT_DATE_INDEX = 13;
var EVENT_FORMATTED_DATE_TIME_INDEX = 14;
var EVENT_VENUE_DESCRIPTION = 15;

// fake index
var CRAWL_SHORT_ERROR_MESSAGE_INDEX = 21;
var CRAWL_FREQUENCY_INDEX = 16;
var AUTOMATIC_CRAWL_FREQUENCY_INDEX = 17;

var ticketListingCrawlerRunning = 'false';//${ticketListingCrawler.running};
var editorCrawlerView = "${editorCrawlerView}";

var __currentSortField = CRAWL_NAME_INDEX;
var __currentSortDesc = false;

var __selectedCrawls = {};

// there are some handlers associated when exiting the page that make the page slow, so remove them.
$(window).unbind("unload");

$(document).ready(function() {
	$('#crawlTableContainer').empty();
	$('#trCrawlFrequency').hide();
	$.each($('.addCrawlSelector'), function(i, elt) {
		$(elt).children().get(0).selected = true;
	});
	
	if (crawls.length == 0) {
		$('#crawl-none').show();
		$('#crawl-grid').hide();
		return;
	}
});

function getCrawlShortMessage(crawl) {
	// display in this order:
	// ERROR (crawlState == ERROR && errorMessage != "No tickets found")
	// NO TICKETS  (crawlState == ERROR && errorMessage == "No tickets found") 
	// REGULAR CRAWL (craw)
	if (crawl[CRAWL_STATE_INDEX] == 'ERROR') {
		return (crawl[ERROR_MESSAGE_INDEX] == 'No tickets found')?"NO TICKETS":"ERROR";
	} else if (crawl[CRAWL_ENABLED_INDEX]) {
		return "RUNNING";
	} else {
		return "STOPPED";
	}
}

Ext.onReady(function(){
	var store = new Ext.data.Store({
        reader: new Ext.data.ArrayReader({}, [
			{name: 'crawlId', type: 'int'},
			{name: 'crawlName', type: 'string'},
			{name: 'siteId', type: 'string'},
			{name: 'crawlState', type: 'string'},
			{name: 'crawlEnabled', type: 'boolean'},
			{name: 'startCrawl', type: 'int'},
			{name: 'endCrawl', type: 'int'},
			{name: 'nextCrawlWaitingTime', type: 'int'},
			{name: 'errorMessage', type: 'string'},
			{name: 'artistId', type: 'int'},
			{name: 'artistName', type: 'string'},
			{name: 'broken', type: 'boolean'},
			{name: 'eventId', type: 'int'},
			{name: 'eventDate', type: 'int'},
			{name: 'eventFormattedDateTime', type: 'string'},
			{name: 'eventVenueDescription', type: 'string'},
			{name: 'crawlFrequency', type: 'int'},
			{name: 'automaticCrawlFrequency', type:'boolean'}
        ])
	});

    // manually load local data
    store.loadData(crawls);

    var myView = new Ext.ux.grid.BufferView({
		    rowHeight: 36,
		    scrollDelay: 50,
		    getRowClass : function(record, index){
				if (record.data.broken) {
					return "brokenRow";
				} else if (!record.data.crawlEnabled) {
					return "disabledRow"; 
				} else {
					return "";
				}
        	}
    });	
    
	var grid = new Ext.ux.grid.livegrid.GridPanel({
			stateful: true,
			stateId: 'objectGrid',
			stateEvents: ['columnresize', 'show', 'hide', 'columnmove'],
			enableDragDrop : false,
	        width:975,
			height:400,
			frame: false,
			store: store,
			disableSelection: true,
	        cm : new Ext.grid.ColumnModel([{
				id: 'tBox',
				header: "X",
				width: 24,
				align: 'left',
				renderer: checkboxRenderer,
	            sortable: false
			},{
				id: 'name',
            	header: "Name",
            	width: 260,
            	align: 'left',
				renderer: crawlNameRenderer,
            	sortable: true,
            	resizable: true,
				dataIndex: 'crawlName'
			},{
				id: 'status',
            	header: "Status",
            	width: 100,
            	align: 'left',
				renderer: crawlStatusRenderer,
            	sortable: false,
            	resizable: true
			},{
				id: 'frequency',
            	header: "Freq.",
            	width: 60,
            	align: 'left',
				renderer: crawlFrequencyRenderer,
            	sortable: true,
			dataIndex: 'crawlFrequency',
            	resizable: false
			},{
				id: 'AutoFrequency',
            	header: "AutoFreq",
            	width: 60,
            	align: 'left',
            	sortable: true,
			dataIndex: 'automaticCrawlFrequency',
            	resizable: false
			} ,{
				id: 'artist',
            	header: "Artist/Team",
            	width: 170,
            	align: 'left',
				renderer: artistNameRenderer,
			dataIndex: 'artistName',
            	sortable: true,
            	resizable: false
			} ,{
				id: 'event',
            	header: "Event",
            	width: 80,
            	align: 'left',
				renderer: eventNameRenderer,
            	sortable: true,
			dataIndex: 'eventDate',
            	resizable: false
			},{
				id: 'action',
            	header: "",
            	width: 200,
            	align: 'left',
				renderer: actionRenderer,
            	sortable: false,
            	resizable: false
        	}]),
			loadMask: {
				msg : 'Loading...'
        	},
			view: myView
	});
	
	
	grid.render('crawl-grid');
});

function checkboxRenderer(value, cell, record) {
	if (__selectedCrawls[record.data.crawlId]) {
		return "<img id='checkbox-crawl-" + record.data.crawlId + "' class='checkbox-crawl' src='../images/checkbox-ticked.gif' onclick='toggleCheckbox(" + record.data.crawlId + ")' />";
	} else {
		return "<img id='checkbox-crawl-" + record.data.crawlId + "' class='checkbox-crawl' src='../images/checkbox-empty.gif' onclick='toggleCheckbox(" + record.data.crawlId + ")' />";
	}
};

function toggleCheckbox(crawlId) {
	if (__selectedCrawls[crawlId]) {
		delete(__selectedCrawls[crawlId]);
		$('#checkbox-crawl-' + crawlId).attr('src', '../images/checkbox-empty.gif');
	} else {
		__selectedCrawls[crawlId] = true;
		$('#checkbox-crawl-' + crawlId).attr('src', '../images/checkbox-ticked.gif');
	}
};

function crawlNameRenderer(value, cell, record) {
	return "<img src='../images/ico-" + record.data.siteId + ".gif' title='" + record.data.siteId + "'/>"		 
		+ " <a href='EditorEditTicketListingCrawl?action=edit&id=" + record.data.crawlId + "' title='" + record.data.crawlName + "'>" + record.data.crawlName + "</a>";
};

function crawlStatusRenderer(value, cell, record) {
	var statusHtml = "";
	if (record.data.errorMessage == "No tickets found") {
		statusHtml += "<span class='crawlError' title='last crawl ended at " + formatTime(record.data.endCrawl, true) + "'>NO TICKETS</span>";
	} else if (record.data.crawlState == "NET_ERROR" || record.data.crawlState == "EXCEPTION" || record.data.crawlState == "TIMED_OUT" || record.data.crawlState == "NO_TIX") {
		statusHtml += "<span class='crawlError' title='ERROR: " + record.data.errorMessage + "'><a href='javascript:showCrawlException(" + record.data.crawlId + ")'>" + record.data.crawlState + "</a></span>";
	} 

	if (record.data.nextCrawlWaitingTime == 0) {
		statusHtml += "<span class='crawlRunning' title='crawl started at " + formatTime(record.data.startCrawl, true) + "'>"
			+ "<img src='../images/process-running.gif' align='absbottom' />RUNNING</span";
	} else if (!ticketListingCrawlerRunning || record.data.nextCrawlWaitingTime == -1) {
		statusHtml += "<span class='crawlStopped'><img src='../images/ico-stopped.gif' align='absbottom'> STOPPED</span>";
	} else {
		var waitingTime;
		if (record.data.nextCrawlWaitingTime < 60) {
			waitingTime = "READY";
		} else {
			waitingTime = "in " + parseInt(record.data.nextCrawlWaitingTime / 60) + "mn";
		}	

		if (record.data.startCrawl == null) {
			statusHtml += "<span class='crawlTime'>" + waitingTime + "</span>";					
		} else {
			statusHtml += "<span class='crawlTime' title='last crawl ended at " + formatTime(record.data.startCrawl, true) + "' >" + waitingTime + "</span>";					
		}
		
	}
	
	return statusHtml;
};

function artistNameRenderer(value, cell, record) {
	return "<a href='BrowseEvents?artistId=" + record.data.artistId + "'>" + record.data.artistName + "</a>";
};

function crawlFrequencyRenderer(value, cell, record) {
	var frequencies = {
		300: "5mn",
		600: "10mn",
		900: "15mn",
		1800: "30mn",
		3600: "1h",
		7200: "2h",
		10800: "3h",
		14400: "4h",
		18000: "5h",
		21600: "6h",
		25200: "7h",
		28800: "8h",
		32400: "9h",
		36000: "10h",
		39600: "11h",
		43200: "12h",
		86400: "1d",
		172800: "2d",
		1576800000: "Idle"		
	};

	return frequencies[record.data.crawlFrequency];
};

function eventNameRenderer(value, cell, record) {
	var eventFormattedDate;
	if (record.data.eventId == null) {
		return "AUTO";
	} else {
		eventFormattedDate = record.data.eventFormattedDateTime.split(" ")[0];
		return "<a href='BrowseTickets?eventId=" + record.data.eventId + "' title='" + record.data.eventFormattedDateTime + " - " + record.data.eventVenueDescription + "'>" + eventFormattedDate + "</a>";
	}
};

function actionRenderer(value, cell, record) {
	return "<a class='edit-link' href='EditorEditTicketListingCrawl?action=edit&id=" + record.data.crawlId + "'>Edit</a>"
		/* + " <a class='delete-link' href='EditorConfirmDeleteCrawl?crawlId=" + record.data.crawlId + "'>Delete</a>" */
		+ " <a class='disable-link' href='EditorConfirmDisableCrawl?crawlId=" + record.data.crawlId + "'>Disable</a>" 
		+ " <a class='outside-link' href='RedirectToListingPage?id=" + record.data.eventId + "'>Merchant</a>"
};
	
</script>