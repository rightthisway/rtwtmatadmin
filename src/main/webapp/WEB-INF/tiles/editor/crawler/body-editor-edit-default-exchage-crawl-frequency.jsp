<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">
	 
	function changeAction(action){
		var flag =false;
		$('#selectFrequencyError').css("display","none");
		$('#exchangeSelectError').css("display","none");
		
		if($('#selectFrequency').val()=='--'){
			$('#selectFrequencyError').css("display","block");
			flag=true;
		}
		if($('#exchangeSelect').val()=='--'){
			$('#exchangeSelectError').css("display","block");
			flag=true;
		}
		if(flag){
			return false;
		}
		
		
		
		if(action == 'Save'){
			document.getElementById('action').value="save";
		}else if(action == 'Delete'){
			document.getElementById('action').value="delete";		
		}
		
		
		$('#formId').submit();
	}
	
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> > Default Exchange Crawl Frequency
</div>
<br/>
<center>
	<c:choose>
		<c:when test="${not empty info}">
			<div class="info">
				<div class="infoText">
					${info}
				</div>
				
			</div>
		</c:when>
		<c:when test="${not empty error}">
			<div class="error">
				<div class="errorText">
					${error}
				</div>
				
			</div>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
	<form action="loadEditorEditExchangeCrawlFrequency" id="formId" method="post" >
		<table>
			<tr>
				<td>
					Exchange :
				</td>
				<td>
				<input type="hidden" name="id"  id="id"  value="${id}" />
					<select name="exchangeSelect" id="exchangeSelect" onchange="getTicketType($(this).val(),'')">
						<option value="--" <c:if test="${exchange eq '' || exchange eq null}">selected</c:if>>Select</option>
						<option <c:if test="${exchange=='admitoneeventinventory'}">selected</c:if> id="admitoneeventinventory">admitoneeventinventory</option>
						<option <c:if test="${exchange=='ebay'}">selected</c:if> id="ebay">ebay</option>
						<option <c:if test="${exchange=='eibox'}">selected</c:if> id="eibox">eibox</option>
						<option <c:if test="${exchange=='eimarketplace'}">selected</c:if> id="eimarketplace">eimarketplace</option>
						
						<option <c:if test="${exchange=='eventinventory'}">selected</c:if> id="eventinventory">eventinventory</option>
						<option <c:if test="${exchange=='fansnap'}">selected</c:if> id="fansnap">fansnap</option>
						<option <c:if test="${exchange=='flashseats'}">selected</c:if> id="flashseats">flashseats</option>
						<option <c:if test="${exchange=='tickpick'}">selected</c:if> id="tickpick">tickpick</option>
						<option <c:if test="${exchange=='razorgator'}">selected</c:if> id="razorgator">razorgator</option>
						<option <c:if test="${exchange=='seatwave'}">selected</c:if> id="seatwave">seatwave</option>
						
						<option <c:if test="${exchange=='seatwavefeed'}">selected</c:if> id="seatwavefeed">seatwavefeed</option>
						<option <c:if test="${exchange=='stubhub'}">selected</c:if> id="stubhub">stubhub</option>
						<option <c:if test="${exchange=='stubhubfeed'}">selected</c:if> id="stubhubfeed">stubhubfeed</option>
						<option <c:if test="${exchange=='ticketevolution'}">selected</c:if> id="ticketevolution">ticketevolution</option>
						
						<option <c:if test="${exchange=='ticketmaster'}">selected</c:if> id="ticketmaster">ticketmaster</option>
						<option <c:if test="${exchange=='ticketsnetwork'}">selected</c:if> id="ticketsnetwork">ticketsnetwork</option>
						<option <c:if test="${exchange=='ticketnetworkdirect'}">selected</c:if> id="ticketnetworkdirect">ticketnetworkdirect</option>
						<option <c:if test="${exchange=='ticketsnow'}">selected</c:if> id="ticketsnow">ticketsnow</option>
						
						<option <c:if test="${exchange=='tickesolutions'}">selected</c:if> id="tickesolutions">tickesolutions</option>
						<option <c:if test="${exchange=='viagogo'}">selected</c:if> id="viagogo">viagogo</option>
						<option <c:if test="${exchange=='vividseat'}">selected</c:if> id="vividseat">vividseat</option>
						<option <c:if test="${exchange=='wstickets'}">selected</c:if> id="wstickets">wstickets</option>
					</select>
					<div id="exchangeSelectError" style="display:none;">
						<font color="red">Select Exchange.</font>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
					Crawl Frequency:
				</td>
				<td colspan="2">
				  <select name="selectFrequency" id="selectFrequency">
				  <option value="--" <c:if test="${crawlFrequency  eq '' || crawlFrequency eq null}">selected</c:if>>Select</option>
				  <option value="86400"  <c:if test="${crawlFrequency=='86400'}">selected</c:if>>1d</option>
				  <option value="172800" <c:if test="${crawlFrequency=='172800'}">selected</c:if>>2d</option>
				   <option value="259200" <c:if test="${crawlFrequency=='259200'}">selected</c:if>>3d</option>
				   <option value="345600" <c:if test="${crawlFrequency=='345600'}">selected</c:if>>4d</option>
				  <option value="1576800000" <c:if test="${crawlFrequency=='1576800000'}">selected</c:if>>Idle</option>
				</select>
				<div id="selectFrequencyError" style="display:none;">
						<font color="red">Select Crawl Frequency.</font>
				</div>
			</tr>
			<tr>
				<td >Automatic Crawl Frequency:</td>
				<td>
				 	<input type="radio" name="acf" value="true"  <c:if test="${autoFrequency}">checked="checked"</c:if>/> True
					<input type="radio" name="acf" value="false"  <c:if test="${!autoFrequency}">checked="checked"</c:if> /> False
				</td>
			</tr>
			
			<tr>
			
			<c:choose>
			<c:when test="${!enableDelete}">
				<td colspan="2" align ="center"> 
					<input type="hidden" name="action" id="action" value="" />
					<input type= "submit"  id="submitValue" value="Submit" class="medButton" onclick="return changeAction('Save')"/>
				</td>
			
			</c:when>
			<c:otherwise>
				<td align ="center" colspan="2"> 
					<input type="hidden" name="action" id="action" value="" />
					<input type= "submit" id="submitValue" value="Save" class="medButton" onclick="return changeAction('Save')"/>
					<input type= "submit"  id="submitValue" value="Delete" class="medButton" onclick="return changeAction('Delete')"/>
				</td>
				
			</c:otherwise>
			</c:choose>
				
			</tr>
		</table>
	</form>
	<div>
		<display:table class="list" name="${exchangeCrawlFrquencies}" id="exchange" requestURI="loadEditorEditExchangeCrawlFrequency" pagesize="50">
			<display:column sortable="true" >
				<a href="loadEditorEditExchangeCrawlFrequency?edit=${exchange.id}">Edit</a>
			</display:column>
			<display:column title="Exchange"  sortable="true" property="exchange"/>
			<display:column title="Crawl Frequency"  sortable="true" property="defaultCrawlFrequency"/>
			
			<display:column title="Auto Frequency"  sortable="true" property="automaticCrawlFrequency">
				
			</display:column>
			
		</display:table>
	</div>
	
</center>