<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	<!-- &gt; <a href="EditorTourCrawls">Tour Crawls</a> --> 
	&gt; <a href="EditorManageCrawls">Manage Crawls</a>
	&gt; Delete Crawl
</div>

<h1>Delete ticket listing crawl ${ticketListingCrawl.name}?</h1>

<table>
	<tr>
		<td>Crawl Id</td><td>${ticketListingCrawl.id}</td>
	</tr>
	<tr>
		<td>Crawl Name</td><td>${ticketListingCrawl.name}</td>
	</tr>
	<tr>
		<td>Site Id</td><td>${ticketListingCrawl.siteId}</td>
	</tr>
	<%-- <tr>
		<td>Tour/Season</td><td>${ticketListingCrawl.tour.name}</td>
	</tr> --%>
	<tr>
		<td>Event</td>
		<td>
			<c:choose>
				<c:when test="${empty ticketListingCrawl.eventId}">
					AUTO
				</c:when>
				<c:otherwise>
					<fmt:formatDate value="${ticketListingCrawl.event.date}" pattern="yy/MM/dd" /> - ${ticketListingCrawl.event.venue.building}, ${ticketListingCrawl.event.venue.city}, ${ticketListingCrawl.event.venue.state}, ${ticketListingCrawl.event.venue.country}
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
	<tr>
		<td>Query URL</td><td>${ticketListingCrawl.queryUrl}</td>
	</tr>
	<tr>
		<td>Extra Parameters</td><td>${ticketListingCrawl.extraParametersString}</td>
	</tr>
	<tr>
		<td>Creator</td><td>${ticketListingCrawl.creator}</td>
	</tr>
	<tr>
		<td>Creation Date</td><td><fmt:formatDate value="${ticketListingCrawl.created}" pattern="yy/MM/dd" /></td>
	</tr>
	<tr>
		<td>Last Updater</td><td>${ticketListingCrawl.lastUpdater}</td>
	</tr>
	<tr>
		<td>Last Update Date</td><td><fmt:formatDate value="${ticketListingCrawl.lastUpdated}" pattern="yy/MM/dd" /></td>
	</tr>
	<tr>
		<td>Enabled</td><td>${ticketListingCrawl.enabled}</td>
	</tr>
	<tr>
		<td>Crawl Automatic Frequency</td><td>${ticketListingCrawl.automaticCrawlFrequency}</td>
	</tr>
	<tr>
		<td>Crawl Frequency</td><td>${ticketListingCrawl.crawlFrequency} s</td>
	</tr>
</table>

<h2>Crawl Stat</h2>
<table class="list">
	<thead>
	<tr>
		<th></th>
		<th>ACTIVE</th>
		<th>SOLD</th>
		<th>EXPIRED</th>
		<th>TOTAL</th>
	</tr>	
	</thead>
	<tbody>
		<tr>
			<td>Listing Count</td>
			<td>${ticketListingCrawl.totalFetchedActiveTicketCount}</td>
			<td>${ticketListingCrawl.totalFetchedSoldTicketCount}</td>
			<td>${ticketListingCrawl.totalFetchedExpiredTicketCount}</td>
			<td>${ticketListingCrawl.totalFetchedTicketCount}</td>
		</tr>
		<tr>			
			<td>Ticket Count</td>
			<td>${ticketListingCrawl.totalFetchedActiveTicketQuantitySum}</td>
			<td>${ticketListingCrawl.totalFetchedSoldTicketQuantitySum}</td>
			<td>${ticketListingCrawl.totalFetchedExpiredTicketQuantitySum}</td>
			<td>${ticketListingCrawl.totalFetchedTicketQuantitySum}</td>
		</tr>
	</tbody>
</table>

<br /><br />
<div class="error">
	<div class="errorText">
		This action cannot be reversed. All the tickets fetched by this crawl will be permanently deleted.<br />
		Review the information above  before deleting the crawl.		
	</div>
</div> 
<br /><br />
<input type="button" value="Delete" class="medButton" onClick="document.location.href='EditorManageCrawls?crawlFormIds=${ticketListingCrawl.id}&crawlFormAction=removeCrawl'" />
<input type="button" value="Cancel" class="medButton" onClick="document.location.href='EditorManageCrawls'" />		