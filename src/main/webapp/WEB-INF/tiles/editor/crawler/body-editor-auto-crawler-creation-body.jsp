<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<head>
	<script type="text/javascript">
		function changeDate(cur){
			$('#selectedEventId').val('');
			var reset= new Date();
			$('#toDateYear').val(reset.getFullYear());
			$('#toDateMonth').val(reset.getMonth()+1);
			$('#toDateDay').val(reset.getDate());
			$('#fromDateYear').val(reset.getFullYear());
			$('#fromDateMonth').val(reset.getMonth()+1);
			$('#fromDateDay').val(reset.getDate());
			var defaultValue = 0;
			$("#group .eventCheckBox").each(function(){
				if($(this).is(":checked")){
					var id = $(this).attr("id").split('-')[1];
					$('#selectedEventId').val($('#selectedEventId').val()+',' + id);
					//alert($('#selectedEventId').val());
					var dates = $('#eventDate-'+id).val().split(' ')[0].split('/');					
					var eTime = $('#eventDate-'+id).val().split(' ')[1];					
					if(eTime == 'TBD'){
						$('#eventDate-'+id).val($('#eventDate-'+id).val().split(' ')[0] + ' 00:00');
					}	
					var strTBD = $('#eventDate-'+id).val().split(' ')[0];
					if($('#eventDate-'+id).val().split(' ')[0] == 'TBD'){
						var selectedDate=new Date();
					}else{
						var selectedDate=new Date($('#eventDate-'+id).val());
					}					
					
					var now= new Date();
					var toDate= new Date();
					toDate.setFullYear($('#toDateYear').val(),$('#toDateMonth').val()-1,$('#toDateDay').val());
					//alert(toDate);
					//alert("Now:" + now + "to Date:" + toDate + "Selected Date:"  + selectedDate);
					//alert(toDate-selectedDate);
					if(strTBD == 'TBD'){
						selectedDate = new Date(selectedDate.getFullYear(),selectedDate.getMonth()+8,selectedDate.getDate());
					}
					
					if(toDate<selectedDate || defaultValue==0){
					//	alert(selectedDate);
						$('#toDateYear').val(selectedDate.getFullYear());
						$('#toDateMonth').val(selectedDate.getMonth()+1);
						$('#toDateDay').val(selectedDate.getDate());
						}
					now= new Date();
					var fromDate= new Date();
					fromDate.setFullYear($('#fromDateYear').val(),$('#fromDateMonth').val()-1,$('#fromDateDay').val());
					//alert("Now:" + now + "From Date:" + fromDate + "Selected Date:"  + selectedDate);
					//alert(now-fromDate);
					//alert(selectedDate-fromDate);
					//if(now-fromDate >=0 || selectedDate-fromDate<0){
					if(strTBD != 'TBD'){
						if(fromDate > selectedDate || defaultValue==0){
					//alert(selectedDate);
						$('#fromDateYear').val(selectedDate.getFullYear());
						$('#fromDateMonth').val(selectedDate.getMonth()+1);
						$('#fromDateDay').val(selectedDate.getDate());
						}
					}
					
					defaultValue=1;
				}
			});
			$('#fromDateYear').trigger('change');
			$('#toDateYear').trigger('change');
		}
	</script>
</head>
<fmt:formatDate pattern="yyyy" value="${fromDate}" var="fromYear" />
<fmt:formatDate pattern="M" value="${fromDate}" var="fromMonth" />
<fmt:formatDate pattern="d" value="${fromDate}" var="fromDay" />

<fmt:formatDate pattern="yyyy" value="${toDate}" var="toYear" />
<fmt:formatDate pattern="M" value="${toDate}" var="toMonth" />
<fmt:formatDate pattern="d" value="${toDate}" var="toDay" />

<%-- <authz:authorize ifAnyGranted="PRIV_ADMIN"> --%>
	  <div>
	    <form id="marketMakerEventTrackingUpdateForm" action="MarketMakerEventTrackingUpdate">
		    <input type="hidden" name="eventId" value="${event.id}"/>
	    	Managed by: <img src="../images/ico-user.gif" align="bottom"/>
		    <select name="marketMaker" onchange="$('#marketMakerEventTrackingUpdateForm').submit()">
		      <c:forEach var="curMarketMaker" items="${marketMakers}">
		        <option value="${curMarketMaker.username}" <c:if test="${marketMaker.username == curMarketMaker.username}">selected</c:if>>${curMarketMaker.username}</option>
		      </c:forEach>
	          <option value="DESK" <c:if test="${empty marketMaker}">selected</c:if>>DESK</option>
		    </select>
	   </form>
	  </div>
<%-- </authz:authorize> --%>

<h2>Crawls</h2>
<table>
	<tr>
		<td>
			<select class="addCrawlSelector" onchange="changeAddCrawler($(this).val())">
				<option selected>-- Create new crawl --</option>
				<c:forEach var="siteId" items="${constants.siteIds}">
					<c:set var="safeSiteId" value="[${siteId}]" />
				   <c:choose>
				      <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
					    <option style="text-decoration:line-through;margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}" disabled>${siteId}</option>
				      </c:when>
				      <c:otherwise>
					    <option style="margin-left: 5px;padding-left: 20px; background-image:url(../images/ico-${siteId}.gif);background-repeat: no-repeat; height: 20px;" value="${siteId}">${siteId}</option>
				      </c:otherwise>
				   </c:choose>
				</c:forEach>
			</select>
		</td>
		
	</tr>
</table>

<a name="addCrawlers"></a>
<h2>Add Crawlers</h2>

<form id="editorAutoCrawlerCreation" action="EditorAutoCrawlerCreation#addCrawlers" method="POST" onsubmit="return checkSearch()">
<input type="hidden" name="artistId" value="${artistId}"/>
<input type="hidden" name="venueId" value="${venueId}"/>
<input type="hidden" name="eventId" value="${event.id}"/>
<input type="hidden" name="selectedEventId" id="selectedEventId" value="${selectedEventId}"/>



<b>Keywords:</b> <input id="keywords" name="keywords" type="text" size="50"
value="<c:choose><c:when test="${not empty keywords}">${keywords}</c:when><c:otherwise>${event.artist.name}</c:otherwise></c:choose>" />
<input type="submit" id="search" name="action" class="medButton" value="Search" />
<span id="searchWait" style="display:none"><img src="../images/process-running.gif" align="absmiddle" /> <b>Searching. Please wait...</b></span>
<br/><br/>
<b>Location:</b> <input type="text" name="location" value="${param.location}" />

<c:choose>
	<c:when test="${event.date != null}">
		<fmt:formatDate pattern="yyyy" value="${event.date}" var="eventYear" />
	</c:when>
	<c:otherwise>
	<jsp:useBean id="now" class="java.util.Date" scope="request" />
		<fmt:formatDate pattern="yyyy" value="${now}" var="eventYear" />
	</c:otherwise>
</c:choose>

	<b>From Date:</b>
	<select name="fromDateYear" id="fromDateYear">
	  <c:forEach var="year" begin="${eventYear - 1}" end="${eventYear + 5}" step="1">
	    <option value="${year}" <c:if test="${year == fromYear}">selected</c:if>>${year}</option>
	  </c:forEach>
	</select> /
	<select name="fromDateMonth" id="fromDateMonth">
	  <c:forEach var="month" begin="1" end="12" step="1">
	    <option value="${month}" <c:if test="${month == fromMonth}">selected</c:if>>${month}</option>
	  </c:forEach>
	</select> /
	<select name="fromDateDay" id="fromDateDay">
	  <c:forEach var="day" begin="1" end="31" step="1">
	    <option value="${day}" <c:if test="${day == fromDay}">selected</c:if>>${day}</option>
	  </c:forEach>
	</select>
	<input type="text" id="fromCalendar" class="calendar"  style="visibility:hidden;width:0px;" />
	

	<b>To Date:</b>
	<select name="toDateYear" id="toDateYear">
	  <c:forEach var="year" begin="${eventYear - 1}" end="${eventYear + 5}" step="1">
	    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
	  </c:forEach>
	</select> /
	<select name="toDateMonth" id="toDateMonth">
	  <c:forEach var="month" begin="1" end="12" step="1">
	    <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
	  </c:forEach>
	</select> /
	<select name="toDateDay" id="toDateDay">
	  <c:forEach var="day" begin="1" end="31" step="1">
	    <option value="${day}" <c:if test="${day == toDay}">selected</c:if>>${day}</option>
	  </c:forEach>
	</select>
	
	<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<%
/*
<b>To:</b>
<select name="toYear">
  <c:forEach var="year" begin="${eventYear - 1}" end="${eventYear + 5}" step="1">
    <option value="${year}" <c:if test="${year == toYear}">selected</c:if>>${year}</option>
  </c:forEach>
</select> /
<select name="toMonth">
  <c:forEach var="month" begin="1" end="12" step="1">
    <option value="${month}" <c:if test="${month == toMonth}">selected</c:if>>${month}</option>
  </c:forEach>
</select> /
<select name="toDay">
  <c:forEach var="day" begin="1" end="31" step="1">
    <option value="${day}" <c:if test="${day == toDay}">selected</c:if>>${day}</option>
  </c:forEach>
</select>

<input type="text" id="toCalendar" class="calendar" style="visibility:hidden;width:0px;" />

 (YYYY/MM/DD)
*/
%>

<c:if test="${not empty dateError}">
  <span class="error">${dateError}</span>
</c:if>
<br/><br/>
<b>Create Stubhub crawl From TicketEvolution:</b> <input type="checkbox" id="isStubhubFromTicketEvolution" name="isStubhubFromTicketEvolution" class="isStubhubFromTicketEvolution"
	<c:if test="${isStubhubFromTicketEvolution != null and isStubhubFromTicketEvolution}">checked</c:if> >
<br/><br/>
Sites: (<a href="#" onclick="return checkAllSites()">check all</a> - <a href="#" onclick="return uncheckAllSites()">uncheck all</a>)
<div style="clear:both"></div>
<c:forEach var="sid" items="${constants.siteIds}">
	<c:set var="safeSiteId" value="[${sid}]" />

	<div style="float: left; width: 280px;height: 24px;">
		<input type="checkbox" id="checkbox-${sid}" name="checkbox-${sid}" class="site-checkbox"
			<c:set var="safeSid" value="[${sid}]" /> 
			<c:if test="${siteFilters[sid] and not fn:contains(constants.disabledSiteIdsString, safeSiteId)}">checked</c:if>
			<c:if test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">disabled</c:if>
		>
		<label for="checkbox-${sid}">
	    	<img src="../images/ico-${sid}.gif" border="0" align="absmiddle"/>						
			<c:choose>
			  <c:when test="${fn:contains(constants.disabledSiteIdsString, safeSiteId)}">
			    <span style="color: #888888; text-decoration: line-through" tooltip="${sid} is disabled">
				  ${sid}
			    </span>
			  </c:when>
			  <c:otherwise>
			    ${sid}
			  </c:otherwise>
			</c:choose>
		</label>
	</div>
</c:forEach>
<div style="clear:both"></div>
</form>

<form id="editorAutoCrawlerCreation" action="EditorAutoCrawlerCreation" method="POST">
<input type="hidden" name="artistId" value="${artistId}"/>
<input type="hidden" name="eventId" value="${event.id}"/>
<input type="hidden" name="keywords" value="${param.keywords}"/>
<input type="hidden" name="location" value="${param.location}"/>
<input type="hidden" name="fromYear" value="${fromYear}"/>
<input type="hidden" name="fromMonth" value="${fromMonth}"/>
<input type="hidden" name="fromDay" value="${fromDay}"/>
<input type="hidden" name="toYear" value="${toYear}"/>
<input type="hidden" name="toMonth" value="${toMonth}"/>
<input type="hidden" name="toDay" value="${toDay}"/>
<input type="hidden" name="selectedEventId" id="selectedEventId" value="${selectedEventId}"/>
<c:choose>
	<c:when test="${not empty eventHits}">
	<h2>Matching Crawls</h2>
	  Select:
	  <a href="#" onclick="return checkAllEventHits()">All</a> - 
	  <a href="#" onclick="return uncheckAllEventHits()">None</a>
	  <br/>
	  <display:table class="list" name="${eventHits}" id="eventHit" requestURI="EditorAutoCrawlerCreation" defaultsort="3">
	    <display:column>
		<!--
		<c:choose>
			<c:when test="${fn:indexOf(eventHit.url,'ticketexchangebyticketmaster')>0}">
				<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}"  disabled class="eventHit-checkbox"/>
					  <label for="eventHit-${eventHit.id}">
						<img src="../images/ico-${eventHit.siteId}.gif"/>
					  </label>	
			</c:when>
			<c:otherwise>
					<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}" class="eventHit-checkbox" />
					  <label for="eventHit-${eventHit.id}">
						<img src="../images/ico-${eventHit.siteId}.gif"/>
					  </label>
			</c:otherwise>
		</c:choose>
	      -->
	      <c:if test="${empty eventHit.existingCrawlId}">
		 	<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}" class="eventHit-checkbox"/>
	 	</c:if>
		<label for="eventHit-${eventHit.id}">
			<img src="../images/ico-${eventHit.siteId}.gif"/>
		</label>	
	    </display:column>
	    <display:column sortable="true" title="Event Name">
		<c:choose>
				<c:when test="${fn:indexOf(eventHit.url,'ticketexchangebyticketmaster')>0}">
				<a target="_blank" href="${eventHit.url}" onclick="return false">${eventHit.name}</a>			
				</c:when>
				<c:otherwise>
					<a target="_blank" href="${eventHit.url}">${eventHit.name}</a>
				</c:otherwise>
		</c:choose>	      
	    </display:column>
	    <display:column sortable="true" sortProperty="dateAndTime" title="Date">
		<c:set var="eventHitDate" value="" />
		<c:set var="eventHitTime" value="" />
		<c:set var="eventHitDateTime" value="" />
			<c:choose>
				<c:when test="${not empty eventHit.date}">
					<fmt:formatDate var="eventHitDate" pattern="MM/dd/yyyy" value="${eventHit.date}" />
					<c:choose>
						<c:when test="${not empty eventHit.time}">
							<fmt:formatDate var="eventHitTime" pattern="HH:mm" value="${eventHit.time}" />
					  </c:when>
					  <c:otherwise>
						<c:set var="eventHitTime" value="TBD" />
					  </c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:set var="eventHitDate" value="TBD" />
				</c:otherwise>
			</c:choose>
			<c:set var="eventHitDateTime" value="${eventHitDate} ${eventHitTime}"/> 
			${eventHitDateTime}
	    </display:column>
		<display:column sortable="true" title="Event">
			<select id="event-${eventHit.id}" name="event-${eventHit.id}" >
				<option value="0">select</option>
				<c:forEach var="event" items="${selectedEvents}">
					<c:set var="eventDate" value="" />
					<c:set var="eventTime" value="" />
					<c:set var="eventDateTime" value="" />
					<c:choose>
						<c:when test="${not empty event.date}">
							<fmt:formatDate var ="eventDate" pattern="MM/dd/yyyy" value="${event.date}" />
							<c:choose>
								<c:when test="${not empty event.time}">
									<fmt:formatDate var ="eventTime" pattern="HH:mm" value="${event.time}" />
								</c:when>
								<c:otherwise>
									<c:set var="eventTime" value="TBD" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:set var="eventDate" value="TBD" />
						</c:otherwise>
					</c:choose>
					<c:set var="eventDateTime" value="${eventDate} ${eventTime}"/>
					<option value="${event.id}"  <c:if test="${eventDateTime == eventHitDateTime}">selected</c:if>>${event.name}-${event.formatedDate} 
					</option>
				</c:forEach>
			</select>
	    </display:column>
	    <display:column sortable="true" title="Status">
			<c:choose>
			  <c:when test="${not empty eventHit.crawlState.error}">				
					<font color="red">${eventHit.crawlState}</font>				
			  </c:when>
			  <c:otherwise>
			  	<font color="green">OK</font>		  
			  </c:otherwise>
			</c:choose>
	    </display:column>
	    <display:column sortable="true" title="Location" property="location" />
	    <display:column sortable="true" title="isCreated">
			<c:choose>
			  <c:when test="${not empty eventHit.existingCrawlId}">
			  <a href="#" onclick="getExistingTMATEventDetails('${eventHit.existingCrawlId}');">Yes</a>				
			  </c:when>
			  <c:otherwise>
			  	-	  
			  </c:otherwise>
			</c:choose>
		</display:column>
	  </display:table>
	  <br/>
	  <br/>
	  
	</c:when>
	<c:when test="${param.action == 'Search'}">
	  <i>No events found. Please try again by discarding some keywords, location and increase the date range.</i> 
	</c:when>
</c:choose>

<br />

<c:choose>
	<c:when test="${not empty additionalEventHits}">
	<h2>Probable Crawls</h2>
	  Select:
	  <a href="#" onclick="return checkAllProbableEventHits()">All</a> - 
	  <a href="#" onclick="return uncheckAllProbableEventHits()">None</a>
	  <br/>
	
		<div >
	   <display:table class="list" name="${additionalEventHits}" id="eventHit" requestURI="EditorAutoCrawlerCreation" defaultsort="3">
	    <display:column>
		<!--
		<c:choose>
			<c:when test="${fn:indexOf(eventHit.url,'ticketexchangebyticketmaster')>0}">
			
				<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}"  class="probable-eventHit-checkbox" disabled/>
				<label for="eventHit-${eventHit.id}">
					<img src="../images/ico-${eventHit.siteId}.gif"/>
				</label>	
			</c:when>
			<c:otherwise>
				<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}" class="probable-eventHit-checkbox" />
				<label for="eventHit-${eventHit.id}">
					<img src="../images/ico-${eventHit.siteId}.gif"/>
				</label>
			</c:otherwise>
		</c:choose>
		-->
		 <c:if test="${empty eventHit.existingCrawlId}">
	    	<input type="checkbox" id="eventHit-${eventHit.id}" name="eventHit-${eventHit.id}" class="probable-eventHit-checkbox" />
	     </c:if>
		<label for="eventHit-${eventHit.id}">
			<img src="../images/ico-${eventHit.siteId}.gif"/>
		</label>
	    </display:column>
	    <display:column sortable="true" title="Event Name">
		<c:choose>
				<c:when test="${fn:indexOf(eventHit.url,'ticketexchangebyticketmaster')>0}">
				<a target="_blank" href="${eventHit.url}" onclick="return false">${eventHit.name}</a>			
				</c:when>
				<c:otherwise>
					<a target="_blank" href="${eventHit.url}">${eventHit.name}</a>
				</c:otherwise>
		</c:choose>	      
	    </display:column>
	    <display:column sortable="true" sortProperty="date" title="Date">
		<c:set var="eventHitDate" value="" />
		<c:set var="eventHitTime" value="" />
		<c:set var="eventHitDateTime" value="" />
			<c:choose>
				<c:when test="${not empty eventHit.date}">
					<fmt:formatDate var="eventHitDate" pattern="MM/dd/yyyy" value="${eventHit.date}" />
					<c:choose>
						<c:when test="${not empty eventHit.time}">
							<fmt:formatDate var="eventHitTime" pattern="HH:mm" value="${eventHit.time}" />
					  </c:when>
					  <c:otherwise>
						<c:set var="eventHitTime" value="TBD" />
					  </c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:set var="eventHitDate" value="TBD" />
				</c:otherwise>
			</c:choose>
			<c:set var="eventHitDateTime" value="${eventHitDate} ${eventHitTime}"/> 
			${eventHitDateTime}
	    </display:column>
		<display:column sortable="true" title="Event">
			<select id="event-${eventHit.id}" name="event-${eventHit.id}" onchange="autoSelect($(this))">
				<option value="0">select</option>
				<c:forEach var="event" items="${selectedEvents}">
					<c:set var="eventDate" value="" />
					<c:set var="eventTime" value="" />
					<c:set var="eventDateTime" value="" />
					<c:choose>
						<c:when test="${not empty event.date}">
							<fmt:formatDate var ="eventDate" pattern="MM/dd/yyyy" value="${event.date}" />
							<c:choose>
								<c:when test="${not empty event.time}">
									<fmt:formatDate var ="eventTime" pattern="HH:mm" value="${event.time}" />
								</c:when>
								<c:otherwise>
									<c:set var="eventTime" value="TBD" />
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:set var="eventDate" value="TBD" />
						</c:otherwise>
					</c:choose>
					<c:set var="eventDateTime" value="${eventDate} ${eventTime}"/>
					<option value="${event.id}"  <c:if test="${eventDateTime == eventHitDateTime}">selected</c:if>>${event.name}-${event.formatedDate} 
					</option>
				</c:forEach>
			</select>
	    </display:column>
	    <display:column sortable="true" title="Status">
			<c:choose>
			  <c:when test="${not empty eventHit.crawlState.error}">				
					<font color="red">${eventHit.crawlState}</font>				
			  </c:when>
			  <c:otherwise>
			  	<font color="green">OK</font>		  
			  </c:otherwise>
			</c:choose>
	    </display:column>
	    <display:column sortable="true" title="Location" property="location" />
	    <display:column sortable="true" title="isCreated">
			<c:choose>
			  <c:when test="${not empty eventHit.existingCrawlId}">
			  <a href="#" onclick="getExistingTMATEventDetails('${eventHit.existingCrawlId}');">Yes</a>				
			  </c:when>
			  <c:otherwise>
			  	-	  
			  </c:otherwise>
			</c:choose>
		</display:column>
	  </display:table>
	  <br/>
	  <br/>
	 
	 </div>
	</c:when>
	
</c:choose>
<c:choose>
	<c:when test="${not empty eventHits || not empty additionalEventHits}">
		<input type="submit" class="medButton" name="action" value="Create" onclick="return createCrawlers()" />
	</c:when>
</c:choose>


<c:forEach var="eventListingResult" items="${eventListingResults}">
	<c:if test="${not empty eventListingResult.exception}">
		<br />
		<img src="../images/ico-error.gif" align="absbottom" />Error during search on ${eventListingResult.siteId}:${eventListingResult.exception}
		(<small><a href="javascript:void(0)" onclick="$('#eventListingResultException-${eventListingResult.siteId}').toggle()">show/hide exception</a></small>)
		<div id="eventListingResultException-${eventListingResult.siteId}" style="display:none;border: 1px solid black; overflow: auto;height: 100px;">${eventListingResult.exceptionStackTrace}</div>
	</c:if>
</c:forEach>

</form>

<script type="text/javascript">

	$(document).ready(function() {
		$('#fromDateYear').change(function(){
			var year =parseInt($('#fromDateYear').val(),'10');
			var month =parseInt($('#fromDateMonth').val(),'10');
			var day = parseInt($('#fromDateDay').val(),'10');
			var monthStr='';
			var dayStr='';
			if(month<10){
				monthStr = '0' + month;
			}else{
				monthStr = '' + month;
			}
			if(day<10){
				dayStr = '0' + day;
			}else{
				dayStr = '' + day;
			}
			$('#fromCalendar').val( monthStr + '/' + dayStr + '/' + year);
		});
		
		$('#fromDateMonth').change(function(){
			$('#fromDateYear').trigger('change');
		});
		
		$('#fromDateDay').change(function(){
			$('#fromDateYear').trigger('change');
		});
		
		$('#isStubhubFromTicketEvolution').click(function(){
			if($('#isStubhubFromTicketEvolution').attr('checked')){
				$('#checkbox-ticketevolution').attr('checked', true);
				$('#checkbox-stubhub').attr('checked', false);
			}
		});
		$('#checkbox-stubhub').click(function(){
			if($('#checkbox-stubhub').attr('checked')){
				$('#isStubhubFromTicketEvolution').attr('checked', false);
			}
		});
		$('#checkbox-ticketevolution').click(function(){
			if(!$('#checkbox-ticketevolution').attr('checked')){
				$('#isStubhubFromTicketEvolution').attr('checked', false);
			}
		});
		
		$('#fromCalendar').datepicker(
			{
				'buttonImageOnly': true,
				buttonImage: '../images/ico-calendar.gif',
				showOn: 'button',
				//showButtonPanel: true,
				changeMonth: true,
				changeYear: true,
				minDate: new Date((new Date()).getTime()- (24 * 60 * 60 * 1000)),
				onClose:function(dateText){
					var year =parseInt(dateText.split('/')[2],'10');
					var month =parseInt(dateText.split('/')[0],'10');
					var day = parseInt(dateText.split('/')[1],'10');
					
					$('#fromDateYear').val(year);
					$('#fromDateMonth').val(month);
					$('#fromDateDay').val(day);
				}
			}
		);
		
		$('#toDateYear').change(function(){
			var year =parseInt($('#toDateYear').val(),'10');
			var month =parseInt($('#toDateMonth').val(),'10');
			var day = parseInt($('#toDateDay').val(),'10');
			var monthStr='';
			var dayStr='';
			if(month<10){
				monthStr = '0' + month;
			}else{
				monthStr = '' + month;
			}
			if(day<10){
				dayStr = '0' + day;
			}else{
				dayStr = '' + day;
			}
			$('#toCalendar').val( monthStr + '/' + dayStr + '/' + year);
		});
		
		$('#toDateMonth').change(function(){
			$('#toDateYear').trigger('change');
		});
		
		$('#toDateDay').change(function(){
			$('#toDateYear').trigger('change');
		});
		
		$('#toCalendar').datepicker(
			{
				'buttonImageOnly': true,
				buttonImage: '../images/ico-calendar.gif',
				showOn: 'button',
				changeMonth: true,
				changeYear: true,
				minDate: new Date((new Date()).getTime()- (24 * 60 * 60 * 1000)),
				onClose:function(dateText){
					var year =parseInt(dateText.split('/')[2],'10');
					var month =parseInt(dateText.split('/')[0],'10');
					var day = parseInt(dateText.split('/')[1],'10');
					
					$('#toDateYear').val(year);
					$('#toDateMonth').val(month);
					$('#toDateDay').val(day);
				}
			}
		);
	/*
		$('.calendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        //showButtonPanel: true,
			//changeMonth: true,
			//changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime()- (24 * 60 * 60 * 1000))/*,
	        maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000)*/
		/*});
		$('#fromCalendar').datepicker(
			{onClose: function(dateText){ 
				alert(dateText);
			$('#fromDateYear').val(dateText.split('/')[0]);
			$('#fromDateMonth').html( dateText.split('/')[1]);
			$('#fromDateDayy').html( dateText.split('/')[2]);
		}});
		/*
		$('#fromCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#fromCalendar').val(($('SELECT[name=fromYear]').val()) + "-" + $('SELECT[name=fromMonth]').val() + "-" + $('SELECT[name=fromDay]').val());
	        }
	    );

		$('#fromCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				//alert(dateText);
				var tokens = dateText.split("-");
				$('SELECT[name=fromYear]').val(tokens[0]);
				$('SELECT[name=fromMonth]').val(parseInt(tokens[1]));
				$('SELECT[name=fromDay]').val(parseInt(tokens[2]));
			}
		);
		
		$('#toCalendar').datepicker('option', 'beforeShow',
	        function(input) {
	        	$('#toCalendar').val(($('SELECT[name=toYear]').val()) + "-" + $('SELECT[name=toMonth]').val() + "-" + $('SELECT[name=toDay]').val());
	        }
	    );

		$('#toCalendar').datepicker('option', 'onSelect',
	        function(dateText) {
				var tokens = dateText.split("-");
				$('SELECT[name=toYear]').val(tokens[0]);
				$('SELECT[name=toMonth]').val(parseInt(tokens[1]));
				$('SELECT[name=toDay]').val(parseInt(tokens[2]));
			}
		);		
		
		$.each($('.addCrawlSelector'), function(i, elt) {
			$(elt).children().get(0).selected = true;
		});
		*/
		$('.ticket-listing-crawl').attr('checked', false);
	
		$('table#crawl TBODY tr').click(
			function() {
				var checkbox = $($(this).children('TD').get(0)).children('INPUT');
				$(checkbox).attr('checked', !$(checkbox).attr('checked'));
				updateTicketListingCrawlRow($(checkbox).attr('id').split("-")[3]);
			}
		);							
						
	});		


	function checkAllSites() {
		$.each($('.site-checkbox'), function(intIndex, input) {
		  if (!$(input).attr('disabled')) {
		  	$(input).attr('checked', 'true').change();
		  }
		});
		return false;
	}

	function uncheckAllSites() {
		$('.site-checkbox').removeAttr('checked').change();
		return false;
  	}

	function checkAllEventHits() {
		$('.eventHit-checkbox').attr('checked', 'true').change();
		return false;
	}

	function uncheckAllEventHits() {
		$('.eventHit-checkbox').removeAttr('checked').change();
		return false;
	};

	function checkAllProbableEventHits() {
		$('.probable-eventHit-checkbox:not(:checked)').each(function(){
			var name=$(this).attr('id').split('-');
			if($('#event-'+name[1]+'-'+name[2]).val()!=0){
				$(this).attr('checked','true').change();
			}
		});
		return false;
	}

	function uncheckAllProbableEventHits() {
		$('.probable-eventHit-checkbox').removeAttr('checked').change();
		return false;
	};
	
	function createCrawlers() {
		var flag=false;
		var duplicatecrawlFlag = false;
		var crawlCountMap ={}; 
		$(".eventCheckBox:checked").each(function() {
			var name=$(this).attr('id').split('-');
			var eventId = name[1];
			var value = $('#enabledCrawl-'+eventId).val();
			//if(value!='' && value.lenght > 0){
				var valueArray = value.split(",");
				for(var i =0;i<valueArray.length;i++) {
					var key = eventId+"-"+valueArray[i];
					if(crawlCountMap[key] == null) {
						crawlCountMap[key] = 1;
					} else {
						crawlCountMap[key] = crawlCountMap[key]+1;
					}
				}
			//}
		});
		$(".eventHit-checkbox:checked").each(function() {
			var name=$(this).attr('id').split('-');
			//alert(name);
			//alert($('#event-'+name[1]+'-'+name[2]).val());
			if($('#event-'+name[1]+'-'+name[2]).val()==0){
				alert('Select valid event for crawler');
				$('#event-'+name[1]+'-'+name[2]).focus();
				flag= false;
				return
			} else {
				var eventId = $('#event-'+name[1]+'-'+name[2]).val();
				var siteId = name[1];
				var key = eventId+"-"+siteId;
				if(crawlCountMap[key] == null) {
					crawlCountMap[key] = 1;	
				} else {
					crawlCountMap[key] = crawlCountMap[key]+1;
					var eventName = $('#eventName-'+eventId).val();
					var eventDate = $('#eventDate-'+eventId).val();
					var venueName = $('#venueName-'+eventId).val();
					alert("We cannot create this crawl at this point of time as it is duplicating with the crawls for following event\n\n"+
							" Event : "+eventName+"\n"+
							" Event Date : "+eventDate+"\n"+
							" Venue : "+venueName+"\n"+
							" Site : "+siteId+"\n\n"+
							"if you still want to create the crawl disable the duplicate crawl in the system for the above mentioned event.");
					
					$('#event-'+name[1]+'-'+name[2]).focus();
					duplicatecrawlFlag= true;
					return false;
				}
			}
			flag=true;
		});
		if(!flag){
			$(".probable-eventHit-checkbox:checked").each(function() {
				var name=$(this).attr('id').split('-');
				//alert(name);
				//alert($('#event-'+name[1]+'-'+name[2]).val());
				if($('#event-'+name[1]+'-'+name[2]).val()==0){
					alert('Select valid event for crawler');
					$('#event-'+name[1]+'-'+name[2]).focus();
					flag= false;
					return
				} else {
					var eventId = $('#event-'+name[1]+'-'+name[2]).val();
					var siteId = name[1];
					var key = eventId+"-"+siteId;
					if(crawlCountMap[key] == null) {
						crawlCountMap[key] = 1;
					} else {
						crawlCountMap[key] = crawlCountMap[key]+1;
						var eventName = $('#eventName-'+eventId).val();
						var eventDate = $('#eventDate-'+eventId).val();
						var venueName = $('#venueName-'+eventId).val();
						alert("We cannot create this crawl at this point of time as it is duplicating with the crawls for following event\n\n"+
								" Event : "+eventName+"\n"+
								" Event Date : "+eventDate+"\n"+
								" Venue : "+venueName+"\n"+
								" Site : "+siteId+"\n\n"+
								"if you still want to create the crawl disable the duplicate crawl in the system for the above mentioned event.");
						
						$('#event-'+name[1]+'-'+name[2]).focus();
						duplicatecrawlFlag= true;
						return false;
					}
				}
				flag=true;
			});
		}
		if(duplicatecrawlFlag) {
		return false;	
		}
		if(!flag){
			alert('Select at least one crawler');
			return false;
		}
		
    	var answer = confirm("Are you sure you want to create the selected crawlers?");
	//	alert(answer);
		if (answer) {
			//$('#editorAutoCrawlerCreation').submit();
			return true;
		}
		return false;
	};

	function autoSelect(obj) {
		var name=$(obj).attr('id').split('-');
		if(obj.val()==0){
			$('#eventHit-'+name[1]+'-'+name[2]).attr('checked',false);
		}else{
			$('#eventHit-'+name[1]+'-'+name[2]).attr('checked',true);
		}
	}	

	function checkSearch() {
/*
		if ($.trim($('#keywords').val()).length == 0) {
			alert('Please enter some keywords');
			return false;
		}
*/
		$('#search').hide();
		$('#searchWait').show();
		return true;
	};
	
	function __getSelectedTicketListingCrawlIds() {
		var ids = "";
		$.each($('.ticket-listing-crawl'), function(i, elt) {
			if ($(elt).attr('checked')) {
				var id = $(elt).attr('id').split("-")[3]; 
				ids += id + ",";
			}
		});
		
		if (ids == "") {
			return null;
		}
		
		// remove the final comma
		ids = ids.substring(0, ids.length - 1)
		return ids;		
	};	
	
	function updateTicketListingCrawlRow(crawlId) {
		if ($('#ticket-listing-crawl-' + crawlId).attr('checked')) {
			$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', '#748C20');
			$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', '#ffffff');
		} else {
			$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', null);
			$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', null);
		}
	};
	

	function selectAllTicketListingCrawls() {
		$.each($('.ticket-listing-crawl'),  function(i,elt) {
			var crawlId = $(elt).attr('id').split("-")[3];
			$('#ticket-listing-crawl-' + crawlId).attr('checked', true);		
			updateTicketListingCrawlRow(crawlId);
		});
	};

	function unselectAllTicketListingCrawls() {
		$.each($('.ticket-listing-crawl'),  function(i,elt) {
			var crawlId = $(elt).attr('id').split("-")[3];
			$('#ticket-listing-crawl-' + crawlId).attr('checked', false);		
			updateTicketListingCrawlRow(crawlId);
		});
	};
	
	function toggleTicketListingCrawlRow(crawlId) {
		$('#ticket-listing-crawl-' + crawlId).attr('checked', !$('#ticket-listing-crawl-' + crawlId).attr('checked'));
		updateTicketListingCrawlRow(crawlId);
	};	
  
  	function updateTicketListingCrawlRow(crawlId) {
		if ($('#ticket-listing-crawl-' + crawlId).attr('checked')) {
			$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', '#748C20');
			$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', '#ffffff');
		} else {
			$('#ticket-listing-crawl-' + crawlId).parent().parent().css('background-color', null);
			$('#ticket-listing-crawl-' + crawlId).parent().parent().find('*').css('color', null);
		}
	};
  
	function enableSelectedTicketListingCrawls() {
		var ids = __getSelectedTicketListingCrawlIds();
		if (ids == null) {
			alert("No crawl selected");
			return;
		}
		document.location.href="EditorManageCrawls?action=enableCrawl&crawlIds=" + ids + "&backUrl=" + escape("EditorAutoCrawlerCreation?artistId=${event.artistId}&eventId=${event.id}");
	};

	function disableSelectedTicketListingCrawls() {
		var ids = __getSelectedTicketListingCrawlIds();
		if (ids == null) {
			alert("No crawl selected");
			return;
		}
		document.location.href="EditorManageCrawls?action=disableCrawl&crawlIds=" + ids + "&backUrl=" + escape("EditorAutoCrawlerCreation?artistId=${event.artistId}&eventId=${event.id}");
	};
	
	function forceRecrawlSelectedTicketListingCrawls() {
		var ids = __getSelectedTicketListingCrawlIds();
		if (ids == null) {
			alert("No crawl selected");
			return;
		}
		document.location.href = "EditorManageCrawls?action=forceRecrawl&crawlIds=" + ids + "&backUrl=" + escape("EditorAutoCrawlerCreation?artistId=${event.artistId}&eventId=${event.id}");
	};

	function deleteSelectedTicketListingCrawls() {
		var ids = __getSelectedTicketListingCrawlIds();
		if (ids == null) {
			alert("No crawl selected");
			return;
		}
		
		var idCount = ids.split(",").length;
		var r = confirm("Do you really want to delete the " + idCount  + " selected crawl(s)?\nAll the tickets associated to these crawls will be deleted as well.");
		if (r) {
			document.location.href = "EditorManageCrawls?action=removeCrawl&crawlIds=" + ids + "&backUrl=" + escape("EditorAutoCrawlerCreation?artistId=${event.artistId}&eventId=${event.id}");
		}
	};
	
	function changeAddCrawler(siteId) {
		var selectedEvent;
		var artistId = $('#artistId').val();
		if (siteId) {
			if($('#selectedEventId').val().trim()==''){
				alert("Select a sinlge event..");
				return false;
			}else{
				var selectedEventIds = $('#selectedEventId').val().split(',');
				if(selectedEventIds.length==2){
					selectedEvent=selectedEventIds[1];
				}else{
					alert("Select a sinlge event..");
					return false;
				}
			}
		
		document.location.href = "EditorEditTicketListingCrawl?action=create&artistId="+artistId+"&eventId="+selectedEvent+"&siteId=" + siteId + "&backUrl=" + escape("EditorAutoCrawlerCreation?artistId="+artistId+"&eventId="+ selectedEvent);
		}
	};
	
	function is_int(value){ 
		if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
			return true;
		} else { 
			return false;
		} 
	}
	function getExistingTMATEventDetails(crawlId) {
		var url = "GetTicketListingCrawlPopup?id="+crawlId;
		newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
		if (window.focus) {newwindow.focus()}
		return false;	
	}
  
</script>
