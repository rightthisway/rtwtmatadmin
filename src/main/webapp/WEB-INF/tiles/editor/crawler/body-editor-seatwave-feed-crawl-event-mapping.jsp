<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; SeatWave Feed event mapping
</div>

<h1>SeatWave Feed event mapping</h1>

<div class="info">
	<div class="infoText">
		This page purpose is to check that the event mapping done on seatwave feed is correct.<br />
	</div>
</div>

<c:if test="${not empty errorMessage}">
	<div class="error">
		<div class="errorText">
			${errorMessage}
		</div>
	</div>
</c:if>

<display:table class="list" name="${eventMappings}" id="eventMapping" requestURI="EditorSeatWaveFeedCrawlEventMapping">

    <display:column title="SeatWave Event Name" sortable="true" property="seatWaveEventName" />
    <display:column title="SeatWave Event Date" sortable="true"><fmt:formatDate pattern="MM/dd/yyyy HH:mm" value="${eventMapping.seatWaveEventDate}" /></display:column>
    <display:column title="TMAT Tour Name" sortable="true" property="event.artist.name" />
    <display:column title="TMAT Event Name" sortable="true" property="event.name" />
    <display:column title="TMAT Event Date" sortable="true"><fmt:formatDate pattern="MM/dd/yyyy HH:mm" value="${eventMapping.event.date}" /></display:column>
</display:table>
