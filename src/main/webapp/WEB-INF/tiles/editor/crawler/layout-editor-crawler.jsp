<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="editorSubMenu">
	<%-- <c:choose>
	  <c:when test="${selectedSubMenu == 'Tour Crawls'}">
   	    <b>Tour Crawls</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="EditorTourCrawls">Tour Crawls</a>
	  </c:otherwise>
	</c:choose>
	| --%>	
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Crawler Controls'}">
   	    <b>Crawler Controls</b>
	  </c:when>
	  <c:otherwise>
   	    <a href="EditorCrawlerControls">Crawler Controls</a>
	  </c:otherwise>
	</c:choose>
	|	
	
 
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Crawl Errors'}">
   	    <b>Crawl Errors</b>
	  </c:when>
	  <c:otherwise>
     	<a href="EditorCrawlErrors">Crawl Errors</a>
	  </c:otherwise>
	</c:choose>
	|	
	<c:choose>
	  <c:when test="${selectedSubMenu == 'Crawler Site Settings'}">
   	    <b>Crawler Site Settings</b>
	  </c:when>
	  <c:otherwise>
     	<a href="EditorEditCrawlerSites">Crawler Site Settings</a>
	  </c:otherwise>
	</c:choose>
	
</div>

<tiles:insertAttribute name="subBody" />