<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; Edit merchant settings
</div>

<c:if test="${not empty param.info}">
	<div id="info" class="info">
		<div class="infoText">${param.info}</div>
	</div>
</c:if>

<h1>Merchant settings</h1>
This is the maximum number of concurrent connections for each machine doing the crawling.<br/><br/>
<form action="EditorEditCrawlerSites" onsubmit="submitForm()" method="post">
	<input type="hidden" name="backUrl" value="${param.backUrl}" />
	<input type="hidden" name="action" value="update" />
	<c:set var="rowCount" value="0" />
	<table>
		<c:forEach var="siteId" items="${constants.siteIds}">
			<tr>
				<td><b><img src="../images/ico-${siteId}.gif" title="${crawl.siteId}" align="absbottom" /> ${siteId}</b></td>
				<td>
					Max concurrent connections:
					<select name="maxConcurrentConnections_${siteId}">
						<c:forEach var="c" begin="1" end="20">
							<option value="${c}" <c:if test="${siteById[siteId].maxConcurrentConnections == c}">selected</c:if> >${c}</option>
						</c:forEach>
					</select>					
				</td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<div id="buttonContainer">
		<input type="submit" name="submit" value="Save Changes" class="medButton" />
		<c:choose>
			<c:when test="${empty param.backUrl}">
				<c:set var="cancelUrl" value="EditorTCAP" />
			</c:when>
			<c:otherwise>
				<c:set var="cancelUrl" value="${param.backUrl}" />
			</c:otherwise>
		</c:choose>
		<input type="button" value="Cancel" class="medButton" onclick="document.location.href='${cancelUrl}'" />
	</div>
	<span id="loading" style="color:red;font-weight:bold;display:none">
		<img src="../images/process-running.gif" align="absbottom" /> Saving...		
		Please wait, this may take a while.
	</span>

</form>

<script type="text/javascript">
	function submitForm() {
		$('#buttonContainer').hide();
		$('#loading').show();
	};
</script>