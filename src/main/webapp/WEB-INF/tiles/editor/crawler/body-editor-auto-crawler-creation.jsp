<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
	function selectAllEvents() {
			$.each($('.eventCheckBox'),  function(i,elt) {
				$(this).attr('checked', true);	
				changeDate();
			});
		};

		function unselectAllEvents() {
			$.each($('.eventCheckBox'),  function(i,elt) {
				$(this).attr('checked', false);			
				changeDate();
			});
		};
	function changeAndsubmitForm(makeBlank){
		/* $('#'+makeBlank).val(''); */
		$('#tourEventSelector').submit();
	}
	
	$(document).ready(function(){
		$('#autoArtist').autocomplete("AutoCompleteArtist", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				return row[1];
			}
		}).result(function (event,row,formatted){
			$('#autoArtist').val(row[1]);
			$("#artistId").val(row[0]);
			$('#tourEventSelector').submit();
		});
		
		$('#autoVenue').autocomplete("AutoCompleteVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				return row[1];
			}
			}).result(function (event,row,formatted){
				$('#autoVenue').val(row[1]);
				$("#venueId").val(row[0]);
				$('#tourEventSelector').submit();
			});
		
	});

	function callChangeArtist(){ 
		$('#artistLabelSpanId').hide();
		$('#artistSelectSpanId').show();
		$('#tempArtistId').val('');
	}

	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#tempVenueId').val('');
	}
	
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Automatic Crawler Creation
</div>

<h1>Automatic Crawler Creation</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<c:if test="${not empty error}">
	<div class="error">
		<div class="errorText">
			${error}
		</div>
	</div>
</c:if>

<form id="tourEventSelector" action="EditorAutoCrawlerCreation" method="POST">

<input type="hidden" id="artistId" name="artistId" value="${artistId }"/>

<b>Artist:</b>

				<span id="artistLabelSpanId"  <c:if test="${not empty artistId }">style="display: black;"</c:if> 
					<c:if test="${empty artistId }">style="display: none;"</c:if>  >
					<span id="artistNameDiv" >
						<c:choose>
							<c:when test="${not empty artistName }">
								${artistName}
							</c:when>
							<c:otherwise>
									-
							</c:otherwise>
						
						</c:choose>
					</span>	
						<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
					</span>
					
					
					<span id="artistSelectSpanId" <c:if test="${not empty artistId }">style="display: none;"</c:if> 
						<c:if test="${empty artistId }">style="display: black;"</c:if> >
					
						<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>

<br/>
<b>AND</b>
<br/>
<b>Venue:</b>

				<c:choose>
					<c:when test="${(empty artistId || artistId == 0) && empty venues}">
						<span id="venueLabelSpanId"  <c:if test="${not empty venueId && venueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty venueId || venueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName }">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty venueId && venueId != 0 }">style="display: none;"</c:if> 
				<c:if test="${empty venueId || venueId == 0 }">style="display: block;"</c:if> >
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
						<input type="hidden" id="venueId" name="venueId" value="${venueId }"/>
					</c:when>
					<c:otherwise>
						<select id="venueId" name="venueId" onchange="changeAndsubmitForm('artistId');">
							<option value="">Select</option>
							<c:forEach var="venue" items="${venues}">
								<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == venueId}">selected</c:if>>${venue.building}</option>
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>	
<br/>
<br/>
<a href="javascript:void(0)" onclick="selectAllEvents(); return false">All</a> -
<a href="javascript:void(0)" onclick="unselectAllEvents(); return false">None</a>	
<b>Event:</b>
<c:choose>
  <c:when test="${not empty events}">
  <!--
	<select name="eventId" onchange="$('#tourEventSelector').submit()">
	  <c:forEach var="event" items="${events}">
	    <option value="${event.id}" <c:if test="${param.eventId == event.id}">selected</c:if>>
	    ${event.name}
	    - ${event.formattedVenueDescription}
	    - ${event.formattedEventDate}    
	    </option>
	  </c:forEach>
	</select>
	<img src="../images/ico-${crawl.siteId}.gif"/>
	-->
	<div id="group">
		<display:table class="list" name="${events}" id="event" requestURI="EditorAutoCrawlerCreation">
		  <display:column style="width:60px">
			<label for="event-${event.id}"><input type="checkbox" class="eventCheckBox" onclick ="changeDate(this)" id="event-${event.id}"  
				<c:forEach var="item" items="${eventIdList}">
					<c:if test="${item eq event.id}">checked="checked"</c:if>
				</c:forEach>  /> 
			</label>
			<input type="hidden" value="${event.formattedEventDate}" id="eventDate-${event.id}"/>
			
			<c:set var="enabledCrawl" value="" />
			<c:forEach var="site" items="${event.enabledCrawlSites}">
			 <c:set var="enabledCrawl" value="${enabledCrawl},${site}" />
			</c:forEach>
			<c:if test="${not empty enabledCrawl}" >
				<c:set var="enabledCrawl" value="${fn:substring(enabledCrawl,1,fn:length(enabledCrawl))}" />
			</c:if>
			<input type="hidden" value="${enabledCrawl}" id="enabledCrawl-${event.id}"/>
			
		  </display:column>
		  <display:column sortable="true" title="Name">
			${event.name}
			<input type="hidden" value="${event.name}" id="eventName-${event.id}"/>
			<input type="hidden" value="${event.formattedVenueDescription}" id="venueName-${event.id}"/>
		  </display:column>
		  <display:column sortable="true" title="Venue" property="formattedVenueDescription" />
		  
		  <display:column sortable="true" title="Event" property="formattedEventDate"/>
		  <display:column title="Enabled Crawls" sortable="true" >
		  	<c:set var="ecFlag" value="true" />
			<c:forEach var="site" items="${event.enabledCrawlSites}">
			  <img src="../images/ico-${site}.gif" tooltip="${site}" align="absbottom"/>
			</c:forEach>
		  </display:column>
		  <display:column title="Disabled Crawls" sortable="true" >
			<c:forEach var="site" items="${event.disabledCrawlSites}">
			  <img src="../images/ico-${site}.gif" tooltip="${site}" align="absbottom"/>
			</c:forEach>
		  </display:column>
		</display:table>
	</div>
  </c:when>
  <c:otherwise>
  <b>NO ACTIVE EVENT IN THE TOUR</b>
  </c:otherwise>
</c:choose>
</form>

<c:if test="${not empty events}">
  <jsp:include page="/WEB-INF/tiles/editor/crawler/body-editor-auto-crawler-creation-body.jsp"/>
</c:if>
