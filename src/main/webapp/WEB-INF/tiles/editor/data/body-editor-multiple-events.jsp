<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorManageArtists">${artist.id}</a> 
	&gt; ${artist.name}
</div>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
	<br /><br />
</c:if>

<h1>Manage events</h1>

<a href="EditorEditEvent?action=create&artistId=${param.artistId}"><img src="../images/ico-plus.gif" align="absbottom" /> Add new event</a>
<br /><br />


<form action="EditorEditMultipleEvents" method="post" id="editorEditMultipleEvents" name="editorEditMultipleEvents" onSubmit="return submitForm();">
<font color="#850001"><b>Note : If checkbox is checked ,That field of all selected events will get affected</b></font>
<br /><br />
	<table align="center">
		<tr>
			<td>
				<input type="checkbox" name="name-checkbox" id="name-checkbox" >
			</td>
			<td>
				<b>Name:</b>
			</td>
			<td  style="width:300px">
				<input type="text" name="name" id="name" style="width:250px"	 onchange="autoSelectName()">
				
			</td>
			<td style="width:300px">
				<label id="lblName"><font color="red">Name Can not be blank. Unselect check box, if you dont want to change it.</font></label>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" name="date-checkbox" id="date-checkbox">
			</td>
			<td>
				<b>Date (YYYY/MM/DD)</b>
			</td>
			<td>
				<input type="radio" id="dateTbd1" name="dateTbd" value="true" onclick="autoSelectDate()"/> TBD &nbsp; <b>or</b> &nbsp;
				<input type="radio" id="dateTbd2"  name="dateTbd" value="false" onclick="autoSelectDate()" />					
				<select name="eventDateYear" id="eventDateYear">
				  <c:forEach var="year" begin="2009" end="2020" step="1">
				    <option value="${year}" <c:if test="${year == eventYear}">selected</c:if>>${year}</option>
				  </c:forEach>
				</select> /
				<select name="eventDateMonth" id="eventDateMonth">
				  <c:forEach var="month" begin="1" end="12" step="1">
				    <option value="${month}" <c:if test="${month == eventMonth}">selected</c:if>><fmt:formatNumber pattern="00" value="${month}" /></option>
				  </c:forEach>
				</select> /
				<select name="eventDateDay" id="eventDateDay">
				  <c:forEach var="day" begin="1" end="31" step="1">
				    <option value="${day}" <c:if test="${day == eventDay}">selected</c:if>><fmt:formatNumber pattern="00" value="${day}" /></option>
				  </c:forEach>
				</select>
				<input type="text" id="localDateCalendar" name="localDateCalendar" style="visibility:hidden;width:0px;" />
				
			</td>
			<td style="width:300px">
				<label id="lblDate"><font color="red">Date Can not be past date. if you dont want to change it unselect check box.</font></label>
				<label id="lblDate1"><font color="red">If you dont want to change date unselect check box.</font></label>
				&nbsp;
			</td>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td>
				<input type="checkbox" name="time-checkbox" id="time-checkbox">
			</td>
			<td>
				<b>Time (HH:MM)</b>
			</td>
			<td>
				<input type="radio" id="timeTbd1" name="timeTbd" value="true" onclick="autoSelectTime()"/>TBD: &nbsp; <b>or</b> &nbsp;
			    <input type="radio" id="timeTbd2" name="timeTbd" value="false" onclick="autoSelectTime()" />			    			
			
				<select id="localTimeHours" name="localTimeHours">
				  <c:forEach var="hour" begin="0" end="23" step="1">
				    <option value="${hour}"><fmt:formatNumber pattern="00" value="${hour}" /></option>
				  </c:forEach>
				</select>
				:
				<select id="localTimeMinutes" name="localTimeMinutes">
				  <c:forEach var="minute" begin="0" end="59" step="1">
				    <option value="${minute}"><fmt:formatNumber pattern="00" value="${minute}" /></option>
				  </c:forEach>
				</select>
										
				<!--<form:errors id="date" cssClass="error" />-->
  			</td>
			<td style="width:300px">
				<label id="lblTime"><font color="red">Time is not selected. if you dont want to change it unselect check box.</font></label>
				&nbsp;
			</td>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td>
				<input type="checkbox" name="venue-checkbox" id="venue-checkbox">
			</td>
			<td>
				<b>Venue</b>
			</td>
			<td>
				<select id="venueId" name="venueId" style="width:300px" onchange="getVenueCategory(this.value);">
					<option value="">SELECT</option>
					<c:forEach var="venue" items="${venues}">
						<option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</option>
					</c:forEach>
				</select>
				<!--<form:errors id="venue" cssClass="error" />-->
			</td>
			<td style="width:300px">
				<label id="lblVenue"><font color="red">Please select venue. if you dont want to change it unselect check box.</font></label>
				&nbsp;
			</td>
			
		</tr>
		
		<tr>
			<td>
				<input type="checkbox" name="venueCategory-checkbox" id="venueCategory-checkbox">
			</td>
			<td>
				<b>Venue Category</b>
			</td>
			<td>
				<select name="selectedVenueCategory" id="selectedVenueCategory" class="venueCategory" style="width:300px">
					<option value="">SELECT</option>
					<option value="NoCategory">NO VENUE CATEGORY</option>
				</select>
			</td>
			<td style="width:300px">
				<label id="lblVenueCategory"><font color="red">Please select venue. if you dont want to change it unselect check box.</font></label>
				&nbsp;
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" name="artist-checkbox" id="artist-checkbox">
			</td>
			<td>
				<b>Artist</b>
			</td>
			<td>
				<select id="selectedartistId" style="width:300px">
					<option value="">SELECT</option>
					<c:forEach var="artist" items="${artists}">
						<option value="${artist.id}">${artist.name}</option>
					</c:forEach>
				</select>
				<!--<form:errors id="venue" cssClass="error" />-->
			</td>
			<td style="width:300px">
				<label id="lblArtist"><font color="red">Please select tour. if you dont want to change it unselect check box.</font></label>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" name="type-checkbox" id="type-checkbox">
			</td>
			<td>
				<b>Type:</b>
			</td>
		  <td>
			<select id="eventType"  name="eventType">
				<option value="">SELECT</option>
				<option value="SPORT">SPORT/CONCERT</option>
				<option value="THEATER">THEATER</option>
				<!-- <option value="CONCERT">CONCERT</option> -->
				<option value="OTHER">OTHER</option>
			</select>
		  </td>
		  <td style="width:300px">
				<label id="lblEventType"><font color="red">Please select tour. if you dont want to change it unselect check box.</font></label>
				&nbsp;
			</td>
		</tr>
		<tr align="center">
		  <td colspan="3">
			<input type="submit" value="Submit" class="medButton" >
		  </td>
		  
		</tr>
	</table>
	
	<input type="hidden" id="artistId" name = "artistId" value="${artist.id}"/>
	<input type="hidden" id="action" name = "action" />
	<display:table class="list" name="${events}" id="event" requestURI="EditorEditMultipleEvents" defaultsort="2">
		<display:column title='<input type="checkbox" name="title-id" id="title-id" >' >
			<input type="checkbox" name="${event.id}-checkbox" id="${event.id}-checkbox" class="checkbox-id" onchange="checkId()">
		</display:column>
		<display:column title='AutoCorrect'>
			<c:choose>
					<c:when test="${event.autoCorrect}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
			</c:choose>
			
		</display:column>
		<display:column title='IgnoreTBDTime' >
			<c:choose>
					<c:when test="${event.ignoreTBDTime}">Yes</c:when>
					<c:otherwise>No</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="Name" sortable="true" sortProperty="name">
		  <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" title="event.eventType" align="absbottom">${event.name}
		</display:column>
		<display:column title="Date" sortable="true">
			<b>
			<c:choose>
				<c:when test="${not empty event.date}">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
				</c:when>
				<c:otherwise>
					TBD
				</c:otherwise>
			</c:choose>
			</b>
		</display:column>
		<display:column title="Time" sortable="true">
			<b>
				<c:choose>
				  <c:when test="${not empty event.time}">
					<fmt:formatDate pattern="HH:mm" value="${event.time}" />
				  </c:when>
				  <c:otherwise>
					TBD
				  </c:otherwise>
				</c:choose>	
				
			</b>
		</display:column>
		<display:column title="Venue" sortable="true" property="venue.building" />
		<display:column title="State" sortable="true" property="venue.state" />
		<display:column title="Country">
			<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
				<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
			</c:if>
		</display:column>
		<display:column title="Crawl Count" sortable="true" sortProperty="crawlCount" class="text-align-center" property="crawlCount" />
		<display:column>
			<a href="EditorEditEvent?id=${event.id}&action=edit&multiEveUrl=EditorEditMultipleEvents?artistId=${event.artistId}"><img src="../images/ico-edit.gif" align="absbottom" />  edit</a> - 
			<a href="javascript:confirmRemove(${event.id})"><img src="../images/ico-delete.gif" align="absbottom" /> remove</a>
		</display:column>    
	</display:table>
</form>
<c:if test="${not empty events}">
	<br />
	<a href="EditorEditEvent?action=create&artistId=${param.artistId}"><img src="../images/ico-plus.gif" align="absbottom" /> Add new event</a>
</c:if>

<br /><br />
<a href="EditorManageArtists">Back to manage Artist page</a>

<div id="deleteEventDialog" title="" style="display:none">
	<span id="deleteEventDialogMessage" align="center"></span>
</div>

<script type="text/javascript">
	function submitForm(){
		var result=false;
		var isValidName=false;
		var isValidDate=false;
		var isValidTime=false;
		var isValidVenue=false;
		var isValidArtist=false;
		var isValidType=false;
		var count=0;
		$('#lblName').hide();
		$('#lblDate').hide();
		$('#lblDate1').hide();
		$('#lblTime').hide();
		$('#lblVenue').hide();
		$('#lblVenueCategory').hide();
		$('#lblArtist').hide();
		$('#lblEventType').hide();
		$('.checkbox-id').each(function(i, obj) {
			if($(obj).is(':checked')){
				result=true;
				return;
			}
		});
		if(!result){
			alert('Select an event first.');
			result=false;
		}
		if($("#name-checkbox").is(':checked')){
			if($("#name").val()==''){
				//alert('Name Can not be blank. if you dont want to change it unselect check box.');
				$('#lblName').show();
				result=false;
				isValidName=false;
			}else{
				isValidName=true;
			}
			count=count+1;
		}else{
			isValidName=true;
		}
		if($("#date-checkbox").is(':checked')){
			if($("#dateTbd1").is(':checked')){
				isValidDate=true;
			}else if($("#dateTbd2").is(':checked')){
				if($("#localDateCalendar").val()==''){
					//alert('Date Can not be past date. if you dont want to change it unselect check box.');
					$('#lblDate').show();
					result=false;
					isValidDate=false;
				}else{
					isValidDate=true;
				}
			}else{
				//alert('If you dont want to change date unselect check box.');
				$('#lblDate1').show();
				isValidDate=false;
			}
			count=count+1;
		}else{
			isValidDate=true;
		}
		if($("#time-checkbox").is(':checked')){
			isValidTime=false;
			if($("#timeTbd1").is(':checked') || $("#timeTbd2").is(':checked')){
				isValidTime=true;
			}else{
				//alert('Time is not selected. if you dont want to change it unselect check box.');
				$('#lblTime').show();
				result=false;
				isValidTime=false;
			}
			count=count+1;
		}else{
			isValidTime=true;
		}
		if($("#venue-checkbox").is(':checked')){
			if($("#venueId").val()==''){
				//alert('Please select venue. if you dont want to change it unselect check box.');
				$('#lblVenue').show();
				result=false;
				isValidVenue=false;
			}else{
				isValidVenue=true;
			}
			count=count+1;
		}else{
			isValidVenue=true;
		}
		if($("#venueCategory-checkbox").is(':checked')){
			if($("#selectedVenueCategory").val()==''){
				//alert('Please select venue. if you dont want to change it unselect check box.');
				$('#lblVenueCategory').hide();
				result=false;
				isValidVenueCategory=false;
			}else{
				isValidVenueCategory=true;
			}
			count=count+1;
		}else{
			isValidVenueCategory=true;
		}
		if($("#artist-checkbox").is(':checked')){
			if($("#selectedartistId").val()==''){
				//alert('Please select tour. if you dont want to change it unselect check box.');
				$('#lblArtist').show();
				result=false;
				isValidArtist=false;
			}else{
				isValidArtist=true;
			}
			count=count+1;
		}else{
			isValidArtist=true;
		}
		if($("#type-checkbox").is(':checked')){
			if($("#eventType").val()==''){
				$('#lblEventType').show();
				result=false;
				isValidType=false;
			}else{
				isValidType=true;
			}
			count=count+1;
		}else{
			isValidType=true;
		}
		if(count==0){
			alert('Please select any field to change.');
			return false;
		}else if(isValidName && isValidDate && isValidTime && isValidVenue && isValidArtist && isValidType && result){
			$("#action").val('edit');
			return true;
		}else{
			return false;
		}
	}
	function checkId(){
		var flag=true;
		$('.checkbox-id').each(function(i, obj) {
			if(!$(obj).is(':checked')){
				flag=false;
				return;
			}
		});
		$("#title-id").attr('checked', flag);
	}
	
	function autoSelectDate(){
		$("#date-checkbox").attr('checked', true);
	}
	function autoSelectName(){
		if($("#name").val()!=''){
			$("#name-checkbox").attr('checked', true);
			//alert('in');
		}else{
			$("#name-checkbox").attr('checked', false);
		}
	}
	/***************/
	function autoSelectTime(){
		$("#time-checkbox").attr('checked', true);
	}
	
	$("#time-checkbox").click(function(){
		if(!$("#time-checkbox").is(':checked')){
			$("#timeTbd1").attr('checked', false);
			$("#timeTbd2").attr('checked', false);
		}
	});
	$("#localTimeHours").change(function(){
		$("#timeTbd2").attr('checked', true);
		$("#time-checkbox").attr('checked', true);
	});
	$("#localTimeMinutes").change(function(){
		$("#timeTbd2").attr('checked', true);
		$("#time-checkbox").attr('checked', true);
	});
	/****************/
	$("#venueId").change(function(){
		if($("#venueId").val()!=''){
			$("#venue-checkbox").attr('checked', true);
		}else{
			$("#venue-checkbox").attr('checked', false);
		}
	});
	$("#selectedVenueCategory").change(function(){
		if($("#selectedVenueCategory").val()!=''){
			$("#venueCategory-checkbox").attr('checked', true);
		}else{
			$("#venueCategory-checkbox").attr('checked', false);
		}
	});
	$("#selectedartistId").change(function(){
		if($("#selectedartistId").val()!=''){
			$("#artist-checkbox").attr('checked', true);
		}else{
			$("#artist-checkbox").attr('checked', false);
		}
	});
	
	$("#venue-checkbox").click(function(){
		if(!$("#venue-checkbox").is(':checked')){
			$("#venueId").val('');
		}
	});
	
	
	$("#eventType").change(function(){
		if($("#eventType").val()!=''){
			$("#type-checkbox").attr('checked', true);
		}else{
			$("#type-checkbox").attr('checked', false);
		}
	});
		$("#selectedartistId").change(function(){
		  if($("#selectedartistId").val()!=''){
		   $("#artist-checkbox").attr('checked', true);
		  }else{
		   $("#artist-checkbox").attr('checked', false);
		  }
	});
	
	
	$("#type-checkbox").click(function(){
		if(!$("#type-checkbox").is(':checked')){
			$("#eventType").val('');
		}
	});
	
	
	$("#date-checkbox").click(function(){
		if(!$("#date-checkbox").is(':checked')){
			$("#dateTbd1").attr('checked', false);
			$("#dateTbd2").attr('checked', false);
		}
	});
	$("#eventDateDay").change(function(){
		$("#dateTbd2").attr('checked', true);
		$("#date-checkbox").attr('checked', true);
	});
	$("#eventDateMonth").change(function(){
		$("#dateTbd2").attr('checked', true);
		$("#date-checkbox").attr('checked', true);
	});
	$("#eventDateYear").change(function(){
		$("#dateTbd2").attr('checked', true);
		$("#date-checkbox").attr('checked', true);
	});
	$("#title-id").click(function(){
		var flag = false;
		if($("#title-id").is(':checked')){
			flag=true;
		}
		
		$('.checkbox-id').each(function(i, obj) {
			$(obj).attr('checked', flag);
		});
	});
	
	function confirmRemove(eventId) {
		$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Please wait');		
		$('#deleteEventDialog').dialog('open');
		$('#deleteEventDialog').dialog('option', 'title', 'Please wait...');
		$('#deleteEventDialog').dialog('option', 'buttons', {});

        DataDwr.getNumTickets(eventId, function(response) {
			$('#deleteEventDialog').dialog('option', 'title', 'Delete event?');
        	$('#deleteEventDialogMessage').html(
        		"The event has " + response + " tickets attached to it. "
        		+ "<br /> If you delete it, all the associated tickets will be deleted as well. "
        		+ "<br /><br />Do you really want to delete it?");

			$("#deleteEventDialog").dialog('open');
			$("#deleteEventDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorDetailArtist?id=" + eventId + "&artistId=${param.artistId}&action=remove";			
					$('#deleteEventDialog').dialog('option', 'title', 'Deleting event...');
					$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting event...');		
					$("#deleteEventDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
		
	};	
	
	$(document).ready(function() {
		$('#lblName').hide();
		$('#lblDate').hide();
		$('#lblDate1').hide();
		$('#lblTime').hide();
		$('#lblVenue').hide();
		$('#lblVenueCategory').hide();
		$('#lblArtist').hide();
		$('#lblEventType').hide();
		$('#localDateCalendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime()),
	        maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000),
	        beforeShow: function(input) {
	        	$('#localDateCalendar').val(($('#eventDateYear').val()) + "-" + $('#eventDateMonth').val() + "-" + $('#eventDateDay').val());
	        },
	        onSelect: function(dateText) {
				var tokens = dateText.split("-");
				$('#eventDateYear').val(tokens[0]);
				$('#eventDateMonth').val(tokens[1]);
				$('#eventDateDay').val(tokens[2]);
				$("#dateTbd2").attr('checked', true);
				$("#date-checkbox").attr('checked', true);
			}
		});
	});
	function getVenueCategory(venueId){
		$.ajax({
			url:"GetVenueCategoryByVenue?venueId=" + venueId,
			success:function(result){
				var cats = result.split(",")
				var options = "<option value ='0'>--Select--</option>";
				for(i=0;i<cats.length-1;i++){
					var val = cats[i].split("-");
					options = options + "<option value ='" + val[1]+ "'>" + val[0] + "</option>";
				}
				$("#selectedVenueCategory").html(options);
				$("#venueCategory-checkbox").attr('checked', false);
			}
		});
	}
	
</script>