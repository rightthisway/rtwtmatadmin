<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Valuation Factors
</div>

<div id="quickLinks">
 	<a href="BrowseTickets?eventId=${eventId}">Go to Event</a>
</div>
<div style="clear:both"></div>

<c:if test="${not empty param.error}">
  <div class="error">
    <div class="errorText">
      ${param.error}
    </div>
  </div>
</c:if>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<h1>Valuation Factors</h1>
<b>Tour:</b>
<select name="tourId" onchange="location.href='EditorValuationFactors?tourId=' + $(this).val()">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}" <c:if test="${tour.id == tourId}">selected</c:if>>${tour.name}</option>
  </c:forEach>
</select>

<b>Event:</b>
<select name="eventId" onchange="location.href='EditorValuationFactors?tourId=${tourId}&eventId=' + $(this).val()">
  <c:forEach var="event" items="${events}">
    <option value="${event.id}" <c:if test="${event.id == eventId}">selected</c:if>>
      ${event.name}
      - 
      <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />
  	  <c:choose>
	    <c:when test="${not empty event.time}">
	      <fmt:formatDate pattern="HH:mm" value="${event.time}" />
	    </c:when>
	    <c:otherwise>
	      TBD
	    </c:otherwise>
	 </c:choose>		
	</option>
  </c:forEach>
</select>
<br/>
<b>Venue:</b> ${event.venue.building}
<h2>Valuation Factors</h2>
<display:table class="list" name="${valuationFactors}" id="valuationFactor" requestURI="EditorValuationFactors?tourId=${tourId}&eventId=${eventId}" pagesize="20">
  <display:column title="Start Section" property="startSection" sortable="true"/>
  <display:column title="End Section" property="endSection" sortable="true" />
  <display:column title="Start Row" property="startRow" sortable="true" />
  <display:column title="End Row" property="endRow" sortable="true" />
  <display:column title="Factor" property="value" sortable="true" />
</display:table>
<c:if test="${not empty valuationFactors}">
    <br/>
	<img src="../images/ico-delete.gif" align="absbottom"/> <a href="EditorValuationFactors?tourId=${tourId}&eventId=${eventId}&action=deleteAll" onclick="return confirmDeleteAll()">Delete All the Valuation Factors for this tour</a>
</c:if>	

<div id="container-tabs" style="margin-top: 5px">
  <ul>
    <li><a href="#fragment-complete-upload">Evaluation Factor Upload</a></li>
    <li><a href="#fragment-super-upload">Super Evaluation Factor Upload</a></li>
    <li><a href="#fragment-help">Help</a></li>
  </ul>
  <div id="fragment-complete-upload">
	<h2>Valuation Factor Upload</h2>
	<div class="info">
	  <div class="infoText">
	    The CSV file should be in the following format:<br/>
	    date, venue, section, row, value<br/><br/>
	    section, row can be: 45, 56, 50-52<br/>
	    date format is: MM/dd/YYYY. You can also define a range MM/dd/YYYY-MM/dd/YYYY 
	    <br/><br/>
	    For instance:
	    01/03/2010,Giants Stadium,102-105,40-43,47<br/>
	    07/13/2009-01/03/2010,Giants Stadium,103,40,45<br/>
	    <b><img src="../images/ico-warning.gif" align="absbottom"/>When you upload a file, we will first delete all the valuation factors for the tour.</b>
	  </div>
	</div>
	<form enctype="multipart/form-data" action="EditorUploadValuationFactors" method="POST">
	    <input type="hidden" name="tourId" value="${tourId}"/>
	    <input type="hidden" name="eventId" value="${eventId}"/>
	    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
		<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file"/>
		<input type="submit" name="submit" value="Upload" class="medButton"/>
	</form>
  </div>
  <div id="fragment-super-upload">
    <h2>Super Valuation Factor Upload</h2>
	<div class="info">
		<div class="infoText">
		  This allows to create valuation factors from simple inputs.<br/>
		  Please read the <a href="#" onclick="$('#container-tabs').tabs('select', 2); return false;">help</a> for more information.
		  <a href="../samples/rangers.txt">Download Sample file</a>.
		</div>
	</div>

	<form enctype="multipart/form-data" action="EditorUploadSuperValuationFactors" method="POST">
	    <input type="hidden" name="tourId" value="${tourId}"/>
	    <input type="hidden" name="eventId" value="${eventId}"/>
	    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
		<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file">
		<input type="submit" name="submit" value="Preview&gt;&gt;" class="medButton"/>
	</form>
  </div>
  <div id="fragment-help">
    <h2>Super Valuation Factor Upload</h2>
    Let's consider the following stadium. In this example we want to create the valuation factors <b>ONLY for the sections
    from 40 to 97</b>.<br/>
    <img src="../images/stadium-madison.gif"/><br/>
    First open a text editor and create a file named for instance <b>rangers.txt</b>.<br /><br />
    The stadium is the <b>Rangers Stadium</b> and let's say that the venues for which we want to have the valuation factors
    are from 09/05/2009 to 09/24/2009.
    
    In the file <b>rangers.txt</b> we define the stadium and the dates as follows:
    <pre style="border: solid 1px #aaaaaa">
      [VENUE]
      Rangers Stadium
      
      [DATE]
      09/05/2009-09/24/2009</pre>
        
    <h3>Mirrors</h3>
    We can see that we can split the stadium in 4 parts which can be mirrored.<br/>
    - We can see that the sections from 52 to 40 is a mirror of sections from 53 to 65,
    sections from 81 to 69 and sections from 82 to 94.<br/>
    - The sections 96 is mirrored to section 67<br/>
    - The section 97 is mirrored to section 95, 68 and 66.<br/>
    <br/>
    <i>Note:We define section ranges by starting by the section with the best valuation factor</i><br/><br/>
    In the file <b>rangers.txt</b>, we can define the mirrors as follows:
    <pre style="border: solid 1px #aaaaaa">
      [MIRRORS]
      52-40, 53-65, 81-69, 82-94
      96, 67
      97, 95, 68, 66</pre>
    <h3>Rows</h3>
    In this stadium, we consider that for any sections, the valuation factors can be ordered as follows:<br/>
    Row A is better than B-C which is better than D-F which is better than G-J which is better than K-N.<br/>
    In the file <b>rangers.txt</b>, we can define the row order as follows:
    <pre style="border: solid 1px #aaaaaa">
      [ROWS]
	  A
	  B-C
	  D-F
	  G-J
	  K-M</pre>
	  
	<h3>Home Team Sections</h3>
	In some cases, we want to give some advantage to home team sections (for instance give +2 valuation factors
	to sections in the home team area).<br/>
	In our example, the home team sections are:<br/>
	- sections from 40 to 67<br/>
	- sections from 96 to 97<br/>
	<br/>
	We want to give them an advantage of 2.
    In the file <b>rangers.txt</b>, we can define the home team sections as follows:
    <pre style="border: solid 1px #aaaaaa">
      [HOMETEAM_SECTIONS]
      40-67, 2
      96-97, 2</pre>
    
    <h3>Sections</h3>
    We define the sections ordered by best sections (best sections come first). We'll only need to define
    the sections for one quarter of the stadium in our case since the mirroring will take care of mirrored sections.
    Let's consider bottom-right quarter. we can see that the sections can be ordered as follows:<br/>
    - from 52 to 40; and<br/>
    - from 97 to 96<br/>
    <br/>
    Let's consider the section 52 and 51. In our case, we might want to consider that the row A
    on Section 51 is better than rows G-J of section 52 (overlap).
    In the file <b>rangers.txt</b>, we can define the ordered sections as follows:
    <pre style="border: solid 1px #aaaaaa">
      [SECTIONS]
      52-40, G-J, &gt;
      97-96, G-J, &gt;</pre>
    If we have for instance the sections 52-40 to be equivalent (same valuation factors), we can define them as follows:
    <pre style="border: solid 1px #aaaaaa">
      [SECTIONS]
      52-40, A, =</pre>
    That will tell that the row A on section 51 is equals to row A on section 52, then 51.B=52.B and so on.
    <h3>Misc</h3>
    To add comments in your file, you can prefix a line with <b>--</b>. For instance
    <pre style="border: solid 1px #aaaaaa">
    -- This is a comment</pre>
    You can download a sample file <a href="../samples/rangers.txt">here</a>.
  </div>
</div>

<script type="text/javascript">
  function confirmDeleteAll() {
    var answer = confirm("Are you sure you want to delete all the valuation factors for this tour?")
    if (answer) {
      return true;
    }
    
    return false;
  }
  
  $('#container-tabs').tabs();
</script>