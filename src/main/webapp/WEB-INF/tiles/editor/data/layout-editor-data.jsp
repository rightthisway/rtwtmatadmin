<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="editorSubMenu">
<%-- <c:choose>
   <c:when test="${selectedSubMenu == 'Event Bulk Upload'}">
        <b>Event Bulk Upload</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageTCAP">Event Bulk Upload</a>
   </c:otherwise>
 </c:choose>
 
 | --%>
 
 <c:choose>
   <c:when test="${selectedSubMenu == 'TCAP'}">
        <b>TCAP</b>
   </c:when>
   <c:otherwise>
        <a href="EditorTCAP">TCAP</a>
   </c:otherwise>
 </c:choose> 
 |
 <authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR">
 <c:choose>
   <c:when test="${selectedSubMenu == 'SCEC'}">
        <b>SCEC</b>
   </c:when>
   <c:otherwise>
        <a href="EditorSuperCrawlCreation">SCEC</a>
   </c:otherwise>
 </c:choose>
  
 <%-- | 
 <c:choose>
   <c:when test="${selectedSubMenu == 'View Excluded Cheapest Tickets'}">
        <b>View Excluded Cheapest Tickes</b>
   </c:when>
   <c:otherwise>
        <a href="EditorTmatInstantFiles">View Excluded Cheapest Tickets</a>
   </c:otherwise>
 </c:choose> --%>
 |
 </authz:authorize>
 <c:choose>
   <c:when test="${selectedSubMenu == 'Crawls'}">
        <b>Crawls</b>
   </c:when>
   <c:otherwise>
        <a href="EditorAutoCrawlerCreation">Crawls</a>
   </c:otherwise>
 </c:choose>
 <authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR"> 
 |
 
 <c:choose>
   <c:when test="${selectedSubMenu == 'Data Maintenance'}">
        <b>Data Maintenance</b>
   </c:when>
   <c:otherwise>
        <a href="EditorPriceManagement">Data Maintenance</a>
   </c:otherwise>
 </c:choose> 
 <%-- |
 <c:choose>
   <c:when test="${selectedSubMenu == 'PresaleEvent'}">
        <b>PreSale Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManagePresaleEvent">PreSale Event</a>
   </c:otherwise>
 </c:choose> --%>
 </authz:authorize>
</div>

<tiles:insertAttribute name="subBody" />