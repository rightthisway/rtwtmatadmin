<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubDataMaintenanceMenu" name="selectedSubDataMaintenanceMenu" scope="request" />

<div id="editorSubMenu">


<c:choose>
		<c:when test="${selectedSubDataMaintenanceMenu == 'Manage Price'}">
			<b>Manage Price</b>
		</c:when>
		<c:otherwise>
			<a href="EditorPriceManagement">Manage Price</a>
		</c:otherwise>
	</c:choose>
	|
	
	<c:choose>
		<c:when test="${selectedSubDataMaintenanceMenu == 'Default Manage Price'}">
			<b>Default Manage Price</b>
		</c:when>
		<c:otherwise>
			<a href="DefaultPriceManagement">Default Manage Price</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
	   <c:when test="${selectedSubDataMaintenanceMenu == 'Expired Events'}">
	        <b>Expired Events</b>
	   </c:when>
	   <c:otherwise>
	        <a href="EditorManageExpiredEvents">Expired Events</a>
	   </c:otherwise>
	 </c:choose>
 	|
	<%-- <c:choose>
	   <c:when test="${selectedSubDataMaintenanceMenu == 'Empty Artists'}">
	        <b>Empty Artists</b>
	   </c:when>
	   <c:otherwise>
			<a href="EditorManageEmptyArtists">Empty Artits</a>
	   </c:otherwise>
	 </c:choose>
	 | --%>
	 <c:choose>
	   <c:when test="${selectedSubDataMaintenanceMenu == 'Site Settings'}">
	        <b>Site Settings</b>
	   </c:when>
	   <c:otherwise>
	        <a href="EditorEditSites">Site Settings</a>
	   </c:otherwise>
	 </c:choose>
	 |
	 <c:choose>
	      <c:when test="${selectedSubDataMaintenanceMenu == 'Link Admitone Events'}">
	        <b>Link Admitone Events</b>
	      </c:when>
	      <c:otherwise>
	        <a href="EditorLinkAdmitone">Link Admitone Events</a>
	      </c:otherwise>
	    </c:choose> 
	 |
	 <c:choose>
	 	<c:when test="${selectedSubDataMaintenanceMenu == 'Link Admitone Dates'}">
	    	<b>Link Admitone Dates</b>
		</c:when>
	    <c:otherwise>
	    	<a href="EditorLinkDate">Link Admitone Dates</a>
	 	</c:otherwise>
	 </c:choose> 
</div>    
 <tiles:insertAttribute name="subBody1" />  
 