<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Tour Category
</div>
<c:choose>
<c:when test="${not empty info}">
<div class="info">
		<div class="infoText">
			${info}
		</div>
</div>
</c:when>
</c:choose>
<div align="center">
	
	<form:form id="childTourCategory" commandName="childTourCategory" method="post" >
		<table>
			<tr>
				<td>Select Parent Category Name:</td>
				<td>
					<form:select path="tourCategory" >
						<form:option value="NONE" label="--- Select ---"/>
   						<form:options items="${tourCategoryList}" itemLabel="name" itemValue="id"/>
					</form:select>		
					<form:errors path="tourCategory" cssClass="error" />
				</td>
			</tr>
			
			<tr>
				<td>Child Tour Category Name:</td>
				<td>
					<form:input path="name" cssClass="longInput" id="name"/>		
				</td>
			</tr>
			
			
		</table>
		<input type="submit" value="Add" name="submit" class="medButton" id="submitBtn">
		
	</form:form>
</div>
<script>
$('#submitBtn').click(function(){
	if($('#name').val() == ""){
		alert("Please Enter Child Tour Category Name");
		return false;
	}else{
		return true;
	}
});
</script>