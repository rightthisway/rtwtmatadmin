<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Category
</div>
<c:choose>
<c:when test="${not empty info}">
<div class="info">
		<div class="infoText">
			${info}
		</div>
</div>
</c:when>
</c:choose>
<div align="center">
	
	<form:form id="grandChildTourCategory" commandName="grandChildTourCategory" method="post" >
		<input type="hidden" id="tourCatId" value="" name="tourCatId" /> 
		<table>
			<tr>
				<td>Select Parent Category Name:</td>
				<td>
					<select id="tCategory" name="tourCategory">
						<option value="">--- Select ---</option>
						<c:forEach items="${tourCategoryList}" var="tCategory">
							<option Value="${tCategory.id}">${tCategory.name}</option>
						</c:forEach>	
					</select>		
					
				</td>
			</tr>
		
			
			<tr>
				<td>Select Child Category Name:</td>
				<td>
					<form:select path="childTourCategory" id="childTourCategory">
						<form:option value="NONE" label="--- Select ---"/>
   						<form:options items="${childTourCategoryList}" itemLabel="name" itemValue="id"/>
					</form:select>		
					<%-- <form:errors path="childTourCategory" cssClass="error" /> --%>
				</td>
			</tr>
			
			<tr>
				<td>Grand Child Category Name:</td>
				<td>
					<form:input path="name" cssClass="longInput" id="name"/>			
				</td>
			</tr>
			
			
		</table>
		<input id="subButton" type="submit" value="Add" name="submit" class="medButton">
		
	</form:form>
</div>
<br /><br />
<h1>Artists</h1>
<div align="center">
	<display:table class="list" name="${artists}" id="artist" requestURI="EditorEditGrandChildTourCategory" defaultsort="2" pagesize="20">
		<display:column title="Id" sortable="true" property="id" />
		<display:column title="Name" sortable="true" property ="name" >
			${tour.name}
		</display:column>
	</display:table>
</div> 
<script type="text/javascript">
	 $(document).ready(function(){	   
		var str = <%=(String)session.getAttribute("tourId")%>
				<%session.removeAttribute("tourId");%>
		if(str != null){
			$('#tCategory').val(str);
		}
		
		$('#tCategory').change(function(){
			$('#tourCatId').val($('#tCategory').val())
			$('#subButton').trigger('click');
			
	 });
	 $('#subButton').click(function(){
		
		if($('#tCategory').val() == ""){
			alert("Please Enter Tour Category Name");
			return false;
		}
		if($('#tourCatId').val() != ""){
			return true;
		}
		
		if($('#childTourCategory').val() == "NONE"){
			alert("Please Enter Child Tour Category Name");
			return false;
		}		 
		if($('#name').val() == ""){
			alert("Please Enter Grand Child Category Name");
			return false;
		}
		$('#tourCatId').val($('#tCategory').val())
		return true;
		
	 });	 
	});
</script>