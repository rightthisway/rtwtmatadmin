<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
 
<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Manage TCAP
</div>


<h1>Manage Events</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<b>Filter:</b> <br>
<b>Artist:</b>

				<span id="artistLabelSpanId"  <c:if test="${not empty editorArtistId && editorArtistId != 0 }">style="display: black;"</c:if> 
					<c:if test="${empty editorArtistId || editorArtistId == 0}">style="display: none;"</c:if>  >
					<span id="artistNameDiv" >
						<c:choose>
							<c:when test="${not empty artistName }">
								${artistName}
							</c:when>
							<c:otherwise>
									-
							</c:otherwise>
						
						</c:choose>
					</span>	
						<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
					</span>
					
					
					<span id="artistSelectSpanId" <c:if test="${not empty editorArtistId && editorArtistId != 0 }">style="display: none;"</c:if> 
						<c:if test="${empty editorArtistId || editorArtistId == 0}">style="display: black;"</c:if> >
					
						<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>
<%-- <select onchange="changeFilterArtist(this.value);">
	<option value="">Select</option>
	<option value="-1" <c:if test="${editorArtistId == -1}">selected</c:if>>View All</option>
	<c:forEach var="artist" items="${artists}">
		<option value="${artist.id}" style="height:20px;" <c:if test="${artist.id == editorArtistId}">selected</c:if>>${artist.name}</option>
	</c:forEach>
</select> --%>

<br />
AND 
<br />
<b>Venue:</b>

				<c:choose>
					<c:when test="${(empty editorVenueId || editorVenueId == 0) && empty venues}">
						<span id="venueLabelSpanId"  <c:if test="${not empty editorVenueId && editorVenueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty editorVenueId || editorVenueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName }">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty editorVenueId && editorVenueId != 0 }">style="display: none;"</c:if> 
				<c:if test="${empty editorVenueId || editorVenueId == 0 }">style="display: block;"</c:if> >
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
					</c:when>
					<c:otherwise>
						<select onchange="changeFilterVenue(this.value);">
							<option value="">Select</option>
							<c:forEach var="venue" items="${venues}">
								<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == editorVenueId}">selected</c:if>>${venue.building}</option>
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>	
				

<c:if test="${editorVenueId != null && editorVenueId != 0}">
	<c:set var="link" value="&venueId=${editorVenueId}"></c:set>
	<c:set var="link1" value="&venueId=${editorVenueId}"></c:set>
	<c:set var="emeUrlLink" value="&emeUrl=EditorEditMultipleEvent"></c:set>
</c:if>
<c:if test="${editorArtistId != null && editorArtistId != 0}">
	<c:set var="link" value="&artistId=${editorArtistId}"></c:set>
	<c:set var="artistLink" value="&artistId=${editorArtistId}"></c:set>
	<c:set var="emeUrlLink" value="&emeUrl=EditorEditMultipleEvent"></c:set>
</c:if>
<br /><br />
<form action="EditorEditMultipleEvent" method="post" onSubmit="return submitForm();">
<input type="hidden" id="artistId" name = "artistId" value="${editorArtistId}"/>
<input type="hidden" id="venueId" name = "venueId" value="${editorVenueId}"/>
<c:choose> 
<c:when test="${not empty events}">
Export to :<c:if test="${editorVenueId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=1&amp;venueId=${editorVenueId}">CSV </a></c:if>
<c:if test="${editorArtistId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=1&amp;artistId=${editorArtistId}">CSV </a></c:if> | <c:if test="${editorVenueId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=2&amp;venueId=${editorVenueId}">Excel </a></c:if>
<c:if test="${editorArtistId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=2&amp;artistId=${editorArtistId}">Excel </a></c:if> | <c:if test="${editorVenueId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=3&amp;venueId=${editorVenueId}">Xml </a></c:if>
<c:if test="${editorArtistId !=0}"><a href="EditorEditMultipleEvent?6578706f7274=1&amp;d-148316-e=3&amp;artistId=${editorArtistId}">Xml </a></c:if>
<display:table class="list" name="${events}" id="event"  requestURI="EditorEditMultipleEvent" pagesize="50" export="true">
  
	<display:setProperty name="export.excel.filename" value="TCAP.xls"/> 
	<display:setProperty name="export.csv.filename" value="TCAP.csv"/>
	<display:column title='ALL<input type="checkbox" name="title-id" id="title-id" >' >
		<input type="checkbox" name="${event.id}-checkbox" id="${event.id}-checkbox" class="checkbox-id" onchange="checkId()">
    </display:column>
	<display:column style="width:150px" title="Name" sortable="true" property="name" href="EditorEditEvent?action=edit${artistLink}${link1}${emeUrlLink}" paramId="id" paramProperty="id">
	</display:column>
	<display:column title="Date"  sortable="true"  property="formatedDate" />
	<%-- <display:column title="VenueId"  sortable="true" property="venue.id"/> --%>
	<display:column title="Venue"  sortable="true" property="venue.building" 
	href="EditorEditVenue?action=edit&${link}" paramId="id" paramProperty="venue.id"/>
	
	<display:column title="VenueCategory<br/><input type='checkbox' name='copyVenueCategory' id='copyVenueCategory' onclick='callAllVenueCate()'>" >
	  <input type="hidden" value="${event.venue.id}" id="venueIds"/>
	  <select name="${event.id}-venueCategoryId"  id="${event.id}-venueCategoryId" class="venueCategory" >
	   <option value="0" >Select</option>
	   <c:forEach var="venueCategory" items="${event.venue.venueCategoryList}" >
	    <option value="${venueCategory.id}"<c:if test="${venueCategory.id == event.venueCategoryId }">selected </c:if>>${venueCategory.categoryGroup}</option>
	   </c:forEach>
	  </select>
	</display:column>
	<%-- <display:column title="Presale Event<br/><input type='checkbox' name='copyPresaleEvent' id='copyPresaleEvent' onclick='callAllPresaleEvent();'>" >
		<input type='checkbox' name='${event.id}-presaleEvent' id='${event.id}-presaleEvent' onclick="checkPresaleEventCSV(${event.id});" class="presaleEvent"
		<c:if test ="${event.presaleEvent !=null && event.presaleEvent}"> checked="checked" </c:if>>
		<input type="hidden" value="${event.artist.grandChildTourCategory.childTourCategory.tourCategory.name}" id="${event.id}-tourCategory"/>
	</display:column> --%>
	<display:column title="Artist"  sortable="true" property="artist.name" 
	href="EditorEditArtist?action=edit&${link}" paramId="id" paramProperty="artist.id"/>
	<display:column title="Parent"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.tourCategory.name"/>
	<display:column title="Child"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.name"/>
	<display:column title="Grand Child"  sortable="true" property="artist.grandChildTourCategory.name"/>
    <display:column title="Crawl"  sortable="true"  property="crawlCount" />
	<display:column title="Zones"  sortable="true" property="uniqueCategoryGroupName" 
    href="EditorEditCategories?&${link}" paramId="artistId" paramProperty="artist.id"/>
    <display:column title='AutoCorrect'>
		<c:choose>
				<c:when test="${event.autoCorrect}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='IgnoreTBDTime' >
		<c:choose>
			<c:when test="${event.ignoreTBDTime}">Yes</c:when>
			<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title="Map" media="html" sortable="false" >	
	<c:choose> 
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}">		
	<a href="javascript:loadImage(${event.id}, '${event.venueCategory.categoryGroup}')">
	<img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" />
	</a>
	</c:when>
	<c:otherwise>
    		<font color="red">No Map</font>
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="is Map">
	<c:choose>
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}">Yes</c:when>
	<c:otherwise>No</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="Have Link " media="csv excel xml" sortable="false" property="formatedAdmitoneId" />
  <%--  <display:column title="Link" media="html" sortable="false" >
    	<c:if test="${empty event.admitoneId}">
    		<font color="red">False</font>
    	</c:if>
    	<c:if test="${not empty event.admitoneId}">
    		<font color="green">${admitoneEventNameDateTime}</font>
    	</c:if>
    </display:column>  
  <display:column title="Link"   property="admitoneEventDetails" />
 --%>
   <display:column title="LinkTo" media="html" sortable="false" >
    	<%-- <c:if test="${empty event.admitoneId || event.admitoneEventDetails }">
    		<font color="black">-</font>
    	</c:if>
    	<c:if test="${not empty event.admitoneId}">
    		<font color="black">${event.admitoneEventDetails}</font>
    	</c:if>--%>
    	<c:choose>
  <c:when test="${empty event.admitoneId}">
   <font color="black">-</font>
  </c:when>
  <c:when test="${not empty event.admitoneEventDetails}">
    <font color="black">${event.admitoneEventDetails}</font>
  </c:when>
  <c:otherwise>
    <font color="black">-</font>
  </c:otherwise>
</c:choose>
    </display:column>
    <display:column title="Price Adjustment"  sortable="false">
    	${shPercentAdjustment}
    </display:column>
<display:setProperty name="export.banner.placement" value="top" />
</display:table>
</c:when >
<c:otherwise>
There is no event for selected option.
</c:otherwise>
</c:choose>
<c:choose> 
<c:when test="${not empty events}">
<div  align="center">
    <input type="submit" value="Update">
</div>
</c:when>
</c:choose>
</form>
<script type="text/javascript">

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val(row[1]);
		changeFilterArtist(row[0]);
	});
	
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
		}).result(function (event,row,formatted){
			$('#autoVenue').val(row[1]);
			changeFilterVenue(row[0]);
		});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#artistId').val('');
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#venueId').val('');
}

function changeFilterArtist(artistId) {
	document.location.href = "EditorEditMultipleEvent?artistId=" + artistId;
	if(${editorVenueId!=null}){
		document.location.href = "EditorEditMultipleEvent?artistId=" + artistId +"&venueId="+"${editorVenueId}";
	}else{    
		document.location.href = "EditorEditMultipleEvent?artistId=" + artistId;
	}
	
};

function changeFilterVenue(venueId) {
	if(${editorArtistId!=null}){
		document.location.href = "EditorEditMultipleEvent?venueId=" + venueId +"&artistId="+"${editorArtistId}";
	}else{    
         document.location.href = "EditorEditMultipleEvent?venueId=" + venueId;
	}
};

function loadImage(eventId, catScheme) {
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};
function checkId(){
	var flag=true;
	$('.checkbox-id').each(function(i, obj) {
		if(!$(obj).is(':checked')){
			flag=false;
			return;
		}
	});
	$("#title-id").attr('checked', flag);
}
function submitForm(){
	var isSelected=false;
	
	/*$('.checkbox-id').each(function(i, obj) {
		if($(obj).is(':checked')){
			result=true;
			//return;
		}
	});*/
	var flag= true;
	$('.checkbox-id:checkbox:checked').each(function () {
		isSelected = true;
		var id,value;
		
		id = this.id.replace('checkbox','venueCategoryId');
		var venueCatVal = $("#"+id).val();
		if(venueCatVal == "0") {
			alert("Select Valid Venue category.");
			$("#" + id ).focus();
			flag = false;
			return false;
		}
		
		/* id = this.id.replace('checkbox','presaleEvent');
		if($("#"+id).attr('checked')) {
			id = this.id.replace('checkbox','venueCategoryId');
			var text = $("option:selected",$("#"+id)).text();
			
			var tCatId = this.id.replace('checkbox','tourCategory');
			var tourCategory = $("#"+tCatId).val();
			if(text =='') {
				alert("Select Venue category before selecting PRESALE Event.");
				$("#" + id ).focus();
				flag = false;
				return false;
			} else if (tourCategory.toUpperCase()=="CONCERTS" && text.toUpperCase()!="END_STAGE" && text.toUpperCase() !="PRESALE_GA") {
				alert("Select END_STAGE or PRESALE_GA Venue category for CONCERT Events before selecting as presale.");
				$("#" + id ).focus();
				flag = false;
				return false;
			}
		} */
	});
	if(!flag) {
		return flag;
	}
	if(!isSelected){
		alert('Select an event first.');
		isSelected=false;
		return false;
	}
}
$("#title-id").click(function(){
	var flag = false;
	if($("#title-id").is(':checked')){
		flag=true;
	}
	
	$('.checkbox-id').each(function(i, obj) {
		$(obj).attr('checked', flag);
	});
});
function callAllVenueCate(){
	if($('#copyVenueCategory').attr('checked')){
	var isFirst=true;
	var options = "";
	var selected ='';
	var venueId="";
	var categoryIds="";
	var  currentVenueId="";
	 
	$('.venueCategory').each(function() {
		if(isFirst){
			var string=$(this).attr('id'); 
			$("#" + $(this).attr('id') +" option").each(function(){
				if($(this).attr('selected')){
					selected = "selected";
				}else{
					selected = "";
				}		
				<c:forEach var="event" items="${events}">
				  
				   var id=${event.id};
	               var eventId=id.toString();
	                       
	                            
	               var res=string.replace("-venueCategoryId",'');
	              
	               if(eventId==res){
	            	  currentVenueId=${event.venue.id};
	            	  venueId=currentVenueId;	            	
	              }			  
				 </c:forEach>
				options += '<option value = ' + $(this).val() + ' ' + selected +'>' + $(this).text() + '</option>';
				categoryIds+=$(this).val()+" ";
			});
			isFirst=false;
		}else{
			 /*  var flag=categoryIds.search($("#"+$(this).attr('id')).val()); */
			 var check=true;
			  <c:forEach var="event" items="${events}">
			   if(check){
				   $("#"+${event.id}+'-checkbox').attr('checked',true);
				   check=false;
			   }
			   var id=${event.id};
               var eventId=id.toString();
                       
               var string=$(this).attr('id');              
               var res=string.replace("-venueCategoryId",'');
              
               if(eventId==res){
            	  currentVenueId=${event.venue.id};
            		if(venueId==currentVenueId){
            	       $("#"+${event.id}+'-checkbox').attr('checked',true);
            		}
              }			  
			 </c:forEach>
			         
			    	if(venueId==currentVenueId){
			    		
			          $(this).html(options);
			    	}			    	
		}
	});
         
  }else{
         $('.checkbox-id').attr('checked',false);
}
}
function callAllPresaleEvent() {
	if($('#copyPresaleEvent').attr('checked')){
		$('.checkbox-id').attr('checked', true);
		$('.presaleEvent').attr('checked', true);
	}else{
		 $('.checkbox-id').attr('checked', true); 
		$('.presaleEvent').attr('checked', false);
	}
}

function checkPresaleEventCSV(eventId) {
	
	if($("#"+eventId+"-presaleEvent").attr('checked')) {
		var text = $("option:selected",$("#"+eventId+"-venueCategoryId")).text();
		var tourCategory = $('#'+eventId+'-tourCategory').val();
		if(text =='') {
			alert("Select Venue category before selecting PRESALE Event.");
			$("#"+eventId+"-venueCategoryId").focus();
			flag = false;
			return false;
		} else if (tourCategory.toUpperCase()=="CONCERTS" && text.toUpperCase()!="END_STAGE" && text.toUpperCase() !="PRESALE_GA")  {
			alert("Select END_STAGE or PRESALE_GA Venue category for CONCERT Events before selecting as presale.");
			$("#"+eventId+"-venueCategoryId").focus();
			flag = false;
			return false;
		}
	}
	return true;
}
</script>
