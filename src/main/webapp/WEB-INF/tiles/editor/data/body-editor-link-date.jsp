<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Link Admitone Events
</div>

<h1>Link Admitone Events</h1>
<a href="#" onclick="getEvents('[0-9]')">[0-9]</a>|
<a href="#" onclick="getEvents('a')">A</a>|
<a href="#" onclick="getEvents('b')">B</a>|
<a href="#" onclick="getEvents('c')">C</a>|
<a href="#" onclick="getEvents('d')">D</a>|
<a href="#" onclick="getEvents('e')">E</a>|
<a href="#" onclick="getEvents('f')">F</a>|
<a href="#" onclick="getEvents('g')">G</a>|
<a href="#" onclick="getEvents('h')">H</a>|
<a href="#" onclick="getEvents('i')">I</a>|
<a href="#" onclick="getEvents('j')">J</a>|
<a href="#" onclick="getEvents('k')">K</a>|
<a href="#" onclick="getEvents('l')">L</a>|
<a href="#" onclick="getEvents('m')">M</a>|
<a href="#" onclick="getEvents('n')">N</a>|
<a href="#" onclick="getEvents('o')">O</a>|
<a href="#" onclick="getEvents('p')">P</a>|
<a href="#" onclick="getEvents('q')">Q</a>|
<a href="#" onclick="getEvents('r')">R</a>|
<a href="#" onclick="getEvents('s')">S</a>|
<a href="#" onclick="getEvents('t')">T</a>|
<a href="#" onclick="getEvents('u')">U</a>|
<a href="#" onclick="getEvents('v')">V</a>|
<a href="#" onclick="getEvents('w')">W</a>|
<a href="#" onclick="getEvents('x')">X</a>|
<a href="#" onclick="getEvents('y')">Y</a>|
<a href="#" onclick="getEvents('z')">Z</a>
<br/><br/>
<form action="EditorLinkDate" id="form" name ="form">
<input type="hidden" id="firstChar" name="firstChar" value="${firstChar}">
<br/>
</form>


<h2>TMAT Events --> Admitone Events</h2>
<c:forEach var="event" items="${events}">
	${event.name}  |  <fmt:formatDate value="${event.date}" pattern="MMM dd,yyyy" />  |  ${event.time} -->
  <select id="aoneEvent_${event.id}" >
  	<option value="">NONE SELECTED</option>
  	<c:forEach var="aoneEvent" items="${eventListMap[event.id]}">
		<c:set var="isSelected" value="a" />	  		
		<c:if test="${fn:length(eventListMap[event.id]) eq 1}">
  			<c:set var="isSelected" value="selected" />
  		</c:if>
    	<option  ${isSelected} value="${aoneEvent.eventId}"> ${aoneEvent.eventName} - <fmt:formatDate value="${aoneEvent.eventDate}" pattern="MMM dd,yyyy" />  ${aoneEvent.eventTime} - ${aoneEvent.venueName}</option>
  	</c:forEach>
  </select>
<br/><br/>
</c:forEach>
<input id="linkEventsButton" type="button" class="medButton" onclick="linkEvents()"  value="Link Events"/>
<br/>
    
<jsp:directive.page import="com.admitone.tmat.data.Event" />
    
<script type="text/javascript">


var eventIds = [
<c:forEach var="event" items="${events}"><%
    Event event = (Event)pageContext.getAttribute("event");
	
%>${event.id},
</c:forEach>
null
];

eventIds = eventIds.slice(0, eventIds.length - 1);
function getEvents(character){
	$('#firstChar').val(character);
	var checked = $('#getFromDB').is(':checked');
	$('#form').submit();
}
function linkEvents() {
$.each(eventIds, function(i, eventId) {
	var aOneId = parseInt($('#aoneEvent_' + eventId).val());
    if (!isNaN(aOneId)){
  	  DataDwr.updateEventAdmitoneId(eventId, aOneId, function(response) {                
  	         alert(response);
  	         var url= 'EditorLinkAdmitone?firstChar=' + $("#firstChar").val();
  	         location.href =  url;
  	   });
	}
  });
}

</script>