<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubCrawlsMenu" name="selectedSubCrawlsMenu" scope="request" />

<div id="editorSubMenu">

<c:choose>
		<c:when test="${selectedSubCrawlsMenu == 'Automatic Crawler Creation'}">
			<b>Automatic Crawler Creation</b>
		</c:when>
		<c:otherwise>
			<a href="EditorAutoCrawlerCreation">Automatic Crawler Creation</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
		<c:when test="${selectedSubCrawlsMenu == 'Manage Crawls'}">
			<b>Manage Crawls</b>
		</c:when>
		<c:otherwise>
			<a href="EditorManageCrawls">Manage Crawls</a>
		</c:otherwise>
	</c:choose>
	<authz:authorize ifAnyGranted="PRIV_SUPER_ADMIN,PRIV_ADMIN,PRIV_EDITOR">
	|
	<c:choose>
		<c:when test="${selectedSubCrawlsMenu == 'Default Exchange Crawl Frequency'}">
			<b>Default Exchange Crawl Frequency</b>
		</c:when>
		<c:otherwise>
			<a href="loadEditorEditExchangeCrawlFrequency">Default Exchange Crawl Frequency</a>
		</c:otherwise>
	</c:choose>
	|
	
 <c:choose>
   <c:when test="${selectedSubCrawlsMenu == 'Super Automated Event Creator'}">
        <b>Super Automated Event/Crawl Creator</b>
   </c:when>
   <c:otherwise>
        <a href="EditorAutomatedEventCreator">Super Automated Event/Crawl Creator</a>
   </c:otherwise>
 </c:choose>
 </authz:authorize>
</div>	
<tiles:insertAttribute name="subBody1" /> 