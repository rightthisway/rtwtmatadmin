<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubScecMenu" name="selectedSubScecMenu" scope="request" />

<div id="editorSubMenu">
<c:choose>
		<c:when test="${selectedSubScecMenu == 'Super Crawl Creation'}">
			<b>Super Crawl/Event Creation</b>
		</c:when>
		<c:otherwise>
			<a href="EditorSuperCrawlCreation">Super Crawl/Event Creation</a>
		</c:otherwise>
	</c:choose>
	|
	<c:choose>
      <c:when test="${selectedSubScecMenu == 'Quick Create Event'}">
        <b>Quick Create Event</b>
      </c:when>
      <c:otherwise>
        <a href="EditorEditEvent?action=create">Create Event Manually</a>
      </c:otherwise>
    </c:choose> 
</div>
<tiles:insertAttribute name="subBody1" /> 	