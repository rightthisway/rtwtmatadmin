<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; GrandChild Category
</div>

<c:if test="${not empty info}">
<div class="info">
		<div class="infoText">
			${info}
		</div>
</div>
</c:if>

<h1>GrandChild Category</h1>

<form method="post" action="EditorManageGrandChildTourCategory" id="tourForm" name="tourForm">
  <b>Parent Category Filter:</b>
  <select id="parentCategory" name="parentCategory" onchange="getChildCategory();">
  <option value="0" <c:if test="${parentCategoryString==0}">selected</c:if>>--All--</option>
  <c:forEach items="${parentCategoryList}" var="parentCategory">
  	<option value="${parentCategory.id}" <c:if test="${parentCategoryString==parentCategory.id}">selected</c:if>>${parentCategory.name}</option>
  	</c:forEach>
  </select>
   <br />
   <b>Child Category Filter:</b>
   
  <select id="childCategory" name="childCategory">
  <option value="0" <c:if test="${childCategoryString==0}">selected</c:if>>--All--</option>
  <c:forEach items="${childCategoryList}" var="childCategory">
  	<option value="${childCategory.id}" <c:if test="${childCategoryString==childCategory.id}">selected</c:if>>${childCategory.name}</option>
  	</c:forEach>
  </select>
  <!--<input type="hidden" name="tobeRemoved" id="tobeRemoved" />
  <input type="hidden" name="action" id="action" />
  -->
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form>



<a href="EditorEditGrandChildTourCategory?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new GrandChild Tour Category</a>
<br /><br />
<a href="javascript:selectAll()">All</a>
<a href="javascript:selectNone()">None</a>
<a href="javascript:removeSelected()">Remove</a>
<br />
<br />
<display:table class="list" name="${grandChildTourCategorys}" id="grandChildTourCategory" requestURI="EditorManageGrandChildTourCategory" defaultsort="3" pagesize="200">
    <display:column>
      <input type="checkbox" class="checkbox-tour" id="${grandChildTourCategory.id}:${grandChildTourCategory.name}"/>
    </display:column>
    <display:column title="Id" sortable="true" property="id"/>
	<display:column title="Parent Category" sortable="true" >
		${grandChildTourCategory.childTourCategory.tourCategory.name}
	</display:column>

	<display:column title="Child Category" sortable="true" >
		${grandChildTourCategory.childTourCategory.name}
	</display:column>
    <display:column title="Grand Child Category" sortable="true" property="name" />
    <display:column title="Edit Category">
		<a href="EditorEditGrandChildTourCategory?id=${grandChildTourCategory.id}&tourCatId=${grandChildTourCategory.childTourCategory.tourCategory.id}">Edit</a>
	</display:column>
</display:table>
<script type="text/javascript">
	$(document).ready(function(){
	});

		function selectAll(){
			$(".checkbox-tour").each(function(){
				this.checked=true;
			});
		}
		function selectNone(){
			$(".checkbox-tour").each(function(){
				this.checked=false;
			});
		}
	
		function removeSelected(){
			var tobeRemoved='';
			 $('input:checkbox:checked.checkbox-tour').each(function() {
				 tobeRemoved+=this.id + ',';
			 });
			 //$('#action').val('remove');
			 //$('#tobeRemoved').val(tobeRemoved.substring(0,tobeRemoved.length-1));
			 //alert($("#tourForm").attr("action"));
			 window.location ="EditorManageGrandChildTourCategory?action=remove&tobeRemoved=" + tobeRemoved.substring(0,tobeRemoved.length-1);

			
		}
	function getChildCategory(){
	
	$.getJSON("GetChildTourCategory?tourCategory="+$('#parentCategory').val(),
	function(data){
		var option='<option value="0">---- Select ----</option>';
		$.each(data,function(i,childCategory){
			option += '<option value="' + childCategory.id +'">' + childCategory.name + '</option>';
		});
//		alert(option);
		$('select#childCategory').html(option);
	});	
}
</script>