<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	$("#searchBtn").click(function(){
		if($('#venueName').val() == '' && $('#city').val() == '' && $('#state').val() == ''
			&& $('#country').val() == '' && $('#venueType').val() == ''){
			alert("Please Enter atleast one criteria.!");
		}else if($('#dateRange1').val() != '' && $('#dateRange2').val() == ''){
			alert("Please Enter both Dates.!");
		}else if($('#dateRange2').val() != '' && $('#dateRange1').val() == ''){
			alert("Please Enter both Dates.!");
		}else{
			window.location="EditorManageVenues?venue="+$('#venueName').val()+"&city="+$('#city').val()+"&state="+$('#state').val()+"&country="+$('#country').val()+"&venueType="+$('#venueType').val()+"&action=search";
		}
	
	});  
});
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Venues
</div>

<h1>Manage venues</h1>

<%-- <form method="GET" action="EditorManageVenues">
  <b>Filter:</b>
  <input type="text" name="filter" value="${param.filter}"/>
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form> --%>

<br/>
<div id="ticket-filter-toggle" style="background:#a1a8ff; padding: 5px">
	<table>
		<tr>
			<td><b>Venue :</b>
			<input type="text" name="venueName" id="venueName" value="${venue}"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>City :</b>
			<input type="text" name="city" id="city" value="${city}"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>State :</b>
			<input type="text" name="state" id="state" value="${state}"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Country :</b>
			<input type="text" name="country" id="country" value="${country}"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Venue Type :</b>
				<select id="venueType">
					<option value="">--Select Type--</option>
					<option value="THEATER" <c:if test="${venueType=='THEATER'}">selected="selected"</c:if>>THEATER</option>
					<option value="SPORT" <c:if test="${venueType=='SPORT'}">selected="selected"</c:if>>SPORT/CONCERT</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5" align="center"><input type="button" class="medButton" id="searchBtn" value="Filter"></td>
		</tr>
	</table>
</div>
<br/><br/>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>

<a href="EditorEditVenue?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new venue</a>
<br /><br />
<c:choose>
    	<c:when test="${not empty venues}">
    		<display:table class="list" name="${venues}" id="venue" requestURI="EditorManageVenues" pagesize="200">
				<display:column title="Id" sortable="true" property="id" href="EditorEditVenue?action=edit" paramId="id" paramProperty="id"  />
				<display:column title="Venue" sortable="true" property="building" href="EditorEditVenue?action=edit" paramId="id" paramProperty="id" class="content-left-align" headerClass="header-left-align"/>
				<display:column title="City" sortable="true" property="city" class="content-left-align" headerClass="header-left-align"/>
				<display:column title="State" sortable="true" property="state" />
				<display:column title="Country" sortable="true" property="country" />
				
				<!--to display sport/concert instead of sport or concert  -->
				<c:choose>
					<c:when test="${venue.venueType == 'CONCERT' || venue.venueType == 'SPORT'}">
							<c:set var="venue1" value="SPORT/CONCERT"></c:set>
					</c:when>
					<c:otherwise>
							<c:set var="venue1" value="${venue.venueType}"></c:set>
					</c:otherwise>
				</c:choose>
				   <display:column title="Venue Type">${venue1}</display:column>
				<display:column>
					<a href="EditorEditVenue?id=${venue.id}&action=edit"><img src="../images/ico-edit.gif" align="absbottom" /> edit</a><!-- - 
					<a href="javascript:confirmRemove(${venue.id})"><img src="../images/ico-delete.gif" align="absbottom" /> remove</a> -->
				</display:column> 
					
			</display:table>
    	</c:when>
    	<c:otherwise>
			Nothing found to display
		</c:otherwise>
	</c:choose>

<div id="deleteVenueDialog" title="" style="display:none">
	<span id="deleteVenueDialogMessage" align="center"></span>
</div>


<script type="text/javascript">

	$("#deleteVenueDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		minHeight:120,
		height:120,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});


	function confirmRemove(venueId) {
        DataDwr.getNumEventsInVenue(venueId, function(response) {
			$('#deleteVenueDialog').dialog('option', 'title', 'Delete venue?');
        	$('#deleteVenueDialogMessage').html(
        		"The venue has " + response + " event(s) attached to it.<br />"
        		+ "If you delete it, all the associated events will be deleted as well.<br /><br />"
        		+ "Do you really want to delete it?");

			$("#deleteVenueDialog").dialog('open');
			$("#deleteVenueDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorManageVenues?id=" + venueId + "&action=remove";			
					$('#deleteVenueDialog').dialog('option', 'title', 'Deleting Venue...');
					$('#deleteVenueDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting venue...');		
					$("#deleteVenueDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
	};
</script>