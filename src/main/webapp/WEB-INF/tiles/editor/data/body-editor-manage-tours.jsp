<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
function submitForm(){
		if($('#tourName').val() == '' && $('#artist').val() == '' ){
			alert("Please Enter Atleast one Criteria to Filter.!");
			return false;
		}else{
			$('#action').val('filter');
			return true;
		}
	}
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Tours/Seasons
</div>

<h1>Manage tours/seasons</h1>

<%-- <form method="GET" action="EditorManageTours">
  <b>Filter:</b>
  <input type="text" name="filter" value="${param.filter}"/>
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form> --%>


<br/>
<div id="ticket-filter-toggle" style="background:#a1a8ff; padding: 5px">
	<form action="EditorManageTours" onSubmit="return submitForm();">
	<table>
		<tr>
			<td><b>Tour/Season :</b></td>
			<td><input type="text" name="tourName" id="tourName" value="${tourName}"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Artist :</b>
			<input type="text" name="artist" id="artist" value="${artist}"></td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<input type="hidden" name="action" id="action" >
				<input type="submit" class="medButton" id="searchBtn" value="Filter">
			</td>
		</tr>
	</table>
	</form>
</div>
<br/><br/>


<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>

Note: Tour and Season is the same entity and can be interchanged. For user's convenience, we display it as Season in case the tour is of type SPORT and Tour otherwise.<br /><br /> 

<a href="EditorEditTour?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new tour / season</a><br /><br />
<b>Total no. of tour having active events :</b> ${toursCount} <br /><br /><b>Total no. of events having active crawls :</b> ${eventsCount}
<br /><br />
<display:table class="list" name="${tours}" id="tour" requestURI="EditorManageTours" pagesize="400">
    <display:column>
      <input type="checkbox" class="checkbox-tour" id="tour-${tour.id}"/>
    </display:column>
    <display:column title="Id" sortable="true" >
    	<a href="EditorEditTour?action=edit&id=${tour.id}">${tour.id}</a>
    </display:column>
    <display:column title="">
    	<img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}" />
    </display:column>    
    <display:column title="Tour / Season Name" sortable="true" property="name" href="EditorEditTour?action=edit&id=${tour.id}"  class="content-left-align" headerClass="header-left-align" />
    <display:column title="Artist / Team Name" sortable="true" property="artist.name" class="content-left-align" headerClass="header-left-align"/>
    <display:column title="Location" sortable="true" property="location" />
    <display:column title="StartDate" sortable="true">
      <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
    </display:column>
    <display:column title="EndDate" sortable="true">
      <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
    </display:column>
    <display:column title="Event Count" sortable="true" sortProperty="eventCount" class="text-align-center" property="eventCount" />
    <display:column>
    	<a href="EditorDetailTour?tourId=${tour.id}"><img src="../images/ico-edit.gif" align="absbottom" /> details</a><!-- - 
    	 <a href="javascript:confirmRemove(${tour.id})"><img src="../images/ico-delete.gif" align="absbottom" />  remove</a> -->
    </display:column>    
</display:table>
<br/><br/>
Merge to tour:
<select id="goodTour">
  <c:forEach var="tour" items="${tours}">
    <option value="${tour.id}">${tour.name} - ${tour.id}</option>
  </c:forEach>
</select>

<input id="mergeButton" type="button" class="medButton" value="Merge" onclick="mergeTours()"/>
<span id="mergeWaitSpan" style="display:none"><img src="../images/process-running.gif" align="absbottom"/> Please wait...</span>

<div id="deleteTourDialog" title="" style="display:none">
	<span id="deleteTourDialogMessage" align="center"></span>
</div>

<script type="text/javascript">

//
// Merge tours
//

function mergeTours() {
  var mergeIds = "";
  var checkboxChecked = $('.checkbox-tour:checked');
  if (checkboxChecked.length == 0) {
    alert('You have to select at least one tour');
    return;
  }

  var goodTourId = $('#goodTour').val(); 
  var tourIds = "";
  $.each(checkboxChecked, function(intIndex, elt) {
    tourIds += elt.id.replace(/tour-/, '') + ",";
  });
  var answer = confirm("Are you sure you want to merge the selected tours?");
  if (answer) {
  			   $('#mergeWaitSpan').show();
  			   $('#mergeButton').hide();
		       DataDwr.mergeMultipleTours(tourIds, goodTourId, function(response) {
  			      $('#mergeWaitSpan').hide();
  			      $('#mergeButton').show();
				  alert(response);
				  if (response == 'OK') {
				    if (location.href.indexOf('?') >= 0) {
				    	location.href = location.href + "&reload=" + new Date().getTime();
				    } else {
				        location.href = location.href + "?reload=" + new Date().getTime();
				    }
				  }
       			});			

  }
}

//
// Autocomplete
//

$('#goToTextDataField').val('Edit Artist, Tour, Event');
$('#goToTextDataField').css({color: '#aaaaaa', 'font-style': 'italic'});

$('#goToTextDataField').focus(function() {
	$('#goToTextDataField').val("");
	$('#goToTextDataField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextDataFieldStatus').empty();
});

$('#goToTextDataField').blur(function() {
	$('#goToTextDataField').val('Edit Artist, Tour, Event');
	$('#goToTextDataField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextDataFieldStatus').empty();
});

$('#goToTextDataField').autocomplete("AutoCompleteSearch", {
		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (row[3] != "null" || row[4] != "null") {
				 	tourRow += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextDataFieldStatus').empty();
			$('#goToTextDataFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextDataFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextDataFieldStatus').text("No results");
			} else {
				$('#goToTextDataFieldStatus').text("Found " + rows.length + " results");
			}
		}
});

$('#goToTextDataField').result(function(event, row, formatted) {
	$('#goToTextFieldDataContainer').hide();
	$('#dataRedirectingContainer').show();
	
	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
	
	$('#dataRedirectingPage').html(pageDescription);
	
	if (row[0] == "ARTIST") {
		document.location.href = "EditorEditArtist?action=edit&id=" + row[2];
		return;
	} else if (row[0] == "TOUR") {
		document.location.href = "EditorDetailTour?tourId=" + row[2];
		return;
	} else if (row[0] == "EVENT") {
		document.location.href = "EditorEditEvent?action=edit&id=" + row[2];
		return;
	}
});

	$("#deleteTourDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

	function confirmRemove(tourId) {
		$('#deleteTourDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Please wait');		
		$('#deleteTourDialog').dialog('open');
		$('#deleteTourDialog').dialog('option', 'title', 'Please wait...');
		$('#deleteTourDialog').dialog('option', 'buttons', {});

        DataDwr.getNumEvents(tourId, function(response) {
			$('#deleteTourDialog').dialog('option', 'title', 'Delete tour?');
        	$('#deleteTourDialogMessage').html(
        		"The tour has " + response + " event(s) attached to it.<br />"
        		+ "If you delete it, all the associated events will be deleted as well.<br /><br />"
        		+ "Do you really want to delete it?");

			$("#deleteTourDialog").dialog('open');
			$("#deleteTourDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorManageTours?id=" + tourId + "&action=remove";			
					$('#deleteTourDialog').dialog('option', 'title', 'Deleting tour...');
					$('#deleteTourDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting tour...');		
					$("#deleteTourDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
	
	};
	
</script>
