<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Artists
</div>

<c:if test="${not empty info}">
  <div class="info"><div class="infoText">${info}</div></div>
  <br />
</c:if>

<h1>Manage Artists / Teams</h1>


<form method="GET" action="EditorManageArtists">
  <b>Search:</b>
  <input type="text" name="filter" value="${param.filter}"/>
  <input class="medButton" type="submit" name="submit" value="Search"/>
</form>

Note: Artist and Team is the same entity and can be interchanged. For user's convenience, we display it as Team in case the tour is of type SPORT and Artist otherwise.<br /><br /> 

<a href="EditorEditArtist?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new artist / team</a>
<br /><br />

<display:table class="list" name="${artists}" id="artist" requestURI="EditorManageArtists" pagesize="100" defaultsort="3" defaultorder="descending">
    <display:column title="Id" sortable="true" property="id" href="EditorEditArtist?action=edit" paramId="id" paramProperty="id" />
    <display:column title="Artist / Team Name" sortable="true" property="name"  href="EditorEditArtist?action=edit" paramId="id" paramProperty="id" class="content-left-align" headerClass="header-left-align" />
    <display:column title="Status" sortable="true" sortProperty="artistStatus" class="text-align-center" property="artistStatus"/>
    <display:column title="GrandChildCategory" sortable="true" sortProperty="grandChildTourCategory" class="text-align-center" >${artist.grandChildTourCategory.name}</display:column>
    <display:column>
    	<a href="EditorDetailArtist?artistId=${artist.id}"><img src="../images/ico-edit.gif" align="absbottom" /> detail</a> - 
		<a href="EditorEditArtist?id=${artist.id}&action=edit"><img src="../images/ico-edit.gif" align="absbottom" /> edit</a> - 
    	<a href="javascript:confirmRemove(${artist.id})"><img src="../images/ico-delete.gif" align="absbottom" /> remove</a>
    </display:column>    
</display:table>

<div id="deleteArtistDialog" title="" style="display:none">
	<span id="deleteArtistDialogMessage" align="center"></span>
</div>

<script type="text/javascript">
	$("#deleteArtistDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});


	function confirmRemove(artistId) {
		$('#deleteArtistDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Please wait');		
		$('#deleteArtistDialog').dialog('open');
		$('#deleteArtistDialog').dialog('option', 'title', 'Please wait...');
		$('#deleteArtistDialog').dialog('option', 'buttons', {});

        DataDwr.getNumEventsByEventId(artistId, function(response) {
			$('#deleteArtistDialog').dialog('option', 'title', 'Delete artist?');
        	$('#deleteArtistDialogMessage').html(
        		"The artist has " + response + " tour(s) attached to it.<br />"
        		+ "If you delete it, all the associated tours will be deleted as well.<br /><br />"
        		+ "Do you really want to delete it?");

			$("#deleteArtistDialog").dialog('open');
			$("#deleteArtistDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorManageArtists?id=" + artistId + "&action=remove";			
					$('#deleteArtistDialog').dialog('option', 'title', 'Deleting artist...');
					$('#deleteArtistDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting artist...');		
					$("#deleteArtistDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
	};
	
	
</script>
