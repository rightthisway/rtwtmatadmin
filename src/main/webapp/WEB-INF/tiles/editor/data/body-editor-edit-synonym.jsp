<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorManageSynonyms">Theater Synonyms</a>
	&gt; 
    <c:choose>
	  <c:when test="${empty param.id}">Add a new synonym</c:when>
	  <c:otherwise>Edit Synonym</c:otherwise>
    </c:choose>
</div>

<h1>
<c:choose>
	<c:when test="${empty param.id}">Add a new synonym</c:when>
	<c:otherwise>Edit Synonym</c:otherwise>
</c:choose>
</h1>

<form:form commandName="synonym" method="POST" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Name</td>
			<td>
				<form:input path="name" cssClass="longInput" id="name"/>			
			</td>
		</tr>
		<tr>
			<td>Synonyms</td>
			<td>
				<form:input path="value" cssClass="longInput" id="value"/> (comma separated)		
			</td>
		</tr>
		<tr>
			<td>Synonyms Type</td>
			<td>
				<form:select path="type" id="type">
					<form:option value="Concert">CONCERT</form:option>
 			 		<form:option value="Theater">THEATER</form:option>
 			 	</form:select>			
			</td>
		</tr>
		
	</table>
		
	<input type="submit" name="submit" value="Save Changes" class="medButton" id="submitBtn"/>
	<input type="button" value="Cancel" class="medButton" onclick="document.location.href='EditorManageSynonyms'" />
</form:form>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#name').focus();
		
		$("#submitBtn").click(function(){
			if($("#name").val() == ""){
				alert("Please Enter Name");
				return false;
			}
			if($("#value").val() == ""){
				alert("Please Enter Synonyms");
				return false;
			}
			if($("#type").val() == ""){
				alert("Please Enter Synonyms Type");
				return false;
			}
			return true;
		});
		
	});
</script>