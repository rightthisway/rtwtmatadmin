<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP"></a> 
  &gt; <a href="EditorManageArtists">Artist</a>
  &gt; Merge Artist
<c:choose> 
  <c:when test="${not empty artist}">
    &gt; Edit ${artist.name}
  </c:when>
  <c:otherwise>
    &gt; New Artist
  </c:otherwise>
</c:choose>
</div>

<h2>Merge This Artist</h2> 

	<div class="info">
		<div class="infoText">
			ONCE ARTIST IS MERGED IT CANNOT BE UNDONE.
			<br/>
			MERGE ARTIST WOULD BE MERGED WITH THE SELECTED ARTIST.
			<br/>
			MERGE ARTIST BOOKMARKS AND ARTIST PRICE ADJUSTMENTS WOULD BE DELETED. 
			<br/>
			SELECTED ARTIST WOULD TAKE MERGE ARTIST'S EVENTS.
		</div>
	</div> 
	
	<div id="mergeArtistDialog" title="" style="display:none">
		<span id="mergeArtistDialogMessage" align="center"></span>
	</div>
	
	
	
	<input type="hidden" id="artistId"/>
	<input type="hidden" id="mergeToArtistId"/>
	<input type="hidden" id="artistName"/>
	<input type="hidden" id="mergeToArtistName"/>
	
Select Artist      : 
	
		<span id="artistLabelSpanId" style="display: none;">
			<span id="artistNameDiv" >
			</span>	
			<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"  />
		</span>
		
		<span id="artistSelectSpanId">
			<input type="text" name ="autoArtist" id ="autoArtist" >
		</span>
	<br/><br/>
Merge Artist :	
		<span id="mergeArtistLabelSpanId" style="display: none;">
			<span id="mergeArtistNameDiv" >
			
			</span>	
			<input type="button" value="Change Artist" class="medButton" onclick="callMergeArtist();"  />
		</span>
		
		
		<span id="mergeArtistSelectSpanId">

			<input type="text" name ="mergeAutoArtist" id ="mergeAutoArtist" >
		</span>
	to Selected Artist.
	 <br/><br/>
	<input id="mergeArtistsButton" type="button" class="medButton" onclick="mergeArtists()"  value="Merge Artists" />

<script type="text/javascript" language="javascript">

function validateData(){
	
	if(document.getElementById("name").value == ""){
		alert("Please Enter Name");	
		return false;
	}
	
}

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val('');
		$("#artistId").val(row[0]);
		$('#artistName').val(row[1]);
		$("#tempArtistId").val(row[0]);
		$('#artistNameDiv').text(row[1]);
		$('#artistLabelSpanId').show();
		$('#artistSelectSpanId').hide();
	});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#tempArtistId').val('');
}

$(document).ready(function(){
	$('#mergeAutoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		if($("#artistId").val() != row[0]){
			$('#mergeAutoArtist').val('');
			$("#mergeToArtistId").val(row[0]);
			$('#mergeToArtistName').val(row[1]);
			$("#mergetempArtistId").val(row[0]);
			$('#mergeArtistNameDiv').text(row[1]);
			$('#mergeArtistLabelSpanId').show();
			$('#mergeArtistSelectSpanId').hide();
		}else{
			$('#mergeAutoArtist').val('');
			alert("Merge Artist should not be same as selected Artist.");
		}
	});
	
});

function callMergeArtist(){ 
	$("#mergeToArtistId").val("");
	$('#mergeToArtistName').val("");
	$('#mergeArtistLabelSpanId').hide();
	$('#mergeArtistSelectSpanId').show();
	$('#mergetempArtistId').val('');
}

	
	//merge tour	
	$("#mergeArtistDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	
	
	function mergeArtists() {
		var goodId = $("#artistId").val();
		var mergeId = $('#mergeToArtistId').val();
		var artist = $("#artistName").val();
		var mergeArtist = $("#mergeToArtistName").val();
		$('#mergeArtistDialogMessage').html(
        		"If you merge this artist, all the associated events will be "
        		+ "<br /> transferred from " + mergeArtist + " to " + artist + ". "
        		+ "<br /> All the ${mergeId} artist price adjustments and bookmarks "
        		+ "<br /> will be deleted. "
        		+ "<br /><br />Do you really want to merge this artist?");
        $("#mergeArtistDialog").dialog('open');
        $('#mergeArtistDialog').dialog('option', 'title', 'Merge Artists?');
		$('#mergeArtistDialog').dialog('option', 'buttons', {});
		$("#mergeArtistDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeArtistDialog').dialog('option', 'title', 'Merging Artists...');
				$('#mergeArtistDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging Artists...');		
				$("#mergeArtistDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeArtists(mergeId, goodId, function(response) {
				   $('#mergeArtistDialogMessage').html(response + "!");
				   $("#mergeArtistDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$("#mergeArtistDialog").dialog('close');
							
					        //return home
					        document.location.href = "EditorManageArtists"; 
					   }
				   });
       			});			
			},
			Cancel: function() {
				$("#mergeArtistDialog").dialog('close');
			}
		}); 
	};
</script>