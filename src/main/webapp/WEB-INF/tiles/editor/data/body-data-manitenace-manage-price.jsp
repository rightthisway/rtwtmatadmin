<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<%@page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript">
var rowIncrement=3;
$(document).ready(function(){

	var isFirstLoad = '${isFirst}'	;
	 $("#isAllArtist").click(function() {
		if((document.getElementById("isAllArtist").checked)){		
			$("#artistSelect").each(function(){
				$("#artistSelect option").attr("selected","selected");				
			});
			deleteAllRow("dataTable");		
		}
	 }) ;
	 
	 $("#isAllEvent").click(function() {
		if((document.getElementById("isAllEvent").checked)){		
			$("#eventSelect").each(function(){
				$("#eventSelect option").attr("selected","selected");				
			});
					
			deleteAllRow("dataTable");		
		}
	}) ;
	 
	if('${exchangeSelected}' != ''){
	 	$("#exchangeSelect").val('${exchangeSelected}');
	 }
	 $("#exchangeSelect").change(function () {
		
		 var str = "";
         $("#exchangeSelect option:selected").each(function () {  
				str += $(this).val();		  
         });
		 
		 $('#ticketTypeSelect').children().remove();
         $('#ticketTypeSelect').append(' <option value="--">--Select--</option>'); 
		
         var optionStr = '<option value="REGULAR" <c:if test="${ticketTypeSelected == 'REGULAR'}">selected</c:if>>REGULAR</option>'; 
         if(str == "admitoneeventinventory"){
        	 $('#ticketTypeSelect').append(optionStr);        	 
         }else if(str == "ebay"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eibox"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eimarketplace"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "eventinventory"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "fansnap"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "getmein"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "razorgator"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "seatwave"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "seatwavefeed"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "stubhub"){
        	 $('#ticketTypeSelect').append(optionStr); 
        	 $('#ticketTypeSelect').append('<option value="INSTANT" <c:if test="${ticketTypeSelected == 'INSTANT'}">selected</c:if>>INSTANT</option>'); 
        	 $('#ticketTypeSelect').append('<option value="EDELIVERY" <c:if test="${ticketTypeSelected == 'EDELIVERY'}">selected</c:if>>EDELIVERY</option>'); 
         }else if(str == "stubhubfeed"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "ticketevolution"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="ETICKETS" <c:if test="${ticketTypeSelected == 'ETICKETS'}">selected</c:if>>ETICKETS</option>');  
         }else if(str == "ticketmaster"){
        	 $('#ticketTypeSelect').append(optionStr); 
         }else if(str == "ticketnetwork"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "ticketnetworkdirect"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="MERCURY" <c:if test="${ticketTypeSelected == 'MERCURY'}">selected</c:if>>MERCURY</option>');
        	 $('#ticketTypeSelect').append('<option value="MERCURYPOS" <c:if test="${ticketTypeSelected == 'MERCURYPOS'}">selected</c:if>>MERCURYPOS</option>');
         }else if(str == "ticketsnow"){
        	 $('#ticketTypeSelect').append(optionStr);
        	 $('#ticketTypeSelect').append('<option value="INSTANT" <c:if test="${ticketTypeSelected == 'INSTANT'}">selected</c:if>>INSTANT</option>'); 
        	 $('#ticketTypeSelect').append('<option value="ETICKETS" <c:if test="${ticketTypeSelected == 'ETICKETS'}">selected</c:if>>ETICKETS</option>');
         }else if(str == "ticketsolutions"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "viagogo"){
        	 $('#ticketTypeSelect').append(optionStr);
         }else if(str == "wstickets"){
        	 $('#ticketTypeSelect').append(optionStr);
         }
         
         
	 }).change();
		'<c:if test="${viewData == 'true'}">'
			loadData();
		'</c:if>'
		$("#exchangeSelect").click(function () {          
		  $('#isAllExchange').attr('checked', false);		  
         
        });
		
		$("#ticketTypeSelect").click(function () {          
		  $('#isAllTicketType').attr('checked', false);		  
         
        });
		
	
	 $("#isAllParent").click(function() {
		if((document.getElementById("isAllParent").checked)){
		
		$("#parentSelect").each(function(){
				$("#parentSelect option").attr("selected","selected");				
				});	
				var str = "";
				$("#parentSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
				setChildInfo(str);
		}
	 }) ;
	 
	 $("#parentSelect").change(function () {
	
	
          var str = "";
          $("#parentSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
			   if(!(isFirstLoad == 'true')){
			    $('#isAllParent').attr('checked', false);
				setChildInfo(str);	
			}				
    });
	
	$("#childSelect").change(function () { 
	 
          var str = "";
          $("#childSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
				if(!(isFirstLoad == 'true')){
				$('#isAllChild').attr('checked', false);
				setGrandChildInfo(str);	
			}				
    });
		
	$("#grandChildSelect").change(function () {
	    var str = "";
        $("#grandChildSelect option:selected").each(function () {  
			str += $(this).val() + ";";		  
		}); 
		//alert(str);
		if(!(isFirstLoad == 'true')){
			$('#isAllGrandChild').attr('checked', false);
			setArtistInfo(str);
		}
	});
	
	$("#artistSelect").change(function () {
          var str = "";
		  $("#artistSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 
		  if(!(isFirstLoad == 'true')){
			 $('#isAllArtist').attr('checked', false);
		  }
		  setEventInfo(str);
        });
	// 

	$("#eventSelect").change(function () {
          var str = "";
		  if(!(isFirstLoad == 'true')){
			$('#isAllEvent').attr('checked', false);
		  }
		  isFirstLoad = 'false';
          deleteAllRow("dataTable");
         /// $("div").text(str);
     }).change();

	 $("#isAllChild").click(function() {
		if((document.getElementById("isAllChild").checked)){
		
		$("#childSelect").each(function(){
				$("#childSelect option").attr("selected","selected");				
				});
			var str = "";
          $("#childSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 				
				setGrandChildInfo(str);
		}
	 }) ;
	 
		
	 $("#isAllGrandChild").click(function() {
		$('#processing').show();
		if((document.getElementById("isAllGrandChild").checked)){
		$("#grandChildSelect").each(function(){
				$("#grandChildSelect option").attr("selected","selected");				
				});	
		  var str = "";
          $("#grandChildSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              });   
				
				setArtistInfo(str);
		}
		
	 }) ;	 
	
	 $("#isAllArtist").click(function() {
		$('#processing').show();
		if((document.getElementById("isAllArtist").checked)){
		$("#artistSelect").each(function(){
				$("#artistSelect option").attr("selected","selected");				
				});	
		  var str = "";
          $("#artistSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              });   
		setEventInfo(str);
		}
		
	 }) ;

	$("#getData").click(function() {
		var selectedArtists="";
		var selectedEvents="";
		var selectedParent = "";
		var selectedChild = "";
		var selectedGrandChild = "";
		var selectedExchange="";
		var selectedTicketType="";
		rowIncrement=3;
		
		//alert("Clicked");
		$("#parentSelect option:selected").each(function () {
        	selectedParent += $(this).val() + ";";
				////alert($(this).val());
				
		});
		if(selectedParent == ""){
			alert("Please select parent category");
			return false;
		}
		$("#childSelect option:selected").each(function () {
			selectedChild += $(this).val() + ";";
////alert($(this).val());

		});
		if(selectedChild == ""){
			alert("Please select child category");
			return false;
		}	
		$("#grandChildSelect option:selected").each(function () {
			selectedGrandChild += $(this).val() + ";";
				////alert($(this).val());
				
		});
		if(selectedGrandChild == ""){
			alert("Please select grand child category");
			return false;
		}				
		$("#artistSelect option:selected").each(function () {
            selectedArtists += $(this).val() + ";";
			//alert($(this).val());
		});
		if(selectedArtists == ""){
			alert("Please select artist");
			return false;
		}
		$("#eventSelect option:selected").each(function () {
			selectedEvents += $(this).val() + ";";
			//alert($(this).val());
				
        });
		if(selectedEvents == ""){
			alert("Please select event");
			return false;
		}
		if($('#exchangeSelect').val()!='--'){
			selectedExchange += $('#exchangeSelect').val() + ";";
			//alert($(this).val());
        }else{
			alert("Please select exchange.");
			return false;
		}
		if($("#ticketTypeSelect").val()!='--'){
			selectedTicketType  += $('#ticketTypeSelect').val() + ";";
			//alert($(this).val());
        }else{
			alert("Please select TicketType.");
			return false;
		}
			
	  	deleteAllRow("dataTable");
	 	addRow("dataTable",selectedEvents,selectedExchange,selectedTicketType);
	 }) ;
	
});
function  setChildInfo(parentid){
////alert("hello");
	   //$('.error').hide();
	   deleteAllRow('dataTable');
	   $('#childSelect').children().remove();
	   //$('#child').append("<option value=''>-Select-</option>");
	   $('#grandChildSelect').children().remove();
	   $('#artistSelect').children().remove();
	   $('#eventSelect').children().remove();
	   $('#isAllChild').attr('checked', false);	   
	   $('#isAllGrandChild').attr('checked', false);	   
	   $('#isAllArtist').attr('checked', false);
	   $('#isAllEvent').attr('checked', false);	   
	   //$('#grandChild').append("<option value=''>-Select-</option>");
	    
	   var parent = parentid;	
		////alert(parent);
		var myurl = "GetCategory?parent="+parent;	
		 if(parent!='' && parent!='-Select-'){
	 	 $.ajax({
			url:myurl,
			success: function(res){
			//	//alert(res);
			//var jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#childSelect').append(rowText);
              }				
				
			}
 
		}); 
		}
		 
		 
} 
function  setGrandChildInfo(childID){
	$('.error').hide();
	deleteAllRow('dataTable');
	$('#grandChildSelect').children().remove();	
	$('#artistSelect').children().remove();
	$('#eventSelect').children().remove();
	$('#isAllGrandChild').attr('checked', false);
	$('#isAllArtist').attr('checked', false);
    $('#isAllEvent').attr('checked', false);	 
	var child = childID;
	 
	 var url = "GetCategory?child="+child;	
	 if(child!=''){
	 $.ajax({
		url:url,
		success: function(res){
			 var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#grandChildSelect').append(rowText);
              }	
			 //$('#grandChildSelect').append(res);
			 
			}
		
		
	}); 
	 }
	 
 } 
 function  setArtistInfo(grandChildID){
	$('.error').hide(); 
	deleteAllRow('dataTable');
	$('#artistSelect').children().remove();
	$('#eventSelect').children().remove();
	$('#isAllArtist').attr('checked', false);	
    $('#isAllEvent').attr('checked', false);
	var grandChild = grandChildID;
	 if(grandChild!=''){
	 var url = "GetCategory?grandChild="+grandChild;		   
	 $.ajax({
		url:url,
		success: function(res){
				$('#processing').show();
			  var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#artistSelect').append(rowText);
              }
			 //$('#artistSelect').children().remove().end().append(res);
			 $('#processing').hide();
			}
		
		
	}); 
	 }
	 
 } 

  function  setEventInfo(artistId){
	$('.error').hide(); 
	deleteAllRow('dataTable');
	$('#eventSelect').children().remove();	
	$('#isAllEvent').attr('checked', false);	
	 if(artistId!=''){
	 var url = "GetCategory?artistId="+artistId;		   
	 $.ajax({
		url:url,
		success: function(res){
				$('#processing').show();
			  var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#eventSelect').append(rowText);
              }
			 $('#processing').hide();
			}
		
		
	}); 
	 }
	 
 } 

 function addRow(tableID,eventId,selectedExchange,selectedticketType) {			
			var artistName="";
			var eventName="";
			var table = document.getElementById(tableID);
			$.post("GetPurchasePriceByEvent", { eventId: eventId ,exchange: selectedExchange ,tickettype:selectedticketType},
			function(data) {		 
				//alert(data);
				var jsonArrayData = JSON.parse(data);	
				var count=0;
				for (var j = 0; j < jsonArrayData.length; j++) {
					var jsonData = jsonArrayData[j];
					for (var i = 0; i < jsonData.length; i++) {
					
						var singleData = jsonData[i];
						var eventId = singleData.id;
						var eventName = singleData.eventName;
						var artistName = singleData.artistName;
						////alert(artistID+"-"+artistName);
						var rowCount = table.rows.length;
						var row = table.insertRow(rowCount);
						row.id = eventId+"_row_"+singleData.exchange;
						
						row.class="rowClass";
						var selectCell = row.insertCell(0);
						var selectElement = document.createElement("input");
						selectElement.name=eventId+"_select_"+singleData.exchange+"_"+singleData.tickettype;
						selectElement.setAttribute("id", eventId+"_select_"+singleData.exchange+"_"+singleData.tickettype);	
						selectElement.setAttribute("class", "myClass");
						selectElement.type = "checkbox";			
						selectCell.appendChild(selectElement);

						var input = document.createElement("input");
						input.setAttribute("type", "hidden");
						input.setAttribute("name", eventId+"_"+singleData.exchange+"_"+singleData.tickettype);
						input.setAttribute("value", singleData.rowid);
						selectCell.appendChild(input);
						
						var cellEventId = row.insertCell(1);
						cellEventId.innerHTML = eventId;
						
						var cellArtistName = row.insertCell(2);
						cellArtistName.innerHTML = artistName;
						
						var cellEventName = row.insertCell(3);
						cellEventName.innerHTML = eventName;

						var cellEventDate = row.insertCell(4);
						cellEventDate.innerHTML = singleData.eventDate;

						var cellEventTime = row.insertCell(5);
						cellEventTime.innerHTML = singleData.eventTime;

						var cellVenue = row.insertCell(6);
						cellVenue.innerHTML = singleData.venue;

						var cellExchange = row.insertCell(7);
						var items= ${siteList};			
						var elementExchange = document.createElement("input");
						elementExchange.id=eventId+"_exchange_"+singleData.exchange+"_"+singleData.tickettype;
						elementExchange.name=eventId+"_exchange_"+singleData.exchange+"_"+singleData.tickettype;
						

						elementExchange.type = "text";
						elementExchange.value=singleData.exchange;
						elementExchange.setAttribute("readonly","true");
						cellExchange.appendChild(elementExchange);

						var cellTicketTypes = row.insertCell(8);
						var tickettypes= ${ticketTypeList};	
						///alert('${ticketTypeList}');
						var elementTicketTypes = document.createElement("input");
						//alert(singleData.exchange);
						elementTicketTypes.id=eventId+"_tickettype_"+singleData.exchange+"_"+singleData.tickettype;
					elementTicketTypes.name=eventId+"_tickettype_"+singleData.exchange+"_"+singleData.tickettype;
						elementTicketTypes.setAttribute("readonly","true");
						
						elementTicketTypes.type = "text";
						elementTicketTypes.value=singleData.tickettype;
						cellTicketTypes.appendChild(elementTicketTypes);

						var cellServiceFee = row.insertCell(9);
						var elementServiceFee = document.createElement("input");

						elementServiceFee.id=eventId+"_servicefee_"+singleData.exchange+"_"+singleData.tickettype;
						elementServiceFee.name=eventId+"_servicefee_"+singleData.exchange+"_"+singleData.tickettype;
						
						elementServiceFee.type = "text";
						elementServiceFee.value =singleData.servicefee;

						var elementCurrencyType = document.createElement("select");
						var currencyTypes = {"0":"$","1":"%"}
						elementCurrencyType.id=eventId+"_currencytype_"+singleData.exchange+"_"+singleData.tickettype;
						elementCurrencyType.name=eventId+"_currencytype_"+singleData.exchange+"_"+singleData.tickettype;			

						for ( var key1 in currencyTypes) {			
							var ov1 = document.createElement("option");				
							ov1.value = key1; 
							ov1.appendChild(document.createTextNode(currencyTypes[key1]))
							elementCurrencyType.appendChild(ov1);
						}
						elementCurrencyType.value=singleData.currencytype;
						cellServiceFee.appendChild(elementServiceFee);
						cellServiceFee.appendChild(elementCurrencyType);


						var cellShipping = row.insertCell(10);
						var elementShipping = document.createElement("input");
						elementShipping.id=eventId+"_shipping_"+singleData.exchange+"_"+singleData.tickettype;
						elementShipping.name=eventId+"_shipping_"+singleData.exchange+"_"+singleData.tickettype;
						
						elementShipping.type = "text";
						elementShipping.value =singleData.shipping;
						
						cellShipping.appendChild(elementShipping);
						
						var elementExist = document.createElement("input");
						elementExist.id=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_exist";
						elementExist.name=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_exist";
						elementExist.type = "hidden";
						elementExist.value =singleData.exist;
						cellServiceFee.appendChild(elementExist);
						
						var elementRowId = document.createElement("input");
						elementRowId.id=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_id";
						elementRowId.name=eventId + "_" + singleData.exchange + "_" + singleData.tickettype + "_id";
						elementRowId.type = "hidden";
						elementRowId.value =singleData.rowId;
						
						cellServiceFee.appendChild(elementRowId);
						
						var cell7 = row.insertCell(11);
						if(i==(jsonData.length-1)){
							if(i==(jsonData.length-1)){
								//alert(i+"-"+(jsonData.length-1))
								var row1 = table.insertRow(rowCount+1);
								row1.id = j+"_"+i+"_row";
								row1.insertCell(0);
								row1.insertCell(1);
								row1.insertCell(2);
								row1.insertCell(3);
								row1.insertCell(4);
								row1.insertCell(5);
								row1.insertCell(6);
								row1.insertCell(7);
								$("#"+j+"_"+i+"_row").attr("class", "darken");
								count++;
							}
						
						}			
						count++;		
					}
				}
				
				//}
			}); 
            
			//}
			//}
			//}
        }
		
        function deleteRow(tableID) {
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
 
            for(var i=0; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            }
            }catch(e) {
                //alert(e);
            }
        }
		function deleteAllRow(tableID) {
            try {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
			////alert(rowCount);
            for(var i=2; i<rowCount; i++) {
                var row = table.rows[i];                
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }
 
            
            }catch(e) {
                //alert(e);
            }
        }
		
		function selectAll(){
		$(".myClass").each(function(){
			this.checked=true;
		});
		}
		function selectNone(){
			$(".myClass").each(function(){
				this.checked=false;
			});
		}
		
		function copyAllServiceFee(){
		if($("#copyAllServiceFeeCheckbox").attr('checked')){
		selectAll();}
		var isFirst=true;
		var serviceFee;
		var serviceFeeType;		
		var shippingFee;
		var tempServiceFeeId="";
		var tempServiceFeeTypeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllServiceFeeCheckbox").attr('checked')){
				return;
			}
			
			var serviceFeeId = this.id;
			tempServiceFeeId = serviceFeeId.replace("select", "servicefee");
			var serviceFeeTypeId =  this.id+""; 
			tempServiceFeeTypeId = serviceFeeTypeId.replace("select", "currencytype");
			
			if(isFirst){
			
				if(!isFloat($("#"+tempServiceFeeId).val()) && !isInteger($("#"+tempServiceFeeId).val())){
					alert($("#"+tempServiceFeeId).val() + " is not valid.");
					$("#"+tempServiceFeeId).focus();
					return false;
				}				
				isFirst=false;
				serviceFee=$("#"+tempServiceFeeId).val();				
				serviceFeeType=$("#"+tempServiceFeeTypeId).val();	
					
			}
			else{		
				
				$("#"+tempServiceFeeId).val(serviceFee);
				$("#"+tempServiceFeeTypeId).val(serviceFeeType);			

			}
		});
	}
		function copyAllShipping(){
		if($("#copyAllShippingCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var shippingFee;
		var tempShippingId="";		
		$(".myClass").each(function(){
			if(!$("#copyAllShippingCheckbox").attr('checked')){
				return;
			}
			
			var shippingId = this.id;
			tempShippingId = shippingId.replace("select", "shipping");
			
			
			if(isFirst){
			
				if(!isFloat($("#"+tempShippingId).val()) && !isInteger($("#"+tempShippingId).val())){
					alert($("#"+tempShippingId).val() + " is not valid.");
					$("#"+tempShippingId).focus();
					return false;
				}				
				isFirst=false;
				shippingFee=$("#"+tempShippingId).val();								
					
			}
			else{		
				
				$("#"+tempShippingId).val(shippingFee);				

			}
		});
	}
	function copyAllExchange(){
		if($("#copyAllExchangeCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var exchange;
		var tempExchangeId="";
		var tempFirstExchangeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllExchangeCheckbox").attr('checked')){
				return;
			}
			
			var exchangeId = this.id;
			//alert(exchangeId);
			tempFirstExchangeId = exchangeId.replace("select", "exchange");
			tempExchangeId = exchangeId.split("_")[0]+"_exchange";
			
			if(isFirst){		
				if($("#"+tempFirstExchangeId).val()=='0'){
					alert("Please select proper exchange");
					$("#"+tempFirstExchangeId).focus();
					return false;
				}				
				isFirst=false;
				exchange=$("#"+tempFirstExchangeId).val();								
					
			}
			else{		
				
				$("#"+tempExchangeId).val(exchange);				

			}
		});
	}
	
	function copyAllTicketType(){
		if($("#copyAllTicketTypeCheckbox").attr('checked')){
		selectAll();
		}
		var isFirst=true;			
		var ticketType;
		var tempTypeId="";
		var tempFirstTypeId="";
		$(".myClass").each(function(){
			if(!$("#copyAllTicketTypeCheckbox").attr('checked')){
				return;
			}
			
			var tickettypeId = this.id;
			//alert(exchangeId);
			tempFirstTypeId = tickettypeId.replace("select", "tickettype");
			tempTypeId = tickettypeId.split("_")[0]+"_tickettype";
			
			if(isFirst){		
				if($("#"+tempFirstTypeId).val()=='0'){
					alert("Please select proper ticket type");
					$("#"+tempFirstTypeId).focus();
					return false;
				}				
				isFirst=false;
				ticketType=$("#"+tempFirstTypeId).val();								
					
			}
			else{		
				
				$("#"+tempTypeId).val(ticketType);				

			}
		});
	}
		
	function isInteger(value){
		var integerExpression=/^\d+$/;
		return integerExpression.test(value);
	}
	
	function isFloat(value){
		var integerExpression=/^\d+.{0,1}\d{0,2}$/;
		return integerExpression.test(value);
	}
	
	function loadData(){
		var selectedEvents="";
		var selectedExchange="";
		var selectedTicketType="";
		rowIncrement=3;
		deleteAllRow("dataTable");		
		$("#eventSelect option:selected").each(function () {
                selectedEvents += $(this).val() + ";";
				////alert($(this).val());
				
        });
		$("#exchangeSelect option:selected").each(function () {
                selectedExchange += $(this).val() + ";";
				////alert($(this).val());
		});
		$("#ticketTypeSelect option:selected").each(function () {
                selectedTicketType += $(this).val() + ";";
				///alert($(this).val());
		});
			  
		addRow("dataTable",selectedEvents,selectedExchange,selectedTicketType);
	}
	
</script>
<style>
table#dataTable {
    
	border-color: #600;
    border-width: 1px 1px 1px 1px;
    border-style: solid;	
 }
 table#dataTable td {
    
   
	border-color: #600;
    border-width: 1px 1px 0 0;
    border-style: solid;
    margin: 0;
    padding: 4px;
   
 } 
  table#dataTable tr {
    
   
	border-color: #600;
    border-width: 1px 1px 1px 1px;
    border-style: solid;
 }
 .darken { 
border: 2px solid; 
     background-color: #FFC;
 }
 .tabelRow{
	display: table-header-group;
	vertical-align: middle;
	border-color: inherit;
}
.tableCell{
	background-color: #850001;
	background-repeat: no-repeat;
	border-left: 2px solid #C10F0F;
	border-right: 2px solid #4D0101;
	padding-top: 4px;
	padding-bottom: 4px;
	color:white;
}
 .rowClass {
    border: 1px solid;
 }
 .leftcol {
    font-weight: bold;
    text-align: left;
    width: 150px;
    background-color: #CCCCCC;
 }
</style>
<div id="breadCrumbPath" class="breadCrumbPathAdmin">
  <a href="..">Home</a> &gt;
Price Management
</div>

<c:if test="${not empty info}">
	<div class="info"><div class="infoText">${info}</div></div>
</c:if>

<c:if test="${not empty error}">
	<div class="error">
		<div class="errorText">
			${error}
		</div>
	</div>
</c:if>


<h1>Price Management</h1>
<body>
<div align="center">
<form id="priceManagementForm" action="EditorPriceManagement" method="Post">
<table>
<tr>
<td align="center"><b>Parent Category :</b> Check  All: <input type="checkbox" name="isAllParent" <c:if test="${isAllParentSelected == 'on'}">checked</c:if> id="isAllParent"/>
</td>
<td align="center"><b>Child Category :</b> Check  All: <input type="checkbox" name="isAllChild" <c:if test="${isAllChildSelected == 'on'}">checked</c:if> id="isAllChild" />
</td>
<td align="center"><b>Grand Child Category :</b> Check  All: <input type="checkbox" name="isAllGrandChild" <c:if test="${isAllGrandChildSelected == 'on'}">checked</c:if> id="isAllGrandChild"/>
</td>
</tr>
<tr><td>
<select name="parentSelect" id="parentSelect" style="width: 300px" multiple size="5" >
 <c:forEach var="parent" items="${parentList}">
 <c:set var="a1" value="false" />
 <c:forEach var="selected" items="${parentSelected}">
  <c:if test="${parent.id == selected}">
  <c:set var="a1" value="true" />
  </c:if>
  </c:forEach>
    <option value="${parent.id}" <c:if test="${a1 == 'true'}">selected</c:if>>${parent.name}</option>
  </c:forEach>
</select> 
</td>
<td> 
<select name="childSelect" id="childSelect" style="width: 300px" multiple size="5">
<c:forEach var="child" items="${childList}">
<c:set var="a2" value="false" />  
  <c:forEach var="selected1" items="${childSelected}">
  <c:if test="${child.id == selected1}">
  <c:set var="a2" value="true" />
  </c:if>
  </c:forEach>
    <option value="${child.id}" <c:if test="${a2 == 'true'}">selected</c:if>>${child.name}</option>
  </c:forEach>

</select>  
</td>
<td> 
<select name="grandChildSelect" id="grandChildSelect" style="width: 300px" multiple size="5">
<c:forEach var="grandChild" items="${grandChildList}">
<c:set var="a3" value="false" />  
  <c:forEach var="selected2" items="${grandChildSelected}">
  <c:if test="${grandChild.id == selected2}">
  <c:set var="a3" value="true" />
  </c:if>
  </c:forEach>
    <option value="${grandChild.id}" <c:if test="${a3 == 'true'}">selected</c:if>>${grandChild.name}</option>
  </c:forEach>
  
</select>  
</td>
</tr>
<tr><td colspan="3" align="center">
</td></tr>
<tr><td colspan="3" align="center">
<table width="100%">
<tr>
	<td align="center"><b>Artist :</b>  Check  All: <input type="checkbox" name="isAllArtist" <c:if test="${isAllArtistSelected == 'on'}">checked</c:if> id="isAllArtist" /></td>
	<td align="center"><b>Event :</b>  Check  All: <input type="checkbox" name="isAllEvent" <c:if test="${isAllEventSelected == 'on'}">checked</c:if> id="isAllEvent" /></td>
</tr>
<tr>
<td align="center">
<select name="artistSelect" id="artistSelect" style="width: 400px" multiple size="10" >
	<c:forEach var="artist" items="${artistList}">
		<c:set var="ar4" value="false" />  
		<c:forEach var="artistId" items="${artistSelected}">
			<c:if test="${artist.id == artistId}">
				<c:set var="ar4" value="true" />
			</c:if>
		</c:forEach>
		<option value="${artist.id}" <c:if test="${ar4 == 'true'}">selected</c:if>>${artist.name}</option>
	</c:forEach>
 
</select> 

</td>
<td align="center">
<select name="eventSelect" id="eventSelect" style="width: 500px" multiple size="10" >
<c:forEach var="event" items="${eventList}">
  <c:set var="a5" value="false" />  
  <c:forEach var="eventId" items="${eventSelected}">
	  <c:if test="${event.id == eventId}">
		<c:set var="a5" value="true" />
	  </c:if>
  </c:forEach>
	<option value="${event.id}" <c:if test="${a5 == 'true'}">selected</c:if>>${event.name}</option>
  </c:forEach>
  
</select> 

</td>
</tr>
<tr>
	<td align="center"><b>Exchange :</b> </td>
	<td align="center"><b>Ticket Type :</b></td>
</tr>
<tr>
<td align="center">

			<select name="exchangeName" id="exchangeSelect" style="width: 200px">
			<option value="--">--Select--</option>
			<c:forEach var="exchange" items="${exchangeList}">
			  <option value="${exchange}" <c:if test="${exchangeSelected == exchange}">selected</c:if>>${exchange}</option>
			</c:forEach>	
			</select> 
		</td>
		<td align="center">
		
			<select name="ticketTypeName" id="ticketTypeSelect" style="width: 200px">
			<option value="--">--Select--</option>
		<!-- 	<c:forEach var="tickettype" items="${ticketTypes}">
			  <option value="${tickettype}" <c:if test="${ticketTypeSelected == tickettype}">selected</c:if>>${tickettype}</option>
			</c:forEach>
		 -->
			</select>  
		</td> 
</tr>
</table>
</td>
</tr>
<tr><td colspan="3" align="center"><input id="artistSelectorAction" type="hidden" name="action" value="${action}" /><input type="button" class="medButton" value="Get Data" id="getData" /> &nbsp;<br/></td></tr>

<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr><td colspan="3"></td></tr>
<tr><td colspan="3" align='center'><table><tr><td colspan="8" align="right">
<input type="button" class="medButton" value="Save Selected" id="saveButton" onclick="$('#artistSelectorAction').val('save'); $('#priceManagementForm').submit()"/>
<input type="button" class="medButton" value="Delete Selected" onclick="$('#artistSelectorAction').val('delete'); $('#priceManagementForm').submit()"/></td></tr></table></td></tr>

 <tr><td colspan="3" align='center'>   
 
  </td></tr>

</table>
<TABLE id="dataTable" width="100%"  align="center">
<tr><td colspan='3'>Select : <a href="javascript:selectAll();">All</a>&nbsp;|
	<a href="javascript:selectNone()">None</a>&nbsp;|&nbsp;<a href="javascript:loadData()">Undo Copy All</a></td>
	<td></td><td></td><td></td><td></td>
	<td></td><td></td><td><input onClick="javascript:copyAllServiceFee()" type="checkbox" id="copyAllServiceFeeCheckbox" > Copy All</td><td><input onClick="javascript:copyAllShipping()" type="checkbox" id="copyAllShippingCheckbox" > Copy All</td></tr>
     <tr class="tableRow"><td class="tableCell"><b>Select</b></td><td class="tableCell"><b>Event Id</b></td><td class="tableCell"><b>Artist Name</b></td><td class="tableCell"><b>Event Name</b></td><td class="tableCell"><b>Event Date</b></td><td class="tableCell"><b>Event Time</b></td><td class="tableCell"><b>Venue</b></td><td class="tableCell"><b>Exchange</b></td><td class="tableCell"><b>Ticket Type</b></td><td class="tableCell"><b>Service Fees</b></td><td class="tableCell"><b>Shipping</b></td><td class="tableCell"></td></tr>  
    </TABLE>
<span id="processing" style="position:fixed;bottom:280px;right:600px;cursor:pointer"><img src="../images/process-running.gif" align="absbottom" />Loading...</span>
<script type="text/javascript">
$('#processing').hide();

</script>
</form>
</div>
</body>
