<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Manage TCAP
</div>


<h1>Manage Presale</h1>

<c:if test="${not empty info}">
	<div class="info"><div class="infoText">${info}</div></div>
</c:if>

<b>Filter:</b> <br><br>
<b>Artist :</b>
<span id="artistLabelSpanId"  <c:if test="${not empty artistId && artistId != 0 }">style="display: black;"</c:if> 
	<c:if test="${empty artistId || artistId == 0 }">style="display: none;"</c:if>  >
	<span id="artistNameDiv" >
		<c:choose>
			<c:when test="${not empty artistName}">
				${artistName}
			</c:when>
			<c:otherwise>
					-
			</c:otherwise>
		
		</c:choose>
	</span>	
		<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
	</span>
	
	
	<span id="artistSelectSpanId" <c:if test="${not empty artistId && artistId != 0 }">style="display: none;"</c:if> 
		<c:if test="${empty artistId || artistId == 0 }">style="display: black;"</c:if> >
	
		<input type="text" name ="autoArtist" id ="autoArtist" >
</span>

<br />
AND 
<br />
<b>Venue :</b> 

<c:choose>
	<c:when test="${(empty artistId || artistId == 0) && empty venues}">
		<span id="venueLabelSpanId"  <c:if test="${not empty venueId && venueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty venueId || venueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName }">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty venueId && venueId != 0 }">style="display: none;"</c:if> 
				<c:if test="${empty venueId || venueId == 0 }">style="display: block;"</c:if> >
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
	</c:when>
	<c:otherwise>
		<select onchange="changeFilterVenue(this.value);">
			<option value="">Select</option>
			<c:forEach var="venue" items="${venues}">
				<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == venueId}">selected</c:if>>${venue.building}</option>
			</c:forEach>
		</select>
	</c:otherwise>
</c:choose>

<c:if test="${venueId != null && venueId != 0}">
	<c:set var="link" value="&venueId=${venueId}"></c:set>
	<c:set var="link1" value="&venueId=${venueId}"></c:set>
</c:if>
<c:if test="${artistId != null && artistId != 0}">
	<c:set var="link" value="&artistId=${artistId}"></c:set>
	<c:set var="artistLink" value="&artistId=${artistId}"></c:set>
</c:if>
<br /><br />


<c:choose> 
<c:when test="${not empty presaleEvents}">

<form action="EditorManagePresaleEvent" method="post" id="editorManagePresaleEvent">
<input type="hidden" id="action" name="action">
<input type="hidden" id="artistId" name = "artistId" value="${artistId}"/>
<input type="hidden" id="venueId" name = "venueId" value="${venueId}"/>
<div align="center" >
	<input type="button" class ="medButton" value="Update" onclick="submitForm()"/>
</div>
<display:table class="list" name="${presaleEvents}" id="presale"  requestURI="EditorManagePresaleEvent" pagesize="25" export="true">
  <display:column title='<input type="checkbox" name ="selectAll" id ="selectAll"/>'>
  	<input type="checkbox" name ="${presale.event.id}-${presale.event.venueId}-checkbox" id ="${presale.event.id}-${presale.event.venueId}-checkbox" class="selectCheckBox"/>

  </display:column>
	<display:column style="width:150px" title="Name" sortable="true" property="event.name" href="EditorEditEvent?action=edit${artistLink}${link1}" paramId="id" paramProperty="event.id"/>
	<display:column title="Date"  sortable="true"  property="event.formatedDate" />
	<display:column title="VenueId"  sortable="true" property="event.venueId"/>
	<display:column title="Venue"  sortable="true" property="event.venue.building"/> 
	<display:column title="Artist"  sortable="true" property="event.artist.name"/>
	<display:column title="Parent"  sortable="true" property="event.artist.grandChildTourCategory.childTourCategory.tourCategory.name"/>
	<display:column title="Child"  sortable="true" property="event.artist.grandChildTourCategory.childTourCategory.name"/>
	<display:column title="Grand Child"  sortable="true" property="event.artist.grandChildTourCategory.name"/>
	<%-- <display:column title="Current Category"  sortable="true" property="event.artist.grandChildTourCategory.name"/> --%>
	<display:column title="Venue Category"  sortable="true" property="event.uniqueCategoryGroupName"/>
	
	<display:column title="Presale Event"  sortable="true">
	<input type="hidden" id="${presale.event.id}-${presale.event.venueId}-createdDate" name = "${presale.event.id}-${presale.event.venueId}-createdDate" value="${presale.createdDate}"/>
		<c:choose>
				<c:when test="${presale.event.presaleEvent}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Presale Assigned Date' >
		<c:choose>
				<c:when test="${presale.createdDate ne null}">
					<fmt:formatDate value='${presale.createdDate}' pattern='MM/dd/yyyy hh:mm a' />
				</c:when>
				<c:otherwise>-</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='Assign to Category <br/><input type="checkbox" name ="selectAllCategory" id ="selectAllCategory"/>'>
		<select id="${presale.event.id}-${presale.event.venueId}-venueCategory" name="${presale.event.id}-${presale.event.venueId}-venueCategory" class="venueCategory" onChange ="checkThis($(this).attr('id'))">
			<option value="0">-- Select Category --</option>
			<c:forEach var="venueCategory" items="${presale.venueCategoryList}">
				<option value="${venueCategory.id}" <c:if test="${presale.assignToVenueCategory.id == venueCategory.id}">selected</c:if> >${venueCategory.categoryGroup}</option>
			</c:forEach>
		</select>
	</display:column>
	<display:column title='Apply Date <br/> <input type="checkbox" name ="selectAllDate" id ="selectAllDate">' >
		<input type="text" value ="<fmt:formatDate value='${presale.applyDate}' pattern='MM/dd/yyyy' />" id="${presale.event.id}-${presale.event.venueId}-applyDate" name="${presale.event.id}-${presale.event.venueId}-applyDate" class="applyDateClass" onChange ="checkThis($(this).attr('id'))"/>
	</display:column>
	
	<display:column style="width:150px" title='Apply Time <br/> <input type="checkbox" name ="selectAllHourMinute" id ="selectAllHourMinute">' >
		Hr :
		<select id="${presale.event.id}-${presale.event.venueId}-applyDateHour" name="${presale.event.id}-${presale.event.venueId}-applyDateHour" class="applyDateHour" onChange ="checkThis($(this).attr('id'))">
			
			<option value="0" <c:if test="${presale.applyDateHour eq 0}">selected="selected"</c:if>>0</option>
  			<c:forEach var="hour" begin="1" end="23" step="1">
  			<option value="${hour}" <c:if test="${presale.applyDateHour eq hour}">selected="selected"</c:if>>${hour}</option>
  			
  			</c:forEach>
		</select>
		Min :
		<select id="${presale.event.id}-${presale.event.venueId}-applyDateMinute" name="${presale.event.id}-${presale.event.venueId}-applyDateMinute" class="applyDateMinute" onChange ="checkThis($(this).attr('id'))">
			<option value="0" <c:if test="${presale.applyDateMinute eq 0}">selected="selected"</c:if> >0</option>
  			<c:forEach var="minute" begin="1" end="59" step="1">
  			<option value="${minute}" <c:if test="${presale.applyDateMinute eq minute}">selected="selected"</c:if> >${minute}</option>
  			</c:forEach>
		</select>

	</display:column>
    
</display:table>

<div align="center" >
	<input type="button" class ="medButton" value="Update" onclick="submitForm()"/>
</div>
</form>
</c:when >

<c:otherwise>
There is no event for selected option.
</c:otherwise>
</c:choose>


<script type="text/javascript">
  
function checkThis(id){
	id = id.replace('venueCategory','checkbox');
	id = id.replace('applyDate','checkbox');
	id = id.replace('applyDateHour','checkbox');
	id = id.replace('applyDateMinute','checkbox');
	$('#' + id).attr('checked', true);
}
$(function() {
    $(".applyDateClass").datepicker({
	dateFormat: 'mm/dd/yy',
    minDate: 0
	});
});
  
$('#selectAllCategory').click(function(){

  var thisCheck = $(this);
  if (thisCheck.is(':checked') ) {
	var isFirst=true;
	var value = '';
	var venueId = '';
	var currentVenueId = '';
	$('.venueCategory').each(function() {
		var id = this.id;
		if(isFirst){
			isFirst=false;
			venueId = id.split("-")[1];
			eventId = id.split("-")[0];
			value = $(this).val();
			$('#' + eventId + '-' + venueId + '-checkbox').attr('checked', true);
		}else{
			var currentVenueId = id.split("-")[1];
			var currentEventId = id.split("-")[0];
			if(currentVenueId == venueId){
				$(this).val(value);
				$('#' + currentEventId + '-' + currentVenueId + '-checkbox').attr('checked', true);
			}
		}
	});

  }
});

$('#selectAllDate').click(function(){
  var thisCheck = $(this);
  if(thisCheck.is(':checked') ) {
	var isFirst=true;
	var value = '';
	var eventId = '';
	var venueId = '';
	var currentVenueId = '';
	$('.applyDateClass').each(function() {
		var id = this.id;
		if(isFirst){
			isFirst=false;
			venueId = id.split("-")[1];
			eventId = id.split("-")[0];
			value = $(this).val();
			$('#' + eventId + '-' + venueId + '-checkbox').attr('checked', true);
		}else{
			var currentVenueId = id.split("-")[1];
			var currentEventId = id.split("-")[0];
			if(currentVenueId == venueId){
				$(this).val(value);
				$('#' + currentEventId + '-' + currentVenueId + '-checkbox').attr('checked', true);
			}
		}
	});
  }
});

$('#selectAll').click(function(){
	var isChecked = $(this).is(':checked');

	$('.selectCheckBox').each(function() {
		$(this).attr('checked', isChecked);
	});
});

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val(row[1]);
		changeFilterArtist(row[0]);
	});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#artistId').val('');
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#venueId').val('');
}

$('#autoVenue').autocomplete("AutoCompleteVenue", {
	width: 650,
	max: 1000,
	minChars: 2,		
	formatItem: function(row, i, max) {
		return row[1];
	}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		changeFilterVenue(row[0]);
});

function changeFilterArtist(artistId) {
	document.location.href = "EditorManagePresaleEvent?artistId=" + artistId;
	if(${venueId!=null}){
		document.location.href = "EditorManagePresaleEvent?artistId=" + artistId +"&venueId="+"${venueId}";
	}else{    
		document.location.href = "EditorManagePresaleEvent?artistId=" + artistId;
	}
	
};

function changeFilterVenue(venueId) {
	if(${artistId!=null}){
		document.location.href = "EditorManagePresaleEvent?venueId=" + venueId +"&artistId="+"${artistId}";
	}else{    
         document.location.href = "EditorManagePresaleEvent?venueId=" + venueId;
	}
};
function isValidDate(date) {
    var temp = date.split('/');
    var d = new Date(temp[2] + '/' + temp[0] + '/' + temp[1]);
    return (d && (d.getMonth() + 1) == temp[0] && d.getDate() == Number(temp[1]) && d.getFullYear() == Number(temp[2]));
}
function submitForm() {
	var numberOfDaysToAdd = 7;
	var flag= true;
	var isSelected = false;
	$('.selectCheckBox:checkbox:checked').each(function () {
		isSelected = true;
		var id,value;
		
		id = this.id.replace('checkbox','applyDate');
		var applyDateStr = $("#"+id).val();
		var applyDate = new Date(applyDateStr);
		
		if(applyDateStr == "" || applyDate =="Invalid Date") {
			alert("Select Valid Apply Date.");
			$("#" + id ).focus();
			flag = false;
			return false;
		}
		id = this.id.replace('checkbox','applyDateHour');
		value = $("#"+id).val();
		applyDate.setHours(value);
		id = this.id.replace('checkbox','applyDateMinute');
		value = $("#"+id).val();
		applyDate.setMinutes(value);
		
		id = this.id.replace('checkbox','createdDate');
		var maxAppliyDateRange = $("#"+id).val();
		if(maxAppliyDateRange != "") {
			maxAppliyDateRange = new Date(maxAppliyDateRange);
		} else {
			maxAppliyDateRange = new Date();
		}
		maxAppliyDateRange.setDate(maxAppliyDateRange.getDate() + numberOfDaysToAdd); 
		
		if(applyDate.getTime()>maxAppliyDateRange.getTime()) {
			alert("Select Apply Date within "+numberOfDaysToAdd+" days from Presale Assigned Date.");
			id = this.id.replace('checkbox','applyDate');
			$("#" + id ).focus();
			flag = false;
			return false;
		} 
		
	});
	if(!flag) {
		return flag;
	}
	if(!isSelected){
		alert('Select minimum one Event for Update.');
		flag=false;
		return false;
	}
	if(flag) {
		$("#action").val('update');
		$("#editorManagePresaleEvent").submit();
	}
}


  	

$('#selectAllHourMinute').click(function(){

	  var thisCheck = $(this);
	  if (thisCheck.is(':checked') ) {
		var isFirst=true;
		var value = '';
		var venueId = '';
		var currentVenueId = '';
		$('.applyDateHour').each(function() {
			var id = this.id;
			if(isFirst){
				isFirst=false;
				venueId = id.split("-")[1];
				eventId = id.split("-")[0];
				value = $(this).val();
				$('#' + eventId + '-' + venueId + '-checkbox').attr('checked', true);
			}else{
				var currentVenueId = id.split("-")[1];
				var currentEventId = id.split("-")[0];
				if(currentVenueId == venueId){
					$(this).val(value);
					$('#' + currentEventId + '-' + currentVenueId + '-checkbox').attr('checked', true);
				}
			}
		});
		
		/* $('.applyDateMinute').each(function() {
			var id = this.id;
			if(isFirst){
				isFirst=false;
				venueId = id.split("-")[1];
				eventId = id.split("-")[0];
				value = $(this).val();
				$('#' + eventId + '-' + venueId + '-checkbox').attr('checked', true);
			}else{
				var currentVenueId = id.split("-")[1];
				var currentEventId = id.split("-")[0];
				if(currentVenueId == venueId){
					$(this).val(value);
					$('#' + currentEventId + '-' + currentVenueId + '-checkbox').attr('checked', true);
				}
			}
		}); */

	  }
	});

</script>
