<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript">

/* $(document).ready(function(){
	$('#btnSubmit').click(function(){
		if(${fromTcap != null}){
			$('tcap').val();
		}
		$('#editEvent').submit();
	});
	
}); */
$(document).ready(function(){
	if(${param.action == 'create'}){
			$('#Audit1').hide();
	}
	
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val('');
		$("#tempArtistId").val(row[0]);
		$('#artistNameDiv').text(row[1]);
			
		$('#artistLabelSpanId').show();
		$('#artistSelectSpanId').hide();
	});
	
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val('');
		$("#tempVenueId").val(row[0]);
		$('#venueNameDiv').text(row[1]);
			
		$('#venueLabelSpanId').show();
		$('#venueSelectSpanId').hide();
		
		getVenueCategory(row[0]);
	});
	$('#autoMergeEvent').autocomplete("AutoCompleteMergeEvents", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoMergeEvent').val('');
		$("#eventCmb").val(row[0]);
		$('#mergeEventNameDiv').text(row[1]);
			
		showMergeButton();
	});
});

function checkPresaleEventCSV() {
	if($("#presaleEvent1").attr('checked')) {
		//var csv = $('#venueCategoryId').val();
		//var $sel = $("#venueCategoryId");
		//var value = $sel.val();
		//var text = $("option:selected",$sel).text();
		var artistId = $('#tempArtistId').val();
		if(artistId != "") {
			var url = "GetTourCategoryByArtist?aId=" + artistId;
			$.ajax({
				url: url,
				success: function(data){
					if(data != null && data != "") {
						var parentCategory = data.trim().split("|")[1];
				  	   $('#tourCategory').val(parentCategory.trim());
					}
			      
				},
				async:false
			});
		}
					
		var tourCategory = $('#tourCategory').val();
		 text = $("option:selected",$("#venueCategoryId")).text();
		if(text =='') {
			alert("Select Venue category before selecting PRESALE Event.");
			$("#presaleEvent1").attr('checked',false);
			$("#venueCategoryId").focus();
			return false;
		} else if (tourCategory.toUpperCase()=="CONCERTS" && text.toUpperCase()!="END_STAGE" && text.toUpperCase() !="PRESALE_GA") {
			alert("Select END_STAGE or PRESALE_GA Venue category for CONCERT Events before selecting as presale.");
			$("#presaleEvent1").attr('checked',false);
			$("#venueCategoryId").focus();
			return false;
		}
	}
	return true;
}
function popupEventAudit(eventId){
	var url = "EventAudit?eventId="+eventId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
</script>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a>
	<c:if test = "${not empty event.artistName}">
		&gt; <a href="EditorManageArtists">${event.artistName}</a> 
		&gt; <a href="EditorDetailTour?artistId=${event.artistId}">${event.artistName}</a>
	</c:if> 
    &gt;
    <c:choose>
	  <c:when test="${param.action == 'create'}">Add a new event</c:when>
	  <c:otherwise>Edit event ${event.name}</c:otherwise>
    </c:choose>
</div>

<h1>
<c:choose>
	<c:when test="${param.action == 'create'}">Add a new event</c:when>
	<c:otherwise>Edit event</c:otherwise> 
</c:choose>
</h1>
<fmt:formatDate pattern="yyyy" value="${event.localDate}" var="eventYear" />
<fmt:formatDate pattern="M" value="${event.localDate}" var="eventMonth" />
<fmt:formatDate pattern="d" value="${event.localDate}" var="eventDay" />
<form:form commandName="event" method="POST" id="editEvent" onsubmit="return validateForm();">
    <input type="hidden" name="action" value="${param.action}"/>
    <input type="hidden" id= "tcap" name="tcap" value="${fromTcap}"/>
    <input type="hidden" name="eventId" id="eventId" value="${event.id}"/>
    <input type="hidden" id="tourCategory" value=""/>
	<table>
		<%-- <tr>
			<td>Id</td>
			<td>
                <c:choose>
                  <c:when test="${param.action == 'create'}">
				    <form:input path="id" cssClass="longInput" />(If not entered, TMAT will create one)
				  </c:when>
				  <c:otherwise>
				    ${event.id}
				  </c:otherwise>
				</c:choose>
				<form:errors path="id" cssClass="error" />			
			</td>
		</tr> --%>
		<tr>
			<td><b>Name</b></td>
			<td>
				<form:input path="name" cssClass="longInput" />
				<form:errors path="name" cssClass="error" />			
			</td>
		</tr>
		
		<%-- <tr>		
			<td><b>Artist:</b></td>
			<td>
				<form:select path="artistId" >
				<form:option value="">----SELECT---</form:option>
					<c:forEach var="artist" items="${artists}">
						<form:option value="${artist.id}">${artist.name}</form:option>
					</c:forEach>
				</form:select>
				<form:errors path="artistId" cssClass="error" />	
			</td>
		</tr> --%>
		
		<tr>
			<td><b>Artist:</b></td>
			<td>
				
				<%-- <input type="hidden" name="artistId" id="artistId" value="${event.artistId}"> --%>
				
				<span id="artistLabelSpanId"  <c:if test="${event.id ne null }">style="display: black;"</c:if> 
				<c:if test="${event.id eq null }">style="display: none;"</c:if>  >
				<span id="artistNameDiv" >
					<c:choose>
						<c:when test="${event.id ne null }">
							${event.artist.name}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
				</span>	
					<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"  />
				</span>
				
				
				<span id="artistSelectSpanId" <c:if test="${event.id ne null }">style="display: none;"</c:if> 
					<c:if test="${event.id eq null }">style="display: black;"</c:if> >
				
					<%-- <form:select path="tempArtistId" >
						<form:option value="">----SELECT---</form:option>
						<c:forEach var="artist" items="${artists}">
							<form:option value="${artist.id}">${artist.name}</form:option>
						</c:forEach>
					</form:select> --%>
					<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>
				<form:hidden path="tempArtistId"/>
				<form:errors path="artistId" cssClass="error" />				
			</td>
			
		</tr>
		
		<tr>
			<td><b>Date (YYYY/MM/DD)</b></td>
			<td>
				<b>TBD:</b> <form:radiobutton path="dateTbd" value="true" /> &nbsp; <b>or</b> &nbsp;
				<b>Date:</b> <form:radiobutton path="dateTbd" value="false" />					
				<select name="eventDateYear" id="eventDateYear">
				  <c:forEach var="year" begin="2009" end="2020" step="1">
				    <option value="${year}" <c:if test="${year == eventYear}">selected</c:if>>${year}</option>
				  </c:forEach>
				</select> /
				<select name="eventDateMonth" id="eventDateMonth">
				  <c:forEach var="month" begin="1" end="12" step="1">
				    <option value="${month}" <c:if test="${month == eventMonth}">selected</c:if>><fmt:formatNumber pattern="00" value="${month}" /></option>
				  </c:forEach>
				</select> /
				<select name="eventDateDay" id="eventDateDay">
				  <c:forEach var="day" begin="1" end="31" step="1">
				    <option value="${day}" <c:if test="${day == eventDay}">selected</c:if>><fmt:formatNumber pattern="00" value="${day}" /></option>
				  </c:forEach>
				</select>
				<input type="text" id="localDateCalendar" style="visibility:hidden;width:0px;" />
				
			</td>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td>
				<b>Time (HH:MM)</b>
			</td>
			<td>
				<b>TBD:</b> <form:radiobutton path="timeTbd" value="true" />&nbsp; <b>or</b> &nbsp;
			    <b>Time:</b> <form:radiobutton path="timeTbd" value="false" />			    			
			
				<form:select path="localTime.hours">
				  <c:forEach var="hour" begin="0" end="23" step="1">
				    <form:option value="${hour}"><fmt:formatNumber pattern="00" value="${hour}" /></form:option>
				  </c:forEach>
				</form:select>
				<b>:</b>
				<form:select path="localTime.minutes">
				  <c:forEach var="minute" begin="0" end="59" step="1">
				    <form:option value="${minute}"><fmt:formatNumber pattern="00" value="${minute}" /></form:option>
				  </c:forEach>
				</form:select>
				
				<form:errors path="localTime" cssClass="error" />			
  			</td>
		</tr>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<tr>
			<td><b>Venue</b></td>
			<td>
				
				<%-- <input type="hidden" name="venueId" id="venueId" value="${event.venueId}"> --%>
				
				<span id="venueLabelSpanId"  <c:if test="${event.id ne null }">style="display: black;"</c:if> 
				
				<c:if test="${event.id eq null }">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${event.id ne null }">
							${event.venue.building}, ${event.venue.city}, ${event.venue.state}, ${event.venue.country}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${event.id ne null }">style="display: none;"</c:if> 
				<c:if test="${event.id eq null }">style="display: black;"</c:if> >
				
					<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
					<c:forEach var="venue" items="${venues}">
						<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
					</c:forEach>
				</form:select> --%>
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
				<form:hidden path="tempVenueId"/>				
				<form:errors path="venueId" cssClass="error" />			
			</td>
			
		</tr>
		</tr>
		<tr>
			<td><b>Venue Category</b></td>
			<td>
				<form:select path="venueCategoryId">
					<form:option value="">--No Venue Category--</form:option>
					<c:forEach var="venueCategory" items="${venueCategories}">
						<form:option value="${venueCategory.id}">${venueCategory.categoryGroup}</form:option>
					</c:forEach>
				</form:select>
				<form:errors path="venueCategoryId" cssClass="error" />			
			</td>
		</tr>
		<%-- <tr>
			<td><b>Presale Event</b></td>
			<td>
				<form:checkbox path="presaleEvent" onclick="checkPresaleEventCSV();"/>
				<form:errors path="presaleEvent" cssClass="error" />			
			</td>
		</tr> --%>
		<tr>
		  <td><b>Type:</b></td>
		  <td>
			<form:select path="eventType">
 			  <form:option value="SPORT">SPORT/CONCERT</form:option>
 			  <form:option value="THEATER">THEATER</form:option>
 			 <%--  <form:option value="CONCERT">CONCERT</form:option> --%>
 			  <form:option value="OTHER">OTHER</form:option>
			</form:select>
		  </td>
		</tr>
		<%-- <tr>
			<td>No Price Event:</td>
		  	<td>
				<form:select path="noPrice">
				  <form:option value="">---SELECT---</form:option>
	 			  <form:option value="0">No</form:option>
	 			  <form:option value="1">Yes</form:option>
	 			</form:select>
		  	</td>
		</tr> --%>
		<tr>
			<td><b>TN Exchange Event:</b></td>
			<td>
				<%-- <form:select path="admitoneId">
					<form:option value="">--SELECT--</form:option>
	 				<c:forEach var="tnExchangeEvent" items="${tnExchangeEvents}">
						<form:option value="${tnExchangeEvent.eventId}">${tnExchangeEvent.eventName}-${tnExchangeEvent.formatedDate}-${tnExchangeEvent.venueName}</form:option>
					</c:forEach>
				</form:select> --%>
				<form:hidden path="tnPOSEventId"/>
				<span id="tnPOSEventName"  name="tnPOSEventName" >${event.tnPOSEventName}</span> <a id ="linkTNEvent" onclick="javascript:getTNPOSEvent()">(Select Different TN POS Event) </a>
				<input type="text" name ="selectedTNPOSEventName" id ="selectedTNPOSEventName">
				
			</td>
		</tr>
	</table>
	<br /><br />
	<input type="submit" name="submit" value="Save Changes" class="medButton" id="btnSubmit" />
	<!-- <input type="button" name="submit" value="Save Changes" class="medButton" id="btnSubmit" onClick="validateForm();" /> -->
	<input type="button" value="Audit"  class="medButton" id="Audit1" name="Audit" onClick="popupEventAudit(${event.id});"/>
	<input type="button" value="Cancel" class="medButton" onclick="callCancel('EditorDetailArtist?artistId=${event.artistId}');"  />
	
	<h2>Merge This Event</h2>
	<div class="info">
		<div class="infoText">
			MERGED EVENTS CANNOT BE UNDONE.
			<br/>
			THIS EVENT WOULD BE MERGED WITH THE SELECTED EVENT.
			<br/>
			THIS EVENT'S DETAILS WOULD BE DELETED. 
			<br/>
			THE SELECTED EVENT WOULD TAKE THIS EVENT'S SHORTS, TICKETS, AND CRAWLS.
		</div>
	</div> 
	Selected Event: 
	
<div id="mergeEventDialog" title="" style="display:none">
	<span id="mergeEventDialogMessage" align="center"></span>
</div>
	
	<%-- <select id="eventCmb" onchange="showMergeButton()">
		<option value="">NONE SELECTED</option>
		<c:forEach var="ev" items="${events}">
			<c:if test="${ev.id != event.id}">
				<option value="${ev.id}" >${ev.name} - ${ev.localDate} - ${ev.localTime} - ${ev.id} </option>
			</c:if>  
		</c:forEach>
	</select> --%>
	<input type="text" name ="autoMergeEvent" id ="autoMergeEvent">
	<input type="hidden" id="eventCmb" name="eventCmb" />
	<br />
	<b><span id="mergeEventNameDiv"></span></b>
	<br />
	<input id="mergeEventsButton" type="button" class="medButton" onclick="mergeEvents()"  value="Merge Events"  style="display:none"/>
	<h2>Event deletion</h2>
	<div class="info">
		<div class="infoText">
			Deleting event cannot be undone.
		</div>
	</div>
	<input type="button" value="Delete event" class="medButton" onclick="confirmRemove(${event.id});">

	<c:if test="${event.localDate!=null && (event.localDate < constants.currentDate)}">
		<h2>Event expiration</h2>
		This event date is in the past. You can expire it.
		<div class="info">
			<div class="infoText">
				Expiring an event will change the status of the event to 'EXPIRED', will disabled its crawls and will disable
				its tickets.
			</div>
		</div>
		<input type="button" value="Expire event" class="medButton" onclick="confirmExpire(${event.id});">
	</c:if>
	
</form:form>

<div id="deleteEventDialog" title="" style="display:none">
	<span id="deleteEventDialogMessage" align="center"></span>
</div>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#name').focus();
		$('#selectedTNPOSEventName').hide();
		$('#localDateCalendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime() - ((23 * 60 + 59) * 60) * 1000),
	        maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000),
	        beforeShow: function(input) {
	        	$('#localDateCalendar').val(($('#eventDateYear').val()) + "-" + $('#eventDateMonth').val() + "-" + $('#eventDateDay').val());
	        },
	        onSelect: function(dateText) {
				var tokens = dateText.split("-");
				$('#eventDateYear').val(tokens[0]);
				$('#eventDateMonth').val(tokens[1]);
				$('#eventDateDay').val(tokens[2]);
			}
		});
		
		
	});

	function callSubmit(){
		return true;
	}
	function getTNPOSEvent(){
		$('#linkTNEvent').hide();
		$('#selectedTNPOSEventName').show();
		$('#selectedTNPOSEventName').focus();
	}
	$('#selectedTNPOSEventName').autocomplete("AutoCompleteTNPOSEvents", {
	  		width: 550,
			max: 1000,
			minChars: 2,		
			formatResult: function(row) {
				return row[0];
			},
			startSearch: function() {
			},
			stopSearch: function(rows) {
			},
			highlight: function(value, term) {
				return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.trim(term.replace(/ +/g, ' ')).replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
			}
	});

	$('#selectedTNPOSEventName').result(function(event, row, formatted) {
		$("#selectedTNPOSEventName").hide();
		$("#linkTNEvent").show();
		$("#tnPOSEventName").text(row[0]) ;
 	  	$("#tnPOSEventId").val(row[1]);
	});
	
function validateForm(){
	var flag = true;
	if($("#name").val().trim()==""){
		alert('Please enter a name.');
		flag = false;
		return false;
	}
	if($("#tempArtistId").val().trim()==""){
		alert('Please select a artist.');
		flag = false;
		return false;
	}
	/* flag = checkPresaleEventCSV();
	if(!flag) {
		return false;
	} */
	if($("#noPrice") != null && $("#noPrice").val()=='1'){
		if($("#venueCategoryId").val()=='' || $("#venueCategoryId").val()=='0'){
			alert('NoPrice Event must have category group.');
			flag = false;
			return false;
		}
		if($("#admitoneId").val()==''){
			alert('NoPrice Event must be linked to TicketNetwork exchange event.');
			flag = false;
			return false;
		}
	}
	if(flag) {
		//$("#action").val(action);
		$("#editEvent").submit();
	}
	
}
	$("#deleteEventDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	$("#mergeEventDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

	function confirmExpire(eventId) {
		var r = confirm("Do you really want to expire this event?");
		if (r) {
			document.location.href="EditorManageExpiredEvents?action=expire&eventIds=" + eventId;
		}
	};

	function confirmRemove(eventId) {
		
		if(eventId == null || eventId =='') {
			alert('Event can not be empty.');
		} else {
		$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Please wait');		
		$('#deleteEventDialog').dialog('open');
		$('#deleteEventDialog').dialog('option', 'title', 'Please wait...');
		$('#deleteEventDialog').dialog('option', 'buttons', {});

        DataDwr.getNumTickets(eventId, function(response) {
			$('#deleteEventDialog').dialog('option', 'title', 'Delete event?');
        	$('#deleteEventDialogMessage').html(
        		"The event has " + response + " tickets attached to it. "
        		+ "<br /> If you delete it, all the associated tickets will be deleted as well. "
        		+ "<br /><br />Do you really want to delete it?");

			$("#deleteEventDialog").dialog('open');
			$("#deleteEventDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorDetailArtist?id=" + eventId + "&artistId=${event.artistId}&action=remove";			
					$('#deleteEventDialog').dialog('option', 'title', 'Deleting event...');
					$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting event...');		
					$("#deleteEventDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
		}
		
	};
	

	function showMergeButton() {
		if ($('#mergeEventsButton').css('display') == 'none') {
			$('#mergeEventsButton').show();
		}
	};
	
	function mergeEvents() {

		var mergeId = "${event.id}";
		var goodId = $('#eventCmb').val();

		var flag = true;
		if(mergeId == null || mergeId == '') {
			alert('Event can not be empty.');
			flag = false;
			return false;
		}
		if(goodId == null || goodId == '') {
			alert('Merge event can not be empty.');
			flag = false;
			return false;
		}
		
		if(flag) {
		$('#mergeEventDialogMessage').html(
        		"If you merge this event, all the associated short transactions,  "
        		+ "<br /> short broadcasts, tickets, and event synonyms will be "
        		+ "<br /> transferred from " + mergeId + " to " + goodId + ". "
        		+ "<br /> All the ${mergeId} event price adjustments, zone mappings, "
        		+ "<br /> and ticket listing crawls will be deleted as well. "
        		+ "<br /><br />Do you really want to merge this event?");
        $("#mergeEventDialog").dialog('open');
        $('#mergeEventDialog').dialog('option', 'title', 'Merge event?');
		$('#mergeEventDialog').dialog('option', 'buttons', {});
		$("#mergeEventDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeEventDialog').dialog('option', 'title', 'Merging event...');
				$('#mergeEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging event...');		
				$("#mergeEventDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeEvents(mergeId, goodId, function(response) {
				   $('#mergeEventDialogMessage').html(response + "!");
				   $("#mergeEventDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$(mergeEventDialog).dialog('close');

					        //return home
					        document.location.href = "EditorManageTours";
					   }
				   });
       			});			
			},
			Cancel: function() {
				$(mergeEventDialog).dialog('close');
			}
		}); 
		}
	};
	
	function getVenueCategory(venueId){
		$.ajax({
			url:"GetVenueCategoryByVenue?venueId="+venueId,
			success:function(result){
				var cats = result.split(",")
				var options = "<option value ='0'>--Select--</option>";
				for(i=0;i<cats.length-1;i++){
					var val = cats[i].split("-");
					options = options + "<option value ='" + val[1]+ "'>" + val[0] + "</option>";
				}
				$("#venueCategoryId").html(options);
			}
		});
	}
	
			
	function callCancel(defaultUrl){
		
		var url =document.getElementById("tcap").value;
		if(null == url || url == ""){
			url =defaultUrl ;
		}
		document.location.href=url;
	}
	
	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#tempVenueId').val('');
	}
	 
	function callChangeArtist(){ 
		$('#artistLabelSpanId').hide();
		$('#artistSelectSpanId').show();
		$('#tempArtistId').val('');
	}
	
	var eventId = document.getElementById("eventId").value;
	if(null == eventId || eventId == ""){
		callChangeVenue();
		callChangeArtist();
	} 
</script>