<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; <a href="EditorManageTours">Artist</a>
<c:choose> 
  <c:when test="${not empty artist}">
    &gt; Edit ${artist.name}
  </c:when>
  <c:otherwise>
    &gt; New Artist
  </c:otherwise>
</c:choose>
</div>
<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>

<form action="EditorArtistCreateConfirmation" method="post">
	<input type="hidden" name="name" id="name" value="${name}"/>
	<input type="submit" name="continueButton" value="Continue" class="medButton" />
</form>

<h1>Found matching artist</h1>
	<div>
		<display:table class="list" name="${artists}" id="artist" requestURI="EditorEditArtist" defaultsort="2">
			<display:column property="name" title="Name"/>
			<display:column  sortable="true" >
					<a href="EditorDetailTour?artistId=${artist}">Detail</a> 
			</display:column>    
    		<display:column  sortable="true" >
					<a href="EditorDetailTour?artistId=${artist}">Edit</a> 
			</display:column>
			<display:column  sortable="true" >
					<a href="EditorDetailTour?artistId=${artist}">Remove</a> 
			</display:column>
    
		</display:table>
	</div>