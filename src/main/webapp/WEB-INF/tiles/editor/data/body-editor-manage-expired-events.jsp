<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Expired Events 
</div>

<h1>Manage Expired Events</h1>

<div id="info" class="info" <c:if test="${empty param.info}"> style="display:none" </c:if>>
  <div id="infoText" class="infoText">
  	${param.info}
  </div>
</div>

<div style="clear:both"></div>
Note: expiring an event will disable its crawls and its tickets.
<br />
<br />

<div style="line-height: 20px;float: left">
	<b>Show:</b>
	<input 
		type="checkbox" 
		id="showExpiredEvents" 
		onchange="changeShowExpiredEvents(this.checked)" 
		<c:if test="${showExpiredEvents}">checked</c:if>	
	/><label for="showExpiredEvents">include expired events</label>
</div>

<!-- form used to submit list of event ids using POST -->
<form id="expirationForm" action="EditorManageExpiredEvents" method="post">
	<input type="hidden" id="expirationFormAction" name="action" />
	<input type="hidden" id="expirationFormEventIds" name="eventIds" />
</form>


<script language="javascript" type="text/javascript">

$(document).ready(
	function() {
		$('.event').attr('checked', false);
	
		$('table#event TBODY tr').click(
			function() {
				var checkbox = $($(this).children('TD').get(0)).children('INPUT');
				$(checkbox).attr('checked', !$(checkbox).attr('checked'));
				updateEventRow($(checkbox).attr('id').split("-")[1]);
			}
		);
							
	}
);

	function toggleEventRow(eventId) {
		$('#event-' + eventId).attr('checked', !$('#event-' + eventId).attr('checked'));
		updateEventRow(eventId);
	};
	
	function updateEventRow(eventId) {
		if ($('#event-' + eventId).attr('checked')) {
			$('#event-' + eventId).parent().parent().css('background-color', '#748C20');
			$('#event-' + eventId).parent().parent().find('*').css('color', '#ffffff');
		} else {
			$('#event-' + eventId).parent().parent().css('background-color', null);
			$('#event-' + eventId).parent().parent().find('*').css('color', null);
		}
	};

	function selectAllEvents() {
		$.each($('.event'),  function(i,elt) {
			var eventId = $(elt).attr('id').split("-")[1];
			$('#event-' + eventId).attr('checked', true);		
			updateEventRow(eventId);
		});
	};

	function unselectAllEvents() {
		$.each($('.event'),  function(i,elt) {
			var eventId = $(elt).attr('id').split("-")[1];
			$('#event-' + eventId).attr('checked', false);		
			updateEventRow(eventId);
		});
	};
	
	function __getSelectedEventIds() {
		var ids = "";
		$.each($('.event'), function(i, elt) {
			if ($(elt).attr('checked')) {
				var id = $(elt).attr('id').split("-")[1]; 
				ids += id + ",";
			}
		});
		
		if (ids == "") {
			return null;
		}
		
		// remove the final comma
		ids = ids.substring(0, ids.length - 1)
		return ids;		
	};
	
	function expireSelectedEvents() {
		var eventIds = __getSelectedEventIds();
		if (eventIds == null) {
			alert("No event selected");
			return;
		}
		
		var r = confirm("Do you really want to expire the " + eventIds.split(",").length + " event(s)?", "Expire event(s)?");
		if (r) {
			$('#expirationFormAction').val('expire');
			$('#expirationFormEventIds').val(eventIds);
			$('#expirationForm').submit();
		
			// document.location.href="EditorManageExpiredEvents?action=expire&eventIds=" + eventIds;
		}
		
	};
	
	function changeShowExpiredEvents(showDisabledCrawls) {
		document.location.href = "EditorManageExpiredEvents?showExpiredEvents=" + (showDisabledCrawls?"1":"0");			
	};

</script>


<br /><br /><br />
<c:if test="${not empty events}">
	<div>
		<div style="float: left">
			<b>Select: </b>
			<a style="cursor:pointer" onclick="selectAllEvents()">All</a> -
			<a style="cursor:pointer" onclick="unselectAllEvents()">None</a>
		</div>
		<div style="float: left; margin-left: 40px;">
			<b>Action: </b>
			<a href="javascript:void(0)" onclick="expireSelectedEvents()">Expire</a>			
		</div>
	</div>
</c:if>

<jsp:directive.page import="com.admitone.tmat.data.Event"/>
<jsp:directive.page import="com.admitone.tmat.enums.EventStatus"/>
<br /><br />
<display:table class="list" name="${events}" id="event" requestURI="EditorManageExpiredEvents" decorator="expiredEventTableDecorator" pagesize="200">
    <display:column style="width:20px">
		<input type="checkbox" id="event-${event.id}" class="event" onChange="toggleEventRow($(this).attr('id').split('-')[1])" />    	
    </display:column>
    <display:column title="Date" sortable="true"  paramProperty="id">
    	<b>
		    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
		    <span> </span>
			<c:choose>
			  <c:when test="${not empty event.time}">
			    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
			  </c:when>
			  <c:otherwise>
			    TBD
			  </c:otherwise>
			</c:choose>		
		</b>
    </display:column>
    <display:column title="Name" sortable="true" class="content-left-align" headerClass="header-left-align">
    		<b>${event.name}</b>
    	<c:if test="${event.eventStatus == 'EXPIRED'}">
			(<font color="red">expired</font>)
		</c:if>
    </display:column>
    <display:column title="Venue" sortable="true" property="venue.building" />
    <display:column title="State" sortable="true" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    <%--
    <display:column title="Listing" sortable="true" sortProperty="nonDuplicateTicketCount" property="nonDuplicateTicketCount" class="text-align-center" />
    <display:column title="Ticket" sortable="true" property="nonDuplicateTicketQuantitySum" class="text-align-center" />
    --%>
</display:table>
