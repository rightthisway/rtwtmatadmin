<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	$("#searchBtn").click(function(){
		if($('#txtName').val() == ''){
			alert("Please Enter valid Synonym Name.")
			return;
		}else{
			window.location="EditorManageConcertSynonyms?filterBy="+$('#txtName').val()+"&action=search";
		}
	});  
});
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; Synonyms
</div>
<div id="pleaseWait" style="display:none">
	<img src="../images/process-running.gif"  align="absbottom"/>Please wait...
	<br /><br />
</div>


<br/><br/>
<h1>Concert Synonyms</h1>
<br/>
<div id="ticket-filter-toggle" style="background:#a1a8ff; padding: 5px">
	<table>
		<tr>
			<td><b>Synonym :</b></td>
			<td><input type="text" name="txtName" id="txtName"></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><input type="button" class="medButton" id="searchBtn" value="Filter"> 
		</tr>
	</table>
</div>
<br/><br/>
<display:table class="list" name="${concertSynonyms}" id="synonym" requestURI="EditorManageConcertSynonyms" defaultsort="2">
    <display:column title="Name" sortable="true" property="name" />
    <display:column title="Synonyms" sortable="true" property="value" />
    <display:column>
    	<a href="EditorEditSynonym?id=${synonym.name}&type=${synonym.type}"><img src="../images/ico-edit.gif" align="absbottom" /> edit</a> - 
    	<a href="javascript:confirmRemove('${synonym.name}','${synonym.type}')"><img src="../images/ico-delete.gif" align="absbottom" />  remove</a>    	
    </display:column>  
    <display:column>
    	<a href="javascript:confirmCopy('${synonym.name}')">Copy To Theater</a>
    </display:column>  
</display:table>
<br/>
<img src="../images/ico-plus.gif"  align="absbottom"/> <a href="EditorEditSynonym?action=createConcert">Add synonyms</a>

<script type="text/javascript">
function confirmRemove(id,type) {
  var response = confirm("Are you sure you want to remove " + id + "?");
  if (response) {
    location.href='EditorManageConcertSynonyms?action=remove&id=' + id+'&type=' + type;
  }
}
function confirmCopy(id) {
	  var response = confirm("Are you sure you want to copy " + id + "?");
	  if (response) {
	    location.href='EditorManageConcertSynonyms?action=copy&id=' + id;
	  }
	}

</script>