<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Artist
</div>

<c:if test="${not empty param.info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>

<a href="EditorEditArtist?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new artist / team</a>
<br /><br />
<h1>Artist</h1>
<display:table class="list" name="${artists}" id="artist" >
	  <display:column title="Id" sortable="true" property="id" href="EditorDetailArtists" paramId="artistId" paramProperty="id"  />
    <display:column title="name" property="name"/>    
    <display:column>
    	<a href="javascript:removeTour(${artist.id})"><img src="../images/ico-delete.gif" align="absbottom" />Remove</a>
    </display:column>
</display:table>