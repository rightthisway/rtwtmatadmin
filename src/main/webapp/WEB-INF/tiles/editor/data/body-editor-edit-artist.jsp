<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; <a href="EditorManageTours">Artist</a>
<c:choose> 
  <c:when test="${not empty artist}">
    &gt; Edit ${artist.name}
  </c:when>
  <c:otherwise>
    &gt; New Artist
  </c:otherwise>
</c:choose>
</div>

<h1>
<c:choose>
	<c:when test="${empty param.id}">Add a new artist / team</c:when>
	<c:otherwise>Edit artist</c:otherwise>
</c:choose>
</h1>

<form:form commandName="artist" method="POST">
	<table>
		<tr>
			<td>Name</td>
			<td>
				<form:input path="name" cssClass="longInput" />
				<form:errors path="name" cssClass="error" />			
			</td>
		</tr>
	</table>
	<input type="submit" name="submit" value="Save Changes" class="medButton" onclick="return validateData();"/>
	<input type="button" value="Cancel" class="medButton" onclick="document.location.href='EditorManageArtists'" />
</form:form>
<h1>Tour List</h1>
<display:table class="list" name="${tours}" id="tour" requestURI="EditorEditArtist" defaultsort="2" pagesize="20">
    <display:column title="Id" sortable="true" property="id" href="EditorDetailTour" paramId="tourId" paramProperty="id"  />
    <display:column title="">
    	<img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}" />
    </display:column>    
    <display:column title="Tour / Season Name" sortable="true" property="name" href="EditorDetailTour" paramId="tourId" paramProperty="id"  />
    <display:column title="Location" sortable="true" property="location" />
    <display:column title="StartDate" sortable="true">
      <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
    </display:column>
    <display:column title="EndDate" sortable="true">
      <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
    </display:column>
    <display:column title="Event Count" sortable="true" sortProperty="eventCount" class="text-align-center" property="eventCount" />
    <display:column>
    	<a href="EditorDetailTour?tourId=${tour.id}"><img src="../images/ico-edit.gif" align="absbottom" /> details</a>
    </display:column>  
    <display:column>
    	<a href="javascript:removeTour(${tour.id})"><img src="../images/ico-delete.gif" align="absbottom" />Remove</a>
    </display:column>  
</display:table> 
<h2>Merge This Artist</h2> 

	<div class="info">
		<div class="infoText">
			MERGED ARTISTS CANNOT BE UNDONE.
			<br/>
			THIS PAGES ARTIST WOULD BE MERGED WITH THE SELECTED ARTIST.
			<br/>
			THIS PAGES ARTISTS BOOKMARKS AND ARTIST PRICE ADJUSTMENTS WOULD BE DELETED. 
			<br/>
			THE SELECTED ARTIST WOULD TAKE THIS PAGES ARTIST'S TOURS.
		</div>
	</div> 
	Selected Artist: 
	
<div id="mergeArtistDialog" title="" style="display:none">
	<span id="mergeArtistDialogMessage" align="center"></span>
</div>
	
	<select id="artistCmb" onchange="showMergeButton()">
		<option value="">NONE SELECTED</option>
		<c:forEach var="art" items="${artists}">
			<c:if test="${art.id != artist.id}">
				<option value="${art.id}" >${art.name} - ${art.id} </option>
			</c:if>  
		</c:forEach>
	</select>
	<input id="mergeArtistsButton" type="button" class="medButton" onclick="mergeArtists()"  value="Merge Artists"  style="display:none"/>

<script type="text/javascript" language="javascript">

function validateData(){
	
	if(document.getElementById("name").value == ""){
		alert("Please Enter Name");	
		return false;
	}
	
}

	$(document).ready(function() {
		$('#name').focus();
	});

	//merge tour	
	$("#mergeArtistDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

		function removeTour(tourId){
		var agree = confirm("Are you sure you want to delete this Tour.?");
		if(agree){
			window.location="EditorDetailTour?tourId="+tourId+"&action=removeTour";	
		}
		
	}
	
	function showMergeButton() {
		if ($('#mergeArtistsButton').css('display') == 'none') {
			$('#mergeArtistsButton').show();
		}
	};
	
	function mergeArtists() {

		var mergeId = "${artist.id}";
		var goodId = $('#artistCmb').val();

		
		$('#mergeArtistDialogMessage').html(
        		"If you merge this artist, all the associated tours will be "
        		+ "<br /> transferred from " + mergeId + " to " + goodId + ". "
        		+ "<br /> All the ${mergeId} artist price adjustments and bookmarks "
        		+ "<br /> will be deleted. "
        		+ "<br /><br />Do you really want to merge this artist?");
        $("#mergeArtistDialog").dialog('open');
        $('#mergeArtistDialog').dialog('option', 'title', 'Merge Artists?');
		$('#mergeArtistDialog').dialog('option', 'buttons', {});
		$("#mergeArtistDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeArtistDialog').dialog('option', 'title', 'Merging Artists...');
				$('#mergeArtistDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging Artists...');		
				$("#mergeArtistDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeArtists(mergeId, goodId, function(response) {
				   $('#mergeArtistDialogMessage').html(response + "!");
				   $("#mergeArtistDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$("#mergeArtistDialog").dialog('close');
							
					        //return home
					        document.location.href = "EditorManageArtists"; 
					   }
				   });
       			});			
			},
			Cancel: function() {
				$("#mergeArtistDialog").dialog('close');
			}
		}); 
	};
</script>
