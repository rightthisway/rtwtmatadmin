<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a>  
	&gt; 
    <c:choose>
	  <c:when test="${empty param.id}">Venue Synonym</c:when>
	  <c:otherwise>Edit Venue Synonym</c:otherwise>
    </c:choose>
</div>
<h1>
	Edit Venue Synonym
</h1>

<c:if test="${not empty info}">
	<div class="info"><div class="infoText">${info}</div></div>
</c:if>

<b>Filter:</b> <br><br>
<b>Venue :</b> 
<select onchange="changeFilterVenue(this.value);">
	<option value="">ALL</option>
	<c:forEach var="venueInfo" items="${allVenues}">
		<option value="${venueInfo.venueId}" style="height:20px;" <c:if test="${venueInfo.venueId == venueId}">selected</c:if>>
			${venueInfo.venue.building}, ${venueInfo.venue.city}, ${venueInfo.venue.state}, ${venueInfo.venue.country}
		</option>
	</c:forEach>
</select>
<c:choose> 
<c:when test="${not empty venueSynonymInfoList}">

<form action="EditorEditManageVenueSynonym" method="post" id="editorManageVenueSynonyms">
<input type="hidden" id="action" name="action" value="update">
<input type="hidden" id="artistId" name = "artistId" value="${artistId}"/>
<input type="hidden" id="venueId" name = "venueId" value="${venueId}"/>
<div align="center" >
	<input type="button" class ="medButton" value="Update" onclick="submitForm()"/>
</div>
<display:table class="list" name="${venueSynonymInfoList}" id="venueInfo"  requestURI="EditorEditManageVenueSynonym" pagesize="50" export="true">
  	<display:column title='<input type="checkbox" name ="selectAll" id ="selectAll"/>'>
  		<input type="checkbox" name ="${venueInfo.venueId}-checkbox" id ="${venueInfo.venueId}-checkbox" class="selectCheckBox"/>
  	</display:column>
  	
  	<display:column title='Venue' style="width:400px;text-align:left;">
  		${venueInfo.venue.building}, ${venueInfo.venue.city}, ${venueInfo.venue.state}, ${venueInfo.venue.country}
  	</display:column>
  	
  	<display:column title='Synonyms' >
		<select id="${venueInfo.venueId}-synonym" name="${venueInfo.venueId}-synonym" multiple style="width:400px;height:80px;">
			<c:forEach items="${venueInfo.probableVenues}" var="synonymObj" >
				<option value="${synonymObj.id}">${synonymObj.siteId} - ${synonymObj.probleName}</option>
			</c:forEach>
		</select>
	</display:column>
	
	<display:column title='Action' >
		<div align="center" >
			<input type="button" id="${venueInfo.venueId}-action" class ="medButton" value="Remove" onclick="addVenue($(this).attr('id'));" />
		</div> 
	</display:column>
	 
	<display:column title='To Be Remove Synonyms' >
		<select id="${venueInfo.venueId}-removeVenue" name="${venueInfo.venueId}-removeVenue" multiple style="width:400px;height:80px;">
		</select> 
	</display:column>
	
	<display:column title='Updated By' >
		<div align="center" >
			${venueInfo.creator}
		</div> 
	</display:column>
	
	<display:column title='Updated Time' >
		<div align="center" >
			${venueInfo.createdTime}
		</div> 
	</display:column>
	
</display:table>

<div align="center" >
	<input type="button" class ="medButton" value="Update" onclick="submitForm()"/>
</div>
</form>
</c:when >

<c:otherwise>
There is no synonym for selected option.
</c:otherwise>
</c:choose>

<script type="text/javascript">

function addVenue(id){
	var removeVenue = id.replace('action','removeVenue'); 
	var synonym = id.replace('action','synonym'); 
	
	var checkBoxId = id.replace('action','checkbox'); 
	$('#' + checkBoxId).attr('checked', true);
	
	var multipleValues = $("#"+synonym).val() || [];
	if(null == multipleValues || multipleValues == "" || multipleValues.length <=0 ){
		alert("Please Choose Atleast One Synonym To Remove");
	}
	
	//var foo = [];
	$('#'+synonym+' :selected').each(function(i, selected){
	  var text = $(selected).text();
	  var value = $(selected).val();
	  $('#'+removeVenue).append("<option value=\"" + value + "\">" + text + "</option>");
	  $("#"+synonym+" option[value='"+value+"']").remove();
	  
	});
	$('#'+removeVenue+' option').attr('selected', true);
	
}
  
function checkThis(id){
	id = id.replace('synonym','checkbox'); 
	$('#' + id).attr('checked', true);
}

$('#selectAll').click(function(){
	var isChecked = $(this).is(':checked');

	$('.selectCheckBox').each(function() {
		$(this).attr('checked', isChecked);
	});
});

function submitForm() {
	var flag= true;
	var isSelected = false;
	$('.selectCheckBox:checkbox:checked').each(function () {
		isSelected = true;
		var id,value;
		id = this.id.replace('checkbox','removeVenue');
		var synonymStr = $("#"+id).val(); 
		
		if(synonymStr == null || synonymStr == "") {
			alert("Please enter synonyms");
			$("#" + id ).focus();
			flag = false;
			return false;
		}
	});
	if(!flag) {
		return flag;
	}
	if(!isSelected){
		alert('Select Minimum One Venue For Update.');
		flag=false;
		return false;
	} 
	if(flag) {
		$("#action").val('update');
		$("#editorManageVenueSynonyms").submit();
	}
}

function changeFilterVenue(venueId) {
	document.location.href = "EditorEditManageVenueSynonym?venueId=" + venueId;
};
</script>

