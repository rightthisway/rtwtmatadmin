<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; <a href="EditorManageArtists">Artist</a>
<c:choose> 
  <c:when test="${not empty artist}">
    &gt; Edit ${artist.name}
  </c:when>
  <c:otherwise>
    &gt; New Artist
  </c:otherwise>
</c:choose>
</div>
<c:if test="${not empty param.info or not empty info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>
<h1>
<c:choose>
	<c:when test="${empty param.id}">Add a new artist / team</c:when>
	<c:otherwise>Edit artist</c:otherwise>
</c:choose>
</h1>

<form:form commandName="artist" method="POST">
	<table>
		<tr>
			<td>Name:</td>
			<td>
				<form:input path="name" cssClass="longInput" />
				<form:errors path="name" cssClass="error" />			
			</td>
			<td>Grand Child Category:</td>
			<td>
				<form:input path="grandChildTourCategory" cssClass="longInput"/>
				<form:errors path="grandChildTourCategoryId" cssClass="error" />			
				<form:hidden path="grandChildTourCategoryId" />
				
			</td>
		</tr>
	</table>
			<input type="submit" name="submit" value="Save Changes" class="medButton" onclick="return validateData();"/>
			<input type="button" value="Cancel" class="medButton" onclick="document.location.href='EditorManageArtists'" />	
</form:form>

<c:if test ="${not empty artists}">
	<h1>Found matching artist</h1>
	<div>
		<display:table class="list" name="${artists}" id="artist" requestURI="EditorEditArtist" defaultsort="2">
			<display:column property="name" title="Name"/>
			<display:column  sortable="true" >
					<a href="EditorDetailArtist?artistId=${artist}">Detail</a> 
			</display:column>    
    		<display:column  sortable="true" >
					<a href="EditorDetailArtist?artistId=${artist}">Edit</a> 
			</display:column>
			<display:column  sortable="true" >
					<a href="EditorDetailArtist?artistId=${artist}">Remove</a> 
			</display:column>
    
		</display:table>
	</div>
</c:if>
<script type="text/javascript" language="javascript">

	function validateData(){
		if($.trim($("#name").val()) == ''){
			alert("Please Enter Name");	
			return false;
		}
		if($.trim($("#grandChildTourCategoryId").val()) == ''||
				$.trim($("#grandChildTourCategory").val()) == '' ){
			alert("Please Select GrandChild Category");	
			return false;
		}
	}

	$(document).ready(function() {
		$('#name').focus(); 
    });

	$('#grandChildTourCategory').autocomplete("AutoCompleteGrandChildCategory", {
	  		width: 550,
			max: 1000,
			minChars: 2,		
			/* formatItem: function(row, i, max) {
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			}, */
			formatResult: function(row) {
				return row[0];
			},
			startSearch: function() {
				//$('#grandChildTourCategoryStatus').empty();
				//$('#grandChildTourCategoryStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
			},
			stopSearch: function(rows) {
				/* $('#grandChildTourCategoryStatus').empty();
				if (rows.length == 0) {
					$('#grandChildTourCategoryStatus').text("No results");
				} else {
					$('#grandChildTourCategoryStatus').text("Found " + rows.length + " results");
				} */
			},
			highlight: function(value, term) {
				return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.trim(term.replace(/ +/g, ' ')).replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
			}
	});

	$('#grandChildTourCategory').result(function(event, row, formatted) {
 	  	$("#grandChildTourCategoryId").val(row[1]);
	});
</script>
