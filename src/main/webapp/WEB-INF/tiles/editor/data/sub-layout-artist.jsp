 <%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedTourSubMenu" name="selectedTourSubMenu" scope="request" />

<div id="editorSubMenu">
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'SearchArtist'}">
        <b>Search Artist</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageArtists">Search Artist</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'AddArtist'}">
        <b>Add Artist</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditArtist?action=create">Add Artist</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'MergeArtist'}">
        <b>Merge Artist</b>
   </c:when>
   <c:otherwise>
        <a href="EditorMergeArtists">Merge Artist</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'Quick Create Event'}">
        <b>Create Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditEvent?action=create">Create Event</a>
   </c:otherwise>
 </c:choose> 
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'Edit Multiple Event'}">
        <b>Edit Multiple Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditMultipleEvent">Edit Multiple Event</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'Audit Event'}">
        <b>Audit Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorAuditEvent">Audit Event</a>
   </c:otherwise>
 </c:choose>
 </div>
 <tiles:insertAttribute name="subBody1" /> 