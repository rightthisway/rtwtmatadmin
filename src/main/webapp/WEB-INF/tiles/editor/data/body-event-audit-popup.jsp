
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="/WEB-INF/tiles/decorators.jsp"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<%-- <div>
	 <b>Event : ${event.name}</b>
	<br/>
	<b>Event Date : <fmt:formatDate pattern="MM/dd/yyyy" value="${event.localDate}" />				
					<c:choose>
					<c:when test="${event.localTime == null}">
								 TBD
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm aa" value="${event.localTime}" />
					</c:otherwise>
					</c:choose>
	</b>
	<br/>
	 <b>Venue : ${event.venue.building}</b>
	<br/>
	 
</div> --%>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

			<display:column  title ="Action">	
				${audit.action}
			</display:column>										
			
			<display:column title="Modified Date">
				<fmt:formatDate pattern="MM/dd/yyyy hh:mm aa" value="${audit.modifiedDate}" />					
			</display:column>
			<display:column title="User">${audit.userName}</display:column>
			
			<display:column title="EventName"> 
				<c:choose>
						<c:when test="${audit.eventName == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.eventName}
						</c:otherwise>
					</c:choose>
				<%-- &nbsp;/&nbsp;
				<c:choose>
						<c:when test="${audit.newEventName==null}">
							-
						</c:when>
						<c:otherwise>
							${audit.newEventName}
						</c:otherwise>
					</c:choose> --%>
				
			</display:column>
			<%-- <display:column title="TourName">
				<c:choose>
					<c:when test="${audit.tourName== null}">
						-
					</c:when>
					<c:otherwise>
						${audit.tourName}
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newTourName == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newTourName}
				</c:otherwise>
				</c:choose>
			</display:column>
			 --%>
			<display:column title="ArtistName">
				<c:choose>
					<c:when test="${audit.artistName== null}">
						-
					</c:when>
					<c:otherwise>
						${audit.artistName}
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="EventDate">
				<c:choose>
					<c:when test="${audit.eventDate == null}">
						-
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="MM/dd/yyyy" value="${audit.eventDate}" />	
					</c:otherwise>
				</c:choose>
			<%-- 		&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newEventDate == null}">
					-
				</c:when>
				<c:otherwise>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${audit.newEventDate}" />
				</c:otherwise>
				</c:choose> --%>
			</display:column>
			
			<display:column title="EventTime">
				<c:choose>
					<c:when test="${audit.eventTime == null}">
						-
					</c:when>
					<c:otherwise>
						<fmt:formatDate pattern="hh:mm a" value="${audit.eventTime}" />
					</c:otherwise>
				</c:choose>
				<%-- 	&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newEventTime == null}">
					-
				</c:when>
				<c:otherwise>
					<fmt:formatDate pattern="hh:mm a" value="${audit.newEventTime}" />
				</c:otherwise>
				</c:choose> --%>
			</display:column>
			
		  <display:column title="VenueName"> 
				<c:choose>
						<c:when test="${audit.venueName == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.venueName}
						</c:otherwise>
					</c:choose>
			<%-- &nbsp;/&nbsp;
				<c:choose>
						<c:when test="${audit.newVenueName == null}">
							-
						</c:when>
						<c:otherwise>
							${audit.newVenueName}
						</c:otherwise>
					</c:choose> --%>
				
			</display:column>	
			<display:column title="venueCategory">
				<c:choose>
					<c:when test="${audit.venueCategory == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.venueCategory}
					</c:otherwise>
				</c:choose>
			<%-- 		&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newVenueCategory == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newVenueCategory}
				</c:otherwise>
				</c:choose> --%>
			</display:column>
			<%-- <display:column title="Presale Event">
				<c:choose>
					<c:when test="${audit.presaleEvent}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column> --%>
			<display:column title="EventType">
				<c:choose>
					<c:when test="${audit.eventType== null}">
						-
					</c:when>
					<c:otherwise>
						${audit.eventType}
					</c:otherwise>
				</c:choose>
			<%-- 		&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newEventType == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newEventType}
				</c:otherwise>
				</c:choose> --%>
			</display:column>	
			<display:column title="No Price">
				<c:choose>
					<c:when test="${audit.noPrice == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.noPrice}
					</c:otherwise>
				</c:choose>
				<%-- 	&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newNoPrice == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newNoPrice}
				</c:otherwise>
				</c:choose> --%>
			</display:column>
			<display:column title="admitoneId">
				<c:choose>
					<c:when test="${audit.admitoneId == null}">
						-
					</c:when>
					<c:otherwise>
						${audit.admitoneId}
					</c:otherwise>
				</c:choose>
				<%-- 	&nbsp;/&nbsp;
				<c:choose>
				<c:when test="${audit.newAdmitoneId == null}">
					-
				</c:when>
				<c:otherwise>
					${audit.newAdmitoneId}
				</c:otherwise>
				</c:choose> --%>
			</display:column>
			
			
			<%-- <display:column title="TicketNetwork Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'TNE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'TNE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="VividSeats Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'VIVID')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'VIVID')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="TickPick Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'TPE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'TPE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="ScoreBig Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'SBE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'SBE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Ebay Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'EBAY')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'EBAY')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Exchange Two Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'EXTWO')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'EXTWO')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Exchange Three Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'EXTHREE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'EXTHREE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="Exchange Four Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'EXFOUR')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'EXFOUR')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column>
			
			<display:column title="exchange Five Event">
				<c:choose>
					<c:when test="${audit.oldExchanges != null && fn:contains(audit.oldExchanges,'EXFIVE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
					&nbsp;/&nbsp;
				<c:choose>
					<c:when test="${audit.newExchanges != null && fn:contains(audit.newExchanges,'EXFIVE')}">
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</display:column> --%>
			
			
				
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit for this Event.</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	<div style="float:right; padding-right:320px;">
				<input type="button" value="Close"    class="formBtns" id="update2" name="update2" onclick="window.close();"/> 
	</div>
	
</body>
</html>