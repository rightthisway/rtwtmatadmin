<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorManageTours">Manage Tours/Seasons</a> 
	&gt; Edit ${tour.name}
</div>

	<h2>Merge This Tour</h2>
	<div class="info">
		<div class="infoText">
			MERGED TOURS CANNOT BE UNDONE.
			<br/>
			THIS TOUR WOULD BE MERGED WITH THE SELECTED TOUR.
			<br/>
			THIS TOUR'S DETAILS, CATEGORIES, AND PRICE ADJUSTMENTS WOULD BE DELETED. 
			<br/>
			THE SELECTED TOUR WOULD TAKE THIS TOUR'S EVENTS, AND CRAWLS.
		</div>
	</div> 
	 
	
<div id="mergeTourDialog" title="" style="display:none">
	<span id="mergeTourDialogMessage" align="center"></span>
</div>
	Selected Tour:
	<select id="tourId" onchange="showMergeButton()">
		<option value="">NONE SELECTED</option>
		<c:forEach var="to" items="${tours}">
			<c:if test="${to.id != tour.id}">
				<option value="${to.id}" >${to.name} - ${to.startDate} - ${to.endDate} - ${to.id} </option>
			</c:if>  
		</c:forEach>
	</select>
	<br/><br/>
	Merge into Tour:
	<select id="tourToMergeId" onchange="showMergeButton()">
		<option value="">NONE SELECTED</option>
		<c:forEach var="to" items="${tours}">
			<c:if test="${to.id != tour.id}">
				<option value="${to.id}" >${to.name} - ${to.startDate} - ${to.endDate} - ${to.id} </option>
			</c:if>  
		</c:forEach>
	</select>
	<input id="mergeToursButton" type="button" class="medButton" onclick="mergeTours()"  value="Merge Tours" />

<script type="text/javascript" language="javascript">
//merge tour	
	$("#mergeTourDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});
	
	function mergeTours() {
		var mergeId = $('#tourToMergeId').val();
		var goodId = $('#tourId').val();
	
		$('#mergeTourDialogMessage').html(
        		"If you merge this tour, all the associated events,  "
        		+ "<br /> and crawls will be "
        		+ "<br /> transferred from " + goodId + " to " +mergeId  + ". "
        		+ "<br /> All the ${mergeId} tour price adjustments and zones, "
        		+ "<br /> will be deleted. "
        		+ "<br /><br />Do you really want to merge this tour?");
        $("#mergeTourDialog").dialog('open');
        $('#mergeTourDialog').dialog('option', 'title', 'Merge tour?');
		$('#mergeTourDialog').dialog('option', 'buttons', {});
		$("#mergeTourDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeTourDialog').dialog('option', 'title', 'Merging tour...');
				$('#mergeTourDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging tour...');		
				$("#mergeTourDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeTours(goodId,mergeId, function(response) {
				   $('#mergeTourDialogMessage').html(response + "!");
				   $("#mergeTourDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$("#mergeTourDialog").dialog('close');
							
					        //return home
					        document.location.href = "EditorManageTours"; 
					   }
				   });
       			});			
			},
			Cancel: function() {
				$("#mergeTourDialog").dialog('close');
			}
		}); 
	}
	
</script>