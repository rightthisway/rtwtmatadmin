<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; <a href="EditorPriceManagement">Data Maintenance</a>
  &gt; Empty Tours
</div>
<c:if test="${not empty info}">
  <div class="info"><div class="infoText">${param.info}</div></div>
  <br /><br />
</c:if>
<h1>
	Empty Tours
</h1>
<br/><br/>
<div align="center">
	<form action="EditorManageEmptyTours"  method="post" id="form" name="form">
		<table>
			<tr>
				<td>
					<b>Tour:</b>
					<input type="hidden" id="action" name="action" value="delete"/>
				</td>
				<td><input type="text" id="tour" name="tour"  value="${tourName}"/></td>
			</tr>
			<tr>
				<td>
					<input type="Submit" class="medButton" value="Search" onclick="submitForm('search')"/>
				</td>
				<td>
					<input type="button" class="medButton" value="Delete" onclick="submitForm('delete');"/>
				</td>
			</tr>	
		</table>
		<display:table class="list" name="${tours}" id="tour" requestURI="EditorManageEmptyTours" pagesize="400" defaultsort="4">
		<display:column title="<input type='checkbox' id='selectAll' name='selectAll'  onclick ='toggleSelect()'/> " >
		  <input type="checkbox" class="checkbox-tour" id="tour-${tour.id}" name="tour-${tour.id}"/>
		</display:column>
		<display:column title="Id" sortable="true" property="id" href="EditorDetailTour" paramId="tourId" paramProperty="id"  />
		<display:column title="">
			<img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}" />
		</display:column>    
		<display:column title="Tour / Season Name" sortable="true" property="name" href="EditorDetailTour" paramId="tourId" paramProperty="id" class="content-left-align" headerClass="header-left-align" />
		<display:column title="Artist / Team Name" sortable="true" property="artist.name" class="content-left-align" headerClass="header-left-align"/>
		<display:column title="Location" sortable="true" property="location" />
		<display:column title="StartDate" sortable="true">
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
		</display:column>
		<display:column title="EndDate" sortable="true">
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
		</display:column>
		<display:column>
			<a href="EditorDetailTour?tourId=${tour.id}"><img src="../images/ico-edit.gif" align="absbottom" /> details</a> 
		</display:column>    
	</display:table>
	</form>
</div>
<br/><br/>

<br/><br/>


<script type="text/javascript">
	function toggleSelect(){
		var flag = $("#selectAll").is(':checked');
		if(flag){
			$(".checkbox-tour").attr('checked', true);
		}else{
			$(".checkbox-tour").attr('checked', false);
		}
	}
	
	function submitForm(action){
		$("#action").val(action);
		if(action=='delete'){
			var flag=false;
			$("input:checkbox.checkbox-tour").each(function(){
				if(this.checked){
					flag=true;
					return false;
				}
			});
			if(!flag){
				alert("Please select a tour..");
				return false;
			}
		}
		$("#form").submit();
	}
</script>