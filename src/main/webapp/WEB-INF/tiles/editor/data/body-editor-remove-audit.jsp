
<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<jsp:include page="/WEB-INF/tiles/decorators.jsp"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script type='text/javascript' src='../js/date.js'></script>
	<script type='text/javascript' src='../js/utils.js'></script>
	<script type="text/javascript" src="../js/FABridge.js"></script>
	<script type="text/javascript" src="../js/swfobject.js"></script>
	<style type="text/css">
		    @import url(../css/jquery-all.css);
		</style>
		
<script type='text/javascript' src='../js/jquery-all.js'></script>
<script type="text/javascript">
function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#tempVenueId').val('');
}
 
function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#tempArtistId').val('');
}

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val(row[1]);
		changeFilterTour(row[0]);
	});
	
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		changeFilterVenue(row[0]);	
	});
});

function popupEventAudit(eventId){
	var url = "EventAudit?eventId="+eventId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
function changeFilterTour(artistId) {
	document.location.href = "EditorAuditEvent?artistId=" + artistId;
	
};

function changeFilterVenue(venueId) {
document.location.href = "EditorAuditEvent?venueId=" + venueId;
};

</script>
</head>
     <style type="text/css">
            @import url(../css/common.css);
		</style>
<body>
<h1>Removed Event Audit</h1>
<br>
<b>Filter:</b> 
By Artist

				<span id="artistLabelSpanId"  <c:if test="${not empty selectedArtistId && selectedArtistId != 0 }">style="display: black;"</c:if> 
				<c:if test="${empty selectedArtistId || selectedArtistId == 0}">style="display: none;"</c:if>  >
				<span id="artistNameDiv" >
					<c:choose>
						<c:when test="${not empty artistName }">
							${artistName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
				</span>	
					<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"  />
				</span>
				
				
				<span id="artistSelectSpanId" <c:if test="${not empty selectedArtistId && selectedArtistId != 0}">style="display: none;"</c:if> 
					<c:if test="${empty selectedArtistId || selectedArtistId == 0}">style="display: black;"</c:if> >
					<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>

<%-- <select onchange="changeFilterTour(this.value);">
	<option value="">Select</option>
	<option value="-1" <c:if test="${editorTCAPTourId == -1}">selected</c:if>>View All</option>
	<c:forEach var="artist" items="${tours}">
		 <option value="${artist.id}" style="height:20px;" 
						<c:if test="${artist.id == selectedArtistId}">selected</c:if>>${artist.name}</option>
	</c:forEach>
</select> --%>

<br />

OR By Venue 

<span id="venueLabelSpanId"  <c:if test="${not empty selectedVenueId && selectedVenueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty selectedVenueId || selectedVenueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName}">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty selectedVenueId && selectedVenueId != 0}">style="display: none;"</c:if> 
				<c:if test="${empty selectedVenueId || selectedVenueId == 0}">style="display: black;"</c:if> >
				
					<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
					<c:forEach var="venue" items="${venues}">
						<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
					</c:forEach>
				</form:select> --%>
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>

<%-- <select onchange="changeFilterVenue(this.value);">
 <option value="">Select</option>
	<c:forEach var="venue" items="${venues}">
		<option value="${venue.id}" style="height:20px;" 
						<c:if test="${venue.id == selectedVenueId}">selected</c:if>>${venue.building}</option>
	</c:forEach>
</select> --%>
<c:if test="${not empty auditList}" >
	<div id="auditTable" >

		<display:table list="${auditList}"  decorator="org.displaytag.decorator.TotalTableDecorator"   id="audit"  requestURI="#" class="list">

			<display:column  title ="EventId">	
				${audit.eventId}
			</display:column>									
			
			<display:column title="EventName"> 
				${audit.eventName}
			</display:column>
			
			<display:column title="EventDate">
				<fmt:formatDate pattern="MM/dd/yyyy" value="${audit.eventDate}" />
			</display:column>
			
			<display:column title="EventTime">
				<fmt:formatDate pattern="hh:mm a" value="${audit.eventTime}" />
			</display:column>
			
		 <display:column title="VenueName"> 
			 ${audit.venueName}
		</display:column>	
			
		<display:column title="Action">
		<%-- <a href="javascript:confirmRemove(${event.id})"><img src="../images/ico-delete.gif" align="absbottom" /> Audit</a>--%>
		<input type="button" value="Audit"  class="smallButton" id="Audit1" name="Audit" onClick="popupEventAudit(${audit.eventId});"/>
		<%-- <a href="EditorEditEvent?id=${event.id}&action=edit&multiEveUrl=EditorDetailTour?tourId=${tour.id}"><img src="../images/ico-edit.gif" align="absbottom" />  edit</a> --%>
		</display:column>
						
		</display:table>
	</div>	
	</c:if>
	<c:if test="${empty auditList}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any audit</span></h4> 
	</div>
		
		<br/>
		<br/>		
	</c:if>
	<br/>
	<br/>
	
</body>
</html>