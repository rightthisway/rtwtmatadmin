<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a>
  &gt; <a href="ValuationFactors">Valuation Factors</a> 
  &gt; Valuation Factors Preview
</div>

<c:if test="${not empty error}">
  <div class="error">
    <div class="errorText">
      ${error}
    </div>
  </div>
</c:if>

<h2>Preview</h2>
<b>Tour:</b> ${tour.name}
<br/><br/>
Please check that the generated valuation factors are correct.
<form method="POST" action="EditorUploadSuperValuationFactorsFinish">
<input type="hidden" name="tourId" value="${tour.id}"/>
<input type="hidden" name="eventId" value="${eventId}"/>
<textarea name="valuation_factors" rows="30" cols="80">
${valuationFactorText}
</textarea>
<br/><br/>
<input type="button" class="medButton" value="&lt;&lt;Go Back" onclick="location.href='EditorValuationFactors?tourId=${tour.id}&event_id=${eventId}'"/>
<input type="submit" value="Create" class="medButton"/>
</form>