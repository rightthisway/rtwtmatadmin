<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Super Automated Event/Crawl Creator
</div>

<c:if test="${not empty param.info}">
  <div class="info">
    <div class="infoText">
      ${param.info}
    </div>
  </div>
</c:if>

<h1>Super Automated Event/Crawl Creator</h1>
<b>Number of automated events created: ${automatedEventCreator.eventCreatedCount}</b><br/>
<b>Number of errors: ${automatedEventCreator.errorEventCount}</b></br>
<b>Number of events to be created: ${automatedEventCreator.eventToBeCreatedCount}</b><br/>
<b>Number of events to be updated:${automatedEventCreator.eventToBeUpdatedCount}</b><br/>
<b>Status: ${automatedEventCreator.status}</b><br/>
<c:if test="${automatedEventCreator.running}">
  <b>Started: <fmt:formatDate value="${automatedEventCreator.startDate}" pattern="yyyy/MM/dd HH:mm:ss"/></b></br>
</c:if>

<b>Automated Event Creator: </b>
<span id="creatorStatusSpan">
<c:choose>
  <c:when test="${automatedEventCreator.running}">
	<img src="../images/process-running.gif" align="absbottom" /><font color="green"><b>RUNNING</b></font>
  </c:when>
  <c:otherwise>
	<font color="red">NOT RUNNING</font>
  </c:otherwise>
</c:choose>
</span>
<span id="pleaseWaitSpan" style="display:none">
<img src="../images/process-running.gif" align="absbottom" /> Please wait...
</span>

<h2>Created events</h2>
<display:table class="list" name="${automatedEventCreator.createdEvents}" id="eventHit" requestURI="EditorAutomatedEventCreator" pagesize="50" decorator="creationEventHitTableDecorator">
  <display:column title="">
   	<img src="../images/ico-${fn:toLowerCase(eventHit.tourType)}.gif" align="absbottom" title="${eventHit.tourType}" />
  </display:column>
  <display:column sortable="true" title="Event Name">
    <a href="BrowseTickets?eventId=${eventHit.eventId}">
    ${eventHit.name}
    </a>
  </display:column>
  <display:column sortable="true" title="Date">
    <span style="white-space:nowrap">
    <fmt:formatDate pattern="MM/dd/yyyy" value="${eventHit.date}" />&nbsp;
	  <c:choose>
		<c:when test="${not empty eventHit.time}">
		  <fmt:formatDate pattern="HH:mm" value="${eventHit.time}" />
		</c:when>
	    <c:otherwise>
		  TBD
		</c:otherwise>
	  </c:choose>
	</span>
  </display:column>
  <display:column sortable="true" title="Tour Name">
    <a href="BrowseEvents?tourId=${eventHit.tourId}">${eventHit.tourName}</a>
  </display:column>
  <display:column sortable="true" title="Location">
    ${eventHit.building}, ${eventHit.city}, ${eventHit.state}, ${eventHit.country}
  </display:column>
  <display:column title="Crawls" sortable="true" sortProperty="siteCount" >
	  <c:forEach var="siteId" items="${constants.siteIds}">
	          <c:if test="${fn:length(eventHit.eventHitMap[siteId]) != 0}">
	              <c:if test="${siteId == 'ebay'}">
	                <img src="../images/ico-warning.gif" align="absbottom" tooltip="Warning! The ebay crawl which has been created might not be accurate since it doesn't account for the time" /> 
	              </c:if>
	              <img src="../images/ico-${siteId}.gif" align="absbottom" tooltip="${siteId}" />
	          </c:if>
	  </c:forEach>
  </display:column>
</display:table>


<h2>Errors</h2>
<display:table class="list" name="${automatedEventCreator.errorEvents}" id="eventHit" requestURI="EditorAutomatedEventCreator" pagesize="50" decorator="creationEventHitTableDecorator">
  <display:column title="">
   	<img src="../images/ico-${fn:toLowerCase(eventHit.tourType)}.gif" align="absbottom" title="${eventHit.tourType}" />
  </display:column>
  <display:column sortable="true" title="Event Name">
    <c:choose>
      <c:when test="${eventHit.exists}">
        <a target="_blank" href="BrowseTickets?eventId=${eventHit.eventId}">${eventHit.name}</a>
      </c:when>
      <c:otherwise>
        ${eventHit.name}
      </c:otherwise>
    </c:choose>
  </display:column>
  <display:column sortable="true" title="Date">
    <span style="white-space:nowrap">
    <fmt:formatDate pattern="MM/dd/yyyy" value="${eventHit.date}" />&nbsp;
	  <c:choose>
		<c:when test="${not empty eventHit.time}">
		  <fmt:formatDate pattern="HH:mm" value="${eventHit.time}" />
		</c:when>
	    <c:otherwise>
		  TBD
		</c:otherwise>
	  </c:choose>
	</span>
  </display:column>
  <display:column sortable="true" title="Tour Name">
    ${eventHit.tourName}
  </display:column>
  <display:column sortable="true" title="Artist Name">
    ${eventHit.artistName}
  </display:column>
  <display:column sortable="true" title="Location">
    ${eventHit.building}, ${eventHit.city}, ${eventHit.state}, ${eventHit.country}
  </display:column>
  <display:column title="Crawls" sortable="true" sortProperty="siteCount" >
	  <c:forEach var="siteId" items="${constants.siteIds}">
	          <c:if test="${fn:length(eventHit.eventHitMap[siteId]) != 0}">
	              <c:choose>
	                <c:when test="${fn:length(eventHit.eventHitMap[siteId]) > 1}">
	                  <b style="color:red">${fn:length(eventHit.eventHitMap[siteId])}<b/> x <img src="../images/ico-${siteId}.gif" align="absbottom" tooltip="${siteId}" />
	                </c:when>
	              </c:choose>
	          </c:if>
	  </c:forEach>
  </display:column>
</display:table>

<h2>Setup</h2>
<form action="EditorAutomatedEventCreator">
      Mode:
        <select name="mode" onchange="if ($(this).val() == 'SCHEDULED') $('#hoursTr').show(); else $('#hoursTr').hide();">
          <option value="NONE" <c:if test="${mode == 'NONE'}">selected</c:if>>NONE</option>
          <option value="ALWAYS" <c:if test="${mode == 'ALWAYS'}">selected</c:if>>ALWAYS</option>
          <option value="SCHEDULED" <c:if test="${mode == 'SCHEDULED'}">selected</c:if>>SCHEDULED</option>
        </select>
        <span id="hoursTr" <c:if test="${mode != 'SCHEDULED'}">style="display:none"</c:if>>
        Start: 
        <select name="startHour">
          <c:forEach var="hour" begin="0" end="23">
            <option value="${hour}" <c:if test="${startHour == hour}">selected</c:if>>${hour}:00</option>
          </c:forEach>
        </select>
        End: 
        <select name="stopHour">
          <c:forEach var="hour" begin="0" end="23">
            <option value="${hour}" <c:if test="${stopHour == hour}">selected</c:if>>${hour}:00</option>
          </c:forEach>
        </select>
    </span>
  <input type="submit" name="action" value="Update" class="medButton"/>
</form>