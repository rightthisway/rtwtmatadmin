<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
function removeSelected(){
			var tobeRemoved='';
			 $('input:checkbox:checked.checkbox-tour').each(function() {
				 tobeRemoved+=this.id + ',';
			 });
			
			 //$('#action').val('remove');
			 //$('#tobeRemoved').val(tobeRemoved.substring(0,tobeRemoved.length-1));
			 //alert($("#tourForm").attr("action"));
			window.location ="EditorManageChildTourCategory?action=remove&tobeRemoved=" + tobeRemoved.substring(0,tobeRemoved.length-1);

			
		}
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Child Tour Category
</div>
<c:if test="${not empty info}">
<div class="info">
		<div class="infoText">
			${info}
		</div>
</div>
</c:if>
<h1>Child Tour Category</h1>

<form method="GET" action="EditorManageChildTourCategory">
  <b>Filter:</b>
  <select id="category" name="category">
  <option value="0" <c:if test="${childCategoryString==0}">selected</c:if>>--All--</option>
  <c:forEach items="${categoryList}" var="category">
  	<option value="${category.id}" <c:if test="${categoryString==category.id}">selected</c:if>>${category.name}</option>
  	</c:forEach>
  </select>
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form>



<a href="EditorEditChildTourCategory?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new Child Tour Category</a>
<br/><br/>
<a href="javascript:removeSelected()">Remove</a>
<br /><br />
<display:table class="list" name="${childTourCategories}" id="childTourCategory" requestURI="EditorManageChildTourCategory" defaultsort="2" pagesize="200">
     <display:column>
      <input type="checkbox" class="checkbox-tour" id="${childTourCategory.id}:${childTourCategory.name}"/>
    </display:column>
	<display:column title="Id" sortable="true" property="id" href="EditorEditChildTourCategory" paramId="id" paramProperty="id"  />
    <display:column title="Parent Category" sortable="true" >
		${childTourCategory.tourCategory.name}
	</display:column>
    <display:column title="Child Category" sortable="true" property="name" href="EditorEditChildTourCategory" paramId="id" paramProperty="id"  />
</display:table>