<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorManageTours">Manage Tours/Seasons</a> 
	&gt; Edit ${tour.name}
</div>

<h1>
<c:choose>
	<c:when test="${empty param.id}">Add a new tour/season</c:when>
	<c:otherwise>Edit tour/season</c:otherwise>
</c:choose>
</h1>

<form:form commandName="tour" method="POST" enctype="multipart/form-data">
<input type="hidden" name="tcap" value="${fromTcap}" />
	<table>
		<tr>
			<td>Tour/Season name</td>
			<td>
				<form:input path="name" cssClass="longInput" />
				<form:errors path="name" cssClass="error" />			
			</td>
		</tr>
		<tr>		
			<td>Artist/Team</td>
			<td>
				<form:select path="artistId">
					<c:forEach var="artist" items="${artists}">
						<form:option value="${artist.id}">${artist.name}</form:option>
					</c:forEach>
				</form:select>
				<form:errors path="artistId" cssClass="error" />			
			</td>
		</tr>




 <tr>
		<td>Parent Category:</td>
			<td>
				<form:select onchange="getChildCategory();" path="tourCategory">
					<form:option value="0">---- Select ----</form:option>
					<form:options items="${tourCategoryList}" itemLabel="name" itemValue="id"/>
				</form:select>
			</td>
		</tr>
		<tr>
		<td>Child Category:</td>
		<td>
			<form:select path ="childCategory" onchange="getGrandChildCategory();">
				<form:option value="0">---- Select ----</form:option>
				<form:options items="${childTourCategoryList}" itemLabel="name" itemValue="id"/>
			</form:select>	
			<form:errors path="childCategory" cssClass="error" />
		</td>
			
		</tr>
		<tr>
		  <td>GrandChild Category:</td>
		  <td>
			<form:select path="grandChildCategory">
				<form:option value="0" label="--- Select ---"/>
   			 	<form:options items="${grandChildTourCategoryList}" itemLabel="name" itemValue="id"/>  
			</form:select>		
			<form:errors path="grandChildCategory" cssClass="error" />
			</td>
			
		</tr>



		<tr>
			<td>Location</td>
			<td>
				<form:input path="location" cssClass="longInput" />
				<form:errors path="location" cssClass="error" />			
			</td>
		</tr>
		<tr>
		<td>Type:</td>
		  <td>
			<form:select path="tourType">
 			  <form:option value="SPORT">SPORT/CONCERT</form:option>
 			  <form:option value="THEATER">THEATER</form:option>
 			 <%--  <form:option value="CONCERT">CONCERT</form:option> --%>
 			  <form:option value="OTHER">OTHER</form:option>
			</form:select>
		  </td>
		 </tr>
		
	</table>

	<input type="submit" name="submit" value="Save Changes" class="medButton" onClick="return Validate();" />
	<input type="button" value="Cancel" class="medButton" onclick="document.location.href='EditorManageTours'" />
	
</form:form>

<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#name').focus();
	});
//merge tour	
	$("#mergeTourDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

	
	function showMergeButton() {
		if ($('#mergeToursButton').css('display') == 'none') {
			$('#mergeToursButton').show();
		}
	};
	
	function mergeTours() {

		var mergeId = "${tour.id}";
		var goodId = $('#tourCmb').val();

		
		$('#mergeTourDialogMessage').html(
        		"If you merge this tour, all the associated events,  "
        		+ "<br /> and crawls will be "
        		+ "<br /> transferred from " + mergeId + " to " + goodId + ". "
        		+ "<br /> All the ${mergeId} tour price adjustments and zones, "
        		+ "<br /> will be deleted. "
        		+ "<br /><br />Do you really want to merge this tour?");
        $("#mergeTourDialog").dialog('open');
        $('#mergeTourDialog').dialog('option', 'title', 'Merge tour?');
		$('#mergeTourDialog').dialog('option', 'buttons', {});
		$("#mergeTourDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeTourDialog').dialog('option', 'title', 'Merging tour...');
				$('#mergeTourDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging tour...');		
				$("#mergeTourDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeTours(mergeId, goodId, function(response) {
				   $('#mergeTourDialogMessage').html(response + "!");
				   $("#mergeTourDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$("#mergeTourDialog").dialog('close');
							
					        //return home
					        document.location.href = "EditorManageTours"; 
					   }
				   });
       			});			
			},
			Cancel: function() {
				$("#mergeTourDialog").dialog('close');
			}
		}); 
	};
	
var dtCh= "/";
var minYear=2001;
var maxYear=2020;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}
	
	
function Validate() {
	 if($("#tourCategory").val() == '0'){
		 alert("Please select parent category");
		 document.getElementById("tourCategory").focus();
		return false;
	 }
	
	 if($("#childCategory").val() == '0'){
	 alert("Please select child category");
		 document.getElementById("childCategory").focus();
		return false;
	 }
	 
	 if($("#grandChildCategory").val() == '0'){
	  alert("Please select grand child category");
		 document.getElementById("grandChildCategory").focus();
		return false;
	 }
	var eDate = document.getElementById("startDate").value;
	if(document.getElementById("startDate").value!="") {	    
	    if (isDate(eDate)==false) {
	        document.getElementById("startDate").focus();
	        return false;
	    }
    }
    var eeDate = document.getElementById("endDate").value;
    if(document.getElementById("endDate").value!="") {
	    if (isDate(eeDate)==false){
	        document.getElementById("endDate").focus();
	        return false;
	    }
	}
	
	if(eeDate<eDate) {	
		alert("EndDate cannot be smaller than the StartDate");
		return false;
	}
	 
}
function getChildCategory(){
	$.getJSON("GetChildTourCategory?tourCategory="+$('#tourCategory').val(),
	function(data){
		var option='<option value="0">---- Select ----</option>';
		$.each(data,function(i,childCategory){
			option += '<option value="' + childCategory.id +'">' + childCategory.name + '</option>';
		});
//		alert(option);
		$('select#childCategory').html(option);
	});	
}

function getGrandChildCategory(){
	$.getJSON("GetGrandChildTourCategory?childTourCategory="+$('#childCategory').val(),
	function(data){
		var option='<option value="0">---- Select ----</option>';
		$.each(data,function(i,grandChildCategory){
			option += '<option value="' + grandChildCategory.id +'">' + grandChildCategory.name + '</option>';
		});
		$('select#grandChildCategory').html(option);
	});	
}
</script>