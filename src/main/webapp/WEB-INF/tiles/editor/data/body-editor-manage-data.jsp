<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Manage Data
</div>

<div id="info" class="info" <c:if test="${empty info && empty param.info}">style="display:none"</c:if>>
  <div id="infoText" class="infoText">
      ${info} ${param.info}
  </div>
</div>

<c:if test="${not empty eventInfoList}">
  <div class="info">
    <div id="infoText" class="infoText">
      <c:forEach var="eventInfo" items="${eventInfoList}">
        <c:choose>
          <c:when test="${empty eventInfo.errorMessage}">
            <a href="BrowseTickets?eventId=${eventInfo.eventId}">${eventInfo.name}</a>: ${eventInfo.message}<br/>
          </c:when>
          <c:otherwise>
            <img src="../images/ico-warning.gif" align="absbottom"/>${eventInfo.errorMessage}<br/>
          </c:otherwise>
        </c:choose>
      </c:forEach>
    </div>
  </div>
</div>
</c:if>


<c:if test="${not empty error}">
  <div class="error">
    <div class="errorText">
      ${error}
    </div>
  </div>
</c:if>

  <div id="dataHeaderGoTo">
  	<span id="goToTextFieldDataContainer">
  		Go to <input type="text" id="goToTextDataField" style="width: 240px" />
  		<span id="goToTextDataFieldStatus" style="color:#888888;font-style:italic"></span>
  	</span>
  	<span id="dataRedirectingContainer" style="display:none">
  		<div>					
  			<div style="float:left"><img src="../images/process-running.gif" align="absbottom" /> Redirecting to &nbsp;</div>
  			<div id="dataRedirectingPage" style="float:left"></div>
  		</div>
  	</span>
  </div>        
  
<h1>Event Bulk Upload</h1>
This allows to create events from a CSV file.
<div class="info">
  <div class="infoText">
    The CSV file should be in the following format:<br/>
    event id, event name, event type, event date, event time, building, state, country<br/>
    event id is optional (if unspecified, a generated value will be assigned)
    The type can be: sport, theater or other.
    The time can be in 12 format (e.g., 5:30 pm) or 24 format (e.g., 17:30).<br/>
    <b>Important Note:</b> The first line will be skipped.
  </div>
</div>
<form enctype="multipart/form-data" action="EditorUploadEventCsvFilePage" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" value="Upload"/>
</form>
<br/>
<div class="info">
  <div class="infoText">
    The Artist Tour Event CSV file should be in the following format:<br/>
    artist name, tour name, event id, event name, event type, event date, event time, building, state, country<br/>
    event id is optional (if unspecified, a generated value will be assigned)
    The type can be: sport, theater or other.
    The time can be in 12 format (e.g., 5:30 pm) or 24 format (e.g., 17:30).<br/>
    <b>Important Note:</b> The first line will be skipped.<br />
    <b>Important Note 2:</b> The tour and artist names must match EXACTLY or new tours/artists will be crated.
  </div>
</div>
<form enctype="multipart/form-data" action="EditorUploadArtistTourEventCsvFilePage" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import Artist, Tour, Event csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" value="Upload"/>
</form>
<br/>
<a href="EditorDownloadEventCsvFile"><img src="../images/ico-download.gif" align="absbottom" /> Download CSV</a>

<script type="text/javascript">

//
// Autocomplete
//

$('#goToTextDataField').val('Edit Artist, Tour, Event');
$('#goToTextDataField').css({color: '#aaaaaa', 'font-style': 'italic'});

$('#goToTextDataField').focus(function() {
	$('#goToTextDataField').val("");
	$('#goToTextDataField').css({color: '#000000', 'font-style': 'normal'});
	$('#goToTextDataFieldStatus').empty();
});

$('#goToTextDataField').blur(function() {
	$('#goToTextDataField').val('Edit Artist, Tour, Event');
	$('#goToTextDataField').css({color: '#aaaaaa', 'font-style': 'italic'});
	$('#goToTextDataFieldStatus').empty();
});

$('#goToTextDataField').autocomplete("AutoCompleteSearch", {
		width: 550,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			if (row[0] == "ARTIST") {			
				return "<div class='searchArtistTag'>ARTIST</div>" + row[1];
			} else if (row[0] == "TOUR") {
				var tourRow = "<div  class='searchTourTag'>TOUR</div>" + row[1];
				if (row[3] != "null" || row[4] != "null") {
				 	tourRow += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
				 }
				 return tourRow;
			} else if (row[0] == "EVENT") {
				return "<div  class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
			}
		},
		formatResult: function(row) {
			return row[1];
		},
		startSearch: function() {
			$('#goToTextDataFieldStatus').empty();
			$('#goToTextDataFieldStatus').append($.IMG({src: '../images/process-running.gif', align: 'absbottom'}));
		},
		stopSearch: function(rows) {
			$('#goToTextDataFieldStatus').empty();
			if (rows.length == 0) {
				$('#goToTextDataFieldStatus').text("No results");
			} else {
				$('#goToTextDataFieldStatus').text("Found " + rows.length + " results");
			}
		}
});

$('#goToTextDataField').result(function(event, row, formatted) {
	$('#goToTextFieldDataContainer').hide();
	$('#dataRedirectingContainer').show();
	
	var pageDescription;
	if (row[0] == "ARTIST") {			
		pageDescription = "<div class='searchArtistTag'>ARTIST</div>" + row[1];
	} else if (row[0] == "TOUR") {
		pageDescription = "<div class='searchTourTag'>TOUR</div>" + row[1];
		if (row[3] != "null" || row[4] != "null") {
		 	pageDescription += " - <font color='#943400'>" +  row[3] + "-" + row[4] + "</font>";
		 }
		 pageDescription += "</div>";		 
	} else if (row[0] == "EVENT") {
		pageDescription = "<div class='searchEventTag'>EVENT</div>" + row[1] + " - <font color='#943400'>" +  formatShortDate(new Date(row[3]*1)) + "</font>";
	}
	
	$('#dataRedirectingPage').html(pageDescription);
	
	if (row[0] == "ARTIST") {
		document.location.href = "EditorEditArtist?action=edit&id=" + row[2];
		return;
	} else if (row[0] == "TOUR") {
		document.location.href = "EditorDetailTour?tourId=" + row[2];
		return;
	} else if (row[0] == "EVENT") {
		document.location.href = "EditorEditEvent?action=edit&id=" + row[2];
		return;
	}
});
	
</script>