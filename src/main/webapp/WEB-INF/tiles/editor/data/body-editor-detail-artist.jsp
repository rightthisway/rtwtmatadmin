<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<script type="text/javascript">
 $(document).ready(function(){	
	$('#all').change(function(){
		if($('#all').is(':checked')){
			checkAll();			
		}else{
			uncheckAll();
		}
		$()
	});
 });
function uncheckAll(){
	  var all = document.getElementsByName("select");
	  
		 for(var i= 0;i<all.length;i++){
		 	all[i].checked=false;
		 }
	}
	function checkAll(){
		  var all = document.getElementsByName("select");
		  
			 for(var i= 0;i<all.length;i++){
			 	all[i].checked=true;
			 }
		}
function removeSelected(){
			var tobeRemoved='';
			 $('input:checkbox:checked.checkbox-event').each(function() {
				 tobeRemoved+=this.id + ',';
			 });
			if(tobeRemoved == ''){
				alert("Please Select Atleast One Event.");
				return;
			}else{
				var conf  = confirm("Are You Sure You Want to Remove Selected Events.");
				if(conf){
					window.location ="EditorDetailTour?action=removeMultiple&artistId="+${artist.id}+"&id=" + tobeRemoved.substring(0,tobeRemoved.length-1);
				}
			}			
			 
		}
function popupEventAudit(eventId){
	var url = "EventAudit?eventId="+eventId;
	newwindow=window.open(url,'name','height=360,width=750, scrollbars=yes');
	if (window.focus) {newwindow.focus()}
	return false;
	
}
</script>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; <a href="EditorTCAP">Manage Data</a> 
	&gt; <a href="EditorManageArtist">${artist.name}</a> 
	&gt; ${artist.name}
</div>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
	<br /><br />
</c:if>

<%-- <div id="quickLinks">
	<a href="BrowseEvents?artistId=${artist.id}">Browse Events</a>
</div> --%>
<div style="clear:both"></div>

<h1>${artist.name} Detail</h1>

<b>${artist.name}:</b> <img src="../images/ico-${fn:toLowerCase(artist.grandChildTourCategory.childTourCategory.tourCategory.tourType)}.gif" title="${artist.grandChildTourCategory.childTourCategory.tourCategory.tourType}" align="absbottom">${artist.name}<br />
 <!--to display sport/concert instead of sport or concert  -->
<c:choose>
	<c:when test="${artist.grandChildTourCategory.childTourCategory.tourCategory.tourType} =='CONCERT' || ${artist.grandChildTourCategory.childTourCategory.tourCategory.tourType} =='SPORT'}">
		<b>Type:</b>SPORT/CONCERT<br />
	</c:when>
	<c:otherwise>
			<b>Type:</b>${artist.grandChildTourCategory.childTourCategory.tourCategory.tourType}<br />
	</c:otherwise>
</c:choose>

<b>Artist:</b> ${artist.name}<br />
<br />
<a href="EditorEditArtist?id=${artist.id}&action=edit" class="medButton"><img src="../images/ico-edit.gif" align="absbottom" /> Edit Details</a> 
<br/>
<a href="EditorEditMultipleEvents?artistId=${artist.id}" class="medButton"><img src="../images/ico-edit.gif" align="absbottom" /> Edit Multiple Events</a>

<div style="clear: both"></div>

<h1>Manage events</h1>

<a href="EditorEditEvent?action=create&artistId=${param.artistId}"><img src="../images/ico-plus.gif" align="absbottom" /> Add new event</a>
<br /><br />

<div align="center">
	<a href="javascript:updateAutoCorrectEvent();" ><b>Update</b></a>&nbsp | &nbsp
	<a href="javascript:removeSelected()"><b>Remove</b></a>
</div>
<form action="EditorUpdateAutoCorrectEvent" method="post" id="updateAutoCorrectEvent" name="updateAutoCorrectEvent">
	<input type="hidden" id="artistId" name = "artistId" value="${artist.id}"/>
	<display:table class="list" name="${events}" id="event" requestURI="EditorDetailArtist">
		 <display:column title='All<br/><input type="checkbox" id="all" class="all-event">'>
      		<input type="checkbox" name="select" class="checkbox-event" id="${event.id}"/>
    	</display:column>
		<display:column title="Id" sortable="true" property="id" href="EditorEditEvent?action=edit" paramId="id" paramProperty="id" />
		<display:column title='AutoCorrect<br/><input type="checkbox" name="title-autoCorrect" id="title-autoCorrect">' >
			<input type="checkbox" name="${event.id}-autoCorrect" id="${event.id}-autoCorrect" onchange="checkAutoCorrect()" class="checkbox-autoCorrect" <c:if test="${event.autoCorrect}">checked </c:if>>
		</display:column>
		<display:column title='IgnoreTBDTime<br/><input type="checkbox" name="title-ignoreTBDTime" id="title-ignoreTBDTime">' >
			<input type="checkbox" name="${event.id}-ignoreTBDTime" id="${event.id}-ignoreTBDTime" onchange="checkIgnoreTBDTime()" class="checkbox-ignoreTBDTime" <c:if test="${event.ignoreTBDTime}">checked </c:if>>
		</display:column>
		<display:column title="Name" sortable="true" sortProperty="name">
		  <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" title="event.eventType" align="absbottom">${event.name}
		</display:column>
		<display:column title="Date" sortable="true">
			<b>
			<c:choose>
				<c:when test="${not empty event.date}">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
				</c:when>
				<c:otherwise>
					TBD
				</c:otherwise>
			</c:choose>
			</b>
		</display:column>
		<display:column title="Time" sortable="true">
			<b>
				<c:choose>
				  <c:when test="${not empty event.time}">
					<fmt:formatDate pattern="HH:mm" value="${event.time}" />
				  </c:when>
				  <c:otherwise>
					TBD
				  </c:otherwise>
				</c:choose>	
				
			</b>
		</display:column>
		<display:column title="Venue" sortable="true" property="venue.building" />
		<display:column title="State" sortable="true" property="venue.state" />
		<display:column title="Country">
			<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
				<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
			</c:if>
		</display:column>
		<display:column title="Crawl Count" sortable="true" sortProperty="crawlCount" class="text-align-center" property="crawlCount" />
		<display:column>
			<a href="EditorEditEvent?id=${event.id}&action=edit&multiEveUrl=EditorDetailArtist?artistId=${artist.id}"><img src="../images/ico-edit.gif" align="absbottom" />  edit</a> - 
			<a href="javascript:confirmRemove(${event.id})"><img src="../images/ico-delete.gif" align="absbottom" /> remove</a>						
		</display:column>
		<display:column>
			<c:if test="${not empty event.admitoneId}">								
				<a href="javascript:confirmUnLink(${event.id})"><b>UnLink</b></a>
			</c:if>
		</display:column>  
		<display:column title="Action">
		<%-- <a href="javascript:confirmRemove(${event.id})"><img src="../images/ico-delete.gif" align="absbottom" /> Audit</a>--%>
		<input type="button" value="Audit"  class="smallButton" id="Audit1" name="Audit" onClick="popupEventAudit(${event.id});"/>
		<%-- <a href="EditorEditEvent?id=${event.id}&action=edit&multiEveUrl=EditorDetailTour?artistId=${artist.id}"><img src="../images/ico-edit.gif" align="absbottom" />  edit</a> --%>
		</display:column>
	</display:table>
</form>
<c:if test="${not empty events}">
	<br />
	<a href="EditorEditEvent?action=create&toursId=${param.artistId}"><img src="../images/ico-plus.gif" align="absbottom" /> Add new event</a>
</c:if>

<br /><br />
<a href="EditorManageArtists">Back to manage artist page</a>

<div id="deleteEventDialog" title="" style="display:none">
	<span id="deleteEventDialogMessage" align="center"></span>
</div>

<script type="text/javascript">
	
	function checkIgnoreTBDTime(){
		var flag=true;
		$('.checkbox-ignoreTBDTime').each(function(i, obj) {
			if(!$(obj).is(':checked')){
				flag=false;
				return;
			}
		});
		
		$("#title-ignoreTBDTime").attr('checked', flag);
		
	}
	function checkAutoCorrect(){
		var flag=true;
		$('.checkbox-autoCorrect').each(function(i, obj) {
			if(!$(obj).is(':checked')){
				flag=false;
				return;
			}
		});
		$("#title-autoCorrect").attr('checked', flag);
	}
	function updateAutoCorrectEvent(){
		$('#updateAutoCorrectEvent').submit();
	}
	$("#title-autoCorrect").click(function(){
		var flag = false;
		if($("#title-autoCorrect").is(':checked')){
			flag=true;
		}
		
		$('.checkbox-autoCorrect').each(function(i, obj) {
			$(obj).attr('checked', flag);
		});
	});
	$("#title-ignoreTBDTime").click(function(){
		var flag = false;
		if($("#title-ignoreTBDTime").is(':checked')){
			flag=true;
		}
		
		$('.checkbox-ignoreTBDTime').each(function(i, obj) {
			$(obj).attr('checked', flag);
		});
	});
	$("#deleteEventDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

	function confirmRemove(eventId) {
		$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Please wait');		
		$('#deleteEventDialog').dialog('open');
		$('#deleteEventDialog').dialog('option', 'title', 'Please wait...');
		$('#deleteEventDialog').dialog('option', 'buttons', {});

        DataDwr.getNumTickets(eventId, function(response) {
			$('#deleteEventDialog').dialog('option', 'title', 'Delete event?');
        	$('#deleteEventDialogMessage').html(
        		"The event has " + response + " tickets attached to it. "
        		+ "<br /> If you delete it, all the associated tickets will be deleted as well. "
        		+ "<br /><br />Do you really want to delete it?");

			$("#deleteEventDialog").dialog('open');
			$("#deleteEventDialog").dialog('option', 'buttons', {
				'Delete': function() {
					document.location.href = "EditorDetailArtist?id=" + eventId + "&artistId=${param.artistId}&action=remove";			
					$('#deleteEventDialog').dialog('option', 'title', 'Deleting event...');
					$('#deleteEventDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Deleting event...');		
					$("#deleteEventDialog").dialog('option', 'buttons', {});
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			});
		});
		
	};
	
	function confirmUnLink(eventId) {		
		var flag = confirm("Want to unLink ticketgallary Event?");
		if(flag){
			DataDwr.unLinkEvent(eventId, function(response) {
				if(response){
					alert("Successfully Unlinked.");
					location.reload();
				}else{
					alert("Error");
				}
			});
		}
	}
	
	$(document).ready(function() {
		checkIgnoreTBDTime();
		checkAutoCorrect();
	});
</script>