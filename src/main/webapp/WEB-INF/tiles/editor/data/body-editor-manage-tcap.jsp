<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<script type="text/javascript" src="../js/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="../js/ext/ext-all.js"></script>
<script type="text/javascript" src="../js/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="../js/livegrid/livegrid-all.js"></script>
<script type="text/javascript" src="../js/livegrid/supplement/ArrayReader.js"></script>
<link rel="stylesheet" type="text/css" href="../js/ext/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="../js/livegrid/resources/css/ext-ux-livegrid.css" />


<div id="breadCrumbPath" class="breadCrumbPathEditor">
	<a href="..">Home</a> 
	&gt; Manage TCAP
</div>


<h1>Manage TCAP</h1>

<c:if test="${not empty param.info}">
	<div class="info"><div class="infoText">${param.info}</div></div>
</c:if>

<table>
  <tr>
			<td><b>Artist:</b></td>
			<td>
				
				<%-- <input type="hidden" name="artistId" id="artistId" value="${event.artistId}"> --%>
				
				<span id="artistLabelSpanId"  <c:if test="${not empty editorTCAPArtistId && editorTCAPArtistId != 0}">style="display: block;"</c:if> 
				<c:if test="${empty editorTCAPArtistId || editorTCAPArtistId == 0 }">style="display: none;"</c:if>  >
				<span id="artistNameDiv" >
					<c:choose>
						<c:when test="${not empty artistName }">
							${artistName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
				</span>	
					<input type="button" value="Change Artist" class="medButton" onclick="callChangeArtist();"/>
				</span>
				
				
				<span id="artistSelectSpanId" <c:if test="${not empty editorTCAPArtistId && editorTCAPArtistId != 0 }">style="display: none;"</c:if> 
					<c:if test="${empty editorTCAPArtistId || editorTCAPArtistId == 0}">style="display: black;"</c:if> >
				
					<input type="text" name ="autoArtist" id ="autoArtist" >
				</span>		
			</td>
			
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><b>Venue</b></td>
			<td>
				<c:choose>
					<c:when test="${(empty editorTCAPArtistId || editorTCAPArtistId == 0) && empty venues}">
						<span id="venueLabelSpanId"  <c:if test="${not empty editorTCAPVenueId && editorTCAPVenueId != 0}">style="display: black;"</c:if> 
				
				<c:if test="${empty editorTCAPVenueId || editorTCAPVenueId == 0}">style="display: none;"</c:if>  >
				<span id="venueNameDiv" >
					<c:choose>
						<c:when test="${not empty venueName }">
							${venueName}
						</c:when>
						<c:otherwise>
								-
						</c:otherwise>
					
					</c:choose>
					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" <c:if test="${not empty editorTCAPVenueId && editorTCAPVenueId != 0 }">style="display: none;"</c:if> 
				<c:if test="${empty editorTCAPVenueId || editorTCAPVenueId == 0 }">style="display: block;"</c:if> >
				
					<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
					<c:forEach var="venue" items="${venues}">
						<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
					</c:forEach>
				</form:select> --%>
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
					</c:when>
					<c:otherwise>
						<select onchange="changeFilterVenue(this.value);">
							<option value="">Select</option>
							<c:forEach var="venue" items="${venues}">
								<option value="${venue.id}" style="height:20px;" <c:if test="${venue.id == editorTCAPVenueId}">selected</c:if>>${venue.building}</option>
							</c:forEach>
						</select>
					</c:otherwise>
				</c:choose>		
			</td>
			
		</tr>
</table>

<c:if test="${editorTCAPVenueId != null && editorTCAPVenueId != 0}">
	<c:set var="link" value="&venueId=${editorTCAPVenueId}"></c:set>
	<c:set var="link1" value="&venueId=${editorTCAPVenueId}"></c:set>
</c:if>
<c:if test="${editorTCAPArtistId != null && editorTCAPArtistId != 0}">
	<c:set var="link" value="&artistId=${editorTCAPArtistId}"></c:set>
	<c:set var="artistLink" value="&artistId=${editorTCAPArtistId}"></c:set>
</c:if>
<br /><br />

<c:choose> 
<c:when test="${not empty events}">
Export to :<c:if test="${editorTCAPVenueId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=1&amp;venueId=${editorTCAPVenueId}">CSV </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=1&amp;artistId=${editorTCAPArtistId}">CSV </a></c:if> | <c:if test="${editorTCAPVenueId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=2&amp;venueId=${editorTCAPVenueId}">Excel </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=2&amp;artistId=${editorTCAPArtistId}">Excel </a></c:if> | <c:if test="${editorTCAPVenueId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=3&amp;venueId=${editorTCAPVenueId}">Xml </a></c:if>
<c:if test="${editorTCAPArtistId !=0}"><a href="EditorTCAP?6578706f7274=1&amp;d-148316-e=3&amp;artistId=${editorTCAPArtistId}">Xml </a></c:if>
<display:table class="list" name="${events}" id="event"  requestURI="EditorTCAP" pagesize="50" export="true">
  
	<display:setProperty name="export.excel.filename" value="TCAP.xls"/> 
	<display:setProperty name="export.csv.filename" value="TCAP.csv"/>
	<display:column style="width:150px" title="Name" sortable="true" property="name" href="EditorEditEvent?action=edit${artistLink}${link1}" paramId="id" paramProperty="id"/>
	<display:column title="Date"  sortable="true"  property="formatedDate" />
	<display:column title="VenueId"  sortable="true" property="venue.id"/>
	<display:column title="Venue"  sortable="true" property="venue.building" 
	href="EditorEditVenue?action=edit&${link}" paramId="id" paramProperty="venue.id"/>
	<display:column title="Artist"  sortable="true" property="artist.name" 
	href="EditorEditArtist?action=edit&${link}" paramId="id" paramProperty="artist.id"/>
	<display:column title="Parent"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.tourCategory.name"/>
	<display:column title="Child"  sortable="true" property="artist.grandChildTourCategory.childTourCategory.name"/>
	<display:column title="Grand Child"  sortable="true" property="artist.grandChildTourCategory.name"/>
    <display:column title="Crawl"  sortable="true"  property="crawlCount" />
	<display:column title="Zones"  sortable="true" property="uniqueCategoryGroupName" 
    href="EditorEditCategories?&${link}" paramId="artistId" paramProperty="artist.id"/>
   <%--  <display:column title='Presale Event' sortable="true">
		<c:choose>
				<c:when test="${event.presaleEvent}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column> --%>
    <display:column title='AutoCorrect'>
		<c:choose>
				<c:when test="${event.autoCorrect}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title='IgnoreTBDTime' >
		<c:choose>
			<c:when test="${event.ignoreTBDTime}">Yes</c:when>
			<c:otherwise>No</c:otherwise>
		</c:choose>
	</display:column>
	<display:column title="Map" media="html" sortable="false" >	
	<c:choose> 
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}">		
	<a href="javascript:loadImage(${event.id}, '${event.venueCategory.categoryGroup}')">
	<img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" />
	</a>
	</c:when>
	<c:otherwise>
    		<font color="red">No Map</font>
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="is Map">
	<c:choose>
	<c:when test="${event.venueCategory ne null and event.venueCategory.isVenuMap}">Yes</c:when>
	<c:otherwise>No</c:otherwise>
	</c:choose>
	</display:column>
	<display:column title="Have Link " media="csv excel xml" sortable="false" property="formatedAdmitoneId" />
  <%--  <display:column title="Link" media="html" sortable="false" >
    	<c:if test="${empty event.admitoneId}">
    		<font color="red">False</font>
    	</c:if>
    	<c:if test="${not empty event.admitoneId}">
    		<font color="green">${admitoneEventNameDateTime}</font>
    	</c:if>
    </display:column>  
  <display:column title="Link"   property="admitoneEventDetails" />
 --%>
   <display:column title="LinkTo" media="html" sortable="false" >
    	<%-- <c:if test="${empty event.admitoneId || event.admitoneEventDetails }">
    		<font color="black">-</font>
    	</c:if>
    	<c:if test="${not empty event.admitoneId}">
    		<font color="black">${event.admitoneEventDetails}</font>
    	</c:if>--%>
    	<c:choose>
  <c:when test="${empty event.admitoneId}">
   <font color="black">-</font>
  </c:when>
  <c:when test="${not empty event.admitoneEventDetails}">
    <font color="black">${event.admitoneEventDetails}</font>
  </c:when>
  <c:otherwise>
    <font color="black">-</font>
  </c:otherwise>
</c:choose>
    </display:column>
    <display:column title="Price Adjustment"  sortable="false">
    	${shPercentAdjustment}
    </display:column>
<display:setProperty name="export.banner.placement" value="top" />
</display:table>
</c:when >
<c:otherwise>
There is no event for selected option.
</c:otherwise>
</c:choose>
<script type="text/javascript">

$(document).ready(function(){
	$('#autoArtist').autocomplete("AutoCompleteArtist", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoArtist').val(row[1]);
		window.location.href = "EditorTCAP?venueId=${editorTCAPVenueId}&artistId="+row[0];
	});
	
});

function callChangeArtist(){ 
	$('#artistLabelSpanId').hide();
	$('#artistSelectSpanId').show();
	$('#tempArtistId').val('');
}

function callChangeVenue(){
	$('#venueLabelSpanId').hide();
	$('#venueSelectSpanId').show();
	$('#tempVenueId').val('');
}

$('#autoVenue').autocomplete("AutoCompleteVenue", {
width: 650,
max: 1000,
minChars: 2,		
formatItem: function(row, i, max) {
	return row[1];
}
}).result(function (event,row,formatted){
	$('#autoVenue').val(row[1]);
	location.href='EditorTCAP?artistId=${editorTCAPArtistId}&venueId=' + row[0];
});

function changeFilterArtist(artistId) {
	document.location.href = "EditorTCAP?artistId=" + artistId;
	if(${editorTCAPVenueId!=null}){
		document.location.href = "EditorTCAP?artistId=" + artistId +"&venueId="+"${editorTCAPVenueId}";
	}else{    
		document.location.href = "EditorTCAP?artistId=" + artistId;
	}
	
};

function changeFilterVenue(venueId) {
	if(${editorTCAPArtistId!=null}){
		document.location.href = "EditorTCAP?venueId=" + venueId +"&artistId=${editorTCAPArtistId}";
	}else{    
         document.location.href = "EditorTCAP?venueId=" + venueId;
	}
};

function loadImage(eventId, catScheme) {
	window.open('EventImage?eventId=' + eventId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};
</script>
