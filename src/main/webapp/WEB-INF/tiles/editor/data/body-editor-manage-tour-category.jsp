<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Tour Category
</div>

<h1>Tour Category</h1>


<c:if test="${not empty info}">
  <div class="info"><div class="infoText">${info}</div></div>
  <br /><br />
</c:if>


<a href="EditorEditTourCategory?action=create"><img src="../images/ico-plus.gif" align="absbottom" /> Add new Tour Category</a>
<br/>

<br /><br />
<display:table class="list" name="${tourCategorys}" id="tourCategory" requestURI="EditorManageTourCategory" defaultsort="2" pagesize="20">
    <display:column title="Id" sortable="true" property="id" href="EditorEditTourCategory" paramId="id" paramProperty="id"  />
    <display:column title="Name" sortable="true" property="name" href="EditorEditTourCategory" paramId="id" paramProperty="id"  />
      <display:column>
      	<a href="EditorManageTourCategory?tourCategoryId=${tourCategory.id}">Remove</a>
      </display:column>
</display:table>