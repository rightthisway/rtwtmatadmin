<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; <a href="EditorManageVenues">Venues</a>
  &gt; Edit Venue
</div>

<h1>
<c:choose>
	<c:when test="${empty param.id}">Add a new venue</c:when>
	<c:otherwise>Edit venue</c:otherwise>
</c:choose>
</h1>

<form:form commandName="venue" method="POST">
	<input type="hidden" name="tcap" value="${fromTcap}"/>
	<table>
		<tr>
			<td>Venue</td>
			<td>
				<form:input path="building" cssClass="longInput" />
				<form:errors path="building" cssClass="error" />			
			</td>
		</tr>
		
		<tr>
			<td>City</td>
			<td>
				<form:input path="city" cssClass="longInput" />
				<form:errors path="city" cssClass="error" />			
			</td>
		</tr>

		<tr>
			<td>State</td>
			<td>
				<form:select path="state">
					<c:forEach var="state" items="${constants.states}">
						<form:option value="${state.symbol}">${state.name}</form:option>
					</c:forEach>				
				</form:select>
				<form:errors path="state" cssClass="error" />			
			</td>
		</tr>

		<tr>
			<td>Country</td>
			<td>
				<form:select path="country">
					<c:forEach var="country" items="${constants.countries}">
						<form:option value="${country.symbol}">${country.name}</form:option>
					</c:forEach>				
				</form:select>
				<form:errors path="country" cssClass="error" />			
			</td>
		</tr>
		<tr>
			<td>Venue Type</td>
			<td>
				<form:select path="venueType">
					<form:option value="">--Select---</form:option>
					<form:option value="SPORT">SPORT/CONCERT</form:option>
 			 		<form:option value="THEATER">THEATER</form:option>
 			  		<%-- <form:option value="CONCERT">CONCERT</form:option> --%>
 			 		<form:option value="OTHER">OTHER</form:option>
 			 	</form:select>
			</td>
		</tr>

	</table>
	<input type="submit" name="submit" value="Save Changes" class="medButton" />
	<input type="button" value="Cancel" class="medButton" onclick="document.location.href='EditorManageVenues'" />


<h1>Event List</h1>
<display:table class="list" name="${events}" id="event" requestURI="EditorEditVenue" defaultsort="2" pagesize="20">
    <display:column title="EventId" sortable="true" property="id" href="EditorEditEvent?action=edit" paramId="id" paramProperty="id"  /> 
    <display:column title="Event Name" sortable="true" sortProperty="name">
      <a href="EditorEditEvent?action=edit&id=${event.id}"><img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" title="event.eventType" align="absbottom">${event.name}</a>
    </display:column>
    <display:column title="Date" sortable="true">
    
	    <fmt:formatDate pattern="MM/dd/yyyy zzz" value="${event.date}" />
	   		<c:choose>
		  <c:when test="${not empty event.time}">
		    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
		  </c:when>
		  <c:otherwise>
		    TBD
		  </c:otherwise>
		</c:choose>
    	
    </display:column>
    <display:column title="Venue" sortable="true" property="venue.building" />
     <display:column title="City" sortable="true" property="venue.city" />
    <display:column title="State" sortable="true" property="venue.state" />
    <display:column title="Country">
    	<c:if test="${not empty event.venue.country && event.venue.country != 'null' }">
    		<img src="../images/flags/${event.venue.country}.gif" />${event.venue.country}
    	</c:if>
    </display:column>
    
</display:table>

<h2>Merge Venues</h2>

	<div class="info">
		<div class="infoText">
			MERGED VENUES CANNOT BE UNDONE.
			THIS VENUE WOULD BE MERGED WITH THE SELECTED VENUE.
			THE EVENTS OF THE MERGED VENUE WOULD BE DELETED. 
			THE SELECTED VENUE WOULD TAKE THE EVENTS OF THE MERGED VENUE.
		</div>
	</div> 
	Selected Venue: 
	
<div id="mergeVenueDialog" title="" style="display:none">
	<span id="mergeVenueDialogMessage" align="center"></span>
</div>
	<span id="venueLabelSpanId" style="display: none;">
				<span id="venueNameDiv" >

					</span>
					
					<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
				</span>
				
				
				<span id="venueSelectSpanId" style="display: block;">
				
				
				<input type="text" name ="autoVenue" id ="autoVenue" >
				</span>
						<input type="hidden" id="venueCmb" name="venueCmb"/>
	<%-- <select id="venueCmb" onchange="showMergeButton()">
		<option value="">NONE SELECTED</option>
		<c:forEach var="ven" items="${venues}">
			<c:if test="${ven.id != venue.id}">
				<option value="${ven.id}" >${ven.building} - ${ven.id} </option>
			</c:if>  
		</c:forEach>
	</select> --%>
	<input id="mergeVenuesButton" type="button" class="medButton" onclick="mergeVenues()"  value="Merge Venues"  style="display:none"/>

</form:form>

<script type="text/javascript" language="javascript">
		$(document).ready(function() {
		$('#building').focus();
	});
		
		$('#autoVenue').autocomplete("AutoCompleteVenue", {
			width: 650,
			max: 1000,
			minChars: 2,		
			formatItem: function(row, i, max) {
				return row[1] + " - " + row[0];
			}
		}).result(function (event,row,formatted){
			$('#autoVenue').val('');
			$("#venueCmb").val(row[0]);
			$('#venueNameDiv').text(row[1]);
				
			$('#venueLabelSpanId').show();
			$('#venueSelectSpanId').hide();
			
			showMergeButton();
		});
		
		function callChangeVenue(){
			$('#venueLabelSpanId').hide();
			$('#venueSelectSpanId').show();
			$('#venueCmb').val('');
		}

	//merge venue	
	$("#mergeVenueDialog").dialog({
		autoOpen: false,
		bgiframe: true,
		resizable: false,
		width: 350,
		height:160,
		minHeight:160,
		maxHeight:160,
		modal: true,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		},
		buttons: {}
	});

	
	function showMergeButton() {
		if ($('#mergeVenuesButton').css('display') == 'none') {
			$('#mergeVenuesButton').show();
		}
	};
	
	
	function mergeVenues() {
		var mergeId = "${venue.id}";
		var goodId = $('#venueCmb').val();

		
		$('#mergeVenueDialogMessage').html(
        		"If you merge this venue, all the associated events will be "
        		+ "<br /> transferred from " + mergeId + " to " + goodId + ". "
        		+ "<br /><br />Do you really want to merge this venue?");
        $("#mergeVenueDialog").dialog('open');
        $('#mergeVenueDialog').dialog('option', 'title', 'Merge Venues?');
		$('#mergeVenueDialog').dialog('option', 'buttons', {});
		$("#mergeVenueDialog").dialog('option', 'buttons', {
			'Merge': function() {
				$('#mergeVenueDialog').dialog('option', 'title', 'Merging Venues...');
				$('#mergeVenueDialogMessage').html('<br /><br /><img src="../images/process-running.gif" align="absbottom"/>&nbsp;Merging Venues...');		
				$("#mergeVenueDialog").dialog('option', 'buttons', {});
		       DataDwr.mergeVenues(mergeId, goodId, function(response) {
				   $('#mergeVenueDialogMessage').html(response + "!");
				   $("#mergeVenueDialog").dialog('option', 'buttons', {
					   Close: function() {
					   		$("#mergeVenueDialog").dialog('close');
							
					        //return home
					        document.location.href = "EditorManageVenues"; 
					   }
				   });
       			});			
			},
			Cancel: function() {
				$("#mergeVenueDialog").dialog('close');
			}
		}); 
	};
</script>
