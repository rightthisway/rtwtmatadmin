<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Quick Create Event
</div>

<h1>Quick Create Event</h1>
<div style="border-top:1px  dashed black;margin-top:4px;padding-top:4px; padding-bottom:5px">
<h2>Create / Select Artist</h2>
  Existing Artists: 

	<c:choose>
	  <c:when test="${not empty artistId}">
		  
		  <select id="artist" >
		  	<c:forEach var="art" items="${artists}">
		    	<option value="${art.id}">${art.name}</option>
		  	</c:forEach>
		  </select><a href="EditorQuickCreate">(Clear All Data)</a> 
		  	
	  </c:when>
	  <c:when test="${not empty tourId}">
		  
		  <select id="artist" >
		  	<c:forEach var="art" items="${artists}">
		    	<option value="${art.id}">${art.name}</option>
		  	</c:forEach>
		  </select><a href="EditorQuickCreate">(Clear All Data)</a> 
		  	
	  </c:when>
	  <c:otherwise>
		  
		  <select id="artist" onchange="location.href='EditorQuickCreate?artistId=' + $(this).val()">
		  	<option value="">NONE SELECTED</option>
		  	<c:forEach var="art" items="${artists}">
		    	<option value="${art.id}">${art.name}</option>
		  	</c:forEach>
		  </select>
		  	
	  </c:otherwise>
	</c:choose>
	
<div style="border-top:1px  dashed black;margin-top:4px;padding-top:4px; padding-bottom:5px">
    New Artist Name: 
    <input id="artistName" type="text"/>
<h2>Create / Select Tour</h2>
  Existing Tours: 

	<c:choose>
	  <c:when test="${not empty artistId}">
	  
		  <select id="tour">
		  	<option value="">NONE SELECTED</option>
		  	<c:forEach var="tor" items="${tours}">
		    	<option value="${tor.id}">${tor.name}</option>
		  	</c:forEach>
		  </select><a href="EditorQuickCreate">(Clear All Data)</a> 
		  
	  </c:when>
	  <c:when test="${not empty tourId}">
		  <select id="tour">
		  	<c:forEach var="tor" items="${tours}">
		    	<option value="${tor.id}">${tor.name}</option>
		  	</c:forEach>
		  </select><a href="EditorQuickCreate">(Clear All Data)</a> 
		  
	  </c:when>
	  <c:otherwise>
		  
		  <select id="tour" onchange="location.href='EditorQuickCreate?tourId=' + $(this).val()">
		  	<option value="">NONE SELECTED</option>
		  	<c:forEach var="tor" items="${tours}">
		    	<option value="${tor.id}">${tor.name}</option>
		  	</c:forEach>
		  </select>
		  	
	  </c:otherwise>
	</c:choose>
	
<div style="border-top:1px  dashed black;margin-top:4px;padding-top:4px; padding-bottom:5px">
    New Tour Name: 
    <input id="tourName" type="text"/>
    <br />
    New Tour Type: 
    <select id="tourType">
  		<option value="">NONE SELECTED</option>
 		<option value="0">SPORT</option>
 		<option value="1">THEATER</option>
 		<option value="2">CONCERT</option>
 		<option value="3">OTHER</option>
  	</select><br />	
	Parent Category:
	<select name="parnetName" id="parentSelect" style="width: 300px">
	<option value="">NONE SELECTED</option>
		 <c:forEach var="parent" items="${parentCategories}">		
			<option value="${parent.id}">${parent.name}</option>
		  </c:forEach>
	</select> <br />
	Child Category:
	<select name="childName" id="childSelect" style="width: 300px">
	<option value="">NONE SELECTED</option>
		<c:forEach var="child" items="${childCategories}">		
			<option value="${child.id}">${child.name}</option>
		</c:forEach>
	</select> <br />
	GrandChild Category:
	<select name="grandchildName" id="grandChildSelect" style="width: 300px">
	<option value="">NONE SELECTED</option>
		<c:forEach var="grandChild" items="${grandChildCategories}">		
			<option value="${grandChild.id}">${grandChild.name}</option>
		  </c:forEach>  
	</select><br />
<h2>Create / Select Venue</h2>
  Existing Venues: 
  <select id="venue">
  	<option value="">NONE SELECTED</option>
  	<c:forEach var="ven" items="${venues}">
    	<option value="${ven.id}">${ven.building}</option>
  	</c:forEach>
  </select>
<div style="border-top:1px  dashed black;margin-top:4px;padding-top:4px; padding-bottom:5px">
    New Venue Building: 
    <input id="venueBuilding" type="text"/>
    <br />
    New Venue City: 
    <input id="venueCity" type="text"/>
    <br />
    New Venue State: 
    <input id="venueState" type="text"/>
    <br />
    New Venue Country: 
    <input id="venueCountry" type="text"/>
<h2>Create Event</h2>
    New Event Id: 
    <input id="eventId" type="text"/>(If not entered, TMAT will create one)
    <br />
    New Event Name: 
    <input id="eventName" type="text"/>
    <br />
    New Event Date:
	TBD: <input type="radio" name="dateTbd" value="true" /> &nbsp; <b>or</b> &nbsp;
				Date: <input type="radio" name="dateTbd" value="false" />
				<select name="year" id="year">
					<c:forEach var="year" begin="2010" end="2050" step="1">
				    <option value="${year}">${year}</option>
					</c:forEach>
				</select>				
				/
				<select name="month" id="month">
					<c:forEach var="month" begin="1" end="12" step="1">
				    <option value="${month}"><fmt:formatNumber pattern="00" value="${month}" /></option>
				  </c:forEach>
				</select>				
				/
				<select name="date" id="date">
					<c:forEach var="day" begin="1" end="31" step="1">
				    <option value="${day}"><fmt:formatNumber pattern="00" value="${day}" /></option>
				  </c:forEach>
				</select>			
				<input type="text" id="localDateCalendar" style="visibility:hidden;width:0px;" />
    <br />
    New Event Time:
	TBD: <input type="radio" name="timeTbd" value="true" />&nbsp; <b>or</b> &nbsp;
			    Time: <input type="radio" name="timeTbd" value="false" />			    			
				<select name="hour" id="hour">
					<c:forEach var="hour" begin="0" end="23" step="1">
				    <option value="${hour}"><fmt:formatNumber pattern="00" value="${hour}" /></option>
					</c:forEach>
				</select>
				<select name="minute" id="minute">
					<c:forEach var="minute" begin="0" end="59" step="1">
				    <option value="${minute}"><fmt:formatNumber pattern="00" value="${minute}" /></option>
					</c:forEach>
				</select>	
				
    <br />
    New Event Time Zone:    
	<select id="eventZone">
		<option value="">-- Select --</option>
		<c:forEach var="timeZone" items="${constants.timeZones}">
				  	<c:set var="minuteCount" value="${timeZone.rawOffset / 1000 / 60}" />
				    <option value="${timeZone.ID}">
				    	${timeZone.ID}				    				    	
				    	<c:choose>
				    		<c:when test="${minuteCount < 0}">
						    	[-<fmt:formatNumber pattern="00" value="${-minuteCount / 60}" />:<fmt:formatNumber pattern="00" value="${-minuteCount % 60}" />]
				    		</c:when>
				    		<c:otherwise>
						    	[+<fmt:formatNumber pattern="00" value="${minuteCount / 60}" />:<fmt:formatNumber pattern="00" value="${minuteCount % 60}" />]
				    		</c:otherwise>
				    	</c:choose>
				    </option>
		 </c:forEach>
	 </select>
    
<br/>
<input id="quickCreateEventButton" type="button" class="medButton" onclick="makeEvent()"  value="Create Event"/>
<br/>
    
<script type="text/javascript">
 $("#parentSelect").change(function () {
	
	
          var str = "";
          $("#parentSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              });
				setChildInfo(str);	
							
        })
        .change();
	
	$("#childSelect").change(function () { 
	 
          var str = "";
          $("#childSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 		
				setGrandChildInfo(str);								
        })
        .change();
		
	$("#grandChildSelect").change(function () {
	 
	
	 
          var str = "";
          $("#grandChildSelect option:selected").each(function () {  
				str += $(this).val() + ";";		  
              }); 				
        })
        .change();
		
function  setChildInfo(parentid){
	   $('#childSelect').children().remove();
	   $('#childSelect').append("<option value=''>NONE SELECTED</option>");
	   $('#grandChildSelect').children().remove();  
	   $('#grandChildSelect').append("<option value=''>NONE SELECTED</option>");	    
	   var parent = parentid;	
		////alert(parent);
		var myurl = "GetCategory?parent="+parent;	
		 if(parent!='' && parent!='-Select-'){
	 	 $.ajax({
			url:myurl,
			success: function(res){
			//	//alert(res);
			//var jsonData = jQuery.parseJSON(res);
			var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#childSelect').append(rowText);
              }				
				
			}
 
		}); 
		}
		 
		 
} 
function  setGrandChildInfo(childID){
	$('.error').hide();
	
	$('#grandChildSelect').children().remove();	
	$('#grandChildSelect').append("<option value=''>NONE SELECTED</option>");
	 
	var child = childID;
	 
	 var url = "GetCategory?child="+child;	
	 if(child!=''){
	 $.ajax({
		url:url,
		success: function(res){
			 var jsonData = JSON.parse(res);
			////alert(jsonData);
			for (var i = 0; i < jsonData.length; i++) {
				var data = jsonData[i];
				////alert(data);
				var rowText = "<option value="+data.id+ ">"+ data.name+"</option>"			
				$('#grandChildSelect').append(rowText);
              }	
			 //$('#grandChildSelect').append(res);
			 
			}
		
		
	}); 
	 }
	 
 } 
$(document).ready(function() {
		$('#name').focus();		
		$('#year').val(new Date().getYear()+1900);
		$('#month').val(new Date().getMonth()+1);
		$('#date').val(new Date().getDate());
		$('#localDateCalendar').datepicker({
			'buttonImageOnly': true,
	        buttonImage: '../images/ico-calendar.gif',
	        showOn: 'button',
	        showButtonPanel: true,
			changeMonth: true,
			changeYear: true,	        
	        dateFormat:'yy-mm-dd',
	        minDate: new Date((new Date()).getTime() - ((23 * 60 + 59) * 60) * 1000),
	        maxDate: new Date((new Date()).getTime() + 10 * 365 * 24 * 60 * 60 * 1000),
	        beforeShow: function(input) {
	        	$('#localDateCalendar').val(($('#year').val()) + "-" + $('#month').val() + "-" + $('#date').val());
	        },
	        onSelect: function(dateText) {
				var tokens = dateText.split("-");
				$('#year').val(tokens[0]);
				$('#month').val(tokens[1]);
				$('#date').val(tokens[2]);
			}
		});
		
		
	});
	function createEvent(aId, newArtistName, tId, newTourName, newTourType, newTourGrandChildType, vId, newVenueBuilding, newVenueCity, newVenueState, newVenueCountry, newEventId, newEventName, newEventDate, newEventTime, newEventZone, ignoreWarning) {
	
		DataDwr.quickCreateEvent(aId, newArtistName, tId, newTourName, newTourType, newTourGrandChildType, vId, newVenueBuilding, newVenueCity, newVenueState, newVenueCountry, newEventId, newEventName, newEventDate, newEventTime, newEventZone, ignoreWarning, function(response) {
			var tokens = response.split("|"); 
			var code = tokens[0];
			var message = null;
			if (tokens.length >= 2) {
				message = tokens[1];
			}
			if (code == 'OK') {
				alert("The event has been created");
				return;
			} else if (code == "ERR") {		
		      alert("ERROR: " + message);
		      return;
		    } else if (code == "WARN") {
		    	var r = confirm("Warning: " + message + "\n\nDo you still want to create the event?");
				if (!r) {
					return;
				}
				createEvent(aId, newArtistName, tId, newTourName, newTourType, newTourGrandChildType, vId, newVenueBuilding, newVenueCity, newVenueState, newVenueCountry, newEventId, newEventName, newEventDate, newEventTime, newEventZone, true);
				return;				
		    } else {
		    	alert("Unknown response:" + response);
		    }
		});
	};

  function makeEvent() {
    var aId = parseInt($('#artist').val());
    if (isNaN(aId)) {aId = 0}
    var newArtistName = $.trim($('#artistName').val());
    
    var tId = parseInt($('#tour').val());
    if (isNaN(tId)) {tId = 0}
    var newTourName = $.trim($('#tourName').val());	
    var newTourType = $('#tourType').val();
	var newTourParentType = $('#parentSelect').val();
	var newTourChildType = $('#childSelect').val();
	var newTourGrandChildType = $('#grandChildSelect').val();
    
    var vId = parseInt($('#venue').val());
    if (isNaN(vId)) {vId = 0}
    var newVenueBuilding = $.trim($('#venueBuilding').val());
    var newVenueState = $.trim($('#venueState').val());
    var newVenueCity = $.trim($('#venueCity').val());
    var newVenueCountry = $('#venueCountry').val();
    
    var newEventId = parseInt($.trim($('#eventId').val()));
    if (isNaN(newEventId)) {newEventId = 0}
    var newEventName = $.trim($('#eventName').val());
	var newEventDate = null;
	var newEventTime = null;
	var newEventZone = null;	
	if($('input[name=dateTbd]:checked').val() == 'false'){
    newEventDate = $.trim($('#month').val()+'/'+$('#date').val()+'/'+$('#year').val());
	}
	if($('input[name=timeTbd]:checked').val() == 'false'){
    newEventTime = $.trim($('#hour').val()+':'+$('#minute').val());
    newEventZone = $('#eventZone').val();
	}

	if (aId == 0) {
		if (newArtistName.length == 0) {
		  alert('Please select an artist, or enter a new artist name');
		  return;
		}
	}
	
	if (tId == 0) {
		if (newTourName.length == 0) {
		  alert('Please select a tour, or enter a new tour name');
		  return;
		}
		if (newTourType.length == 0) {
		  alert('Please select a Tour Type');
		  return;
		}
		if (newTourParentType.length == 0) {
		  alert('Please select a Tour Parent Category');
		  return;
		}
		if (newTourChildType.length == 0) {
		  alert('Please select a Child Category');
		  return;
		}
		if (newTourGrandChildType.length == 0) {
		  alert('Please select a Grand Child Category');
		  return;
		}
	}
		
	if (vId == 0) {
		if (newVenueBuilding.length == 0) {
		  alert('Please select a venue, or enter a new venue building');
		  return;
		}
	}
	createEvent(aId, newArtistName, tId, newTourName, newTourType,newTourGrandChildType, vId, newVenueBuilding, newVenueCity, newVenueState, newVenueCountry, newEventId, newEventName, newEventDate, newEventTime, newEventZone, false);

  };
      
</script>