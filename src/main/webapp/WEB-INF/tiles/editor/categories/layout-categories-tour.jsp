 <%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedTourSubMenu" name="selectedTourSubMenu" scope="request" />

<div id="editorSubMenu">
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'SearchTour'}">
        <b>Search Tour</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageTours">Search Tour</a>
   </c:otherwise>
 </c:choose>
 |
  <c:choose>
   <c:when test="${selectedTourSubMenu == 'AddTour'}">
        <b>Add Tour</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditTour?action=create">Add Tour</a>
   </c:otherwise>
 </c:choose>
 |
  <c:choose>
   <c:when test="${selectedTourSubMenu == 'MergeTour'}">
        <b>Merge Tour</b>
   </c:when>
   <c:otherwise>
        <a href="EditorMergeTours">Merge Tour</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'Quick Create Event'}">
        <b>Create Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditEvent?action=create">Create Event</a>
   </c:otherwise>
 </c:choose> 
 |
 <c:choose>
   <c:when test="${selectedTourSubMenu == 'Audit Event'}">
        <b>Audit Event</b>
   </c:when>
   <c:otherwise>
        <a href="EditorAuditEvent">Audit Event</a>
   </c:otherwise>
 </c:choose> 
 </div>
 <tiles:insertAttribute name="subBody1" /> 