<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSynonymSubMenu" name="selectedSynonymSubMenu" scope="request" />

<div id="editorSubMenu">



 <c:choose>
   <c:when test="${selectedSynonymSubMenu == 'Theater Synonyms'}">
        <b>Theater Synonyms</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageSynonyms">Theater Synonyms</a>
   </c:otherwise>
 </c:choose>
    |
    <c:choose>
   <c:when test="${selectedSynonymSubMenu == 'Concert Synonyms'}">
        <b>Concert Synonyms</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageConcertSynonyms">Concert Synonyms</a>
   </c:otherwise>
 </c:choose>
 |
    <c:choose>
   <c:when test="${selectedSynonymSubMenu == 'Add Venue Synonyms'}">
        <b>Add Venue Synonyms</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditVenueSynonym">Add Venue Synonyms</a>
   </c:otherwise>
 </c:choose>
 |
    <c:choose>
   <c:when test="${selectedSynonymSubMenu == 'Edit Venue Synonyms'}">
        <b>Edit Venue Synonyms</b>
   </c:when>
   <c:otherwise>
        <a href="EditorEditManageVenueSynonym">Edit Venue Synonyms</a>
   </c:otherwise>
 </c:choose>
 </div>
 <tiles:insertAttribute name="subBody1" /> 