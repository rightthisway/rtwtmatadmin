<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; Manage Zones 
</div>

<h1>Zones</h1>

<form method="GET" action="EditorCategories">
  <b>Artist:</b>
  <select name="artistId" onchange="location.href='EditorCategories?artistId=' + $(this).val()">
    <option value="">ALL</option>
    <c:forEach var="artist" items="${artists}">
      <option value="${artist.id}" <c:if test="${artist.id == param.artistId}">selected</c:if>>${artist.name}</option>
    </c:forEach>
  </select>
</form>

<form method="GET" action="EditorCategories">
  <b>Filter:</b>
  <input type="text" name="filter" value="${param.filter}"/>
  <input class="medButton" type="submit" name="submit" value="Filter"/>
</form>

Choose the tour/season you want to categorize.

<display:table class="list" name="${tours}" id="tour" requestURI="EditorCategories" defaultsort="4" pagesize="20">

    <display:column value="View"   href="EditorEditCategories" paramId="tourId" paramProperty="id" class="bold" />
    <display:column title="Dates" sortable="true">
      <span style="white-space:nowrap">
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.startDate}"/>
		  -
		  <fmt:formatDate pattern="MM/dd/yyyy" value="${tour.endDate}"/>
	  </span>
    </display:column>
    <display:column title="Artist/Team"  sortable="true" property="artist.name" />
    <display:column title="Location" sortable="true" property="location" />
    <display:column title="Tour/Season Name"  sortable="true" sortProperty="name">
      <img src="../images/ico-${fn:toLowerCase(tour.tourType)}.gif" align="absbottom" title="${tour.tourType}"/> ${tour.name}
    </display:column>
    <display:column title="Event Count" sortable="true" sortProperty="eventCount" class="text-align-center">${tour.eventCount}</display:column>  
    <display:column title="Zone Count" sortable="true" sortProperty="categoryCount" class="text-align-center" >${tour.categoryCount}</display:column>
      
</display:table>