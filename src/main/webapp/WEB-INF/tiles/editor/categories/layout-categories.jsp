<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedSubMenu" name="selectedSubMenu" scope="request" />

<div id="editorSubMenu">
	<c:choose>
   <c:when test="${selectedSubMenu == 'Manage Categories'}">
        <b>Manage Zones</b>
   </c:when>
   <c:otherwise>
   		<a href="EditorLoadCategoryMapping">Manage Zones</a>
        <!-- <a href="EditorLoadCategoryMapping">Manage Zones</a> -->
   </c:otherwise>
 </c:choose> 
 |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Csv Audit'}">
        <b>Zones Audit</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageCsvAudit">Zones Audit</a>
   </c:otherwise>
 </c:choose>
  |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Venue Map Status'}">
        <b>Venue Map Status</b>
   </c:when>
   <c:otherwise>
        <a href="EditorVenueMapStatusUpdater">Venue Map Status</a>
   </c:otherwise>
 </c:choose> 
 |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Display'}">
        <b>Parent,Child,Grand Child</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageTourCategory">Parent,Child,Grandchild</a>
   </c:otherwise>
 </c:choose> 
 |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Artist'}">
        <b>Artist/Events</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageArtists">Artist/Events</a>
   </c:otherwise>
 </c:choose>
 <%-- |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Tour'}">
        <b>Tour / Events</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageTours">Tour/Events</a>
   </c:otherwise>
 </c:choose> --%>
 |
<c:choose>
   <c:when test="${selectedSubMenu == 'Venues'}">
        <b>Venues</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageVenues">Venues</a>
   </c:otherwise>
 </c:choose>
 |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Synonyms'}">
        <b>Synonyms</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageSynonyms">Synonyms</a>
   </c:otherwise>
 </c:choose>
 <%-- |
 <c:choose>
   <c:when test="${selectedSubMenu == 'Valuation'}">
        <b>Valuation</b>
   </c:when>
   <c:otherwise>
        <a href="EditorValuationFactors">Valuation</a>
   </c:otherwise>
 </c:choose> --%> 
</div>
<tiles:insertAttribute name="subBody" />