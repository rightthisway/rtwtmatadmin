<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorCategories">Manage Zones</a> 
  &gt; <a href="EditorEditCategories?tourId=${event.artist.id}">${event.artist.name}</a> 
  &gt; <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
</div>

<div id="quickLinks">
 	<a href="BrowseTickets?eventId=${event.id}">Go to Browse Tickets</a>
	| <a href="TMATBrowseShorts?eventId=${tour.id}">Go to Shorts</a>
 	| <a href="Editor?tourId=${tour.id}">Edit Tour</a>
</div>
<div style="clear:both"></div>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>
<!-- 
 Category Group:  
<select id="catScheme">
  <c:forEach var="catg" items="${catGroups}">
    <option value="${catg}" <c:if test="${catGroupName == catg}">selected</c:if>>${catg}</option>
  </c:forEach>
</select>
  -->
  <input id="changeGroupButton" type="button" class="medButton" onclick="changeGroup()"  value="Change"/><br/><br/>

<c:choose>
<c:when test="${not empty categories}">
<b>Zone:</b>
<select id="category" onchange="refreshCategoryMappings()">
   <c:forEach var="category" items="${categories}">
     <option value="${category.id}" <c:if test="${param.cat == category.symbol}">selected</c:if>>${category.symbol} - ${category.description} - ${category.groupName}</option>
   </c:forEach>
</select>

<h2>Rules</h2>

<table id="rulesTable" class="list">
  <thead>
    <th>Id</th>
    <th>Start Section</th>
    <th>End Section</th>
    <th>Start Row</th>
    <th>End Row</th>
    <th>Start Seat</th>
    <th>End Seat</th>
    <th></th>
  </thead>
  <tbody id="categoryMappingTableBody">
  </tbody>
</table>
<div id="noRulesDiv">(no rules defined)</div>
<br/><br/>
<img src="../images/ico-plus.gif" align="absbottom" /><a id="addRuleLink" href="#" onclick="showAddRuleDiv(); return false">Add Rule</a><br/>
<br/>
<a href="#" onclick="removeEventCategory(); return false"><img src="../images/ico-delete.gif" align="absbottom" />Remove all rules</a><br/> 

<div id="addOrEditRuleDiv" style="display:none">
<h1 id="addOrEditRuleTitle">Add Rule</h1>
<a name="AddRuleSection"></a>
<table>
  <tr id="mappingRow">
    <td>Mapping Id:</td>
    <td id="mappingValue"></td>
  </tr>
  <tr>
    <td>Zone:</td>
    <td id="categoryValue">
    </td>  
  </tr>
  <tr>
    <td>Start Section:</td>
    <td><input id="startSection" type="text"/></td>
  </tr>
  <tr>
    <td>End Section:</td>
    <td><input id="endSection" type="text"/> (optional)</td>
  </tr>
  <tr>
    <td>Start Row:</td>
    <td><input id="startRow" type="text"/></td>  
  </tr>
  <tr>
    <td>End Row:</td>
    <td><input id="endRow" type="text"/> (optional)</td>  
  </tr>
    <tr>
    <td>Start Seat:</td>
    <td><input id="startSeat" type="text"/></td>  
  </tr>
  <tr>
    <td>End Seat:</td>
    <td><input id="endSeat" type="text"/> (optional)</td>  
  </tr>
  
</table>
<br/>
<input id="addOrEditRuleButton" type="button" class="medButton" onclick="addCategoryMapping()"  value="Add"/>
<input type="button" class="medButton" onclick="hideAddOrEditRuleDiv()" value="Cancel"/>
</div>

<h2>Test</h2>
<table>
  <tr>
    <td>Section</td>
    <td><input type="text" id="section"/></td>
  </tr>
  <tr>
    <td>Row</td>
    <td><input type="text" id="row"/></td>
  </tr>
   <tr>
    <td>Seat</td>
    <td><input type="text" id="seat"/></td>
  </tr>
  
</table>

<input type="button" class="medButton" value="Get Zone" onclick="testCategory()"/>

<h2>Upload Zones for Event</h2>
<div class="info">
	<div class="infoText">
		You can easily create mapping by importing a CSV file. The format is as follows:<br/>
		venue, sections, rows, seats, categorySymbol, categoryGroup, categorySeatQuantity(optional - can be empty), equivalentCats
		<br/><br/>
		Examples:<br/>
		Rogers Centre, 12, 1, *, A, DEFAULT,, A<br/>
		Rogers Centre, 12-14, *, *, A, DEFAULT, 20, A<br/>
		Rogers Centre, 12-14, 1-5, *, A, DEFAULT, 15, A<br/>
		Rogers Centre, LOGE, *, B, *, DEFAULT,, A B,<br/>
		Rogers Centre, S1-S19, *, *, B, DEFAULT, 80, A B,<br/>
		<br/>
		<b>Note 1:</b> that if you CSV contains an undefined zone, it will be created automatically for you.<br/>
		<b>Note 2:</b> if you need to use the character - (hyphen) for a section, row, or seat please use %MINUS%, for instance:<br/>
		Rogers Centre, CRT%MINUS%56, *, 1, DEFAULT, 20, A<br/>
		Rogers Centre, CRT%MINUS%56-CRT%MINUS%87, 1%MINUS%5-1%MINUS%9, 1, DEFAULT, 50, A<br/>
		<b>Note 3:</b> The categorization will be run against the normalized section/row (the gray part is ignored).<br/>
		<b>Note 4:</b> You can also define rule such as "contain the string XX or YY", then you can do as follows:<br/>
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		<b>Note 5:</b> To make the categorization works on multiple events which share the same venue, one can do as follows:<br/>		
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		In that case all the events occuring between 02/21/2009 and 03/14/2009 will be categorized with the rule. The field time will be ignored.
	</div>
</div>
<i>Upload for the event disabled until further notice</i>
<!--
<form enctype="multipart/form-data" action="EditorUploadCategoryMappingCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <input type="hidden" name="eventId" value="${event.id}" />
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" class="medButton "value="Upload"/>
</form>
-->
(This will remove all the zone definitions and rules)

<h2>Misc</h2>
<a href="#" onclick="location.href='BrowseTickets?eventId=${event.id}&cat=' + $('#category').val(); return false">Browse tickets</a>

<script type="text/javascript">

function changeGroup() {
    var catGroup = $.trim($('#catScheme').val());;
    document.location.href = "EditorEditEventCategories?eventId=" + ${eventId} + "&catScheme=" + catGroup;
}


  var categoryMappings;
  var categories = new Array();
  <c:forEach var="category" items="${categories}">
    categories[${category.id}]='${category.symbol}';
  </c:forEach>

  refreshCategoryMappings();

  function showAddRuleDiv() {
    $('#mappingRow').hide();
    $('#addOrEditRuleTitle').html('Add Mapping');
    $('#addOrEditRuleButton').val("Add");
    showAddOrEditRuleDiv();
  }

  function showEditRuleDiv(mapId) {
    for (var i = 0; i < categoryMappings.length; i++) {
      if (categoryMappings[i].id == mapId) {
        $('#startSection').val(categoryMappings[i].startSection);
        $('#endSection').val(categoryMappings[i].endSection);
        $('#startRow').val(categoryMappings[i].startRow);
        $('#endRow').val(categoryMappings[i].endRow);
        $('#startSeat').val(categoryMappings[i].startSeat);
        $('#endSeat').val(categoryMappings[i].endSeat);
        break;
      }
    } 
    $('#mappingRow').show();
    $('#mappingValue').html(mapId);
    $('#addOrEditRuleTitle').html('Edit Mapping');
    $('#addOrEditRuleButton').val("Update");
    showAddOrEditRuleDiv();
  }

  function showAddOrEditRuleDiv() {
    $('#categoryValue').html(categories[$('#category').val()]);
    $('#addOrEditRuleDiv').show();
    $('#addRuleLink').hide();
  }
  
  function hideAddOrEditRuleDiv() {
    $('#addOrEditRuleDiv').hide();
    $('#addRuleLink').show();
  }

  function refreshCategoryMappings() {
    CategoryMappingDwr.getAllCategoryMappingsFromEventAndCat(${event.id}, $('#category').val(), function(mappings) {
      if (mappings.length == 0) {
        $('#rulesTable').hide();
        $('#noRulesDiv').show();
        return;
      }
      
      $('#rulesTable').show();
      $('#noRulesDiv').hide();      
      categoryMappings = mappings;
      $('#categoryMappingTableBody').children().remove();
      
      $.each(mappings, function(intIndex, mapping) {
        var editLink = $.A({href:'#AddRuleSection'}, 'Edit');
        $(editLink).click(function() {
		  showEditRuleDiv(this.parentNode.parentNode.id.substring(4));
        });
        
        var removeLink = $.A({href:'#'}, 'Remove');
        $(removeLink).click(function() {
          var mapId = this.parentNode.parentNode.id.substring(4);
          var answer = confirm("Are you sure you want to remove the mapping " + mapId);
          if (answer) {
            CategoryMappingDwr.removeCategoryMapping(mapId);
            refreshCategoryMappings();
          }
        });

        var tr = $.TR({id:'map_' + mapping.id},
                   $.TD({}, mapping.id),
                   $.TD({}, mapping.startSection),
                   $.TD({}, mapping.endSection),
                   $.TD({}, mapping.startRow),
                   $.TD({}, mapping.endRow),
                   $.TD({}, mapping.startSeat),
                   $.TD({}, mapping.endSeat),
                   $.TD({}, $.IMG({'src': '../images/ico-edit.gif', 'align': 'absbottom'}), editLink, " ", $.IMG({'src': '../images/ico-delete.gif', 'align': 'absbottom'}), removeLink)
                 );
        $('#categoryMappingTableBody').append(tr);
      });
    });
  }

  function addCategoryMapping() {
    var startSection = $.trim($('#startSection').val());
    var endSection = $.trim($('#endSection').val());
    var startRow = $.trim($('#startRow').val());
    var endRow = $.trim($('#endRow').val());
    var startSeat = $.trim($('#startSeat').val());
    var endSeat = $.trim($('#endSeat').val());
    var category = $('#category').val();

	if (startSection.length == 0) {
	  alert('Please enter a start section');
	}
	
	if (startRow.length == 0) {
	  startRow = '*';
	}

	if (startSeat.length == 0) {
		startSeat = '*';
	}
	
    if (endSection.length == 0) {
      endSection = startSection;
    } else if (startSection.length != endSection.length) {
//      alert("Start Section and End Section must be of same length (e.g., 1 and 6, S1 and S6, SA and SB) and the end section must be greater than the start section");
//      return;
    }

    if (endRow.length == 0) {
      endRow = startRow;
    } else if (startRow.length != endRow.length) {
//      alert("Start Row and End Row must be of same length (e.g., 1 and 6, R1 and R6, RA and RB) and the end Row must be greater than the start Row");
//      return;
    }
    if (endSeat.length == 0) {
    	endSeat = startSeat;
      }

    if ($('#addOrEditRuleButton').val() == 'Add') {
	    CategoryMappingDwr.addCategoryMapping(${event.id}, category, startSection, endSection,
	                                          startRow, endRow,startSeat,endSeat, function(response) {
	                                            if (response != 'OK') {
	                                              alert(response);
	                                              return;
	                                            }
	                                            
	                                            refreshCategoryMappings();
	                                            hideAddOrEditRuleDiv();
	                                          });
     } else {
	    CategoryMappingDwr.updateCategoryMapping($('#mappingValue').text(), ${event.id}, category, startSection, endSection,
	                                          startRow, endRow,startSeat,endSeat, function(response) {
	                                            if (response != 'OK') {
	                                              alert(response);
	                                              return;
	                                            }
	                                            
	                                            refreshCategoryMappings();
	                                            hideAddOrEditRuleDiv();
	                                          });
     }
  }
  
  function testCategory() {
    CategoryDwr.getCategory(${event.id}, $('#section').val(), $('#row').val(), $('#seat').val(), '${catGroupName}',
    						function(response) {
    						    if (response == null) {
    						    	alert("no zone found!");
    						    } else {
    								alert(response);
    							}
    						});
  }
  

  function removeEventCategory()  {
    var answer = confirm('Are you sure you want to remove all the zone mappings?');
    if (answer) {
      CategoryMappingDwr.removeEventCategoryMapping(${event.id}, function(response) {
    	  if (response == 'OK') {
    		  $('#mainInfoText').html("Event Zone Mappings removed!");
    		  $('#info').show();
			  refreshCategoryMappings();
    	  }
      });
    }
 }
  
</script>
</c:when>
<c:otherwise>
<div class="error">
  <div class="errorText">
    No zones have been defined.<br/>
    Please go back to the <a href="EditorEditCategories?tourId=${event.artist.id}">previous page</a> to add zones first.
  </div>
</div>

</c:otherwise>
</c:choose>