<%@include file="/WEB-INF/tiles/taglibs.jsp"%>
<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorTCAP">Manage Data</a> 
  &gt; Tour Category
</div>
<c:choose>
<c:when test="${not empty info}">
<div class="info">
		<div class="infoText">
			${info}
		</div>
</div>
</c:when>
</c:choose>
<form:form id="tourCategory" commandName="tourCategory" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Tour Category Name:</td>
			<td>
				<form:input path="name" cssClass="longInput" id="name"/>		
			</td>
		</tr>
		
	</table>
	<input type="submit" value="Add" name="submit" id="submitBtn" class="medButton">
	
</form:form>

<script>
	$('#submitBtn').click(function(){
		if($('#name').val() == ""){
			alert("Please Enter Tour Category Name");
			return false;
		}else{
			return true;
		}
	});
</script>