<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorVenueMapStatusUpdater">Venue Map Status</a> 
  &gt; ${tour.name}
</div>

<div style="clear:both"></div>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>
<c:if test="${not empty info}">
	<div id="info" class="info" >
		<div id="mainInfoText" class="infoText">
			${info}
		</div>
	</div>
</c:if>

<form:form action="EditorVenueMapStatusUpdater" method="POST" id="formId">
	<input type="hidden" name="action" id="action" >
<table align="center">
	<tr>
	<td ><b>Process Type${processType}</b></td>
	<td >
			<input type="radio" id="processType1" name="processType" value="Venue" onclick="processTypeChange();" <c:if test="${processType eq null or processType eq 'Venue'}">checked</c:if>/> Venue
			<input type="radio" id="processType2" name="processType" value="All" onclick="processTypeChange();" <c:if test="${processType eq 'All'}">checked</c:if> /> All
			
		</td>
   </tr>
</table>
<br />
<br />
<table align="center" id="processAllTableId" border="1" <c:if test="${processType eq null or processType eq 'Venue'}">style="display: none;"</c:if> >
	<tr>
		<td style="width:160px;">
			<b>Last Processed Time</b>
		</td>
		<td style="width:160px;">
			${lastProcessedTime}
		</td>
	</tr>
	<tr>
		<td>
			<b>Next Process Time</b>
		</td>
		<td >
			${nextProcessTime}
		</td>
	</tr>
	<tr>
		<td>
			<b>Running Now</b>
		</td>
		<td >
			<c:choose>
				<c:when test="${isRunningNow}">Yes</c:when>
				<c:otherwise>No</c:otherwise>
			</c:choose>
			
		</td>
	</tr>
	<tr>
	   <td colspan="2"><input id="changeGroupButton" type="button" style="width:160px;margin-left:70px;" class="medButton" onclick="submitForms('update');"  value="Process All Venues"/>
	  	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td>
   </tr>
</table>
			    
	<table align="center" id="processVenueTableId" <c:if test="${processType eq 'All'}">style="display: none;"</c:if> >
	<tr>
	<td>
		<b>Venue:</b>
	</td>
	<td>
		<input type="hidden" name="venueId" id="venueId" value="${venueId}">
		
		<span id="venueLabelSpanId"  <c:if test="${not empty venueId }">style="display: black;"</c:if> 
		
		<c:if test="${empty venueId }">style="display: none;"</c:if>  >
		<span id="venueNameDiv" >
			<c:choose>
				<c:when test="${not empty venueId }">
					${venueName}
				</c:when>
				<c:otherwise>
						-
				</c:otherwise>
			
			</c:choose>
			</span>
			
			<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
		</span>
		
		
		<span id="venueSelectSpanId" <c:if test="${not empty venueId }">style="display: none;"</c:if> 
		<c:if test="${empty venueId }">style="display: black;"</c:if> >
		<input type="text" name ="autoVenue" id ="autoVenue" >
		</span>
	</td>
	</tr>
	<%-- <tr>
		<td> <b>Zone Group:</b></td>
	  
		<td><select id="catScheme" name="catScheme">
			<option value="0">--All--</option>
		  <c:forEach var="venueCat" items="${catGroups}">
		    <option value="${venueCat.categoryGroup}" <c:if test ="${catGroupName==venueCat.categoryGroup}">selected</c:if>>${venueCat.categoryGroup}</option>
		  </c:forEach>
		</select> 
		</td>
	</tr> --%>
	<!-- <tr>
		<td></td>
	   <td><input id="changeGroupButton" type="button" class="medButton" onclick="auditGroup()"  value="Audit"/>
	   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td>
	</tr> -->
	</table>
	<br />
	<br />
	
	<div id="categoryGroupGridId" <c:if test="${empty venueId }">style="display: none;"</c:if> >
	<c:if test="${not empty venueCategoryList}" >
	<div id="ebayInventoryTabel" align="center">
	
		
		<!-- <div style="float:right; padding-right:100px; margin-right:120px">
			<input type="button" value="Update"  name="update1" id="update1" class="medButton" onclick="javascript:submitForms('update');"/> 
		</div> -->
		<div id="copyCheckBox" style="float:right;">
			<!--<input onclick="javascript:copyAllEventData();" type="checkbox" name="copyAllCheckbox" id="copyAllCheckbox" >Copy first row  to all remaining rows &nbsp;&nbsp;&nbsp; -->
		</div>
		<br/>
		<br/>
		<br/>
		<display:table list="${venueCategoryList}"  defaultsort="3" decorator="org.displaytag.decorator.TotalTableDecorator"   id="venueCategory"
		  requestURI="#" class="list" >
			<display:column title ='<br/><input type="checkbox" name="copyAll" id="copyAll">' >
				<input type="checkbox" name="checkbox_${venueCategory.venue.id}_${venueCategory.categoryGroup}"" id="checkbox_${venueCategory.venue.id}_${venueCategory.categoryGroup}" class="selectCheck" />
			</display:column>
			<display:column title="Category Group" sortable="true">${venueCategory.categoryGroup}</display:column>
			<display:column title="Venue Map">
				<c:choose> 
					<c:when test="${venueCategory.isVenuMap}">		
						Yes
					</c:when>
					<c:otherwise>
				    		<font color="red">No</font>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="RTF Map">
				<c:choose> 
					<c:when test="${venueCategory.isVenuMap}">		
						<a href="javascript:loadImage(${venueCategory.venue.id},'${venueCategory.categoryGroup}',${venueCategory.id});">
						<img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" />
						</a>
					</c:when>
					<c:otherwise>
				    		<font color="red">No Map</font>
					</c:otherwise>
				</c:choose>
			</display:column>
			<display:column title="is TMAT Map">
				<c:choose> 
					<c:when test="${venueCategory.mapExistInTmat}">		
						<a href="javascript:loadTMATImage(${venueCategory.venue.id},'${venueCategory.categoryGroup}');">
						<!-- <img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" /> -->
						Yes
						</a>
					</c:when>
					<c:otherwise>
				    		<a href="javascript:loadTMATImage(${venueCategory.venue.id},'${venueCategory.categoryGroup}');">
						<!-- <img src="../images/000_THUMB_AO.gif" width="20" height="20" style="float:left; margin: 0px 5px 5px 5px;" /> -->
						<font color="red">No</font>
						</a>
					</c:otherwise>
				</c:choose>
			</display:column>
			<%-- <display:column title="Action" >
			 	<input type="button" value="Audit" class="medButton" id="audit_${exchangeEvent.event.id}}"
				onClick="popupUpdateTGCatsPricingAudit('${exchangeEvent.event.id}');"/>
			 	
			 </display:column> --%>
		</display:table>
			
			<br/>
			<div style="float:right; padding-right:100px; margin-right:120px">
				<input type="button" value="Update"    class="medButton" id="update2" name="update2" onclick="javascript:submitForms('update');"/> 
			</div>
			</div>
			
	</c:if>
	<c:if test="${empty venueCategoryList and  not empty venueId}" >
	<div align='center'>
		 <h4><span  id="emptyMsg">There isn't any Category group for this Selection.</span></h4> 
	</div>
	</c:if>
	</div>
	
</form:form>   
   

<script type="text/javascript">

function processTypeChange() {
	if ($("#processType1").is(":checked")) {
		$('#processAllTableId').hide();
		$('#processVenueTableId').show();
		$('#categoryGroupGridId').show();
		
	} else if($("#processType2").is(":checked")) {
		$('#processAllTableId').show();
		$('#processVenueTableId').hide();
		$('#categoryGroupGridId').hide();
	}
}
	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#venueId').val('');
	}
	
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
	width: 650,
	max: 1000,
	minChars: 2,		
	formatItem: function(row, i, max) {
		return row[1];
	}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		location.href='EditorVenueMapStatusUpdater?venueId=' + row[0]+'&processType=Venue';
	});

	function validateForm(){
		var flag= true;
		var minRecordFlag = false;
		$('.selectCheck:checkbox:checked').each(function () {
			minRecordFlag = true;
			
			/* var id,value;
			id = this.id.replace('checkbox','exposure');
			value = $.trim($("#"+id).val());
			if(value==''){
				alert('Exposure can not be blank for Events.');
				$("#"+id).focus();
				flag = false;
				return ;
			} */
			
		});
		if(!minRecordFlag) {
			alert("Select minimum one category group to update.");
			flag = false;
		}
		if(!flag){
			return flag;	
		}
		
		return true;
}
function submitForms(action){
	
	if ($("#processType2").is(":checked")) {//All
		$('#venueId').val('');
		$("#action").val(action);
		$("#formId").submit();
	
	} else if($("#processType1").is(":checked")) {//Venue
		if(action=='update'){
			if(validateForm()){
				$("#action").val(action);
				$("#formId").submit();
			}
		}
	}

}

$(function () {
	
	$('#copyAll').click(function(){
		if($('#copyAll').attr('checked')){
			$('.selectCheck').attr('checked', true);
		}else{
			 $('.selectCheck').attr('checked', false); 
		}
	});
	
	
});

	

function loadImage(venueId,catScheme,venueCatId) {
	window.open('VenueImage?venueId=' + venueId + '&catScheme=' + catScheme+'&venueCatId='+venueCatId,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};


function loadTMATImage(venueId,catScheme) {
	window.open('TMATVenueImage?venueId=' + venueId + '&catScheme=' + catScheme,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
};

</script>
