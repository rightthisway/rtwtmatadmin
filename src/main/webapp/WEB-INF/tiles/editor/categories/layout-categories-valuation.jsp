 <%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedValuationSubMenu" name="selectedValuationSubMenu" scope="request" />

<div id="editorSubMenu">
  
 <c:choose>
   <c:when test="${selectedValuationSubMenu == 'Valuation Factors'}">
        <b>Valuation Factors</b>
   </c:when>
   <c:otherwise>
        <a href="EditorValuationFactors">Valuation Factors</a>
   </c:otherwise>
 </c:choose>
 </div>
 <tiles:insertAttribute name="subBody1" />