<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorEditCategories">CSV Audit</a> 
  &gt; ${tour.name}
</div>

<div style="clear:both"></div>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>
<c:if test="${not empty info}">
	<div id="info" class="info" >
		<div id="mainInfoText" class="infoText">
			${info}
		</div>
	</div>
</c:if>

<table>
<tr>
<td>
	<b>Venue:</b>
</td>
<td>
			
			<%-- <input type="hidden" name="venueId" id="venueId" value="${event.venueId}"> --%>
			
			<span id="venueLabelSpanId"  <c:if test="${not empty venueId }">style="display: black;"</c:if> 
			
			<c:if test="${empty venueId }">style="display: none;"</c:if>  >
			<span id="venueNameDiv" >
				<c:choose>
					<c:when test="${not empty venueId }">
						${venueName}
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				
				</c:choose>
				</span>
				
				<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
			</span>
			
			
			<span id="venueSelectSpanId" <c:if test="${not empty venueId }">style="display: none;"</c:if> 
			<c:if test="${empty venueId }">style="display: black;"</c:if> >
			
				<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
				<c:forEach var="venue" items="${venues}">
					<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
				</c:forEach>
			</form:select> --%>
			
			<input type="text" name ="autoVenue" id ="autoVenue" >
			</span>
</td>
</tr>
<br/><br/>
<tr>
<td> <b>Zone Group:</b></td>
  
<td><select id="catScheme" name="catScheme">
  <c:forEach var="venueCat" items="${catGroups}">
    <option value="${venueCat.categoryGroup}" <c:if test ="${catGroupName==venueCat.categoryGroup}">selected</c:if>>${venueCat.categoryGroup}</option>
  </c:forEach>
</select> </td>
</tr>
<tr>
<td></td>
   <td><input id="changeGroupButton" type="button" class="medButton" onclick="auditGroup()"  value="Audit"/>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
   </tr>
   </table>
   <c:if test="${fromTcap != null }">
   <a href = "${fromTcap}">Return To TCAP</a></c:if>
   <br/><br/>
   <!-- <div id="removeCategoryAliasDiv" style="display:none">
     <b id="removeCategoryAliasTitle">Remove Zone Alias</b><br/> 
    <table id="categoryAliasTable" class="list">
	  <thead>
	    <th>Zone Symbol</th>
	    <th>Start Alias</th>
	    <th>End Alias</th>
	    <th></th>
	  </thead>
		  <tbody id="aliasesBody">
		  </tbody>
	</table>
   </div> -->
<!-- </div>

</td>
</tr>
</table> -->
<div>
<form:form action="EditorEditCategories" method="POST">


	
	<input type="hidden" name="tourId" value="${tour.id}" />
	<input type="hidden" name="zoneEventCategory" value="true" />
	
</form:form>


	<form enctype="multipart/form-data" action="EditorUploadCategoryMappingCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <input type="hidden" name="tourId" value="${tour.id}" />
    <input type="hidden" name="catScheme" value="${catGroupName}"/>
	</form>
</div>
<div align="center">
<c:choose>
	  <c:when test="${empty place && not empty catGroups }">
	   <table  class="list">
<tr>
  <th>User Name</th>
  <th>Action</th>		
  <th>Action Date</th>
  <th>Initial Line Count</th>
  <th>Final Line Count</th>
</tr>
<tr>
  <td>Admin</td>
  <td>Updated</td>		
  <td>06/01/2014</td>
  <td>-</td>
  <td>-</td>
</tr>

</table>
	  </c:when>
	  <c:otherwise>
</div>
<div align="center">
	<display:table class="list" name="${place}" id="userAction" requestURI="AdminUserAudit">
		 <display:column title="User Name" property="username" sortable="true"/>
		 <display:column title="Action" property="action" sortable="true"/>
		 <display:column title="Action Date" sortable="true" >
			<fmt:formatDate pattern="MMM dd yyyy hh:mm aa" value="${userAction.createdDate}" />
		 </display:column>
		 <display:column title="Intial Line Count" property="initialLineCount" sortable="true"/>
		 <display:column title="Final Line Count" property="linecount" sortable="true"/>
	</display:table>	
</div>
</c:otherwise>
</c:choose>

<script type="text/javascript">

	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#tempVenueId').val('');
	}
	
	$('#autoVenue').autocomplete("AutoCompleteVenue", {
	width: 650,
	max: 1000,
	minChars: 2,		
	formatItem: function(row, i, max) {
		return row[1];
	}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		location.href='EditorManageCsvAudit?venueIdStr=' + row[0];
	});

	function downloadCsv(){
   		 var catGroup = $.trim($('#catScheme').val());
	 	 document.location.href = "EditorDownloadCategoryMappingCsvFile?venueId=${venueId}&categoryGroup="+catGroup;
  	}

</script>
<script type="text/javascript">
 function auditGroup() {
		var catGroup = $.trim($('#catScheme').val());
		 if(catGroup.length<0)
		{
			 alert('Zone Group Empty for this venue');
		}
		else
		{
			document.location.href = "EditorManageCsvAudit?audit=true&venueIdStr=" + ${venueId} + "&catScheme=" + catGroup
		}
  }
  
  function setGroupForZone(){
	  var catGroup = $.trim($('#catSchemeForZone').val());;
	  if(catGroup != '-1'){
      	document.location.href = "EditorEditCategories?tourId=" + ${tourId} + "&setGroupForZone=true&catScheme=" + catGroup;
      }else{
      	alert('Please select a zone');
      }  
  }
</script>