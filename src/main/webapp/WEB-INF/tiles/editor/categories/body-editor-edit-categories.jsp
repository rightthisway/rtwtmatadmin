<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<div id="breadCrumbPath" class="breadCrumbPathEditor">
  <a href="..">Home</a> 
  &gt; <a href="EditorEditCategories">Manage Zones</a> 
  &gt; ${artist.name}
</div>

<div id="quickLinks">
 	<%-- <a href="BrowseEvents?tourId=${tour.id}">Go to Browse Events</a>
	| <a href="TMATBrowseEvents?tourId=${tour.id}">Go to Shorts</a>
 	|  --%>
 	<c:if test="${not empty artist.id and artist.id ne null}">
 	<a href="EditorDetailArtist?artistId=${artist.id}">Edit Artist</a>
 	</c:if>
</div>
<div style="clear:both"></div>

<c:if test="${not empty param.error}">
<div class="error">
<div class="errorText">
${param.error}
</div>
</div>
</c:if>
<c:if test="${not empty info}">
	<div id="info" class="info" >
		<div id="mainInfoText" class="infoText">
			${info}
		</div>
	</div>
</c:if>
<div id="pleaseWaitDiv" style="display:none"><img src="../images/process-running.gif" align="absbottom" /> Processing. Please wait...</div>

<b>Venue:</b>
			
			<%-- <input type="hidden" name="venueId" id="venueId" value="${event.venueId}"> --%>
			
			<span id="venueLabelSpanId"  <c:if test="${not empty venueId }">style="display: black;"</c:if> 
			
			<c:if test="${empty venueId }">style="display: none;"</c:if>  >
			<span id="venueNameDiv" >
				<c:choose>
					<c:when test="${not empty venueId }">
						${venueName}
					</c:when>
					<c:otherwise>
							-
					</c:otherwise>
				
				</c:choose>
				</span>
				
				<input type="button" value="Change Venue" class="medButton" onclick="callChangeVenue();"  />
			</span>
			
			
			<span id="venueSelectSpanId" <c:if test="${not empty venueId }">style="display: none;"</c:if> 
			<c:if test="${empty venueId }">style="display: black;"</c:if> >
			
				<%-- <form:select path="tempVenueId" onchange="getVenueCategory(this.value);">
				<c:forEach var="venue" items="${venues}">
					<form:option value="${venue.id}">${venue.building}, ${venue.city}, ${venue.state}, ${venue.country}</form:option>
				</c:forEach>
			</form:select> --%>
			
			<input type="text" name ="autoVenue" id ="autoVenue" >
			</span>

<br/><br/>

 Zone Group:
  
<select id="catScheme" name="catScheme">
  <c:forEach var="venueCat" items="${catGroups}">
    <option value="${venueCat.categoryGroup}" <c:if test ="${catGroupName==venueCat.categoryGroup}">selected</c:if>>${venueCat.categoryGroup}</option>
  </c:forEach>
</select>
   <input id="changeGroupButton" type="button" class="medButton" onclick="removeGroup()"  value="Remove"/>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <c:if test="${fromTcap != null }">
   <a href = "${fromTcap}">Return To TCAP</a></c:if>
   <br/><br/>
   <!-- <div id="removeCategoryAliasDiv" style="display:none">
     <b id="removeCategoryAliasTitle">Remove Zone Alias</b><br/> 
    <table id="categoryAliasTable" class="list">
	  <thead>
	    <th>Zone Symbol</th>
	    <th>Start Alias</th>
	    <th>End Alias</th>
	    <th></th>
	  </thead>
		  <tbody id="aliasesBody">
		  </tbody>
	</table>
   </div> -->
<!-- </div>

</td>
</tr>
</table> -->
<form:form action="EditorEditCategories" method="POST">


	
	<input type="hidden" name="artistId" value="${artist.id}" />
	<input type="hidden" name="zoneEventCategory" value="true" />
	
</form:form>

<h2>Upload Zones for Artist</h2>
<div class="info">
	<div class="infoText">
		You can easily create mapping by importing a CSV file. The format is as follows:<br/>
		venue, sections, rows, seats, categorySymbol, categoryGroup, categorySeatQuantity(optional - can be empty), equivalentCats
		<br/><br/>
		Examples:<br/>
		Rogers Centre, 12, 1, *, A, DEFAULT,, A<br/>
		Rogers Centre, 12-14, *, *, A, DEFAULT, 20, A<br/>
		Rogers Centre, 12-14, 1-5, *, A, DEFAULT, 15, A<br/>
		Rogers Centre, LOGE, *, B, *, DEFAULT,, A B,<br/>
		Rogers Centre, S1-S19, *, *, B, DEFAULT, 80, A B,<br/>
		<br/>
		<b>Note 1:</b> that if you CSV contains an undefined zone, it will be created automatically for you.<br/>
		<b>Note 2:</b> if you need to use the character - (hyphen) for a section, row, or seat please use %MINUS%, for instance:<br/>
		Rogers Centre, CRT%MINUS%56, *, 1, DEFAULT, 20, A<br/>
		Rogers Centre, CRT%MINUS%56-CRT%MINUS%87, 1%MINUS%5-1%MINUS%9, 1, DEFAULT, 50, A<br/>
		<b>Note 3:</b> The categorization will be run against the normalized section/row (the gray part is ignored).<br/>
		<b>Note 4:</b> You can also define rule such as "contain the string XX or YY", then you can do as follows:<br/>
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		<b>Note 5:</b> To make the categorization works on multiple events which share the same venue, one can do as follows:<br/>		
		Rogers Centre, >XX>YY, *, *, A, DEFAULT,, A<br/>
		In that case all the events occuring between 02/21/2009 and 03/14/2009 will be categorized with the rule. The field time will be ignored.
	</div>
	<form enctype="multipart/form-data" action="EditorUploadCategoryMappingCsvFile" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
    <input type="hidden" name="artistId" value="${artist.id}" />
    <input type="hidden" name="catScheme" value="${catGroupName}"/>
	<img src="../images/ico-upload.gif" align="absmiddle" /> <b>Import csv file:</b> <input name="file" type="file"/> <input type="submit" name="submit" class="medButton "value="Upload"/>
	</form>
	(This will remove all the zone definitions and rules)
</div>
<h2>Events</h2>
<br/>
<div>
	<display:table class="list" name="${events}" id="event" requestURI="EditorEditCategories">

    <display:column value="View" href="EditorEditEventCategories" paramId="eventId" paramProperty="id"  />
    <display:column title="Name" sortable="true" property="name"/>
    <display:column title="Date" sortable="true">
	    <fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
		<c:choose>
		  <c:when test="${not empty event.time}">
		    <fmt:formatDate pattern="HH:mm" value="${event.time}" />
		  </c:when>
		  <c:otherwise>
		    TBD
		  </c:otherwise>
		</c:choose>
    </display:column>
    <display:column title="Venue" sortable="true" sortProperty="venue.building">
      <img src="../images/ico-${fn:toLowerCase(event.eventType)}.gif" align="absbottom" title="${event.eventType}"/> ${event.venue.building}
    </display:column>
    <display:column title="City" sortable="true" property="venue.city" />
    <display:column title="State" sortable="true" property="venue.state" />
    <display:column title="Country" sortable="true" property="venue.country"  />
   
	</display:table>
	
</div>


<h2>Export Data</h2>
<a href="javascript:downloadCsv();"><img src="../images/ico-download.gif" align="absbottom" /> Download CSV file</a>

<script type="text/javascript">

	function downloadCsv(){
   		 var catGroup = $.trim($('#catScheme').val());
	 	 document.location.href = "EditorDownloadCategoryMappingCsvFile?venueId=${venueId}&categoryGroup="+catGroup;
  	}

</script>
<script type="text/javascript">
  function reloadPage(venueId){
	  if(venueId!=0){
		  location.href='EditorEditCategories?venueIdStr=' + venueId;  
	  }
	  
  }

  function removeGroup() {
	var answer = confirm('All Events assinged to this venue category will have no default venue category..');
	if(answer){
		var catGroup = $.trim($('#catScheme').val());
		document.location.href = "EditorEditCategories?remove=true&venueIdStr=" + ${venueId} + "&catScheme=" + catGroup;
	}
  }
  


  function setGroupForZone(){
	  var catGroup = $.trim($('#catSchemeForZone').val());
	  if(catGroup != '-1'){
      	document.location.href = "EditorEditCategories?artistId=" + ${artistId} + "&setGroupForZone=true&catScheme=" + catGroup;
      }else{
      	alert('Please select a zone');
      }  
  }
  
  	function callChangeVenue(){
		$('#venueLabelSpanId').hide();
		$('#venueSelectSpanId').show();
		$('#tempVenueId').val('');
	}
  
  	$('#autoVenue').autocomplete("AutoCompleteVenue", {
		width: 650,
		max: 1000,
		minChars: 2,		
		formatItem: function(row, i, max) {
			return row[1];
		}
	}).result(function (event,row,formatted){
		$('#autoVenue').val(row[1]);
		location.href='EditorEditCategories?venueIdStr=' + row[0];
	});
  	
  /*
  function removeCategories() {
    var answer = confirm('Are you sure you want to remove all the zones?');
    if (answer) {
      CategoryDwr.removeCategories(${tour.id}, "${catGroupName}", function(response) {
    	  if (response == 'OK') {
    		  $('#mainInfoText').html("Zones deleted!");
    		  $('#info').show();
			  refreshCategories();
    	  }
      });
    }
  }

  function removeCategoryMappings() {
    $('#pleaseWaitDiv').show();
    CategoryMappingDwr.removeAllCategoryMapping(${tour.id}, "${catGroupName}", function(response) {
        $('#pleaseWaitDiv').hide();
    	if (response == 'OK') {
    		$('#mainInfoText').html("Zone Mappings removed!");
    		$('#info').show();
    	}
    });
  }

  function removeCategory(id, symbol) {
    var answer = confirm('Are you sure you want to remove the zone ' + symbol + '?');
    if (answer) {
      CategoryDwr.removeCategory(id);
      refreshCategories();
    }
  }

  function refreshCategories() {
    $('#categoriesBody').children().remove();
    CategoryDwr.getCategories(${tour.id}, "${catGroupName}", function(categories) {
		if (categories.length == 0) {
		  $('#categoryTable').hide();
		  $('#noCategoryDiv').show();
		  return;
		}
		
  	    $('#categoryTable').show();
		$('#noCategoryDiv').hide();
    	$.each(categories, function(intIndex, category) {
    	  var editLink = $.A({href:'#'}, 'Edit');
    	  $(editLink).click(function(event) {
    	    showEditCategory(this.parentNode.parentNode);
    	    event.preventDefault();
    	  });

    	  var removeLink = $.A({href:'#'}, 'Remove');
    	  $(removeLink).click(function(event) {
    	    removeCategory(this.parentNode.parentNode.id.substring(4), this.parentNode.parentNode.name);
    	    event.preventDefault();
    	  });

          var tr = $.TR({id:'cat_' + category.id, name:category.symbol, 'class': ((intIndex % 2 == 0)?'even':'odd')},
                     $.TD({}, category.symbol),
                     $.TD({}, category.description),
                     $.TD({}, category.equalCats),
                     $.TD({}, category.groupName),
                     $.TD({}, category.seatQuantity),
	                 $.TD({}, $.IMG({'src': '../images/ico-edit.gif', 'align': 'absbottom'}), editLink, " ", $.IMG({'src': '../images/ico-delete.gif', 'align': 'absbottom'}), removeLink)
                   );
          $('#categoriesBody').append(tr);               
    	});
    });
  }
  
  function showAliases(catId) {
    $('#aliasesBody').children().remove();
    CategoryDwr.getAliases(catId, function(aliases) {
		if (aliases == null || aliases.length == 0) {
		  $('#categoryAliasTable').hide();
		  $('#removeCategoryAliasDiv').hide();
		  return;
		}
		$('#removeCategoryAliasDiv').show();
  	    $('#categoryAliasTable').show();
    	$.each(aliases, function(intIndex, alias) {
    	  var deleteLink = $.A({href:'#'}, 'Delete');
     	  $(deleteLink).click(function(event) {
    	    removeAlias(alias.id, alias.startSynonym, alias.catId);
    	    event.preventDefault();
    	  });

          var tr = $.TR({id:'alias_' + alias.id, name:alias.startSynonym, 'class': ((intIndex % 2 == 0)?"even":"odd")},
                     $.TD({}, alias.category.symbol),
                     $.TD({}, alias.startSynonym),
                     $.TD({}, alias.endSynonym),
                     $.TD({}, deleteLink)
                   );
          $('#aliasesBody').append(tr);               
    	});
    });
  }
  
  function removeAlias(id, syn, catId) {
    var answer = confirm('Are you sure you want to remove the alias ' + syn + '?');
    if (answer) {
      CategoryDwr.removeAlias(id);
      showAliases(catId);
    }
  }
  
  function addCategory() {
    var categoryName = $.trim($('#category_name').val());
    var categoryDesc = $.trim($('#category_description').val());
    var categoryEq = $.trim($('#category_eq_map').val());
    var categoryGroup = $.trim($('#category_group_name').val());
    var categoryQty = $.trim($('#category_seat_quantity').val());
    if (categoryEq.length == 0) {
    	categoryEq = "*";
    }

    if (categoryName.length == 0 || categoryDesc.length == 0) {
        ("Please provide a name and description");
      return;
    }

    if (categoryGroup.length == 0) {
        ("Please provide a group name ie: DEFAULT");
      return;
    }

	CategoryDwr.addCategory(${tour.id}, categoryName, categoryDesc, categoryEq, categoryGroup, categoryQty, function (response) {
      if (response != 'OK') {
        alert(response);
        return;
      }
      refreshCategories();
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function editCategory() {
    var categoryId = $('#category_id').val();
    var categoryName = $.trim($('#category_name').val());
    var categoryDesc = $.trim($('#category_description').val());
    var categoryEq = $.trim($('#category_eq_map').val());
    var categoryQty = $.trim($('#category_seat_quantity').val());
    var categoryGroup = $.trim($('#category_group_name').val());
    
    if (categoryEq.length == 0) {
    	categoryEq = "*";
    }
    
    
    if (categoryName.length == 0 || categoryDesc.length == 0) {
      alert("Please provide a name and description");
      return;
    }
    
	CategoryDwr.editCategory(categoryId, ${tour.id}, categoryName, categoryDesc, categoryEq, categoryGroup, categoryQty, function (response) {
      if (response != 'OK') {
        $('#info').html(response);
        return;
      }
      refreshCategories();
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function addAlias() {
    var categoryId = $('#category_id').val();
    var newAlias = $.trim($('#category_alias').val());
    var newAliasEnd = $.trim($('#category_alias_end').val());
    
    if (newAlias.length == 0) {
      alert("Please provide an alias");
      return;
    }
    
	CategoryDwr.addAlias(categoryId, newAlias, newAliasEnd, function (response) {
      if (response != 'OK') {
      	alert(response);
        $('#info').html(response);
        return;
      }
      $('#addCategoryLink').show();
      $('#addOrEditCategory').hide();
	});
  }

  function showEditCategory(tr) {
    $('#addCategoryLink').hide();
    var children = $(tr).children('td');
    $('#category_name').val($(children[0]).text());
    $('#category_description').val($(children[1]).text());
    $('#category_eq_map').val($(children[2]).text());
    $('#category_group_name').val($(children[3]).text());
    $('#category_seat_quantity').val($(children[4]).text());

    $('#addOrEditCategoryTitle').html('Edit Zone');
    $('#addOrEditCategoryButton').val("Update");
    $('#addOrEditCategory').show();
    $('#addOrEditCategoryButton').click(editCategory);
    $('#addCategoryAliasButton').click(addAlias);
    $('#category_id').val(tr.id.substring(4));
    showAliases(tr.id.substring(4));
  }

  function showAddCategory() {
    $('#addCategoryLink').hide();
    $('#addOrEditCategoryTitle').html("Add Zone");
    $('#addOrEditCategoryButton').val("Add");
    $('#addOrEditCategory').show();
    $('#addOrEditCategoryButton').click(addCategory);
  }
  
  function sendUpdateRequest() {
  	CategoryDwr.categoryUpdateRequestToZone(${tour.id}, function (response) {
      if (response != 'OK') {
        $('#info').html(response);
        alert("Done sending request to zone");
        return;
      }
	});
  }
*/
</script>