<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<tiles:useAttribute id="selectedDisplaySubMenu" name="selectedDisplaySubMenu" scope="request" />

<div id="editorSubMenu">

 <c:choose>
   <c:when test="${selectedDisplaySubMenu == 'Tour Category'}">
        <b>Parent Category</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageTourCategory">Parent Category</a>
        
   </c:otherwise>
 </c:choose>
    |
    <c:choose>
   <c:when test="${selectedDisplaySubMenu == 'Child Tour Category'}">
        <b>Child Category</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageChildTourCategory">Child Category</a>
   </c:otherwise>
 </c:choose>
    |
    <c:choose>
   <c:when test="${selectedDisplaySubMenu == 'Grand Child Category'}">
        <b>Grand Child Category</b>
   </c:when>
   <c:otherwise>
        <a href="EditorManageGrandChildTourCategory">Grand Child Category</a>
   </c:otherwise>
 </c:choose>
 </div>
 <tiles:insertAttribute name="subBody1" /> 
   