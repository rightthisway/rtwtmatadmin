<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<h1>Tour/Season Price Adjustment</h1>
<form action="EditTourPriceAdjustment" method="post" onsubmit="return checkArguments();">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="tourId" value="${tour.id}" />
	<input type="hidden" name="siteId" value="${param.siteId}" />
	
	<table>
		<tr>
			<td>Tour/Season</td>
			<td>${tour.name}</td>
			<td></td>
		</tr>
		<tr>
			<td>Site</td>
			<td>${param.siteId}</td>
			<td></td>
		</tr>
		<tr>
			<c:choose>
				<c:when test="${empty tourPriceAdjustment.percentAdjustment}">
					<c:set var="tourPriceAdjustment" value="0.00" />
				</c:when>
				<c:otherwise>
					<c:set var="tourPriceAdjustment" value="${tourPriceAdjustment.percentAdjustment}" />
				</c:otherwise>
			</c:choose>

			<td>
				Tour/Season Price Adjustment
				<small><i>(was <a href="javascript:$('#tourPriceAdjustment').val('<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${tourPriceAdjustment}" />');void(0)"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${tourPriceAdjustment}" />%)</a></i></small>				
			</td>
			<td>
				<input type="text" id="tourPriceAdjustment" name="tourPriceAdjustment" value="<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${tourPriceAdjustment}" />" style="width: 60px;" />%
				<c:choose>
					<c:when test="${empty site || site.priceRounding == 'NONE'}">
					</c:when>								
					<c:when test="${site.priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
					</c:when>
					<c:when test="${site.priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
						<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
					</c:when>
				</c:choose>				
			</td>
			<td>
				<span style="color:red" id="tourPriceAdjustment_error"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="javascript:updateEventPriceAdjustments()"><small style="font-size: 10px;">Set all event price adjustments to this value</small></a>
			</td>
		</tr>

		<tr>
			<td colspan="2"><br /><b>Event Price Adjustments</b><br /></td>
		</tr>
		<c:forEach var="event" items="${tour.events}">
			<tr>
				<c:choose>
					<c:when test="${empty eventPriceAdjustments[event.id].percentAdjustment}">
						<c:set var="percentAdjustment" value="0.00" />
					</c:when>
					<c:otherwise>
						<c:set var="percentAdjustment" value="${eventPriceAdjustments[event.id].percentAdjustment}" />
					</c:otherwise>
				</c:choose>

				<td>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${event.date}" />
					<small><i>(was <a href="javascript:$('#eventPriceAdjustment_${event.id}').val('<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />');void(0)"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />%)</a></i></small>
				</td>
				<td>
					<input type="text" id="eventPriceAdjustment_${event.id}" name="eventPriceAdjustment_${event.id}" class="eventPriceAdjustment" value='<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" groupingUsed="false" value="${percentAdjustment}" />'  style="width: 60px;" />%
					<c:choose>
						<c:when test="${empty site || site.priceRounding == 'NONE'}">
						</c:when>								
						<c:when test="${site.priceRounding == 'UP_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-up.gif" align="absmiddle" title="rounded up to the nearest dollar" />
						</c:when>
						<c:when test="${site.priceRounding == 'DOWN_TO_NEAREST_DOLLAR'}">
							<img src="../images/ico-round-down.gif" align="absmiddle" title="rounded down to the nearest dollar" />
						</c:when>
					</c:choose>					
				</td>
				<td>				
					<span style="color:red" id="eventPriceAdjustment_${event.id}_error"></span>
				</td>
			</tr>			
		</c:forEach>
	</table>

	<input type="submit" class="medbutton" value="Save" />
	<input type="button" class="medbutton" value="Cancel" onclick="document.location.href='BrowseEvents?tourId=${tour.id}'" />

</form>

<script type="text/javascript">
	function updateEventPriceAdjustments() {
		var percentValue = formatFloat($('#tourPriceAdjustment').val());
		$('#tourPriceAdjustment').val("" + percentValue);
		$('.eventPriceAdjustment').val("" + percentValue);
	};
	
	function checkArguments() {
	
		var error = false;

		if ($.trim($('#tourPriceAdjustment').val()) == "" || isNaN($('#tourPriceAdjustment').val() * 1)) {
			$('#tourPriceAdjustment_error').text("Invalid price adjustment for tour");
			error = true;
		}
		
		for (var i = 0 ; i < $('.eventPriceAdjustment').length ; i++) {
			var elt = $('.eventPriceAdjustment').get(i);
			if ($.trim($(elt).val()) == "" || isNaN($(elt).val() * 1)) {
				var eventId = $(elt).attr('id').split("_")[1]; 
				$('#eventPriceAdjustment_' + eventId + '_error').text("Invalid price adjustment " + $(elt).val() + " for event.");
				error = true;
			}			
		}
		return !error;
	};
</script>