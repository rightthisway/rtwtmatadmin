<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<jsp:directive.page import="java.io.*"/>

<%
String referrerUrl = request.getHeader("referer");
if (referrerUrl != null) {
	referrerUrl = referrerUrl.replace("\n", "");
	pageContext.setAttribute("referrerUrl", referrerUrl);
}

String userAgent = request.getHeader("user-agent");
if (userAgent != null) {
	userAgent = userAgent.replace("\n", "");
	pageContext.setAttribute("userAgent", userAgent);
}

String clientIpAddress = request.getRemoteAddr();
if (clientIpAddress != null) {
	clientIpAddress = clientIpAddress.replace("\n", "");
	pageContext.setAttribute("clientIpAddress", clientIpAddress);
}

pageContext.setAttribute("constants", com.admitone.tmat.web.Constants.getInstance());

%>

<html>
  <head>
    <title>Error 500</title>
	<script type='text/javascript' src='../js/jquery/jquery.js'></script>
    <script type='text/javascript' src='interface/DataDwr.js'></script>
    <script type='text/javascript' src='engine.js'></script>         
    <style type="text/css">
		@import url(${pageContext.request.contextPath}/css/common.css);
	</style>
    
  </head>
  <body style="padding:10px">
    <a href="${pageContext.request.contextPath}/">
		<c:choose>
			<c:when test="${constants.ipAddress == '172.31.255.12'}"><img src="${pageContext.request.contextPath}/images/logo-admit-one.gif" border="0"/></c:when>
			<c:otherwise><img src="${pageContext.request.contextPath}/images/logo-admit-one-test.gif" border="0"/></c:otherwise>
		</c:choose>
    </a>    
    <br/>
    <h1>Error 500</h1>

	<h2>Error Stack Trace</h2>

    <div id="feedbackForm">    

	<textarea id="errorStackTrace" style="width:100%;height:140px" readonly><%
	Exception ex = (Exception)request.getAttribute("exception"); 
	if (ex != null) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		out.print(sw);
		sw.close();
		pw.close();
	}
%></textarea>
	
<h2>Error Feedback Form</h2>
	
	You can help us to make this application better by sending us an error feedback.<br />
<table>
	<tr>
		<td>Current URL</td>
		<td><input type="text" id="currentUrl" value="" style="width:600px" readonly /></td>
	</tr>
	<tr>
		<td>Referrer URL</td>
		<td><input type="text" id="referrerUrl" value="${referrerUrl}"  style="width:600px" readonly /></td>
	</tr>
	<tr>
		<td>User Agent</td>
		<td><input type="text" id="userAgent" value="${userAgent}"  style="width:600px" readonly /></td>
	</tr>	
</table>
<br />
Describe the action that leads to this error.<br /><br />
Project: <select id="errorFeedbackProject">
	<option value='TMATT'>TMAT-T</option>
	<option value='TMATV'>TMAT-V</option>
	<option value='TMATIV'>TMAT-IV</option>
	<option value='CATEGORYSUPPORT'>Zone Support</option>
	<option value='CHEAPTIX'>Cheap Tix</option>
	<option value='CIRCLES'>Circles</option>
	<option value='EBAY'>eBay</option>
	<option value='INTRANET'>Intranet</option>
	<option value='ONSALE'>Onsale</option>
	<option value='PORTAL'>Portal</option>
	<option value='TMATII'>TMAT-II</option>
</select><br />
Summary: <input type="text" id="errorFeedbackSummary" style="width:100%;" /><br />
Priority:
<select id="errorFeedbackPriority">
  <option value="4">Normal</option>
  <option value="5">High</option>
  <option value="6">Urgent</option>
  <option value="7">Immediate</option>
  <option value="10">Blocker</option>
</select>
<br />
Message<br />
<textarea id="errorFeedbackMessage" style="width:100%;height: 140px"></textarea>
<br /><br />
By clicking on the Send button, an email will be sent to the developers with your message and the exception stack trace shown above.<br />
<div id="feedbackButtonContainer">
	<input type="button" value="Send" class="medbutton" onclick="sendErrorFeedback()" />
	<input type="button" value="Back" class="medbutton" onclick="history.go(-1)" />
</div>
<div id="feedbackLoading" style="display:none">
	<img src="../images/process-running.gif" align="absbottom" />Submitting feedback...
</div>

</div>

<div id="feedbackThankYou" style="display:none">
	<div class="info">
		<div class="infoText">
			Your feedback has been sent. Thank you!
		</div>
	</div>
	<input type="button" value="Back" class="medbutton" onclick="history.go(-1)" />
</div>

<script type="text/javascript">
	$('#currentUrl').val(document.location.href);

	function sendErrorFeedback() {	
		$('#feedbackButtonContainer').hide();	
		$('#feedbackLoading').show();	
		DataDwr.sendErrorFeedback(
				$('#errorFeedbackProject').val(),		
				$('#errorFeedbackSummary').val(),
				$('#errorFeedbackPriority').val(),
				$('#errorFeedbackMessage').val(),
				$('#errorStackTrace').val(),
				document.location.href,
				$('#referrerUrl').val(),
				document.cookie,
				$('#userAgent').val(),
				"${clientIpAddress}",
				function(result) {
					if (result == "OK") {
						$('#feedbackForm').hide();
						$('#feedbackThankYou').show();
					} else {
						alert(result);
						$('#feedbackButtonContainer').show();	
						$('#feedbackLoading').hide();	
					}
				}
		);
				
	};
</script>


  </body>
</html>