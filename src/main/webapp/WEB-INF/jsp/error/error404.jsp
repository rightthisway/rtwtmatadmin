<%@include file="/WEB-INF/tiles/taglibs.jsp"%>

<%
pageContext.setAttribute("constants", com.admitone.tmat.web.Constants.getInstance());
%>

<html>
  <head>
    <title>Error 404</title>
    <style type="text/css">
		@import url(${pageContext.request.contextPath}/css/common.css);
	</style>
    
  </head>
  <body style="padding:10px">
    <a href="${pageContext.request.contextPath}/">
		<c:choose>
			<c:when test="${constants.ipAddress == '172.31.255.12'}"><img src="${pageContext.request.contextPath}/images/logo-admit-one.gif" border="0"/></c:when>
			<c:otherwise><img src="${pageContext.request.contextPath}/images/logo-admit-one-test.gif" border="0"/></c:otherwise>
		</c:choose>
    </a>
    <br/>
    <h1>Error 404 - Not Found</h1>
    The page <font color="#0f7e00"><%= request.getAttribute("javax.servlet.forward.request_uri") %></font> was not found on this server.
    <br /><br />
    <a href="${pageContext.request.contextPath}/">Go to TMAT</a>
  </body>
</html>