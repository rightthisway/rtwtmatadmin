/**
 * Ext.ux.grid.livegrid.ArrayReader

Ext.namespace('Ext.ux.grid.livegrid');

/**
 * @class Ext.ux.grid.livegrid.ArrayReader
 * @extends Ext.data.ArrayReader
 * @constructor
 * @param {Object} config
 *
 * @author pblaze
 */
Ext.ux.grid.livegrid.ArrayReader = function(meta, recordType){

    Ext.ux.grid.livegrid.ArrayReader.superclass.constructor.call(this, meta, recordType);
};


Ext.extend(Ext.ux.grid.livegrid.ArrayReader, Ext.data.ArrayReader, {

    /**
     * @cfg {String} versionProperty Name of the property from which to retrieve the
     *                               version of the data repository this reader parses
     *                               the reponse from
     */



    /**
     * Create a data block containing Ext.data.Records from a Array object.
     * @param {Object} o An object which contains an Array of row objects in the property specified
     * in the config as 'root, and optionally a property, specified in the config as 'totalProperty'
     * which contains the total size of the dataset.
     * @return {Object} data A data block which is used by an Ext.data.Store object as
     * a cache of Ext.data.Records.
     */
    readRecords : function(o)
    {
        var s = this.meta;

        if(!this.ef && s.versionProperty) {
            this.getVersion = this.getArrayAccessor(s.versionProperty);
        }

        // shorten for future calls
        if (!this.__readRecords) {
            this.__readRecords = Ext.ux.grid.livegrid.ArrayReader.superclass.readRecords;
        }

        var intercept = this.__readRecords.call(this, o);


        if (s.versionProperty) {
            var v = this.getVersion(o);
            intercept.version = (v === undefined || v === "") ? null : v;
        }


        return intercept;
    }

});