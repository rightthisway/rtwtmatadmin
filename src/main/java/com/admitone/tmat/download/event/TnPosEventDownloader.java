package com.admitone.tmat.download.event;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.NewPOSEvents;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.utils.POSEventUtil;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.mail.MailAttachment;
public class TnPosEventDownloader implements InitializingBean {
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}

	private void start() {
		TimerTask tasknew = new TimerSchedulePeriod(mailIdProperty);
		Timer timer = new Timer();
		timer.schedule(tasknew, getStartTimeForTimerOne(), fONCE_PER_DAY); //1000*60*60*1
		
		TimerTask taskPerDay = new TimerSchedulePeriod(mailIdProperty);
		Timer timerPerDay = new Timer();
		timerPerDay.schedule(taskPerDay, getStartTimeForTimerTwo(), 1000*60*60*24); //1000*60*60*24
	}
	private final static long fONCE_PER_DAY = 1000*60*60*1;
		static Date date=new Date();
		static int hour = Integer.parseInt(new SimpleDateFormat("HH").format(date));

	  private final static int HOUR_TIMER_ONE = (hour+1); //(hour+1);
	  private final static int MINUTE_TIMER_ONE = 0; //0
	  
	  
	  private final static int HOUR_TIMER_TWO = 6; //6;
	  private final static int MINUTE_TIMER_TWO = 55; //55;

	 private static Date getStartTimeForTimerOne(){
		    Calendar tomorrow = new GregorianCalendar();
		    Calendar result = new GregorianCalendar(
		      tomorrow.get(Calendar.YEAR),
		      tomorrow.get(Calendar.MONTH),
		      tomorrow.get(Calendar.DATE),
		      HOUR_TIMER_ONE,
		      MINUTE_TIMER_ONE 
		    );
		    return result.getTime();
	 }
	 private static Date getStartTimeForTimerTwo() {
		 
		    Calendar calendar = new GregorianCalendar();
		    
		    if(calendar.get(Calendar.HOUR_OF_DAY) > HOUR_TIMER_TWO || 
		    	(calendar.get(Calendar.HOUR_OF_DAY) == HOUR_TIMER_TWO && calendar.get(Calendar.MINUTE) >= MINUTE_TIMER_TWO)) {
		    	calendar.add(Calendar.DATE, 1);
		    }
		    
		    Calendar result = new GregorianCalendar(
		    	calendar.get(Calendar.YEAR),
		    	calendar.get(Calendar.MONTH),
		    	calendar.get(Calendar.DATE),
		    	HOUR_TIMER_TWO,
		    	MINUTE_TIMER_TWO
		    );
		    return result.getTime();
	 }
}

class TimerSchedulePeriod extends TimerTask {
	private Logger logger = LoggerFactory.getLogger(TimerSchedulePeriod.class);
	public TimerSchedulePeriod(MailIdProperty mailIdProperty) {
		super();
		this.mailIdProperty = mailIdProperty;
	}
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	@Override
	public void run() {
		
		try{
			Calendar calendar =Calendar.getInstance();
			boolean fileCreationFlag = true,sendMailFlag = false,noEventEmailFlag = false;
			Calendar cal = Calendar.getInstance();
			DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			if(calendar.get(Calendar.HOUR_OF_DAY) == 6 && calendar.get(Calendar.MINUTE) == 55) {//if(calendar.get(Calendar.MINUTE) == MINUTE_TIMER_TWO) {
				fileCreationFlag = false;
			} 
			System.out.println("Timer started:" + new Date());
			
			Map<String, Object> map = new HashMap<String, Object>();
			MailAttachment[] mailAttachments = null;
			String subject=null,resource=null;
			
			if(fileCreationFlag) {
				FileOutputStream fop = null;
				File file;
				try {
					//System.out.println("file location..."+mailIdProperty.getExternalFileServerUrl());
					file = new File("\\\\"+mailIdProperty.getEventsNotinTMATCsvFolderPath()+"\\EVENTSNOTINTMAT.csv");
					//file= new File("S:\\EVENTSNOTINTMAT.csv");
					fop = new FileOutputStream(file);
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					}
					StringBuffer buffer = POSEventUtil.getLatestNewPosEvents(null,null);
					fop.write(buffer.toString().getBytes());
					
					fop.flush();
					fop.close();
					logger.info("Excel file generated.....");
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Eror while generating excel file....",e);
				} finally {
					try {
						if (fop != null) {
							fop.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				//Condition checking - for sending hourly email between morning 7Am to evening 9PM only
				if(calendar.get(Calendar.HOUR_OF_DAY) >= 7 && calendar.get(Calendar.HOUR_OF_DAY) <= 21 ) {	
					String fromDateParam = null;
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					
//					Property property = DAORegistry.getPropertyDAO().get("pos.eventlisting.mail.datetime");
					
//					if(property.getValue() != null && property.getValue().trim().length() > 0) {
//						fromDateParam = property.getValue();
//						Date lastEventCreateDate = dateTimeFormat.parse(fromDateParam);
						
//						cal = Calendar.getInstance();
//						cal.add(Calendar.HOUR_OF_DAY, -2);
//						cal.set(Calendar.MINUTE,0);
//						cal.set(Calendar.SECOND,0);
						
//						Date lastHourDate = new Date(cal.getTimeInMillis());
						/*Date lastHourDate = new Date();
						lastHourDate.setHours(lastHourDate.getHours()-2);
						lastHourDate.setMinutes(0);
						lastHourDate.setSeconds(0);*/
						
						//Condition checking - for if last event created time is before 2 hours then fetch all events from that time other wise fetch events created within last 2 hours 
//						if(lastEventCreateDate.getTime()>lastHourDate.getTime()) {
//							fromDateParam = dateTimeFormat.format(lastHourDate);
//						}
						
//					} else {
						cal = Calendar.getInstance();
						cal.add(Calendar.HOUR_OF_DAY, -2);
						cal.set(Calendar.MINUTE,0);
						cal.set(Calendar.SECOND,0);
						
						Date lastHourDate = new Date(cal.getTimeInMillis());
						fromDateParam = dateTimeFormat.format(lastHourDate);
						/*Date lastHourDate = new Date();
						lastHourDate.setHours(lastHourDate.getHours()-2);
						fromDateParam = df.format(lastHourDate)+" "+lastHourDate.getHours()+":00:00"; */
//					}
					
					//Condition checking - for every day first hourly email(AT 7-AM) should contain last one hour events only.
					/*if(calendar.get(Calendar.HOUR_OF_DAY) == 7) {
						cal = Calendar.getInstance();
						cal.add(Calendar.HOUR_OF_DAY, -1);
						cal.set(Calendar.MINUTE,0);
						cal.set(Calendar.SECOND,0);
						
						Date lastHourDate = new Date(cal.getTimeInMillis());
						fromDateParam = dateTimeFormat.format(lastHourDate);
						property.setValue(dateTimeFormat.format(lastHourDate));
						
						DAORegistry.getPropertyDAO().saveOrUpdate(property);
						
						cal = Calendar.getInstance();
						cal.add(Calendar.HOUR_OF_DAY, -2);
						cal.set(Calendar.MINUTE,0);
						cal.set(Calendar.SECOND,0);
						
						lastHourDate = new Date(cal.getTimeInMillis());
						fromDateParam = dateTimeFormat.format(lastHourDate);
					}*/
					
					List<NewPOSEvents> list = DAORegistry.getQueryManagerDAO().getNewPosEventsFromStub(fromDateParam);
					//List<NewPOSEvents> list = DAORegistry.getNewPOSEventDAO().getNewPOSEventsByDateStr(fromDateParam);
					
					if(null != list && !list.isEmpty()) {
						int createdEventCount = 0;
						for (NewPOSEvents newPOSEvents : list) {
							if(newPOSEvents.getEventExistinTMAT() != null &&
									newPOSEvents.getEventExistinTMAT().equalsIgnoreCase("Yes")) {
								createdEventCount++;
							}
						}
//						Date createdDate = list.get(0).getCreatedDate();
//						cal = Calendar.getInstance();
//						cal.setTime(createdDate);
//						cal.add(Calendar.SECOND, 1);
//						createdDate = new Date(cal.getTimeInMillis());
//						property.setValue(dateTimeFormat.format(createdDate));
						
						/*Date createdDate = list.get(0).getCreatedDate();
						createdDate.setSeconds(createdDate.getSeconds()+1);
						
						DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						property.setValue(dateTimeFormat.format(createdDate));*/
//						DAORegistry.getPropertyDAO().saveOrUpdate(property);
						
						map.put("numevents", createdEventCount);
						map.put("newPosEventsList", list);
						
						resource = "mail-new-pos-events.txt";
						
					} else {
						noEventEmailFlag = true;
						resource = "mail-new-pos-no-events.txt";
					}
					mailAttachments = null;
					subject = "Updated Ticketnetwork Event Creation(LAST 2HOUR)";
					sendMailFlag = true;
				}
					
			} else {
				
				
				String fromDateParam = null;
//				Property property = DAORegistry.getPropertyDAO().get("pos.eventlisting.mail.datetime");
				
//				if(property.getValue() != null && property.getValue().trim().length() > 0) {
//					fromDateParam = property.getValue();
					
//				} else {
					cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_MONTH, -1);
					cal.set(Calendar.HOUR_OF_DAY,21);
					cal.set(Calendar.MINUTE,0);
					cal.set(Calendar.SECOND,0);
					
					Date lastDayDate = new Date(cal.getTimeInMillis());
					fromDateParam = dateTimeFormat.format(lastDayDate);
					
					/*Date lastDayDate = new Date();
					lastDayDate.setDate(lastDayDate.getDate()-1);
					DateFormat  df = new SimpleDateFormat("yyyy-MM-dd");
					fromDateParam = df.format(lastDayDate)+" 21:00:00";*/ 
//				}
				
				List<NewPOSEvents> list = DAORegistry.getQueryManagerDAO().getNewPosEventsFromStub(fromDateParam);
				//List<NewPOSEvents> list = DAORegistry.getNewPOSEventDAO().getNewPOSEventsByDateStr(fromDateParam);
				
				logger.info("TNPOS time..."+fromDateParam);
				if(null != list && !list.isEmpty()) {
					logger.info("TNPOS size..."+list.size());
					
					int createdEventCount = 0;
					for (NewPOSEvents newPOSEvents : list) {
						if(newPOSEvents.getEventExistinTMAT() != null &&
								newPOSEvents.getEventExistinTMAT().equalsIgnoreCase("Yes")) {
							createdEventCount++;
						}
					}
					
//					Date createdDate = list.get(0).getCreatedDate();
//					cal = Calendar.getInstance();
//					cal.setTime(createdDate);
//					cal.add(Calendar.SECOND, 1);
//					createdDate = new Date(cal.getTimeInMillis());
//					property.setValue(dateTimeFormat.format(createdDate));
					
					/*Date createdDate = list.get(0).getCreatedDate();
					createdDate.setSeconds(createdDate.getSeconds()+1);
					DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					property.setValue(dateTimeFormat.format(createdDate));*/
					
//					DAORegistry.getPropertyDAO().saveOrUpdate(property);
					
					StringBuffer buffer = POSEventUtil.getLatestNewPosEvents(fromDateParam,list);
					
					if(null != buffer && buffer.length() > 0) {
						//List<NewPOSEvents> list = DAORegistry.getNewPOSEventDAO().getNewPOSEventsByDateStr(fromDateParam);
						map.put("numevents", createdEventCount);
						//map.put("newPosEventsList", posList);
						
						mailAttachments = new MailAttachment[1];
						mailAttachments[0] = new MailAttachment(buffer.toString().getBytes(), "text/csv", "EVENTSNOTINTMAT.csv");
						
						/*df = new SimpleDateFormat("MM/dd/yyyy");
						df.format(lastDayDate)+" 21:00";*/
						
						//subject = "Updated Ticketnetwork Event Creation(LASTNIGHT TO TODAY 7AM)";
						resource = "mail-event.html";
						sendMailFlag = true;
					}
				} else {
					resource = "mail-new-pos-no-events.txt";
					mailAttachments = null;
				}
				subject = "Updated Ticketnetwork Event Creation(LASTNIGHT TO TODAY 7AM)";
				sendMailFlag = true;
			}
			
			if(sendMailFlag) {
				String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
			//	String smtpHost="192.168.0.108";
				Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
				String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
				String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
				String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
				Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
				String transportProtocol = "";
				String mailTo =mailIdProperty.getTnposMailerList();/* "ezippo@rightthisway.com,nizzo@rightthisway.com,jtatarzycki@rightthisway.com," +
					//	"leor@rewardthefan.com,jmitchell@rightthisway.com,rdelligatti@rightthisway.com,lrobinson@rightthisway.com," +
					//	"srosenberg@rightthisway.com,jstfleur@rightthisway.com";*/
				//String mailTo="achawan@rightthisway.com";
				String mailFrom = mailIdProperty.getTmatmanagers();//"tmatmanagers@rightthisway.com";
				String cc = null;//"araut@rightthisway.com";
				String bcc = mailIdProperty.getAoDev();
				
				SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
						smtpUsername, smtpPassword, null, mailFrom, mailTo, cc, bcc, subject,resource, map, "text/html", mailAttachments);
				
				if(noEventEmailFlag) {
					
					if(calendar.get(Calendar.HOUR_OF_DAY) >= 7) {//send alert emails only after 7AM
						//mailTo = "tselvan@rightthisway.com,araut@rightthisway.com,cshah@rightthisway.com";
						//mailFrom = "tselvan@rightthisway.com";
						
						mailTo = mailIdProperty.getPosNoEventAlertMailTo();
						mailFrom = mailIdProperty.getPosNoEventAlertMailFrom();
						
						cc = null;//"araut@rightthisway.com";
						bcc = null;//mailIdProperty.getAoDev();
						subject = "No New Events Creatd in POS within Last 2Hour";
						resource = "mail-new-pos-no-events.txt";
						mailAttachments = null;
						
						SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
								smtpUsername, smtpPassword, null, mailFrom, mailTo, cc, bcc, subject,resource, map, "text/html", mailAttachments);
					} 
					
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}