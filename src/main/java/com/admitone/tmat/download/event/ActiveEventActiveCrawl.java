package com.admitone.tmat.download.event;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.persistence.Column;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ActiveEventAndActiveCrawls;
import com.admitone.tmat.utils.SpringUtil;
public class ActiveEventActiveCrawl implements InitializingBean {
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}

	private void start() {
		TimerTask tasknew = new TimerSchedulePeriodTmatStat(mailIdProperty);
		Timer timerPerDay = new Timer();
		timerPerDay.schedule(tasknew, getStartTimeForTimerTwo(), 1000*60*60*24); //3600000 //60000
	}
	//private final static long fONCE_PER_DAY = 1000*60*60*1;
	 private final static int HOUR_TIMER_TWO =6;  //6;
	 private final static int MINUTE_TIMER_TWO =00; // 00;

	 private static Date getStartTimeForTimerTwo() {
		 
		    Calendar calendar = new GregorianCalendar();
		    
		    if(calendar.get(Calendar.HOUR_OF_DAY) > HOUR_TIMER_TWO || 
		    	(calendar.get(Calendar.HOUR_OF_DAY) == HOUR_TIMER_TWO && calendar.get(Calendar.MINUTE) >= MINUTE_TIMER_TWO)) {
		    	//calendar.add(Calendar.DATE, 1);
		    }
		    
		    Calendar result = new GregorianCalendar(
		    	calendar.get(Calendar.YEAR),
		    	calendar.get(Calendar.MONTH),
		    	calendar.get(Calendar.DATE),
		    	HOUR_TIMER_TWO,
		    	MINUTE_TIMER_TWO
		    );
		    return result.getTime();
	 }
}

class TimerSchedulePeriodTmatStat extends TimerTask {
	public TimerSchedulePeriodTmatStat(MailIdProperty mailIdProperty) {
		super();
		this.mailIdProperty = mailIdProperty;
	}
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	@Override
	public void run() {
		System.out.println("Tmat stat Timer started:" + new Date());
		int list=DAORegistry.getEventDAO().getEventCount();
		int sport=DAORegistry.getEventDAO().getEventCountSport();
		int concert=DAORegistry.getEventDAO().getEventCountConcerts();
		//int defaults=DAORegistry.getEventDAO().getEventCountDefault();
		int theater=DAORegistry.getEventDAO(). getEventCountTheater();
		//int lasvegas=DAORegistry.getEventDAO().getEventCountLasVegas();
		int other=DAORegistry.getEventDAO().getEventCountOther();
		int totalenabledcrawl=DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlCountByEnabled();
		
		//stubhub
		int sportStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubSportsCrawlCount();
		int theaterStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubTheaterCrawlCount();
		int concertStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubConcertCrawlCount();
		int otherStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubOtherCrawlCount();
		//int lasVegasStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubLasVegasCrawlCount();
		//int defaultStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubDefaultCrawlCount();
		int totalStubhub=sportStubhub+theaterStubhub+concertStubhub+otherStubhub/*+lasVegasStubhub+defaultStubhub*/;
		//TND
		int sportTnd=DAORegistry.getTicketListingCrawlDAO().getTndSportsCrawlCount();
		int theaterTnd=DAORegistry.getTicketListingCrawlDAO().getTndTheaterCrawlCount();
		int concertTnd=DAORegistry.getTicketListingCrawlDAO().getTndConcertCrawlCount();
		int otherTnd=DAORegistry.getTicketListingCrawlDAO().getTndOtherCrawlCount();
		//int lasVegasTnd=DAORegistry.getTicketListingCrawlDAO().getTndLasVegasCrawlCount();
		//int defaultTnd=DAORegistry.getTicketListingCrawlDAO().getTndDefaultCrawlCount();
		int totalTnd=sportTnd+theaterTnd+concertTnd+otherTnd/*+lasVegasTnd+defaultTnd*/;
		
		//Tnow
		int sportTnow=DAORegistry.getTicketListingCrawlDAO().getTnowSportsCrawlCount();
		int theaterTnow=DAORegistry.getTicketListingCrawlDAO().getTnowTheaterCrawlCount();
		int concertTnow=DAORegistry.getTicketListingCrawlDAO().getTnowConcertCrawlCount();
		int otherTnow=DAORegistry.getTicketListingCrawlDAO().getTnowOtherCrawlCount();
		//int lasVegasTnow=DAORegistry.getTicketListingCrawlDAO().getTnowLasVegasCrawlCount();
		//int defaultTnow=DAORegistry.getTicketListingCrawlDAO().getTnowDefaultCrawlCount();
		int totalTnow=sportTnow+theaterTnow+concertTnow+otherTnow/*+lasVegasTnow+defaultTnow*/;
		//Te
		int sportTe=DAORegistry.getTicketListingCrawlDAO().getTeSportsCrawlCount();
		int theaterTe=DAORegistry.getTicketListingCrawlDAO().getTeTheaterCrawlCount();
		int concertTe=DAORegistry.getTicketListingCrawlDAO().getTeConcertCrawlCount();
		int otherTe=DAORegistry.getTicketListingCrawlDAO().getTeOtherCrawlCount();
		//int lasVegasTe=DAORegistry.getTicketListingCrawlDAO().getTeLasVegasCrawlCount();
		//int defaultTe=DAORegistry.getTicketListingCrawlDAO().getTeDefaultCrawlCount();
		int totalTe=sportTe+theaterTe+concertTe+otherTe/*+lasVegasTe+defaultTe*/;
		
		
		//Flash Seat
		int sportFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsSportsCrawlCount();
		int theaterFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsTheaterCrawlCount();
		int concertFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsConcertCrawlCount();
		int otherFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsOtherCrawlCount();
		int totalFlSheet=sportFlSheet+theaterFlSheet+concertFlSheet+otherFlSheet;
		
		//Vivid Seat
		int sportVs=DAORegistry.getTicketListingCrawlDAO().getVsSportsCrawlCount();
		int theaterVs=DAORegistry.getTicketListingCrawlDAO().getVsTheaterCrawlCount();
		int concertVs=DAORegistry.getTicketListingCrawlDAO().getVsConcertCrawlCount();
		int otherVs=DAORegistry.getTicketListingCrawlDAO().getVsOtherCrawlCount();
		int totalVs=sportVs+theaterVs+concertVs+otherVs;
		
		
		
		//Total
		int totalSport=sportStubhub+sportTnd+sportTnow+sportTe+sportFlSheet+sportVs;
		int totalConcert=concertStubhub+concertTnd+concertTnow+concertTe+concertFlSheet+concertVs;
		int totalTheater=theaterStubhub+theaterTnd+theaterTnow+theaterTe+theaterFlSheet+theaterVs;
		int totalOther=otherStubhub+otherTnd+otherTnow+otherTe+otherFlSheet+otherVs;
		//int totalLasVegas=lasVegasStubhub+lasVegasTnd+lasVegasTnow+lasVegasTe;
		//int totalDefault=defaultStubhub+defaultTnd+defaultTnow+defaultTe;
		int total=totalenabledcrawl;
		
		  int tndSportPer=(sportTnd*100)/sport;  
		  int tndTheaterPer=(theaterTnd*100)/theater;
		  int tndConcertPer=(concertTnd*100)/concert;
		  int tndOtherPer=(otherTnd*100)/other;
		  
		  int shSportPer=(sportStubhub*100)/sport;  
		  int shTheaterPer=(theaterStubhub*100)/theater;
		  int shConcertPer=(concertStubhub*100)/concert;
		  int shOtherPer=(otherStubhub*100)/other;
		  
		  int teSportPer=(sportTe*100)/sport;  
		  int teTheaterPer=(theaterTe*100)/theater;
		  int teConcertPer=(concertTe*100)/concert;
		  int teOtherPer=(otherTe*100)/other;
		  
		  int tnowSportPer=(sportTnow*100)/sport;  
		  int tnowTheaterPer=(theaterTnow*100)/theater;
		  int tnowConcertPer=(concertTnow*100)/concert;
		  int tnowOtherPer=(otherTnow*100)/other;
		  
		  int fsSportPer=(sportFlSheet*100)/sport;  
		  int fsTheaterPer=(theaterFlSheet*100)/theater;
		  int fsConcertPer=(concertFlSheet*100)/concert;
		  int fsOtherPer=(otherFlSheet*100)/other;
		  
		  int vsSportPer=(sportVs*100)/sport;  
		  int vsTheaterPer=(theaterVs*100)/theater;
		  int vsConcertPer=(concertVs*100)/concert;
		  int vsOtherPer=(otherVs*100)/other;
	
		if(list==0)
		{
			System.out.println("no data available");
		}
		else{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("tndSportPer", tndSportPer);
		map.put("tndTheaterPer", tndTheaterPer);
		map.put("tndConcertPer", tndConcertPer);
		map.put("tndOtherPer", tndOtherPer);
		map.put("shSportPer", shSportPer);
		map.put("shTheaterPer",shTheaterPer);			
		map.put("shConcertPer", shConcertPer);
		map.put("shOtherPer", shOtherPer);
		map.put("teSportPer", teSportPer);
		map.put("teTheaterPer",teTheaterPer);			
		map.put("teConcertPer", teConcertPer);
		map.put("teOtherPer",teOtherPer);
		map.put("tnowSportPer", tnowSportPer);
		map.put("tnowTheaterPer",tnowTheaterPer);			
		map.put("tnowConcertPer", tnowConcertPer);
		map.put("tnowOtherPer",tnowOtherPer);
		map.put("fsSportPer",fsSportPer);
		map.put("fsTheaterPer",fsTheaterPer);
		map.put("fsConcertPer",fsConcertPer);
		map.put("fsOtherPer",fsOtherPer);
		map.put("vsSportPer",vsSportPer);
		map.put("vsTheaterPer",vsTheaterPer);
		map.put("vsConcertPer",vsConcertPer);
		map.put("vsConcertPer",vsConcertPer);
		map.put("vsOtherPer",vsOtherPer);
		map.put("numevents", list);
		map.put("sports", sport);
		map.put("concert", concert);
		
		
		//map.put("defaults", defaults);
		map.put("theater", theater);
		map.put("other", other);
		//map.put("lasvegas", lasvegas);
		map.put("totalenabledcrawl", totalenabledcrawl);
		//stubhub
		map.put("sportStubhub", sportStubhub);
		map.put("theaterStubhub", theaterStubhub);
		map.put("concertStubhub", concertStubhub);
		map.put("otherStubhub", otherStubhub);
		//map.put("lasVegasStubhub", lasVegasStubhub);
		//map.put("defaultStubhub", defaultStubhub);
		map.put("totalStubhub", totalStubhub);
		//TND
		map.put("sportTnd", sportTnd);
		map.put("theaterTnd", theaterTnd);
		map.put("concertTnd", concertTnd);
		map.put("otherTnd", otherTnd);
		//map.put("lasVegasTnd", lasVegasTnd);
		//map.put("defaultTnd", defaultTnd);
		map.put("totalTnd", totalTnd);
		//Tnow
		map.put("sportTnow", sportTnow);
		map.put("theaterTnow", theaterTnow);
		map.put("concertTnow", concertTnow);
		map.put("otherTnow", otherTnow);
		//map.put("lasVegasTnow", lasVegasTnow);
		//map.put("defaultTnow", defaultTnow);
		map.put("totalTnow", totalTnow);
		//Te
		map.put("sportTe", sportTe);
		map.put("theaterTe", theaterTe);
		map.put("concertTe", concertTe);
		map.put("otherTe", otherTe);
		//map.put("lasVegasTe", lasVegasTe);
		//map.put("defaultTe", defaultTe);
		map.put("totalTe", totalTe);
		//FlashSeat
		map.put("sportFlSheet", sportFlSheet);
		map.put("theaterFlSheet",theaterFlSheet);			
		map.put("concertFlSheet", concertFlSheet);
		map.put("otherFlSheet",otherFlSheet);
		map.put("totalFlSheet", totalFlSheet);
		//Vivid Seat
		map.put("sportVs", sportVs);
		map.put("theaterVs",theaterVs);			
		map.put("concertVs", concertVs);
		map.put("otherVs",otherVs);
		map.put("totalVs", totalVs);
		
		
		
		//Total
		map.put("totalSport", totalSport);
		map.put("totalTheater", totalTheater);
		map.put("totalConcert", totalConcert);
		map.put("totalOther", totalOther);
		//map.put("totalLasVegas", totalLasVegas);
		//map.put("totalDefault", totalDefault);
		map.put("total", total);
		
		ActiveEventAndActiveCrawls activeEventActiveCrawl=new ActiveEventAndActiveCrawls();
		Date date=new Date();
		Date dateBefore = new Date(date.getTime() - 1 * 24 * 3600 * 1000);
		activeEventActiveCrawl.setStatisticDate(dateBefore);
		activeEventActiveCrawl.setActiveEventSports(sport);
		activeEventActiveCrawl.setActiveEventTheater(theater);
		activeEventActiveCrawl.setActiveEventConcert(concert);
		activeEventActiveCrawl.setActiveEventOther(other);
		
		activeEventActiveCrawl.setTndExchangeCrawlsSports(sportTnd);
		activeEventActiveCrawl.setTndExchangeCrawlsTheater(theaterTnd);
		activeEventActiveCrawl.setTndExchangeCrawlsConcert(concertTnd);
		activeEventActiveCrawl.setTndExchangeCrawlsOther(otherTnd);
		
		activeEventActiveCrawl.setShExchangeCrawlsSports(sportStubhub);
		activeEventActiveCrawl.setShExchangeCrawlsTheater(theaterStubhub);
		activeEventActiveCrawl.setShExchangeCrawlsConcert(concertStubhub);
		activeEventActiveCrawl.setShExchangeCrawlsOther(otherStubhub);
		
		activeEventActiveCrawl.setTeExchangeCrawlsSports(sportTe);
		activeEventActiveCrawl.setTeExchangeCrawlsTheater(theaterTe);
		activeEventActiveCrawl.setTeExchangeCrawlsConcert(concertTe);
		activeEventActiveCrawl.setTeExchangeCrawlsOther(otherTe);
		
		activeEventActiveCrawl.setTnowExchangeCrawlsSports(sportTnow);
		activeEventActiveCrawl.setTnowExchangeCrawlsTheater(theaterTnow);
		activeEventActiveCrawl.setTnowExchangeCrawlsConcert(concertTnow);
		activeEventActiveCrawl.setTnowExchangeCrawlsOther(otherTnow);
		
		activeEventActiveCrawl.setFsExchangeCrawlsSports(sportFlSheet);
		activeEventActiveCrawl.setFsExchangeCrawlsTheater(theaterFlSheet);
		activeEventActiveCrawl.setFsExchangeCrawlsConcert(concertFlSheet);
		activeEventActiveCrawl.setFsExchangeCrawlsOther(totalFlSheet);
		
		activeEventActiveCrawl.setVsExchangeCrawlsSports(sportVs);
		activeEventActiveCrawl.setVsExchangeCrawlsTheater(theaterVs);
		activeEventActiveCrawl.setVsExchangeCrawlsConcert(concertVs);
		activeEventActiveCrawl.setVsExchangeCrawlsOther(otherVs);
		
	  	
		
		DAORegistry.getActiveEventAndActiveCrawlsDAO().save(activeEventActiveCrawl);
		
		
		try {			
			//String from = DAORegistry.getPropertyDAO().get("smtp.from").getValue();
			String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
			//String smtpHost="192.168.0.108";
			Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
			String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
			String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
			String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
			Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
			String transportProtocol = "";
			//String mailTo ="nkaliyannan@rightthisway.com";
			String mailTo = "amit.raut@rightthisway.com";//mailIdProperty.getBccList() + ",leor@rewardthefan.com";//"araut@rightthisway.com";
			//String mailTo ="achawan@rightthisway.com";//"pmehul@rightthisway.com";
			String mailFrom = mailIdProperty.getTmatmanagers();//"tmatmanagers@rightthisway.com";
		//	String mailFrom="tmatmanagers@rightthisway.com";
			/*String mailTo = "pmehul@rightthisway.com";
			String mailFrom = "tmatmanagers@rightthisway.com";*/
			String cc =null;//"pdhebar@rightthisway.com";//"marjun@rightthisway.com";
			String bcc ="tselvan@rightthisway.com";
			
			SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
					smtpUsername, smtpPassword, null, mailFrom, mailTo, cc, bcc, "TMAT statistics","mail-tmat-stats.html", map, "text/html",null);
			//System.out.println("Mail sent....");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	}
}