package com.admitone.tmat.download.event;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.mail.MailAttachment;
public class UnlinkedEventDownloader implements InitializingBean {
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}

	private void start() {
		TimerTask taskPerDay = new TimerScheduleUnlinkedEvent(mailIdProperty);
		Timer timerPerDay = new Timer();
		timerPerDay.schedule(taskPerDay, getStartTimeForTimerTwo(), 1000*60*60*24); //3600000 //60000//300000
	}
	 private final static int HOUR_TIMER_TWO = 7; //6;
	  private final static int MINUTE_TIMER_TWO = 30; //55;


	 private static Date getStartTimeForTimerTwo() {
		 
		    Calendar calendar = new GregorianCalendar();
		    
		    if(calendar.get(Calendar.HOUR_OF_DAY) > HOUR_TIMER_TWO || 
		    	(calendar.get(Calendar.HOUR_OF_DAY) == HOUR_TIMER_TWO && calendar.get(Calendar.MINUTE) >= MINUTE_TIMER_TWO)) {
		    	calendar.add(Calendar.DATE, 1);
		    }
		    
		    Calendar result = new GregorianCalendar(
		    	calendar.get(Calendar.YEAR),
		    	calendar.get(Calendar.MONTH),
		    	calendar.get(Calendar.DATE),
		    	HOUR_TIMER_TWO,
		    	MINUTE_TIMER_TWO
		    );
		    return result.getTime();
	 }
}
class TimerScheduleUnlinkedEvent extends TimerTask  {
	public TimerScheduleUnlinkedEvent(MailIdProperty mailIdProperty) {
		super();
		this.mailIdProperty = mailIdProperty;
	}
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	@Override
	public void run()  {
		System.out.println("Unlinked events Timer started:" + new Date());
		System.out.println("Time 7:30");
		Collection<Event> events=DAORegistry.getEventDAO().getAllEventByAdmitoneIdNull();
		String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
		Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
		String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
		String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
		String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
		Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
		String transportProtocol = "";
		String mailTo = mailIdProperty.getEcommerce();//"ecommerce@admitone.com";//ecommerce@admitone.com
		String mailFrom = mailIdProperty.getTmatmanagers();//"tmatmanagers@rightthisway.com";
		String cc = mailIdProperty.getBccList();//"araut@rightthisway.com,marjun@rightthisway.com";
		if(events==null)
		{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("numevents", "null");
			System.out.println("No Unlinked Events");
			try {
				System.out.println("mail with no data");
				SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
						smtpUsername, smtpPassword, null, mailFrom, mailTo, cc, null, "Unlinked TMAT Events","mail-emptyunlinkedevent.html", map, "text/html", null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
		FileOutputStream fop = null;
		File file;
		try {
			file = new File("\\\\Admit1\\Shared\\E-Commerce\\Eventstobeaddedtotmat\\UNLINKEDEVENT.csv");
			//file = new File("C:\\Arjun\\test\\FileDownload\\UNLINKEDEVENT.csv");//C:\Arjun\test\FileDownload
			fop = new FileOutputStream(file);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			// get the content in bytes
			fop.write("Event Id,Event,Event Date,Artist,Venue,Event Type \n".getBytes());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm");
			for (Event event : events) {
				String line = event.getId()  + ","+event.getName().replaceAll(",", "-")+"," + df.format(event.getLocalDate())+","+ event.getArtist().getName().replaceAll(",", "-")+"," +event.getVenue().getBuilding()+"," + event.getEventType()+"\n";
				fop.write(line.getBytes());
			}
			fop.flush();
			fop.close();
			System.out.println("Done");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("numevents", "null");
		//String filename = "C:\\Arjun\\test\\FileDownload\\UNLINKEDEVENT.csv";
		String filename = "\\\\Admit1\\Shared\\E-Commerce\\Eventstobeaddedtotmat\\UNLINKEDEVENT.csv";
		MailAttachment[] mailAttachments = new MailAttachment[1];
		try {
			mailAttachments[0] = new MailAttachment(IOUtils.toByteArray(new FileInputStream(filename)), "image/gif", "UNLINKEDEVENTS.csv");	
			System.out.println("mail with data");
			SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
					smtpUsername, smtpPassword, null, mailFrom, mailTo, cc, null, "Unlinked TMAT Events","mail-unlinkedevent.html", map, "text/html", mailAttachments);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
	}
