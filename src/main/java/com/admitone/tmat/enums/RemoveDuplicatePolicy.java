package com.admitone.tmat.enums;

public enum RemoveDuplicatePolicy {
	NONE, SIMPLE, REMOVE_NON_WORD_CHARACTERS, SMART, MANUAL, SUPER_SMART
}
