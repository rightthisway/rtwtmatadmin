package com.admitone.tmat.enums;

public enum CrawlPhase {
	STOPPED, EXTRACTION, FLUSH, EXPIRATION, DISPATCHED, REQUEUING
}
