package com.admitone.tmat.enums;

public enum PriceRounding {
	NONE, UP_TO_NEAREST_DOLLAR, DOWN_TO_NEAREST_DOLLAR, ROUND_TO_NEAREST_DOLLAR
}
