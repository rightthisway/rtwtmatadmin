package com.admitone.tmat.enums;

public enum EbayInventoryStatus {
	ACTIVE, DISABLED, INVALID, SUGGESTED, REPLACEMENT, DELETED
}
