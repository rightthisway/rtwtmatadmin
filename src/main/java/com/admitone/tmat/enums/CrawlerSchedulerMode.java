package com.admitone.tmat.enums;

public enum CrawlerSchedulerMode {
	ALWAYS_SCHEDULED, ALWAYS_UNSCHEDULED, SCHEDULED_UNSCHEDULED;
}
