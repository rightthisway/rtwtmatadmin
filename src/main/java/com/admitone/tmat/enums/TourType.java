package com.admitone.tmat.enums;

public enum TourType {
	SPORT, THEATER, CONCERT, OTHER
}
