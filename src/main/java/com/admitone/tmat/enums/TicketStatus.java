package com.admitone.tmat.enums;

public enum TicketStatus {
	ACTIVE, EXPIRED, SOLD, DISABLED
}
