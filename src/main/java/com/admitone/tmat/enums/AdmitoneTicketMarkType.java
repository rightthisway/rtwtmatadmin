package com.admitone.tmat.enums;

public enum AdmitoneTicketMarkType {
	MARK, UNMARK
}