package com.admitone.tmat.enums;

public enum UserAlertStatus {
	ACTIVE, DISABLED, EXPIRED, DELETED
}
