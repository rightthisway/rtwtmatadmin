package com.admitone.tmat.web;

import com.admitone.tmat.data.Tour1;
import com.admitone.tmat.enums.TourType;

/**
 * Class that holds all the fields of the Edit Tour Form.
 */
public class TourCommand {
	private Integer id;
	private Integer artistId;
	private String name;
	private String location;
	private TourType tourType;
	private Integer grandChildCategory;
	private Integer childCategory;
	private Integer tourCategory;
	
	public TourCommand() {		
	}
	
	public TourCommand(Tour1 tour) {
		id = tour.getId();
		artistId = tour.getArtistId();
		name = tour.getName();
		location = tour.getLocation();
		tourType = tour.getTourType();
		grandChildCategory=tour.getGrandChildTourCategory().getId();
		childCategory=tour.getGrandChildTourCategory().getChildTourCategory().getId();
		tourCategory=tour.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getId();
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getArtistId() {
		return artistId;
	}
	
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public TourType getTourType() {
		return tourType;
	}

	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}

	public Integer getGrandChildCategory() {
		return grandChildCategory;
	}

	public void setGrandChildCategory(Integer grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}

	public Integer getChildCategory() {
		return childCategory;
	}

	public void setChildCategory(Integer childCategory) {
		this.childCategory = childCategory;
	}

	public Integer getTourCategory() {
		return tourCategory;
	}

	public void setTourCategory(Integer tourCategory) {
		this.tourCategory = tourCategory;
	}	
}
