package com.admitone.tmat.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.dao.PropertyDAO;
import com.admitone.tmat.dao.hibernate.AuditTnApiCallLimitDAO;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.AuditTnApiCallLimit;
import com.admitone.tmat.data.AutoCatsCategoryTicket;
import com.admitone.tmat.data.AutoScoreBigExchangeEvent;
import com.admitone.tmat.data.AutoTNExchangeEvent;
import com.admitone.tmat.data.AutoVividSeatExchangeEvent;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.DateTimePop;
import com.admitone.tmat.data.DefaultAutoPricingAudit;
import com.admitone.tmat.data.DefaultExchangeMarkupAudit;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ExchangeEventAudit;
import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.HistoricalTicket;
import com.admitone.tmat.data.JKTZonesPricingCategoryTicket;
import com.admitone.tmat.data.LarryLastCategoryTicket;
import com.admitone.tmat.data.LarryLastTnCategoryTicket;
import com.admitone.tmat.data.LarryMiniTNExchangeEvent;
import com.admitone.tmat.data.LastFiveRowMCStubhubExchangeEvent;
import com.admitone.tmat.data.LastFiveRowMiniCategoryTicket;
import com.admitone.tmat.data.LastRowMiniCategoryTicket;
import com.admitone.tmat.data.LastRowMiniCatsError;
import com.admitone.tmat.data.LastRowMiniTNExchangeEvent;
import com.admitone.tmat.data.ManageBrokerAudit;
import com.admitone.tmat.data.MiniScoreBigExchangeEvent;
import com.admitone.tmat.data.MiniTNExchangeEvent;
import com.admitone.tmat.data.MiniVividSeatExchangeEvent;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TGCatsCategoryTicket;
import com.admitone.tmat.data.TGCatsError;
import com.admitone.tmat.data.TNError;
import com.admitone.tmat.data.TgCatsEbayListing;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TnnApiCallLimitTemp;
import com.admitone.tmat.data.ZonePricingTnCategoryTicket;
import com.admitone.tmat.data.ZonedLastRowMiniCategoryTicket;
import com.admitone.tmat.data.ZonedLastRowTnCategoryTicket;
import com.admitone.tmat.data.ZonesPricingCategoryTicket;
//import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.VWLastRowMiniCatsCategoryTicket;
import com.admitone.tmat.data.VWTGCatsCategoryTicket;
import com.admitone.tmat.data.VWVipCatsCategoryTicket;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VipAutoCategoryTicket;
import com.admitone.tmat.data.VipAutoCatsError;
import com.admitone.tmat.data.VipAutoScoreBigExchangeEvent;
import com.admitone.tmat.data.VipAutoTNExchangeEvent;
import com.admitone.tmat.data.VipAutoVividSeatExchangeEvent;
import com.admitone.tmat.data.VipCatsCategoryTicket;
import com.admitone.tmat.data.VipCatsError;
import com.admitone.tmat.data.VipMiniScoreBigExchangeEvent;
import com.admitone.tmat.data.VipMiniTNExchangeEvent;
import com.admitone.tmat.data.VipMiniVividSeatExchangeEvent;
import com.admitone.tmat.data.VwVipAutoCategoryTicket;
import com.admitone.tmat.data.ZonedLastRowMiniTNExchangeEvent;
import com.admitone.tmat.data.ZonesPricingTNExchangeEvent;
import com.admitone.tmat.pojo.ExchangeEventAutoPricing;
import com.admitone.tmat.util.ScoreBigListedEvent;
import com.admitone.tmat.util.TGScoreBigListedEvent;
import com.admitone.tmat.util.TGTNListedEvent;
import com.admitone.tmat.util.TGTickPickListedEvent;
import com.admitone.tmat.util.TGVividListedEvent;
import com.admitone.tmat.util.TNListedEvent;
import com.admitone.tmat.util.TickPickListedEvent;
import com.admitone.tmat.util.VividListedEvent;
import com.admitone.tmat.utils.AlphanumericSorting;

/*
 * Web Controller to handle pages related to the autoPricing
 */
public class AutoPricingController extends MultiActionController {

	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}

	public void getAutoCompleteGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");
		String brokerId = request.getParameter("brokerId");

		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);
		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(
				param);
		Collection<GrandChildTourCategory> grandChildCategories = DAORegistry
				.getGrandChildTourCategoryDAO().getGrandChildCategoriesByName(
						param);
		Collection<ChildTourCategory> childTourCategories = DAORegistry
				.getChildTourCategoryDAO().getChildTourCategoriesByName(param);

		if (artists == null && venues == null && grandChildCategories == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				response.getOutputStream().println(
						"ARTIST" + "|" + artist.getId() + "|" + artist.getName());
			}
		}
		if (venues != null) {
			for (Venue venue : venues) {
				response.getOutputStream().println(
						"VENUE" + "|" + venue.getId() + "|"
								+ venue.getBuilding());
			}
		}
		if (grandChildCategories != null) {
			for (GrandChildTourCategory grandChildCateogry : grandChildCategories) {
				response.getOutputStream().println(
						"GRANDCHILD" + "|" + grandChildCateogry.getId() + "|"
								+ grandChildCateogry.getName());
			}
		}

		if (childTourCategories != null) {
			for (ChildTourCategory childTourCategory : childTourCategories) {
				response.getOutputStream().println(
						"CHILD" + "|" + childTourCategory.getId() + "|"
								+ childTourCategory.getName());
			}
		}

	}

	public void getEventsByGrandChildAndArtistAndVenue(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JSONException {

		String artistId = request.getParameter("artistId");
		String venueId = request.getParameter("venueId");
		String grandChildId = request.getParameter("grandChildId");
		String childId = request.getParameter("childId");
		String brokerId = request.getParameter("brokerId");
		JSONArray jsonArray = new JSONArray();
		
		Collection<Event> events = null;
		if (brokerId != null && !brokerId.isEmpty()) {
			if (artistId != null) {
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(Integer.parseInt(artistId), Integer.parseInt(brokerId));
			}
			if (venueId != null) {
				events = DAORegistry.getEventDAO().getAllEventsByVenue(
						Integer.parseInt(venueId), Integer.parseInt(brokerId));
			}
			if (childId != null) {
				events = DAORegistry.getEventDAO().getAllEventsByChildCategory(
						Integer.parseInt(childId), Integer.parseInt(brokerId));
			}
			if (grandChildId != null) {
				events = DAORegistry.getEventDAO()
						.getAllEventsByGrandChildCategoryOrderdByEventDate(
								Integer.parseInt(grandChildId),
								Integer.parseInt(brokerId));
			}
		} else {
			if (artistId != null) {
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistId));
			}
			if (venueId != null) {
				events = DAORegistry.getEventDAO().getAllEventsByVenue(
						Integer.parseInt(venueId));
			}
			if (childId != null) {
				events = DAORegistry.getEventDAO().getAllEventsByChildCategory(
						Integer.parseInt(childId));
			}
			if (grandChildId != null) {
				events = DAORegistry.getEventDAO()
						.getAllEventsByGrandChildCategoryOrderdByEventDate(
								Integer.parseInt(grandChildId));
			}
		}

		if (events != null) {
			for (Event event : events) {

				/*
				 * if(event.getAdmitoneId() == null ||
				 * event.getVenueCategoryId() == null) { continue; }
				 */

				JSONObject jObject = new JSONObject();
				jObject.put("id", event.getId());
				jObject.put("name", event.getName());

				Format formatter = new SimpleDateFormat("MM/dd/yyyy");
				Date date = event.getLocalDate();
				String eventDate = "TBD";
				if (date != null) {
					eventDate = formatter.format(date);
				}

				formatter = new SimpleDateFormat("hh:mm aa");
				Time eventTime = event.getLocalTime();
				String time = "TBD";
				if (eventTime != null) {
					time = formatter.format(eventTime);
				}
				jObject.put("date", eventDate);
				jObject.put("time", time);
				jObject.put("venue", event.getVenue().getBuilding());
//				System.out.println("Event Venue Is:" + event.getVenue().getBuilding());
				// jObject.put("venue", event.venue.getcountry());

				jsonArray.put(jObject);

			}
		}
		IOUtils.write(jsonArray.toString().getBytes(),
				response.getOutputStream());

	}

	public ModelAndView loadManageBrokerEvents(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-broker-event");

		String[] eventStr = request.getParameterValues("events");
		String artistStr = request.getParameter("artist");

		String venueStr = request.getParameter("venue");
		String childStr = request.getParameter("Child");
		String grandChildStr = request.getParameter("grandChild");

		String brokerStatus = request.getParameter("brokerStatus");
		String brokerId = request.getParameter("brokerId");
		//Integer broker_id =Integer.parseInt(brokerId);
		
		
		String selectedEvent = "";

		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAll());
		mav.addObject("brokerId", request.getParameter("brokerId"));

		String action = request.getParameter("status");
		
		String info = "";
		// List<Event> events = null;
		Collection<com.admitone.tmat.data.Event> events = null;
		// Event event = null;
		// VenueCategory venueCategory=null;
		List<Integer> eventIds = new ArrayList<Integer>();
//		List<ExchangeEventAutoPricing> exchangeEventAutoPricingList = new ArrayList<ExchangeEventAutoPricing>();

		// try {

		if(artistStr!=null && !artistStr.isEmpty()){
			if(brokerId != null && !brokerId.isEmpty()) {
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(Integer.parseInt(artistStr),Integer.parseInt(brokerId));
	
			} else {
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
			}
			mav.addObject("selectedOption", "Artist");
			mav.addObject("selectedValue", (DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr))).getName());
		
		}else if(venueStr!=null && !venueStr.isEmpty()){
			if(brokerId != null && !brokerId.isEmpty()) {
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr),Integer.parseInt(brokerId));
			} else {
				events = DAORegistry.getEventDAO().getAllEventsByVenue(Integer.parseInt(venueStr));
			}
			mav.addObject("selectedOption", "Venue");
			mav.addObject("selectedValue", (DAORegistry.getVenueDAO().get(Integer.parseInt(venueStr))).getBuilding());
		
		} else if(grandChildStr != null && !grandChildStr.isEmpty()) {
			if(brokerId != null && !brokerId.isEmpty()) {
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr),Integer.parseInt(brokerId));
			} else {
				events = DAORegistry.getEventDAO().getAllEventsByGrandChildCategoryOrderdByEventDate(Integer.parseInt(grandChildStr));
			}
			mav.addObject("selectedOption", "GrandChild");
			mav.addObject("selectedValue", (DAORegistry.getGrandChildTourCategoryDAO().get(Integer.parseInt(grandChildStr))).getName());
			
		} else if(childStr != null && !childStr.isEmpty()) {
			if(brokerId != null && !brokerId.isEmpty()) {
				events = DAORegistry.getEventDAO().getAllEventsByChildCategory(Integer.parseInt(childStr),Integer.parseInt(brokerId));
			} else {
				events = DAORegistry.getEventDAO().getAllEventsByChildCategory(Integer.parseInt(childStr));
			}
			mav.addObject("selectedOption", "Child");
			mav.addObject("selectedValue", (DAORegistry.getChildTourCategoryDAO().get(Integer.parseInt(childStr))).getName());
		}
		Collection<Event> events_list = new ArrayList<Event>();
		if (eventStr != null && eventStr.length != 0) {
			for (String eventIdStr : eventStr) {
				if (eventIdStr.isEmpty()) {
					continue;
				}
				selectedEvent += eventIdStr + ",";
				eventIds.add(Integer.parseInt(eventIdStr));
				Event ev = DAORegistry.getEventDAO().get(
						Integer.parseInt(eventIdStr));
				if (ev != null)
					events_list.add(ev);
				String username = SecurityContextHolder.getContext()
						.getAuthentication().getName();
				Date now = new Date();
				ManageBrokerAudit managebrokeraudit = null;
				/*List<ManageBrokerAudit> managebrokerdata = DAORegistry
						.getManageBrokerAuditDAO()
						.getManageBrokerAuditByEventId(ev.getId());
				if (managebrokerdata.isEmpty()) {
					managebrokeraudit = new ManageBrokerAudit();
					managebrokeraudit.setEvent(ev);
					//managebrokeraudit.setEvent(ev.setEventId);
					managebrokeraudit.setBroker_id(ev.getBrokerId());
					managebrokeraudit.setBroker_status(ev.getBrokerStatus());
					managebrokeraudit.setUserName(username);
					managebrokeraudit.setCreatedDate(now);
					managebrokeraudit.setAction("Auto Created");
	} else {
					managebrokeraudit = managebrokerdata.get(0);

					managebrokeraudit.setEvent(ev);
					//managebrokeraudit.setEvent(ev.setEventId);
					managebrokeraudit.setBroker_id(ev.getBrokerId());
					managebrokeraudit.setBroker_status(ev.getBrokerStatus());
					// managebrokeraudit.setCreatedDate(now);

				}

				ManageBrokerAuditDAO managerDAO = DAORegistry
						.getManageBrokerAuditDAO();
				

				managerDAO.saveOrUpdate(managebrokeraudit);*/
			}
		}

		

		/*
		 * ManagerbrokeAudit managebrokeraudit ;
		 * 
		 * managebrokeraudit.setUsername(); managebrokeraudit.setAction("");
		 * managebrokeraudit
		 */
		if (action != null) {
			
			if (action.equals("update")) {

				Set<String> params = request.getParameterMap().keySet();
				List<Integer> eventId_arry = new ArrayList<Integer>();
				List<Integer> eventId_list = new ArrayList<Integer>();
				int i = 0, j = 0;

				String username = SecurityContextHolder.getContext()
						.getAuthentication().getName();
				Date now = new Date();
				ManageBrokerAudit managebrokeraudit = null;

				
				for (String param : params) {

					if (param.contains("checkbox_")) {

						
						String temp[] = param.split("_");
						eventId_arry.add(Integer.parseInt(temp[1]));

						
						i++;
					}

					if (param.contains("broker_")) {

						System.out.println("event list"
								+ param.contains("broker_") + "*************");
						String temp[] = param.split("_");
						eventId_list.add(Integer.parseInt(temp[1]));

						
						j++;

					}
				}

				Collection<com.admitone.tmat.data.Event> eventsToUpdate=null;
				for (Integer eventId : eventId_list) {

					if (eventId_arry.contains(eventId)) {
						// String venueCatId = temp[2];
						// Event event =
						// eventMap.get(Integer.parseInt(eventId));
						System.out.println(eventId + " Checked Event");

						Event upEvent = DAORegistry.getEventDAO()
								.get(eventId);

					
						Integer artistId= upEvent.getArtistId();
						if(brokerId != null && !brokerId.isEmpty())
						{
							eventsToUpdate = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(artistId,Integer.parseInt(brokerId));
						}
						else
						{
							eventsToUpdate = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
						}
						 
					/*	List<ManageBrokerAudit> managebrokeraudit = DAORegistry
								.getManageBrokerAuditDAO()
								.getManageBrokerAuditByEventId(upEvent.getId());
						//managebrokeraudit = managebrokerdata.get(0);
*/						
						
						 managebrokeraudit =new ManageBrokerAudit();

						int upBrokerId = upEvent.getBrokerId();
						String upBrokerStatus = upEvent.getBrokerStatus();

						

						String brokerstr = request.getParameter("broker_"
								+ eventId);
						int updatedBrokerId = Integer.parseInt(brokerstr);
						
						if (upBrokerId != updatedBrokerId) {
							upEvent.setBrokerId(updatedBrokerId);
							DAORegistry.getEventDAO().saveOrUpdate(upEvent);

							managebrokeraudit.setBrokerId(updatedBrokerId);
							managebrokeraudit.setUserName(username);
							managebrokeraudit.setCreatedDate(now);
							managebrokeraudit.setEventId(upEvent.getId());
							managebrokeraudit.setBrokerStatus(upBrokerStatus);
							managebrokeraudit.setAction("Event Level Updated");
							DAORegistry.getManageBrokerAuditDAO().saveOrUpdate(managebrokeraudit);

						}
						if (upBrokerStatus.equals("ACTIVE")) {
							upEvent.setBrokerStatus("ACTIVE");
							DAORegistry.getEventDAO().saveOrUpdate(upEvent);

							managebrokeraudit.setBrokerId(updatedBrokerId);
							managebrokeraudit.setBrokerStatus("ACTIVE");
							managebrokeraudit.setUserName(username);
							managebrokeraudit.setCreatedDate(now);
							managebrokeraudit.setEventId(upEvent.getId());
							managebrokeraudit.setAction("Event Level Updated");
							DAORegistry.getManageBrokerAuditDAO().saveOrUpdate(
									managebrokeraudit);

						}
						
						info ="Events added successfully.";
					}

					else {

						System.out.println(eventId);
						Event upEvent = DAORegistry.getEventDAO()
								.get(eventId);

						
						Integer artistId= upEvent.getArtistId();
						if(brokerId != null && !brokerId.isEmpty())
						{
						 eventsToUpdate  = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(artistId,Integer.parseInt(brokerId));
						}
						else
						{
							eventsToUpdate  = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
						}
						//List<ManageBrokerAudit> managebrokeraudit =new ManageBrokerAudit();
						/*List<ManageBrokerAudit> managebrokeraudit = DAORegistry
								.getManageBrokerAuditDAO()
								.getManageBrokerAuditByEventId(upEvent.getId());
						managebrokeraudit =new ManageBrokerAudit();

*/
						managebrokeraudit =new ManageBrokerAudit();
						int upBrokerId = upEvent.getBrokerId();
						String upBrokerStatus = upEvent.getBrokerStatus();

						

						String brokerstr = request.getParameter("broker_"
								+ eventId);
						int updatedBrokerId = Integer.parseInt(brokerstr);
						
						if (upBrokerId == updatedBrokerId) {
							;
						} else {
							upEvent.setBrokerId(updatedBrokerId);
							DAORegistry.getEventDAO().saveOrUpdate(upEvent);
							
							managebrokeraudit.setBrokerId(updatedBrokerId);
							managebrokeraudit.setUserName(username);
							managebrokeraudit.setCreatedDate(now);
							managebrokeraudit.setBrokerStatus(upBrokerStatus);
							managebrokeraudit.setEventId(upEvent.getId());
							managebrokeraudit.setAction("Event Level Updated");
							DAORegistry.getManageBrokerAuditDAO().saveOrUpdate(
									managebrokeraudit);
						}
						if (upBrokerStatus.equals("DELETED")) {
							;
						} else {
							upEvent.setBrokerStatus("DELETED");
							DAORegistry.getEventDAO().saveOrUpdate(upEvent);

							managebrokeraudit.setBrokerId(updatedBrokerId);
							managebrokeraudit.setBrokerStatus("DELETE");
							// managebrokeraudit.setBroker_status("DELETE");
							managebrokeraudit.setUserName(username);
							managebrokeraudit.setCreatedDate(now);
							managebrokeraudit.setEventId(upEvent.getId());
							managebrokeraudit.setAction("Event Level Removed");
							DAORegistry.getManageBrokerAuditDAO().saveOrUpdate(
									managebrokeraudit);

						}
						
						info ="Events added successfully.";
					}

				}
				List<Integer> eventIds1 = new ArrayList<Integer>();
				String[] eventStr1 = request.getParameterValues("events");
				Collection<Event> events_list1 = new ArrayList<Event>();
				if (eventStr1 != null && eventStr1.length != 0) {
					for (String eventIdStr : eventStr1) {
						if (eventIdStr.isEmpty()) {
							continue;
						}
						selectedEvent += eventIdStr + ",";
						eventIds1.add(Integer.parseInt(eventIdStr));
						//mav.addObject("selectedValue", (DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr))).getTour().getName());
						Event ev = DAORegistry.getEventDAO().get(
								Integer.parseInt(eventIdStr));

						if (ev != null)
							events_list1.add(ev);
					}
				}
				// mav.addObject("autoprieventslst",events_list1);
				// mav.addObject("events", events);
				// mav.addObject("eventStr", selectedEvent);
				// return mav;
				//mav.addObject("brokers", DAORegistry.getBrokerDAO().getAll());
				mav.addObject("venue", venueStr);
				mav.addObject("grandChild", grandChildStr);
				mav.addObject("child", childStr);
				mav.addObject("artist", artistStr);
				mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers());
				//mav.addObject("broker", brokerIdStr);
				mav.addObject("autoprieventslst", events_list1);
				mav.addObject("events", eventsToUpdate);
				//mav.addObject("selectedOption", "Artist");
				mav.addObject("info", info);
				mav.addObject("eventStr", selectedEvent);
				return mav;
			}

		}
		// }

		/*
		 * if((eventStr!=null && eventStr.length!=0) && (brokerStatus != null &&
		 * !brokerStatus.isEmpty()) && (brokerId != null &&
		 * !brokerId.isEmpty())){
		 * 
		 * // ArrayList<Integer> eventIds =new ArrayList<Integer>();
		 * 
		 * 
		 * if(eventStr!=null && eventStr.length!=0){ for(String eventIdStr
		 * :eventStr){ if(eventIdStr.isEmpty()){ continue; } selectedEvent +=
		 * eventIdStr +","; eventIds.add(Integer.parseInt(eventIdStr)); } }
		 * if(brokerStatus.equalsIgnoreCase("DELETED")){
		 * DAORegistry.getEventDAO().updateBrokerEvents(eventIds); }else{
		 * //DAORegistry.getEventDAO().get(eventIds, Integer.parseInt(brokerId),
		 * brokerStatus); }
		 * 
		 * 
		 * Get brokers int brokerid=Integer.parseInt(brokerId) if(brokerid==0)
		 * mav.addObject("brokers", DAORegistry.getBrokerDAO().getAll()); else
		 * mav.addObject("brokers",
		 * DAORegistry.getBrokerDAO().getBrokerById(brokerid)); }
		 */
		//mav.addObject("brokers", DAORegistry.getBrokerDAO().getAll());
		mav.addObject("autoprieventslst", events_list);
		mav.addObject("events", events);
		mav.addObject("eventStr", selectedEvent);
		mav.addObject("venue", venueStr);
		mav.addObject("grandChild", grandChildStr);
		mav.addObject("child", childStr);
		mav.addObject("artist", artistStr);
		return mav;
	}


	public ModelAndView loadExchangeEventAutoPricing(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-exchange-event-auto-pricing");
		String artistStr = request.getParameter("artist");
		String[] eventStr = request.getParameterValues("events");
		String venueStr = request.getParameter("venue");
		String grandChildStr = request.getParameter("grandChild");
		String childStr = request.getParameter("child");
		String selectedEvent = "";
		
		String brokerIdStr = request.getParameter("brokerId");
		Integer brokerId = null;
		
		if(brokerIdStr != null && !brokerIdStr.isEmpty()) {
			brokerId = Integer.parseInt(brokerIdStr);
		}
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers());
		mav.addObject("broker", brokerIdStr);

		// total frequent events
		List<Object[]> list = DAORegistry.getVividExchangeEventDAO()
				.getAllFrequentEvents();
		mav.addObject("totalFrequentEvents", list.size());

		Property sportshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.sports");
		Property sportneartermdisplayoption = DAORegistry.getPropertyDAO().get(
				"exchange.default.neartermdisplayoption.sports");

		Property concertshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.concerts");
		Property concertneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.concerts");

		Property theatershippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.theater");
		Property theaterneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.theater");

		Property vegasshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.lasvegas");
		Property vegasneartermdisplayoption = DAORegistry.getPropertyDAO().get(
				"exchange.default.neartermdisplayoption.lasvegas");

		Property othersshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.default");
		Property othersneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.default");

		Integer defaultSportsShippingMethod = Integer
				.valueOf(sportshippingmethod.getValue()), defaultSportsNearTermDisplay = Integer
				.valueOf(sportneartermdisplayoption.getValue());

		Integer defaultConcertsShippingMethod = Integer
				.valueOf(concertshippingmethod.getValue()), defaultConcertsNearTermDisplay = Integer
				.valueOf(concertneartermdisplayoption.getValue());

		Integer defaultTheaterShippingMethod = Integer
				.valueOf(theatershippingmethod.getValue()), defaultTheaterNearTermDisplay = Integer
				.valueOf(theaterneartermdisplayoption.getValue());

		Integer defaultVegasShippingMethod = Integer
				.valueOf(vegasshippingmethod.getValue()), defaultVegasNearTermDisplay = Integer
				.valueOf(vegasneartermdisplayoption.getValue());

		Integer defaultOthersShippingMethod = Integer
				.valueOf(othersshippingmethod.getValue()), defaultOthersNearTermDisplay = Integer
				.valueOf(othersneartermdisplayoption.getValue());

		String action = request.getParameter("action");
		String info = "";
		// List<Event> events = null;
		Collection<com.admitone.tmat.data.Event> events = null;
		// Event event = null;
		// VenueCategory venueCategory=null;
		List<Integer> eventIds = new ArrayList<Integer>();
		List<ExchangeEventAutoPricing> exchangeEventAutoPricingList = new ArrayList<ExchangeEventAutoPricing>();

		try {

			if (artistStr != null && !artistStr.isEmpty()) {
				if(brokerId != null) {
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistIdAndBrokerId(Integer.parseInt(artistStr),brokerId);
				} else {
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(Integer.parseInt(artistStr));
				}
				
				mav.addObject("selectedOption", "Artist");
				mav.addObject("selectedValue", (DAORegistry.getArtistDAO().get(Integer.parseInt(artistStr))).getName());

			} else if (venueStr != null && !venueStr.isEmpty()) {
				if(brokerId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByVenue(
							Integer.parseInt(venueStr),brokerId);
				} else {
					events = DAORegistry.getEventDAO().getAllEventsByVenue(
							Integer.parseInt(venueStr));
				}
				mav.addObject("selectedOption", "Venue");
				mav.addObject("selectedValue", (DAORegistry.getVenueDAO()
						.get(Integer.parseInt(venueStr))).getBuilding());

			} else if (grandChildStr != null && !grandChildStr.isEmpty()) {
				if(brokerId != null) {
					events = DAORegistry.getEventDAO()
							.getAllEventsByGrandChildCategoryOrderdByEventDate(
								Integer.parseInt(grandChildStr),brokerId);
				} else {
					events = DAORegistry.getEventDAO()
					.getAllEventsByGrandChildCategoryOrderdByEventDate(
						Integer.parseInt(grandChildStr));
				}
				mav.addObject("selectedOption", "GrandChild");
				mav.addObject("selectedValue", (DAORegistry
						.getGrandChildTourCategoryDAO().get(Integer
						.parseInt(grandChildStr))).getName());
			}else if (childStr != null && !childStr.isEmpty()) {
				if(brokerId != null) {
					events = DAORegistry.getEventDAO().getAllEventsByChildCategory(Integer.parseInt(childStr),brokerId);
				} else {
					events = DAORegistry.getEventDAO().getAllEventsByChildCategory(Integer.parseInt(childStr));
				}
				mav.addObject("selectedOption", "Child");
				mav.addObject("selectedValue", (DAORegistry.getChildTourCategoryDAO().get(Integer.parseInt(childStr))).getName());
			} 

			if (eventStr != null && eventStr.length != 0) {
				for (String eventIdStr : eventStr) {
					if (eventIdStr.isEmpty()) {
						continue;
					}
					selectedEvent += eventIdStr + ",";
					eventIds.add(Integer.parseInt(eventIdStr));
				}
			}

			if (action != null) {
				Map<Integer, Event> eventMap = new HashMap<Integer, com.admitone.tmat.data.Event>();

				if (null != events) {
					for (Event event : events) {
						eventMap.put(event.getId(), event);
					}
				}
				Map<Integer, AutoTNExchangeEvent> existingAutoTnExchangeEventMap = new HashMap<Integer, AutoTNExchangeEvent>();
				Map<Integer, AutoTNExchangeEvent> deletedAutoTnExchangeEventMap = new HashMap<Integer, AutoTNExchangeEvent>();
				Map<Integer, MiniTNExchangeEvent> existingMiniTnExchangeEventMap = new HashMap<Integer, MiniTNExchangeEvent>();
				Map<Integer, MiniTNExchangeEvent> deletedMiniTnExchangeEventMap = new HashMap<Integer, MiniTNExchangeEvent>();
				Map<Integer, VipMiniTNExchangeEvent> existingVipMiniTnExchangeEventMap = new HashMap<Integer, VipMiniTNExchangeEvent>();
				Map<Integer, VipMiniTNExchangeEvent> deletedVipMiniTnExchangeEventMap = new HashMap<Integer, VipMiniTNExchangeEvent>();
				Map<Integer, VipAutoTNExchangeEvent> existingVipAutoTnExchangeEventMap = new HashMap<Integer, VipAutoTNExchangeEvent>();
				Map<Integer, VipAutoTNExchangeEvent> deletedVipAutoTnExchangeEventMap = new HashMap<Integer, VipAutoTNExchangeEvent>();
				Map<Integer, LastRowMiniTNExchangeEvent> existingLastRowMiniTnExchangeEventMap = new HashMap<Integer, LastRowMiniTNExchangeEvent>();
				Map<Integer, LastRowMiniTNExchangeEvent> deletedLastRowMiniTnExchangeEventMap = new HashMap<Integer, LastRowMiniTNExchangeEvent>();

				Map<Integer, AutoVividSeatExchangeEvent> existingAutoVividSeatExchangeEventMap = new HashMap<Integer, AutoVividSeatExchangeEvent>();
				Map<Integer, AutoVividSeatExchangeEvent> deletedAutoVividSeatExchangeEventMap = new HashMap<Integer, AutoVividSeatExchangeEvent>();
				Map<Integer, MiniVividSeatExchangeEvent> existingMiniVividSeatExchangeEventMap = new HashMap<Integer, MiniVividSeatExchangeEvent>();
				Map<Integer, MiniVividSeatExchangeEvent> deletedMiniVividSeatExchangeEventMap = new HashMap<Integer, MiniVividSeatExchangeEvent>();
				Map<Integer, VipMiniVividSeatExchangeEvent> existingVipMiniVividSeatExchangeEventMap = new HashMap<Integer, VipMiniVividSeatExchangeEvent>();
				Map<Integer, VipMiniVividSeatExchangeEvent> deletedVipMiniVividSeatExchangeEventMap = new HashMap<Integer, VipMiniVividSeatExchangeEvent>();
				Map<Integer, VipAutoVividSeatExchangeEvent> existingVipAutoVividSeatExchangeEventMap = new HashMap<Integer, VipAutoVividSeatExchangeEvent>();
				Map<Integer, VipAutoVividSeatExchangeEvent> deletedVipAutoVividSeatExchangeEventMap = new HashMap<Integer, VipAutoVividSeatExchangeEvent>();

				/*
				 * Map<Integer, AutoTickPickExchangeEvent>
				 * existingAutoTickPickExchangeEventMap = new
				 * HashMap<Integer,AutoTickPickExchangeEvent>(); Map<Integer,
				 * AutoTickPickExchangeEvent>
				 * deletedAutoTickPickExchangeEventMap = new
				 * HashMap<Integer,AutoTickPickExchangeEvent>(); Map<Integer,
				 * MiniTickPickExchangeEvent>
				 * existingMiniTickPickExchangeEventMap = new
				 * HashMap<Integer,MiniTickPickExchangeEvent>(); Map<Integer,
				 * MiniTickPickExchangeEvent>
				 * deletedMiniTickPickExchangeEventMap = new
				 * HashMap<Integer,MiniTickPickExchangeEvent>(); Map<Integer,
				 * VipMiniTickPickExchangeEvent>
				 * existingVipMiniTickPickExchangeEventMap = new
				 * HashMap<Integer,VipMiniTickPickExchangeEvent>(); Map<Integer,
				 * VipMiniTickPickExchangeEvent>
				 * deletedVipMiniTickPickExchangeEventMap = new
				 * HashMap<Integer,VipMiniTickPickExchangeEvent>();
				 * 
				 * Map<Integer, EbayExchangeEvent> existingEbayExchangeEvents =
				 * new HashMap<Integer,EbayExchangeEvent>(); Map<Integer,
				 * EbayExchangeEvent> deletedEbayExchangeEvents = new
				 * HashMap<Integer,EbayExchangeEvent>();
				 */

				Map<Integer, AutoScoreBigExchangeEvent> existingAutoScoreBigExchangeEventMap = new HashMap<Integer, AutoScoreBigExchangeEvent>();
				Map<Integer, AutoScoreBigExchangeEvent> deletedAutoScoreBigExchangeEventMap = new HashMap<Integer, AutoScoreBigExchangeEvent>();
				Map<Integer, MiniScoreBigExchangeEvent> existingMiniScoreBigExchangeEventMap = new HashMap<Integer, MiniScoreBigExchangeEvent>();
				Map<Integer, MiniScoreBigExchangeEvent> deletedMiniScoreBigExchangeEventMap = new HashMap<Integer, MiniScoreBigExchangeEvent>();
				Map<Integer, VipMiniScoreBigExchangeEvent> existingVipMiniScoreBigExchangeEventMap = new HashMap<Integer, VipMiniScoreBigExchangeEvent>();
				Map<Integer, VipMiniScoreBigExchangeEvent> deletedVipMiniScoreBigExchangeEventMap = new HashMap<Integer, VipMiniScoreBigExchangeEvent>();
				Map<Integer, VipAutoScoreBigExchangeEvent> existingVipAutoScoreBigExchangeEventMap = new HashMap<Integer, VipAutoScoreBigExchangeEvent>();
				Map<Integer, VipAutoScoreBigExchangeEvent> deletedVipAutoScoreBigExchangeEventMap = new HashMap<Integer, VipAutoScoreBigExchangeEvent>();

				Map<Integer, LarryMiniTNExchangeEvent> existingLarryMiniTNExchangeEvents = new HashMap<Integer, LarryMiniTNExchangeEvent>();
				Map<Integer, LarryMiniTNExchangeEvent> deletedLarryMiniTNExchangeEvents = new HashMap<Integer, LarryMiniTNExchangeEvent>();

				Map<Integer, LastFiveRowMCStubhubExchangeEvent> existingLast5MCStubExchangeEvents = new HashMap<Integer, LastFiveRowMCStubhubExchangeEvent>();
				Map<Integer, LastFiveRowMCStubhubExchangeEvent> deletedLast5MCStubExchangeEvents = new HashMap<Integer, LastFiveRowMCStubhubExchangeEvent>();

				Map<Integer, ZonesPricingTNExchangeEvent> existingZonesPricingTNExchangeEvents = new HashMap<Integer, ZonesPricingTNExchangeEvent>();
				Map<Integer, ZonesPricingTNExchangeEvent> deletedZonesPricingTNExchangeEvents = new HashMap<Integer, ZonesPricingTNExchangeEvent>();
				
				Map<Integer, ZonedLastRowMiniTNExchangeEvent> existingZonedLastRowMiniTNExchangeEvents = new HashMap<Integer, ZonedLastRowMiniTNExchangeEvent>();
				Map<Integer, ZonedLastRowMiniTNExchangeEvent> deletedZonedLastRowMiniTNExchangeEvents = new HashMap<Integer, ZonedLastRowMiniTNExchangeEvent>();
				// Map<Integer, ExchangeFourExchangeEvent>
				// existingExchangeFourExchangeEvents = new
				// HashMap<Integer,ExchangeFourExchangeEvent>();
				// Map<Integer, ExchangeFourExchangeEvent>
				// deletedExchangeFourExchangeEvents = new
				// HashMap<Integer,ExchangeFourExchangeEvent>();

				// Map<Integer, ExchangeFiveExchangeEvent>
				// existingExchangeFiveExchangeEvents = new
				// HashMap<Integer,ExchangeFiveExchangeEvent>();
				// Map<Integer, ExchangeFiveExchangeEvent>
				// deletedExchangeFiveExchangeEvents = new
				// HashMap<Integer,ExchangeFiveExchangeEvent>();

				if (!eventIds.isEmpty()) {
					Collection<AutoTNExchangeEvent> existingAutoTNExchangeEventList = DAORegistry
							.getAutoTNExchangeEventDAO()
							.getAllActiveAutoTNExchangeEventsByEvent(eventIds,
									true);
					for (AutoTNExchangeEvent tn : existingAutoTNExchangeEventList) {
						existingAutoTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<AutoTNExchangeEvent> deletedAutoTNExchangeEventsList = DAORegistry
							.getAutoTNExchangeEventDAO()
							.getAllDeletedAutoTNExchangeEventsByEvent(eventIds,
									true);
					for (AutoTNExchangeEvent tn : deletedAutoTNExchangeEventsList) {
						deletedAutoTnExchangeEventMap.put(
								tn.getEvent().getId(), tn);
					}
					Collection<MiniTNExchangeEvent> existingMiniTNExchangeEventList = DAORegistry
							.getMiniTNExchangeEventDAO()
							.getAllActiveMiniTNExchangeEventsByEvent(eventIds,
									true);
					for (MiniTNExchangeEvent tn : existingMiniTNExchangeEventList) {
						existingMiniTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<MiniTNExchangeEvent> deletedMiniTNExchangeEventsList = DAORegistry
							.getMiniTNExchangeEventDAO()
							.getAllDeletedMiniTNExchangeEventsByEvent(eventIds,
									true);
					for (MiniTNExchangeEvent tn : deletedMiniTNExchangeEventsList) {
						deletedMiniTnExchangeEventMap.put(
								tn.getEvent().getId(), tn);
					}
					Collection<VipMiniTNExchangeEvent> existingVipMiniTNExchangeEventList = DAORegistry
							.getVipMiniTNExchangeEventDAO()
							.getAllActiveVipMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniTNExchangeEvent tn : existingVipMiniTNExchangeEventList) {
						existingVipMiniTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<VipMiniTNExchangeEvent> deletedVipMiniTNExchangeEventsList = DAORegistry
							.getVipMiniTNExchangeEventDAO()
							.getAllDeletedVipMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniTNExchangeEvent tn : deletedVipMiniTNExchangeEventsList) {
						deletedVipMiniTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<VipAutoTNExchangeEvent> existingVipAutoTNExchangeEventList = DAORegistry
							.getVipAutoTNExchangeEventDAO()
							.getAllActiveVipAutoTNExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoTNExchangeEvent tn : existingVipAutoTNExchangeEventList) {
						existingVipAutoTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<VipAutoTNExchangeEvent> deletedVipAutoTNExchangeEventsList = DAORegistry
							.getVipAutoTNExchangeEventDAO()
							.getAllDeletedVipAutoTNExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoTNExchangeEvent tn : deletedVipAutoTNExchangeEventsList) {
						deletedVipAutoTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<LastRowMiniTNExchangeEvent> existingLastRowMiniTNExchangeEventList = DAORegistry
							.getLastRowMiniTNExchangeEventDAO()
							.getAllActiveLastRowMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (LastRowMiniTNExchangeEvent tn : existingLastRowMiniTNExchangeEventList) {
						existingLastRowMiniTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<LastRowMiniTNExchangeEvent> deletedLastRowMiniTNExchangeEventsList = DAORegistry
							.getLastRowMiniTNExchangeEventDAO()
							.getAllDeletedLastRowMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (LastRowMiniTNExchangeEvent tn : deletedLastRowMiniTNExchangeEventsList) {
						deletedLastRowMiniTnExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}

					Collection<AutoVividSeatExchangeEvent> existingAutoVividSeatExchangeEventsList = DAORegistry
							.getAutoVividSeatExchangeEventDAO()
							.getAllActiveAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (AutoVividSeatExchangeEvent tn : existingAutoVividSeatExchangeEventsList) {
						existingAutoVividSeatExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<AutoVividSeatExchangeEvent> deletedAutoVividSeatExchangeEventsList = DAORegistry
							.getAutoVividSeatExchangeEventDAO()
							.getAllDeletedAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (AutoVividSeatExchangeEvent tn : deletedAutoVividSeatExchangeEventsList) {
						deletedAutoVividSeatExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<MiniVividSeatExchangeEvent> existingMiniVividSeatExchangeEventsList = DAORegistry
							.getMiniVividSeatExchangeEventDAO()
							.getAllActiveMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (MiniVividSeatExchangeEvent tn : existingMiniVividSeatExchangeEventsList) {
						existingMiniVividSeatExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<MiniVividSeatExchangeEvent> deletedMiniVividSeatExchangeEventsList = DAORegistry
							.getMiniVividSeatExchangeEventDAO()
							.getAllDeletedMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (MiniVividSeatExchangeEvent tn : deletedMiniVividSeatExchangeEventsList) {
						deletedMiniVividSeatExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<VipMiniVividSeatExchangeEvent> existingVipMiniVividSeatExchangeEventsList = DAORegistry
							.getVipMiniVividSeatExchangeEventDAO()
							.getAllActiveVipMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniVividSeatExchangeEvent tn : existingVipMiniVividSeatExchangeEventsList) {
						existingVipMiniVividSeatExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}
					Collection<VipMiniVividSeatExchangeEvent> deletedVipMiniVividSeatExchangeEventsList = DAORegistry
							.getVipMiniVividSeatExchangeEventDAO()
							.getAllDeletedVipMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniVividSeatExchangeEvent tn : deletedVipMiniVividSeatExchangeEventsList) {
						deletedVipMiniVividSeatExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}

					Collection<VipAutoVividSeatExchangeEvent> existingVipAutoVividSeatExchangeEventsList = DAORegistry
							.getVipAutoVividSeatExchangeEventDAO()
							.getAllActiveVipAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoVividSeatExchangeEvent tn : existingVipAutoVividSeatExchangeEventsList) {
						existingVipAutoVividSeatExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}
					Collection<VipAutoVividSeatExchangeEvent> deletedVipAutoVividSeatExchangeEventsList = DAORegistry
							.getVipAutoVividSeatExchangeEventDAO()
							.getAllDeletedVipAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoVividSeatExchangeEvent tn : deletedVipAutoVividSeatExchangeEventsList) {
						deletedVipAutoVividSeatExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}

					/*
					 * Collection<AutoTickPickExchangeEvent>
					 * existingAutoTickPickExchangeEventsList =
					 * DAORegistry.getAutoTickPickExchangeEventDAO
					 * ().getAllActiveAutoTickPickExchangeEventsByEvent
					 * (eventIds,true); for(AutoTickPickExchangeEvent tn:
					 * existingAutoTickPickExchangeEventsList){
					 * existingAutoTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<AutoTickPickExchangeEvent
					 * >deletedAutoTickPickExchangeEventsList =
					 * DAORegistry.getAutoTickPickExchangeEventDAO
					 * ().getAllDeletedAutoTickPickExchangeEventsByEvent
					 * (eventIds,true); for(AutoTickPickExchangeEvent tn:
					 * deletedAutoTickPickExchangeEventsList){
					 * deletedAutoTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<MiniTickPickExchangeEvent>
					 * existingMiniTickPickExchangeEventsList =
					 * DAORegistry.getMiniTickPickExchangeEventDAO
					 * ().getAllActiveMiniTickPickExchangeEventsByEvent
					 * (eventIds,true); for(MiniTickPickExchangeEvent tn:
					 * existingMiniTickPickExchangeEventsList){
					 * existingMiniTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<MiniTickPickExchangeEvent
					 * >deletedMiniTickPickExchangeEventsList =
					 * DAORegistry.getMiniTickPickExchangeEventDAO
					 * ().getAllDeletedMiniTickPickExchangeEventsByEvent
					 * (eventIds,true); for(MiniTickPickExchangeEvent tn:
					 * deletedMiniTickPickExchangeEventsList){
					 * deletedMiniTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<VipMiniTickPickExchangeEvent>
					 * existingVipMiniTickPickExchangeEventsList =
					 * DAORegistry.getVipMiniTickPickExchangeEventDAO
					 * ().getAllActiveVipMiniTickPickExchangeEventsByEvent
					 * (eventIds,true); for(VipMiniTickPickExchangeEvent tn:
					 * existingVipMiniTickPickExchangeEventsList){
					 * existingVipMiniTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<VipMiniTickPickExchangeEvent
					 * >deletedVipMiniTickPickExchangeEventsList =
					 * DAORegistry.getVipMiniTickPickExchangeEventDAO
					 * ().getAllDeletedVipMiniTickPickExchangeEventsByEvent
					 * (eventIds,true); for(VipMiniTickPickExchangeEvent tn:
					 * deletedVipMiniTickPickExchangeEventsList){
					 * deletedVipMiniTickPickExchangeEventMap
					 * .put(tn.getEvent().getId(), tn); }
					 * 
					 * Collection<EbayExchangeEvent>
					 * existingEbayExchangeEventsList =
					 * DAORegistry.getEbayExchangeEventDAO
					 * ().getAllActiveEbayExchangeEventsByEvent(eventIds,true);
					 * for(EbayExchangeEvent tn:
					 * existingEbayExchangeEventsList){
					 * existingEbayExchangeEvents.put(tn.getEvent().getId(),
					 * tn); } Collection<EbayExchangeEvent>
					 * deletedEbayExchangeEventsList =
					 * DAORegistry.getEbayExchangeEventDAO
					 * ().getAllDeletedEbayExchangeEventsByEvent(eventIds,true);
					 * for(EbayExchangeEvent tn: deletedEbayExchangeEventsList){
					 * deletedEbayExchangeEvents.put(tn.getEvent().getId(), tn);
					 * }
					 */

					Collection<AutoScoreBigExchangeEvent> existingAutoScoreBigExchangeEventsList = DAORegistry
							.getAutoScoreBigExchangeEventDAO()
							.getAllActiveAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (AutoScoreBigExchangeEvent tn : existingAutoScoreBigExchangeEventsList) {
						existingAutoScoreBigExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<AutoScoreBigExchangeEvent> deletedAutoScoreBigExchangeEventsList = DAORegistry
							.getAutoScoreBigExchangeEventDAO()
							.getAllDeletedAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (AutoScoreBigExchangeEvent tn : deletedAutoScoreBigExchangeEventsList) {
						deletedAutoScoreBigExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<MiniScoreBigExchangeEvent> existingMiniScoreBigExchangeEventsList = DAORegistry
							.getMiniScoreBigExchangeEventDAO()
							.getAllActiveMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (MiniScoreBigExchangeEvent tn : existingMiniScoreBigExchangeEventsList) {
						existingMiniScoreBigExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<MiniScoreBigExchangeEvent> deletedMiniScoreBigExchangeEventsList = DAORegistry
							.getMiniScoreBigExchangeEventDAO()
							.getAllDeletedMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (MiniScoreBigExchangeEvent tn : deletedMiniScoreBigExchangeEventsList) {
						deletedMiniScoreBigExchangeEventMap.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<VipMiniScoreBigExchangeEvent> existingVipMiniScoreBigExchangeEventsList = DAORegistry
							.getVipMiniScoreBigExchangeEventDAO()
							.getAllActiveVipMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniScoreBigExchangeEvent tn : existingVipMiniScoreBigExchangeEventsList) {
						existingVipMiniScoreBigExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}
					Collection<VipMiniScoreBigExchangeEvent> deletedVipMiniScoreBigExchangeEventsList = DAORegistry
							.getVipMiniScoreBigExchangeEventDAO()
							.getAllDeletedVipMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (VipMiniScoreBigExchangeEvent tn : deletedVipMiniScoreBigExchangeEventsList) {
						deletedVipMiniScoreBigExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}

					Collection<VipAutoScoreBigExchangeEvent> existingVipAutoScoreBigExchangeEventsList = DAORegistry
							.getVipAutoScoreBigExchangeEventDAO()
							.getAllActiveVipAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoScoreBigExchangeEvent tn : existingVipAutoScoreBigExchangeEventsList) {
						existingVipAutoScoreBigExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}
					Collection<VipAutoScoreBigExchangeEvent> deletedVipAutoScoreBigExchangeEventsList = DAORegistry
							.getVipAutoScoreBigExchangeEventDAO()
							.getAllDeletedVipAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					for (VipAutoScoreBigExchangeEvent tn : deletedVipAutoScoreBigExchangeEventsList) {
						deletedVipAutoScoreBigExchangeEventMap.put(tn
								.getEvent().getId(), tn);
					}

					Collection<LarryMiniTNExchangeEvent> existingLarryMiniTNExchangeEventsList = DAORegistry
							.getLarryMiniTNExchangeEventDAO()
							.getAllActiveLarryMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (LarryMiniTNExchangeEvent tn : existingLarryMiniTNExchangeEventsList) {
						existingLarryMiniTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<LarryMiniTNExchangeEvent> deletedLarryMiniTNExchangeEventsList = DAORegistry
							.getLarryMiniTNExchangeEventDAO()
							.getAllDeletedLarryMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (LarryMiniTNExchangeEvent tn : deletedLarryMiniTNExchangeEventsList) {
						deletedLarryMiniTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<LastFiveRowMCStubhubExchangeEvent> existingLast5MCStubExchangeEventsList = DAORegistry
							.getLastFiveRowMCStubhubExchangeEventDAO()
							.getAllActiveLastFiveRowMCStubhubExchangeEventsByEvent(
									eventIds, true);
					for (LastFiveRowMCStubhubExchangeEvent tn : existingLast5MCStubExchangeEventsList) {
						existingLast5MCStubExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<LastFiveRowMCStubhubExchangeEvent> deletedLast5MCStubExchangeEventsList = DAORegistry
							.getLastFiveRowMCStubhubExchangeEventDAO()
							.getAllDeletedLastFiveRowMCStubhubExchangeEventsByEvent(
									eventIds, true);
					for (LastFiveRowMCStubhubExchangeEvent tn : deletedLast5MCStubExchangeEventsList) {
						deletedLast5MCStubExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					
					
					Collection<ZonesPricingTNExchangeEvent> existingZonesPricingTNExchangeEventsList = DAORegistry
					.getZonesPricingTNExchangeEventDAO()
					.getAllActiveZonesPricingTNExchangeEventsByEvent(
							eventIds, true);
					for (ZonesPricingTNExchangeEvent tn : existingZonesPricingTNExchangeEventsList) {
						existingZonesPricingTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<ZonesPricingTNExchangeEvent> deletedZonesPricingTNExchangeEventsList = DAORegistry
							.getZonesPricingTNExchangeEventDAO()
							.getAllDeletedZonesPricingTNExchangeEventsByEvent(
									eventIds, true);
					for (ZonesPricingTNExchangeEvent tn : deletedZonesPricingTNExchangeEventsList) {
						deletedZonesPricingTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					
					Collection<ZonedLastRowMiniTNExchangeEvent> existingZonedLastRowMiniTNExchangeEventsList = DAORegistry
					.getZonedLastRowMiniTNExchangeEventDAO()
					.getAllActiveZonedLastRowMiniTNExchangeEventsByEvent(
							eventIds, true);
					for (ZonedLastRowMiniTNExchangeEvent tn : existingZonedLastRowMiniTNExchangeEventsList) {
						existingZonedLastRowMiniTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					Collection<ZonedLastRowMiniTNExchangeEvent> deletedZonedLastRowMiniTNExchangeEventsList = DAORegistry
							.getZonedLastRowMiniTNExchangeEventDAO()
							.getAllDeletedZonedLastRowMiniTNExchangeEventsByEvent(
									eventIds, true);
					for (ZonedLastRowMiniTNExchangeEvent tn : deletedZonedLastRowMiniTNExchangeEventsList) {
						deletedZonedLastRowMiniTNExchangeEvents.put(tn.getEvent()
								.getId(), tn);
					}
					/*
					 * Collection<ExchangeFourExchangeEvent>
					 * existingExchangeFourExchangeEventsList =
					 * DAORegistry.getExchangeFourExchangeEventDAO
					 * ().getAllActiveExchangeFourExchangeEventsByEvent
					 * (eventIds,true); for(ExchangeFourExchangeEvent tn:
					 * existingExchangeFourExchangeEventsList){
					 * existingExchangeFourExchangeEvents
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<ExchangeFourExchangeEvent
					 * >deletedExchangeFourExchangeEventsList =
					 * DAORegistry.getExchangeFourExchangeEventDAO
					 * ().getAllDeletedExchangeFourExchangeEventsByEvent
					 * (eventIds,true); for(ExchangeFourExchangeEvent tn:
					 * deletedExchangeFourExchangeEventsList){
					 * deletedExchangeFourExchangeEvents
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<ExchangeFiveExchangeEvent>
					 * existingExchangeFiveExchangeEventsList =
					 * DAORegistry.getExchangeFiveExchangeEventDAO
					 * ().getAllActiveExchangeFiveExchangeEventsByEvent
					 * (eventIds,true); for(ExchangeFiveExchangeEvent tn:
					 * existingExchangeFiveExchangeEventsList){
					 * existingExchangeFiveExchangeEvents
					 * .put(tn.getEvent().getId(), tn); }
					 * Collection<ExchangeFiveExchangeEvent
					 * >deletedExchangeFiveExchangeEventsList =
					 * DAORegistry.getExchangeFiveExchangeEventDAO
					 * ().getAllDeletedExchangeFiveExchangeEventsByEvent
					 * (eventIds,true); for(ExchangeFiveExchangeEvent tn:
					 * deletedExchangeFiveExchangeEventsList){
					 * deletedExchangeFiveExchangeEvents
					 * .put(tn.getEvent().getId(), tn); }
					 */

				}

				if (action.equals("update")) {
					Set<String> params = request.getParameterMap().keySet();
					Collection<AutoTNExchangeEvent> newAutoTNExchangeEventsList = new ArrayList<AutoTNExchangeEvent>();
					Collection<MiniTNExchangeEvent> newMiniTNExchangeEventsList = new ArrayList<MiniTNExchangeEvent>();
					Collection<VipMiniTNExchangeEvent> newVipMiniTNExchangeEventsList = new ArrayList<VipMiniTNExchangeEvent>();
					Collection<VipAutoTNExchangeEvent> newVipAutoTNExchangeEventsList = new ArrayList<VipAutoTNExchangeEvent>();
					Collection<LastRowMiniTNExchangeEvent> newLastRowMiniTNExchangeEventsList = new ArrayList<LastRowMiniTNExchangeEvent>();

					Collection<AutoTNExchangeEvent> existingAutoTNExchangeEventsList = new ArrayList<AutoTNExchangeEvent>();
					Collection<MiniTNExchangeEvent> existingMiniTNExchangeEventsList = new ArrayList<MiniTNExchangeEvent>();
					Collection<VipMiniTNExchangeEvent> existingVipMiniTNExchangeEventsList = new ArrayList<VipMiniTNExchangeEvent>();
					Collection<VipAutoTNExchangeEvent> existingVipAutoTNExchangeEventsList = new ArrayList<VipAutoTNExchangeEvent>();
					Collection<LastRowMiniTNExchangeEvent> existingLastRowMiniTNExchangeEventsList = new ArrayList<LastRowMiniTNExchangeEvent>();

					Collection<AutoTNExchangeEvent> removeAutoTNExchangeEventsList = new ArrayList<AutoTNExchangeEvent>();
					Collection<MiniTNExchangeEvent> removeMiniTNExchangeEventsList = new ArrayList<MiniTNExchangeEvent>();
					Collection<VipMiniTNExchangeEvent> removeVipMiniTNExchangeEventsList = new ArrayList<VipMiniTNExchangeEvent>();
					Collection<VipAutoTNExchangeEvent> removeVipAutoTNExchangeEventsList = new ArrayList<VipAutoTNExchangeEvent>();
					Collection<LastRowMiniTNExchangeEvent> removeLastRowMiniTNExchangeEventsList = new ArrayList<LastRowMiniTNExchangeEvent>();

					Collection<AutoVividSeatExchangeEvent> newAutoVividSeatExchangeEventsList = new ArrayList<AutoVividSeatExchangeEvent>();
					Collection<MiniVividSeatExchangeEvent> newMiniVividSeatExchangeEventsList = new ArrayList<MiniVividSeatExchangeEvent>();
					Collection<VipMiniVividSeatExchangeEvent> newVipMiniVividSeatExchangeEventsList = new ArrayList<VipMiniVividSeatExchangeEvent>();
					Collection<VipAutoVividSeatExchangeEvent> newVipAutoVividSeatExchangeEventsList = new ArrayList<VipAutoVividSeatExchangeEvent>();

					Collection<AutoVividSeatExchangeEvent> existingAutoVividSeatExchangeEventsList = new ArrayList<AutoVividSeatExchangeEvent>();
					Collection<MiniVividSeatExchangeEvent> existingMiniVividSeatExchangeEventsList = new ArrayList<MiniVividSeatExchangeEvent>();
					Collection<VipMiniVividSeatExchangeEvent> existingVipMiniVividSeatExchangeEventsList = new ArrayList<VipMiniVividSeatExchangeEvent>();
					Collection<VipAutoVividSeatExchangeEvent> existingVipAutoVividSeatExchangeEventsList = new ArrayList<VipAutoVividSeatExchangeEvent>();

					Collection<AutoVividSeatExchangeEvent> removeAutoVividSeatExchangeEventsList = new ArrayList<AutoVividSeatExchangeEvent>();
					Collection<MiniVividSeatExchangeEvent> removeMiniVividSeatExchangeEventsList = new ArrayList<MiniVividSeatExchangeEvent>();
					Collection<VipMiniVividSeatExchangeEvent> removeVipMiniVividSeatExchangeEventsList = new ArrayList<VipMiniVividSeatExchangeEvent>();
					Collection<VipAutoVividSeatExchangeEvent> removeVipAutoVividSeatExchangeEventsList = new ArrayList<VipAutoVividSeatExchangeEvent>();

					// Collection<TickPickExchangeEvent>
					// newTickPickExchangeEventsList = new
					// ArrayList<TickPickExchangeEvent>();
					// Collection<TickPickExchangeEvent>
					// existingTickPickExchangeEventsList = new
					// ArrayList<TickPickExchangeEvent>();
					// Collection<TickPickExchangeEvent>
					// removeTickPickExchangeEventsList = new
					// ArrayList<TickPickExchangeEvent>();

					/*
					 * Collection<AutoTickPickExchangeEvent>
					 * newAutoTickPickExchangeEventsList = new
					 * ArrayList<AutoTickPickExchangeEvent>();
					 * Collection<MiniTickPickExchangeEvent>
					 * newMiniTickPickExchangeEventsList = new
					 * ArrayList<MiniTickPickExchangeEvent>();
					 * Collection<VipMiniTickPickExchangeEvent>
					 * newVipMiniTickPickExchangeEventsList = new
					 * ArrayList<VipMiniTickPickExchangeEvent>();
					 * 
					 * Collection<AutoTickPickExchangeEvent>
					 * existingAutoTickPickExchangeEventsList = new
					 * ArrayList<AutoTickPickExchangeEvent>();
					 * Collection<MiniTickPickExchangeEvent>
					 * existingMiniTickPickExchangeEventsList = new
					 * ArrayList<MiniTickPickExchangeEvent>();
					 * Collection<VipMiniTickPickExchangeEvent>
					 * existingVipMiniTickPickExchangeEventsList = new
					 * ArrayList<VipMiniTickPickExchangeEvent>();
					 * 
					 * Collection<AutoTickPickExchangeEvent>
					 * removeAutoTickPickExchangeEventsList = new
					 * ArrayList<AutoTickPickExchangeEvent>();
					 * Collection<MiniTickPickExchangeEvent>
					 * removeMiniTickPickExchangeEventsList = new
					 * ArrayList<MiniTickPickExchangeEvent>();
					 * Collection<VipMiniTickPickExchangeEvent>
					 * removeVipMiniTickPickExchangeEventsList = new
					 * ArrayList<VipMiniTickPickExchangeEvent>();
					 * 
					 * Collection<EbayExchangeEvent> newEbayExchangeEventsList =
					 * new ArrayList<EbayExchangeEvent>();
					 * Collection<EbayExchangeEvent>
					 * existingEbayExchangeEventsList = new
					 * ArrayList<EbayExchangeEvent>();
					 * Collection<EbayExchangeEvent>
					 * removeEbayExchangeEventsList = new
					 * ArrayList<EbayExchangeEvent>();
					 */

					// Collection<ScoreBigExchangeEvent>
					// newScoreBigExchangeEventsList = new
					// ArrayList<ScoreBigExchangeEvent>();
					// Collection<ScoreBigExchangeEvent>
					// existingScoreBigExchangeEventsList = new
					// ArrayList<ScoreBigExchangeEvent>();
					// Collection<ScoreBigExchangeEvent>
					// removeScoreBigExchangeEventsList = new
					// ArrayList<ScoreBigExchangeEvent>();

					Collection<AutoScoreBigExchangeEvent> newAutoScoreBigExchangeEventsList = new ArrayList<AutoScoreBigExchangeEvent>();
					Collection<MiniScoreBigExchangeEvent> newMiniScoreBigExchangeEventsList = new ArrayList<MiniScoreBigExchangeEvent>();
					Collection<VipMiniScoreBigExchangeEvent> newVipMiniScoreBigExchangeEventsList = new ArrayList<VipMiniScoreBigExchangeEvent>();
					Collection<VipAutoScoreBigExchangeEvent> newVipAutoScoreBigExchangeEventsList = new ArrayList<VipAutoScoreBigExchangeEvent>();

					Collection<AutoScoreBigExchangeEvent> existingAutoScoreBigExchangeEventsList = new ArrayList<AutoScoreBigExchangeEvent>();
					Collection<MiniScoreBigExchangeEvent> existingMiniScoreBigExchangeEventsList = new ArrayList<MiniScoreBigExchangeEvent>();
					Collection<VipMiniScoreBigExchangeEvent> existingVipMiniScoreBigExchangeEventsList = new ArrayList<VipMiniScoreBigExchangeEvent>();
					Collection<VipAutoScoreBigExchangeEvent> existingVipAutoScoreBigExchangeEventsList = new ArrayList<VipAutoScoreBigExchangeEvent>();

					Collection<AutoScoreBigExchangeEvent> removeAutoScoreBigExchangeEventsList = new ArrayList<AutoScoreBigExchangeEvent>();
					Collection<MiniScoreBigExchangeEvent> removeMiniScoreBigExchangeEventsList = new ArrayList<MiniScoreBigExchangeEvent>();
					Collection<VipMiniScoreBigExchangeEvent> removeVipMiniScoreBigExchangeEventsList = new ArrayList<VipMiniScoreBigExchangeEvent>();
					Collection<VipAutoScoreBigExchangeEvent> removeVipAutoScoreBigExchangeEventsList = new ArrayList<VipAutoScoreBigExchangeEvent>();

					Collection<LarryMiniTNExchangeEvent> newLarryMiniTNExchangeEventsList = new ArrayList<LarryMiniTNExchangeEvent>();
					Collection<LarryMiniTNExchangeEvent> existingLarryMiniTNExchangeEventsList = new ArrayList<LarryMiniTNExchangeEvent>();
					Collection<LarryMiniTNExchangeEvent> removeLarryMiniTNExchangeEventsList = new ArrayList<LarryMiniTNExchangeEvent>();

					Collection<LastFiveRowMCStubhubExchangeEvent> newLast5MCStubExchangeEventsList = new ArrayList<LastFiveRowMCStubhubExchangeEvent>();
					Collection<LastFiveRowMCStubhubExchangeEvent> existingLast5MCStubExchangeEventsList = new ArrayList<LastFiveRowMCStubhubExchangeEvent>();
					Collection<LastFiveRowMCStubhubExchangeEvent> removeLast5MCStubExchangeEventsList = new ArrayList<LastFiveRowMCStubhubExchangeEvent>();
					
					Collection<ZonesPricingTNExchangeEvent> newZonesPricingTNExchangeEventsList = new ArrayList<ZonesPricingTNExchangeEvent>();
					Collection<ZonesPricingTNExchangeEvent> existingZonesPricingTNExchangeEventsList = new ArrayList<ZonesPricingTNExchangeEvent>();
					Collection<ZonesPricingTNExchangeEvent> removeZonesPricingTNExchangeEventsList = new ArrayList<ZonesPricingTNExchangeEvent>();
					
					Collection<ZonedLastRowMiniTNExchangeEvent> newZonedLastRowMiniTNExchangeEventsList = new ArrayList<ZonedLastRowMiniTNExchangeEvent>();
					Collection<ZonedLastRowMiniTNExchangeEvent> existingZonedLastRowMiniTNExchangeEventsList = new ArrayList<ZonedLastRowMiniTNExchangeEvent>();
					Collection<ZonedLastRowMiniTNExchangeEvent> removeZonedLastRowMiniTNExchangeEventsList = new ArrayList<ZonedLastRowMiniTNExchangeEvent>();

					// Collection<ExchangeFourExchangeEvent>
					// newExchangeFourExchangeEventsList = new
					// ArrayList<ExchangeFourExchangeEvent>();
					// Collection<ExchangeFourExchangeEvent>
					// existingExchangeFourExchangeEventsList = new
					// ArrayList<ExchangeFourExchangeEvent>();
					// Collection<ExchangeFourExchangeEvent>
					// removeExchangeFourExchangeEventsList = new
					// ArrayList<ExchangeFourExchangeEvent>();

					// Collection<ExchangeFiveExchangeEvent>
					// newExchangeFiveExchangeEventsList = new
					// ArrayList<ExchangeFiveExchangeEvent>();
					// Collection<ExchangeFiveExchangeEvent>
					// existingExchangeFiveExchangeEventsList = new
					// ArrayList<ExchangeFiveExchangeEvent>();
					// Collection<ExchangeFiveExchangeEvent>
					// removeExchangeFiveExchangeEventsList = new
					// ArrayList<ExchangeFiveExchangeEvent>();

					Map<Integer, ExchangeEventAudit> autoPricingAudits = new HashMap<Integer, ExchangeEventAudit>();

					ExchangeEventAudit autoPricingAudit = null;
					String username = SecurityContextHolder.getContext()
							.getAuthentication().getName();
					Date now = new Date();
					boolean existingEventFlag = false;
					Map<Integer, Venue> tmatVenueMap = new HashMap<Integer, Venue>();
					Collection<Venue> tmatVenues = DAORegistry.getVenueDAO()
							.getAll();
					for (Venue venue : tmatVenues) {
						tmatVenueMap.put(venue.getId(), venue);
					}

					for (String param : params) {
						if (param.contains("checkbox_")) {
							String temp[] = param.split("_");
							String eventId = temp[1];
							// String venueCatId = temp[2];
							Event event = eventMap.get(Integer
									.parseInt(eventId));

							String lowerMarkupStr = request
									.getParameter("lowerMarkup_" + eventId);
							String upperMarkupStr = request
									.getParameter("upperMarkup_" + eventId);
							String lowerShippingFeesStr = request
									.getParameter("lowerShippingFees_"
											+ eventId);
							String upperShippingFeesStr = request
									.getParameter("upperShippingFees_"
											+ eventId);
							String rptFactorStr = request
									.getParameter("rptFactor_" + eventId);
							String autoExposure = request
									.getParameter("auto_exposure_" + eventId);
							String miniExposure = request
									.getParameter("mini_exposure_" + eventId);
							String vipMiniExposure = request
									.getParameter("vip_mini_exposure_"
											+ eventId);
							String vipAutoExposure = request
									.getParameter("vip_auto_exposure_"
											+ eventId);
							String lastRowMiniExposure = request
									.getParameter("lastrow_mini_exposure_"
											+ eventId);
							String lastFiveRowMiniExposure = request
									.getParameter("lastfiverow_mini_exposure_"
											+ eventId);
							
							String zonesPricingTNExposure = request
							.getParameter("zones_pricing_exposure_"
									+ eventId);
							
							String zonedLastRowMiniTNExposure = request
							.getParameter("zoned_lastrowmini_exposure_"
									+ eventId);

							String shippingMethodStr = request
									.getParameter("shipping_method_" + eventId);
							String nearTermDisplayOptionStr = request
									.getParameter("near_term_display_option_"
											+ eventId);
							String shippingDaysStr = request
							.getParameter("shipping_days_" + eventId);

							String priceBreakupStr = request
									.getParameter("priceBreakup_" + eventId);
							String allowSectionRangeStr = request
									.getParameter("allowSectionRange_"
											+ eventId);
							String isFrequentStr = request
									.getParameter("isFrequentEvent_" + eventId);
							String isTnExchangeStr = request
									.getParameter("isTicketNetworkEvent_"
											+ eventId);
							String isVividSeatstStr = request
									.getParameter("isVividSeatsEvent_"
											+ eventId);
							// String isTickPickStr =
							// request.getParameter("isTickPickEvent_" +
							// eventId);
							// String isEbayExchangeStr =
							// request.getParameter("isEbayEvent_" + eventId);
							String isScoreBigExchangeStr = request
									.getParameter("isScoreBigEvent_" + eventId);
							String isLarryMiniTNExchangeStr = request
									.getParameter("isLarryMiniTNEvent_"
											+ eventId);
							String isStubhubExchangeStr = request
									.getParameter("isStubhubEvent_" + eventId);// Stubhub
																				// exchange
																				// only
																				// for
																				// last
																				// five
																				// row
																				// minicats
																				// project
							
							String isZonesPricingTNExchangeStr = request
							.getParameter("isZonesPricingTNEvent_" + eventId);//Zones Pricing Project.
							
							String isZonedLastRowMiniTNExchangeStr = request
							.getParameter("isZonedLastRowMiniTNEvent_" + eventId);//Zones Pricing Project.
							
							// String isExFourExchangeStr =
							// request.getParameter("isExchangeFourEvent_" +
							// eventId);
							// String isExFiveExchangeStr =
							// request.getParameter("isExchangeFiveEvent_" +
							// eventId);

							Boolean isFrequentEvent = (isFrequentStr == null || isFrequentStr
									.isEmpty()) ? false : true;
							Boolean allowSectionRange = (allowSectionRangeStr == null || allowSectionRangeStr
									.isEmpty()) ? false : true;
							Boolean isTnExchange = (isTnExchangeStr == null || isTnExchangeStr
									.isEmpty()) ? false : true;
							Boolean isVividSeats = (isVividSeatstStr == null || isVividSeatstStr
									.isEmpty()) ? false : true;
							// Boolean isTickPick = (isTickPickStr==null ||
							// isTickPickStr.isEmpty())?false:true;
							// Boolean isEbayExchange = (isEbayExchangeStr==null
							// || isEbayExchangeStr.isEmpty())?false:true;

							Boolean isScoreBigExchange = (isScoreBigExchangeStr == null || isScoreBigExchangeStr
									.isEmpty()) ? false : true;
							Boolean isLarryMiniTNExchange = (isLarryMiniTNExchangeStr == null || isLarryMiniTNExchangeStr
									.isEmpty()) ? false : true;
							Boolean isStubhubExchange = (isStubhubExchangeStr == null || isStubhubExchangeStr
									.isEmpty()) ? false : true;
							
							Boolean isZonesPricingTNExchange = (isZonesPricingTNExchangeStr == null || isZonesPricingTNExchangeStr
									.isEmpty()) ? false : true;
							
							Boolean isZonedLastRowMiniTNExchange = (isZonedLastRowMiniTNExchangeStr == null || isZonedLastRowMiniTNExchangeStr
									.isEmpty()) ? false : true;
							
							// Boolean isExFourExchange =
							// (isExFourExchangeStr==null ||
							// isExFourExchangeStr.isEmpty())?false:true;
							// Boolean isExFiveExchange =
							// (isExFiveExchangeStr==null ||
							// isExFiveExchangeStr.isEmpty())?false:true;

							Integer shippingMethod = null, nearTermDisplayOption = null,shippingDays=null;
							/*
							 * String parentCategory =
							 * event.getTour().getGrandChildTourCategory
							 * ().getChildTourCategory
							 * ().getTourCategory().getName();
							 * 
							 * if(parentCategory.equalsIgnoreCase("Sports")){
							 * shippingMethod=defaultSportsShippingMethod;
							 * nearTermDisplayOption
							 * =defaultSportsNearTermDisplay; }else
							 * if(parentCategory.equalsIgnoreCase("Concerts")){
							 * shippingMethod=defaultConcertsShippingMethod;
							 * nearTermDisplayOption
							 * =defaultConcertsNearTermDisplay; }else
							 * if(parentCategory.equalsIgnoreCase("Theater")){
							 * shippingMethod=defaultTheaterShippingMethod;
							 * nearTermDisplayOption
							 * =defaultTheaterNearTermDisplay; }else
							 * if(parentCategory.equalsIgnoreCase("Default")){
							 * shippingMethod=defaultOthersShippingMethod;
							 * nearTermDisplayOption
							 * =defaultOthersNearTermDisplay; }else
							 * if(parentCategory.equalsIgnoreCase("Las Vegas")){
							 * shippingMethod=defaultVegasShippingMethod;
							 * nearTermDisplayOption
							 * =defaultVegasNearTermDisplay; }
							 */

							Double lowerMarkup = null, upperMarkup = null, lowerShippingFees = null, upperShippingFees = null, rptFactor = null, priceBreakup = null;

							if (nearTermDisplayOptionStr != null
									&& !nearTermDisplayOptionStr.isEmpty()) {
								nearTermDisplayOption = Integer
										.parseInt(nearTermDisplayOptionStr);
							}
							if (shippingMethodStr != null
									&& !shippingMethodStr.isEmpty()) {
								shippingMethod = Integer
										.parseInt(shippingMethodStr);
							}
							
							if (shippingDaysStr != null
									&& !shippingDaysStr.isEmpty()) {
								shippingDays = Integer
										.parseInt(shippingDaysStr);
							}

							if (lowerMarkupStr != null
									&& !lowerMarkupStr.isEmpty()) {
								lowerMarkup = Double
										.parseDouble(lowerMarkupStr);
							}
							if (upperMarkupStr != null
									&& !upperMarkupStr.isEmpty()) {
								upperMarkup = Double
										.parseDouble(upperMarkupStr);
							}
							if (lowerShippingFeesStr != null
									&& !lowerShippingFeesStr.isEmpty()) {
								lowerShippingFees = Double
										.parseDouble(lowerShippingFeesStr);
							}
							if (upperShippingFeesStr != null
									&& !upperShippingFeesStr.isEmpty()) {
								upperShippingFees = Double
										.parseDouble(upperShippingFeesStr);
							}
							if (rptFactorStr != null && !rptFactorStr.isEmpty()) {
								rptFactor = Double.parseDouble(rptFactorStr);
							}
							if (priceBreakupStr != null
									&& !priceBreakupStr.isEmpty()) {
								priceBreakup = Double
										.parseDouble(priceBreakupStr);
							}

							existingEventFlag = false;
							String oldExchanges = "", newExchanges = "";
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Create");
							autoPricingAudit.setEvent(event);
							autoPricingAudit.setDefaultExchangeEventAudit(
									rptFactor, priceBreakup, lowerMarkup,
									upperMarkup, lowerShippingFees,
									upperShippingFees, isFrequentEvent,
									shippingMethod, nearTermDisplayOption,
									allowSectionRange,shippingDays);
							autoPricingAudit.setAutoExposure(autoExposure);
							autoPricingAudit.setMiniExposure(miniExposure);
							autoPricingAudit
									.setVipMiniExposure(vipMiniExposure);
							autoPricingAudit
									.setVipAutoExposure(vipAutoExposure);
							autoPricingAudit
									.setLastRowMiniExposure(lastRowMiniExposure);
							autoPricingAudit
									.setLastFiveRowMiniExposure(lastFiveRowMiniExposure);
							
							autoPricingAudit
							.setLastFiveRowMiniExposure(lastFiveRowMiniExposure);

							autoPricingAudit.setZonesPricingExposure(zonesPricingTNExposure);
							
							autoPricingAudit.setZonedLastRowMiniExposure(zonedLastRowMiniTNExposure);
							
							// (rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent)

							if (isTnExchange) {
								newExchanges = newExchanges + ",TNE";
								AutoTNExchangeEvent autoTnExchangeEvent = existingAutoTnExchangeEventMap
										.remove(event.getId());
								if (autoTnExchangeEvent == null) {
									autoTnExchangeEvent = deletedAutoTnExchangeEventMap
											.remove(event.getId());
									if (null == autoTnExchangeEvent) {
										autoTnExchangeEvent = new AutoTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",TNE";
								}
								autoTnExchangeEvent.setEvent(event);
								autoTnExchangeEvent.setAutoPricingValues(
										autoExposure, rptFactor, priceBreakup,
										lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								autoTnExchangeEvent.setStatus("ACTIVE");
								newAutoTNExchangeEventsList
										.add(autoTnExchangeEvent);

								MiniTNExchangeEvent miniTnExchangeEvent = existingMiniTnExchangeEventMap
										.remove(event.getId());
								if (miniTnExchangeEvent == null) {
									miniTnExchangeEvent = deletedMiniTnExchangeEventMap
											.remove(event.getId());
									if (null == miniTnExchangeEvent) {
										miniTnExchangeEvent = new MiniTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",TNE";
								}
								miniTnExchangeEvent.setEvent(event);
								miniTnExchangeEvent.setAutoPricingValues(
										miniExposure, rptFactor, priceBreakup,
										lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								miniTnExchangeEvent.setStatus("ACTIVE");
								newMiniTNExchangeEventsList
										.add(miniTnExchangeEvent);

								VipMiniTNExchangeEvent vipMiniTnExchangeEvent = existingVipMiniTnExchangeEventMap
										.remove(event.getId());
								if (vipMiniTnExchangeEvent == null) {
									vipMiniTnExchangeEvent = deletedVipMiniTnExchangeEventMap
											.remove(event.getId());
									if (null == vipMiniTnExchangeEvent) {
										vipMiniTnExchangeEvent = new VipMiniTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",TNE";
								}
								vipMiniTnExchangeEvent.setEvent(event);
								vipMiniTnExchangeEvent.setAutoPricingValues(
										vipMiniExposure, rptFactor,
										priceBreakup, lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								vipMiniTnExchangeEvent.setStatus("ACTIVE");
								newVipMiniTNExchangeEventsList
										.add(vipMiniTnExchangeEvent);

								VipAutoTNExchangeEvent vipAutoTnExchangeEvent = existingVipAutoTnExchangeEventMap
										.remove(event.getId());
								if (vipAutoTnExchangeEvent == null) {
									vipAutoTnExchangeEvent = deletedVipAutoTnExchangeEventMap
											.remove(event.getId());
									if (null == vipAutoTnExchangeEvent) {
										vipAutoTnExchangeEvent = new VipAutoTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",TNE";
								}
								vipAutoTnExchangeEvent.setEvent(event);
								vipAutoTnExchangeEvent.setAutoPricingValues(
										vipAutoExposure, rptFactor,
										priceBreakup, lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								vipAutoTnExchangeEvent.setStatus("ACTIVE");
								newVipAutoTNExchangeEventsList
										.add(vipAutoTnExchangeEvent);

								LastRowMiniTNExchangeEvent lastRowMiniTnExchangeEvent = existingLastRowMiniTnExchangeEventMap
										.remove(event.getId());
								if (lastRowMiniTnExchangeEvent == null) {
									lastRowMiniTnExchangeEvent = deletedLastRowMiniTnExchangeEventMap
											.remove(event.getId());
									if (null == lastRowMiniTnExchangeEvent) {
										lastRowMiniTnExchangeEvent = new LastRowMiniTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",TNE";
								}
								lastRowMiniTnExchangeEvent.setEvent(event);
								lastRowMiniTnExchangeEvent
										.setAutoPricingValues(
												lastRowMiniExposure, rptFactor,
												priceBreakup, lowerMarkup,
												upperMarkup, lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								lastRowMiniTnExchangeEvent.setStatus("ACTIVE");
								newLastRowMiniTNExchangeEventsList
										.add(lastRowMiniTnExchangeEvent);

							} else {
								AutoTNExchangeEvent autoTNExchangeEvent = existingAutoTnExchangeEventMap
										.remove(event.getId());
								if (autoTNExchangeEvent != null) {
									autoTNExchangeEvent.setStatus("DELETE");
									removeAutoTNExchangeEventsList
											.add(autoTNExchangeEvent);
								}
								MiniTNExchangeEvent miniTNExchangeEvent = existingMiniTnExchangeEventMap
										.remove(event.getId());
								if (miniTNExchangeEvent != null) {
									miniTNExchangeEvent.setStatus("DELETE");
									removeMiniTNExchangeEventsList
											.add(miniTNExchangeEvent);
								}
								VipMiniTNExchangeEvent vipMiniTNExchangeEvent = existingVipMiniTnExchangeEventMap
										.remove(event.getId());
								if (vipMiniTNExchangeEvent != null) {
									vipMiniTNExchangeEvent.setStatus("DELETE");
									removeVipMiniTNExchangeEventsList
											.add(vipMiniTNExchangeEvent);
								}
								VipAutoTNExchangeEvent vipAutoTNExchangeEvent = existingVipAutoTnExchangeEventMap
										.remove(event.getId());
								if (vipAutoTNExchangeEvent != null) {
									vipAutoTNExchangeEvent.setStatus("DELETE");
									removeVipAutoTNExchangeEventsList
											.add(vipAutoTNExchangeEvent);
								}
								LastRowMiniTNExchangeEvent lastRowMiniTNExchangeEvent = existingLastRowMiniTnExchangeEventMap
										.remove(event.getId());
								if (lastRowMiniTNExchangeEvent != null) {
									lastRowMiniTNExchangeEvent
											.setStatus("DELETE");
									removeLastRowMiniTNExchangeEventsList
											.add(lastRowMiniTNExchangeEvent);
								}
							}

							if (isVividSeats) {
								newExchanges = newExchanges + ",VividSeatE";
								AutoVividSeatExchangeEvent autoVividSeatExchangeEvent = existingAutoVividSeatExchangeEventMap
										.remove(event.getId());
								if (autoVividSeatExchangeEvent == null) {
									autoVividSeatExchangeEvent = deletedAutoVividSeatExchangeEventMap
											.remove(event.getId());
									if (null == autoVividSeatExchangeEvent) {
										autoVividSeatExchangeEvent = new AutoVividSeatExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",VividSeatE";
								}
								autoVividSeatExchangeEvent.setEvent(event);
								autoVividSeatExchangeEvent
										.setAutoPricingValues(autoExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								autoVividSeatExchangeEvent.setStatus("ACTIVE");
								newAutoVividSeatExchangeEventsList
										.add(autoVividSeatExchangeEvent);

								MiniVividSeatExchangeEvent miniVividSeatExchangeEvent = existingMiniVividSeatExchangeEventMap
										.remove(event.getId());
								if (miniVividSeatExchangeEvent == null) {
									miniVividSeatExchangeEvent = deletedMiniVividSeatExchangeEventMap
											.remove(event.getId());
									if (null == miniVividSeatExchangeEvent) {
										miniVividSeatExchangeEvent = new MiniVividSeatExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",VividSeatE";
								}
								miniVividSeatExchangeEvent.setEvent(event);
								miniVividSeatExchangeEvent
										.setAutoPricingValues(miniExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								miniVividSeatExchangeEvent.setStatus("ACTIVE");
								newMiniVividSeatExchangeEventsList
										.add(miniVividSeatExchangeEvent);

								VipMiniVividSeatExchangeEvent vipMiniVividSeatExchangeEvent = existingVipMiniVividSeatExchangeEventMap
										.remove(event.getId());
								if (vipMiniVividSeatExchangeEvent == null) {
									vipMiniVividSeatExchangeEvent = deletedVipMiniVividSeatExchangeEventMap
											.remove(event.getId());
									if (null == vipMiniVividSeatExchangeEvent) {
										vipMiniVividSeatExchangeEvent = new VipMiniVividSeatExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",VividSeatE";
								}
								vipMiniVividSeatExchangeEvent.setEvent(event);
								vipMiniVividSeatExchangeEvent
										.setAutoPricingValues(vipMiniExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								vipMiniVividSeatExchangeEvent
										.setStatus("ACTIVE");
								newVipMiniVividSeatExchangeEventsList
										.add(vipMiniVividSeatExchangeEvent);

								VipAutoVividSeatExchangeEvent vipAutoVividSeatExchangeEvent = existingVipAutoVividSeatExchangeEventMap
										.remove(event.getId());
								if (vipAutoVividSeatExchangeEvent == null) {
									vipAutoVividSeatExchangeEvent = deletedVipAutoVividSeatExchangeEventMap
											.remove(event.getId());
									if (null == vipAutoVividSeatExchangeEvent) {
										vipAutoVividSeatExchangeEvent = new VipAutoVividSeatExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",VividSeatE";
								}
								vipAutoVividSeatExchangeEvent.setEvent(event);
								vipAutoVividSeatExchangeEvent
										.setAutoPricingValues(vipAutoExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								vipAutoVividSeatExchangeEvent
										.setStatus("ACTIVE");
								newVipAutoVividSeatExchangeEventsList
										.add(vipAutoVividSeatExchangeEvent);

							} else {
								AutoVividSeatExchangeEvent autoVividSeatExchangeEvent = existingAutoVividSeatExchangeEventMap
										.remove(event.getId());
								if (autoVividSeatExchangeEvent != null) {
									autoVividSeatExchangeEvent
											.setStatus("DELETE");
									removeAutoVividSeatExchangeEventsList
											.add(autoVividSeatExchangeEvent);
								}
								MiniVividSeatExchangeEvent miniVividSeatExchangeEvent = existingMiniVividSeatExchangeEventMap
										.remove(event.getId());
								if (miniVividSeatExchangeEvent != null) {
									miniVividSeatExchangeEvent
											.setStatus("DELETE");
									removeMiniVividSeatExchangeEventsList
											.add(miniVividSeatExchangeEvent);
								}
								VipMiniVividSeatExchangeEvent vipMiniVividSeatExchangeEvent = existingVipMiniVividSeatExchangeEventMap
										.remove(event.getId());
								if (vipMiniVividSeatExchangeEvent != null) {
									vipMiniVividSeatExchangeEvent
											.setStatus("DELETE");
									removeVipMiniVividSeatExchangeEventsList
											.add(vipMiniVividSeatExchangeEvent);
								}
								VipAutoVividSeatExchangeEvent vipAutoVividSeatExchangeEvent = existingVipAutoVividSeatExchangeEventMap
										.remove(event.getId());
								if (vipAutoVividSeatExchangeEvent != null) {
									vipAutoVividSeatExchangeEvent
											.setStatus("DELETE");
									removeVipAutoVividSeatExchangeEventsList
											.add(vipAutoVividSeatExchangeEvent);
								}
							}
							/*
							 * if(isVividSeats) { newExchanges = newExchanges
							 * +",VIVID"; VividExchangeEvent vividExchangeEvent
							 * =
							 * existingVividExchangeEvents.remove(event.getId()
							 * ); if(vividExchangeEvent==null){
							 * vividExchangeEvent = new VividExchangeEvent();
							 * vividExchangeEvent.setEvent(event);
							 * vividExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup,
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent);
							 * 
							 * vividExchangeEvent.setStatus("ACTIVE");
							 * newVividExchangeEventsList
							 * .add(vividExchangeEvent);
							 * 
							 * VividExchangeEvent deleteVividExchangeEvent =
							 * deletedVividExchangeEvents.remove(event.getId());
							 * if(null != deleteVividExchangeEvent) {
							 * removeVividExchangeEventsList
							 * .add(deleteVividExchangeEvent); } } else {
							 * oldExchanges = oldExchanges +",VIVID";
							 * if(!existingEventFlag) {
							 * autoPricingAudit.setOldAutoPricingValues
							 * (vividExchangeEvent.getExposure(),
							 * vividExchangeEvent.getRptFactor(),
							 * vividExchangeEvent.getPriceBreakup(),
							 * vividExchangeEvent.getLowerMarkup(),
							 * vividExchangeEvent.getUpperMarkup(),
							 * vividExchangeEvent.getLowerShippingFees(),
							 * vividExchangeEvent.getUpperShippingFees(),
							 * vividExchangeEvent.getIsFrequentEvent()); }
							 * vividExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup,
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent);
							 * existingVividExchangeEventsList
							 * .add(vividExchangeEvent); } }
							 */

							/*
							 * if(isTickPick) { newExchanges = newExchanges
							 * +",TPE"; TickPickExchangeEvent
							 * autoTickPickExchangeEvent =
							 * existingTickPickExchangeEvents
							 * .remove(event.getId());
							 * if(tickPickExchangeEvent==null){
							 * tickPickExchangeEvent = new
							 * TickPickExchangeEvent();
							 * tickPickExchangeEvent.setEvent(event);
							 * tickPickExchangeEvent
							 * .setAutoPricingValues(exposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent);
							 * tickPickExchangeEvent.setStatus("ACTIVE");
							 * newTickPickExchangeEventsList
							 * .add(tickPickExchangeEvent);
							 * 
							 * TickPickExchangeEvent
							 * deletedTickPickExchangeEvent=
							 * deletedTickPickExchangeEvents
							 * .remove(event.getId()); if(null !=
							 * deletedTickPickExchangeEvent) {
							 * removeTickPickExchangeEventsList
							 * .add(deletedTickPickExchangeEvent); }
							 * 
							 * }else { oldExchanges = oldExchanges +",TPE";
							 * if(!existingEventFlag) {
							 * autoPricingAudit.setOldAutoPricingValues
							 * (tickPickExchangeEvent.getExposure(),
							 * tickPickExchangeEvent.getRptFactor(),
							 * tickPickExchangeEvent.getPriceBreakup(),
							 * tickPickExchangeEvent.getLowerMarkup(),
							 * tickPickExchangeEvent.getUpperMarkup(),
							 * tickPickExchangeEvent.getLowerShippingFees(),
							 * tickPickExchangeEvent.getUpperShippingFees(),
							 * tickPickExchangeEvent.getIsFrequentEvent()); }
							 * 
							 * tickPickExchangeEvent.setAutoPricingValues(exposure
							 * , rptFactor, priceBreakup, lowerMarkup,
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent);
							 * existingTickPickExchangeEventsList
							 * .add(tickPickExchangeEvent);
							 * 
							 * } }
							 */

							/*
							 * if(isTickPick) { newExchanges = newExchanges
							 * +",TickPickE"; AutoTickPickExchangeEvent
							 * autoTickPickExchangeEvent =
							 * existingAutoTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(autoTickPickExchangeEvent==null) {
							 * autoTickPickExchangeEvent =
							 * deletedAutoTickPickExchangeEventMap
							 * .remove(event.getId()); if(null ==
							 * autoTickPickExchangeEvent) {
							 * autoTickPickExchangeEvent = new
							 * AutoTickPickExchangeEvent(); }
							 * 
							 * }else{ oldExchanges = oldExchanges +",TickPickE";
							 * } autoTickPickExchangeEvent.setEvent(event);
							 * autoTickPickExchangeEvent
							 * .setAutoPricingValues(autoExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent);
							 * 
							 * autoTickPickExchangeEvent.setStatus("ACTIVE");
							 * newAutoTickPickExchangeEventsList
							 * .add(autoTickPickExchangeEvent);
							 * 
							 * MiniTickPickExchangeEvent
							 * miniTickPickExchangeEvent =
							 * existingMiniTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(miniTickPickExchangeEvent==null) {
							 * miniTickPickExchangeEvent =
							 * deletedMiniTickPickExchangeEventMap
							 * .remove(event.getId()); if(null ==
							 * miniTickPickExchangeEvent) {
							 * miniTickPickExchangeEvent = new
							 * MiniTickPickExchangeEvent(); }
							 * 
							 * }else{ oldExchanges = oldExchanges +",TickPickE";
							 * } miniTickPickExchangeEvent.setEvent(event);
							 * miniTickPickExchangeEvent
							 * .setAutoPricingValues(miniExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent);
							 * 
							 * miniTickPickExchangeEvent.setStatus("ACTIVE");
							 * newMiniTickPickExchangeEventsList
							 * .add(miniTickPickExchangeEvent);
							 * 
							 * VipMiniTickPickExchangeEvent
							 * vipMiniTickPickExchangeEvent =
							 * existingVipMiniTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(vipMiniTickPickExchangeEvent==null) {
							 * vipMiniTickPickExchangeEvent =
							 * deletedVipMiniTickPickExchangeEventMap
							 * .remove(event.getId()); if(null ==
							 * vipMiniTickPickExchangeEvent) {
							 * vipMiniTickPickExchangeEvent = new
							 * VipMiniTickPickExchangeEvent(); }
							 * 
							 * }else{ oldExchanges = oldExchanges +",TickPickE";
							 * } vipMiniTickPickExchangeEvent.setEvent(event);
							 * vipMiniTickPickExchangeEvent
							 * .setAutoPricingValues(vipMiniExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent);
							 * 
							 * vipMiniTickPickExchangeEvent.setStatus("ACTIVE");
							 * newVipMiniTickPickExchangeEventsList
							 * .add(vipMiniTickPickExchangeEvent); }else{
							 * AutoTickPickExchangeEvent
							 * autoTickPickExchangeEvent =
							 * existingAutoTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(autoTickPickExchangeEvent!=null) {
							 * autoTickPickExchangeEvent.setStatus("DELETE");
							 * removeAutoTickPickExchangeEventsList
							 * .add(autoTickPickExchangeEvent); }
							 * MiniTickPickExchangeEvent
							 * miniTickPickExchangeEvent =
							 * existingMiniTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(miniTickPickExchangeEvent!=null) {
							 * miniTickPickExchangeEvent.setStatus("DELETE");
							 * removeMiniTickPickExchangeEventsList
							 * .add(miniTickPickExchangeEvent); }
							 * VipMiniTickPickExchangeEvent
							 * vipMiniTickPickExchangeEvent =
							 * existingVipMiniTickPickExchangeEventMap
							 * .remove(event.getId());
							 * if(vipMiniTickPickExchangeEvent!=null) {
							 * vipMiniTickPickExchangeEvent.setStatus("DELETE");
							 * removeVipMiniTickPickExchangeEventsList
							 * .add(vipMiniTickPickExchangeEvent); } }
							 * 
							 * if(isEbayExchange) { newExchanges = newExchanges
							 * +",EBAY"; EbayExchangeEvent otherExchangeEvent =
							 * existingEbayExchangeEvents.remove(event.getId());
							 * if(otherExchangeEvent==null){ otherExchangeEvent
							 * =
							 * deletedEbayExchangeEvents.remove(event.getId());
							 * if(null == otherExchangeEvent) {
							 * otherExchangeEvent = new EbayExchangeEvent(); }
							 * 
							 * }else { oldExchanges = oldExchanges +",EBAY"; //
							 * if(!existingEventFlag) { //
							 * autoPricingAudit.setOldAutoPricingValues
							 * (otherExchangeEvent.getExposure(),
							 * otherExchangeEvent.getRptFactor(), //
							 * otherExchangeEvent.getPriceBreakup(),
							 * otherExchangeEvent.getLowerMarkup(),
							 * otherExchangeEvent.getUpperMarkup(), //
							 * otherExchangeEvent.getLowerShippingFees(),
							 * otherExchangeEvent.getUpperShippingFees(),
							 * otherExchangeEvent.getIsFrequentEvent()); // } //
							 * //
							 * otherExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup, //
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent); //
							 * existingEbayExchangeEventsList
							 * .add(otherExchangeEvent); }
							 * otherExchangeEvent.setEvent(event);
							 * otherExchangeEvent
							 * .setAutoPricingValues(autoExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent);
							 * otherExchangeEvent.setStatus("ACTIVE");
							 * newEbayExchangeEventsList
							 * .add(otherExchangeEvent);
							 * 
							 * 
							 * }else{ EbayExchangeEvent ebayExchangeEvent =
							 * existingEbayExchangeEvents.remove(event.getId());
							 * if(ebayExchangeEvent!=null) {
							 * ebayExchangeEvent.setStatus("DELETE");
							 * removeEbayExchangeEventsList
							 * .add(ebayExchangeEvent); } }
							 */
							/*
							 * if(isScoreBigExchange) { newExchanges =
							 * newExchanges +",SBE"; ScoreBigExchangeEvent
							 * otherExchangeEvent =
							 * existingScoreBigExchangeEvents
							 * .remove(event.getId());
							 * if(otherExchangeEvent==null){ otherExchangeEvent
							 * = new ScoreBigExchangeEvent();
							 * otherExchangeEvent.setEvent(event);
							 * otherExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup,
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent);
							 * otherExchangeEvent.setStatus("ACTIVE");
							 * newScoreBigExchangeEventsList
							 * .add(otherExchangeEvent);
							 * 
							 * ScoreBigExchangeEvent
							 * deletedScoreBigExchangeEvent=
							 * deletedScoreBigExchangeEvents
							 * .remove(event.getId()); if(null !=
							 * deletedScoreBigExchangeEvent) {
							 * removeScoreBigExchangeEventsList
							 * .add(deletedScoreBigExchangeEvent); }
							 * 
							 * }else { oldExchanges = oldExchanges +",SBE";
							 * if(!existingEventFlag) {
							 * autoPricingAudit.setOldAutoPricingValues
							 * (otherExchangeEvent.getExposure(),
							 * otherExchangeEvent.getRptFactor(),
							 * otherExchangeEvent.getPriceBreakup(),
							 * otherExchangeEvent.getLowerMarkup(),
							 * otherExchangeEvent.getUpperMarkup(),
							 * otherExchangeEvent.getLowerShippingFees(),
							 * otherExchangeEvent.getUpperShippingFees(),
							 * otherExchangeEvent.getIsFrequentEvent()); }
							 * 
							 * otherExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup,
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent);
							 * existingScoreBigExchangeEventsList
							 * .add(otherExchangeEvent);
							 * 
							 * } }
							 */
							System.out.println("isScoreBigExchange--->"
									+ isScoreBigExchange);
							if (isScoreBigExchange) {
								newExchanges = newExchanges + ",ScoreBigE";
								AutoScoreBigExchangeEvent autoScoreBigExchangeEvent = existingAutoScoreBigExchangeEventMap
										.remove(event.getId());
								if (autoScoreBigExchangeEvent == null) {
									autoScoreBigExchangeEvent = deletedAutoScoreBigExchangeEventMap
											.remove(event.getId());
									if (null == autoScoreBigExchangeEvent) {
										autoScoreBigExchangeEvent = new AutoScoreBigExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",ScoreBigE";
								}
								autoScoreBigExchangeEvent.setEvent(event);
								autoScoreBigExchangeEvent.setAutoPricingValues(
										autoExposure, rptFactor, priceBreakup,
										lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								autoScoreBigExchangeEvent.setStatus("ACTIVE");
								newAutoScoreBigExchangeEventsList
										.add(autoScoreBigExchangeEvent);

								MiniScoreBigExchangeEvent miniScoreBigExchangeEvent = existingMiniScoreBigExchangeEventMap
										.remove(event.getId());
								if (miniScoreBigExchangeEvent == null) {
									miniScoreBigExchangeEvent = deletedMiniScoreBigExchangeEventMap
											.remove(event.getId());
									if (null == miniScoreBigExchangeEvent) {
										miniScoreBigExchangeEvent = new MiniScoreBigExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",ScoreBigE";
								}
								miniScoreBigExchangeEvent.setEvent(event);
								miniScoreBigExchangeEvent.setAutoPricingValues(
										miniExposure, rptFactor, priceBreakup,
										lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);

								miniScoreBigExchangeEvent.setStatus("ACTIVE");
								newMiniScoreBigExchangeEventsList
										.add(miniScoreBigExchangeEvent);

								VipMiniScoreBigExchangeEvent vipMiniScoreBigExchangeEvent = existingVipMiniScoreBigExchangeEventMap
										.remove(event.getId());
								if (vipMiniScoreBigExchangeEvent == null) {
									vipMiniScoreBigExchangeEvent = deletedVipMiniScoreBigExchangeEventMap
											.remove(event.getId());
									if (null == vipMiniScoreBigExchangeEvent) {
										vipMiniScoreBigExchangeEvent = new VipMiniScoreBigExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",ScoreBigE";
								}
								vipMiniScoreBigExchangeEvent.setEvent(event);
								vipMiniScoreBigExchangeEvent
										.setAutoPricingValues(vipMiniExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								vipMiniScoreBigExchangeEvent
										.setStatus("ACTIVE");
								newVipMiniScoreBigExchangeEventsList
										.add(vipMiniScoreBigExchangeEvent);

								VipAutoScoreBigExchangeEvent vipAutoScoreBigExchangeEvent = existingVipAutoScoreBigExchangeEventMap
										.remove(event.getId());
								if (vipAutoScoreBigExchangeEvent == null) {
									vipAutoScoreBigExchangeEvent = deletedVipAutoScoreBigExchangeEventMap
											.remove(event.getId());
									if (null == vipAutoScoreBigExchangeEvent) {
										vipAutoScoreBigExchangeEvent = new VipAutoScoreBigExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",ScoreBigE";
								}
								vipAutoScoreBigExchangeEvent.setEvent(event);
								vipAutoScoreBigExchangeEvent
										.setAutoPricingValues(vipAutoExposure,
												rptFactor, priceBreakup,
												lowerMarkup, upperMarkup,
												lowerShippingFees,
												upperShippingFees,
												isFrequentEvent,
												shippingMethod,
												nearTermDisplayOption,
												allowSectionRange,shippingDays);

								vipAutoScoreBigExchangeEvent
										.setStatus("ACTIVE");
								newVipAutoScoreBigExchangeEventsList
										.add(vipAutoScoreBigExchangeEvent);

							} else {
								AutoScoreBigExchangeEvent autoScoreBigExchangeEvent = existingAutoScoreBigExchangeEventMap
										.remove(event.getId());
								if (autoScoreBigExchangeEvent != null) {
									autoScoreBigExchangeEvent
											.setStatus("DELETE");
									removeAutoScoreBigExchangeEventsList
											.add(autoScoreBigExchangeEvent);
								}
								MiniScoreBigExchangeEvent miniScoreBigExchangeEvent = existingMiniScoreBigExchangeEventMap
										.remove(event.getId());
								if (miniScoreBigExchangeEvent != null) {
									miniScoreBigExchangeEvent
											.setStatus("DELETE");
									removeMiniScoreBigExchangeEventsList
											.add(miniScoreBigExchangeEvent);
								}
								VipMiniScoreBigExchangeEvent vipMiniScoreBigExchangeEvent = existingVipMiniScoreBigExchangeEventMap
										.remove(event.getId());
								if (vipMiniScoreBigExchangeEvent != null) {
									vipMiniScoreBigExchangeEvent
											.setStatus("DELETE");
									removeVipMiniScoreBigExchangeEventsList
											.add(vipMiniScoreBigExchangeEvent);
								}
								VipAutoScoreBigExchangeEvent vipAutoScoreBigExchangeEvent = existingVipAutoScoreBigExchangeEventMap
										.remove(event.getId());
								if (vipAutoScoreBigExchangeEvent != null) {
									vipAutoScoreBigExchangeEvent
											.setStatus("DELETE");
									removeVipAutoScoreBigExchangeEventsList
											.add(vipAutoScoreBigExchangeEvent);
								}
							}

							if (isLarryMiniTNExchange) {
								newExchanges = newExchanges + ",LarryMini";
								LarryMiniTNExchangeEvent otherExchangeEvent = existingLarryMiniTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent == null) {
									otherExchangeEvent = deletedLarryMiniTNExchangeEvents
											.remove(event.getId());
									if (null == otherExchangeEvent) {
										otherExchangeEvent = new LarryMiniTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges + ",LarryMini";
									// if(!existingEventFlag) {
									// autoPricingAudit.setOldAutoPricingValues(otherExchangeEvent.getExposure(),
									// otherExchangeEvent.getRptFactor(),
									// otherExchangeEvent.getPriceBreakup(),
									// otherExchangeEvent.getLowerMarkup(),
									// otherExchangeEvent.getUpperMarkup(),
									// otherExchangeEvent.getLowerShippingFees(),
									// otherExchangeEvent.getUpperShippingFees(),
									// otherExchangeEvent.getIsFrequentEvent());
									// }
									//
									// otherExchangeEvent.setAutoPricingValues(exposure,
									// rptFactor, priceBreakup, lowerMarkup,
									// upperMarkup, lowerShippingFees,
									// upperShippingFees, isFrequentEvent);
									// existingLarryMiniTNExchangeEventsList.add(otherExchangeEvent);
								}
								otherExchangeEvent.setEvent(event);
								otherExchangeEvent.setAutoPricingValues(
										miniExposure, rptFactor, priceBreakup,
										lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);
								otherExchangeEvent.setStatus("ACTIVE");
								newLarryMiniTNExchangeEventsList
										.add(otherExchangeEvent);

							} else {
								LarryMiniTNExchangeEvent otherExchangeEvent = existingLarryMiniTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent != null) {
									otherExchangeEvent.setStatus("DELETE");
									removeLarryMiniTNExchangeEventsList
											.add(otherExchangeEvent);
								}
							}

							if (isStubhubExchange) {
								newExchanges = newExchanges
										+ ",LastFiveRowMCStub";
								LastFiveRowMCStubhubExchangeEvent otherExchangeEvent = existingLast5MCStubExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent == null) {
									otherExchangeEvent = deletedLast5MCStubExchangeEvents
											.remove(event.getId());
									if (null == otherExchangeEvent) {
										otherExchangeEvent = new LastFiveRowMCStubhubExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges
											+ ",LastFiveRowMCStub";
								}
								otherExchangeEvent.setEvent(event);
								otherExchangeEvent.setAutoPricingValues(
										lastFiveRowMiniExposure, rptFactor,
										priceBreakup, lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);
								otherExchangeEvent.setStatus("ACTIVE");
								newLast5MCStubExchangeEventsList
										.add(otherExchangeEvent);
							} else {
								LastFiveRowMCStubhubExchangeEvent otherExchangeEvent = existingLast5MCStubExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent != null) {
									otherExchangeEvent.setStatus("DELETE");
									removeLast5MCStubExchangeEventsList
											.add(otherExchangeEvent);
								}
							}
							
							if (isZonesPricingTNExchange) {
								newExchanges = newExchanges
										+ ",ZonesPricingTN";
								ZonesPricingTNExchangeEvent otherExchangeEvent = existingZonesPricingTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent == null) {
									otherExchangeEvent = deletedZonesPricingTNExchangeEvents
											.remove(event.getId());
									if (null == otherExchangeEvent) {
										otherExchangeEvent = new ZonesPricingTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges
											+ ",ZonesPricingTN";
								}
								otherExchangeEvent.setEvent(event);
								otherExchangeEvent.setAutoPricingValues(
										zonesPricingTNExposure, rptFactor,
										priceBreakup, lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);
								otherExchangeEvent.setStatus("ACTIVE");
								newZonesPricingTNExchangeEventsList
										.add(otherExchangeEvent);
							} else {
								ZonesPricingTNExchangeEvent otherExchangeEvent = existingZonesPricingTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent != null) {
									otherExchangeEvent.setStatus("DELETE");
									removeZonesPricingTNExchangeEventsList
											.add(otherExchangeEvent);
								}
							}
							
							if (isZonedLastRowMiniTNExchange) {
								newExchanges = newExchanges
										+ ",ZonedLastRowTN";
								ZonedLastRowMiniTNExchangeEvent otherExchangeEvent = existingZonedLastRowMiniTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent == null) {
									otherExchangeEvent = deletedZonedLastRowMiniTNExchangeEvents
											.remove(event.getId());
									if (null == otherExchangeEvent) {
										otherExchangeEvent = new ZonedLastRowMiniTNExchangeEvent();
									}

								} else {
									oldExchanges = oldExchanges
											+ ",ZonedLastRowTN";
								}
								otherExchangeEvent.setEvent(event);
								otherExchangeEvent.setAutoPricingValues(
										zonedLastRowMiniTNExposure, rptFactor,
										priceBreakup, lowerMarkup, upperMarkup,
										lowerShippingFees, upperShippingFees,
										isFrequentEvent, shippingMethod,
										nearTermDisplayOption,
										allowSectionRange,shippingDays);
								otherExchangeEvent.setStatus("ACTIVE");
								newZonedLastRowMiniTNExchangeEventsList
										.add(otherExchangeEvent);
							} else {
								ZonedLastRowMiniTNExchangeEvent otherExchangeEvent = existingZonedLastRowMiniTNExchangeEvents
										.remove(event.getId());
								if (otherExchangeEvent != null) {
									otherExchangeEvent.setStatus("DELETE");
									removeZonedLastRowMiniTNExchangeEventsList
											.add(otherExchangeEvent);
								}
							}

							/*
							 * if(isExFourExchange) { newExchanges =
							 * newExchanges +",EXFOUR";
							 * ExchangeFourExchangeEvent otherExchangeEvent =
							 * existingExchangeFourExchangeEvents
							 * .remove(event.getId());
							 * if(otherExchangeEvent==null){ otherExchangeEvent
							 * =
							 * deletedExchangeFourExchangeEvents.remove(event.getId
							 * ()); if(null == otherExchangeEvent) {
							 * otherExchangeEvent = new
							 * ExchangeFourExchangeEvent(); }
							 * 
							 * 
							 * 
							 * }else { oldExchanges = oldExchanges +",EXFOUR";
							 * // if(!existingEventFlag) { //
							 * autoPricingAudit.setOldAutoPricingValues
							 * (otherExchangeEvent.getExposure(),
							 * otherExchangeEvent.getRptFactor(), //
							 * otherExchangeEvent.getPriceBreakup(),
							 * otherExchangeEvent.getLowerMarkup(),
							 * otherExchangeEvent.getUpperMarkup(), //
							 * otherExchangeEvent.getLowerShippingFees(),
							 * otherExchangeEvent.getUpperShippingFees(),
							 * otherExchangeEvent.getIsFrequentEvent()); // } //
							 * //
							 * otherExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup, //
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent); //
							 * existingExchangeFourExchangeEventsList
							 * .add(otherExchangeEvent); }
							 * otherExchangeEvent.setEvent(event);
							 * otherExchangeEvent
							 * .setAutoPricingValues(autoExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent
							 * ,shippingMethod,nearTermDisplayOption
							 * ,allowSectionRange);
							 * otherExchangeEvent.setStatus("ACTIVE");
							 * newExchangeFourExchangeEventsList
							 * .add(otherExchangeEvent);
							 * 
							 * 
							 * }else{ ExchangeFourExchangeEvent
							 * otherExchangeEvent =
							 * existingExchangeFourExchangeEvents
							 * .remove(event.getId());
							 * if(otherExchangeEvent!=null) {
							 * otherExchangeEvent.setStatus("DELETE");
							 * existingExchangeFourExchangeEventsList
							 * .add(otherExchangeEvent); } }
							 * if(isExFiveExchange) { newExchanges =
							 * newExchanges +",EXFIVE";
							 * ExchangeFiveExchangeEvent otherExchangeEvent =
							 * existingExchangeFiveExchangeEvents
							 * .remove(event.getId());
							 * if(otherExchangeEvent==null){ otherExchangeEvent
							 * =
							 * deletedExchangeFiveExchangeEvents.remove(event.getId
							 * ()); if(null == otherExchangeEvent) {
							 * otherExchangeEvent = new
							 * ExchangeFiveExchangeEvent(); }
							 * 
							 * 
							 * 
							 * }else { oldExchanges = oldExchanges +",EXFIVE";
							 * // if(!existingEventFlag) { //
							 * autoPricingAudit.setOldAutoPricingValues
							 * (otherExchangeEvent.getExposure(),
							 * otherExchangeEvent.getRptFactor(), //
							 * otherExchangeEvent.getPriceBreakup(),
							 * otherExchangeEvent.getLowerMarkup(),
							 * otherExchangeEvent.getUpperMarkup(), //
							 * otherExchangeEvent.getLowerShippingFees(),
							 * otherExchangeEvent.getUpperShippingFees(),
							 * otherExchangeEvent.getIsFrequentEvent()); // } //
							 * //
							 * otherExchangeEvent.setAutoPricingValues(exposure,
							 * rptFactor, priceBreakup, lowerMarkup, //
							 * upperMarkup, lowerShippingFees,
							 * upperShippingFees, isFrequentEvent); //
							 * existingExchangeFiveExchangeEventsList
							 * .add(otherExchangeEvent); }
							 * otherExchangeEvent.setEvent(event);
							 * otherExchangeEvent
							 * .setAutoPricingValues(autoExposure, rptFactor,
							 * priceBreakup, lowerMarkup, upperMarkup,
							 * lowerShippingFees, upperShippingFees,
							 * isFrequentEvent
							 * ,shippingMethod,nearTermDisplayOption
							 * ,allowSectionRange);
							 * otherExchangeEvent.setStatus("ACTIVE");
							 * newExchangeFiveExchangeEventsList
							 * .add(otherExchangeEvent);
							 * 
							 * 
							 * }else{ ExchangeFiveExchangeEvent
							 * otherExchangeEvent =
							 * existingExchangeFiveExchangeEvents
							 * .remove(event.getId());
							 * if(otherExchangeEvent!=null) {
							 * otherExchangeEvent.setStatus("DELETE");
							 * existingExchangeFiveExchangeEventsList
							 * .add(otherExchangeEvent); } }
							 */
							if (!oldExchanges.equals("")) {
								oldExchanges = oldExchanges.substring(1);
								autoPricingAudit
										.setAction("Event Level Updated");
							} else {
								oldExchanges = null;
							}
							if (!newExchanges.equals("")) {
								newExchanges = newExchanges.substring(1);
							} else {
								newExchanges = null;
							}
							// autoPricingAudit.setOldExchanges(oldExchanges);
							autoPricingAudit.setExchanges(newExchanges);
							autoPricingAudits.put(event.getId(),
									autoPricingAudit);

							info = "Events added successfully.";
							// ticketNetworkCategoriesMarkup.add(tn);
						}
					}
					for (AutoTNExchangeEvent tn : existingAutoTnExchangeEventMap
							.values()) {// TN exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("TNE");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",TNE");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}

					for (AutoVividSeatExchangeEvent tn : existingAutoVividSeatExchangeEventMap
							.values()) {// VividSeat exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("VividSeatE");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",VividSeatE");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}

					for (AutoScoreBigExchangeEvent tn : existingAutoScoreBigExchangeEventMap
							.values()) {// ScoreBig exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("ScoreBigE");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",ScoreBigE");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}

					for (LarryMiniTNExchangeEvent tn : existingLarryMiniTNExchangeEvents
							.values()) {// Larry Mini exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("LarryMini");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",LarryMini");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}

					for (LastFiveRowMCStubhubExchangeEvent tn : existingLast5MCStubExchangeEvents
							.values()) {// Larry Mini exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("LastFiveRowMCStub");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",LastFiveRowMCStub");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}
					
					for (ZonesPricingTNExchangeEvent tn : existingZonesPricingTNExchangeEvents
							.values()) {// Zones Pricing TN exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("ZonesPricingTN");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",ZonesPricingTN");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}
					
					for (ZonedLastRowMiniTNExchangeEvent tn : existingZonedLastRowMiniTNExchangeEvents
							.values()) {// Zoned Lastrow minicat TN exchange removal audit
						autoPricingAudit = autoPricingAudits.get(tn.getEvent()
								.getId());
						if (null == autoPricingAudit) {
							autoPricingAudit = new ExchangeEventAudit();
							autoPricingAudit.setEvent(tn.getEvent());
							autoPricingAudit.setUserName(username);
							autoPricingAudit.setCreatedDate(now);
							autoPricingAudit.setAction("Event Level Removed");
							// autoPricingAudit.setOldExchanges("TNE");
							autoPricingAudit.setDefaultExchangeEventAudit(
									tn.getRptFactor(), tn.getPriceBreakup(),
									tn.getLowerMarkup(), tn.getUpperMarkup(),
									tn.getLowerShippingFees(),
									tn.getUpperShippingFees(),
									tn.getIsFrequentEvent(),
									tn.getShippingMethod(),
									tn.getNearTermDisplayOption(),
									tn.getAllowSectionRange(),tn.getShippingDays());
							// rptFactor, priceBreakup, lMarkup, uMarkup,
							// lShippingFees, uShippingFees, isFrequent

						} else {
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit
										.setAction("Event Level Removed");
							} else {
								autoPricingAudit
										.setAction("Event Level Updated");
							}
							if (autoPricingAudit.getExchanges() == null) {
								autoPricingAudit.setExchanges("ZonedLastRowTN");
								autoPricingAudit.setDefaultExchangeEventAudit(
										tn.getRptFactor(),
										tn.getPriceBreakup(),
										tn.getLowerMarkup(),
										tn.getUpperMarkup(),
										tn.getLowerShippingFees(),
										tn.getUpperShippingFees(),
										tn.getIsFrequentEvent(),
										tn.getShippingMethod(),
										tn.getNearTermDisplayOption(),
										tn.getAllowSectionRange(),tn.getShippingDays());
							} else {
								autoPricingAudit.setExchanges(autoPricingAudit
										.getExchanges() + ",ZonedLastRowTN");
							}
						}
						autoPricingAudits.put(tn.getEvent().getId(),
								autoPricingAudit);
					}

					/*
					 * for (VividExchangeEvent tn :
					 * existingVividExchangeEvents.values()) {//Vivid exchange
					 * removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("VIVID");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("VIVID");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",VIVID"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); } for
					 * (TickPickExchangeEvent tn :
					 * existingTickPickExchangeEvents.values()) {//TN exchange
					 * removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("TPE");
					 * autoPricingAudit.
					 * setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("TPE");
					 * autoPricingAudit.
					 * setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",TPE"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (EbayExchangeEvent tn :
					 * existingEbayExchangeEvents.values()) {//Ebay exchange
					 * removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("EBAY");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("EBAY");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",EBAY"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (ScoreBigExchangeEvent tn :
					 * existingScoreBigExchangeEvents.values()) {//EX One
					 * exchange removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("SBE");
					 * autoPricingAudit.
					 * setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("SBE");
					 * autoPricingAudit.
					 * setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",SBE"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (ExchangeTwoExchangeEvent tn :
					 * existingExchangeTwoExchangeEvents.values()) {//Ex two
					 * exchange removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("EXTWO");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("EXTWO");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",EXTWO"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (ExchangeThreeExchangeEvent tn :
					 * existingExchangeThreeExchangeEvents.values()) {//EX Three
					 * exchange removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("EXTHREE");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("EXTHREE");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",EXTHREE"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (ExchangeFourExchangeEvent tn :
					 * existingExchangeFourExchangeEvents.values()) {//EX Four
					 * exchange removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("EXFOUR");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("EXFOUR");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",EXFOUR"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 * 
					 * for (ExchangeFiveExchangeEvent tn :
					 * existingExchangeFiveExchangeEvents.values()) {//Ex Five
					 * exchange removal audit autoPricingAudit =
					 * autoPricingAudits.get(tn.getEvent().getId()); if(null ==
					 * autoPricingAudit) { autoPricingAudit = new
					 * ExchangeEventAutoPricingAudit();
					 * autoPricingAudit.setEvent(tn.getEvent());
					 * autoPricingAudit.setUserName(username);
					 * autoPricingAudit.setCreatedDate(now);
					 * autoPricingAudit.setAction("Event Level Removed");
					 * autoPricingAudit.setOldExchanges("EXFIVE");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * if(autoPricingAudit.getNewExchanges() == null) {
					 * autoPricingAudit.setAction("Event Level Removed"); } else
					 * { autoPricingAudit.setAction("Event Level Updated"); }
					 * if(autoPricingAudit.getOldExchanges() == null) {
					 * autoPricingAudit.setOldExchanges("EXFIVE");
					 * autoPricingAudit
					 * .setOldAutoPricingValues(tn.getExposure(),
					 * tn.getRptFactor(), tn.getPriceBreakup(),
					 * tn.getLowerMarkup(), tn.getUpperMarkup(),
					 * tn.getLowerShippingFees(), tn.getUpperShippingFees(),
					 * tn.getIsFrequentEvent()); } else {
					 * autoPricingAudit.setOldExchanges
					 * (autoPricingAudit.getOldExchanges()+",EXFIVE"); } }
					 * autoPricingAudits
					 * .put(tn.getEvent().getId(),autoPricingAudit); }
					 */
					// TN Exchange DB updates
					DAORegistry.getAutoTNExchangeEventDAO().saveOrUpdateAll(
							newAutoTNExchangeEventsList);
					DAORegistry.getMiniTNExchangeEventDAO().saveOrUpdateAll(
							newMiniTNExchangeEventsList);
					DAORegistry.getVipMiniTNExchangeEventDAO().saveOrUpdateAll(
							newVipMiniTNExchangeEventsList);
					DAORegistry.getVipAutoTNExchangeEventDAO().saveOrUpdateAll(
							newVipAutoTNExchangeEventsList);
					DAORegistry
							.getLastRowMiniTNExchangeEventDAO()
							.saveOrUpdateAll(newLastRowMiniTNExchangeEventsList);
					// DAORegistry.getAutoTNExchangeEventDAO().deleteAll(existingAutoTNExchangeEventsList);
					// DAORegistry.getMiniTNExchangeEventDAO().deleteAll(existingMiniTNExchangeEventsList);
					// DAORegistry.getVipMiniTNExchangeEventDAO().deleteAll(existingVipMiniTNExchangeEventsList);
					DAORegistry.getAutoTNExchangeEventDAO().saveOrUpdateAll(
							removeAutoTNExchangeEventsList);
					DAORegistry.getMiniTNExchangeEventDAO().saveOrUpdateAll(
							removeMiniTNExchangeEventsList);
					DAORegistry.getVipAutoTNExchangeEventDAO().saveOrUpdateAll(
							removeVipAutoTNExchangeEventsList);
					DAORegistry.getVipMiniTNExchangeEventDAO().saveOrUpdateAll(
							removeVipMiniTNExchangeEventsList);
					DAORegistry.getLastRowMiniTNExchangeEventDAO()
							.saveOrUpdateAll(
									removeLastRowMiniTNExchangeEventsList);

					if (null != existingAutoTnExchangeEventMap
							&& !existingAutoTnExchangeEventMap.isEmpty()) {
						DAORegistry.getAutoTNExchangeEventDAO()
								.updateAllTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingAutoTnExchangeEventMap
														.keySet()));
					}
					existingAutoTNExchangeEventsList = DAORegistry
							.getAutoTNExchangeEventDAO()
							.getAllActiveAutoTNExchangeEventsByEvent(eventIds,
									true);
					existingAutoTnExchangeEventMap.clear();

					for (AutoTNExchangeEvent tgCats : existingAutoTNExchangeEventsList) {
						existingAutoTnExchangeEventMap.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					if (null != existingMiniTnExchangeEventMap
							&& !existingMiniTnExchangeEventMap.isEmpty()) {
						DAORegistry.getMiniTNExchangeEventDAO()
								.updateAllMiniTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingMiniTnExchangeEventMap
														.keySet()));
					}
					existingMiniTNExchangeEventsList = DAORegistry
							.getMiniTNExchangeEventDAO()
							.getAllActiveMiniTNExchangeEventsByEvent(eventIds,
									true);
					existingMiniTnExchangeEventMap.clear();
					for (MiniTNExchangeEvent tgCats : existingMiniTNExchangeEventsList) {
						existingMiniTnExchangeEventMap.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					if (null != existingVipMiniTnExchangeEventMap
							&& !existingVipMiniTnExchangeEventMap.isEmpty()) {
						DAORegistry
								.getVipMiniTNExchangeEventDAO()
								.updateAllVipMiniTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipMiniTnExchangeEventMap
														.keySet()));
					}
					existingVipMiniTNExchangeEventsList = DAORegistry
							.getVipMiniTNExchangeEventDAO()
							.getAllActiveVipMiniTNExchangeEventsByEvent(
									eventIds, true);
					existingVipMiniTnExchangeEventMap.clear();
					for (VipMiniTNExchangeEvent tgCats : existingVipMiniTNExchangeEventsList) {
						existingVipMiniTnExchangeEventMap.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					if (null != existingVipAutoTnExchangeEventMap
							&& !existingVipAutoTnExchangeEventMap.isEmpty()) {
						DAORegistry
								.getVipAutoTNExchangeEventDAO()
								.updateAllVipAutoTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipAutoTnExchangeEventMap
														.keySet()));
					}
					existingVipAutoTNExchangeEventsList = DAORegistry
							.getVipAutoTNExchangeEventDAO()
							.getAllActiveVipAutoTNExchangeEventsByEvent(
									eventIds, true);
					existingVipAutoTnExchangeEventMap.clear();
					for (VipAutoTNExchangeEvent tgCats : existingVipAutoTNExchangeEventsList) {
						existingVipAutoTnExchangeEventMap.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					if (null != existingLastRowMiniTnExchangeEventMap
							&& !existingLastRowMiniTnExchangeEventMap.isEmpty()) {
						DAORegistry
								.getLastRowMiniTNExchangeEventDAO()
								.updateAllLastRowMiniTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingLastRowMiniTnExchangeEventMap
														.keySet()));
					}
					existingLastRowMiniTNExchangeEventsList = DAORegistry
							.getLastRowMiniTNExchangeEventDAO()
							.getAllActiveLastRowMiniTNExchangeEventsByEvent(
									eventIds, true);
					existingLastRowMiniTnExchangeEventMap.clear();
					for (LastRowMiniTNExchangeEvent tgCats : existingLastRowMiniTNExchangeEventsList) {
						existingLastRowMiniTnExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					// VIVID DB updates
					DAORegistry
							.getAutoVividSeatExchangeEventDAO()
							.saveOrUpdateAll(newAutoVividSeatExchangeEventsList);
					DAORegistry
							.getMiniVividSeatExchangeEventDAO()
							.saveOrUpdateAll(newMiniVividSeatExchangeEventsList);
					DAORegistry.getVipMiniVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									newVipMiniVividSeatExchangeEventsList);
					DAORegistry.getVipAutoVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									newVipAutoVividSeatExchangeEventsList);
					// DAORegistry.getAutoVividSeatExchangeEventDAO().deleteAll(existingAutoVividSeatExchangeEventsList);
					// DAORegistry.getMiniVividSeatExchangeEventDAO().deleteAll(existingMiniVividSeatExchangeEventsList);
					// DAORegistry.getVipMiniVividSeatExchangeEventDAO().deleteAll(existingVipMiniVividSeatExchangeEventsList);
					DAORegistry.getAutoVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									removeAutoVividSeatExchangeEventsList);
					DAORegistry.getMiniVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									removeMiniVividSeatExchangeEventsList);
					DAORegistry.getVipMiniVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									removeVipMiniVividSeatExchangeEventsList);
					DAORegistry.getVipAutoVividSeatExchangeEventDAO()
							.saveOrUpdateAll(
									removeVipAutoVividSeatExchangeEventsList);

					if (null != existingAutoVividSeatExchangeEventMap
							&& !existingAutoVividSeatExchangeEventMap.isEmpty()) {
						DAORegistry
								.getAutoVividSeatExchangeEventDAO()
								.updateAllAutoVividSeatExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingAutoVividSeatExchangeEventMap
														.keySet()));
					}
					existingAutoVividSeatExchangeEventsList = DAORegistry
							.getAutoVividSeatExchangeEventDAO()
							.getAllActiveAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					existingAutoVividSeatExchangeEventMap.clear();
					for (AutoVividSeatExchangeEvent tgCats : existingAutoVividSeatExchangeEventsList) {
						existingAutoVividSeatExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingMiniVividSeatExchangeEventMap
							&& !existingMiniVividSeatExchangeEventMap.isEmpty()) {
						DAORegistry
								.getMiniVividSeatExchangeEventDAO()
								.updateAllMiniVividSeatExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingMiniVividSeatExchangeEventMap
														.keySet()));
					}
					existingMiniVividSeatExchangeEventsList = DAORegistry
							.getMiniVividSeatExchangeEventDAO()
							.getAllActiveMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					existingMiniVividSeatExchangeEventMap.clear();
					for (MiniVividSeatExchangeEvent tgCats : existingMiniVividSeatExchangeEventsList) {
						existingMiniVividSeatExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingVipMiniVividSeatExchangeEventMap
							&& !existingVipMiniVividSeatExchangeEventMap
									.isEmpty()) {
						DAORegistry
								.getVipMiniVividSeatExchangeEventDAO()
								.updateAllVipMiniVividSeatExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipMiniVividSeatExchangeEventMap
														.keySet()));
					}
					existingVipMiniVividSeatExchangeEventsList = DAORegistry
							.getVipMiniVividSeatExchangeEventDAO()
							.getAllActiveVipMiniVividSeatExchangeEventsByEvent(
									eventIds, true);
					existingVipMiniVividSeatExchangeEventMap.clear();
					for (VipMiniVividSeatExchangeEvent tgCats : existingVipMiniVividSeatExchangeEventsList) {
						existingVipMiniVividSeatExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingVipAutoVividSeatExchangeEventMap
							&& !existingVipAutoVividSeatExchangeEventMap
									.isEmpty()) {
						DAORegistry
								.getVipAutoVividSeatExchangeEventDAO()
								.updateAllVipAutoVividSeatExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipAutoVividSeatExchangeEventMap
														.keySet()));
					}
					existingVipAutoVividSeatExchangeEventsList = DAORegistry
							.getVipAutoVividSeatExchangeEventDAO()
							.getAllActiveVipAutoVividSeatExchangeEventsByEvent(
									eventIds, true);
					existingVipAutoVividSeatExchangeEventMap.clear();
					for (VipAutoVividSeatExchangeEvent tgCats : existingVipAutoVividSeatExchangeEventsList) {
						existingVipAutoVividSeatExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					// Other Exchange DB updates
					/*
					 * DAORegistry.getTickPickExchangeEventDAO().saveAll(
					 * newTickPickExchangeEventsList);
					 * DAORegistry.getTickPickExchangeEventDAO
					 * ().updateAll(existingTickPickExchangeEventsList);
					 * DAORegistry.getTickPickExchangeEventDAO().deleteAll(
					 * removeTickPickExchangeEventsList);
					 * 
					 * if(null != existingTickPickExchangeEvents &&
					 * !existingTickPickExchangeEvents.isEmpty()) {
					 * DAORegistry.getTickPickExchangeEventDAO
					 * ().updateAllTickPickExchangeEventAsDeleteByEvent(new
					 * ArrayList(existingTickPickExchangeEvents.keySet())); }
					 * existingTickPickExchangeEventsList =
					 * DAORegistry.getTickPickExchangeEventDAO
					 * ().getAllActiveTickPickExchangeEventsByEvent
					 * (eventIds,true); existingTickPickExchangeEvents.clear();
					 * 
					 * for(TickPickExchangeEvent tgCats :
					 * existingTickPickExchangeEventsList){
					 * existingTickPickExchangeEvents
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 */

					/*
					 * DAORegistry.getAutoTickPickExchangeEventDAO().saveOrUpdateAll
					 * (newAutoTickPickExchangeEventsList);
					 * DAORegistry.getMiniTickPickExchangeEventDAO
					 * ().saveOrUpdateAll(newMiniTickPickExchangeEventsList);
					 * DAORegistry
					 * .getVipMiniTickPickExchangeEventDAO().saveOrUpdateAll
					 * (newVipMiniTickPickExchangeEventsList); //
					 * DAORegistry.getAutoTickPickExchangeEventDAO
					 * ().deleteAll(existingAutoTickPickExchangeEventsList); //
					 * DAORegistry.getMiniTickPickExchangeEventDAO().deleteAll(
					 * existingMiniTickPickExchangeEventsList); //
					 * DAORegistry.getVipMiniTickPickExchangeEventDAO
					 * ().deleteAll(existingVipMiniTickPickExchangeEventsList);
					 * DAORegistry
					 * .getAutoTickPickExchangeEventDAO().saveOrUpdateAll
					 * (removeAutoTickPickExchangeEventsList);
					 * DAORegistry.getMiniTickPickExchangeEventDAO
					 * ().saveOrUpdateAll(removeMiniTickPickExchangeEventsList);
					 * DAORegistry
					 * .getVipMiniTickPickExchangeEventDAO().saveOrUpdateAll
					 * (removeVipMiniTickPickExchangeEventsList);
					 * 
					 * 
					 * if(null != existingAutoTickPickExchangeEventMap &&
					 * !existingAutoTickPickExchangeEventMap.isEmpty()) {
					 * DAORegistry.getAutoTickPickExchangeEventDAO().
					 * updateAllAutoTickPickExchangeEventAsDeleteByEvent(new
					 * ArrayList
					 * (existingAutoTickPickExchangeEventMap.keySet())); }
					 * existingAutoTickPickExchangeEventsList =
					 * DAORegistry.getAutoTickPickExchangeEventDAO
					 * ().getAllActiveAutoTickPickExchangeEventsByEvent
					 * (eventIds,true);
					 * existingAutoTickPickExchangeEventMap.clear();
					 * for(AutoTickPickExchangeEvent tgCats :
					 * existingAutoTickPickExchangeEventsList){
					 * existingAutoTickPickExchangeEventMap
					 * .put(tgCats.getEvent().getId(),tgCats); } if(null !=
					 * existingAutoTickPickExchangeEventMap &&
					 * !existingAutoTickPickExchangeEventMap.isEmpty()) {
					 * DAORegistry.getMiniTickPickExchangeEventDAO().
					 * updateAllMiniTickPickExchangeEventAsDeleteByEvent(new
					 * ArrayList
					 * (existingMiniTickPickExchangeEventMap.keySet())); }
					 * existingMiniTickPickExchangeEventsList =
					 * DAORegistry.getMiniTickPickExchangeEventDAO
					 * ().getAllActiveMiniTickPickExchangeEventsByEvent
					 * (eventIds,true);
					 * existingMiniTickPickExchangeEventMap.clear();
					 * for(MiniTickPickExchangeEvent tgCats :
					 * existingMiniTickPickExchangeEventsList){
					 * existingMiniTickPickExchangeEventMap
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 * 
					 * if(null != existingVipMiniTickPickExchangeEventMap &&
					 * !existingVipMiniTickPickExchangeEventMap.isEmpty()) {
					 * DAORegistry.getVipMiniTickPickExchangeEventDAO().
					 * updateAllVipMiniTickPickExchangeEventAsDeleteByEvent(new
					 * ArrayList
					 * (existingMiniTickPickExchangeEventMap.keySet())); }
					 * existingVipMiniTickPickExchangeEventsList =
					 * DAORegistry.getVipMiniTickPickExchangeEventDAO
					 * ().getAllActiveVipMiniTickPickExchangeEventsByEvent
					 * (eventIds,true);
					 * existingVipMiniTickPickExchangeEventMap.clear();
					 * for(VipMiniTickPickExchangeEvent tgCats :
					 * existingVipMiniTickPickExchangeEventsList){
					 * existingVipMiniTickPickExchangeEventMap
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 */

					// EX One Exchange DB updates
					/*
					 * DAORegistry.getScoreBigExchangeEventDAO().saveAll(
					 * newScoreBigExchangeEventsList);
					 * DAORegistry.getScoreBigExchangeEventDAO
					 * ().updateAll(existingScoreBigExchangeEventsList);
					 * DAORegistry.getScoreBigExchangeEventDAO().deleteAll(
					 * removeScoreBigExchangeEventsList);
					 * 
					 * if(null != existingScoreBigExchangeEvents &&
					 * !existingScoreBigExchangeEvents.isEmpty()) {
					 * DAORegistry.getScoreBigExchangeEventDAO
					 * ().updateAllScoreBigExchangeEventAsDeleteByEvent(new
					 * ArrayList(existingScoreBigExchangeEvents.keySet())); }
					 * existingScoreBigExchangeEventsList =
					 * DAORegistry.getScoreBigExchangeEventDAO
					 * ().getAllActiveScoreBigExchangeEventsByEvent
					 * (eventIds,true); existingScoreBigExchangeEvents.clear();
					 * 
					 * for(ScoreBigExchangeEvent tgCats :
					 * existingScoreBigExchangeEventsList){
					 * existingScoreBigExchangeEvents
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 */

					DAORegistry.getAutoScoreBigExchangeEventDAO()
							.saveOrUpdateAll(newAutoScoreBigExchangeEventsList);
					DAORegistry.getMiniScoreBigExchangeEventDAO()
							.saveOrUpdateAll(newMiniScoreBigExchangeEventsList);
					DAORegistry.getVipMiniScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									newVipMiniScoreBigExchangeEventsList);
					DAORegistry.getVipAutoScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									newVipAutoScoreBigExchangeEventsList);
					// DAORegistry.getAutoScoreBigExchangeEventDAO().deleteAll(existingAutoScoreBigExchangeEventsList);
					// DAORegistry.getMiniScoreBigExchangeEventDAO().deleteAll(existingMiniScoreBigExchangeEventsList);
					// DAORegistry.getVipMiniScoreBigExchangeEventDAO().deleteAll(existingVipMiniScoreBigExchangeEventsList);
					DAORegistry.getAutoScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									removeAutoScoreBigExchangeEventsList);
					DAORegistry.getMiniScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									removeMiniScoreBigExchangeEventsList);
					DAORegistry.getVipMiniScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									removeVipMiniScoreBigExchangeEventsList);
					DAORegistry.getVipAutoScoreBigExchangeEventDAO()
							.saveOrUpdateAll(
									removeVipAutoScoreBigExchangeEventsList);

					if (null != existingAutoScoreBigExchangeEventMap
							&& !existingAutoScoreBigExchangeEventMap.isEmpty()) {
						DAORegistry
								.getAutoScoreBigExchangeEventDAO()
								.updateAllAutoScoreBigExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingAutoScoreBigExchangeEventMap
														.keySet()));
					}
					existingAutoScoreBigExchangeEventsList = DAORegistry
							.getAutoScoreBigExchangeEventDAO()
							.getAllActiveAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					existingAutoScoreBigExchangeEventMap.clear();
					for (AutoScoreBigExchangeEvent tgCats : existingAutoScoreBigExchangeEventsList) {
						existingAutoScoreBigExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingMiniScoreBigExchangeEventMap
							&& !existingMiniScoreBigExchangeEventMap.isEmpty()) {
						DAORegistry
								.getMiniScoreBigExchangeEventDAO()
								.updateAllMiniScoreBigExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingMiniScoreBigExchangeEventMap
														.keySet()));
					}
					existingMiniScoreBigExchangeEventsList = DAORegistry
							.getMiniScoreBigExchangeEventDAO()
							.getAllActiveMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					existingMiniScoreBigExchangeEventMap.clear();
					for (MiniScoreBigExchangeEvent tgCats : existingMiniScoreBigExchangeEventsList) {
						existingMiniScoreBigExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingVipMiniScoreBigExchangeEventMap
							&& !existingVipMiniScoreBigExchangeEventMap
									.isEmpty()) {
						DAORegistry
								.getVipMiniScoreBigExchangeEventDAO()
								.updateAllVipMiniScoreBigExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipMiniScoreBigExchangeEventMap
														.keySet()));
					}
					existingVipMiniScoreBigExchangeEventsList = DAORegistry
							.getVipMiniScoreBigExchangeEventDAO()
							.getAllActiveVipMiniScoreBigExchangeEventsByEvent(
									eventIds, true);
					existingVipMiniScoreBigExchangeEventMap.clear();
					for (VipMiniScoreBigExchangeEvent tgCats : existingVipMiniScoreBigExchangeEventsList) {
						existingVipMiniScoreBigExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}

					if (null != existingVipAutoScoreBigExchangeEventMap
							&& !existingVipAutoScoreBigExchangeEventMap
									.isEmpty()) {
						DAORegistry
								.getVipAutoScoreBigExchangeEventDAO()
								.updateAllVipAutoScoreBigExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingVipAutoScoreBigExchangeEventMap
														.keySet()));
					}
					existingVipAutoScoreBigExchangeEventsList = DAORegistry
							.getVipAutoScoreBigExchangeEventDAO()
							.getAllActiveVipAutoScoreBigExchangeEventsByEvent(
									eventIds, true);
					existingVipAutoScoreBigExchangeEventMap.clear();
					for (VipAutoScoreBigExchangeEvent tgCats : existingVipAutoScoreBigExchangeEventsList) {
						existingVipAutoScoreBigExchangeEventMap.put(tgCats
								.getEvent().getId(), tgCats);
					}
					// EBay Exchange DB updates
					/*
					 * DAORegistry.getEbayExchangeEventDAO().saveOrUpdateAll(
					 * newEbayExchangeEventsList); //
					 * DAORegistry.getEbayExchangeEventDAO
					 * ().updateAll(existingEbayExchangeEventsList);
					 * DAORegistry.getEbayExchangeEventDAO().saveOrUpdateAll(
					 * removeEbayExchangeEventsList);
					 * 
					 * if(null != existingEbayExchangeEvents &&
					 * !existingEbayExchangeEvents.isEmpty()) {
					 * DAORegistry.getEbayExchangeEventDAO
					 * ().updateAllEbayExchangeEventAsDeleteByEvent(new
					 * ArrayList(existingEbayExchangeEvents.keySet())); }
					 * existingEbayExchangeEventsList =
					 * DAORegistry.getEbayExchangeEventDAO
					 * ().getAllActiveEbayExchangeEventsByEvent(eventIds,true);
					 * existingEbayExchangeEvents.clear();
					 * 
					 * for(EbayExchangeEvent tgCats :
					 * existingEbayExchangeEventsList){
					 * existingEbayExchangeEvents
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 */

					// Larry Mini Exchange DB updates
					DAORegistry.getLarryMiniTNExchangeEventDAO()
							.saveOrUpdateAll(newLarryMiniTNExchangeEventsList);
					// DAORegistry.getLarryMiniTNExchangeEventDAO().updateAll(existingLarryMiniTNExchangeEventsList);
					DAORegistry.getLarryMiniTNExchangeEventDAO()
							.saveOrUpdateAll(
									removeLarryMiniTNExchangeEventsList);

					if (null != existingLarryMiniTNExchangeEvents
							&& !existingLarryMiniTNExchangeEvents.isEmpty()) {
						DAORegistry
								.getLarryMiniTNExchangeEventDAO()
								.updateAllLarryMiniTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingLarryMiniTNExchangeEvents
														.keySet()));
					}
					existingLarryMiniTNExchangeEventsList = DAORegistry
							.getLarryMiniTNExchangeEventDAO()
							.getAllActiveLarryMiniTNExchangeEventsByEvent(
									eventIds, true);
					existingLarryMiniTNExchangeEvents.clear();

					for (LarryMiniTNExchangeEvent tgCats : existingLarryMiniTNExchangeEventsList) {
						existingLarryMiniTNExchangeEvents.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					// Last Five ROw Mini DB updates
					DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO()
							.saveOrUpdateAll(newLast5MCStubExchangeEventsList);
					// DAORegistry.getExchangeThreeExchangeEventDAO().updateAll(existingExchangeThreeExchangeEventsList);
					DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO()
							.saveOrUpdateAll(
									removeLast5MCStubExchangeEventsList);

					if (null != existingLast5MCStubExchangeEvents
							&& !existingLast5MCStubExchangeEvents.isEmpty()) {
						DAORegistry
								.getLastFiveRowMCStubhubExchangeEventDAO()
								.updateAllLastFiveRowMCStubhubExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingLast5MCStubExchangeEvents
														.keySet()));
					}
					existingLast5MCStubExchangeEventsList = DAORegistry
							.getLastFiveRowMCStubhubExchangeEventDAO()
							.getAllActiveLastFiveRowMCStubhubExchangeEventsByEvent(
									eventIds, true);
					existingLast5MCStubExchangeEvents.clear();

					for (LastFiveRowMCStubhubExchangeEvent tgCats : existingLast5MCStubExchangeEventsList) {
						existingLast5MCStubExchangeEvents.put(tgCats.getEvent()
								.getId(), tgCats);
					}
					
					
					
					// Zones Pricing DB updates
					DAORegistry.getZonesPricingTNExchangeEventDAO()
							.saveOrUpdateAll(newZonesPricingTNExchangeEventsList);

					DAORegistry.getZonesPricingTNExchangeEventDAO()
							.saveOrUpdateAll(
									removeZonesPricingTNExchangeEventsList);

					if (null != existingZonesPricingTNExchangeEvents
							&& !existingZonesPricingTNExchangeEvents.isEmpty()) {
						DAORegistry
								.getZonesPricingTNExchangeEventDAO()
								.updateAllZonesPricingTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingZonesPricingTNExchangeEvents
														.keySet()));
					}
					existingZonesPricingTNExchangeEventsList = DAORegistry
							.getZonesPricingTNExchangeEventDAO()
							.getAllActiveZonesPricingTNExchangeEventsByEvent(
									eventIds, true);
					existingZonesPricingTNExchangeEvents.clear();

					for (ZonesPricingTNExchangeEvent tgCats : existingZonesPricingTNExchangeEventsList) {
						existingZonesPricingTNExchangeEvents.put(tgCats.getEvent()
								.getId(), tgCats);
					}
					
					// Zoned Lastrow minicats DB updates
					DAORegistry.getZonedLastRowMiniTNExchangeEventDAO()
							.saveOrUpdateAll(newZonedLastRowMiniTNExchangeEventsList);

					DAORegistry.getZonedLastRowMiniTNExchangeEventDAO()
							.saveOrUpdateAll(
									removeZonedLastRowMiniTNExchangeEventsList);

					if (null != existingZonedLastRowMiniTNExchangeEvents
							&& !existingZonedLastRowMiniTNExchangeEvents.isEmpty()) {
						DAORegistry
								.getZonedLastRowMiniTNExchangeEventDAO()
								.updateAllZonedLastRowMiniTNExchangeEventAsDeleteByEvent(
										new ArrayList(
												existingZonedLastRowMiniTNExchangeEvents
														.keySet()));
					}
					existingZonedLastRowMiniTNExchangeEventsList = DAORegistry
							.getZonedLastRowMiniTNExchangeEventDAO()
							.getAllActiveZonedLastRowMiniTNExchangeEventsByEvent(
									eventIds, true);
					existingZonedLastRowMiniTNExchangeEvents.clear();

					for (ZonedLastRowMiniTNExchangeEvent tgCats : existingZonedLastRowMiniTNExchangeEventsList) {
						existingZonedLastRowMiniTNExchangeEvents.put(tgCats.getEvent()
								.getId(), tgCats);
					}

					// Ex Four Exchange DB updates
					/*
					 * DAORegistry.getExchangeFourExchangeEventDAO().saveOrUpdateAll
					 * (newExchangeFourExchangeEventsList); //
					 * DAORegistry.getExchangeFourExchangeEventDAO
					 * ().updateAll(existingExchangeFourExchangeEventsList);
					 * DAORegistry
					 * .getExchangeFourExchangeEventDAO().saveOrUpdateAll
					 * (removeExchangeFourExchangeEventsList);
					 * 
					 * if(null != existingExchangeFourExchangeEvents &&
					 * !existingExchangeFourExchangeEvents.isEmpty()) {
					 * DAORegistry.getExchangeFourExchangeEventDAO().
					 * updateAllExchangeFourExchangeEventAsDeleteByEvent(new
					 * ArrayList(existingExchangeFourExchangeEvents.keySet()));
					 * } existingExchangeFourExchangeEventsList =
					 * DAORegistry.getExchangeFourExchangeEventDAO
					 * ().getAllActiveExchangeFourExchangeEventsByEvent
					 * (eventIds,true);
					 * existingExchangeFourExchangeEvents.clear();
					 * 
					 * for(ExchangeFourExchangeEvent tgCats :
					 * existingExchangeFourExchangeEventsList){
					 * existingExchangeFourExchangeEvents
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 * 
					 * //Ex Five Exchange DB updates
					 * DAORegistry.getExchangeFiveExchangeEventDAO
					 * ().saveOrUpdateAll(newExchangeFiveExchangeEventsList); //
					 * DAORegistry.getExchangeFiveExchangeEventDAO().updateAll(
					 * existingExchangeFiveExchangeEventsList);
					 * DAORegistry.getExchangeFiveExchangeEventDAO
					 * ().saveOrUpdateAll(removeExchangeFiveExchangeEventsList);
					 * 
					 * if(null != existingExchangeFiveExchangeEvents &&
					 * !existingExchangeFiveExchangeEvents.isEmpty()) {
					 * DAORegistry.getExchangeFiveExchangeEventDAO().
					 * updateAllExchangeFiveExchangeEventAsDeleteByEvent(new
					 * ArrayList(existingExchangeFiveExchangeEvents.keySet()));
					 * } existingExchangeFiveExchangeEventsList =
					 * DAORegistry.getExchangeFiveExchangeEventDAO
					 * ().getAllActiveExchangeFiveExchangeEventsByEvent
					 * (eventIds,true);
					 * existingExchangeFiveExchangeEvents.clear();
					 * 
					 * for(ExchangeFiveExchangeEvent tgCats :
					 * existingExchangeFiveExchangeEventsList){
					 * existingExchangeFiveExchangeEvents
					 * .put(tgCats.getEvent().getId(),tgCats); }
					 */

					// Audit DB updates
					DAORegistry.getExchangeEventAutoPricingAuditDAO().saveAll(
							new ArrayList(autoPricingAudits.values()));
					action = "";
				}

				if (action.isEmpty() || action.equals("search")) {
					/*
					 * List<Integer> frequentEventIds =
					 * ZonesDAORegistry.getQueryManagerDAO
					 * ().getTGCatsFrequentEventIds(); if(null ==
					 * frequentEventIds) { frequentEventIds = new
					 * ArrayList<Integer>(); } int otherFrequentEventsCount =
					 * frequentEventIds.size();
					 */

					boolean existingEventFlag;
					ExchangeEventAutoPricing autoPricing = null;
					for (Event event : eventMap.values()) {
						if (!eventIds.contains(event.getId())) {
							continue;
						}
						existingEventFlag = false;
						autoPricing = new ExchangeEventAutoPricing();
						autoPricing.setEvent(event);

						Integer shippingMethod = 3, nearTermDisplayOption = 0;

						String parentCategory = event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName();

						if (parentCategory.equalsIgnoreCase("Sports")) {
							shippingMethod = defaultSportsShippingMethod;
							nearTermDisplayOption = defaultSportsNearTermDisplay;
						} else if (parentCategory.equalsIgnoreCase("Concerts")) {
							shippingMethod = defaultConcertsShippingMethod;
							nearTermDisplayOption = defaultConcertsNearTermDisplay;
						} else if (parentCategory.equalsIgnoreCase("Theater")) {
							shippingMethod = defaultTheaterShippingMethod;
							nearTermDisplayOption = defaultTheaterNearTermDisplay;
						} else if (parentCategory.equalsIgnoreCase("other")) {
							shippingMethod = defaultOthersShippingMethod;
							nearTermDisplayOption = defaultOthersNearTermDisplay;
						} /*else if (parentCategory.equalsIgnoreCase("Las Vegas")) {
							shippingMethod = defaultVegasShippingMethod;
							nearTermDisplayOption = defaultVegasNearTermDisplay;
						}*/

						autoPricing.setShippingMethod(shippingMethod);
						autoPricing
								.setNearTermDisplayOption(nearTermDisplayOption);

						AutoTNExchangeEvent autoTN = existingAutoTnExchangeEventMap
								.remove(event.getId());
						MiniTNExchangeEvent miniTN = existingMiniTnExchangeEventMap
								.remove(event.getId());
						VipMiniTNExchangeEvent vipMiniTn = existingVipMiniTnExchangeEventMap
								.remove(event.getId());
						VipAutoTNExchangeEvent vipAutoTn = existingVipAutoTnExchangeEventMap
								.remove(event.getId());
						LastRowMiniTNExchangeEvent lastRowMiniTN = existingLastRowMiniTnExchangeEventMap
								.remove(event.getId());

						if (autoTN != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(
										autoTN.getRptFactor(),
										autoTN.getPriceBreakup(),
										autoTN.getLowerMarkup(),
										autoTN.getUpperMarkup(),
										autoTN.getLowerShippingFees(),
										autoTN.getUpperShippingFees(),
										autoTN.getIsFrequentEvent(),
										autoTN.getShippingMethod(),
										autoTN.getNearTermDisplayOption(),
										autoTN.getAllowSectionRange(),autoTN.getShippingDays());
								autoPricing.setAutoExposure(autoTN
										.getExposure());
								autoPricing.setMiniExposure(miniTN
										.getExposure());
								autoPricing.setVipMiniExposure(vipMiniTn
										.getExposure());
								autoPricing.setVipAutoExposure(vipAutoTn
										.getExposure());
								autoPricing
										.setLastRowMiniExposure(lastRowMiniTN
												.getExposure());

							}
							autoPricing.setIsTicketNetworkEvent(true);
							existingEventFlag = true;
						}

						/*
						 * VividExchangeEvent vivid =
						 * existingVividExchangeEvents.remove(event.getId());
						 * if(vivid != null){ if(!existingEventFlag) {
						 * autoPricing
						 * .setAutoPricingValues(vivid.getRptFactor(),
						 * vivid.getPriceBreakup(), vivid.getLowerMarkup(),
						 * vivid.getUpperMarkup(), vivid.getLowerShippingFees(),
						 * vivid.getUpperShippingFees(),
						 * vivid.getIsFrequentEvent()); }
						 * autoPricing.setAutoExposure(tn.getExposure());
						 * autoPricing.setIsVividSeatsEvent(true);
						 * existingEventFlag = true; }
						 */
						AutoVividSeatExchangeEvent autoVividSeat = existingAutoVividSeatExchangeEventMap
								.remove(event.getId());
						MiniVividSeatExchangeEvent miniVividSeat = existingMiniVividSeatExchangeEventMap
								.remove(event.getId());
						VipMiniVividSeatExchangeEvent vipMiniVividSeat = existingVipMiniVividSeatExchangeEventMap
								.remove(event.getId());
						VipAutoVividSeatExchangeEvent vipAutoVividSeat = existingVipAutoVividSeatExchangeEventMap
								.remove(event.getId());

						if (autoVividSeat != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(autoVividSeat
										.getRptFactor(), autoVividSeat
										.getPriceBreakup(), autoVividSeat
										.getLowerMarkup(), autoVividSeat
										.getUpperMarkup(), autoVividSeat
										.getLowerShippingFees(), autoVividSeat
										.getUpperShippingFees(), autoVividSeat
										.getIsFrequentEvent(), autoVividSeat
										.getShippingMethod(), autoVividSeat
										.getNearTermDisplayOption(),
										autoVividSeat.getAllowSectionRange(),autoVividSeat.getShippingDays());
								autoPricing.setAutoExposure(autoVividSeat
										.getExposure());
								autoPricing.setMiniExposure(miniVividSeat
										.getExposure());
								autoPricing.setVipMiniExposure(vipMiniVividSeat
										.getExposure());
								autoPricing.setVipAutoExposure(vipAutoVividSeat
										.getExposure());

							}
							autoPricing.setIsVividSeatsEvent(true);
							existingEventFlag = true;
						}
						/*
						 * AutoTickPickExchangeEvent autoTickPick =
						 * existingAutoTickPickExchangeEventMap
						 * .remove(event.getId()); MiniTickPickExchangeEvent
						 * miniTickPick =
						 * existingMiniTickPickExchangeEventMap.remove
						 * (event.getId()); VipMiniTickPickExchangeEvent
						 * vipMiniTickPick =
						 * existingVipMiniTickPickExchangeEventMap
						 * .remove(event.getId());
						 * 
						 * if(autoTickPick != null){ if(!existingEventFlag) {
						 * autoPricing
						 * .setAutoPricingValues(autoTickPick.getExposure
						 * (),miniTickPick
						 * .getExposure(),vipMiniTickPick.getExposure
						 * (),autoTickPick
						 * .getRptFactor(),autoTickPick.getPriceBreakup(),
						 * autoTickPick.getLowerMarkup(),
						 * autoTickPick.getUpperMarkup(),
						 * autoTickPick.getLowerShippingFees(),
						 * autoTickPick.getUpperShippingFees(),
						 * autoTickPick.getIsFrequentEvent());
						 * 
						 * } autoPricing.setIsTickPickEvent(true);
						 * existingEventFlag = true; }
						 */

						/*
						 * TickPickExchangeEvent other =
						 * existingTickPickExchangeEvents.remove(event.getId());
						 * if(other != null){ if(!existingEventFlag) {
						 * autoPricing.setAutoPricingValues(other.getExposure(),
						 * other.getRptFactor(), other.getPriceBreakup(),
						 * other.getLowerMarkup(), other.getUpperMarkup(),
						 * other.getLowerShippingFees(),
						 * other.getUpperShippingFees(),
						 * other.getIsFrequentEvent()); }
						 * autoPricing.setIsTickPickEvent(true);
						 * existingEventFlag = true; }
						 */

						/*
						 * EbayExchangeEvent ebay =
						 * existingEbayExchangeEvents.remove(event.getId());
						 * if(ebay != null){ if(!existingEventFlag) {
						 * autoPricing
						 * .setAutoPricingValues(ebay.getExposure(),ebay
						 * .getExposure(),ebay.getExposure(),
						 * ebay.getRptFactor(), ebay.getPriceBreakup(),
						 * ebay.getLowerMarkup(), ebay.getUpperMarkup(),
						 * ebay.getLowerShippingFees(),
						 * ebay.getUpperShippingFees(),
						 * ebay.getIsFrequentEvent()); }
						 * autoPricing.setIsEbayEvent(true); existingEventFlag =
						 * true; }
						 */

						/*
						 * ScoreBigExchangeEvent exOne =
						 * existingScoreBigExchangeEvents.remove(event.getId());
						 * if(exOne != null){ if(!existingEventFlag) {
						 * autoPricing.setAutoPricingValues(exOne.getExposure(),
						 * exOne.getRptFactor(), exOne.getPriceBreakup(),
						 * exOne.getLowerMarkup(), exOne.getUpperMarkup(),
						 * exOne.getLowerShippingFees(),
						 * exOne.getUpperShippingFees(),
						 * exOne.getIsFrequentEvent()); }
						 * autoPricing.setIsScoreBigEvent(true);
						 * existingEventFlag = true; }
						 */

						AutoScoreBigExchangeEvent autoScoreBig = existingAutoScoreBigExchangeEventMap
								.remove(event.getId());
						MiniScoreBigExchangeEvent miniScoreBig = existingMiniScoreBigExchangeEventMap
								.remove(event.getId());
						VipMiniScoreBigExchangeEvent vipMiniScoreBig = existingVipMiniScoreBigExchangeEventMap
								.remove(event.getId());
						VipAutoScoreBigExchangeEvent vipAutoScoreBig = existingVipAutoScoreBigExchangeEventMap
								.remove(event.getId());

						if (autoScoreBig != null) {
							if (!existingEventFlag) {
								autoPricing
										.setAutoPricingValues(
												autoScoreBig.getRptFactor(),
												autoScoreBig.getPriceBreakup(),
												autoScoreBig.getLowerMarkup(),
												autoScoreBig.getUpperMarkup(),
												autoScoreBig
														.getLowerShippingFees(),
												autoScoreBig
														.getUpperShippingFees(),
												autoScoreBig
														.getIsFrequentEvent(),
												autoScoreBig
														.getShippingMethod(),
												autoScoreBig
														.getNearTermDisplayOption(),
												autoScoreBig
														.getAllowSectionRange(),autoScoreBig.getShippingDays());
								autoPricing.setAutoExposure(autoScoreBig
										.getExposure());
								autoPricing.setMiniExposure(miniScoreBig
										.getExposure());
								autoPricing.setVipMiniExposure(vipMiniScoreBig
										.getExposure());
								autoPricing.setVipAutoExposure(vipAutoScoreBig
										.getExposure());
							}
							autoPricing.setIsScoreBigEvent(true);
							existingEventFlag = true;
						}

						LarryMiniTNExchangeEvent larryMiniTN = existingLarryMiniTNExchangeEvents
								.remove(event.getId());
						if (larryMiniTN != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(
										larryMiniTN.getRptFactor(),
										larryMiniTN.getPriceBreakup(),
										larryMiniTN.getLowerMarkup(),
										larryMiniTN.getUpperMarkup(),
										larryMiniTN.getLowerShippingFees(),
										larryMiniTN.getUpperShippingFees(),
										larryMiniTN.getIsFrequentEvent(),
										larryMiniTN.getShippingMethod(),
										larryMiniTN.getNearTermDisplayOption(),
										larryMiniTN.getAllowSectionRange(),larryMiniTN.getShippingDays());

							}
							autoPricing.setMiniExposure(larryMiniTN
									.getExposure());
							autoPricing.setIsLarryMiniTNEvent(true);
							existingEventFlag = true;
						}

						LastFiveRowMCStubhubExchangeEvent stubhubEx = existingLast5MCStubExchangeEvents
								.remove(event.getId());
						if (stubhubEx != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(
										stubhubEx.getRptFactor(),
										stubhubEx.getPriceBreakup(),
										stubhubEx.getLowerMarkup(),
										stubhubEx.getUpperMarkup(),
										stubhubEx.getLowerShippingFees(),
										stubhubEx.getUpperShippingFees(),
										stubhubEx.getIsFrequentEvent(),
										stubhubEx.getShippingMethod(),
										stubhubEx.getNearTermDisplayOption(),
										stubhubEx.getAllowSectionRange(),stubhubEx.getShippingDays());

							}
							autoPricing.setLastFiveRowMiniExposure(stubhubEx
									.getExposure());
							autoPricing.setIsStubhubEvent(true);
							existingEventFlag = true;
						}
						
						ZonesPricingTNExchangeEvent zonesPricingTNEx = existingZonesPricingTNExchangeEvents
						.remove(event.getId());
						if (zonesPricingTNEx != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(
										zonesPricingTNEx.getRptFactor(),
										zonesPricingTNEx.getPriceBreakup(),
										zonesPricingTNEx.getLowerMarkup(),
										zonesPricingTNEx.getUpperMarkup(),
										zonesPricingTNEx.getLowerShippingFees(),
										zonesPricingTNEx.getUpperShippingFees(),
										zonesPricingTNEx.getIsFrequentEvent(),
										zonesPricingTNEx.getShippingMethod(),
										zonesPricingTNEx.getNearTermDisplayOption(),
										zonesPricingTNEx.getAllowSectionRange(),zonesPricingTNEx.getShippingDays());
		
							}
							autoPricing.setZonesPricingExposure(zonesPricingTNEx
									.getExposure());
							autoPricing.setIsZonesPricingTNEvent(true);
							existingEventFlag = true;
						}
						
						ZonedLastRowMiniTNExchangeEvent zonedLastRowMiniTNEx = existingZonedLastRowMiniTNExchangeEvents
						.remove(event.getId());
						if (zonedLastRowMiniTNEx != null) {
							if (!existingEventFlag) {
								autoPricing.setAutoPricingValues(
										zonedLastRowMiniTNEx.getRptFactor(),
										zonedLastRowMiniTNEx.getPriceBreakup(),
										zonedLastRowMiniTNEx.getLowerMarkup(),
										zonedLastRowMiniTNEx.getUpperMarkup(),
										zonedLastRowMiniTNEx.getLowerShippingFees(),
										zonedLastRowMiniTNEx.getUpperShippingFees(),
										zonedLastRowMiniTNEx.getIsFrequentEvent(),
										zonedLastRowMiniTNEx.getShippingMethod(),
										zonedLastRowMiniTNEx.getNearTermDisplayOption(),
										zonedLastRowMiniTNEx.getAllowSectionRange(),zonedLastRowMiniTNEx.getShippingDays());
		
							}
							autoPricing.setZonedLastRowMiniExposure(zonedLastRowMiniTNEx
									.getExposure());
							autoPricing.setIsZonedLastRowMiniTNEvent(true);
							existingEventFlag = true;
						}

						/*
						 * ExchangeFourExchangeEvent exFour =
						 * existingExchangeFourExchangeEvents
						 * .remove(event.getId()); if(exFour != null){
						 * if(!existingEventFlag) {
						 * autoPricing.setAutoPricingValues
						 * (exFour.getRptFactor(), exFour.getPriceBreakup(),
						 * exFour.getLowerMarkup(), exFour.getUpperMarkup(),
						 * exFour.getLowerShippingFees(),
						 * exFour.getUpperShippingFees(),
						 * exFour.getIsFrequentEvent(),
						 * exFour.getShippingMethod(
						 * ),exFour.getNearTermDisplayOption
						 * (),exFour.getAllowSectionRange()); }
						 * autoPricing.setIsExchangeFourEvent(true);
						 * existingEventFlag = true; }
						 * 
						 * ExchangeFiveExchangeEvent exFive =
						 * existingExchangeFiveExchangeEvents
						 * .remove(event.getId()); if(exFive != null){
						 * if(!existingEventFlag) {
						 * autoPricing.setAutoPricingValues
						 * (exFive.getRptFactor(), exFive.getPriceBreakup(),
						 * exFive.getLowerMarkup(), exFive.getUpperMarkup(),
						 * exFive.getLowerShippingFees(),
						 * exFive.getUpperShippingFees(),
						 * exFive.getIsFrequentEvent(),
						 * exFive.getShippingMethod(
						 * ),exFive.getNearTermDisplayOption
						 * (),exFive.getAllowSectionRange()); }
						 * autoPricing.setIsExchangeFiveEvent(true);
						 * existingEventFlag = true; }
						 */

						exchangeEventAutoPricingList.add(autoPricing);
					}
					// map.put("otherFrequentEventsCount",
					// otherFrequentEventsCount);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		
		mav.addObject("events", events);
		mav.addObject("eventStr", selectedEvent);
		mav.addObject("venue", venueStr);
		mav.addObject("grandChild", grandChildStr);
		mav.addObject("artist", artistStr);
		mav.addObject("info", info);
		mav.addObject("categories", exchangeEventAutoPricingList);
		return mav;
	}

	public ModelAndView loadExchangeEventAutoPricingAudit(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-exchange-event-auto-pricing-audit-popup");
		String eventId = request.getParameter("eId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		List<ExchangeEventAudit> list = DAORegistry.getExchangeEventAuditDAO()
				.getExchangeEventAuditByEventId(event.getId());
		mav.addObject("auditList", list);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadManageBrokerAutoPricingAudit(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-manage-broker-auto-pricing-audit-popup");
		String eventId = request.getParameter("eId");
		//System.out.println("The Event ID for Audit" + eventId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		List<ManageBrokerAudit> list = DAORegistry.getManageBrokerAuditDAO()
				.getManageBrokerAuditByEventId(event.getId());
		
		List<Broker> brokers = DAORegistry.getBrokerDAO().getAllActiveAutopricingBrokers();
		
		mav.addObject("auditList", list);
		mav.addObject("event", event);
		mav.addObject("brokers", brokers);
		return mav;
	}

	public ModelAndView loadDefaultAutopricing(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String sportinfo = "";
		ModelAndView mav = new ModelAndView("page-admin-default-autopricing");
		if ("updateExposure".equalsIgnoreCase(request.getParameter("action"))) {

			String miniExposureSport = request
					.getParameter("mini_exposure_sport");
			String autoExposureSport = request
					.getParameter("auto_exposure_sport");
			String vipMiniExposureSport = request
					.getParameter("vip_mini_exposure_sport");
			String vipAutoExposureSport = request
					.getParameter("vip_auto_exposure_sport");
			String lastRowMiniExposureSport = request
					.getParameter("lastrow_mini_exposure_sport");
			String lastFiveRowMiniExposureSport = request
					.getParameter("lastfiverow_mini_exposure_sport");
			String zonesPricingExposureSport = request
			.getParameter("zones_pricing_exposure_sport");
			String zonedLastRowMiniExposureSport = request
			.getParameter("zoned_lastrowmini_exposure_sport");
			String shippingMethodSport = request
					.getParameter("shipping_method_sport");
			String nearTermDisplayOptionSport = request
					.getParameter("near_term_display_option_sport");
			String rpt_sport = request.getParameter("rpt_sport");
			String pricebreakup_sport = request
					.getParameter("price_breakup_sport");
			String upper_markpu_sport = request
					.getParameter("upper_markup_sport");
			String lower_markpu_sport = request
					.getParameter("lower_markup_sport");
			String upper_shippingfees_sport = request
					.getParameter("upper_shippingFees_sport");
			String lower_shippingfees_sport = request
					.getParameter("lower_shippingFees_sport");
			String sectionCount_sport = request
					.getParameter("sectionCount_sport");

			String miniExposureConcert = request
					.getParameter("mini_exposure_concert");
			String autoExposureConcert = request
					.getParameter("auto_exposure_concert");
			String vipMiniExposureConcert = request
					.getParameter("vip_mini_exposure_concert");
			String vipAutoExposureConcert = request
					.getParameter("vip_auto_exposure_concert");
			String lastRowMiniExposureConcert = request
					.getParameter("lastrow_mini_exposure_concert");
			String lastFiveRowMiniExposureConcert = request
					.getParameter("lastfiverow_mini_exposure_concert");
			String zonesPricingExposureConcert = request
			.getParameter("zones_pricing_exposure_concert");
			String zonedLastRowMiniExposureConcert = request
			.getParameter("zoned_lastrowmini_exposure_concert");
			String shippingMethodConcert = request
					.getParameter("shipping_method_concert");
			String nearTermDisplayOptionConcert = request
					.getParameter("near_term_display_option_concert");
			String rpt_concert = request.getParameter("rpt_concert");
			String pricebreakup_concert = request
					.getParameter("price_breakup_concert");
			String upper_markpu_concert = request
					.getParameter("upper_markup_concert");
			String lower_markpu_concert = request
					.getParameter("lower_markup_concert");
			String upper_shippingfees_concert = request
					.getParameter("upper_shippingFees_concert");
			String lower_shippingfees_concert = request
					.getParameter("lower_shippingFees_concert");
			String sectionCount_concert = request
					.getParameter("sectionCount_concert");

			String miniExposureTheater = request
					.getParameter("mini_exposure_theater");
			String autoExposureTheater = request
					.getParameter("auto_exposure_theater");
			String vipMiniExposureTheater = request
					.getParameter("vip_mini_exposure_theater");
			String vipAutoExposureTheater = request
					.getParameter("vip_auto_exposure_theater");
			String lastRowMiniExposureTheater = request
					.getParameter("lastrow_mini_exposure_theater");
			String lastFiveRowMiniExposureTheater = request
					.getParameter("lastfiverow_mini_exposure_theater");
			String zonesPricingExposureTheater = request
			.getParameter("zones_pricing_exposure_theater");
			String zonedLastRowMiniExposureTheater = request
			.getParameter("zoned_lastrowmini_exposure_theater");
			String shippingMethodTheater = request
					.getParameter("shipping_method_theater");
			String nearTermDisplayOptionTheater = request
					.getParameter("near_term_display_option_theater");
			String rpt_theater = request.getParameter("rpt_theater");
			String pricebreakup_theater = request
					.getParameter("price_breakup_theater");
			String upper_markpu_theater = request
					.getParameter("upper_markup_theater");
			String lower_markpu_theater = request
					.getParameter("lower_markup_theater");
			String upper_shippingfees_theater = request
					.getParameter("upper_shippingFees_theater");
			String lower_shippingfees_theater = request
					.getParameter("lower_shippingFees_theater");
			String sectionCount_theater = request
					.getParameter("sectionCount_theater");
/*
			String miniExposureVegas = request
					.getParameter("mini_exposure_vegas");
			String autoExposureVegas = request
					.getParameter("auto_exposure_vegas");
			String vipMiniExposureVegas = request
					.getParameter("vip_mini_exposure_vegas");
			String vipAutoExposureVegas = request
					.getParameter("vip_auto_exposure_vegas");
			String lastRowMiniExposureVegas = request
					.getParameter("lastrow_mini_exposure_vegas");
			String lastFiveRowMiniExposureVegas = request
					.getParameter("lastfiverow_mini_exposure_vegas");
			String shippingMethodVegas = request
					.getParameter("shipping_method_vegas");
			String nearTermDisplayOptionVegas = request
					.getParameter("near_term_display_option_vegas");
			String rpt_vegas = request.getParameter("rpt_vegas");
			String pricebreakup_vegas = request
					.getParameter("price_breakup_vegas");
			String upper_markpu_vegas = request
					.getParameter("upper_markup_vegas");
			String lower_markpu_vegas = request
					.getParameter("lower_markup_vegas");
			String upper_shippingfees_vegas = request
					.getParameter("upper_shippingFees_vegas");
			String lower_shippingfees_vegas = request
					.getParameter("lower_shippingFees_vegas");
			String sectionCount_vegas = request
					.getParameter("sectionCount_vegas");
*/
			String miniExposureOthers = request
					.getParameter("mini_exposure_others");
			String autoExposureOthers = request
					.getParameter("auto_exposure_others");
			String vipMiniExposureOthers = request
					.getParameter("vip_mini_exposure_others");
			String vipAutoExposureOthers = request
					.getParameter("vip_auto_exposure_others");
			String lastRowMiniExposureOthers = request
					.getParameter("lastrow_mini_exposure_others");
			String lastFiveRowMiniExposureOthers = request
					.getParameter("lastfiverow_mini_exposure_others");
			String zonesPricingExposureOthers = request
			.getParameter("zones_pricing_exposure_others");
			String zonedLastRowMiniExposureOthers = request
			.getParameter("zoned_lastrowmini_exposure_others");
			String shippingMethodOthers = request
					.getParameter("shipping_method_others");
			String nearTermDisplayOptionOthers = request
					.getParameter("near_term_display_option_others");
			String rpt_others = request.getParameter("rpt_others");
			String pricebreakup_others = request
					.getParameter("price_breakup_others");
			String upper_markpu_others = request
					.getParameter("upper_markup_others");
			String lower_markpu_others = request
					.getParameter("lower_markup_others");
			String upper_shippingfees_others = request
					.getParameter("upper_shippingFees_others");
			String lower_shippingfees_others = request
					.getParameter("lower_shippingFees_others");
			String sectionCount_others = request
					.getParameter("sectionCount_others");

			PropertyDAO propertyDAO = null;
			com.admitone.tmat.data.Property property = null;
			DefaultAutoPricingAudit audit = null;
			String username = SecurityContextHolder.getContext()
					.getAuthentication().getName();
			Date now = new Date();
			try {
				audit = new DefaultAutoPricingAudit();
				propertyDAO = DAORegistry.getPropertyDAO();
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				audit.setCategory("sport");

				property = propertyDAO
						.get("exchange.default.exposure.sports.mini");
				if (property != null) {
					audit.setOldMiniExposure(property.getValue());
					property.setValue(miniExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.sports.auto");
				if (property != null) {
					audit.setOldAutoExposure(property.getValue());
					property.setValue(autoExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.sports.vipmini");
				if (property != null) {
					audit.setOldVipMiniExposure(property.getValue());
					property.setValue(vipMiniExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.sports.vipauto");
				if (property != null) {
					audit.setOldVipAutoExposure(property.getValue());
					property.setValue(vipAutoExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.sports.lastrowmini");
				if (property != null) {
					audit.setOldLastRowMiniExposure(property.getValue());
					property.setValue(lastRowMiniExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastRowMiniExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.exposure.sports.lastfiverowmini");
				if (property != null) {
					audit.setOldLastFiveRowMiniExposure(property.getValue());
					property.setValue(lastFiveRowMiniExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastFiveRowMiniExposure(property.getValue());
				}
				
				property = propertyDAO
				.get("exchange.default.exposure.sports.zonespricing");
				if (property != null) {
					audit.setOldZonesPricingExposure(property.getValue());
					property.setValue(zonesPricingExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonesPricingExposure(property.getValue());
				}
				
				property = propertyDAO
				.get("exchange.default.exposure.sports.zonedlastrowmini");
				if (property != null) {
					audit.setOldZonedLastRowMiniExposure(property.getValue());
					property.setValue(zonedLastRowMiniExposureSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonedLastRowMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.shippingmethod.sports");
				if (property != null) {
					audit.setOldShippingMethod(Integer.valueOf(property
							.getValue()));
					property.setValue(shippingMethodSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewShippingMethod(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.neartermdisplayoption.sports");
				if (property != null) {
					audit.setOldNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
					property.setValue(nearTermDisplayOptionSport);
					propertyDAO.saveOrUpdate(property);
					audit.setNewNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO.get("exchange.default.rpt.sports");
				if (property != null) {
					audit.setOldRptFactor(Double.parseDouble(property
							.getValue()));
					property.setValue(rpt_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO.get("exchange.default.rpt.sports");
					audit.setNewRptFactor(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.pricebreakup.sports");
				if (property != null) {
					audit.setOldPriceBreakup(Double.parseDouble(property
							.getValue()));
					property.setValue(pricebreakup_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.pricebreakup.sports");
					audit.setNewPriceBreakup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppermarkup.sports");
				if (property != null) {
					audit.setOldUpperMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_markpu_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppermarkup.sports");
					audit.setNewUpperMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowermarkup.sports");
				if (property != null) {
					audit.setOldLowerMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_markpu_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowermarkup.sports");
					audit.setNewLowerMarkup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppershippingfees.sports");
				if (property != null) {
					audit.setOldUpperShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_shippingfees_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppershippingfees.sports");
					audit.setNewUpperShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowershippingfees.sports");
				if (property != null) {
					audit.setOldLowerShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_shippingfees_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowershippingfees.sports");
					audit.setNewLowerShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.sectioncount.sports");
				if (property != null) {
					audit.setOldSectionCountTicket(Integer.parseInt(property
							.getValue()));
					property.setValue(sectionCount_sport);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.sectioncount.sports");
					audit.setNewSectionCountTicket(Integer.parseInt(property
							.getValue()));
				}
				DAORegistry.getDefaultAutoPricingAuditDAO().save(audit);
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				audit.setCategory("concert");

				property = propertyDAO
						.get("exchange.default.exposure.concerts.mini");
				if (property != null) {
					audit.setOldMiniExposure(property.getValue());
					property.setValue(miniExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.concerts.auto");
				if (property != null) {
					audit.setOldAutoExposure(property.getValue());
					property.setValue(autoExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.concerts.vipmini");
				if (property != null) {
					audit.setOldVipMiniExposure(property.getValue());
					property.setValue(vipMiniExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.concerts.vipauto");
				if (property != null) {
					audit.setOldVipAutoExposure(property.getValue());
					property.setValue(vipAutoExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.concerts.lastrowmini");
				if (property != null) {
					audit.setOldLastRowMiniExposure(property.getValue());
					property.setValue(lastRowMiniExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastRowMiniExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.exposure.concerts.lastfiverowmini");
				if (property != null) {
					audit.setOldLastFiveRowMiniExposure(property.getValue());
					property.setValue(lastFiveRowMiniExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastFiveRowMiniExposure(property.getValue());
				}
				property = propertyDAO
				.get("exchange.default.exposure.concerts.zonespricing");
				if (property != null) {
					audit.setOldZonesPricingExposure(property.getValue());
					property.setValue(zonesPricingExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonesPricingExposure(property.getValue());
				}
				property = propertyDAO
				.get("exchange.default.exposure.concerts.zonedlastrowmini");
				if (property != null) {
					audit.setOldZonedLastRowMiniExposure(property.getValue());
					property.setValue(zonedLastRowMiniExposureConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonedLastRowMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.shippingmethod.concerts");
				if (property != null) {
					audit.setOldShippingMethod(Integer.valueOf(property
							.getValue()));
					property.setValue(shippingMethodConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewShippingMethod(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.neartermdisplayoption.concerts");
				if (property != null) {
					audit.setOldNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
					property.setValue(nearTermDisplayOptionConcert);
					propertyDAO.saveOrUpdate(property);
					audit.setNewNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO.get("exchange.default.rpt.concerts");
				if (property != null) {
					audit.setOldRptFactor(Double.parseDouble(property
							.getValue()));
					property.setValue(rpt_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO.get("exchange.default.rpt.concerts");
					audit.setNewRptFactor(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.pricebreakup.concerts");
				if (property != null) {
					audit.setOldPriceBreakup(Double.parseDouble(property
							.getValue()));
					property.setValue(pricebreakup_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.pricebreakup.concerts");
					audit.setNewPriceBreakup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppermarkup.concerts");
				if (property != null) {
					audit.setOldUpperMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_markpu_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppermarkup.concerts");
					audit.setNewUpperMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowermarkup.concerts");
				if (property != null) {
					audit.setOldLowerMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_markpu_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowermarkup.concerts");
					audit.setNewLowerMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.uppershippingfees.concerts");
				if (property != null) {
					audit.setOldUpperShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_shippingfees_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppershippingfees.concerts");
					audit.setNewUpperShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowershippingfees.concerts");
				if (property != null) {
					audit.setOldLowerShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_shippingfees_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowershippingfees.concerts");
					audit.setNewLowerShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.sectioncount.concerts");
				if (property != null) {
					audit.setOldSectionCountTicket(Integer.parseInt(property
							.getValue()));
					property.setValue(sectionCount_concert);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.sectioncount.concerts");
					audit.setNewSectionCountTicket(Integer.parseInt(property
							.getValue()));
				}
				DAORegistry.getDefaultAutoPricingAuditDAO().save(audit);
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				audit.setCategory("theater");

				property = propertyDAO
						.get("exchange.default.exposure.theater.mini");
				if (property != null) {
					audit.setOldMiniExposure(property.getValue());
					property.setValue(miniExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.theater.auto");
				if (property != null) {
					audit.setOldAutoExposure(property.getValue());
					property.setValue(autoExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.theater.vipmini");
				if (property != null) {
					audit.setOldVipMiniExposure(property.getValue());
					property.setValue(vipMiniExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.exposure.theater.vipauto");
				if (property != null) {
					audit.setOldVipAutoExposure(property.getValue());
					property.setValue(vipAutoExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipAutoExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.exposure.theater.lastrowmini");
				if (property != null) {
					audit.setOldLastRowMiniExposure(property.getValue());
					property.setValue(lastRowMiniExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastRowMiniExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.exposure.theater.lastfiverowmini");
				if (property != null) {
					audit.setOldLastFiveRowMiniExposure(property.getValue());
					property.setValue(lastFiveRowMiniExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastFiveRowMiniExposure(property.getValue());
				}
				
				property = propertyDAO
				.get("exchange.default.exposure.theater.zonespricing");
				if (property != null) {
					audit.setOldZonesPricingExposure(property.getValue());
					property.setValue(zonesPricingExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonesPricingExposure(property.getValue());
				}
				
				property = propertyDAO
				.get("exchange.default.exposure.theater.zonedlastrowmini");
				if (property != null) {
					audit.setOldZonedLastRowMiniExposure(property.getValue());
					property.setValue(zonedLastRowMiniExposureTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonedLastRowMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.shippingmethod.theater");
				if (property != null) {
					audit.setOldShippingMethod(Integer.valueOf(property
							.getValue()));
					property.setValue(shippingMethodTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewShippingMethod(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.neartermdisplayoption.theater");
				if (property != null) {
					audit.setOldNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
					property.setValue(nearTermDisplayOptionTheater);
					propertyDAO.saveOrUpdate(property);
					audit.setNewNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO.get("exchange.default.rpt.theater");
				if (property != null) {
					audit.setOldRptFactor(Double.parseDouble(property
							.getValue()));
					property.setValue(rpt_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO.get("exchange.default.rpt.theater");
					audit.setNewRptFactor(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.pricebreakup.theater");
				if (property != null) {
					audit.setOldPriceBreakup(Double.parseDouble(property
							.getValue()));
					property.setValue(pricebreakup_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.pricebreakup.theater");
					audit.setNewPriceBreakup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppermarkup.theater");
				if (property != null) {
					audit.setOldUpperMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_markpu_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppermarkup.theater");
					audit.setNewUpperMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowermarkup.theater");
				if (property != null) {
					audit.setOldLowerMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_markpu_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowermarkup.theater");
					audit.setNewLowerMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.uppershippingfees.theater");
				if (property != null) {
					audit.setOldUpperShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_shippingfees_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppershippingfees.theater");
					audit.setNewUpperShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowershippingfees.theater");
				if (property != null) {
					audit.setOldLowerShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_shippingfees_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowershippingfees.theater");
					audit.setNewLowerShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.sectioncount.theater");
				if (property != null) {
					audit.setOldSectionCountTicket(Integer.parseInt(property
							.getValue()));
					property.setValue(sectionCount_theater);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.sectioncount.theater");
					audit.setNewSectionCountTicket(Integer.parseInt(property
							.getValue()));
				}
				DAORegistry.getDefaultAutoPricingAuditDAO().save(audit);
				/*
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				audit.setCategory("vegas");

				property = propertyDAO
						.get("exchange.default.mini.exposure.lasvegas");
				if (property != null) {
					audit.setOldMiniExposure(property.getValue());
					property.setValue(miniExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.auto.exposure.lasvegas");
				if (property != null) {
					audit.setOldAutoExposure(property.getValue());
					property.setValue(autoExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.vip.mini.exposure.lasvegas");
				if (property != null) {
					audit.setOldVipMiniExposure(property.getValue());
					property.setValue(vipMiniExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.vip.auto.exposure.lasvegas");
				if (property != null) {
					audit.setOldVipAutoExposure(property.getValue());
					property.setValue(vipAutoExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipAutoExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.lastrow.mini.exposure.lasvegas");
				if (property != null) {
					audit.setOldLastRowMiniExposure(property.getValue());
					property.setValue(lastRowMiniExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastRowMiniExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.lastfiverow.mini.exposure.lasvegas");
				if (property != null) {
					audit.setOldLastFiveRowMiniExposure(property.getValue());
					property.setValue(lastFiveRowMiniExposureVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastFiveRowMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.shippingmethod.lasvegas");
				if (property != null) {
					audit.setOldShippingMethod(Integer.valueOf(property
							.getValue()));
					property.setValue(shippingMethodVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewShippingMethod(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.neartermdisplayoption.lasvegas");
				if (property != null) {
					audit.setOldNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
					property.setValue(nearTermDisplayOptionVegas);
					propertyDAO.saveOrUpdate(property);
					audit.setNewNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO.get("exchange.default.rpt.lasvegas");
				if (property != null) {
					audit.setOldRptFactor(Double.parseDouble(property
							.getValue()));
					property.setValue(rpt_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO.get("exchange.default.rpt.lasvegas");
					audit.setNewRptFactor(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.pricebreakup.lasvegas");
				if (property != null) {
					audit.setOldPriceBreakup(Double.parseDouble(property
							.getValue()));
					property.setValue(pricebreakup_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.pricebreakup.lasvegas");
					audit.setNewPriceBreakup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppermarkup.lasvegas");
				if (property != null) {
					audit.setOldUpperMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_markpu_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppermarkup.lasvegas");
					audit.setNewUpperMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowermarkup.lasvegas");
				if (property != null) {
					audit.setOldLowerMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_markpu_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowermarkup.lasvegas");
					audit.setNewLowerMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.uppershippingfees.lasvegas");
				if (property != null) {
					audit.setOldUpperShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_shippingfees_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppershippingfees.lasvegas");
					audit.setNewUpperShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowershippingfees.lasvegas");
				if (property != null) {
					audit.setOldLowerShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_shippingfees_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowershippingfees.lasvegas");
					audit.setNewLowerShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.sectioncount.lasvegas");
				if (property != null) {
					audit.setOldSectionCountTicket(Integer.parseInt(property
							.getValue()));
					property.setValue(sectionCount_vegas);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.sectioncount.lasvegas");
					audit.setNewSectionCountTicket(Integer.parseInt(property
							.getValue()));
				}
				DAORegistry.getDefaultAutoPricingAuditDAO().save(audit);
				*/
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				audit.setCategory("others");

				property = propertyDAO
						.get("exchange.default.mini.exposure.default");
				if (property != null) {
					audit.setOldMiniExposure(property.getValue());
					property.setValue(miniExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.auto.exposure.default");
				if (property != null) {
					audit.setOldAutoExposure(property.getValue());
					property.setValue(autoExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewAutoExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.vip.mini.exposure.default");
				if (property != null) {
					audit.setOldVipMiniExposure(property.getValue());
					property.setValue(vipMiniExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.vip.auto.exposure.default");
				if (property != null) {
					audit.setOldVipAutoExposure(property.getValue());
					property.setValue(vipAutoExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewVipAutoExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.lastrow.mini.exposure.default");
				if (property != null) {
					audit.setOldLastRowMiniExposure(property.getValue());
					property.setValue(lastRowMiniExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastRowMiniExposure(property.getValue());
				}
				property = propertyDAO
						.get("exchange.default.lastfiverow.mini.exposure.default");
				if (property != null) {
					audit.setOldLastFiveRowMiniExposure(property.getValue());
					property.setValue(lastFiveRowMiniExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewLastFiveRowMiniExposure(property.getValue());
				}
				
				property = propertyDAO
				.get("exchange.default.zonespricing.exposure.default");
				if (property != null) {
					audit.setOldZonesPricingExposure(property.getValue());
					property.setValue(zonesPricingExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonesPricingExposure(property.getValue());
				}
				property = propertyDAO
				.get("exchange.default.zonedlastrowmini.exposure.default");
				if (property != null) {
					audit.setOldZonedLastRowMiniExposure(property.getValue());
					property.setValue(zonedLastRowMiniExposureOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewZonedLastRowMiniExposure(property.getValue());
				}

				property = propertyDAO
						.get("exchange.default.shippingmethod.default");
				if (property != null) {
					audit.setOldShippingMethod(Integer.valueOf(property
							.getValue()));
					property.setValue(shippingMethodOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewShippingMethod(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.neartermdisplayoption.default");
				if (property != null) {
					audit.setOldNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
					property.setValue(nearTermDisplayOptionOthers);
					propertyDAO.saveOrUpdate(property);
					audit.setNewNearTermDisplayOption(Integer.valueOf(property
							.getValue()));
				}

				property = propertyDAO.get("exchange.default.rpt.default");
				if (property != null) {
					audit.setOldRptFactor(Double.parseDouble(property
							.getValue()));
					property.setValue(rpt_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO.get("exchange.default.rpt.default");
					audit.setNewRptFactor(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.pricebreakup.default");
				if (property != null) {
					audit.setOldPriceBreakup(Double.parseDouble(property
							.getValue()));
					property.setValue(pricebreakup_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.pricebreakup.default");
					audit.setNewPriceBreakup(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.default.uppermarkup.default");
				if (property != null) {
					audit.setOldUpperMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_markpu_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppermarkup.default");
					audit.setNewUpperMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowermarkup.default");
				if (property != null) {
					audit.setOldLowerMarkup(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_markpu_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowermarkup.default");
					audit.setNewLowerMarkup(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.uppershippingfees.default");
				if (property != null) {
					audit.setOldUpperShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(upper_shippingfees_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.uppershippingfees.default");
					audit.setNewUpperShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.lowershippingfees.default");
				if (property != null) {
					audit.setOldLowerShippingFees(Double.parseDouble(property
							.getValue()));
					property.setValue(lower_shippingfees_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.lowershippingfees.default");
					audit.setNewLowerShippingFees(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.default.sectioncount.default");
				if (property != null) {
					audit.setOldSectionCountTicket(Integer.parseInt(property
							.getValue()));
					property.setValue(sectionCount_others);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.default.sectioncount.default");
					audit.setNewSectionCountTicket(Integer.parseInt(property
							.getValue()));
				}
				DAORegistry.getDefaultAutoPricingAuditDAO().save(audit);
				sportinfo = "Updated successfully";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Property sportminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.sports.mini");
		Property sportautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.sports.auto");
		Property sportvipminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.sports.vipmini");
		Property sportvipautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.sports.vipauto");
		Property sportlastrowminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.sports.lastrowmini");
		Property sportlastfiverowminiexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.exposure.sports.lastfiverowmini");
		Property sportzonespricingexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.exposure.sports.zonespricing");
		Property sportzonedLastRowMiniexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.exposure.sports.zonedlastrowmini");
		Property sportshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.sports");
		Property sportneartermdisplayoption = DAORegistry.getPropertyDAO().get(
				"exchange.default.neartermdisplayoption.sports");
		com.admitone.tmat.data.Property sportrpt = DAORegistry.getPropertyDAO()
				.get("exchange.default.rpt.sports");
		com.admitone.tmat.data.Property sportpricebreakup = DAORegistry
				.getPropertyDAO().get("exchange.default.pricebreakup.sports");
		com.admitone.tmat.data.Property uppersportmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.uppermarkup.sports");
		com.admitone.tmat.data.Property lowersportmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.lowermarkup.sports");
		com.admitone.tmat.data.Property uppersportshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.uppershippingfees.sports");
		com.admitone.tmat.data.Property lowersportshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.lowershippingfees.sports");
		com.admitone.tmat.data.Property sportsectionCount = DAORegistry
				.getPropertyDAO().get("exchange.default.sectioncount.sports");

		Property concertminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.concerts.mini");
		Property concertautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.concerts.auto");
		Property concertvipminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.concerts.vipmini");
		Property concertvipautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.concerts.vipauto");
		Property concertlastrowminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.concerts.lastrowmini");
		Property concertlastfiverowminiexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.exposure.concerts.lastfiverowmini");
		Property concertzonespricingexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.exposure.concerts.zonespricing");
		Property concertzonedLastRowMiniexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.exposure.concerts.zonedlastrowmini");
		Property concertshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.concerts");
		Property concertneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.concerts");
		com.admitone.tmat.data.Property concertrpt = DAORegistry
				.getPropertyDAO().get("exchange.default.rpt.concerts");
		com.admitone.tmat.data.Property concertpricebreakup = DAORegistry
				.getPropertyDAO().get("exchange.default.pricebreakup.concerts");
		com.admitone.tmat.data.Property upperconcertmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.uppermarkup.concerts");
		com.admitone.tmat.data.Property lowerconcertmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.lowermarkup.concerts");
		com.admitone.tmat.data.Property upperconcertshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.uppershippingfees.concerts");
		com.admitone.tmat.data.Property lowerconcertshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.lowershippingfees.concerts");
		com.admitone.tmat.data.Property concertsectionCount = DAORegistry
				.getPropertyDAO().get("exchange.default.sectioncount.concerts");

		Property theaterminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.theater.mini");
		Property theaterautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.theater.auto");
		Property theatervipminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.theater.vipmini");
		Property theatervipautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.theater.vipauto");
		Property theaterlastrowminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.exposure.theater.lastrowmini");
		Property theaterlastfiverowminiexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.exposure.theater.lastfiverowmini");
		Property theaterzonespricingexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.exposure.theater.zonespricing");
		Property theaterzonedLastRowMiniexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.exposure.theater.zonedlastrowmini");
		Property theatershippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.theater");
		Property theaterneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.theater");
		com.admitone.tmat.data.Property theaterrpt = DAORegistry
				.getPropertyDAO().get("exchange.default.rpt.theater");
		com.admitone.tmat.data.Property theaterpricebreakup = DAORegistry
				.getPropertyDAO().get("exchange.default.pricebreakup.theater");
		com.admitone.tmat.data.Property uppertheatermarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.uppermarkup.theater");
		com.admitone.tmat.data.Property lowertheatermarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.lowermarkup.theater");
		com.admitone.tmat.data.Property uppertheatershippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.uppershippingfees.theater");
		com.admitone.tmat.data.Property lowertheatershippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.lowershippingfees.theater");
		com.admitone.tmat.data.Property theatersectionCount = DAORegistry
				.getPropertyDAO().get("exchange.default.sectioncount.theater");
/*
		Property vegasminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.mini.exposure.lasvegas");
		Property vegasautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.auto.exposure.lasvegas");
		Property vegasvipminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.vip.mini.exposure.lasvegas");
		Property vegasvipautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.vip.auto.exposure.lasvegas");
		Property vegaslastrowminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.lastrow.mini.exposure.lasvegas");
		Property vegaslastfiverowminiexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.lastfiverow.mini.exposure.lasvegas");
		Property vegasshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.lasvegas");
		Property vegasneartermdisplayoption = DAORegistry.getPropertyDAO().get(
				"exchange.default.neartermdisplayoption.lasvegas");
		com.admitone.tmat.data.Property vegasrpt = DAORegistry.getPropertyDAO()
				.get("exchange.default.rpt.lasvegas");
		com.admitone.tmat.data.Property vegaspricebreakup = DAORegistry
				.getPropertyDAO().get("exchange.default.pricebreakup.lasvegas");
		com.admitone.tmat.data.Property uppervegasmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.uppermarkup.lasvegas");
		com.admitone.tmat.data.Property lowervegasmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.lowermarkup.lasvegas");
		com.admitone.tmat.data.Property uppervegasshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.uppershippingfees.lasvegas");
		com.admitone.tmat.data.Property lowervegasshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.lowershippingfees.lasvegas");
		com.admitone.tmat.data.Property vegassectionCount = DAORegistry
				.getPropertyDAO().get("exchange.default.sectioncount.lasvegas");
*/
		Property othersminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.mini.exposure.default");
		Property othersautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.auto.exposure.default");
		Property othersvipminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.vip.mini.exposure.default");
		Property othersvipautoexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.vip.auto.exposure.default");
		Property otherslastrowminiexposure = DAORegistry.getPropertyDAO().get(
				"exchange.default.lastrow.mini.exposure.default");
		Property otherslastfiverowminiexposure = DAORegistry.getPropertyDAO()
				.get("exchange.default.lastfiverow.mini.exposure.default");
		Property otherszonespricingexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.zonespricing.exposure.default");
		Property otherszonedLastRowMiniexposure = DAORegistry.getPropertyDAO()
		.get("exchange.default.zonedlastrowmini.exposure.default");
		Property othersshippingmethod = DAORegistry.getPropertyDAO().get(
				"exchange.default.shippingmethod.default");
		Property othersneartermdisplayoption = DAORegistry.getPropertyDAO()
				.get("exchange.default.neartermdisplayoption.default");
		com.admitone.tmat.data.Property othersrpt = DAORegistry
				.getPropertyDAO().get("exchange.default.rpt.default");
		com.admitone.tmat.data.Property otherspricebreakup = DAORegistry
				.getPropertyDAO().get("exchange.default.pricebreakup.default");
		com.admitone.tmat.data.Property upperothersmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.uppermarkup.default");
		com.admitone.tmat.data.Property lowerothersmarkup = DAORegistry
				.getPropertyDAO().get("exchange.default.lowermarkup.default");
		com.admitone.tmat.data.Property upperothersshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.uppershippingfees.default");
		com.admitone.tmat.data.Property lowerothersshippingfees = DAORegistry
				.getPropertyDAO().get(
						"exchange.default.lowershippingfees.default");
		com.admitone.tmat.data.Property otherssectionCount = DAORegistry
				.getPropertyDAO().get("exchange.default.sectioncount.default");

		mav.addObject("sportminiexposure", sportminiexposure.getValue());
		mav.addObject("sportautoexposure", sportautoexposure.getValue());
		mav.addObject("sportvipminiexposure", sportvipminiexposure.getValue());
		mav.addObject("sportvipautoexposure", sportvipautoexposure.getValue());
		mav.addObject("sportlastrowminiexposure",
				sportlastrowminiexposure.getValue());
		mav.addObject("sportlastfiverowminiexposure",
				sportlastfiverowminiexposure.getValue());
		mav.addObject("sportzonespricingexposure",
				sportzonespricingexposure.getValue());
		mav.addObject("sportzonedlastrowminiexposure",
				sportzonedLastRowMiniexposure.getValue());
		mav.addObject("sportshippingMethod", sportshippingmethod.getValue());
		mav.addObject("sportnearTermDisplayOption",
				sportneartermdisplayoption.getValue());
		mav.addObject("sportrpt", sportrpt.getValue());
		mav.addObject("sportpricebreakup", sportpricebreakup.getValue());
		mav.addObject("sportuppermarkup", uppersportmarkup.getValue());
		mav.addObject("sportlowermarkup", lowersportmarkup.getValue());
		mav.addObject("sportuppershipping", uppersportshippingfees.getValue());
		mav.addObject("sportlowershipping", lowersportshippingfees.getValue());
		mav.addObject("sportsectioncount", sportsectionCount.getValue());

		mav.addObject("concertminiexposure", concertminiexposure.getValue());
		mav.addObject("concertautoexposure", concertautoexposure.getValue());
		mav.addObject("concertvipminiexposure",
				concertvipminiexposure.getValue());
		mav.addObject("concertvipautoexposure",
				concertvipautoexposure.getValue());
		mav.addObject("concertlastrowminiexposure",
				concertlastrowminiexposure.getValue());
		mav.addObject("concertlastfiverowminiexposure",
				concertlastfiverowminiexposure.getValue());
		mav.addObject("concertzonespricingexposure",
				concertzonespricingexposure.getValue());
		mav.addObject("concertzonedlastrowminiexposure",
				concertzonedLastRowMiniexposure.getValue());
		mav.addObject("concertshippingMethod", concertshippingmethod.getValue());
		mav.addObject("concertnearTermDisplayOption",
				concertneartermdisplayoption.getValue());
		mav.addObject("concertrpt", concertrpt.getValue());
		mav.addObject("concertpricebreakup", concertpricebreakup.getValue());
		mav.addObject("concertuppermarkup", upperconcertmarkup.getValue());
		mav.addObject("concertlowermarkup", lowerconcertmarkup.getValue());
		mav.addObject("concertuppershipping",
				upperconcertshippingfees.getValue());
		mav.addObject("concertlowershipping",
				lowerconcertshippingfees.getValue());
		mav.addObject("concertsectioncount", concertsectionCount.getValue());

		mav.addObject("theaterminiexposure", theaterminiexposure.getValue());
		mav.addObject("theaterautoexposure", theaterautoexposure.getValue());
		mav.addObject("theatervipminiexposure",
				theatervipminiexposure.getValue());
		mav.addObject("theaterlastrowminiexposure",
				theaterlastrowminiexposure.getValue());
		mav.addObject("theaterlastfiverowminiexposure",
				theaterlastfiverowminiexposure.getValue());
		mav.addObject("theaterzonespricingexposure",
				theaterzonespricingexposure.getValue());
		mav.addObject("theaterzonedlastrowminiexposure",
				theaterzonedLastRowMiniexposure.getValue());
		mav.addObject("theatervipautoexposure",
				theatervipautoexposure.getValue());
		mav.addObject("theatershippingMethod", theatershippingmethod.getValue());
		mav.addObject("theaternearTermDisplayOption",
				theaterneartermdisplayoption.getValue());
		mav.addObject("theaterrpt", theaterrpt.getValue());
		mav.addObject("theaterpricebreakup", theaterpricebreakup.getValue());
		mav.addObject("theateruppermarkup", uppertheatermarkup.getValue());
		mav.addObject("theaterlowermarkup", lowertheatermarkup.getValue());
		mav.addObject("theateruppershipping",
				uppertheatershippingfees.getValue());
		mav.addObject("theaterlowershipping",
				lowertheatershippingfees.getValue());
		mav.addObject("theatersectioncount", theatersectionCount.getValue());
/*
		mav.addObject("vegasminiexposure", vegasminiexposure.getValue());
		mav.addObject("vegasautoexposure", vegasautoexposure.getValue());
		mav.addObject("vegasvipminiexposure", vegasvipminiexposure.getValue());
		mav.addObject("vegasvipautoexposure", vegasvipautoexposure.getValue());
		mav.addObject("vegaslastrowminiexposure",
				vegaslastrowminiexposure.getValue());
		mav.addObject("vegaslastfiverowminiexposure",
				vegaslastfiverowminiexposure.getValue());
		mav.addObject("vegasshippingMethod", vegasshippingmethod.getValue());
		mav.addObject("vegasnearTermDisplayOption",
				vegasneartermdisplayoption.getValue());
		mav.addObject("vegasrpt", vegasrpt.getValue());
		mav.addObject("vegaspricebreakup", vegaspricebreakup.getValue());
		mav.addObject("vegasuppermarkup", uppervegasmarkup.getValue());
		mav.addObject("vegaslowermarkup", lowervegasmarkup.getValue());
		mav.addObject("vegasuppershipping", uppervegasshippingfees.getValue());
		mav.addObject("vegaslowershipping", lowervegasshippingfees.getValue());
		mav.addObject("vegassectioncount", vegassectionCount.getValue());
*/
		mav.addObject("othersminiexposure", othersminiexposure.getValue());
		mav.addObject("othersautoexposure", othersautoexposure.getValue());
		mav.addObject("othersvipminiexposure", othersvipminiexposure.getValue());
		mav.addObject("othersvipautoexposure", othersvipautoexposure.getValue());
		mav.addObject("otherslastrowminiexposure",
				otherslastrowminiexposure.getValue());
		mav.addObject("otherslastfiverowminiexposure",
				otherslastfiverowminiexposure.getValue());
		mav.addObject("otherszonespricingexposure",
				otherszonespricingexposure.getValue());
		mav.addObject("otherszonedlastrowminiexposure",
				otherszonedLastRowMiniexposure.getValue());
		mav.addObject("othersshippingMethod", othersshippingmethod.getValue());
		mav.addObject("othersnearTermDisplayOption",
				othersneartermdisplayoption.getValue());
		mav.addObject("othersrpt", othersrpt.getValue());
		mav.addObject("otherspricebreakup", otherspricebreakup.getValue());
		mav.addObject("othersuppermarkup", upperothersmarkup.getValue());
		mav.addObject("otherslowermarkup", lowerothersmarkup.getValue());
		mav.addObject("othersuppershipping", upperothersshippingfees.getValue());
		mav.addObject("otherslowershipping", lowerothersshippingfees.getValue());
		mav.addObject("otherssectioncount", otherssectionCount.getValue());
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}

	public ModelAndView loadDefaultAutoPricingAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-default-auto-pricing-audit-popup");
		String category = request.getParameter("exCategory");
		System.out.println("category : " + category);
		List<DefaultAutoPricingAudit> list = DAORegistry
				.getDefaultAutoPricingAuditDAO()
				.getDefaultAutoPricingAuditByCategory(category);
		mav.addObject("auditList", list);
		return mav;
	}

	public ModelAndView loadExchangeMarkup(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String sportinfo = "";
		ModelAndView mav = new ModelAndView("page-admin-exchange-markup");
		if ("updateExposure".equalsIgnoreCase(request.getParameter("action"))) {
			String vivid_markup = request.getParameter("markup_vividseat");
			String tn_markup = request.getParameter("markup_tn");
			// String ebay_markup=request.getParameter("markup_ebay");
			// String otherexchange_markup =
			// request.getParameter("markup_tickpick");
			String exchange1Markup = request.getParameter("markup_scorebig");
			String stubhubMarkup = request.getParameter("markup_stubhub");
			String exchange3Markup = request.getParameter("markup_exchange3");
			String exchange4Markup = request.getParameter("markup_exchange4");
			String exchange5Markup = request.getParameter("markup_exchange5");

			PropertyDAO propertyDAO = null;
			com.admitone.tmat.data.Property property = null;
			DefaultExchangeMarkupAudit audit = null;
			String username = SecurityContextHolder.getContext()
					.getAuthentication().getName();
			Date now = new Date();
			try {
				audit = new DefaultExchangeMarkupAudit();
				propertyDAO = DAORegistry.getPropertyDAO();
				audit.setUserName(username);
				audit.setCreatedDate(now);
				audit.setAction("Updated");
				property = propertyDAO.get("exchange.vivid.markup.additional");
				if (property != null) {
					audit.setOldVividSeats(Double.parseDouble(property
							.getValue()));
					property.setValue(vivid_markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.vivid.markup.additional");
					audit.setNewVividSeats(Double.parseDouble(property
							.getValue()));
				}

				property = propertyDAO
						.get("exchange.ticketnetwork.markup.additional");
				if (property != null) {
					audit.setOldTicketNetwork(Double.parseDouble(property
							.getValue()));
					property.setValue(tn_markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.ticketnetwork.markup.additional");
					audit.setNewTicketNetwork(Double.parseDouble(property
							.getValue()));
				}
				/*
				 * property=propertyDAO.get("exchange.ebay.markup.additional");
				 * if(property != null){
				 * audit.setOldEbay(Double.parseDouble(property.getValue()));
				 * property.setValue(ebay_markup);
				 * propertyDAO.saveOrUpdate(property);
				 * property=propertyDAO.get("exchange.ebay.markup.additional");
				 * audit.setNewEbay(Double.parseDouble(property.getValue())); }
				 * property
				 * =propertyDAO.get("exchange.tickpick.markup.additional");
				 * if(property != null){
				 * audit.setOldOtherExchange(Double.parseDouble
				 * (property.getValue()));
				 * property.setValue(otherexchange_markup);
				 * propertyDAO.saveOrUpdate(property);
				 * property=propertyDAO.get("exchange.tickpick.markup.additional"
				 * );
				 * audit.setNewOtherExchange(Double.parseDouble(property.getValue
				 * ())); }
				 */
				property = propertyDAO
						.get("exchange.scorebig.markup.additional");
				if (property != null) {
					audit.setOldScorebig(Double.parseDouble(property.getValue()));
					property.setValue(exchange1Markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.scorebig.markup.additional");
					audit.setNewScorebig(Double.parseDouble(property.getValue()));
				}
				property = propertyDAO
						.get("exchange.stubhub.markup.additional");
				if (property != null) {
					audit.setOldStubhub(Double.parseDouble(property.getValue()));
					property.setValue(stubhubMarkup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.stubhub.markup.additional");
					audit.setNewStubhub(Double.parseDouble(property.getValue()));
				}
				property = propertyDAO
						.get("exchange.exchange3.markup.additional");
				if (property != null) {
					audit.setOldExchange3(Double.parseDouble(property
							.getValue()));
					property.setValue(exchange3Markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.exchange3.markup.additional");
					audit.setNewExchange3(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.exchange4.markup.additional");
				if (property != null) {
					audit.setOldExchange4(Double.parseDouble(property
							.getValue()));
					property.setValue(exchange4Markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.exchange4.markup.additional");
					audit.setNewExchange4(Double.parseDouble(property
							.getValue()));
				}
				property = propertyDAO
						.get("exchange.exchange5.markup.additional");
				if (property != null) {
					audit.setOldExchange5(Double.parseDouble(property
							.getValue()));
					property.setValue(exchange5Markup);
					propertyDAO.saveOrUpdate(property);
					property = propertyDAO
							.get("exchange.exchange5.markup.additional");
					audit.setNewExchange5(Double.parseDouble(property
							.getValue()));
				}
				DAORegistry.getDefaultExchangeMarkupAuditDAO().save(audit);
				sportinfo = "Updated successfully";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Property vividmarkup = DAORegistry.getPropertyDAO().get(
				"exchange.vivid.markup.additional");
		Property tnmarkup = DAORegistry.getPropertyDAO().get(
				"exchange.ticketnetwork.markup.additional");
		// Property ebaymarkup =
		// DAORegistry.getPropertyDAO().get("exchange.ebay.markup.additional");
		// Property tpmarkup =
		// DAORegistry.getPropertyDAO().get("exchange.tickpick.markup.additional");
		Property sbmarkup = DAORegistry.getPropertyDAO().get(
				"exchange.scorebig.markup.additional");
		Property stubhubmarkup = DAORegistry.getPropertyDAO().get(
				"exchange.stubhub.markup.additional");
		Property exchange3markup = DAORegistry.getPropertyDAO().get(
				"exchange.exchange3.markup.additional");
		Property exchange4markup = DAORegistry.getPropertyDAO().get(
				"exchange.exchange4.markup.additional");
		Property exchange5markup = DAORegistry.getPropertyDAO().get(
				"exchange.exchange5.markup.additional");

		mav.addObject("vividmarkup", vividmarkup.getValue());
		mav.addObject("tnmarkup", tnmarkup.getValue());
		// mav.addObject("ebaymarkup", ebaymarkup.getValue());
		// mav.addObject("tickpickmarkup", tpmarkup.getValue());
		mav.addObject("scorebigmarkup", sbmarkup.getValue());
		mav.addObject("stubhubmarkup", stubhubmarkup.getValue());
		mav.addObject("exchange3markup", exchange3markup.getValue());
		mav.addObject("exchange4markup", exchange4markup.getValue());
		mav.addObject("exchange5markup", exchange5markup.getValue());
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}

	public ModelAndView loadExchangeMarkupAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-default-exchange-markup-audit-popup");
		List<DefaultExchangeMarkupAudit> list = DAORegistry
				.getDefaultExchangeMarkupAuditDAO()
				.getDefaultExchangeMarkupAuditAll();
		mav.addObject("auditList", list);
		return mav;
	}

	public ModelAndView loadLogFiles(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		String sportinfo = "";
		String brokerId=request.getParameter("brokerId");
		
		ModelAndView mav = new ModelAndView("page-admin-log-files");
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAll());
		if ("auto_download".equalsIgnoreCase(request.getParameter("action"))) {
			String exchange = request.getParameter("auto_exchange");
			String link = request.getParameter("auto_link");
			
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadTndCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTndCategoryTickets"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadTndCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTndCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadTndErrors")) {
					System.out
							.println("Inside tn exchange and DownloadTndErrors");
					return new ModelAndView(new RedirectView(
							"DownloadTndErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndConcertsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndConcertsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividConcertsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndConcertsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndConcertsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividConcertsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndSportsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndSportsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividSportsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndSportsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndSportsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividSportsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndTheaterCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTndTheaterCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividTheaterCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTndTheaterCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTndTheaterCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadAutoVividTheaterCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickAutoCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickAutoCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTndCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigAutoCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTndCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigAutoCatsCategoryEvents"));
				}
				if (exchange.equals("exchange1")
						&& link.equals("DownloadTndErrors")) {
					System.out
							.println("Inside exchange1 and DownloadTndErrors");
				}
			}

		} else if ("mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("mini_exchange");
			String link = request.getParameter("mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()){
				 sportinfo = "Please select Broker !";
			    }
			 else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			   } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadTGCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadTGErrors")) {
					System.out
							.println("Inside tn exchange and DownloadTGErrors");
					return new ModelAndView(new RedirectView(
							"DownloadTGCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGConcertsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGConcertsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGConcertsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGSportsCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGSportsCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGSportsCategoryEvents"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadTGTheaterCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadTGTheaterCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividTGTheaterCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickTGCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTGCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadTGCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigTGCatsCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadTGCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadTGCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigTGCatsCategoryEvents"));
				}
				if (exchange.equals("exchange1")
						&& link.equals("DownloadTGErrors")) {
					System.out.println("Inside exchange1 and DownloadTGErrors");
				}
			}
		} else if ("vip_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("vip_mini_exchange");
			String link = request.getParameter("vip_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			    }
			     else if (exchange.isEmpty() || link.isEmpty()) {
					sportinfo = "Please select exchange and file !";
				   } else {
					if (exchange.equals("tn")
							&& link.equals("DownloadVipCategoryTickets")) {
						System.out
								.println("Inside tn exchange and DownloadVipCategoryTickets");
						return new ModelAndView(new RedirectView(
								"DownloadVipCatsCategoryTickets?brokerId="+brokerId));
					}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVipCatsCategoryEvents"));
				}
				if (exchange.equals("tn") && link.equals("DownloadVipErrors")) {
					System.out
							.println("Inside tn exchange and DownloadVipErrors");
					return new ModelAndView(new RedirectView(
							"DownloadVipCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadVividVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipCatsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipCatsCategoryEvents"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside tickPick exchange and DownloadVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickVipCatsCategoryTickets"));
				}
				if (exchange.equals("tickPick")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside tickPick exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTickPickVipCatsCategoryEvents"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipCatsCategoryEvents"));
				}
			}
		} else if ("vip_auto_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("vip_auto_exchange");
			String link = request.getParameter("vip_auto_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsCategoryEvents"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadVipAutoErrors")) {
					System.out
							.println("Inside tn exchange and DownloadVipAutoErrors");
					return new ModelAndView(new RedirectView(
							"DownloadVipAutoCatsErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadVividVipCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside vivid exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadVividVipAutoCatsCategoryEvents"));
				}
				/*
				 * if(exchange.equals("tickPick") &&
				 * link.equals("DownloadVipAutoCategoryTickets")) {
				 * System.out.println(
				 * "Inside tickPick exchange and DownloadVipAutoCatsCategoryTickets"
				 * ); return new ModelAndView(new
				 * RedirectView("DownloadTickPickVipAutoCatsCategoryTickets"));
				 * } if(exchange.equals("tickPick") &&
				 * link.equals("DownloadVipAutoCategoryEvents")) {
				 * System.out.println
				 * ("Inside tickPick exchange and DownloadVipAutoCategoryEvents"
				 * ); return new ModelAndView(new
				 * RedirectView("DownloadTickPickVipAutoCatsCategoryEvents")); }
				 */
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipAutoCategoryTickets")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipAutoCatsCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipAutoCatsCategoryTickets"));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadVipAutoCategoryEvents")) {
					System.out
							.println("Inside scoreBig exchange and DownloadVipAutoCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadScoreBigVipAutoCatsCategoryEvents"));
				}
			}
		} else if ("lastrow_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("lastrow_mini_exchange");
			String link = request.getParameter("lastrow_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			  else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			     } else {
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadTNLastRowMiniCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniCategoryEvents")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniCategoryEvents");
					return new ModelAndView(new RedirectView(
							"DownloadTNLastRowMiniCategoryEvents"));
				}
				if (exchange.equals("tn")
						&& link.equals("DownloadLastRowMiniErrors")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniErrors");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMiniErrors"));
				}
				if (exchange.equals("vivid")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside vivid exchange and DownloadLastRowMiniCategoryTickets");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMinivividCategoryTickets?brokerId="+brokerId));
				}
				if (exchange.equals("scoreBig")
						&& link.equals("DownloadLastRowMiniCategoryTickets")) {
					System.out
							.println("Inside tn exchange and DownloadLastRowMiniErrors");
					return new ModelAndView(new RedirectView(
							"DownloadLastRowMiniscoreBigCategoryTickets?brokerId="+brokerId));
				}

			}
		} else if ("lastfiverow_mini_download".equalsIgnoreCase(request
				.getParameter("action"))) {
			String exchange = request.getParameter("lastfiverow_mini_exchange");
			String link = request.getParameter("lastfiverow_mini_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("stubhub")
						&& link.equals("DownloadLastFiveRowMiniCatTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadStubhubLastFiveRowMiniCatTickets"));
				}
				if (exchange.equals("stubhub")
						&& link.equals("DownloadLastFiveRowMiniCatEvents")) {
					return new ModelAndView(new RedirectView(
							"DownloadStubhubLastFiveRowMiniCatEvents"));
				}

			}
		}else if("zones_pric_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("zones_pric_exchange");
			String link = request.getParameter("zones_pric_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadZonesPricingCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadZonesPricingTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if("zoned_lastrow_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("zoned_last_exchange");
			String link = request.getParameter("zoned_last_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadZonedLastRowCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadZonedLastRowTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}else if("larry_last_download".equalsIgnoreCase(request
				.getParameter("action"))){
			String exchange = request.getParameter("Larry_last_exchange");
			String link = request.getParameter("Larry_last_link");
			System.out.println("Exchange : " + exchange);
			System.out.println("link : " + link);
			if(brokerId.isEmpty()) {
				 sportinfo = "Please select Broker !";
			  }
			else if (exchange.isEmpty() || link.isEmpty()) {
				sportinfo = "Please select exchange and file !";
			} else {
				if (exchange.equals("tn")
						&& link.equals("DownloadLarryLastCategoryTickets")) {
					return new ModelAndView(new RedirectView(
							"DownloadLarryLastTnCategoryTickets?brokerId="+brokerId));
				}
			
			}	
			
		}
		
	
		
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}
	public void downloadZonesPricingTnCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		 String brokerId=request.getParameter("brokerId");
		 
		 if(brokerId.equals("2")){
			 response.setHeader("Content-Type", "application/vnd.ms-excel");
			 response.setHeader("Content-disposition",
						"attachment; filename=JKTZonesPricingTNCategoryTickets." + today + ".csv");
		 }else  if(brokerId.equals("3")){
			 response.setHeader("Content-Type", "application/vnd.ms-excel");
			 response.setHeader("Content-disposition",
						"attachment; filename=ROTZonesPricingTNCategoryTickets." + today + ".csv");
		 } else  if(brokerId.equals("5")){
			 response.setHeader("Content-Type", "application/vnd.ms-excel");
			 response.setHeader("Content-disposition",
						"attachment; filename=TixCityZonesPricingTNCategoryTickets." + today + ".csv");
		 }
		
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
		
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qauntity,Section,Row,Price\n"
					.getBytes());     
		    							   
		    List<ZonePricingTnCategoryTicket> tixs = DAORegistry.getQueryManagerDAO().getTnZonesCategoryTickets(brokerId);
					

			for (ZonePricingTnCategoryTicket cat : tixs) {
				
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenue() + ",";
				outString += cat.getQuantity() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRow() + ",";
				outString += cat.getPrice() + "\n";
				out.write(outString.getBytes());
			}
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}	
	
	
	public void downloadZonedLastRowTnCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2"))
		{
		    response.setHeader("Content-Type", "application/vnd.ms-excel");
		    response.setHeader("Content-disposition",
				 "attachment; filename=JKTZonedLastRowTNCategoryTickets." + today + ".csv");
		}else if(brokerId.equals("3")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTZonedLastRowTNCategoryTickets." + today + ".csv");
		} else if(brokerId.equals("5")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityZonedLastRowTNCategoryTickets." + today + ".csv");
		}
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
		
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
					.getBytes());
			
		   		    
		    List<ZonedLastRowTnCategoryTicket> tixs = DAORegistry.getQueryManagerDAO().getTnZonedLastRowCategoryTickets(brokerId);
					

			for (ZonedLastRowTnCategoryTicket cat : tixs) {
				
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenue() + ",";
				outString += cat.getQuantity() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRow() + ".,";
				outString += cat.getPrice() + "\n";
              
				out.write(outString.getBytes());
			}
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}	
		
		
	public void downloadLarryLastTnCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=JKTLarryLastTNCategoryTickets." + today + ".csv");
		}else if(brokerId.equals("3")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTLarryLastTNCategoryTickets." + today + ".csv");
		} else if(brokerId.equals("5")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityLarryLastTNCategoryTickets." + today + ".csv");
		}
		
		
		
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
		
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Quantity,Section,Row,Price\n"
					.getBytes());
			
			
		    List<LarryLastTnCategoryTicket> tixs = DAORegistry.getQueryManagerDAO().getTnLarryLastCategoryTickets(brokerId);
					

			for (LarryLastTnCategoryTicket cat : tixs) {
				
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenue() + ",";
				outString += cat.getQuantity() + ",";
				//outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRow() + ".,";
				outString += cat.getPrice() + "\n";
                
				
				out.write(outString.getBytes());
			}
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}	
	public void downloadTndCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoTNCategoryTickets." + today + ".csv");
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Zone,Section,Row,Price,ExpectedArrivalDate,ShippingMethod,NearTermDisplayOption\n"
					.getBytes());
			Collection<CategoryTicket> tixs = DAORegistry
					.getCategoryTicketDAO().getAll();

			for (CategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenue() + ",";
				outString += cat.getQuantity() + ",";
				outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRow() + ".,";
				outString += cat.getPrice() + ",";
				outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";
				out.write(outString.getBytes());
			}
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTndCategoryEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoTNCategoryEvents." + today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TNListedEvent> events = DAORegistry.getQueryManagerDAO()
					.getAllTicketNetworkCategoryEvents();

			for (TNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTndErrors(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoTNErrors." + today + ".csv");
		try {
			Collection<Event> events = DAORegistry.getEventDAO().getAll();
			Map<String, Event> eventMap = new HashMap<String, Event>();
			for (Event event : events) {
				eventMap.put(event.getId() + "", event);
			}
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,Process,Message,Example\n"
					.getBytes());
			Collection<TNError> errors = DAORegistry.getTnErrorDAO()
					.getAllTNErrorByDate(new Date());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dt = new SimpleDateFormat("HH:mm");
			for (TNError error : errors) {
				String outString = "";
				Event event = eventMap.get(error.getEventId() + "");
				outString += error.getEventId() + ",";
				if (event == null) {
					outString += ",,,";
				} else {
					outString += event.getName().replaceAll(",", "-") + ",";
					outString += event.getDate() == null ? "TBD," : df
							.format(event.getDate()) + ",";
					outString += event.getTime() == null ? "TBD," : dt
							.format(event.getTime()) + ",";
				}
				// outString += error.getEventId() + "," ;
				outString += error.getProcess() + ",";
				outString += error.getMessage() + ",";
				outString += error.getExample().replaceAll(",", ":") + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadAutoVividConcertsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividConcertsCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\CATS\\vividseats\\concerts\\vividseatsconcertscats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadAutoVividSportsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividSportsCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\CATS\\vividseats\\sports\\vividseatssportscats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadAutoVividTheaterCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividTheaterCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\CATS\\vividseats\\theater\\vividseatstheatercats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadAutoVividConcertsCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividConcertsCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<VividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAutoVividConcertsCategoryEvents();

			for (VividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadAutoVividSportsCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividSportsCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<VividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAutoVividSportsCategoryEvents();

			for (VividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadAutoVividTheaterCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoVividTheaterCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<VividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAutoVividTheaterCategoryEvents();

			for (VividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadAutoCatsTickPickCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoTickPickCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\CATS\\tickpick\\tickpickcats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadAutoCatsTickPickCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoTickPickCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TickPickListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTickPickCategoryEvents();

			for (TickPickListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadAutoCatsScoreBigCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoScoreBigCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\CATS\\scorebig\\scorebigcats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadAutoCatsScoreBigCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=AutoScoreBigCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<ScoreBigListedEvent> events = DAORegistry
					.getQueryManagerDAO().getScoreBigCategoryEvents();

			for (ScoreBigListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTGCatsCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2")){
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=JKTMiniCatsTNCategoryTickets." + today
						+ ".csv");
		}
		else if(brokerId.equals("3")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTMiniCatsTNCategoryTickets." + today
							+ ".csv");
		} else if(brokerId.equals("5")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityMiniCatsTNCategoryTickets." + today
							+ ".csv");
		}
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
			    OutputStream out = response.getOutputStream();
			    out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
					.getBytes());
    
	      
			    List<VWTGCatsCategoryTicket> tixs=null;
			 		
				System.out.println("BrokerId:"+brokerId);
				tixs=DAORegistry.getQueryManagerDAO().getTnTgCategoryTickets(brokerId);
				
				String dateInString=null;
				String newDate=null;     
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
					  
					  			
			for (VWTGCatsCategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenueName().replaceAll(",", "-") + ",";
				outString += cat.getQuantity() + ",";
				// outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRowRange() + ".,";
				outString += cat.getTnPrice() + "\n";
				/*outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";*/
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}


	public void downloadTGCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsTNCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllTGTNCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTGCatsErrors(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsTNErrors." + today + ".csv");
		try {
			Collection<com.admitone.tmat.data.Event> events = DAORegistry
					.getEventDAO().getAll();
			Map<String, com.admitone.tmat.data.Event> eventMap = new HashMap<String, com.admitone.tmat.data.Event>();
			for (com.admitone.tmat.data.Event event : events) {
				eventMap.put(event.getId() + "", event);
			}
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,Process,Message,Example\n"
					.getBytes());
			Collection<TGCatsError> errors = DAORegistry.getTgCatsErrorDAO()
					.getAllTGCatsErrorByDate(new Date());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dt = new SimpleDateFormat("HH:mm");
			for (TGCatsError error : errors) {
				String outString = "";
				com.admitone.tmat.data.Event event = eventMap.get(error
						.getEventId() + "");
				outString += error.getEventId() + ",";
				if (event == null) {
					outString += ",,,";
				} else {
					outString += event.getName().replaceAll(",", "-") + ",";
					outString += (event.getLocalDate() == null ? "TBD," : df
							.format(event.getLocalDate())) + ",";
					outString += (event.getLocalTime() == null ? "TBD," : dt
							.format(event.getLocalTime())) + ",";
				}
				// outString += error.getEventId() + "," ;
				outString += error.getProcess() + ",";
				outString += error.getMessage() + ",";
				outString += error.getExample().replaceAll(",", ":") + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}
	public void downloadVividTGCatsCategoryTickets(HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		
		String brokerId=request.getParameter("brokerId");
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
	    String today = dateFormat.format(new Date());
	    response.setHeader("Content-Type", "application/vnd.ms-excel");
	                                                                  
	    BufferedReader br = null;
	    StringBuilder sb = new StringBuilder();
	    
	    if(brokerId.equals("2")) {
	    	response.setHeader("Content-disposition",
	    			"attachment; filename=JKTMiniCatsVividCategoryTickets."
	    					+ today + ".csv");	
	    }else  if(brokerId.equals("3")){
	    	response.setHeader("Content-disposition",
	    			"attachment; filename=ROTMiniCatsVividCategoryTickets."
	    					+ today + ".csv");
	    } else {
	    	response.setHeader("Content-disposition",
	    			"attachment; filename=TixCityMiniCatsVividCategoryTickets."
	    					+ today + ".csv");
	    
	    }
	    

	try {
		OutputStream out = response.getOutputStream();
		
		if(brokerId.equals("2")){//jkt
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"JKT\\JKTvividseatsminicats.csv");
			br = new BufferedReader(
					new FileReader(file));
		}else if(brokerId.equals("3")){
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTvividseatsminicats.csv");
			br = new BufferedReader(
					new FileReader(file));			
		} else if(brokerId.equals("5"))	 {
			File file=new File(mailIdProperty.getAutopricingfolderpath()+"TIXCITY\\TIXCITYvividseatsminicats.csv");
			br = new BufferedReader(
					new FileReader(file));	
		}
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			sb.append(sCurrentLine + "\n");
		}
		out.write(sb.toString().getBytes());
	} catch (IOException e) {
		System.out.println(e.fillInStackTrace());
	} finally {
		try {
		    	br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
			
		
}
	public void downloadVividTGCatsConcertsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividConcertsCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\MINICATS\\vividseats\\concerts\\vividseatsconcertsminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVividTGCatsSportsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividSportsCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\MINICATS\\vividseats\\sports\\vividseatssportsminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVividTGCatsTheaterCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividTheaterCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\MINICATS\\vividseats\\theater\\vividseatstheaterminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVividTGCatsConcertsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividConcertsCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGVividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTGVividConcertsCategoryEvents();

			for (TGVividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVividTGCatsSportsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividSportsCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGVividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTGVividSportsCategoryEvents();

			for (TGVividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVividTGCatsTheaterEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsVividTheaterCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGVividListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTGVividTheaterCategoryEvents();

			for (TGVividListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTGCatsTickPickCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsTickPickCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\MINICATS\\tickpicks\\tickpickminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadTGCatsTickPickCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsTickPickCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTickPickListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTGTickPickCategoryEvents();

			for (TGTickPickListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTGCatsScoreBigCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2")){
			response.setHeader("Content-disposition",
					"attachment; filename=JKTMiniCatsScoreBigCategoryTickets." + today
							+ ".csv");
		}else if(brokerId.equals("3")){
			response.setHeader("Content-disposition",
					"attachment; filename=ROTMiniCatsScoreBigCategoryTickets." + today
							+ ".csv");	
		}else if(brokerId.equals("5")){
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityMiniCatsScoreBigCategoryTickets." + today
							+ ".csv");	
		}

		try {
			OutputStream out = response.getOutputStream();
			if(brokerId.equals("2")){//jkt
				File file=new File(mailIdProperty.getAutopricingfolderpath()+"JKT\\JKTscorebigminicats.csv");
				br = new BufferedReader(
						new FileReader(file));
			}else if(brokerId.equals("3")){
				File file=new File(mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigminicats.csv");
				br = new BufferedReader(
						new FileReader(file));	
			} else if(brokerId.equals("5")){
				File file=new File(mailIdProperty.getAutopricingfolderpath()+"TIXCITY\\TIXCITYscorebigminicats.csv");
				br = new BufferedReader(
						new FileReader(file));	
			}
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void downloadLastRowMinivividCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		
		System.out.println("In DownloadLastRowMinivividCategoryTickets");
		
		String brokerId=request.getParameter("brokerId");
		
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		
		if(brokerId.equals("2")){
			response.setHeader("Content-disposition",
					"attachment; filename=JKTLastRowMiniCatsvividCategoryTickets." + today
							+ ".csv");
		} else if(brokerId.equals("3")){
			response.setHeader("Content-disposition",
					"attachment; filename=ROTLastRowMiniCatsvividCategoryTickets." + today
							+ ".csv");
		} else if(brokerId.equals("5")){
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityLastRowMiniCatsvividCategoryTickets." + today
							+ ".csv");
		}
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			if(brokerId.equals("2")){//jkt
				System.out.println("in jkt vivid file read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"JKT\\JKTvividseatslastrowmiinicats.csv");
				br = new BufferedReader(
						new FileReader(file));
			}else if(brokerId.equals("3")){
				System.out.println("in rot vividfile read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTvividseatslarrytheater.csv");
				br = new BufferedReader(
						new FileReader(file));	
			} else if(brokerId.equals("5")){
				System.out.println("in TixCity vividfile read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"TIXCITY\\TIXCITYvividseatslastrowmiinicats.csv");
				br = new BufferedReader(
						new FileReader(file));	
			}
			
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	public void downloadLastRowMiniscoreBigCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		
		System.out.println("In  downloadLastRowMiniscoreBigCategoryTickets");
        String brokerId=request.getParameter("brokerId");
		
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		
		if(brokerId.equals("2")){
			response.setHeader("Content-disposition",
					"attachment; filename=JKTLastRowMiniCatsscoreBigCategoryTickets." + today
							+ ".csv");
		}else if(brokerId.equals("3")){
			response.setHeader("Content-disposition",
					"attachment; filename=ROTLastRowMiniCatsscoreBigCategoryTickets." + today
							+ ".csv");
		} else if(brokerId.equals("5")){
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityLastRowMiniCatsscoreBigCategoryTickets." + today
							+ ".csv");
		}
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			if(brokerId.equals("2")){//jkt
				System.out.println("in jkt scoribig file read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"JKT\\JKTscorebigcats.csv");
				br = new BufferedReader(
						new FileReader(file));
			}else if(brokerId.equals("3")){
				System.out.println("in rot scoribig file read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"ROT\\ROTscorebigcats.csv");
				br = new BufferedReader(
						new FileReader(file));	
			} else if(brokerId.equals("5")){
				System.out.println("in TixCity scoribig file read");
				File file=new File(
						mailIdProperty.getAutopricingfolderpath()+"TIXCITY\\TIXCITYscorebigcats.csv");
				br = new BufferedReader(
						new FileReader(file));	
			}
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void downloadVipCatsCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2"))
		{
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=JKTVipMiniCatsTNCategoryTickets." + today
							+ ".csv");
		}else if(brokerId.equals("3")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTVipMiniCatsTNCategoryTickets." + today
							+ ".csv");
		} else if(brokerId.equals("5")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityVipMiniCatsTNCategoryTickets." + today
							+ ".csv");
		}
		
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
					.getBytes());
			List<VWVipCatsCategoryTicket> tixs=null;
			
				tixs=DAORegistry.getQueryManagerDAO().getVwVipCatsCategoryTicketDAO(brokerId);
			
			/*Collection<VWVipCatsCategoryTicket> tixs = DAORegistry
					.getVwVipCatsCategoryTicketDAO()
					.getAllActiveTnVipCatsCategoryTickets();*/
              
				String dateInString=null;
				String newDate=null;     
									
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
 						
			for (VWVipCatsCategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenueName().replaceAll(",", "-") + ",";
				outString += cat.getQuantity() + ",";
				// outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getAlternateRow() + ".,";
				outString += cat.getTnPrice() + "\n";
				
				/*outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";*/
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsTNCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipTnCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipCatsErrors(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsTNErrors." + today + ".csv");
		try {
			Collection<com.admitone.tmat.data.Event> events = DAORegistry
					.getEventDAO().getAll();
			Map<String, com.admitone.tmat.data.Event> eventMap = new HashMap<String, com.admitone.tmat.data.Event>();
			for (com.admitone.tmat.data.Event event : events) {
				eventMap.put(event.getId() + "", event);
			}
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,Process,Message,Example\n"
					.getBytes());
			Collection<VipCatsError> errors = DAORegistry.getVipCatsErrorDAO()
					.getAllVipCatsErrorByDate(new Date());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dt = new SimpleDateFormat("HH:mm");
			for (VipCatsError error : errors) {
				String outString = "";
				com.admitone.tmat.data.Event event = eventMap.get(error
						.getEventId() + "");
				outString += error.getEventId() + ",";
				if (event == null) {
					outString += ",,,";
				} else {
					outString += event.getName().replaceAll(",", "-") + ",";
					outString += (event.getLocalDate() == null ? "TBD," : df
							.format(event.getLocalDate())) + ",";
					outString += (event.getLocalTime() == null ? "TBD," : dt
							.format(event.getLocalTime())) + ",";
				}
				// outString += error.getEventId() + "," ;
				outString += error.getProcess() + ",";
				outString += error.getMessage() + ",";
				outString += error.getExample().replaceAll(",", ":") + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVividVipCatsCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsVividCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\VIPMINICATS\\vividseats\\vividseatsvipminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVividVipCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsVividCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipVividCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipCatsTickPickCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsTickPickCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\VIPMINICATS\\tickpicks\\tickpickvipminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVipCatsTickPickCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsTickPickCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipTickPickCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipCatsScoreBigCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsScoreBigCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\VIPMINICATS\\scorebig\\scorebigvipminicats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVipCatsScoreBigCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipMiniCatsScoreBigCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipScoreBigCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipAutoCatsCategoryTickets(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsTNCategoryTickets." + today
						+ ".csv");
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");

		try {
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price,ExpectedArrivalDate,ShippingMethod,NearTermDisplayOption\n"
					.getBytes());

			Collection<VwVipAutoCategoryTicket> tixs = DAORegistry
					.getVwVipAutoCategoryTicketDAO().getAll();

			// DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			// DateFormat tf = new SimpleDateFormat("hh:mm aa");
			// DateFormat dateTimeformat = new
			// SimpleDateFormat("MM/dd/yyyy HH:mm");

			for (VwVipAutoCategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenue().replaceAll(",", "-") + ",";
				outString += cat.getQuantity() + ",";
				// outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getRow() + ".,";
				outString += cat.getPrice() + ",";
				outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipAutoCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsTNCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipAutoTnCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipAutoCatsErrors(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsTNErrors." + today + ".csv");
		try {
			Collection<com.admitone.tmat.data.Event> events = DAORegistry
					.getEventDAO().getAll();
			Map<String, com.admitone.tmat.data.Event> eventMap = new HashMap<String, com.admitone.tmat.data.Event>();
			for (com.admitone.tmat.data.Event event : events) {
				eventMap.put(event.getId() + "", event);
			}
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,Process,Message,Example\n"
					.getBytes());
			Collection<VipAutoCatsError> errors = DAORegistry
					.getVipAutoCatsErrorDAO().getAllVipAutoCatsErrorByDate(
							new Date());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dt = new SimpleDateFormat("HH:mm");
			for (VipAutoCatsError error : errors) {
				String outString = "";
				com.admitone.tmat.data.Event event = eventMap.get(error
						.getEventId() + "");
				outString += error.getEventId() + ",";
				if (event == null) {
					outString += ",,,";
				} else {
					outString += event.getName().replaceAll(",", "-") + ",";
					outString += (event.getLocalDate() == null ? "TBD," : df
							.format(event.getLocalDate())) + ",";
					outString += (event.getLocalTime() == null ? "TBD," : dt
							.format(event.getLocalTime())) + ",";
				}
				// outString += error.getEventId() + "," ;
				outString += error.getProcess() + ",";
				outString += error.getMessage() + ",";
				outString += error.getExample().replaceAll(",", ":") + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVividVipAutoCatsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsVividCategoryTickets." + today
						+ ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\VIPAUTOCATS\\vividseats\\vividseatsvipautocats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVividVipAutoCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsVividCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipAutoVividCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadVipAutoCatsScoreBigCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsScoreBigCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\VIPAUTOCATS\\scorebig\\scorebigvipautocats.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadVipAutoCatsScoreBigCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VipAutoCatsScoreBigCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllVipAutoScoreBigCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadTGCatsScoreBigCategoryEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=MiniCatsScoreBigCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGScoreBigListedEvent> events = DAORegistry
					.getQueryManagerDAO().getTGScoreBigCategoryEvents();

			for (TGScoreBigListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadLastRowMiniCatsCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		String brokerId=request.getParameter("brokerId");
		
		if(brokerId.equals("2"))
		{
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=JKTLastRowMiniCatsTNCategoryTickets."
							+ today + ".csv");
			
		}else if(brokerId.equals("3")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=ROTLastRowMiniCatsTNCategoryTickets."
							+ today + ".csv");
		} else if(brokerId.equals("5")) {
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=TixCityLastRowMiniCatsTNCategoryTickets."
							+ today + ".csv");
		}
		
		Map<Integer, String> shippingMap = new HashMap<Integer, String>();
		shippingMap.put(0, "Default Website");
		shippingMap.put(1, "E-Ticket");
		shippingMap.put(2, "Will Call");
		shippingMap.put(3, "Local Pickup Near Venue");
		shippingMap.put(4, "E-Ticket or Will Call");
		shippingMap.put(5, "Will Call or Local Pickup Near Venue");
		shippingMap.put(6, "E-Ticket or Local Pickup Near Venue");
		shippingMap.put(7, "E-Ticket or Will Call or Local Pickup Near Venue");
		shippingMap.put(8, "Paperless(Meet Seller at Venue)");
		shippingMap.put(9, "Electronic Transfer");

		Map<Integer, String> nearTermMap = new HashMap<Integer, String>();
		nearTermMap.put(0, "Default near term options");
		nearTermMap.put(1, "Always show term shipping");
		nearTermMap.put(2, "Only show term shipping");
         
		
		try {
			OutputStream out = response.getOutputStream();
			out.write("EventName,EventDate,EventTime,Venue,Qty,Section,Row,Price\n"
					.getBytes());

			List<VWLastRowMiniCatsCategoryTicket>  tixs=DAORegistry.getQueryManagerDAO().getTnLastRowMiniCatsCatsCategoryTickets(brokerId);
			
		/*	Collection<VWLastRowMiniCatsCategoryTicket> tixs = DAORegistry
					.getVwLastRowMiniCatsCategoryTicketDAO()
					.getAllActiveTnLastRowMiniCatsCatsCategoryTickets();*/

			//DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			//DateFormat tf = new SimpleDateFormat("hh:mm aa");
			// DateFormat dateTimeformat = new
			// SimpleDateFormat("MM/dd/yyyy HH:mm");

		    String dateInString=null;
		    String newDate=null;     
		    					
		    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		    
			for (VWLastRowMiniCatsCategoryTicket cat : tixs) {
				String outString = "";
				outString = cat.getEventName().replaceAll(",", "-") + ",";
				outString += cat.getEventDate() + ",";
				outString += cat.getEventTime() + ",";
				outString += cat.getVenueName().replaceAll(",", "-") + ",";
				outString += cat.getQuantity() + ",";
				// outString += cat.getZone() + ",";
				outString += cat.getSection() + ".,";
				outString += cat.getLastRow() + ".,";
				outString += cat.getTnPrice() + "\n";
				/*outString += cat.getExpectedArrivalDate() + ",";
				outString += shippingMap.get(cat.getShippingMethodId()) + ",";
				outString += nearTermMap.get(cat.getNearTermDisplayOptionId())
						+ "\n";*/
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	

	public void downloadLastRowMiniCatsEvents(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=LastRowMiniCatsTNCategoryEvents." + today
						+ ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO().getAllLastRowMiniTNCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadLastRowMiniCatsErrors(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=LastRowMiniCatsTNErrors." + today
						+ ".csv");
		try {
			Collection<com.admitone.tmat.data.Event> events = DAORegistry
					.getEventDAO().getAll();
			Map<String, com.admitone.tmat.data.Event> eventMap = new HashMap<String, com.admitone.tmat.data.Event>();
			for (com.admitone.tmat.data.Event event : events) {
				eventMap.put(event.getId() + "", event);
			}
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,Process,Message,Example\n"
					.getBytes());
			Collection<LastRowMiniCatsError> errors = DAORegistry
					.getLastRowMiniCatsErrorDAO()
					.getAllLastRowMiniCatsErrorErrorByDate(new Date());
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dt = new SimpleDateFormat("HH:mm");
			for (LastRowMiniCatsError error : errors) {
				String outString = "";
				com.admitone.tmat.data.Event event = eventMap.get(error
						.getEventId() + "");
				outString += error.getEventId() + ",";
				if (event == null) {
					outString += ",,,";
				} else {
					outString += event.getName().replaceAll(",", "-") + ",";
					outString += (event.getLocalDate() == null ? "TBD," : df
							.format(event.getLocalDate())) + ",";
					outString += (event.getLocalTime() == null ? "TBD," : dt
							.format(event.getLocalTime())) + ",";
				}
				// outString += error.getEventId() + "," ;
				outString += error.getProcess() + ",";
				outString += error.getMessage() + ",";
				outString += error.getExample().replaceAll(",", ":") + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public void downloadLastFiveRowMiniCatStubhubCategoryTickets(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=LastFiveRowMiniCatStubhubCategoryTickets."
						+ today + ".csv");
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		try {
			OutputStream out = response.getOutputStream();
			br = new BufferedReader(
					new FileReader(
							"\\\\"
									+ mailIdProperty.getExternalFileServerUrl()
									+ "\\c$\\external files\\Autopricing\\LASTROWMINICATS\\STUBHUB\\stubhubtheaterlr.csv"));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				sb.append(sCurrentLine + "\n");
			}
			out.write(sb.toString().getBytes());
		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void downloadLastFiveRowMiniCatStubhubEvents(
			HttpServletRequest request, HttpServletResponse response,
			ModelMap map) throws JSONException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=LastFiveRowMiniCatsStubhubCategoryEvents."
						+ today + ".csv");
		try {
			OutputStream out = response.getOutputStream();
			out.write("Id,EventName,EventDate,EventTime,FrequentEvent\n"
					.getBytes());
			Collection<TGTNListedEvent> events = DAORegistry
					.getQueryManagerDAO()
					.getAllLastFiveRowMiniCatStubhubCategoryEvents();

			for (TGTNListedEvent event : events) {
				String outString = "";
				outString += event.getId() + ",";
				outString += event.getName().replaceAll(",", "-") + ",";
				outString += event.getEventDate() + ",";
				outString += event.getEventTime() + ",";
				outString += event.isFrequentEvent() + "\n";
				out.write(outString.getBytes());
			}

		} catch (IOException e) {
			System.out.println(e.fillInStackTrace());
		}
	}

	public ModelAndView loadAutoPricingTicketDetails(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Collection<Event> events = null;
		String application = request.getParameter("application");
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		String eventIdString = request.getParameter("eventId");
		String section = request.getParameter("section");
		String row = request.getParameter("row");
		String quantity = request.getParameter("quantity");
		String showgrid="false";
		String artistName = new String();
		Integer eventId;
		
		int brokerId=0;
		if(eventIdString!=null){
		Event event=DAORegistry.getEventDAO().get(Integer.parseInt(eventIdString));
		brokerId=event.getBrokerId();
		System.out.println("....brokerId:"+brokerId);
		}
		ModelAndView mav = new ModelAndView("page-auto-pricing-ticket-details");
		List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
		List<Venue> venues = (List<Venue>) DAORegistry.getVenueDAO().getAll();

		Collections.sort(artists, new Comparator<Artist>() {
			public int compare(Artist t1, Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		});

		mav.addObject("artists", artists);
		mav.addObject("venues", venues);
		mav.addObject("selectedApplication", application);
		

		if (null != artistIdString && !artistIdString.isEmpty()) {
			Integer artistIdTemp = Integer.valueOf(artistIdString.trim());
			Artist artist = DAORegistry.getArtistDAO().get(artistIdTemp);
			artistName = artist.getName();
			mav.addObject("selectedTourId",
					Integer.valueOf(artistIdString.trim()));
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistIdTemp);
		} else if (null != venueIdString && !venueIdString.isEmpty()) {
			mav.addObject("selectedVenueId",
					Integer.valueOf(venueIdString.trim()));
			events = DAORegistry.getEventDAO().getAllEventsByVenue(
					Integer.valueOf(venueIdString.trim()));
		}
		
		mav.addObject("artistName", artistName);
		if (artistIdString == null && venueIdString == null) {

			// look for a value in the preference
			// tourIdString = preferenceManager.getPreferenceValue(username,
			// "editorCrawlerTourId");

			return mav;

		} else if (eventIdString == null) {
			return mav;
		}

		

		if (eventIdString != null && eventIdString.trim().length() > 0) {
			 showgrid="true";
			eventId = Integer.valueOf(eventIdString);
			mav.addObject("selectedEventId", eventId);
			System.out.println("iiiiid:"+eventId);
			List<TGCatsCategoryTicket> categoryTicketsList = new ArrayList<TGCatsCategoryTicket>();
			categoryTicketsList = DAORegistry.getTgCatsCategoryTicketDAO().getAllTgCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("categoryTickets", categoryTicketsList);
			
			List<AutoCatsCategoryTicket> autoCatsCategoryTicketsList = new ArrayList<AutoCatsCategoryTicket>();
			autoCatsCategoryTicketsList = DAORegistry.getAutoCatsCategoryTicketDAO().getAllAutoCatsCategoryTicketsByAll(eventId, section, row,quantity);
			mav.addObject("autoCatsCategoryTicketsList",autoCatsCategoryTicketsList);
			
			List<VipCatsCategoryTicket> vipCatsCategoryTicketsList = new ArrayList<VipCatsCategoryTicket>();
			vipCatsCategoryTicketsList = DAORegistry.getVipCatsCategoryTicketDAO().getAllVipCatsCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("vipCatsCategoryTicketsList",vipCatsCategoryTicketsList);
			
			List<LastRowMiniCategoryTicket> lastRowMiniCategoryTicketList = new ArrayList<LastRowMiniCategoryTicket>();
			lastRowMiniCategoryTicketList = DAORegistry.getLastRowMiniCategoryTicketDAO().getAllLastRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("lastRowMiniCategoryTicketList", lastRowMiniCategoryTicketList);
			
			List<LarryLastCategoryTicket> larryLastCategoryTicketList = new ArrayList<LarryLastCategoryTicket>();
			larryLastCategoryTicketList = DAORegistry.getLarryLastCategoryTicketDAO().getAllLarryLastCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("larryLastCategoryTicketList", larryLastCategoryTicketList);
			
			if(brokerId==2){			
			    List<JKTZonesPricingCategoryTicket> jktZonesPricingCategoryTicketList = new ArrayList<JKTZonesPricingCategoryTicket>();
			    jktZonesPricingCategoryTicketList = DAORegistry.getJktZonesPricingCategoryTicketDAO().getAllJktZonesPricingCategoryTicketByAll(eventId, section, row, quantity);
			    mav.addObject("jktZonesPricingCategoryTicketList", jktZonesPricingCategoryTicketList);		
			}else if(brokerId==3){
				List<ZonesPricingCategoryTicket> zonesPricingCategoryTicketList = new ArrayList<ZonesPricingCategoryTicket>();
				zonesPricingCategoryTicketList = DAORegistry.getZonesPricingCategoryTicketDAO().getAllZonesPricingCategoryTicketByAll(eventId, section, row, quantity);
				mav.addObject("zonesPricingCategoryTicketList", zonesPricingCategoryTicketList);
			}
			
			List<ZonedLastRowMiniCategoryTicket> zonedLastRowMiniCategoryTicketList = new ArrayList<ZonedLastRowMiniCategoryTicket>();
			zonedLastRowMiniCategoryTicketList = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().getAllZonedLastRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("zonedLastRowMiniCategoryTicketList", zonedLastRowMiniCategoryTicketList);
			
			/*List<LastFiveRowMiniCategoryTicket> lastFiveRowMiniCategoryTicketList = new ArrayList<LastFiveRowMiniCategoryTicket>();
			lastFiveRowMiniCategoryTicketList = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().getAllLastFiveRowMiniCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("lastFiveRowMiniCategoryTicketList", lastFiveRowMiniCategoryTicketList);
			*/
			
			/*List<VipAutoCategoryTicket> vipAutoCategoryTicketList = new ArrayList<VipAutoCategoryTicket>();
			vipAutoCategoryTicketList = DAORegistry.getVipAutoCategoryTicketDAO().getAllVipAutoCategoryTicketsByAll(eventId,section, row,quantity);
			mav.addObject("vipAutoCategoryTicketList", vipAutoCategoryTicketList);
			*/
		}

		mav.addObject("events", events);
		mav.addObject("section", section);
		mav.addObject("row",row);
		mav.addObject("quantity", quantity);
		mav.addObject("showgrid",showgrid);
		return mav;
	}

	public ModelAndView loadCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		TGCatsCategoryTicket categoryTicket = DAORegistry.getTgCatsCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<TGCatsCategoryTicket> ticketPriceList = new ArrayList<TGCatsCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			TGCatsCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new TGCatsCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new TGCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadTMATTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-tmat-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		TGCatsCategoryTicket tgcatTicket = DAORegistry.getTgCatsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}

	public ModelAndView loadAutoCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-auto-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		AutoCatsCategoryTicket categoryTicket = DAORegistry.getAutoCatsCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<AutoCatsCategoryTicket> ticketPriceList = new ArrayList<AutoCatsCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			AutoCatsCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					ticketPriceObj = new AutoCatsCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new AutoCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice()
						.toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadAutoTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-auto-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		AutoCatsCategoryTicket categoryTicket = DAORegistry.getAutoCatsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(categoryTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = categoryTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(categoryTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = categoryTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(categoryTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = categoryTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(categoryTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = categoryTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	
	public ModelAndView loadVipCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-vip-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		VipCatsCategoryTicket categoryTicket = DAORegistry.getVipCatsCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<VipCatsCategoryTicket> ticketPriceList = new ArrayList<VipCatsCategoryTicket>();
		
		if(categoryTicket != null) {
		
			String pricehistory = categoryTicket.getPriceHistory();
			VipCatsCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				System.out.println("pricehistory :" + pricehistory);
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					System.out.println("Date  : " + ate);
					System.out.println("Price :" + rice);
					ticketPriceObj = new VipCatsCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new VipCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getActualPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	public ModelAndView loadVipTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-vip-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		VipCatsCategoryTicket tgcatTicket = DAORegistry.getVipCatsCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	public ModelAndView loadLastfiverowCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-lastfiverow-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		LastFiveRowMiniCategoryTicket categoryTicket = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<LastFiveRowMiniCategoryTicket> ticketPriceList = new ArrayList<LastFiveRowMiniCategoryTicket>();

		if(categoryTicket != null) {
		
			String pricehistory = categoryTicket.getPriceHistory();
			LastFiveRowMiniCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					ticketPriceObj = new LastFiveRowMiniCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new LastFiveRowMiniCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket
						.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	
	public ModelAndView loadLastfiverowTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-lastfiverow-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		LastFiveRowMiniCategoryTicket tgcatTicket = DAORegistry.getLastFiveRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	
	public ModelAndView loadLastRowCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-lastrow-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		System.out.println("eventId :" + eventId);
		String tmatId = request.getParameter("tId");
		System.out.println("tmatid :" + tmatId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		LastRowMiniCategoryTicket categoryTicket = DAORegistry.getLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<LastRowMiniCategoryTicket> ticketPriceList = new ArrayList<LastRowMiniCategoryTicket>();
		
		if(categoryTicket != null) {
			String pricehistory = categoryTicket.getPriceHistory();
			LastRowMiniCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					ticketPriceObj = new LastRowMiniCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new LastRowMiniCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket
						. getActualPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	public ModelAndView loadLastRowTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-lastrow-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		LastRowMiniCategoryTicket tgcatTicket = DAORegistry.getLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	
	public ModelAndView loadLarryLastCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-larrylast-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		LarryLastCategoryTicket categoryTicket = DAORegistry.getLarryLastCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<LarryLastCategoryTicket> ticketPriceList = new ArrayList<LarryLastCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			LarryLastCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					ticketPriceObj = new LarryLastCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new LarryLastCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket
						. getActualPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	
	public ModelAndView loadLarryLastPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-larrylast-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		LarryLastCategoryTicket tgcatTicket = DAORegistry.getLarryLastCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	
	public ModelAndView loadVipAutoCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-vipauto-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		VipAutoCategoryTicket categoryTicket = DAORegistry.getVipAutoCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<VipAutoCategoryTicket> ticketPriceList = new ArrayList<VipAutoCategoryTicket>();

		if(categoryTicket != null) {
			String pricehistory = categoryTicket.getPriceHistory();
			VipAutoCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String ate = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String rice = str[3];
					System.out.println("Date  : " + ate);
					System.out.println("Price :" + rice);
					ticketPriceObj = new VipAutoCategoryTicket();
					ticketPriceObj.setPopDate(ate);
					ticketPriceObj.setPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new VipAutoCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket
						.getLastUpdated().toString()).substring(0,
						(categoryTicket.getLastUpdated().toString())
								.indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket
						.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}
	
	
	public ModelAndView loadVipAutoTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-vipauto-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		VipAutoCategoryTicket tgcatTicket = DAORegistry.getVipAutoCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	public ModelAndView loadZonedLastrowCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-zonedlastrow-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		ZonedLastRowMiniCategoryTicket categoryTicket = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<ZonedLastRowMiniCategoryTicket> ticketPriceList = new ArrayList<ZonedLastRowMiniCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			ZonedLastRowMiniCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new ZonedLastRowMiniCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new TGCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadZonedLastrowTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-zonedlastrow-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		ZonedLastRowMiniCategoryTicket tgcatTicket = DAORegistry.getZonedLastRowMiniCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	public ModelAndView loadZonesPricingCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-zonespricing-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		ZonesPricingCategoryTicket categoryTicket = DAORegistry.getZonesPricingCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<ZonesPricingCategoryTicket> ticketPriceList = new ArrayList<ZonesPricingCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			ZonesPricingCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new ZonesPricingCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new TGCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadJktZonesPricingCategoryTicketPriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-zonespricing-category-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		String tmatId = request.getParameter("tId");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		JKTZonesPricingCategoryTicket categoryTicket = DAORegistry.getJktZonesPricingCategoryTicketDAO().get(Integer.parseInt(tmatId));
		List<JKTZonesPricingCategoryTicket> ticketPriceList = new ArrayList<JKTZonesPricingCategoryTicket>();
		
		if(categoryTicket != null) {
			
			String pricehistory = categoryTicket.getPriceHistory();
			JKTZonesPricingCategoryTicket ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				String array[] = pricehistory.split(",");
	
				for (String string : array) {
					str = string.split("-");
					String date = str[0] + "-" + str[1] + "-" + str[2] + ":00";
					String price = str[3];
					ticketPriceObj = new JKTZonesPricingCategoryTicket();
					ticketPriceObj.setPopDate(date);
					ticketPriceObj.setPopPrice(price);
					ticketPriceList.add(ticketPriceObj);
				}
			} /*else {
				ticketPriceObj = new TGCatsCategoryTicket();
				ticketPriceObj.setPopDate((categoryTicket.getLastUpdated().toString()).substring(0,(categoryTicket.getLastUpdated().toString()).indexOf(".")));
				ticketPriceObj.setPopPrice(categoryTicket.getPrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}*/
		}
		mav.addObject("ticketPriceList", ticketPriceList);
		mav.addObject("categoryTicket", categoryTicket);
		mav.addObject("event", event);
		return mav;
	}	
	public ModelAndView loadZonesPricingTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-zonespricing-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		ZonesPricingCategoryTicket tgcatTicket = DAORegistry.getZonesPricingCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}

	public ModelAndView loadJktZonesPricingTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-zonespricing-ticket-price-history-popup");
		
		String eventId = request.getParameter("eId");
		//String ticketId = request.getParameter("ticketId");
		String tgcatId = request.getParameter("tgid");
		String ticketType = request.getParameter("ticketType");
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		
		
		
		JKTZonesPricingCategoryTicket tgcatTicket = DAORegistry.getJktZonesPricingCategoryTicketDAO().get(Integer.parseInt(tgcatId));
		List<DateTimePop> tickethistoryList = new ArrayList<DateTimePop>();
		DateTimePop ticketTicObj = null;
		String ticStr[]=null;
		
		if(tgcatTicket != null) {
			String temphistoryStr = null;
			if(ticketType.equalsIgnoreCase("ticketId")) {
				temphistoryStr = tgcatTicket.getTicketIdHistory();
				
			} else if(ticketType.equalsIgnoreCase("baseTicketOne")) {
				
				if(tgcatTicket.getBaseTicketOneHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketOneHistory();
				}
				
			} else if(ticketType.equalsIgnoreCase("baseTicketTwo")) {
				if(tgcatTicket.getBaseTicketTwoHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketTwoHistory();
				}
			} else if(ticketType.equalsIgnoreCase("baseTickeThree")) {
				if(tgcatTicket.getBaseTicketThreeHistory() != null) {
					temphistoryStr = tgcatTicket.getBaseTicketThreeHistory();
				}
			}
			
			if(temphistoryStr != null) {
			    String array[] = temphistoryStr.split(",");

			    for (String string : array) {
				    ticStr = string.split("/");
				    String ate=ticStr[0];
				    String pur = ticStr[2];
				    Integer ticId = Integer.parseInt(ticStr[1]);
				    
				    ticketTicObj = new DateTimePop();
			    	ticketTicObj.settPopTicDate(ate);
					ticketTicObj.settPopTicPur(pur);
					
				    if(ticId.equals(0)) {
				    	tickethistoryList.add(ticketTicObj);
				    	continue;
				    }
					Ticket ticketInfo = DAORegistry.getTicketDAO().get(ticId);
					if(ticketInfo == null) {
						HistoricalTicket historicalTicket = DAORegistry.getHistoricalTicketDAO().get(ticId); 
						if(historicalTicket != null) {
							ticketTicObj.settPopTicOnilinePrice(historicalTicket.getBuyItNowPrice());
							ticketTicObj.settPopTicSection(historicalTicket.getSection());
							ticketTicObj.settPopTicRow(historicalTicket.getRow());
							ticketTicObj.settPopTicquantity(historicalTicket.getQuantity());
							ticketTicObj.settPopTicSiteId(historicalTicket.getSiteId());
							ticketTicObj.settPopTiclastUpdated(historicalTicket.getLastUpdate());
							ticketTicObj.settPopTicTicketStatus(historicalTicket.getTicketStatus());
							ticketTicObj.settPopTicCreatedDate(historicalTicket.getInsertionDate());
							ticketTicObj.settPopTicNormalizedSection(historicalTicket.getNormalizedSection());
						}
						tickethistoryList.add(ticketTicObj);
						
					} else {
						
						ticketTicObj.settPopTicDate(ate);
						ticketTicObj.settPopTicPur(pur);
						ticketTicObj.settPopTicOnilinePrice(ticketInfo.getBuyItNowPrice());
						ticketTicObj.settPopTicSection(ticketInfo.getSection());
						ticketTicObj.settPopTicRow(ticketInfo.getRow());
						ticketTicObj.settPopTicquantity(ticketInfo.getQuantity());
						ticketTicObj.settPopTicSiteId(ticketInfo.getSiteId());
						ticketTicObj.settPopTiclastUpdated(ticketInfo.getLastUpdate());
						ticketTicObj.settPopTicTicketStatus(ticketInfo.getTicketStatus());
						ticketTicObj.settPopTicCreatedDate(ticketInfo.getInsertionDate());
						ticketTicObj.settPopTicNormalizedSection(ticketInfo.getNormalizedSection());
						tickethistoryList.add(ticketTicObj);
					}
			    }
		    }
		}
		
		mav.addObject("tmatTicketList",tickethistoryList);
		//mav.addObject("CategoryTicketlist", CategoryTicketlist);
		//mav.addObject("CategoryTicketHislist", CategoryTicketHislist);
		mav.addObject("event", event);
		mav.addObject("ticketType", ticketType);
		
		return mav;
	}
	public ModelAndView loadEbayTicketPriceHistory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-ebay-ticket-price-history-popup");
		String eventId = request.getParameter("eId");
		System.out.println("eventId :" + eventId);
		String ticketId = request.getParameter("ticketId");
		System.out.println("tickItid :" + ticketId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		List<TgCatsEbayListing> CategoryTicketlist = DAORegistry
				.getTgCatsEbayListingDAO().getAllTgCatsEbayListingsById(
						Integer.parseInt(ticketId));
		List<DateTimePop> ticketPriceList = new ArrayList<DateTimePop>();
		String pricehistory = CategoryTicketlist.get(0).getMainTicketHistory();
		DateTimePop ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			System.out.println("pricehistory :" + pricehistory);
			String array[] = pricehistory.split(",");
			for (String string : array) {
				str = string.split("\\?");
				String ate = str[0];
				String rice = str[2];
				System.out.println("Date  : " + ate);
				System.out.println("Price :" + rice);
				ticketPriceObj = new DateTimePop();
				ticketPriceObj.settPopDate(ate);
				ticketPriceObj.settPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} else {
			ticketPriceObj = new DateTimePop();
			ticketPriceObj.settPopDate(CategoryTicketlist.get(0)
					.getLastUpdated().toString());
			ticketPriceObj.settPopPrice(CategoryTicketlist.get(0)
					.getTmatOnlinePrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}
		System.out.println("ticketPriceList : " + ticketPriceList.size());
		mav.addObject("tmatPriceList", ticketPriceList);
		mav.addObject("CategoryTicketlist", CategoryTicketlist);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadEbayBaseTicket1PriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-ebay-base-ticket1-price-history-popup");
		String eventId = request.getParameter("eId");
		System.out.println("eventId :" + eventId);
		String ticketId = request.getParameter("ticketId");
		System.out.println("tickItid :" + ticketId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		List<TgCatsEbayListing> CategoryTicketlist = null;
		List<DateTimePop> ticketPriceList = new ArrayList<DateTimePop>();
		if (ticketId != null && ticketId != "" && !ticketId.equals("0")) {
			CategoryTicketlist = DAORegistry.getTgCatsEbayListingDAO()
					.getAllTgCatsEbayListingsByBaseTicketOneId(
							Integer.parseInt(ticketId));
			String pricehistory = CategoryTicketlist.get(0)
					.getBaseTicketOneHistory();
			DateTimePop ticketPriceObj = null;
			String str[] = null;
			if (pricehistory != null && pricehistory != "0") {
				System.out.println("pricehistory :" + pricehistory);
				String array[] = pricehistory.split(",");
				for (String string : array) {
					str = string.split("\\?");
					String ate = str[0];
					String rice = str[2];
					System.out.println("Date  : " + ate);
					System.out.println("Price :" + rice);
					ticketPriceObj = new DateTimePop();
					ticketPriceObj.settPopDate(ate);
					ticketPriceObj.settPopPrice(rice);
					ticketPriceList.add(ticketPriceObj);
				}
			} else {
				ticketPriceObj = new DateTimePop();
				ticketPriceObj.settPopDate(CategoryTicketlist.get(0)
						.getLastUpdated().toString());
				ticketPriceObj.settPopPrice(CategoryTicketlist.get(0)
						.getTmatOnlinePrice().toString());
				ticketPriceList.add(ticketPriceObj);
			}
			System.out.println("ticketPriceList : " + ticketPriceList.size());
		}
		mav.addObject("tmatPriceList", ticketPriceList);
		mav.addObject("CategoryTicketlist", CategoryTicketlist);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadEbayBaseTicket2PriceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-ebay-base-ticket2-price-history-popup");
		String eventId = request.getParameter("eId");
		System.out.println("eventId :" + eventId);
		String ticketId = request.getParameter("ticketId");
		System.out.println("tickItid :" + ticketId);
		Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
		List<TgCatsEbayListing> CategoryTicketlist = null;
		if (ticketId != null && ticketId != "" && !ticketId.equals("0")) {
			CategoryTicketlist = DAORegistry.getTgCatsEbayListingDAO()
					.getAllTgCatsEbayListingsByBaseTicketTwoId(
							Integer.parseInt(ticketId));
		}
		List<DateTimePop> ticketPriceList = new ArrayList<DateTimePop>();
		String pricehistory = CategoryTicketlist.get(0)
				.getBaseTicketTwoHistory();
		DateTimePop ticketPriceObj = null;
		String str[] = null;
		if (pricehistory != null && pricehistory != "0") {
			System.out.println("pricehistory :" + pricehistory);
			String array[] = pricehistory.split(",");
			for (String string : array) {
				str = string.split("\\?");
				String ate = str[0];
				String rice = str[2];
				System.out.println("Date  : " + ate);
				System.out.println("Price :" + rice);
				ticketPriceObj = new DateTimePop();
				ticketPriceObj.settPopDate(ate);
				ticketPriceObj.settPopPrice(rice);
				ticketPriceList.add(ticketPriceObj);
			}
		} else {
			ticketPriceObj = new DateTimePop();
			ticketPriceObj.settPopDate(CategoryTicketlist.get(0)
					.getLastUpdated().toString());
			ticketPriceObj.settPopPrice(CategoryTicketlist.get(0)
					.getTmatOnlinePrice().toString());
			ticketPriceList.add(ticketPriceObj);
		}
		System.out.println("ticketPriceList : " + ticketPriceList.size());
		mav.addObject("tmatPriceList", ticketPriceList);
		mav.addObject("CategoryTicketlist", CategoryTicketlist);
		mav.addObject("event", event);
		return mav;
	}

	public ModelAndView loadGlobalAutoPricing(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String sportinfo = "";
		ModelAndView mav = new ModelAndView("page-global-auto-pricing");
		if ("updateExposure".equalsIgnoreCase(request.getParameter("action"))) {
			String auto_exposure_ticketnetwork = request
					.getParameter("auto_exposure");
			String mini_exposure_ticketnetwork = request
					.getParameter("mini_exposure");
			String vip_mini_exposure_ticketnetwork = request
					.getParameter("vip_mini_exposure");
			String vip_auto_exposure_ticketnetwork = request
					.getParameter("vip_auto_exposure");
			String lastrow_mini_exposure_ticketnetwork = request
					.getParameter("lastrow_mini_exposure");
			String lastfiverow_mini_exposure_ticketnetwork = request
					.getParameter("lastfiverow_mini_exposure");
			String zonesPricingExposure = request
			.getParameter("zones_pricing_exposure");
			String zonedLastRowMiniExposure = request
			.getParameter("zoned_lastrowmini_exposure");
			String shippingMethodStr = request.getParameter("shipping_method");
			String nearTermDisplayOptionStr = request
					.getParameter("near_term_display_option");
			String rpt_ticketnetwork = request
					.getParameter("rpt_ticketnetwork");
			String pricebreakup_ticketnetwork = request
					.getParameter("price_breakup_ticketnetwork");
			String upper_markpu_ticketnetwork = request
					.getParameter("upper_markup_ticketnetwork");
			String lower_markpu_ticketnetwork = request
					.getParameter("lower_markup_ticketnetwork");
			String upper_shippingfees_ticketnetwork = request
					.getParameter("upper_shippingFees_ticketnetwork");
			String lower_shippingfees_ticketnetwork = request
					.getParameter("lower_shippingFees_ticketnetwork");

			String[] eventTypeArr = request.getParameterValues("event_type");
			String eventType = "", eventTypeParam = "";

			if (null != eventTypeArr && eventTypeArr.length > 0) {
				for (String string : eventTypeArr) {
					eventType = eventType + "," + string;
					eventTypeParam = eventTypeParam + ",'" + string + "'";
				}
				eventType = eventType.substring(1);
				eventTypeParam = eventTypeParam.substring(1);
			}

			try {
				String userName = SecurityContextHolder.getContext()
						.getAuthentication().getName();
				GlobalAutoPricingAudit audit = new GlobalAutoPricingAudit();
				audit.setCreatedDate(new Date());
				audit.setUserName(userName);
				audit.setAction("Global Update");
				audit.setEventTypes(eventType);

				Boolean isAutoUpdate = false, isMiniUpdate = false, isVipMiniUpdate = false, isVipAutoUpdate = false, isLastRowMiniUpdate = false, 
				isLastFiveRowMiniUpdate = false,isLarryLastUpdate=false,isZonesPricingUpdate=false,isZonedLastRowMiniUpdate=false;

				if (auto_exposure_ticketnetwork != null) {
					audit.setAutoExposure(auto_exposure_ticketnetwork);
					isAutoUpdate = true;
				}
				if (mini_exposure_ticketnetwork != null) {
					audit.setMiniExposure(mini_exposure_ticketnetwork);
					isMiniUpdate = true;
					isLarryLastUpdate = true;
				}
				if (vip_mini_exposure_ticketnetwork != null) {
					audit.setVipMiniExposure(vip_mini_exposure_ticketnetwork);
					isVipMiniUpdate = true;
				}
				if (vip_auto_exposure_ticketnetwork != null) {
					audit.setVipAutoExposure(vip_auto_exposure_ticketnetwork);
					isVipAutoUpdate = true;
				}
				if (lastrow_mini_exposure_ticketnetwork != null) {
					audit.setLastRowMiniExposure(lastrow_mini_exposure_ticketnetwork);
					isLastRowMiniUpdate = true;
				}
				if (lastfiverow_mini_exposure_ticketnetwork != null) {
					audit.setLastFiveRowMiniExposure(lastfiverow_mini_exposure_ticketnetwork);
					isLastFiveRowMiniUpdate = true;
				}
				
				if (zonesPricingExposure != null) {
					audit.setZonesPricingExposure(zonesPricingExposure);
					isZonesPricingUpdate = true;
				}
				if (zonedLastRowMiniExposure != null) {
					audit.setZonedLastRowMiniExposure(zonedLastRowMiniExposure);
					isZonedLastRowMiniUpdate = true;
				}

				if (shippingMethodStr != null
						&& shippingMethodStr.trim().length() > 0) {
					audit.setShippingMethod(Integer.valueOf(shippingMethodStr));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (nearTermDisplayOptionStr != null
						&& nearTermDisplayOptionStr.trim().length() > 0) {
					audit.setNearTermDisplayOption(Integer
							.valueOf(nearTermDisplayOptionStr));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (rpt_ticketnetwork != null) {
					audit.setRptFactor(Double.parseDouble(rpt_ticketnetwork));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (pricebreakup_ticketnetwork != null) {
					audit.setPriceBreakup(Double
							.parseDouble(pricebreakup_ticketnetwork));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (upper_markpu_ticketnetwork != null) {
					audit.setUpperMarkup(Double
							.parseDouble(upper_markpu_ticketnetwork));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (lower_markpu_ticketnetwork != null) {
					audit.setLowerMarkup(Double
							.parseDouble(lower_markpu_ticketnetwork));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (upper_shippingfees_ticketnetwork != null) {
					audit.setUpperShippingFees(Double
							.parseDouble(upper_shippingfees_ticketnetwork));

					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (lower_shippingfees_ticketnetwork != null) {
					audit.setLowerShippingFees(Double
							.parseDouble(lower_shippingfees_ticketnetwork));
					isAutoUpdate = true;
					isMiniUpdate = true;
					isVipMiniUpdate = true;
					isVipAutoUpdate = true;
					isLastRowMiniUpdate = true;
					isLastFiveRowMiniUpdate = true;
					isLarryLastUpdate = true;
					isZonesPricingUpdate=true;
					isZonedLastRowMiniUpdate = true;
				}

				if (isAutoUpdate) {
					DAORegistry.getAutoTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getAutoVividSeatExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getAutoScoreBigExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isMiniUpdate) {
					DAORegistry.getMiniTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getMiniVividSeatExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getMiniScoreBigExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);

				}

				if (isVipAutoUpdate) {
					DAORegistry.getVipAutoTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getVipAutoVividSeatExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getVipAutoScoreBigExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isVipMiniUpdate) {
					DAORegistry.getVipMiniTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getVipMiniVividSeatExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
					DAORegistry.getVipMiniScoreBigExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isLastRowMiniUpdate) {
					DAORegistry.getLastRowMiniTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isLastFiveRowMiniUpdate) {
					DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isLarryLastUpdate) {
					DAORegistry.getLarryMiniTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				
				if (isZonesPricingUpdate) {
					DAORegistry.getZonesPricingTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}
				if (isZonedLastRowMiniUpdate) {
					DAORegistry.getZonedLastRowMiniTNExchangeEventDAO()
							.updateGlobalAutoPricing(audit, eventTypeParam);
				}

				if (isAutoUpdate || isMiniUpdate || isVipMiniUpdate
						|| isVipAutoUpdate || isLastRowMiniUpdate
						|| isLastFiveRowMiniUpdate || isLarryLastUpdate || isZonesPricingUpdate || isZonedLastRowMiniUpdate) {
					DAORegistry.getGlobalAutoPricingAuditDAO().save(audit);

					sportinfo = "Updated successfully";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mav.addObject("sportinfo", sportinfo);
		return mav;
	}

	public ModelAndView loadGlobalAutoPricingAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView(
				"page-global-auto-pricing-audit-popup");

		try {
			List<GlobalAutoPricingAudit> list = DAORegistry
					.getGlobalAutoPricingAuditDAO()
					.getAllAuditsOrderByCreatedDate();
			mav.addObject("auditList", list);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	public void loadFrequentEvents(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			List<Object[]> list = DAORegistry.getVividExchangeEventDAO()
					.getAllFrequentEvents();
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			DateFormat timeFormat = new SimpleDateFormat("HH:mm");
			StringBuilder sb = new StringBuilder();
			sb.append("EventName,EventDate,EventTime,TourName,Venue,eventType");
			sb.append("\n");
			if (list != null && !list.isEmpty()) {
				for (Object[] data : list) {

					if (data[0] != null) {
						sb.append(((String) data[0]).replaceAll(",", " ") + ",");
					} else {
						sb.append(",");
					}

					if (data[1] != null) {
						sb.append(String.valueOf(dateFormat.format(data[1])
								.replaceAll(",", " ") + ","));
					} else {
						sb.append("TBD,");
					}

					if (data[2] != null) {
						sb.append(String.valueOf(timeFormat.format(data[2])
								.replaceAll(",", " ") + ","));
					} else {
						sb.append("TBD,");
					}

					if (data[3] != null) {
						sb.append(((String) data[3]).replaceAll(",", " ") + ",");
					} else {
						sb.append(",");
					}

					if (data[4] != null) {
						sb.append(((String) data[4]).replaceAll(",", " ") + ",");
					} else {
						sb.append(",");
					}

					if (data[5] != null) {
						sb.append(((String) data[5]).replaceAll(",", " ") + ",");
					} else {
						sb.append(",");
					}

					sb.append("\n");
				}
			}

			String today = dateFormat.format(new Date());
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=FrequentEvents-" + today + ".csv");
			OutputStream out = response.getOutputStream();
			out.write(sb.toString().getBytes());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ModelAndView loadTnApiCallLimit(HttpServletRequest request,
			HttpServletResponse response) throws JSONException {

		ModelAndView mav = null;

		String action = request.getParameter("action");
		String project = request.getParameter("project");
		String createCallDay = request.getParameter("createCallDay");
		String createCallMinute = request.getParameter("createCallMinute");
		String deleteCallMinute = request.getParameter("deleteCallMinute");
		String updateCallMinute = request.getParameter("updateCallMinute");

		AuditTnApiCallLimit auditTnApiCallLimit = new AuditTnApiCallLimit();
		AuditTnApiCallLimitDAO auditTnApiCallLimitDAO = (AuditTnApiCallLimitDAO) DAORegistry
				.getAuditTnApiCallLimitDAO();
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		TnnApiCallLimitTemp object = null;
		PropertyDAO propertyDAO = DAORegistry.getPropertyDAO();
		Property property = null;
		try {
			if (action != null && action.equalsIgnoreCase("audit")) {
				mav = new ModelAndView("page-TN-api-call-limit-audit");
				mav.addObject("auditList",
						auditTnApiCallLimitDAO.getByProjectName(project));
				mav.addObject("project", project);
				return mav;

			} else if (action != null && action.equalsIgnoreCase("update")) {
				auditTnApiCallLimit.setUserName(username);
				auditTnApiCallLimit.setCreateDate(new Date());
				auditTnApiCallLimit.setCreateCallDay(createCallDay);
				auditTnApiCallLimit.setCreateCallMinute(createCallMinute);
				auditTnApiCallLimit.setDeleteCallMinute(deleteCallMinute);
				auditTnApiCallLimit.setUpdateCallMinute(updateCallMinute);
				auditTnApiCallLimitDAO.save(auditTnApiCallLimit);

				if (project.equalsIgnoreCase("AUTOCATS")) {
					propertyDAO = DAORegistry.getPropertyDAO();
					property = propertyDAO
							.get("autocats.api.call.limit.create.day");
					if (property != null) {
						property.setValue(createCallDay);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("autocats.api.call.limit.create.minute");
					if (property != null) {
						property.setValue(createCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("autocats.api.call.limit.delete.minute");
					if (property != null) {
						property.setValue(deleteCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("autocats.api.call.limit.update.minute");
					if (property != null) {
						property.setValue(updateCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
				} else if (project.equalsIgnoreCase("TGCATS")) {
					propertyDAO = DAORegistry.getPropertyDAO();
					property = propertyDAO
							.get("tgcats.api.call.limit.create.day");
					if (property != null) {
						property.setValue(createCallDay);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("tgcats.api.call.limit.create.minute");
					if (property != null) {
						property.setValue(createCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("tgcats.api.call.limit.delete.minute");
					if (property != null) {
						property.setValue(deleteCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("tgcats.api.call.limit.update.minute");
					if (property != null) {
						property.setValue(updateCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
				} else if (project.equalsIgnoreCase("ZONES PRICING")) {
					propertyDAO = DAORegistry.getPropertyDAO();
					property = propertyDAO
							.get("zonespricing.api.call.limit.create.day");
					if (property != null) {
						property.setValue(createCallDay);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("zonespricing.api.call.limit.create.minute");
					if (property != null) {
						property.setValue(createCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("zonespricing.api.call.limit.delete.minute");
					if (property != null) {
						property.setValue(deleteCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
					property = propertyDAO
							.get("zonespricing.api.call.limit.update.minute");
					if (property != null) {
						property.setValue(updateCallMinute);
						propertyDAO.saveOrUpdate(property);
					}
				}
			}

			ArrayList<TnnApiCallLimitTemp> list = new ArrayList<TnnApiCallLimitTemp>();
			object = new TnnApiCallLimitTemp();
			object.setId(1);
			object.setCreateCallDay(Integer.parseInt(propertyDAO.get(
					"autocats.api.call.limit.create.day").getValue()));
			object.setCreateCallMinute(Integer.parseInt(propertyDAO.get(
					"autocats.api.call.limit.create.minute").getValue()));
			object.setDeleteCallMinute(Integer.parseInt(propertyDAO.get(
					"autocats.api.call.limit.delete.minute").getValue()));
			object.setUpdateCallMinute(Integer.parseInt(propertyDAO.get(
					"autocats.api.call.limit.update.minute").getValue()));
			object.setProjects("AUTOCATS");
			list.add(object);

			object = new TnnApiCallLimitTemp();
			object.setId(2);
			object.setCreateCallDay(Integer.parseInt(propertyDAO.get(
					"tgcats.api.call.limit.create.day").getValue()));
			object.setCreateCallMinute(Integer.parseInt(propertyDAO.get(
					"tgcats.api.call.limit.create.minute").getValue()));
			object.setDeleteCallMinute(Integer.parseInt(propertyDAO.get(
					"tgcats.api.call.limit.delete.minute").getValue()));
			object.setUpdateCallMinute(Integer.parseInt(propertyDAO.get(
					"tgcats.api.call.limit.update.minute").getValue()));
			object.setProjects("TGCATS");
			list.add(object);

			object = new TnnApiCallLimitTemp();
			object.setId(3);
			object.setCreateCallDay(Integer.parseInt(propertyDAO.get(
					"zonespricing.api.call.limit.create.day").getValue()));
			object.setCreateCallMinute(Integer.parseInt(propertyDAO.get(
					"zonespricing.api.call.limit.create.minute").getValue()));
			object.setDeleteCallMinute(Integer.parseInt(propertyDAO.get(
					"zonespricing.api.call.limit.delete.minute").getValue()));
			object.setUpdateCallMinute(Integer.parseInt(propertyDAO.get(
					"zonespricing.api.call.limit.update.minute").getValue()));
			object.setProjects("ZONES PRICING");
			list.add(object);

			mav = new ModelAndView("page-TN-api-call-limit");
			mav.addObject("tnApiCallLimitList", list);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.fillInStackTrace());
		}

		return mav;
	}

}
