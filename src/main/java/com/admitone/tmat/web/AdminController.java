package com.admitone.tmat.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
//import org.springframework.security.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.dao.PropertyDAO;
import com.admitone.tmat.dao.hibernate.UserBrokerDetailsDAO;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.EIBoxEvent;
import com.admitone.tmat.data.EIInstantEvent;
import com.admitone.tmat.data.EIInstantTour;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.InstantEvent;
import com.admitone.tmat.data.LiveUser;
import com.admitone.tmat.data.MMEventMap;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Role;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.TGCatsCategoryEvent;
import com.admitone.tmat.data.TGCatsCategoryTicket;
import com.admitone.tmat.data.TGCatsError;
import com.admitone.tmat.data.TNInstantEvent;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TnowInstantEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.TourCategory;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAction;
import com.admitone.tmat.data.UserBrokerDetails;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.security.SessionListener;
import com.admitone.tmat.util.TNListedEvent;
import com.admitone.tmat.utils.AlphanumericSorting;
import com.admitone.tmat.utils.EIEventManager;
import com.admitone.tmat.utils.EITicketsUtil;
import com.admitone.tmat.utils.EiMarketPlaceEventPopulator;
import com.admitone.tmat.utils.EimpAndEiboxComparator;
import com.admitone.tmat.utils.InstantEventManager;
import com.admitone.tmat.utils.InstantTicketsUtil;
import com.admitone.tmat.utils.MercuryTicketsUtil;
import com.admitone.tmat.utils.SHWebAndWSTicketListingComparator;
import com.admitone.tmat.utils.SeatWaveEventPopulator;
import com.admitone.tmat.utils.TNEventManager;
import com.admitone.tmat.utils.TNWebAndWSTicketListingComparator;
import com.admitone.tmat.utils.TNowInstantEventManager;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.web.pojo.EIMPTicketComparator;
import com.admitone.tmat.web.pojo.FeedAndCrawlComparatorVO;
import com.admitone.tmat.data.TNError;


/**
 * Web Controller to handle pages related to the administration
 */
public class AdminController extends  MultiActionController {
	private AnnotationSessionFactoryBean sessionFactoryBean;
	private Date applicationStartDate;
//	private TicketListingCrawler ticketListingCrawler;
	private CreationEventListManager creationEventListManager;
//	private EbayManager ebayManager;
	
	public AdminController() {
		applicationStartDate = new Date();
	}
	
	/**
	 * Page showing the Admin stats
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadAdminPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		ModelAndView mav = new ModelAndView("page-admin-system-stats");	
		if (action != null && action.equals("stop")) {
			Integer nodeId = Integer.valueOf(request.getParameter("nodeId"));
			// TODOJMS
//			ticketListingCrawler.getTicketListingCrawlDispatcher().dispatch(new TicketListingCrawlerMessage(nodeId, TicketListingCrawlerMessage.MSG_QUIT));
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return new ModelAndView(new RedirectView("Admin?info=The node " + nodeId + " has been stopped"));			
		}
		
		mav.addObject("applicationStartDate", applicationStartDate);
		
		mav.addObject("userCount", DAORegistry.getUserDAO().getUserCount());
//		mav.addObject("tourCount", DAORegistry.getTourDAO().getTourCount());
		mav.addObject("eventCount", DAORegistry.getEventDAO().getEventCount());
		mav.addObject("artistCount", DAORegistry.getArtistDAO().getArtistCount());
		mav.addObject("shortTransactionCount", DAORegistry.getShortTransactionDAO().getAll().size());
		mav.addObject("shortBroadcastCount", DAORegistry.getShortBroadcastDAO().getAllLinkedShortBroadcasts().size());

		mav.addObject("activeTicketCount", DAORegistry.getTicketDAO().getTicketCount(TicketStatus.ACTIVE));
		mav.addObject("hibernateProperties", sessionFactoryBean.getConfiguration().getProperties().entrySet());
		// TODOJMS
		/*List<TicketListingCrawlStat> stats = new ArrayList<TicketListingCrawlStat>(ticketListingCrawler.getTicketListingCrawlDispatcher().getStatMap().values());
		Collections.sort(stats, new Comparator<TicketListingCrawlStat>() {
			public int compare(TicketListingCrawlStat t1,
					TicketListingCrawlStat t2) {
				return t1.getNodeId().compareTo(t2.getNodeId());
			}
		});
		mav.addObject("stats", stats);
		mav.addObject("maxMasterActiveConnectionsBySiteId", ticketListingCrawler.getActiveTicketListingCrawlCountBySiteId());
		mav.addObject("dispatchQueueSize", ticketListingCrawler.getTicketListingCrawlDispatcher().getDispatchQueueSize());
		*/
		Map<String, Integer> maxActiveConnectionsBySiteId = new HashMap<String, Integer>();
		for(Site site: DAORegistry.getSiteDAO().getAll()) {
			maxActiveConnectionsBySiteId.put(site.getId(), site.getMaxConcurrentConnections());
		}
		
		
		mav.addObject("maxActiveConnectionsBySiteId", maxActiveConnectionsBySiteId);
		
		
		
		return mav;
	}	
	
	public ModelAndView loadAdminInstantFileSettings(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-edit-instant-properties");
		return mav;
	}
	
	public ModelAndView loadUserAuditPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ParseException,JSONException{
		
		
		String username= request.getParameter("userName");
		User user_info;
		Integer brok_info=null;
		if(username !=null && !username.isEmpty()&& !username.equals("undefined"))
		{
		 user_info = DAORegistry.getUserDAO().getUserByUsername(username);
		// brok_info=user_info.getBroker().getId();
		 for(UserBrokerDetails userBrkerDetails:user_info.getUserBrokerDetails()){
			 brok_info=userBrkerDetails.getBrokerId();
		 }
		}
		ModelAndView mav = new ModelAndView("page-admin-users-audit");
		String brokerId=request.getParameter("brokerId");
		String userName = request.getParameter("userName");
		String startDateString = request.getParameter("startDate");
		String endDateString = request.getParameter("endDate");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		DateFormat df1 = new SimpleDateFormat("MM/dd/yyyy");
		Date startDate = null;
		
		
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveBrokers());
		mav.addObject("brokerId", request.getParameter("brokerId"));
		
		
		if(startDateString ==null || startDateString.isEmpty()){
			startDateString = df1.format(new Date());
		}
		startDate = df.parse(startDateString + " 00:00:00");
		Date endDate = null;
		if(endDateString==null || endDateString.isEmpty()){
			endDateString = df1.format(new Date());
		}
		endDate = df.parse(endDateString + " 23:59:59");
		Collection<UserAction> list = null;
		if(userName.equalsIgnoreCase("all")){
			list = DAORegistry.getUserActionDAO().getActionsByUsernameAndDate(null, startDate, endDate);
		}else{
			list = DAORegistry.getUserActionDAO().getActionsByUsernameAndDate(userName, startDate, endDate);
		}
//		String brok=null;
		Collection<User> info=null;
		if(brok_info !=null)
		{
//			brok=Integer.toString(brok_info);
			 info=DAORegistry.getUserDAO().getUsersByBrokerId(brok_info);
		}
		
		mav.addObject("actionList", list);
		mav.addObject("startDate", startDateString);
		mav.addObject("userName", userName);
		mav.addObject("users",info);
		mav.addObject("broker",brok_info);
		mav.addObject("endDate", endDateString);
		return mav;
	}
	
	public void getUsernameByBroker(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ParseException,JSONException{
		
		String brokerId=request.getParameter("brokerId");
		String userName = request.getParameter("userName");
		
		
			
		JSONArray jsonArray = new JSONArray();
		
		if (brokerId==null || brokerId.isEmpty())
		{
			Collection<User> users=DAORegistry.getUserDAO().getAll();
			for (User user:users)
			{
			JSONObject   jObject = new JSONObject(); 
			jObject.put("username ",user.getUsername());
			jObject.put("firstName",user.getFirstName());
			jObject.put("lastName",user.getLastName());
			
			jsonArray.put(jObject);
			}
		}
		else
		{
			Collection<User> users=DAORegistry.getUserDAO().getUsersByBrokerId(Integer.parseInt(brokerId));
			for (User user:users)
			{
			JSONObject   jObject = new JSONObject(); 
			jObject.put("username ",user.getUsername());
			jObject.put("firstName",user.getFirstName());
			jObject.put("lastName",user.getLastName());
			
			jsonArray.put(jObject);
			}
		}
		IOUtils.write(jsonArray.toString().getBytes(), response.getOutputStream());
	}
	
	
	/**
	 * Diplay page Manage Users
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadManageUsersPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		ModelAndView mav = new ModelAndView("page-admin-manage-users");
		String brokerId = request.getParameter("brokerId");		
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveBrokers());
		//mav.addObject("brokerId", request.getParameter("brokerId"));
		String action = request.getParameter("status");
		Collection<User> userList;
		if (brokerId==null || brokerId.isEmpty())
		{
			userList = DAORegistry.getUserDAO().getAll();
		}
		else
		{
			userList = DAORegistry.getUserDAO().getUsersByBrokerId(Integer.parseInt(brokerId));
		}
//		System.out.println("UserList"+ userList);
		for (User user : userList) {
			Collection<LiveUser> liveUserList  =DAORegistry.getLiveUserDAO().getLiveUserByUsername(user.getUsername()); 
			String applicationName ="";
			int i=0;
			if(null != liveUserList && liveUserList.size() >0){
				for (LiveUser liveUser : liveUserList) {
					if(liveUser.getLoggedIn().equals('Y')){
						if(i==0){
							applicationName +=liveUser.getAppName().toUpperCase();
							user.setIsLiveUser(Boolean.TRUE);
						}else{
							applicationName +=","+liveUser.getAppName().toUpperCase();
						}
						i++;
					}
			}
			}
			user.setApplicationName(applicationName);
		}
		mav.addObject("brokerId",brokerId);
		mav.addObject("users", userList);
		
		return mav;
	}
	/*
	public ModelAndView loadAdminEbayPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("submit");
		
		if (action != null) {
			Property mailProperty = new Property("ebay.email.recipient", request.getParameter("emails"));
			Property checkPeriodProperty = new Property("ebay.check.period", request.getParameter("checkPeriod"));
			Property mailSubjectProperty = new Property("ebay.email.subject", request.getParameter("subject"));
			Property rptFactorProperty = new Property("ebay.rptFactor", request.getParameter("rptFactor"));
			Property categorySchemeProperty = new Property("ebay.category.scheme", request.getParameter("categoryScheme"));
			Property eimpFanAdditionProperty = new Property("ebay.eimp.fan.addition", request.getParameter("fanAddition"));
			Property ebayNetProfitProperty = new Property("ebay.filter.netProfit", request.getParameter("netProfitFilter"));
			Property ebayListEasyLoginProperty = new Property("ebay.listeasy.login", request.getParameter("listEasyLogin"));
			Property ebayListEasyPasswordProperty = new Property("ebay.listeasy.password", request.getParameter("listEasyPassword"));
			Property ebayCrawlerFrequencyProperty = new Property("ebay.crawler.frequency", request.getParameter("crawlerFrequency"));
			Property ebayInvalidAdditionProperty = new Property("ebay.invalid.addition", request.getParameter("invalidAddition"));
			Property ebayTicketIdTTLProperty = new Property("ebay.ticketId.ttl", request.getParameter("ticketIdTTL"));
			Property ebayListEasyNotesProperty = new Property("ebay.listeasy.notes", request.getParameter("ebayListEasyNotes").trim().replaceAll("[\n\r]", ""));
			String ebayImageDirUrl = request.getParameter("ebayImageDirUrl").trim();
			if (ebayImageDirUrl.endsWith("/")) {
				ebayImageDirUrl = ebayImageDirUrl.substring(0, ebayImageDirUrl.length() - 1);
			}
//			Property ebayImageDirUrlProperty = new Property("ebay.imageDirUrl", ebayImageDirUrl);
			
			DAORegistry.getPropertyDAO().saveOrUpdate(mailProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(checkPeriodProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(mailSubjectProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(rptFactorProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(categorySchemeProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(eimpFanAdditionProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayNetProfitProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayListEasyLoginProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayListEasyPasswordProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayCrawlerFrequencyProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayInvalidAdditionProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayTicketIdTTLProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayListEasyNotesProperty);
			DAORegistry.getPropertyDAO().saveOrUpdate(ebayImageDirUrlProperty);
			
//			ebayManager.reinitProperties();
//			ebayManager.updateEbayCrawlersFrequency();
//			ebayManager.restart();
			return new ModelAndView(new RedirectView("AdminEbay?info=Ebay Properties updated."));
		}
		
		ModelAndView mav = new ModelAndView("page-admin-ebay");
		
		Property mailProperty = DAORegistry.getPropertyDAO().get("ebay.email.recipient");
		String recipient = "";
		if (mailProperty != null) {
			recipient = mailProperty.getValue();
		}

		Property checkPeriodProperty = DAORegistry.getPropertyDAO().get("ebay.check.period");
		Integer checkPeriod = 10;
		if (checkPeriodProperty != null) {
			checkPeriod = Integer.valueOf(checkPeriodProperty.getValue());
		}

		Property mailSubjectProperty = DAORegistry.getPropertyDAO().get("ebay.email.subject");
		String subject = "Admitone Ebay Notification";
		if (mailSubjectProperty != null) {
			subject = mailSubjectProperty.getValue();
		}

		Property rptFactorProperty = DAORegistry.getPropertyDAO().get("ebay.rptFactor");
		Double rptFactor = 25.00;
		if (rptFactorProperty != null) {
			rptFactor = Double.parseDouble(rptFactorProperty.getValue());
		}

		Property eimpFanAdditionProperty = DAORegistry.getPropertyDAO().get("ebay.eimp.fan.addition");
		Double fanAddition = 15.00;
		if (eimpFanAdditionProperty != null) {
			fanAddition = Double.parseDouble(eimpFanAdditionProperty.getValue());
		}

		Property categorySchemeProperty = DAORegistry.getPropertyDAO().get("ebay.category.scheme");
		String categoryScheme = "AO";
		if (categorySchemeProperty != null) {
			categoryScheme = categorySchemeProperty.getValue();
		}

		Property netProfitFilterProperty = DAORegistry.getPropertyDAO().get("ebay.filter.netProfit");
		Double netProfitFilter = 35.00;
		if (netProfitFilterProperty != null) {
			netProfitFilter = Double.valueOf(netProfitFilterProperty.getValue());
		}

		Property ebayListEasyNotesProperty = DAORegistry.getPropertyDAO().get("ebay.listeasy.notes");
		String ebayListEasyNotes = "";
		if (ebayListEasyNotesProperty != null) {
			ebayListEasyNotes = ebayListEasyNotesProperty.getValue();
		}

		Property ebayImageDirUrlProperty = DAORegistry.getPropertyDAO().get("ebay.imageDirUrl");
		String ebayImageDirUrl = "";
		if (ebayListEasyNotesProperty != null) {
			ebayImageDirUrl = ebayImageDirUrlProperty.getValue();
		}

		Property listEasyLoginProperty = DAORegistry.getPropertyDAO().get("ebay.listeasy.login");
		String listEasyLogin = "";
		if (listEasyLoginProperty != null) {
			listEasyLogin = listEasyLoginProperty.getValue();
		}

		Property listEasyPasswordProperty = DAORegistry.getPropertyDAO().get("ebay.listeasy.password");
		String listEasyPassword = "";
		if (listEasyPasswordProperty != null) {
			listEasyPassword = listEasyPasswordProperty.getValue();
		}

		Property ebayCrawlerFrequencyProperty = DAORegistry.getPropertyDAO().get("ebay.crawler.frequency");
		Integer ebayCrawlerFrequency = 30;
		if (ebayCrawlerFrequencyProperty != null) {
			ebayCrawlerFrequency = Integer.valueOf(ebayCrawlerFrequencyProperty.getValue());
		}

		Property ebayInvalidAdditionProperty = DAORegistry.getPropertyDAO().get("ebay.invalid.addition");
		Double ebayInvalidAddition = 900.00;
		if (ebayInvalidAdditionProperty != null) {
			ebayInvalidAddition = Double.valueOf(ebayInvalidAdditionProperty.getValue());
		}

		Property ebayTicketIdTTLProperty = DAORegistry.getPropertyDAO().get("ebay.ticketId.ttl");
		Integer ebayTicketIdTTL = 30;
		if (ebayTicketIdTTLProperty != null) {
			ebayTicketIdTTL = Integer.valueOf(ebayTicketIdTTLProperty.getValue());
		}
		String actionRefresh = request.getParameter("action");
		if ("refresh".equals(actionRefresh))  {
			String text = ebayManager.refreshAllEvents();
			mav.addObject("ebayInfo", text.replaceAll("\n", "<br/>")); 
		}
		Property lastUpdateProperty = DAORegistry.getPropertyDAO().get("ebay.check.lastRefresh");
		if (lastUpdateProperty != null) {
			mav.addObject("lastRefresh", lastUpdateProperty.getValue());			
		}
		mav.addObject("subject", subject);
		mav.addObject("emails", recipient);
		mav.addObject("checkPeriod", checkPeriod);
		mav.addObject("rptFactor", rptFactor);
		mav.addObject("fanAddition", fanAddition);
		mav.addObject("invalidAddition", ebayInvalidAddition);
		mav.addObject("rptFactor", rptFactor);
		mav.addObject("categoryScheme", categoryScheme);
		mav.addObject("categorySchemes", Categorizer.getAllCategoryGroups());
		mav.addObject("ebayImageDirUrl", ebayImageDirUrl);
		mav.addObject("ebayListEasyNotes", ebayListEasyNotes);
		mav.addObject("listEasyLogin", listEasyLogin);
		mav.addObject("listEasyPassword", listEasyPassword);
		mav.addObject("netProfitFilter", netProfitFilter);
		mav.addObject("crawlerFrequency", ebayCrawlerFrequency);
		mav.addObject("ticketIdTTL", ebayTicketIdTTL);
		
		return mav;
	}
*/
	/**
	 * Display the admin maintenance page
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */

	public ModelAndView loadAdminMaintenancePage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String tourId = request.getParameter("tourId");
		String action = request.getParameter("action");
		if (action != null) {
			// TODOJMS
			/*if (action.equals("pause")) {
				ticketListingCrawler.pauseCrawlsForTour(Integer.valueOf(tourId));
			} else if (action.equals("refreshAutoCrawler")) {
				creationEventListManager.refreshEventList();
			} else {
				ticketListingCrawler.resumeCrawlsForTour(Integer.valueOf(tourId));
			}*/
		}
		ModelAndView mav = new ModelAndView("page-admin-maintenance");
		mav.addObject("users", DAORegistry.getUserDAO().getAll());
		
		mav.addObject("seatWaveEventPopulatorRunning", SeatWaveEventPopulator.isRunning());
		mav.addObject("seatWaveEventPopulatorFetchedEventCount", SeatWaveEventPopulator.getFetchedEventCount());

		mav.addObject("eiMarketPlaceEventPopulatorRunning", EiMarketPlaceEventPopulator.isRunning());
		mav.addObject("eiMarketPlaceEventPopulatorFetchedEventCount", EiMarketPlaceEventPopulator.getFetchedEventCount());

		mav.addObject("refreshAutoCrawlerRunning", creationEventListManager.isRefreshing());
		return mav;
	}

	public ModelAndView loadTndSalesReport(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("page-admin-tnd");
		
		Date date = new Date();
		//List<String> childs = DAORegistry.getQueryManagerDAO().getAllChild();
		//List<String> venues = DAORegistry.getQueryManagerDAO().getAllVenue();
		List<String> companies = DAORegistry.getQueryManagerDAO().getAllCompanyName();
		List<String> stockTypes = DAORegistry.getQueryManagerDAO().getAllStockType();
		
		mav.addObject("fromDate", "1");
		mav.addObject("fromMonth", "1");
		mav.addObject("fromYear", "2015");
		mav.addObject("toDate", date.getDate());
		mav.addObject("toMonth", date.getMonth()+1);
		mav.addObject("toYear", date.getYear()+1900);
		//mav.addObject("venues", venues);
		mav.addObject("cmpNames", companies);
		mav.addObject("stockTypes", stockTypes);
		
		return mav;
	}
	
	public void downloadTndSalesReport(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", "attachment; fileName=TndSalesReport.csv");
		ServletOutputStream out = response.getOutputStream();
		
		String fromDay = request.getParameter("fromDay");
		String fromMonth = request.getParameter("fromMonth");
		String fromYear = request.getParameter("fromYear");
		
		String toDay = request.getParameter("toDay");
		String toMonth = request.getParameter("toMonth");
		String toYear = request.getParameter("toYear");
		
		String orderStartDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
		String orderEndDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
		
		String parent = request.getParameter("parent");
		String child = request.getParameter("child");
		String venueName = request.getParameter("venue");
		String acceptStatus = request.getParameter("acceptStatus");
		String ticketType = request.getParameter("ticketType");
		//String companyName = request.getParameter("cmpName");
		String stockType = request.getParameter("stockType");
		
		StringBuffer sb = new StringBuffer();
		sb.append("OrderId,OrderDate,Parent,Child,EventName,EventId,VenueName,EventDate,TicketQuantity,Section,Row,WholeSalePrice," +
				  "Revenue,AcceptSubStatus,OwnerCustomerId,OwnerCustomerName,IsCategory,Cost,TicketType,CityName,StateProvince,StockType \n");
		
		List<Object[]> list = DAORegistry.getQueryManagerDAO().getTndSalesReport(orderStartDate,orderEndDate,parent,child,venueName,acceptStatus,
								ticketType,stockType);
		
		if(list!=null && !list.isEmpty()){
			for(Object[] event : list){
				
				for(int i=0 ; i<event.length ;i++){
					if(event[i] == null){
						event[i] = "";
					}
				}
				
				sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
						+ "," + event[3].toString().replaceAll(",", "-") + "," +
						event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")
						+","+ event[6].toString().replaceAll(",", "-")+","+ event[7].toString().replaceAll(",", "-")+","+ event[8].toString().replaceAll(",", "-")+","+
						event[9].toString().replaceAll(",", "-")+","+event[10].toString().replaceAll(",", "-")+","+event[11].toString().replaceAll(",", "-")
						+","+event[12].toString().replaceAll(",", "-")+","+event[13].toString().replaceAll(",", "-")+
						","+event[14].toString().replaceAll(",", "-")+","+event[15].toString().replaceAll(",", "-")+","+event[16].toString().replaceAll(",", "-")
						+","+event[17].toString().replaceAll(",", "-")+","+event[18].toString().replaceAll(",", "-")+
						","+event[19].toString().replaceAll(",", "-")+","+event[20].toString().replaceAll(",", "-")+","+event[21].toString().replaceAll(",", "-")+"\n");
			}
		}
		
		out.write(sb.toString().getBytes());
		out.flush();
		out.close();
		
	}
	
	public void tndGetChildByParent(HttpServletRequest request, HttpServletResponse response) throws JSONException, IOException{
		JSONArray jsonArray = new JSONArray();
		String parent = request.getParameter("parent");
		List<String> childs = DAORegistry.getQueryManagerDAO().getChildsByParent(parent);
		
		for(String child : childs){
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("child", child);
			jsonArray.put(jsonObject);
		}
		IOUtils.write(jsonArray.toString().getBytes(),
				response.getOutputStream());
	}
	
	/**
	 * method invoked when an admin delete a user
	 * the deletion will take care of reassigning ownership
	 * of crawls to the user Admin, delete bookmarks and preferences
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	
	public ModelAndView loadDeleteUserPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		if (username == null || username.trim().isEmpty()) {
			return new ModelAndView(new RedirectView("AdminManageUsers"));			
		}
		
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		if (user == null) {
			return new ModelAndView(new RedirectView("AdminManageUsers"));						
		}

		/*
		 * Update creator of crawls
		 */
		
		// for currently running crawls
		// TODOJMS
		/*Collection<TicketListingCrawl> crawls = ticketListingCrawler.getTicketListingCrawlByCreator(username);
		if(crawls != null && !crawls.isEmpty()){
			for(TicketListingCrawl crawl : crawls) {
				crawl.setCreator("admin");
				crawl.setLastUpdater("admin");
				crawl.setLastUpdated(new Date());
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			}
		}
		
		// update disabled crawls (only in DB)
		crawls = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByCreator(username);
		if(crawls != null && !crawls.isEmpty()){
			for(TicketListingCrawl crawl : crawls) {
				crawl.setCreator("admin");
				crawl.setLastUpdater("admin");
				crawl.setLastUpdated(new Date());
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			}
		}
		
		/ *
		 * Update last updater of crawls for which the user was the last updater
		 * /
		// for currently running crawls
		crawls = ticketListingCrawler.getTicketListingCrawlByLastUpdater(username);
		if(crawls != null && !crawls.isEmpty()){
			for(TicketListingCrawl crawl : crawls) {
				crawl.setLastUpdater("admin");
				crawl.setLastUpdated(new Date());
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			}
		}

		// update disabled crawls (only in DB)
		crawls = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByLastUpdater(username);
		if(crawls != null && !crawls.isEmpty()){
			for(TicketListingCrawl crawl : crawls) {
				crawl.setLastUpdater("admin");
				crawl.setLastUpdated(new Date());
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			}
		}
		*/
		// delete all bookmarks
		DAORegistry.getBookmarkDAO().deleteBookmarksFromUser(username);
		DAORegistry.getUserAlertDAO().deleteAlertsFromUser(username);
		
		// delete preferences
		DAORegistry.getPreferenceDAO().deleteAllPreferences(username);
		// delete user
		if(user!=null){
		DAORegistry.getUserActionDAO().deleteUserActionByUserId(user.getId());
		}
		DAORegistry.getUserDAO().delete(user);
		return new ModelAndView(new RedirectView("AdminManageUsers?info=The user " + username + " has been deleted. All his crawls have been assigned to the user Admin."));
	}
	
	/**
	 * Display Page to Edit User details
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditUserPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("username")	;
		
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		Collection<Role> roles = DAORegistry.getRoleDAO().getAll();
		
		ModelAndView mav = new ModelAndView("page-admin-edit-user");
		mav.addObject("user", user);
		mav.addObject("roles", roles);
		String action = request.getParameter("action");
		
		if (action == null) {
			return mav;
		} else if (action.equals("updateInfo")) {			
			// save info
			String email = request.getParameter("email");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String phone = request.getParameter("phone");
			
			// some validation			
			if (email == null || !Pattern.matches("[\\S&&[^@]]{1,20}@[\\w\\.]*\\.\\w{1,5}",	email.trim())) {
				mav.addObject("userInfoError",  email + " is an invalid E-Mail address");
				return mav;
			}
			
			user.setEmail(email);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setPhone(phone);
			Set<Role> newUserRoles = new HashSet<Role>();
			String requestRole[] = request.getParameterValues("rolesStrList");
			//Broker broker = DAORegistry.getBrokerDAO().getBrokerByEmailSuffix("@"+email.split("@")[1]);
			/*if(broker==null){
				mav.addObject("userInfoError", "Email is not matching with broker.");
				return mav;
			}*/
			Map<String,Role> roleMap = new HashMap<String, Role>();
			
			for (Role role: roles) {
				roleMap.put(role.getName(), role);
			}
			/*if(broker==null || broker.getId()!=1){
				for(String newRequestRole:requestRole){
					if(newRequestRole!=null  && !newRequestRole.toUpperCase().contains("LITE")){
						mav.addObject("userInfoMessage", email.split("@")[1] + " Broker can not have this role.");
						return mav;	
					}
					Role role = roleMap.get(newRequestRole.toUpperCase());
					if (role != null) {
						newUserRoles.add(role);
					}
				}
			}else{*/
				for(String newRequestRole:requestRole){
					Role role = roleMap.get(newRequestRole);
					if (role != null) {
						newUserRoles.add(role);
					}
				}
			//}
				
			user.setRoles(newUserRoles);
			Broker broker=DAORegistry.getBrokerDAO().get(1);
			user.setBroker(broker);
			//user.setBroker(broker);
			
			DAORegistry.getUserDAO().update(user);
			
			mav.addObject("userInfoMessage", "User information has been saved.");
			return mav;
			
		} else if (action.equals("updatePassword")) {
			String password = request.getParameter("password");
			String confirmPassword = request.getParameter("confirmPassword");
			
			if (password == null || !Pattern.matches("\\w{6,45}", password)) {
				mav.addObject("userPasswordError", "Password must contains at least 6 characters (letters and numbers).");
				return mav;
			} 
			
			// check if password are equals
			if (!password.equals(confirmPassword)) {
				mav.addObject("userPasswordError", "Passwords do not match");
				return mav;
			}
			
			user.setAndEncryptPassword(password);
			DAORegistry.getUserDAO().update(user);
			
			mav.addObject("userPasswordMessage", "User password has been saved.");
			return mav;
		} else if (action.equals("updateRoles")) {
			Set<Role> newUserRoles = new HashSet<Role>();
			String requestRole[] = request.getParameterValues("rolesStrList");
//			int flag=0;
//			for (Role role: roles) {
//				if (requestRole != null && requestRole.equalsIgnoreCase(role.getName())) {
//					newUserRoles.add(role);
//				}
//			}
			for (Role role: roles) {
				for(String newRequestRole:requestRole){
					if (requestRole != null && newRequestRole.equalsIgnoreCase(role.getName())) {
						newUserRoles.add(role);
					}
				 }
				}
			user.setRoles(newUserRoles);
			String email = request.getParameter("hiddenEmail");
			
			// some validation			
			if (email == null || !Pattern.matches("[\\S&&[^@]]{1,20}@[\\w\\.]*\\.\\w{1,5}",	email)) {
				mav.addObject("userInfoError",  "Broker is not valid with " + email);
				return mav;
			}
			Broker broker=DAORegistry.getBrokerDAO().get(1);
			user.setBroker(broker);
			 
			//Broker broker = DAORegistry.getBrokerDAO().getBrokerByEmailSuffix("@"+email.split("@")[1]);
//			for(String newRequestRole:requestRole){
//				if(newRequestRole.equals("role_super_admin")){
//					flag=1;
					//user.setBroker(broker);
//				}else{
//					if(flag==0){
//						user.setMultiBrokerAccess(0);
//					}
//				}
//			}
			/*String []brokerIds = request.getParameterValues("brokername");*/
			/*Set<UserBrokerDetails> userBrokerDetails = new HashSet<UserBrokerDetails>();
			if(brokerIds[0].equals("0")){
				for(int i=1;i<6;i++){
					  UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
					  userBrokerDetail.setUser(user);
					  userBrokerDetail.setBrokerId(i);
					  userBrokerDetails.add(userBrokerDetail);
					  	
				}		
				 DAORegistry.getUserBrokerDetailsDAO().deleteBrokerDetailsByUserId(user.getId());
				user.setUserBrokerDetails(userBrokerDetails);	
			}else{
			for(String newRequestRole:requestRole){
				if(newRequestRole.equals("role_lite_user") || newRequestRole.equals("role_lite_admin")|| newRequestRole.equals("role_super_admin")||newRequestRole.equals("role_admin")||newRequestRole.equals("role_editor")||newRequestRole.equals("role_user"))
				{
				//String []brokerIds = request.getParameterValues("brokername");
			   
				// Set<UserBrokerDetails> userBrokerDetails = new HashSet<UserBrokerDetails>();
				if (brokerIds ==null || brokerIds.length==0)
				{
					//brokerId= "1";
					  
						  UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
						  userBrokerDetail.setUser(user);
						  userBrokerDetail.setBrokerId(1);
						  userBrokerDetails.add(userBrokerDetail);
						  user.setUserBrokerDetails(userBrokerDetails);
					  
				}
			        
				  for(String brokerId:brokerIds){
					  UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
					  userBrokerDetail.setUser(user);
					  userBrokerDetail.setBrokerId(Integer.valueOf(brokerId));
					  userBrokerDetails.add(userBrokerDetail);
				  }
				  DAORegistry.getUserBrokerDetailsDAO().deleteBrokerDetailsByUserId(user.getId());
				  user.setUserBrokerDetails(userBrokerDetails);
				//user.setBroker(DAORegistry.getBrokerDAO().get(Integer.parseInt(brokerId)));
				//mav.addObject("brokerId", user.getBroker().getId());
                
				for(UserBrokerDetails userBrokerDetail:user.getUserBrokerDetails()){
					mav.addObject("brokerId", userBrokerDetail.getBrokerId());
					
				}
				
			}
			else
			{
				//user.setBroker(null);
				user.setUserBrokerDetails(null);
			}
		}
	}  */     
			DAORegistry.getUserDAO().update(user);
			mav.addObject("userRolesMessage", "User roles have been saved.");
			return mav;
			// DAORegistry.getUserDAO().updateUserRoles(newUserRoles);

		} else if (action.equals("updateMisc")) {
			String lockedString = request.getParameter("locked");
			boolean locked = "on".equals(lockedString);
			Boolean emailCrawlErrorCreatorEnabled = "on".equals(request.getParameter("emailCrawlErrorCreatorEnabled"));
			Boolean emailCrawlErrorLastUpdaterEnabled = "on".equals(request.getParameter("emailCrawlErrorLastUpdaterEnabled"));
			
			user.setLocked(locked);
			user.setEmailCrawlErrorCreatorEnabled(emailCrawlErrorCreatorEnabled);
			user.setEmailCrawlErrorLastUpdaterEnabled(emailCrawlErrorLastUpdaterEnabled);

			DAORegistry.getUserDAO().update(user);

			mav.addObject("userMiscMessage", "User Misc Info have been saved.");
			return mav;
		} else if (action.equals("logout")) {
			boolean wasLoggedIn = SessionListener.invalidateSessionsForUser(username);
			if (wasLoggedIn) {
				mav.addObject("userActionsMessage", "User " + username + " has been logged out.");
			} else {
				mav.addObject("userActionsMessage", "User " + username + " was not logged in.");				
			}
			return mav;
		} else {
			mav.addObject("error", "Invalid action " + action);
			return mav;
		}
	}

	/**
	 * Display page to show a user's details
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadDetailUserPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-detail-user");
		
		String username = request.getParameter("userName");
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		mav.addObject("user", user);
		mav.addObject("createdCrawls", DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByCreator(username));
		mav.addObject("lastUpdatedCrawls", DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByLastUpdater(username));
		return mav;
	}

	/**
	 * Display the page to edit feedback settings
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditFeedbackSettingsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		if ("update".equals(action)) {
			Property feedbackEmailToProperty = DAORegistry.getPropertyDAO().get("feedback.email.to");
			feedbackEmailToProperty.setValue(request.getParameter("feedbackEmailTo"));
			DAORegistry.getPropertyDAO().update(feedbackEmailToProperty);

			Property feedbackErrorEmailToProperty = DAORegistry.getPropertyDAO().get("feedback.error.email.to");
			feedbackErrorEmailToProperty.setValue(request.getParameter("feedbackErrorEmailTo"));
			DAORegistry.getPropertyDAO().update(feedbackErrorEmailToProperty);

			Property feedbackJiraUriProperty = DAORegistry.getPropertyDAO().get("feedback.jira.uri");
			feedbackJiraUriProperty.setValue(request.getParameter("jiraUri"));
			DAORegistry.getPropertyDAO().update(feedbackJiraUriProperty);

			Property feedbackJiraRpcPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.rpcPath");
			feedbackJiraRpcPathProperty.setValue(request.getParameter("jiraRpcPath"));
			DAORegistry.getPropertyDAO().update(feedbackJiraRpcPathProperty);

			Property feedbackJiraSoapPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.soapPath");
			feedbackJiraSoapPathProperty.setValue(request.getParameter("jiraSoapPath"));
			DAORegistry.getPropertyDAO().update(feedbackJiraSoapPathProperty);

			Property feedbackJiraProjectProperty = DAORegistry.getPropertyDAO().get("feedback.jira.project");
			feedbackJiraProjectProperty.setValue(request.getParameter("jiraProject"));
			DAORegistry.getPropertyDAO().update(feedbackJiraProjectProperty);
			
			Property feedbackJiraComponentsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.components");
			feedbackJiraComponentsProperty.setValue(request.getParameter("jiraComponents"));
			DAORegistry.getPropertyDAO().update(feedbackJiraComponentsProperty);

			Property feedbackJiraAffectsVersionsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.affectsVersions");
			feedbackJiraAffectsVersionsProperty.setValue(request.getParameter("jiraAffectsVersions"));
			DAORegistry.getPropertyDAO().update(feedbackJiraAffectsVersionsProperty);

			Property feedbackJiraUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.jira.username");
			feedbackJiraUsernameProperty.setValue(request.getParameter("jiraUsername"));
			DAORegistry.getPropertyDAO().update(feedbackJiraUsernameProperty);

			Property feedbackJiraEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.jira.enabled");
			feedbackJiraEnabledProperty.setValue("on".equals(request.getParameter("jiraEnabled"))?"1":"0");
			DAORegistry.getPropertyDAO().update(feedbackJiraEnabledProperty);
			
			Property feedbackJiraPasswordProperty = DAORegistry.getPropertyDAO().get("feedback.jira.password");
			feedbackJiraPasswordProperty.setValue(request.getParameter("jiraPassword"));
			DAORegistry.getPropertyDAO().update(feedbackJiraPasswordProperty);

			Property feedbackRedmineUriProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.uri");
			feedbackRedmineUriProperty.setValue(request.getParameter("redmineUri"));
			DAORegistry.getPropertyDAO().update(feedbackRedmineUriProperty);

			Property feedbackRedmineProjectProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.project");
			feedbackRedmineProjectProperty.setValue(request.getParameter("redmineProject"));
			DAORegistry.getPropertyDAO().update(feedbackRedmineProjectProperty);

			Property feedbackRedmineEbayProjectProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.ebay.project");
			feedbackRedmineEbayProjectProperty.setValue(request.getParameter("redmineEbayProject"));
			DAORegistry.getPropertyDAO().update(feedbackRedmineEbayProjectProperty);

			Property feedbackRedmineUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.username");
			feedbackRedmineUsernameProperty.setValue(request.getParameter("redmineUsername"));
			DAORegistry.getPropertyDAO().update(feedbackRedmineUsernameProperty);

			Property feedbackRedminePasswordProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.password");
			feedbackRedminePasswordProperty.setValue(request.getParameter("redminePassword"));
			DAORegistry.getPropertyDAO().update(feedbackRedminePasswordProperty);

			Property feedbackRedmineEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.enabled");
			feedbackRedmineEnabledProperty.setValue("on".equals(request.getParameter("redmineEnabled"))?"1":"0");
			DAORegistry.getPropertyDAO().update(feedbackRedmineEnabledProperty);

			return new ModelAndView(new RedirectView("AdminEditFeedbackSettings?info=The feedback settings have been saved"));			
		} else {
			Property feedbackEmailToProperty = DAORegistry.getPropertyDAO().get("feedback.email.to");
			Property feedbackErrorEmailToProperty = DAORegistry.getPropertyDAO().get("feedback.error.email.to");
			Property feedbackJiraEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.jira.enabled");
			Property feedbackJiraComponentsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.components");
			Property feedbackJiraAffectsVersionsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.affectsVersions");
			Property feedbackJiraUriProperty = DAORegistry.getPropertyDAO().get("feedback.jira.uri");
			Property feedbackJiraRpcPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.rpcPath");
			Property feedbackJiraSoapPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.soapPath");
			Property feedbackJiraProjectProperty = DAORegistry.getPropertyDAO().get("feedback.jira.project");
			Property feedbackJiraUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.jira.username");
			Property feedbackJiraPasswordProperty = DAORegistry.getPropertyDAO().get("feedback.jira.password");
			Property feedbackRedmineEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.enabled");
			Property feedbackRedmineUriProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.uri");
			Property feedbackRedmineProjectProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.project");
			Property feedbackRedmineEbayProjectProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.ebay.project");
			Property feedbackRedmineUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.username");
			Property feedbackRedminePasswordProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.password");
			
			ModelAndView mav = new ModelAndView("page-admin-edit-feedback-settings");
			mav.addObject("feedbackEmailTo", feedbackEmailToProperty.getValue());
			mav.addObject("feedbackErrorEmailTo", feedbackErrorEmailToProperty.getValue());
			mav.addObject("jiraEnabled", "1".equals(feedbackJiraEnabledProperty.getValue()));
			mav.addObject("jiraComponents", feedbackJiraComponentsProperty.getValue());
			mav.addObject("jiraAffectsVersions", feedbackJiraAffectsVersionsProperty.getValue());
			mav.addObject("jiraUri", feedbackJiraUriProperty.getValue());
			mav.addObject("jiraRpcPath", feedbackJiraRpcPathProperty.getValue());
			mav.addObject("jiraSoapPath", feedbackJiraSoapPathProperty.getValue());
			mav.addObject("jiraProject", feedbackJiraProjectProperty.getValue());
			mav.addObject("jiraUsername", feedbackJiraUsernameProperty.getValue());
			mav.addObject("jiraPassword", feedbackJiraPasswordProperty.getValue());

			mav.addObject("redmineEnabled", "1".equals(feedbackRedmineEnabledProperty.getValue()));
			mav.addObject("redmineUri", feedbackRedmineUriProperty.getValue());
			mav.addObject("redmineProject", feedbackRedmineProjectProperty.getValue());
			mav.addObject("redmineEbayProject", feedbackRedmineEbayProjectProperty.getValue());
			mav.addObject("redmineUsername", feedbackRedmineUsernameProperty.getValue());
			mav.addObject("redminePassword", feedbackRedminePasswordProperty.getValue());

			return mav;
		}
	}
	
	/**
	 * Display page to edit eimarketplace settings (e.g., username, password)
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditEiMarketPlaceProperties(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		if ("update".equals(action)) {
			String eiMarketPlaceUsername = request.getParameter("eiMarketPlaceUsername");
			String eiMarketPlacePassword = request.getParameter("eiMarketPlacePassword");
			
			Property eiMarketPlaceUsernameProperty = DAORegistry.getPropertyDAO().get("eimarketplace.username");
			eiMarketPlaceUsernameProperty.setValue(eiMarketPlaceUsername);
			DAORegistry.getPropertyDAO().update(eiMarketPlaceUsernameProperty);
			
			Property eiMarketPlacePasswordProperty = DAORegistry.getPropertyDAO().get("eimarketplace.password");
			eiMarketPlacePasswordProperty.setValue(eiMarketPlacePassword);
			DAORegistry.getPropertyDAO().update(eiMarketPlacePasswordProperty);
			
			return new ModelAndView(new RedirectView("AdminEditEiMarketPlaceProperties?info=The EiMarketPlace properties have been saved"));			
		} else {
			ModelAndView mav = new ModelAndView("page-admin-edit-eimarketplace-properties");
			mav.addObject("eiMarketPlaceUsername", DAORegistry.getPropertyDAO().get("eimarketplace.username").getValue());
			return mav;
		}
	}
	
	/**
	 * Display Page to edit alert settings
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditAlertSettingsPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if ("update".equals(action)) {
			Property alertThreadCountProperty = DAORegistry.getPropertyDAO().get("alert.thread.count");
			alertThreadCountProperty.setValue(request.getParameter("alertThreadCount"));
			DAORegistry.getPropertyDAO().update(alertThreadCountProperty);

			Property alertMailCcProperty = DAORegistry.getPropertyDAO().get("alert.mail.cc");
			alertMailCcProperty.setValue(request.getParameter("alertMailCc"));
			DAORegistry.getPropertyDAO().update(alertMailCcProperty);

			Property alertMailBccProperty = DAORegistry.getPropertyDAO().get("alert.mail.bcc");
			alertMailBccProperty.setValue(request.getParameter("alertMailBcc"));
			DAORegistry.getPropertyDAO().update(alertMailBccProperty);
			
			
			/*Property alertPortalMailCcProperty = DAORegistry.getPropertyDAO().get("alert.portalmail.cc");
			alertPortalMailCcProperty.setValue(request.getParameter("alertPortalMailCc"));
			DAORegistry.getPropertyDAO().update(alertPortalMailCcProperty);

			Property alertPortalMailBccProperty = DAORegistry.getPropertyDAO().get("alert.portalmail.bcc");
			alertPortalMailBccProperty.setValue(request.getParameter("alertPortalMailBcc"));
			DAORegistry.getPropertyDAO().update(alertPortalMailBccProperty);		*/	

			Property alertMailMarketMakerEnabledProperty = DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled");
			alertMailMarketMakerEnabledProperty.setValue("on".equals(request.getParameter("alertMailMarketMakerEnabled"))?"1":"0");
			DAORegistry.getPropertyDAO().update(alertMailMarketMakerEnabledProperty);

			return new ModelAndView(new RedirectView("AdminEditAlertSettings?info=The alert settings have been saved"));			
		} else {
			ModelAndView mav = new ModelAndView("page-admin-edit-alert-properties");
			mav.addObject("alertThreadCount", Integer.parseInt(DAORegistry.getPropertyDAO().get("alert.thread.count").getValue()));
			mav.addObject("alertMailCc", DAORegistry.getPropertyDAO().get("alert.mail.cc").getValue());
			mav.addObject("alertMailBcc", DAORegistry.getPropertyDAO().get("alert.mail.bcc").getValue());
//			mav.addObject("alertPortalMailCc", DAORegistry.getPropertyDAO().get("alert.portalmail.cc").getValue());
//			mav.addObject("alertPortalMailBcc", DAORegistry.getPropertyDAO().get("alert.portalmail.bcc").getValue());
			mav.addObject("alertMailMarketMakerEnabled", "1".equals(DAORegistry.getPropertyDAO().get("alert.mail.marketmaker.enabled").getValue()));
			
			return mav;
		}
	}
	
	/**
	 * Display page for Stubhub Instant Tickets
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadInstantTicketsCsvPage(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-instant-tickets");
		
		String action = request.getParameter("action");
		if(action == null){
			Property property = DAORegistry.getPropertyDAO().get("instantTickets.eventIds");
		
			if (property != null) {
				String eventIds = property.getValue();
				mav.addObject("eventIds", eventIds);
			}
			return mav;
		}
		
			if (action.equals("update")){
				String eventIds= request.getParameter("eventIds");
				eventIds = eventIds.replaceAll("\\s+", "").trim();

				String formattedEventIds = "";
				String error = "";
				// remove duplicate eventIds
				Set<Integer> distinctEventIds = new HashSet<Integer>();
				for (String eventIdStr: eventIds.split(",")) {
					try {
						Integer eventId = Integer.valueOf(eventIdStr.trim());
						distinctEventIds.add(eventId);
					} catch(Exception e) {
						error += "Event ID " + eventIdStr + " is invalid.\n";
					}	
				}
				// check if eventId is valid
				for(Integer distinctEventId:distinctEventIds ){
					Event event = DAORegistry.getEventDAO().get(distinctEventId);
					if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
					//	if (event.getDate().getTime()- new Date().getTime()>24L*60L*60L*1000L){
						formattedEventIds += distinctEventId + ",";
					}
				//	}
				}
			
				if (formattedEventIds.length() >= 8000) {
					error += "Too many event Ids (the string should be less than 8,000 characters)\n";
				}
				
				if (!error.isEmpty()) {
					mav.addObject("error", error);				
					mav.addObject("eventIds", eventIds);	
				}
				
				if (!formattedEventIds.isEmpty()) {
					formattedEventIds = formattedEventIds.substring(0, formattedEventIds.length() - 1);
				}

				Property property = new Property("instantTickets.eventIds", formattedEventIds);
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
				mav.addObject("InfoMessage", "EventIds have been updated.");
			}
			return new ModelAndView(new RedirectView("InstantTickets?info=EventIds have been updated"));
	}
	
	
	/**
	 * Download Stubhub Instant Tickets as csv file
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException 
	 */	
	/*public ModelAndView downloadInstantTicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		InstantTicketsUtil.downloadInstantTicketCsvFile(response);
		return null;
	}*/
	
	
	/**
	 * Download Stubhub Instant TND Tickets as csv file
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException 
	 */	
	/*public ModelAndView downloadInstantTicketTNDCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		InstantTicketsUtil.downloadInstantTNDTicketCsvFile(response);
		return null;
	}*/
	
	
	/**
	 * Download Customized Stubhub Instant Tickets as csv file
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */	
	/*public ModelAndView downloadCustomizedInstantTicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		InstantTicketsUtil.downloadCustomizedInstantTicketCsvFile(response);
		return null;
	}*/
	
	
	/* To add existing events to the instant events table */
	public ModelAndView loadBrowseInstantEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		ModelAndView mav = new ModelAndView("page-admin-instant");
		/*String tourIdStr = request.getParameter("tourId");
		
		String action = request.getParameter("action");

		Integer tourId;
		
		Collection<Event> events = new ArrayList<Event>();
		
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtists();		
		
		if (tourIdStr == null || tourIdStr.equalsIgnoreCase("ALL")) {
			mav.addObject("tourId", "ALL");
			events = DAORegistry.getEventDAO().getAllActiveEvents();
		}else {
			tourId = Integer.valueOf(tourIdStr);
			mav.addObject("tourId", tourId);
			events = DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE);
		}
		
		mav.addObject("artists", artists);
		mav.addObject("events", events);
		
		if (action != null && action.equalsIgnoreCase("addSH")) {
			String info = "";
			String existEventInfo="";
			String notExistEventInfo="";
			Event event = null;
			Collection<Event> eventList=new ArrayList<Event>();
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				InstantEvent instantEvent = DAORegistry.getInstantEventDAO().getInstantEventByEventId(eventId);
				if(instantEvent==null){
					event = DAORegistry.getEventDAO().get(eventId);
					if (event != null) {
						notExistEventInfo += eventId + ",";
						eventList.add(event);
					}
				}else{
					existEventInfo += eventId + ",";
				}
				
			}
			if(!existEventInfo.isEmpty()){
				existEventInfo.substring(0, existEventInfo.length()-2);
				existEventInfo += " are already exist in SH Instant events. <br/>";
			}
			if(!notExistEventInfo.isEmpty()){
				notExistEventInfo.substring(0, notExistEventInfo.length()-2);
				notExistEventInfo += " have been added to the SH Instant events. <br/>";
			}
			
			InstantEventManager.saveInstantEvents(eventList);
			
			info = notExistEventInfo + existEventInfo;
			mav.addObject("info", info);

		}	
			
		if (action != null && action.equalsIgnoreCase("addTnow")) {
			String info = "";
			String existEventInfo="";
			String notExistEventInfo="";
			Event event = null;
			Collection<Event> eventList=new ArrayList<Event>();
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				TnowInstantEvent tnowInstantEvent = DAORegistry.getTnowInstantEventDAO().getTnowInstantEventByEventId(eventId);
				if(tnowInstantEvent==null){
					event = DAORegistry.getEventDAO().get(eventId);
					if (event != null) {
						notExistEventInfo += eventId + ",";
						eventList.add(event);
					}
				}else{
					existEventInfo += eventId + ",";
				}
				
			}
			if(!existEventInfo.isEmpty()){
				existEventInfo = existEventInfo.substring(0, existEventInfo.length()-2);
				existEventInfo += " are already exist in TNow Instant events. <br/>";
			}
			if(!notExistEventInfo.isEmpty()){
				notExistEventInfo = notExistEventInfo.substring(0, notExistEventInfo.length()-2);
				notExistEventInfo += " have been added to the TNow Instant events. <br/>";
			}
			
			TNowInstantEventManager.saveTNowInstantEvents(eventList);
			
			info = notExistEventInfo + existEventInfo;
			mav.addObject("info", info);

		}	
		
		if (action != null && action.equalsIgnoreCase("addTN")) {
			String info = "";
			String existEventInfo="";
			String notExistEventInfo="";
//			Collection<Integer> eventIds = new ArrayList<Integer>();
			Event event = null;
			Collection<Event> eventList=new ArrayList<Event>();
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				TNInstantEvent tnInstantEvent = DAORegistry.getTninstantEventDAO().getTNInstantEventByEventId(eventId);
				if(tnInstantEvent==null){
					event = DAORegistry.getEventDAO().get(eventId);
					if (event != null) {
						notExistEventInfo += eventId + ",";
						eventList.add(event);
					}
				}else{
					existEventInfo += eventId + ",";
				}
			
			}
			
			if(!existEventInfo.isEmpty()){
				existEventInfo = existEventInfo.substring(0, existEventInfo.length()-2);
				existEventInfo += " are already exist in TN Instant events. <br/>";
			}
			if(!notExistEventInfo.isEmpty()){
				notExistEventInfo = notExistEventInfo.substring(0, notExistEventInfo.length()-2);
				notExistEventInfo += " have been added to the TN Instant events. <br/>";
			}
			
			TNEventManager.saveTNInstantEvents(eventList);
			
			info = notExistEventInfo + existEventInfo;
			mav.addObject("info", info);

			}	
		
		
		if (action != null && action.equalsIgnoreCase("addEI")) {
			String info = "";
			Collection<Integer> eventIds = new ArrayList<Integer>();
			Event event = null;
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				eventIds.add(eventId);
				
				if (event == null) {
					event = DAORegistry.getEventDAO().get(eventId);					
				}
			}
			EIEventManager.saveEIInstantEvents(eventIds);
			
			info += "The selected events have been added to EI instant events";
			mav.addObject("info", info);
			}	
	*/	
		return mav;
	}
	
	public ModelAndView loadEditInstantEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action")==null?"":request.getParameter("action");
		String tourIdStr = request.getParameter("tourId");
//		String eventIdStr = request.getParameter("eventId");
		
		ModelAndView mav = new ModelAndView("page-admin-edit-instant");
		/*Integer eventId = null;
		Collection<InstantEvent> instantEvents = new ArrayList<InstantEvent>();
		
		if(!"".equals(action)){
			mav = new ModelAndView("redirect:EditInstantEvents?tourId=" + tourIdStr);
			List<Integer> eventIds= new ArrayList<Integer>();
			Map<String, String[]> map=request.getParameterMap();
			
			if(action.equalsIgnoreCase("removeEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
//					System.out.println(map.get("event_" + eventId)[0]);
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					eventIds.add(eventId);
				}
				DAORegistry.getInstantEventDAO().deleteEventsByIds(eventIds);
			}else if(action.equalsIgnoreCase("updateEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					InstantEvent event= DAORegistry.getInstantEventDAO().getInstantEventByEventId(eventId);
					event.setMinThreshold(Double.parseDouble(map.get("minThreshold-" +"event_" + eventId )[0]));
					event.setMaxThreshold(Double.parseDouble(map.get("maxThreshold-" +"event_" + eventId )[0]));
					event.setExpiryTime(Integer.parseInt((map.get("timeExpiry-" +"event_" + eventId )[0])));
					event.setMarkupPercent(Double.parseDouble(map.get("markupPercent-" +"event_" + eventId )[0]));
					event.setSalesPercent(Double.parseDouble(map.get("salesPercent-" +"event_" + eventId)[0]));
					event.setShippingFee(Double.parseDouble(map.get("shippingFee-" +"event_" + eventId)[0]));
					instantEvents.add(event);
				}
				DAORegistry.getInstantEventDAO().updateAll(instantEvents);
			}
//			return new ModelAndView(new RedirectView("EditInstantEvents?tourId="+tourId , true));
		}
		
		if(tourIdStr!=null){
			mav.addObject("tourId", tourIdStr);
			tourIdStr=tourIdStr.replace("ALL", "");
		}
		Collection<Artist> artists = DAORegistry.getInstantTourDAO().getAllToursWithInstantEvents();
		mav.addObject("artists", artists);
		if (artists.isEmpty()){
			return mav;
		}
		
		Integer tourId = null;
		if(tourIdStr!=null && !"".equals(tourIdStr.trim())){
			tourId = Integer.parseInt(tourIdStr);
		}
		
		if (tourId !=null){
			instantEvents=DAORegistry.getInstantEventDAO().getAllInstantEventsByTour(tourId);
		}else{
			instantEvents=DAORegistry.getInstantEventDAO().getAll();
		}
		mav.addObject("instantEvents", instantEvents);
		*/
		return mav;
	}
	
	/*public ModelAndView loadBrowseZonePlatformEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		
		String tourIdStr = request.getParameter("tourId");
		
		String action = request.getParameter("action");

		ModelAndView mav = new ModelAndView("page-admin-zone");

		Integer tourId;
		
		Collection<Event> events = new ArrayList<Event>();
		
		Collection<Artist> artists = DAORegistry.getTourDAO().getAllActiveToursWithEvents();		
		
		if (tourIdStr == null || tourIdStr.equalsIgnoreCase("ALL")) {
			mav.addObject("tourId", "ALL");
			events = DAORegistry.getEventDAO().getAllActiveEvents();
		}else {
			tourId = Integer.valueOf(tourIdStr);
			mav.addObject("tourId", tourId);
			events = DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE);
		}
		
		mav.addObject("artists", artists);
		mav.addObject("events", events);
		
		if (action != null && action.equalsIgnoreCase("addEventsToZone")) {
			String info = "";
			Collection<Integer> eventIds = new ArrayList<Integer>();
			
			Collection<Event> zoneeventList = new ArrayList<Event>();
		
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				eventIds.add(eventId);
				Event event = null;
				if (event == null) {
					event = DAORegistry.getEventDAO().get(eventId);
					event.setZoneEvent(Boolean.TRUE);
					zoneeventList.add(event);
				}
			}
		
			DAORegistry.getEventDAO().saveOrUpdateAll(zoneeventList);
			
			info += "The selected events have been added to zone platform";
			mav.addObject("info", info);
			}	
			

		
		return mav;
	}*/
	
	
	/*public ModelAndView loadBrowseZoneViewEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		String tourIdStr = request.getParameter("tourId");
		
		String action = request.getParameter("action");

		ModelAndView mav = new ModelAndView("page-admin-view-zone");

		Integer tourId;
		
		Collection<Event> events = new ArrayList<Event>();
		
		Collection<Artist> artists = DAORegistry.getTourDAO().getAllActiveToursWithEvents();		
		
		if (tourIdStr == null || tourIdStr.equalsIgnoreCase("ALL")) {
			mav.addObject("tourId", "ALL");
			events = DAORegistry.getEventDAO().getAllActiveZoneEvents();
		}else {
			tourId = Integer.valueOf(tourIdStr);
			mav.addObject("tourId", tourId);
			events = DAORegistry.getEventDAO().getAllActiveZoneEventsByTour(tourId);
		}
		
		mav.addObject("artists", artists);
		mav.addObject("events", events);
		
		if (action != null && action.equalsIgnoreCase("removeEventsFromZone")) {
			String info = "";
			Collection<Integer> eventIds = new ArrayList<Integer>();
			
			Collection<Event> zoneeventList = new ArrayList<Event>();
		
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				eventIds.add(eventId);
				Event event = DAORegistry.getEventDAO().get(eventId);
				if (event != null) {
					//event = DAORegistry.getEventDAO().get(eventId);
					event.setZoneEvent(Boolean.FALSE);
					zoneeventList.add(event);
				}
			}
		
			DAORegistry.getEventDAO().saveOrUpdateAll(zoneeventList);
			
			info += "The selected events have been added to zone platform";
			mav.addObject("info", info);
			}	
			

		
		return mav;
		
	}*/
	/*public ModelAndView loadEditEIInstantEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action");
		String tourIdStr = request.getParameter("tourId");
		String eventIdStr = request.getParameter("eventId");
		
		ModelAndView mav = new ModelAndView("page-admin-edit-eiinstant");
	
		Collection<Artist> artists = DAORegistry.getEiinstantTourDAO().getAllToursWithInstantEvents();
		
		if (artists.isEmpty()){
			return mav;
		}
		
		mav.addObject("artists", artists);
		
		Integer tourId = null;
		Integer eventId = null;
		EIInstantTour instantTour = null;
		EIInstantEvent instantEvent = null;
		
		
		if (tourIdStr == null || tourIdStr.isEmpty()) {
//			if (!artists.isEmpty()) {
//				tourId = artists.iterator().next().getId();
//			}
//			mav.addObject("tourId", tourId);
			return mav;
		} 
		else if (tourIdStr.equals("ALL")) {
			tourId = null;
			eventIdStr = "ALL";
			mav.addObject("tourId", "ALL");
			mav.addObject("eventId", "ALL");
			return mav;
		}
		else {
			tourId = Integer.valueOf(tourIdStr);
//			if (artists.isEmpty()) {
//				tourId = null;
//				mav.addObject("tourId", "ALL");
//				mav.addObject("eventId", "ALL");
//			}
			if (tourId!=0 && DAORegistry.getEiinstantTourDAO().get(tourId) == null){
				tourId = 0;
			}
			
			instantTour = DAORegistry.getEiinstantTourDAO().get(tourId);
			if (instantTour!=null){
			mav.addObject("tourId", tourId);
			mav.addObject("tourExpiry", instantTour.getExpiryTime());
			mav.addObject("tourMarkupPercent", instantTour.getMarkupPercent());
			mav.addObject("addlTourMarkupPercent", instantTour.getSalesPercent());
			mav.addObject("tourShippingFee", instantTour.getShippingFee());
			mav.addObject("minTourThreshold", instantTour.getMinThreshold());
			mav.addObject("maxTourThreshold", instantTour.getMaxThreshold());
			}
		}
		

		if (tourId != null && tourId!=0) {
			Collection<Event> events = DAORegistry.getEiinstantEventDAO().getAllInstantEventsByTour(tourId);
			
			if (events.isEmpty()){
				return mav;
			}
			mav.addObject("events", events);

			if (eventIdStr == null || eventIdStr.isEmpty()) {
//				if (!events.isEmpty()) {
//					eventId = events.iterator().next().getId();
//				} else {
					return mav;
				}
			 else if (eventIdStr.equals("ALL")) {
				eventId = null;
				mav.addObject("eventId", "ALL");
			} 
			else {
				eventId = Integer.valueOf(eventIdStr);
			//	Event event = DAORegistry.getEventDAO().get(eventId);
				 instantEvent = DAORegistry.getEiinstantEventDAO().get(eventId);
//				
//				if (!event.getInstantTour().getTourId().equals(tourId)) {
//					eventId = events.iterator().next().getId();
//				}
			//	InstantEvent instantEvent = DAORegistry.getInstantEventDAO().get(eventId);
				if (instantEvent!=null){
				mav.addObject("eventId", eventId);
				mav.addObject("eventExpiry", instantEvent.getExpiryTime());
				mav.addObject("markupPercent", instantEvent.getMarkupPercent());
				mav.addObject("addlMarkupPercent", instantEvent.getSalesPercent());
				mav.addObject("shippingFee", instantEvent.getShippingFee());
				mav.addObject("minThreshold", instantEvent.getMinThreshold());
				mav.addObject("maxThreshold", instantEvent.getMaxThreshold());
				}
			} 
		}
		if (action == null || action.equals("")||(tourId ==null && eventId ==null)){
				return mav;
		}
		else {
		if (tourId != null && eventId == null) {
			instantTour = DAORegistry.getEiinstantTourDAO().get(tourId);
			Integer expiryTime = 24;
			Double markup = 0.0;
			Double sales = 0.0; 
			Double shipping = 0.0;
			Double minThreshold = 0.0;
			Double maxThreshold = 0.0;
			if (action.equals("updateTourExpiry")) {
				String tourExpiryStr = request.getParameter("tourExpiry");
				if (tourExpiryStr!=null && !tourExpiryStr.isEmpty()){
					try {
				expiryTime = Integer.valueOf(tourExpiryStr);
					}catch (NumberFormatException nfe) {}
				}
				instantTour.setExpiryTime(expiryTime);
			}

			if 	(action.equals("changeTourMarkupPercent")){
				String markupStr = request.getParameter("tourMarkupPercent");
				if (markupStr!=null && !markupStr.isEmpty()){
				try{
				markup = Double.valueOf(markupStr);
				}catch (NumberFormatException nfe) {}
				}
				instantTour.setMarkupPercent(markup);
			}
				
			if (action.equals("changeAddlTourMarkupPercent")){
				String addlMarkupStr = request.getParameter("addlTourMarkupPercent");
				if (addlMarkupStr!=null && !addlMarkupStr.isEmpty()){
					try{
						sales =  Double.valueOf (addlMarkupStr);
					}catch (NumberFormatException nfe) {}
				}
				instantTour.setSalesPercent(sales);
			}
			
			if (action.equals("changeTourShippingFee")){
				String shippingFeeStr = request.getParameter("tourShippingFee");
				if (shippingFeeStr!=null && !shippingFeeStr.isEmpty()){
					try{
				shipping = Double.valueOf(shippingFeeStr);
					}catch (NumberFormatException nfe) {}
				}
				instantTour.setShippingFee(shipping);	
			}
			
			if (action.equals("changeMinTourThreshold")){
				String minThresholdStr = request.getParameter("minTourThreshold");
				if (minThresholdStr!=null && !minThresholdStr.isEmpty()){
					try {
				minThreshold = Double.valueOf(minThresholdStr);
				}catch (NumberFormatException nfe) {}
				}
				instantTour.setMinThreshold(minThreshold);
			} 
			
			if	(action.equals ("changeMaxTourThreshold")){
				String maxThresholdStr = request.getParameter("maxTourThreshold");
				if (maxThresholdStr!=null && !maxThresholdStr.isEmpty()){
					try {
				maxThreshold = Double.valueOf(maxThresholdStr);
				}catch (NumberFormatException nfe) {}
				}
				instantTour.setMaxThreshold(maxThreshold);
			}	

			DAORegistry.getEiinstantTourDAO().update(instantTour);
			Collection<EIInstantEvent> instantEvents = DAORegistry.getEiinstantEventDAO().getInstantEventsByTour(tourId);
			for (EIInstantEvent iEvent: instantEvents){
				iEvent.setExpiryTime(instantTour.getExpiryTime());
				iEvent.setMarkupPercent(instantTour.getMarkupPercent());
				iEvent.setSalesPercent(instantTour.getSalesPercent());
				iEvent.setShippingFee(instantTour.getShippingFee());
				iEvent.setMinThreshold(instantTour.getMinThreshold());
				iEvent.setMaxThreshold(instantTour.getMaxThreshold());
			//	DAORegistry.getEiinstantEventDAO().update(iEvent);
			}
			DAORegistry.getEiinstantEventDAO().updateAll(instantEvents);
			mav.addObject("tourExpiry", instantTour.getExpiryTime());
			mav.addObject("tourMarkupPercent", instantTour.getMarkupPercent());
			mav.addObject("addlTourMarkupPercent", instantTour.getSalesPercent());
			mav.addObject("tourShippingFee", instantTour.getShippingFee());
			mav.addObject("minTourThreshold", instantTour.getMinThreshold());
			mav.addObject("maxTourThreshold", instantTour.getMaxThreshold());
		
			if (action.equalsIgnoreCase("removeTour")) {
				instantTour = DAORegistry.getEiinstantTourDAO().get(tourId);
				if (instantTour!=null){
					EIEventManager.deleteInstantTour(tourId);
				}
				String info = "The instant artist " +instantTour.getTourId()
			+ "  and all the events of the artist has been deleted!";
				mav.addObject("info ", info);
		return new ModelAndView(new RedirectView(
				"EditEIInstantEvents?tourId=ALL&info=" + info, true));
			}
		}
		
		if (eventId != null && tourId != null) {
			Integer eventExpiry = 24;
			Double eventMarkup = 0.0;
			Double eventSales = 0.0; 
			Double eventShipping = 0.0;
			Double eventMinThreshold = 0.0;
			Double eventMaxThreshold = 0.0;
			instantEvent = DAORegistry.getEiinstantEventDAO().get(eventId);
		
			if (action.equals("updateEventExpiry")){
				String eventExpiryStr = request.getParameter("eventExpiry");
				if (eventExpiryStr!=null && !eventExpiryStr.isEmpty()){
					eventExpiry = Integer.valueOf(eventExpiryStr);
				}
				instantEvent.setExpiryTime(eventExpiry);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
			
			if (action.equals("changeMarkupPercent")){	
				String eventMarkupStr = request.getParameter("markupPercent");
				if (eventMarkupStr!=null && !eventMarkupStr.isEmpty()){
					try{
					eventMarkup = Double.valueOf(eventMarkupStr);
					}catch (NumberFormatException nfe){}
				}
				instantEvent.setMarkupPercent(eventMarkup);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
			
			if (action.equals("changeAddlMarkupPercent")){
				String eventSalesStr = request.getParameter("addlMarkupPercent");
				if (eventSalesStr!=null && !eventSalesStr.isEmpty()){
					try{
					eventSales = Double.valueOf(eventSalesStr);
					}catch (NumberFormatException nfe){}
				}
				instantEvent.setSalesPercent(eventSales);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
			
			if (action.equals("changeShippingFee")){
				String eventShippingStr = request.getParameter("shippingFee");
				if (eventShippingStr!=null && !eventShippingStr.isEmpty()){
					try{
					eventShipping = Double.valueOf(eventShippingStr);
					}catch (NumberFormatException nfe){}
				}
				instantEvent.setShippingFee(eventShipping);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
			
			if (action.equals("changeMinThreshold")){
				String eventMinThresholdStr = request.getParameter("minThreshold");
				if (eventMinThresholdStr!=null && !eventMinThresholdStr.isEmpty()){
					try{
					eventMinThreshold= Double.valueOf(eventMinThresholdStr);
					}catch (NumberFormatException nfe){}
				}
				instantEvent.setMinThreshold(eventMinThreshold);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
				
			if (action.equals("changeMaxThreshold")){
				String eventMaxThresholdStr = request.getParameter("maxThreshold");
				if (eventMaxThresholdStr!=null && !eventMaxThresholdStr.isEmpty()){
					try{
					eventMaxThreshold = Double.valueOf(eventMaxThresholdStr);
					}catch (NumberFormatException nfe){}
				}
				instantEvent.setMaxThreshold(eventMaxThreshold);
				DAORegistry.getEiinstantEventDAO().update(instantEvent);
			}
		//	DAORegistry.getEiinstantEventDAO().update(instantEvent);
			mav.addObject("eventExpiry", instantEvent.getExpiryTime());
			mav.addObject("markupPercent", instantEvent.getMarkupPercent());
			mav.addObject("addlMarkupPercent", instantEvent.getSalesPercent());
			mav.addObject("shippingFee", instantEvent.getShippingFee());
			mav.addObject("minThreshold", instantEvent.getMinThreshold());
			mav.addObject("maxThreshold", instantEvent.getMaxThreshold());
			
			if (action.equalsIgnoreCase("removeEvent")) {
				instantEvent = DAORegistry.getEiinstantEventDAO().get(eventId);
				if (instantEvent!=null){
					EIEventManager.deleteInstantEvents(eventId);
				}
				String info = "The instant event " +instantEvent.getEventId()
			+ " has been deleted!";
				mav.addObject("info ", info);
		return new ModelAndView(new RedirectView(
				"EditEIInstantEvents?tourId="+tourId+"&eventId=ALL&action=&info=" + info, true));
			}
		}	
		}
		
		return mav;
	}
	*/
	
	/*public ModelAndView loadEditTNInstantEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-edit-tninstant");
		String tourId= request.getParameter("tourId");
		String action= request.getParameter("action")==null?"":request.getParameter("action");
		Collection<Artist> artists=DAORegistry.getTourDAO().getAllToursFromTNInstatnEvent();
		mav.addObject("artists", artists);
		if(tourId==null || tourId.trim().isEmpty()){
			Collection<TNInstantEvent> events=null;
			events= DAORegistry.getTninstantEventDAO().getAll();
			mav.addObject("instantEvents", events);
			return mav;
		}	
			
		Integer eventId=null;
		if(!"".equals(action)){
			mav = new ModelAndView("redirect:EditTNInstantEvents?tourId=" + tourId);
			List<TNInstantEvent> tnInstantEvents = new ArrayList<TNInstantEvent>();
			List<Integer> eventIds= new ArrayList<Integer>();
			Map<String, String[]> map=request.getParameterMap();
			String info="";
			if(action.equalsIgnoreCase("removeEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					eventIds.add(eventId);
				}
				if(DAORegistry.getTninstantEventDAO().deleteEventsByIds(eventIds)){
					info="Events deleted successfully.";
				}else{
					info="Error while deleting events.";
				}
			}else if(action.equalsIgnoreCase("updateEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					TNInstantEvent event= DAORegistry.getTninstantEventDAO().getTNInstantEventByEventId(eventId);
					event.setMinThreshold(Double.parseDouble(map.get("minThreshold-" +"event_" + eventId )[0]));
					event.setMaxThreshold(Double.parseDouble(map.get("maxThreshold-" +"event_" + eventId )[0]));
					event.setExpiryTime(Integer.parseInt((map.get("timeExpiry-" +"event_" + eventId )[0])));
					event.setMarkupPercent(Double.parseDouble(map.get("markupPercent-" +"event_" + eventId )[0]));
					event.setSalesPercent(Double.parseDouble(map.get("salesPercent-" +"event_" + eventId)[0]));
					event.setShippingFee(Double.parseDouble(map.get("shippingFee-" +"event_" + eventId)[0]));
					tnInstantEvents.add(event);
				}
				if(DAORegistry.getTninstantEventDAO().updateAll(tnInstantEvents)){
					info="Events updated successfully.";
				}else{
					info="Error while updating events.";
				}
			}
			mav.addObject("info", info);
		}
		Collection<TNInstantEvent> events=null;
		if(!tourId.equalsIgnoreCase("All")){
			Integer tourInt = Integer.parseInt(tourId);
			events= DAORegistry.getTninstantEventDAO().getAllTNInstantEventsByTour(tourInt);
		}else{
			events= DAORegistry.getTninstantEventDAO().getAll();
		}
		
		mav.addObject("instantEvents", events);
		mav.addObject("tourId", tourId);
		return mav;
	}
	*/
	
	/*public ModelAndView loadEditTNowInstantEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-edit-tnowinstant");
		String tourId= request.getParameter("tourId");
		String action= request.getParameter("action")==null?"":request.getParameter("action");
		Collection<Artist> artists=DAORegistry.getTourDAO().getAllToursFromTNowInstatnEvent();
		mav.addObject("artists", artists);
		if(tourId==null || tourId.trim().isEmpty()){
			Collection<TnowInstantEvent> events=null;
			events= DAORegistry.getTnowInstantEventDAO().getAll();
			mav.addObject("instantEvents", events);
			return mav;
		}	
			
		Integer eventId=null;
		if(!"".equals(action)){
			mav = new ModelAndView("redirect:EditTNowInstantEvents?tourId=" + tourId);
			List<TnowInstantEvent> tnowInstantEvents = new ArrayList<TnowInstantEvent>();
			List<Integer> eventIds= new ArrayList<Integer>();
			Map<String, String[]> map=request.getParameterMap();
			String info="";
			if(action.equalsIgnoreCase("removeEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
//					System.out.println(map.get("event_" + eventId)[0]);
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					eventIds.add(eventId);
				}
				if(DAORegistry.getTnowInstantEventDAO().deleteEventsByIds(eventIds)){
					info="Events deleted successfully.";
				}else{
					info="Error while deleting events.";
				}
			}else if(action.equalsIgnoreCase("updateEvents")){
				for (Object obj: map.keySet()) {
					String param = (String) obj;
					if (!param.startsWith("event_")) {
						continue;
					}
					eventId = Integer.valueOf(param.split("-")[0].replaceAll("event_", ""));
					TnowInstantEvent event= DAORegistry.getTnowInstantEventDAO().getTnowInstantEventByEventId(eventId);
					event.setMinThreshold(Double.parseDouble(map.get("minThreshold-" +"event_" + eventId )[0]));
					event.setMaxThreshold(Double.parseDouble(map.get("maxThreshold-" +"event_" + eventId )[0]));
					event.setExpiryTime(Integer.parseInt((map.get("timeExpiry-" +"event_" + eventId )[0])));
					event.setMarkupPercent(Double.parseDouble(map.get("markupPercent-" +"event_" + eventId )[0]));
					event.setSalesPercent(Double.parseDouble(map.get("salesPercent-" +"event_" + eventId)[0]));
					event.setShippingFee(Double.parseDouble(map.get("shippingFee-" +"event_" + eventId)[0]));
					tnowInstantEvents.add(event);
				}
				if(DAORegistry.getTnowInstantEventDAO().updateAll(tnowInstantEvents)){
					info="Events updated successfully.";
				}else{
					info="Error while updating events.";
				}
			}
			mav.addObject("info", info);
//			return new ModelAndView(new RedirectView("EditInstantEvents?tourId="+tourId , true));
		}
		Collection<TnowInstantEvent> events=null;
		if(!tourId.equalsIgnoreCase("All")){
			Integer tourInt = Integer.parseInt(tourId);
			events= DAORegistry.getTnowInstantEventDAO().getAllTnowInstantEventsByTour(tourInt);
		}else{
			events= DAORegistry.getTnowInstantEventDAO().getAll();
		}
		
		mav.addObject("instantEvents", events);
		mav.addObject("tourId", tourId);
		return mav;
	}*/
	
	/*public ModelAndView loadEditInstantEventSettingsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-admin-edit-instantevent-settings");
		
		String action = request.getParameter("action");
		if (action!=null && !action.equals("")){
		if (action.equalsIgnoreCase("updateSHsetting")) {
			Property shExpiryProperty = DAORegistry.getPropertyDAO().get("sh.expiry");
			shExpiryProperty.setValue(request.getParameter("shExpiry"));
			DAORegistry.getPropertyDAO().update(shExpiryProperty);

			Property shMarkupProperty = DAORegistry.getPropertyDAO().get("sh.markup.percent");
			shMarkupProperty.setValue(request.getParameter("shMarkup"));
			DAORegistry.getPropertyDAO().update(shMarkupProperty);

			Property shSalesProperty = DAORegistry.getPropertyDAO().get("sh.sales");
			shSalesProperty.setValue(request.getParameter("shSales"));
			DAORegistry.getPropertyDAO().update(shSalesProperty);

			Property shShippingProperty = DAORegistry.getPropertyDAO().get("sh.shipping");
			shShippingProperty.setValue(request.getParameter("shShipping"));
			DAORegistry.getPropertyDAO().update(shShippingProperty);

			Property shMinThresholdProperty = DAORegistry.getPropertyDAO().get("sh.minthreshold");
			shMinThresholdProperty.setValue(request.getParameter("shminTh"));
			DAORegistry.getPropertyDAO().update(shMinThresholdProperty);

			Property shMaxThresholdProperty = DAORegistry.getPropertyDAO().get("sh.maxthreshold");
			shMaxThresholdProperty.setValue(request.getParameter("shmaxTh"));
			DAORegistry.getPropertyDAO().update(shMaxThresholdProperty);	
			
			InstantEventManager.updateInstantEventsandTours();
		}
			
			if (action.equalsIgnoreCase("updateTNsetting")) {
			Property tnExpiryProperty = DAORegistry.getPropertyDAO().get("tn.expiry");
			tnExpiryProperty.setValue(request.getParameter("tnExpiry"));
			DAORegistry.getPropertyDAO().update(tnExpiryProperty);

			Property tnMarkupProperty = DAORegistry.getPropertyDAO().get("tn.markup.percent");
			tnMarkupProperty.setValue(request.getParameter("tnMarkup"));
			DAORegistry.getPropertyDAO().update(tnMarkupProperty);

			Property tnSalesProperty = DAORegistry.getPropertyDAO().get("tn.sales");
			tnSalesProperty.setValue(request.getParameter("tnSales"));
			DAORegistry.getPropertyDAO().update(tnSalesProperty);

			Property tnShippingProperty = DAORegistry.getPropertyDAO().get("tn.shipping");
			tnShippingProperty.setValue(request.getParameter("tnShipping"));
			DAORegistry.getPropertyDAO().update(tnShippingProperty);

			Property tnMinThresholdProperty = DAORegistry.getPropertyDAO().get("tn.minthreshold");
			tnMinThresholdProperty.setValue(request.getParameter("tnminTh"));
			DAORegistry.getPropertyDAO().update(tnMinThresholdProperty);

			Property tnMaxThresholdProperty = DAORegistry.getPropertyDAO().get("tn.maxthreshold");
			tnMaxThresholdProperty.setValue(request.getParameter("tnmaxTh"));
			DAORegistry.getPropertyDAO().update(tnMaxThresholdProperty);
			
			TNEventManager.updateInstantEventsandTours();
			
			}
			
			if (action.equalsIgnoreCase("updateEIsetting")) {
			Property eiExpiryProperty = DAORegistry.getPropertyDAO().get("ei.expiry");
			eiExpiryProperty.setValue(request.getParameter("eiExpiry"));
			DAORegistry.getPropertyDAO().update(eiExpiryProperty);

			Property eiMarkupProperty = DAORegistry.getPropertyDAO().get("ei.markup.percent");
			eiMarkupProperty.setValue(request.getParameter("eiMarkup"));
			DAORegistry.getPropertyDAO().update(eiMarkupProperty);

			Property eiSalesProperty = DAORegistry.getPropertyDAO().get("ei.sales");
			eiSalesProperty.setValue(request.getParameter("eiSales"));
			DAORegistry.getPropertyDAO().update(eiSalesProperty);

			Property eiShippingProperty = DAORegistry.getPropertyDAO().get("ei.shipping");
			eiShippingProperty.setValue(request.getParameter("eiShipping"));
			DAORegistry.getPropertyDAO().update(eiShippingProperty);

			Property eiMinThresholdProperty = DAORegistry.getPropertyDAO().get("ei.minthreshold");
			eiMinThresholdProperty.setValue(request.getParameter("eiminTh"));
			DAORegistry.getPropertyDAO().update(eiMinThresholdProperty);

			Property eiMaxThresholdProperty = DAORegistry.getPropertyDAO().get("ei.maxthreshold");
			eiMaxThresholdProperty.setValue(request.getParameter("eimaxTh"));
			DAORegistry.getPropertyDAO().update(eiMaxThresholdProperty);
			
			EIEventManager.updateInstantEventsandTours();
			}
			return new ModelAndView(new RedirectView("AdminEditInstantEventSettings?info=The instant event settings have been saved"));
		}			
		 else {
			Property shExpiryProperty = DAORegistry.getPropertyDAO().get("sh.expiry");
			Property shMarkupProperty = DAORegistry.getPropertyDAO().get("sh.markup.percent");
			Property shSalesProperty = DAORegistry.getPropertyDAO().get("sh.sales");
			Property shShippingProperty = DAORegistry.getPropertyDAO().get("sh.shipping");
			Property shMinThresholdProperty = DAORegistry.getPropertyDAO().get("sh.minthreshold");
			Property shMaxThresholdProperty = DAORegistry.getPropertyDAO().get("sh.maxthreshold");
			
			Property tnExpiryProperty = DAORegistry.getPropertyDAO().get("tn.expiry");
			Property tnMarkupProperty = DAORegistry.getPropertyDAO().get("tn.markup.percent");
			Property tnSalesProperty = DAORegistry.getPropertyDAO().get("tn.sales");
			Property tnShippingProperty = DAORegistry.getPropertyDAO().get("tn.shipping");
			Property tnMinThresholdProperty = DAORegistry.getPropertyDAO().get("tn.minthreshold");
			Property tnMaxThresholdProperty = DAORegistry.getPropertyDAO().get("tn.maxthreshold");
			
			Property eiExpiryProperty = DAORegistry.getPropertyDAO().get("ei.expiry");
			Property eiMarkupProperty = DAORegistry.getPropertyDAO().get("ei.markup.percent");
			Property eiSalesProperty = DAORegistry.getPropertyDAO().get("ei.sales");
			Property eiShippingProperty = DAORegistry.getPropertyDAO().get("ei.shipping");
			Property eiMinThresholdProperty = DAORegistry.getPropertyDAO().get("ei.minthreshold");
			Property eiMaxThresholdProperty = DAORegistry.getPropertyDAO().get("ei.maxthreshold");
			
			
			mav.addObject("shExpiry", shExpiryProperty.getValue());
			mav.addObject("shMarkup", shMarkupProperty.getValue());
			mav.addObject("shSales", shSalesProperty.getValue());
			mav.addObject("shShipping", shShippingProperty.getValue());
			mav.addObject("shminTh", shMinThresholdProperty.getValue());
			mav.addObject("shmaxTh", shMaxThresholdProperty.getValue());
			
			mav.addObject("tnExpiry", tnExpiryProperty.getValue());
			mav.addObject("tnMarkup", tnMarkupProperty.getValue());
			mav.addObject("tnSales", tnSalesProperty.getValue());
			mav.addObject("tnShipping", tnShippingProperty.getValue());
			mav.addObject("tnminTh", tnMinThresholdProperty.getValue());
			mav.addObject("tnmaxTh", tnMaxThresholdProperty.getValue());
			
			mav.addObject("eiExpiry", eiExpiryProperty.getValue());
			mav.addObject("eiMarkup", eiMarkupProperty.getValue());
			mav.addObject("eiSales", eiSalesProperty.getValue());
			mav.addObject("eiShipping", eiShippingProperty.getValue());
			mav.addObject("eiminTh", eiMinThresholdProperty.getValue());
			mav.addObject("eimaxTh", eiMaxThresholdProperty.getValue());
			
			return mav;
		}
	}*/
	
	
	/**
	 * Display page for TicketNetwork Mercury Tickets
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public ModelAndView loadMercuryTicketsCsvPage(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-mercury-tickets");
		
		String action = request.getParameter("action");
		if(action == null){
			Property property = DAORegistry.getPropertyDAO().get("mercuryTickets.eventIds");
	
			if (property != null) {
				String eventIds = property.getValue();
				mav.addObject("eventIds", eventIds);
			}
			return mav;
		}
		
			if (action.equals("update"))
			{
				String eventIds= request.getParameter("eventIds");
				eventIds = eventIds.replaceAll("\\s+", "").trim();

				String formattedEventIds = "";
				String error = "";
				// remove duplicate eventIds
				Set<Integer> distinctEventIds = new HashSet<Integer>();
				for (String eventIdStr: eventIds.split(",")) {
					try {
						Integer eventId = Integer.valueOf(eventIdStr.trim());
						distinctEventIds.add(eventId);
					} catch(Exception e) {
						error += "Event ID " + eventIdStr + " is invalid.\n";
					}	
				}
				// check if eventId is valid
				for(Integer distinctEventId:distinctEventIds ){
					Event event = DAORegistry.getEventDAO().get(distinctEventId);
					if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
						if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
						formattedEventIds += distinctEventId + ",";
					}
				}
				}
				if (formattedEventIds.length() >= 8000) {
					error += "Too many event Ids (the string should be less than 8,000 characters)\n";
				}
				
				if (!error.isEmpty()) {
					mav.addObject("error", error);				
					mav.addObject("eventIds", eventIds);	
				}
				
				if (!formattedEventIds.isEmpty()) {
					formattedEventIds = formattedEventIds.substring(0, formattedEventIds.length() - 1);
				}

				Property property = new Property("mercuryTickets.eventIds", formattedEventIds);
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
				mav.addObject("InfoMessage", "EventIds have been updated.");
			}
			return new ModelAndView(new RedirectView("MercuryTickets?info=EventIds have been updated"));
	}*/
	
	/**
	 * Download TicketNetwork Mercury Tickets as csv file
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView downloadMercuryTicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MercuryTicketsUtil.downloadMercuryTicketCsvFile(response);
		return null;
	}
	
	/*public ModelAndView downloadCustomizedMercuryTicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MercuryTicketsUtil.downloadCustomizedMercuryTicketCsvFile(response);
		return null;
	}*/
	/**
	 * Display total no of hits by site id
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	
	public ModelAndView loadCrawlerSiteCounter(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String siteId = request.getParameter("siteId");
		String strStartDate=request.getParameter("startDate");
		String strEndDate=request.getParameter("endDate");
		String system=request.getParameter("system");
		Date startDate=null;
		Date endDate=null;
		DateFormat startDateFormat=new SimpleDateFormat("MM/dd/yyyy");
		DateFormat endDateFormat =new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		try {
			if(strStartDate == null){
				strStartDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			startDate= startDateFormat.parse(strStartDate);
			if(strEndDate == null){
				strEndDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			endDate= endDateFormat.parse(strEndDate + " 23:59:59");
		}catch (ParseException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		ModelAndView mav = new ModelAndView("page-view-crawler-site");
		List<CrawlerHistoryManagement>list=DAORegistry.getQueryManagerDAO().getCrawlersBySiteId(siteId,system,startDate,endDate);
		mav.addObject("siteId", siteId);
		mav.addObject("system", system);
		mav.addObject("startDate", strStartDate);
		mav.addObject("endDate", strEndDate);
		mav.addObject("crawls", list);
		return mav;
	}
	/**
	 * Get All Crawler History
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadCrawlerHistory(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-crawler-counter");
		
		Integer crawlId=Integer.parseInt(request.getParameter("crawlId"));
		Date startDate=null;
		Date endDate=null;
		String strStartDate=null;
		String strEndDate=null;
		DateFormat startDateFormat=new SimpleDateFormat("MM/dd/yyyy");
		DateFormat endDateFormat =new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		String system=null;
		try {
			strStartDate=request.getParameter("startDate");
			strEndDate=request.getParameter("endDate");
			system=request.getParameter("system");
			if(strStartDate == null){
				strStartDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			startDate= startDateFormat.parse(strStartDate);
			if(strEndDate == null){
				strEndDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			endDate= endDateFormat.parse(strEndDate + " 23:59:59");
		}catch (ParseException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		TicketListingCrawl crawl= DAORegistry.getQueryManagerDAO().getCrawlSystemIndependently(crawlId, system);
		mav.addObject("crawl", crawl);
		mav.addObject("startDate", strStartDate);
		mav.addObject("endDate", strEndDate);
		mav.addObject("system", system);
		mav.addObject("hits", DAORegistry.getCrawlerHistoryManagementDAO().getCrawlerHistory(crawlId, startDate, endDate,system));
		return mav;
	}
	
	/**
	 * Display page for EI OE/TM_AM Tickets
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEITicketsCsvPage(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-ei-tickets");
		
		String action = request.getParameter("action");
		if(action == null){
			Property property = DAORegistry.getPropertyDAO().get("eiTickets.eventIds");
	
			if (property != null) {
				String eventIds = property.getValue();
				mav.addObject("eventIds", eventIds);
			}
			return mav;
		}
		
			if (action.equals("update"))
			{
				String eventIds= request.getParameter("eventIds");
				eventIds = eventIds.replaceAll("\\s+", "").trim();

				String formattedEventIds = "";
				String error = "";
				// remove duplicate eventIds
				Set<Integer> distinctEventIds = new HashSet<Integer>();
				for (String eventIdStr: eventIds.split(",")) {
					try {
						Integer eventId = Integer.valueOf(eventIdStr.trim());
						distinctEventIds.add(eventId);
					} catch(Exception e) {
						error += "Event ID " + eventIdStr + " is invalid.\n";
					}	
				}
				// check if eventId is valid
				for(Integer distinctEventId:distinctEventIds ){
					Event event = DAORegistry.getEventDAO().get(distinctEventId);
					if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
						if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
						formattedEventIds += distinctEventId + ",";
					}
				}
				}
				if (formattedEventIds.length() >= 8000) {
					error += "Too many event Ids (the string should be less than 8,000 characters)\n";
				}
				
				if (!error.isEmpty()) {
					mav.addObject("error", error);				
					mav.addObject("eventIds", eventIds);	
				}
				
				if (!formattedEventIds.isEmpty()) {
					formattedEventIds = formattedEventIds.substring(0, formattedEventIds.length() - 1);
				}

				Property property = new Property("eiTickets.eventIds", formattedEventIds);
				DAORegistry.getPropertyDAO().saveOrUpdate(property);
				mav.addObject("InfoMessage", "EventIds have been updated.");
			}
			return new ModelAndView(new RedirectView("EITickets?info=EventIds have been updated"));
	}
	
	/**
	 * Download EI Tickets as csv file
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView downloadEITicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EITicketsUtil.downloadEITicketCsvFile(response);
		return null;
	}
	
	public ModelAndView downloadCustomizedEITicketCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EITicketsUtil.downloadCustomizedEITicketCsvFile(response);
		return null;
	}
	/*
	 * ---------------------------------------------------------------------
	 * MM Event Map CSV
	 * ---------------------------------------------------------------------
	 */



	public ModelAndView loadAdminUploadMMEventCsvFilePage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String redirectString = "AdminManageUsers";
		Integer eventId = null;
		StringBuffer errorBuffer = new StringBuffer();
		
		if (request instanceof MultipartHttpServletRequest) {
			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			
			DateFormat formatDate = new SimpleDateFormat("M/d/y");
			DateFormat format12Time = new SimpleDateFormat("hh:mm aa");
			DateFormat format24Time = new SimpleDateFormat("HH:mm");
			
			BufferedReader reader;
		
			reader = new BufferedReader(new InputStreamReader(
					multi.getFile("file").getInputStream()));

			//skip the first line
			String line = reader.readLine();
			int lineCount = 0;

			while ((line = reader.readLine()) != null) {
				boolean lineError = false;
				try {
					lineCount++;
					StringTokenizer tokenizer = new StringTokenizer(line);
					//String eId = tokenizer.nextToken(",").trim();
					String userName = tokenizer.nextToken(",").trim();
					String eventName = tokenizer.nextToken(",").trim();
					String eventDate = tokenizer.nextToken(",").trim();
					String eventTime = tokenizer.nextToken(",").trim();
					String eventVenue = tokenizer.nextToken(",").trim();
					String userCoverage = tokenizer.nextToken(",").trim();
					String userStart = tokenizer.nextToken(",").trim();
					String userEnd = tokenizer.nextToken().trim();

					if (userName.equals("")) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid userName. The user name cannot empty." + "<br/>");
					}

					if (eventName.equals("") && !lineError) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid eventName. The event name cannot empty." + "<br/>");
					}

					if (eventDate.equals("") && !lineError) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid eventDate. The event date cannot empty." + "<br/>");
					}

					if (eventDate.equals("") && !lineError) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid eventVenue. The event venue cannot empty." + "<br/>");
					}

					User user = DAORegistry.getUserDAO().getUserByUsername(userName);
					if (user == null) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid userName. The user[" + userName + "] could not be found." + "<br/>");
					}
					
					Collection<Venue> venue = DAORegistry.getVenueDAO().getVenues(eventVenue);
					Integer venueId = null;
					if(venue == null && !lineError){
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid eventVenue. The event venue[" + eventVenue + "] could not be found." + "<br/>");
					} else if(!lineError) {
						if(venue.size() == 1){
							venueId = venue.iterator().next().getId();
						} else {
							lineError = true;
							errorBuffer.append("Invalid line: " + lineCount
									+ ": " + line
									+ ": Invalid eventVenue. Found multiple matches for the venue[" + eventVenue + "]." + "<br/>");
						}
					}
					Date eDate = null;
					Time eTime = null;
					try {
						eDate = formatDate.parse(eventDate);
						if (eventTime.equalsIgnoreCase("tbd")) {
							eTime = null;
						} else if (eventTime.toLowerCase().charAt(eventTime.length() - 1) == 'm') {
							eTime = new Time(format12Time.parse(eventTime).getTime());
						} else {
							eTime = new Time(format24Time.parse(eventTime).getTime());
						}
					} catch (ParseException e) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid eventDate. The event date or Time[" + eventDate + " " + eventTime + "] must be in the form MM/dd/yyyy hh:mm aa or HH:mm." + "<br/>");
					}
					
					Event event = null;
					
					if(!lineError){
						Collection<Event> events = DAORegistry.getEventDAO().getEvent(eventName, venueId, eDate, eTime);
						if(events != null){
							if(events.size() > 1){
								lineError = true;
								errorBuffer.append("Invalid line: " + lineCount
										+ ": " + line
										+ ": Invalid event. The event for the name "
										+ eventName + " Venue: " + eventVenue + " Date: " 
										+ eventDate + " Time: " + eventTime 
										+ " matched multiple events." + "<br/>");
							} else {
								event = events.iterator().next();
							}
						} else {
							lineError = true;
							errorBuffer.append("Invalid line: " + lineCount
									+ ": " + line
									+ ": Invalid event. The event for the name "
									+ eventName + " Venue: " + eventVenue + " Date: " 
									+ eventDate + " Time: " + eventTime 
									+ " cannot be found." + "<br/>");
						}
					}

					Date startDate = null;
					Date endDate = null;
					try {
						startDate = formatDate.parse(userStart);
						endDate = formatDate.parse(userEnd);
					} catch (ParseException e) {
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount
								+ ": " + line
								+ ": Invalid start/end Date. The event dates [" + userStart + " " + userEnd + "] must be in the form MM/dd/yyyy." + "<br/>");
					}
					if(!lineError) {
						MMEventMap eventMap = DAORegistry.getMMEventMapDAO().getMap(userName, event.getId());
						if(eventMap != null){
							eventMap.setCoverage(Double.parseDouble(userCoverage));
							eventMap.setStartDate(startDate);
							eventMap.setEndDate(endDate);
							
							DAORegistry.getMMEventMapDAO().update(eventMap);
						} else {
							eventMap = new MMEventMap();
							eventMap.setUsername(user.getUsername());
							eventMap.setEventId(event.getId());
							eventMap.setCoverage(Double.parseDouble(userCoverage));
							eventMap.setStartDate(startDate);
							eventMap.setEndDate(endDate);
							
							DAORegistry.getMMEventMapDAO().save(eventMap);
						}
					}

				} catch (Exception e) {
					if(!lineError) {
						e.printStackTrace();
						lineError = true;
						errorBuffer.append("Invalid line: " + lineCount + ": "
								+ line + " - Exception: " + e.getMessage() + "<br/>");
					}
				}
			}
		}
		
		if((eventId == null) && (errorBuffer.length() == 0)) {
			ModelAndView mav = new ModelAndView(
					"page-admin-manage-users");
			mav.addObject("error", "Invalid File" + "<br/>");
			return mav;
		}
		
		ModelAndView mav = null;
		
		if(errorBuffer.length() > 0){
			System.out.println("errorBuffer: " + errorBuffer);
			mav = new ModelAndView(
			"page-admin-manage-users");
			mav.addObject("error", errorBuffer);
		} else {
			redirectString = redirectString + eventId.intValue();
	
			mav = new ModelAndView(new RedirectView(redirectString,
					true));
		}
		return mav;
	}
	
	public ModelAndView compareEIMPToEIBOX(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
//		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		ModelAndView mav = new ModelAndView("page-admin-compare-eimp-eibox");
		String eventFilter = request.getParameter("eventFilter");
		List<EIBoxEvent> eiBoxEvents = null;
		if(eventFilter == null || eventFilter.length() <= 0){
			eiBoxEvents = (List<EIBoxEvent>) DAORegistry.getEiBoxEventDAO().getAll();
			mav.addObject("eiBoxEvents",eiBoxEvents);
		}else{
			eiBoxEvents = (List<EIBoxEvent>) DAORegistry.getEiBoxEventDAO().getEIBoxEventsByNameFilter(eventFilter);
			mav.addObject("eiBoxEvents",eiBoxEvents);
		}
		
		String eimpEventIdStr = request.getParameter("eimpEventId");
		String eiboxIdStr = request.getParameter("eiboxEventId");
		if(eimpEventIdStr != null && eimpEventIdStr.length() > 0 && eiboxIdStr != null && eiboxIdStr.length() > 0 ){
			eiboxIdStr = URLDecoder.decode(eiboxIdStr);
			
			Integer eimpEventId = null;
			Integer eiboxEventId = null;
			Integer eiboxVenueId = null;
			Date eiboxEventDate = null;
			try{
				eimpEventId = Integer.parseInt(eimpEventIdStr);
				//eiboxEventId = Integer.parseInt(eiboxEventIdStr);
				String [] temp = eiboxIdStr.split(";");
				for(int i = 0; i<=2; i++){
					System.out.println("Value = " + temp[i]);
				}
				String eiboxEventIdStr = temp[0];
				String eiboxVenueIdStr = temp[1];
				String eiboxDateStr = temp[2];
				eiboxEventId = Integer.parseInt(eiboxEventIdStr);
				eiboxVenueId = Integer.parseInt(eiboxVenueIdStr);
				//2010-09-04 13:05:00.0
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				eiboxEventDate = df.parse(eiboxDateStr);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(eimpEventId != null && eimpEventId > 0 && eiboxEventId != null && eiboxEventId > 0){
				//https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=907594
				//https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=907601
//				String eimpUrl = "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p="+eimpEventId;
				System.out.println("before invoking the comparator ");
				FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = EimpAndEiboxComparator.getInstance().compare(eimpEventId, eiboxEventId, eiboxVenueId, eiboxEventDate);
				mav.addObject("feedAndCrawlComparator",feedAndCrawlComparatorVO);
			}
		}
		return mav;
	}

	public ModelAndView compareEIMPAndEIBOX(HttpServletRequest request,
			HttpServletResponse response){
		String eventString=request.getParameter("eventId");
		List<TicketListingCrawl>list= DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(Integer.parseInt(eventString));
		ModelAndView mav = new ModelAndView("page-admin-compare-eimp-eibox");
		Event event=null;
		Collection<Ticket> eimpTickets=null;
		Collection<Ticket> eiboxTickets=null;
		for(TicketListingCrawl crawl:list){
			
			if(crawl.getSiteId().equals(Site.EIBOX)){
				event=crawl.getEvent();
				eimpTickets=DAORegistry.getTicketDAO().getAllActiveTicketsByEventAndSiteId(crawl.getEventId(), crawl.getSiteId());
				continue;
			}
			if(crawl.getSiteId().equals(Site.EI_MARKETPLACE)){
				event=crawl.getEvent();
				eiboxTickets=DAORegistry.getTicketDAO().getAllActiveTicketsByEventAndSiteId(crawl.getEventId(), crawl.getSiteId());
				continue;
			}
		}
		Map<String, Ticket> eimpMap = new HashMap<String, Ticket>();
		Map<String, Ticket> eiboxMap = new HashMap<String, Ticket>();
		String key=null;
		if(eimpTickets != null){
			for(Ticket eimpTicket:eimpTickets){
				key=eimpTicket.getSection()+"-"+eimpTicket.getRow()+"-"+eimpTicket.getQuantity()+"-"+eimpTicket.getCurrentPrice();
				eimpMap.put(key, eimpTicket);
			}
		}
		eimpTickets=null;
		if(eiboxTickets != null){
			for(Ticket eiboxTicket:eiboxTickets){
				key=eiboxTicket.getSection()+"-"+eiboxTicket.getRow()+"-"+eiboxTicket.getQuantity()+"-"+eiboxTicket.getCurrentPrice();
				eiboxMap.put(key, eiboxTicket);
			}
		}
		eiboxTickets=null;
		Set<String>keySet=eimpMap.keySet();
		List<EIMPTicketComparator> tc= new ArrayList<EIMPTicketComparator>();
		Ticket eimpTicket=null;
		Ticket eiboxTicket=null;
		int eimpMissingTicketCount=0;
		int eiboxMissingTicketCount=0;
		for(String tempKey:keySet){
			eimpTicket=eimpMap.get(tempKey);
			eiboxTicket=eiboxMap.remove(tempKey);
			if(eiboxTicket==null){
				EIMPTicketComparator etc=new EIMPTicketComparator();
				etc.setEIBOXmissing(true);
				etc.setPrice(eimpTicket.getCurrentPrice());
				etc.setSection(eimpTicket.getSection());
				etc.setRow(eimpTicket.getRow());
				etc.setSeat(eimpTicket.getSeat());
				etc.setQuantity(eimpTicket.getQuantity());
				tc.add(etc);
				eiboxMissingTicketCount++;
			}
			
		}
		keySet=eiboxMap.keySet();
		for(String tempKey:keySet){
			eiboxTicket=eiboxMap.get(tempKey);
			EIMPTicketComparator etc=new EIMPTicketComparator();
			etc.setEIMPmissing(true);
			etc.setPrice(eiboxTicket.getCurrentPrice());
			etc.setSection(eiboxTicket.getSection());
			etc.setRow(eiboxTicket.getRow());
			etc.setSeat(eiboxTicket.getSeat());
			etc.setQuantity(eiboxTicket.getQuantity());
			tc.add(etc);
			eimpMissingTicketCount++;
		}
		eiboxMap.clear();
		eiboxMap=null;
		
		eimpMap.clear();
		eimpMap=null;
		mav.addObject("tickets", tc);
		mav.addObject("eiboxMissingTicketCount", eiboxMissingTicketCount);
		mav.addObject("eimpMissingTicketCount", eimpMissingTicketCount);
		mav.addObject("eventFilter", event);
		return mav;
	}
	/**
	 * Autocomplete events for compare function of EIMP and EIBOX tickets.
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void loadAutoCompleteEvents(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		String strEvent=request.getParameter("q");
		
		if (TextUtil.removeExtraWhitespaces(strEvent).isEmpty()) {
			return ;
		}
		Collection<Event> events= DAORegistry.getEventDAO().getAllEventsByNameExcerpt(strEvent,EventStatus.ACTIVE);
		if(events== null){
			return;
		}
		for(Event event: events){
			response.getOutputStream().println(event.getName()+"|" +event.getLocalDateTime().getTime() +"|" + event.getId());
		}
	}
	/*public ModelAndView compareFeedToCrawl(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
//		String action = request.getParameter("action");
		List<Event> events = null;
		String eventIdString = request.getParameter("eventId");
		String tourIdString = request.getParameter("tourId");
		String sourceIdString = request.getParameter("sourceId");
		
		Integer editorCompareTourId = 0;
		Integer editorCompareEventId = 0;
		
		
		ModelAndView mav = new ModelAndView("page-admin-compare-feed-crawl");
		List<Artist> artists = (List<Artist>) DAORegistry.getTourDAO().getAllActiveTours();
		
		Collections.sort(artists, new Comparator<Artist>(){
			public int compare(Artist t1,	Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		}
		);
		
		int tourId = 0;
		if (tourIdString != null && tourIdString.length() > 0) {
			tourId = Integer.parseInt(tourIdString);
			events = (List<Event>)DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE);
			if(events != null && !(events.isEmpty())){
				Collections.sort(events, new Comparator<Event>(){
					public int compare(Event e1,	Event e2) {
						AlphanumericSorting ans = new AlphanumericSorting();
						return ans.compare(e1.getName(), e2.getName());
					}
				}
				);
			}
			editorCompareTourId = tourId;
		}
		
		if (tourId != 0 && DAORegistry.getTourDAO().get(tourId) == null) {
			tourId = 0;
		}

		int eventId  = 0;
		if(eventIdString != null && eventIdString.length() > 0){
			eventId = Integer.parseInt(eventIdString);
//			Event event = DAORegistry.getEventDAO().get(eventId);
			int sourceId = 0;
			if(sourceIdString != null && sourceIdString.length() > 0){
				sourceId = Integer.parseInt(sourceIdString);
				List<TicketListingCrawl> tixCrawlList = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(eventId);
				if(tixCrawlList != null && !tixCrawlList.isEmpty()){
					TicketListingCrawl tnTixCrawl = null;
					TicketListingCrawl shTixCrawl = null;
//					TicketListingCrawl eiTixCrawl = null;
					//TicketListingCrawl eiBoxCrawl = null;
					for(TicketListingCrawl tixCrawl : tixCrawlList){
						if((tixCrawl.getSiteId().equals(Site.TICKET_NETWORK) || tixCrawl.getSiteId().equals(Site.TICKET_NETWORK_DIRECT)) && sourceId == 2){
							tnTixCrawl = tixCrawl;
							break;
						}else if(tixCrawl.getSiteId().equals(Site.STUB_HUB) && sourceId == 1){
							shTixCrawl = tixCrawl;
							break;
						}
					}
					//If do stubhub specific data fetching
					if(sourceId == 1){
						//http://www.stubhub.com/?event_id=898431
						if(shTixCrawl != null){
							String shCrawlerEventUrl = shTixCrawl.getQueryUrl();
							Pattern shEventIdPattern = null;
							Matcher shMatcher = null;
							shEventIdPattern = Pattern.compile("event_id=(\\d+)");
							shMatcher = shEventIdPattern.matcher(shCrawlerEventUrl);
							
							shMatcher.find();
							String stubHubEventIdString = shMatcher.group(1);
							
							System.out.println("Stubhub Event Id String: " + stubHubEventIdString);
							Integer shEventId = Integer.parseInt(stubHubEventIdString);
							System.out.println("Before callling the compare function, the ZeroMarkupEventId is: " + shEventId);
							FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = SHWebAndWSTicketListingComparator.getInstance().compare(shEventId, shCrawlerEventUrl);
							mav.addObject("feedAndCrawlComparator",feedAndCrawlComparatorVO);
						}
					}else if(sourceId == 2){
						//do ticket network specific data fetching
						if(tnTixCrawl != null){
							String tnCrawlerEventUrl = tnTixCrawl.getQueryUrl();
							Pattern thEventIdPattern = null;
							Matcher tnMatcher = null;
							if(tnTixCrawl.getSiteId().equals(Site.TICKET_NETWORK)){
								thEventIdPattern = Pattern.compile("evtid=(\\d+)&");
								tnMatcher = thEventIdPattern.matcher(tnCrawlerEventUrl);
							}else{
								//http://www.ticketnetwork.com/tix/tix-1309720.aspx
								thEventIdPattern = Pattern.compile("tix-(\\d+).");
								tnMatcher = thEventIdPattern.matcher(tnCrawlerEventUrl);
							}
							tnMatcher.find();
							String zeroMarkupEventIdString = tnMatcher.group(1);
							
							System.out.println("Zero Markup Event Id String: " + zeroMarkupEventIdString);
							Integer zeroMarkupEventId = Integer.parseInt(zeroMarkupEventIdString);
							System.out.println("Before callling the compare function, the ZeroMarkupEventId is: " + zeroMarkupEventId);
							FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = TNWebAndWSTicketListingComparator.getInstance().compare(zeroMarkupEventId);
							mav.addObject("feedAndCrawlComparator",feedAndCrawlComparatorVO);
						}
					}

				}
			}
			editorCompareEventId = eventId;
		}
		editorCompareTourId = tourId;
		
		
		mav.addObject("events", events);
		mav.addObject("artists", artists);
		mav.addObject("editorCompareEventId",editorCompareEventId);
		mav.addObject("editorCompareTourId",editorCompareTourId);
	
		return mav;		
	}*/
	
	public ModelAndView loadPriceManagementPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-manage-price");
		String[] parentCategoryParam = request.getParameterValues("parentSelect");
		String[] childCategoryParam = request.getParameterValues("childSelect");
		String[] grandChildCategoryParam = request.getParameterValues("grandChildSelect");
		String[] artistParam = request.getParameterValues("artistSelect");
		String[] eventParam = request.getParameterValues("eventSelect");
		String[] exchangeParam = request.getParameterValues("exchangeName");
		String[] ticketTypeParam = request.getParameterValues("ticketTypeName");
		String action = request.getParameter("action");
		String isAllParent = request.getParameter("isAllParent");
		String isAllChild = request.getParameter("isAllChild");
		String isAllGrandChild = request.getParameter("isAllGrandChild");
		String isAllArtist = request.getParameter("isAllArtist");
		String isAllEvent = request.getParameter("isAllEvent");
//		String isAllExchange = request.getParameter("isAllExchange");
//		String isAllTicketType = request.getParameter("isAllTicketType");
//		if(exchangeParam == null && isAllExchange == null){
//			isAllExchange="on";
//		}
//		if(ticketTypeParam == null && isAllTicketType == null){
//			isAllTicketType="on";
//		}
		List<TourCategory> parentList = DAORegistry.getTourCategoryDAO().getAllTourCategoryOrderByName();
		Collection<Site> siteList = DAORegistry.getSiteDAO().getAll();	
		List<String> exchangeList = new ArrayList<String>();
		StringBuffer siteStringBuffer=new StringBuffer();
		
		for(Site site:siteList){
			exchangeList.add(site.getId());
			siteStringBuffer.append("\""+site.getId()+"\":\""+site.getId()+"\",");
		}
		String siteString = siteStringBuffer.substring(0, siteStringBuffer.length()-1);
		StringBuffer ticketTypeStringBuffer=new StringBuffer();
		List<String> ticketTypes = new ArrayList<String>();
		ticketTypes.add("REGULAR");		
		ticketTypeStringBuffer.append("\"REGULAR\":\"REGULAR\",");
		for(TicketDeliveryType ticketType:TicketDeliveryType.values()){
			ticketTypes.add(ticketType.toString());
			ticketTypeStringBuffer.append("\""+ticketType.toString()+"\":\""+ticketType.toString()+"\",");
		}
		String ticketTypeString = ticketTypeStringBuffer.substring(0, ticketTypeStringBuffer.length()-1);
		if(action!=null && ("save".equals(action) || "delete".equals(action))){
			if(parentCategoryParam!=null && !"".equals(parentCategoryParam)){			
				int id=0;
				List<Integer> ids  = new ArrayList<Integer>();
				
				for(String str:parentCategoryParam){
					id=Integer.parseInt(str);
					ids.add(id);
				}
				Collection<ChildTourCategory> childList= DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategories(ids);
				mav.addObject("childList", childList);
				mav.addObject("childSelected", childCategoryParam);
			}
			if(childCategoryParam!=null && !"".equals(childCategoryParam)){			
				int id=0;
				List<Integer> ids  = new ArrayList<Integer>();
				
				for(String str:childCategoryParam){
					id=Integer.parseInt(str);
					ids.add(id);
				}
				Collection<GrandChildTourCategory> grandChildList= DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategorys(ids);
				mav.addObject("grandChildList", grandChildList);
				mav.addObject("grandChildSelected", grandChildCategoryParam);
			}
			if(grandChildCategoryParam!=null && !"".equals(grandChildCategoryParam)){
				int id=0;
				List<Integer> ids  = new ArrayList<Integer>();
				
				for(String str:grandChildCategoryParam){
					id=Integer.parseInt(str);
					ids.add(id);
				}
				List<Integer> artistIdList = new ArrayList<Integer>();
				for(String artistId:artistParam){
					artistIdList.add(Integer.parseInt(artistId));
				}
				
				Collection<Artist> list= DAORegistry.getArtistDAO().getAllActiveArtistsByGrandChildCategorys(ids);
				mav.addObject("artistList", list);
				mav.addObject("artistSelected", artistParam);
				
				Collection<Event> eventList= DAORegistry.getEventDAO().getAllActiveEventsByArtistIds(artistIdList);
				mav.addObject("eventList", eventList);
				mav.addObject("eventSelected", eventParam);
				
				
//				Collection<Artist> tourList= DAORegistry.getTourDAO().getTourByIds(tourIdList);
				Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
				for(Event event:eventList){
					eventMap.put(event.getId(), event);
				}
				List<ManagePurchasePrice> managePurchasePriceList = new ArrayList<ManagePurchasePrice>();
				String info="";
				try{
					if("save".equals(action)){
						for(Object param:request.getParameterMap().keySet()){
							String parameter=(String)param;
							if(!parameter.contains("_select_")){
								continue;
							}
							String temp[]=parameter.split("_");
							
	//						String isSelected = request.getParameter(artist.getId()+"_select");
	//						if(isSelected!=null && "on".equals(isSelected)){
							String eventId=temp[0];
							String exchange = temp[2];
							String ticketType = temp[3];
							Event event = eventMap.get(Integer.parseInt(eventId));//DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
							boolean exist = Boolean.parseBoolean(request.getParameter(eventId + "_" + exchange + "_" + ticketType + "_exist"));
							ManagePurchasePrice managePurchasePrice = new ManagePurchasePrice();
							if(exist){
								Integer manageTourPriceId = Integer.parseInt(request.getParameter(eventId + "_" + exchange + "_" + ticketType + "_id"));
								managePurchasePrice.setId(manageTourPriceId);
							}
	//							String exchange = request.getParameter(artist.getId()+"_exchange");
	//							String ticketType = request.getParameter(artist.getId()+"_tickettype");
							String serviceFee = request.getParameter(eventId + "_servicefee_" + exchange + "_" + ticketType);
							String currencyType = request.getParameter(eventId + "_currencytype_" + exchange + "_" + ticketType);
							String shipping = request.getParameter(eventId + "_shipping_" + exchange + "_" + ticketType);					
							managePurchasePrice.setExchange(exchange);
							managePurchasePrice.setTicketType(ticketType);
//							Artist artist =artistMap.get(Integer.parseInt(artistId));
							managePurchasePrice.setEvent(event);  // Need to fix before launch..
							if(serviceFee==null || "".equals(serviceFee)){
								serviceFee="0";	
							}
							managePurchasePrice.setServiceFee(Double.parseDouble(serviceFee));
							managePurchasePrice.setCurrencyType(Integer.parseInt(currencyType));
							if(shipping==null || "".equals(shipping)){
								shipping="0";	
							}
							managePurchasePrice.setShipping(Double.parseDouble(shipping));
							if(!"0".equals(exchange) && !"0".equals(ticketType))
								managePurchasePriceList.add(managePurchasePrice);
	//							System.out.println(exchange+"\t"+ticketType+"\t"+serviceFee+"\t"+shipping);
	//						}
						}
						if(!managePurchasePriceList.isEmpty()){
							DAORegistry.getManagePurchasePriceDAO().saveOrUpdateAll(managePurchasePriceList);
							mav.addObject("viewData", "true");
							mav.addObject("info","Updated successfully.");
						}
					}else if("delete".equalsIgnoreCase(action)){
						for(Object param:request.getParameterMap().keySet()){
							String parameter=(String)param;
							if(!parameter.contains("_select_")){
								continue;
							}
							String temp[]=parameter.split("_");
							
							String tourId=temp[0];
							String exchange = temp[2];
							String ticketType = temp[3];
							boolean exist = Boolean.parseBoolean(request.getParameter(tourId + "_" + exchange + "_" + ticketType + "_exist"));
							if(!exist){
								continue;
							}
							ManagePurchasePrice managePurchasePrice = new ManagePurchasePrice();
							Integer manageTourPriceId = Integer.parseInt(request.getParameter(tourId + "_" + exchange + "_" + ticketType + "_id"));
							managePurchasePrice.setId(manageTourPriceId);
							managePurchasePriceList.add(managePurchasePrice);
						}
						if(!managePurchasePriceList.isEmpty()){
							DAORegistry.getManagePurchasePriceDAO().deleteAll(managePurchasePriceList);
							mav.addObject("viewData", "true");
							mav.addObject(info,"Deleted successfully.");
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
					mav.addObject("error","Error occured while performing request.");
				}
				
				
			}
			
		}
		mav.addObject("parentList", parentList);
		mav.addObject("parentSelected", parentCategoryParam);
		mav.addObject("exchangeSelected", exchangeParam!=null?exchangeParam[0]:null);
		mav.addObject("ticketTypeSelected", ticketTypeParam!=null?ticketTypeParam[0]:ticketTypeParam);
		mav.addObject("siteList", "{"+siteString+"}");
		mav.addObject("ticketTypeList", "{"+ticketTypeString+"}");
//		mav.addObject("isAllTicketTypeSelected", isAllTicketType);
//		mav.addObject("isAllExchangeSelected", isAllExchange);
		mav.addObject("isAllArtistSelected", isAllArtist);
		mav.addObject("isAllGrandChildSelected", isAllGrandChild);
		mav.addObject("isAllChildSelected", isAllChild);
		mav.addObject("isAllParentSelected", isAllParent);
		mav.addObject("isAllEventSelected", isAllEvent);
		mav.addObject("exchangeList", exchangeList);
		mav.addObject("ticketTypes",ticketTypes);
		mav.addObject("isFirst", "true");
		
		return mav;
	}
	public void getCategory(HttpServletRequest request,	HttpServletResponse response)throws ServletException, IOException {
		String parentCategoryParam = request.getParameter("parent");
		String childCategoryParam = request.getParameter("child");
		String grandChildCategoryParam = request.getParameter("grandChild");
		String artistId = request.getParameter("artistId");
//		if((parentCategoryParam==null || parentCategoryParam.equals("")) && (childCategoryParam==null || childCategoryParam.equals("")) && (grandChildCategoryParam==null || grandChildCategoryParam.equals(""))){
//			
//		}
		if(parentCategoryParam!=null && !"".equals(parentCategoryParam)){
			int id=0;
			String[] tourCategories = parentCategoryParam.split(";");
			List<Integer> ids  = new ArrayList<Integer>();
			
			for(String str:tourCategories){
				id=Integer.parseInt(str);
				ids.add(id);
			}
			
			Collection<ChildTourCategory> list=null;
			if(id==0){
				list = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryOrderByName();
			}else{
				list= DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategories(ids);
			}
			JSONArray childTourCategoryList= new JSONArray();
			JSONObject obj = null;
			for(ChildTourCategory childTourCategory:list){
				obj = new JSONObject();
				try {
					obj.put("id",childTourCategory.getId());
					obj.put("name",childTourCategory.getName());
					childTourCategoryList.put(obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			IOUtils.write(childTourCategoryList.toString().getBytes(), response.getOutputStream());
		}else if(childCategoryParam!=null && !"".equals(childCategoryParam)){
			int id=0;
			String[] childCategories = childCategoryParam.split(";");
			List<Integer> ids  = new ArrayList<Integer>();
			
			for(String str:childCategories){
				id=Integer.parseInt(str);
				ids.add(id);
			}
			Collection<GrandChildTourCategory> list = null;
			if(id==0){
				list= DAORegistry.getGrandChildTourCategoryDAO().getAllGrandChildTourOrderByName();
			}else{
				list= DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategorys(ids);	
			}
			JSONArray grandChildTourCategoryList= new JSONArray();
			JSONObject obj = null;
			for(GrandChildTourCategory grandChildTourCategory:list){
				obj = new JSONObject();
				try {
					obj.put("id",grandChildTourCategory.getId());
					obj.put("name",grandChildTourCategory.getName()+"("+grandChildTourCategory.getChildTourCategory().getName()+")");
					grandChildTourCategoryList.put(obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			IOUtils.write(grandChildTourCategoryList.toString().getBytes(), response.getOutputStream());
		}else if(grandChildCategoryParam!=null && !"".equals(grandChildCategoryParam)){
			int id= 0;
			String[] grandChildCategories = grandChildCategoryParam.split(";");
			List<Integer> ids  = new ArrayList<Integer>();
			
			for(String str:grandChildCategories){
				id=Integer.parseInt(str);
				ids.add(id);
			}
			Collection<Artist> list = null;
			if(id==0){
				list = DAORegistry.getArtistDAO().getAllActiveArtists();
			}else{
				list = DAORegistry.getArtistDAO().getAllActiveArtistsByGrandChildCategorys(ids);
			}
			JSONArray grandChildTourCategoryList= new JSONArray();
			JSONObject obj = null;
			for(Artist artist:list){
				obj = new JSONObject();
				try {
					obj.put("id",artist.getId());
					obj.put("name",artist.getName());
					grandChildTourCategoryList.put(obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			IOUtils.write(grandChildTourCategoryList.toString().getBytes(), response.getOutputStream());
		}else if(artistId!=null && !"".equals(artistId)){
			int id= 0;
			String[] artistIds = artistId.split(";");
			List<Integer> ids  = new ArrayList<Integer>();
			
			for(String str:artistIds){
				id=Integer.parseInt(str);
				ids.add(id);
			}
			Collection<Event> list = null;
			if(id==0){
				list = DAORegistry.getEventDAO().getAllActiveEvents();
			}else{
				list = DAORegistry.getEventDAO().getAllActiveEventsByArtistIds(ids);
			}
			JSONArray eventList= new JSONArray();
			JSONObject obj = null;
			for(Event event:list){
				obj = new JSONObject();
				try {
					obj.put("id",event.getId());
					obj.put("name",event.getName() + "" + (event.getFormattedEventDate() +" @ " + event.getVenue().getBuilding() + "," +event.getVenue().getCity() + "," +event.getVenue().getState()));
					eventList.put(obj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			IOUtils.write(eventList.toString().getBytes(), response.getOutputStream());
		}
	}
	public void getChildTourCategory(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
				String stringId= request.getParameter("tourCategory");
				int id;
				if(stringId ==null){
					//return null;
				}
				id=Integer.parseInt(stringId);
				Collection<ChildTourCategory> list=null;
				if(id==0){
					list = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryOrderByName();
				}else{
					list= DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(id);
				}
				JSONArray childTourCategoryList= new JSONArray();
				JSONObject obj = null;
				for(ChildTourCategory childTourCategory:list){
					obj = new JSONObject();
					try {
						obj.put("id",childTourCategory.getId());
						obj.put("name",childTourCategory.getName());
						childTourCategoryList.put(obj);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				IOUtils.write(childTourCategoryList.toString().getBytes(), response.getOutputStream());
	}

	public void getGrandChildTourCategory(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
				String stringId= request.getParameter("childTourCategory");
				int id;
				if(stringId ==null){
					//return null;
				}
				id=Integer.parseInt(stringId);
				Collection<GrandChildTourCategory> list= DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(id);
				JSONArray grandChildTourCategoryList= new JSONArray();
				JSONObject obj = null;
				for(GrandChildTourCategory grandChildTourCategory:list){
					obj = new JSONObject();
					try {
						obj.put("id",grandChildTourCategory.getId());
						obj.put("name",grandChildTourCategory.getName());
						grandChildTourCategoryList.put(obj);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				IOUtils.write(grandChildTourCategoryList.toString().getBytes(), response.getOutputStream());
	}
	
	public ModelAndView loadDefaultPriceManagementPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		ModelAndView mav = new ModelAndView("page-default-manage-price");
		String action=request.getParameter("action");
		String info=null;
		String error=null;
		DefaultPurchasePrice defaultPurchasePrice =  null;
		boolean flag = false;
		if(action!=null){
			String exchange=request.getParameter("exchangeSelect");
			String ticketType=request.getParameter("ticketTypeSelect");
			int currencyType=Integer.parseInt(request.getParameter("currencyType"));
			Double serviceFees=Double.parseDouble(request.getParameter("serviceFees"));
			Double shipping=Double.parseDouble(request.getParameter("shipping"));
			
			if(action.equals("edit")){
				String idStr=request.getParameter("id");
				Integer id=null;
				if(idStr!=null){
					id=Integer.parseInt(idStr);
				}
				defaultPurchasePrice= new DefaultPurchasePrice();
				defaultPurchasePrice.setId(id);
				DefaultPurchasePrice defaultTourPriceDB =  DAORegistry.getDefaultPurchasePriceDAO().getDefaultTourPriceByExchangeAndTicketType(exchange, ticketType);
				if(defaultTourPriceDB!= null && !defaultTourPriceDB.getId().equals(defaultPurchasePrice.getId())){
					error="Default Artist price already exist.";
					defaultPurchasePrice=null;
//					flag=true;
				}
				
			}else if(action.equals("add") ){
				defaultPurchasePrice =  DAORegistry.getDefaultPurchasePriceDAO().getDefaultTourPriceByExchangeAndTicketType(exchange, ticketType);
				if(defaultPurchasePrice!=null){
					error="Default Artist price already exist.";
					defaultPurchasePrice=null;
//					flag=true;
				}else{
				defaultPurchasePrice= new DefaultPurchasePrice();
				/*defaultTourPrice.setExchange(exchange);
				defaultTourPrice.setTicketType(ticketType);
				defaultTourPrice.setCurrencyType(currencyType);
				defaultTourPrice.setServiceFees(serviceFees);
				defaultTourPrice.setShipping(shipping);
				DAORegistry.getDefaultPurchasePriceDAO().save(defaultTourPrice);*/
				}
			}
			if(defaultPurchasePrice!=null){
				defaultPurchasePrice.setExchange(exchange);
				defaultPurchasePrice.setTicketType(ticketType);
				defaultPurchasePrice.setCurrencyType(currencyType);
				defaultPurchasePrice.setServiceFees(serviceFees);
				defaultPurchasePrice.setShipping(shipping);
				DAORegistry.getDefaultPurchasePriceDAO().saveOrUpdate(defaultPurchasePrice);
				info="Default Artist price saved successfully.";
			}
		}else{
			String id = request.getParameter("edit");
			if(id!=null){
				defaultPurchasePrice =  DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPriceByTourId(Integer.parseInt(id));
				flag=true;
			}else{
				mav.addObject("id", id);
			}
		}
		if(flag){
			mav.addObject("exchange", defaultPurchasePrice.getExchange());
			mav.addObject("ticketType", defaultPurchasePrice.getTicketType());
			mav.addObject("currencyType", defaultPurchasePrice.getCurrencyType());
			mav.addObject("serviceFees", defaultPurchasePrice.getServiceFees());
			mav.addObject("shipping", defaultPurchasePrice.getShipping());
			mav.addObject("id", defaultPurchasePrice.getId());
		}
		List<DefaultPurchasePrice> exchangeTourPrices = DAORegistry.getDefaultPurchasePriceDAO().getAllDefaultTourPrice();
		Collection<Site> sites = DAORegistry.getSiteDAO().getAll();
		mav.addObject("sites", sites);
		mav.addObject("exchangeTourPrices", exchangeTourPrices);
		mav.addObject("info",info);
		mav.addObject("error",error );
		return mav;
	}
	
	public void getTicketType(HttpServletRequest request,HttpServletResponse response) throws Exception{
		String exchangeName = request.getParameter("exchangeName");
		String site = DAORegistry.getTicketDAO().getTicketDeliveryType(exchangeName);
		IOUtils.write(site.getBytes(), response.getOutputStream());
	}
	
	public void getPurchasePriceByEvent(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
		String eventIdStr= request.getParameter("eventId");
		String exchanges= request.getParameter("exchange");
		String ticketTypes= request.getParameter("tickettype");
		List<Integer> eventIds = new ArrayList<Integer>();
		List<String> exchangeIds = new ArrayList<String>();
		List<String> ticketTypeIds = new ArrayList<String>();
		List<Integer> newIds = new ArrayList<Integer>();
		for(String str:eventIdStr.split(";")){
			eventIds.add(Integer.parseInt(str));
		}
		for(String str:exchanges.split(";")){
			exchangeIds.add(str.trim());
		}
		for(String str:ticketTypes.split(";")){
			ticketTypeIds.add(str.trim());
		}
		newIds.addAll(eventIds);
//		List<Integer> exceptId = DAORegistry.getManagePurchasePriceDAO().getDistinctTourIds();
//		newIds.removeAll(exceptId);
//		ids.removeAll(newIds);
		JSONArray finalArray = new JSONArray();
		JSONArray artistList= new JSONArray();
		JSONObject obj = null;
//		int i=0;
		if(!eventIds.isEmpty()){
		List<ManagePurchasePrice> managePurchasePrices = DAORegistry.getManagePurchasePriceDAO().getAllByArtistExchangeTicketTypes(eventIds, exchangeIds, ticketTypeIds);
		if(managePurchasePrices!=null && !managePurchasePrices.isEmpty()){
//			Event event = managePurchasePrices.get(0).getEvent();  // Need to change as per bussiness behaviour
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("HH:mm");
			Integer eventId = managePurchasePrices.get(0).getEvent().getId();
			for(ManagePurchasePrice managePurchasePrice:managePurchasePrices){
				eventIds.remove(managePurchasePrice.getEvent().getId());
				obj = new JSONObject();
				
				try {
					//obj.put("rowid",managePurchasePrice.getId());
					Event event = DAORegistry.getEventDAO().get(managePurchasePrice.getEvent().getId());
					obj.put("id",managePurchasePrice.getEvent().getId()); //
					obj.put("artistName",event.getArtist().getName());
					obj.put("eventName",event.getName()); // Need to change as per requirement
					obj.put("eventDate",df.format(event.getDate())); 
					obj.put("eventTime",tf.format(event.getTime()));
					obj.put("venue",event.getVenue().getBuilding()+","+event.getVenue().getCity()+","+event.getVenue().getState());
					obj.put("exchange",managePurchasePrice.getExchange());
					obj.put("tickettype",managePurchasePrice.getTicketType());
					obj.put("servicefee",managePurchasePrice.getServiceFee());
					obj.put("currencytype",managePurchasePrice.getCurrencyType());
					obj.put("shipping",managePurchasePrice.getShipping());
					obj.put("exist",true);
					obj.put("rowId",managePurchasePrice.getId());
					
					if(eventId.compareTo(managePurchasePrice.getEvent().getId())==0){ // Need to change as per req.
						artistList.put(obj);
					}else{
						eventId=managePurchasePrice.getEvent().getId(); // Need to change as per req.
						finalArray.put(artistList);
						artistList= new JSONArray();
						artistList.put(obj);
					}
					if(managePurchasePrices.lastIndexOf(managePurchasePrice)+1== managePurchasePrices.size()){
						finalArray.put(artistList);	
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		
		//finalArray.put(tourList);
		}
		if(!eventIds.isEmpty()){
			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByIds(eventIds);
			artistList = new JSONArray(); 
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("HH:mm");
			for(Event event:events){
	//			for(String exchange:exchangeIds){
					obj = new JSONObject();
					artistList = new JSONArray();
					try {
						obj.put("id",event.getId());
						obj.put("artistName",event.getArtist().getName());
						obj.put("eventName",event.getName()); // Need to change as per requirement
						obj.put("eventDate",df.format(event.getDate())); 
						obj.put("eventTime",tf.format(event.getTime()));
						obj.put("venue",event.getVenue().getBuilding()+","+event.getVenue().getCity()+","+event.getVenue().getState());
						obj.put("exchange",exchangeIds.get(0));
						obj.put("tickettype",ticketTypeIds.get(0));
						obj.put("servicefee","");
						obj.put("currencytype","");
						obj.put("shipping","");
						obj.put("exist",false);
						obj.put("rowId","000");
						artistList.put(obj);
						
						finalArray.put(artistList);
					} catch (JSONException e) {
						e.printStackTrace();
					}
	//			}
			}
		}
		IOUtils.write(finalArray.toString().getBytes(), response.getOutputStream());
		
	}
	
	public ModelAndView loadAuditUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("username");
		Collection<UserAction> actions = null;
		if(userName!=null && !userName.isEmpty()){
			actions= DAORegistry.getUserActionDAO().getActionsByUsername(userName);
		}
		ModelAndView mav = new ModelAndView("page-admin-view-audit");
		mav.addObject("actions", actions);
		User user = (User)request.getSession().getAttribute("userObj");
		if(user==null){
			user = DAORegistry.getUserDAO().getUserByUsernameOrEmail(userName, userName);
			((HttpServletRequest)request).getSession().setAttribute("userObj",user);
		}
		mav.addObject("user", user);
		return mav;
	}
	public AnnotationSessionFactoryBean getSessionFactoryBean() {
		return sessionFactoryBean;
	}

	public void setSessionFactoryBean(AnnotationSessionFactoryBean sessionFactoryBean) {
		this.sessionFactoryBean = sessionFactoryBean;
	}

//	public TicketListingCrawler getTicketListingCrawler() {
//		return ticketListingCrawler;
//	}
//
//	public void setTicketListingCrawler(
//			TicketListingCrawler ticketListingCrawler) {
//		this.ticketListingCrawler = ticketListingCrawler;
//	}
/*
	private class LockTimeOutException extends RuntimeException {
		private static final long serialVersionUID = -3805684140339661109L;

		public LockTimeOutException() {
			super();
		}
		
		public LockTimeOutException(String message) {
			super(message);
		}
	}

/ *	public EbayManager getEbayManager() {
		return ebayManager;
	}

	public void setEbayManager(EbayManager ebayManager) {
		this.ebayManager = ebayManager;
	}
*/
	public CreationEventListManager getCreationEventListManager() {
		return creationEventListManager;
	}

	public void setCreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}

	
}
