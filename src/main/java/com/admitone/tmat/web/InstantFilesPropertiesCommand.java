package com.admitone.tmat.web;

public class InstantFilesPropertiesCommand {

	private String instantTicketsScrubedTNWithThresholdFileLoc;
	private String instantTicketsNotes;
	private String shinstantTnScrubPercentCheck;
	private String shMarkupPercent;
	
	private String mercuryTicketsScrubedNothresholdFileLoc;
	private String mercuryTicketsNotes;
	
	private String mercuryTicketsCategorizedScrubFileLoc;
	private String mercuryTicketsCategorizedScrubNotes;
	
	private String mercuryTndScrubedTicketsFileLoc;
	private String mercuryTndScrubedTicketsNotes;
	
	
	private String tnMarkupPercent;
	private String tnMercuryScrubPercentCheck;
	
	private String tnowScrubedSHFileLoc;
	private String tnowScrubedTNFileLoc;
	private String tnowInstantTicketsNotes;
	private String tnowInstantScrubPercentCheck;
	
	public String getInstantTicketsScrubedTNWithThresholdFileLoc() {
		return instantTicketsScrubedTNWithThresholdFileLoc;
	}
	public void setInstantTicketsScrubedTNWithThresholdFileLoc(
			String instantTicketsScrubedTNWithThresholdFileLoc) {
		this.instantTicketsScrubedTNWithThresholdFileLoc = instantTicketsScrubedTNWithThresholdFileLoc;
	}
	public String getInstantTicketsNotes() {
		return instantTicketsNotes;
	}
	public void setInstantTicketsNotes(String instantTicketsNotes) {
		this.instantTicketsNotes = instantTicketsNotes;
	}
	public String getShinstantTnScrubPercentCheck() {
		return shinstantTnScrubPercentCheck;
	}
	public void setShinstantTnScrubPercentCheck(String shinstantTnScrubPercentCheck) {
		this.shinstantTnScrubPercentCheck = shinstantTnScrubPercentCheck;
	}
	public String getShMarkupPercent() {
		return shMarkupPercent;
	}
	public void setShMarkupPercent(String shMarkupPercent) {
		this.shMarkupPercent = shMarkupPercent;
	}
	public String getMercuryTicketsScrubedNothresholdFileLoc() {
		return mercuryTicketsScrubedNothresholdFileLoc;
	}
	public void setMercuryTicketsScrubedNothresholdFileLoc(
			String mercuryTicketsScrubedNothresholdFileLoc) {
		this.mercuryTicketsScrubedNothresholdFileLoc = mercuryTicketsScrubedNothresholdFileLoc;
	}
	public String getMercuryTicketsNotes() {
		return mercuryTicketsNotes;
	}
	public void setMercuryTicketsNotes(String mercuryTicketsNotes) {
		this.mercuryTicketsNotes = mercuryTicketsNotes;
	}
	public String getMercuryTicketsCategorizedScrubFileLoc() {
		return mercuryTicketsCategorizedScrubFileLoc;
	}
	public void setMercuryTicketsCategorizedScrubFileLoc(
			String mercuryTicketsCategorizedScrubFileLoc) {
		this.mercuryTicketsCategorizedScrubFileLoc = mercuryTicketsCategorizedScrubFileLoc;
	}
	public String getMercuryTicketsCategorizedScrubNotes() {
		return mercuryTicketsCategorizedScrubNotes;
	}
	public void setMercuryTicketsCategorizedScrubNotes(
			String mercuryTicketsCategorizedScrubNotes) {
		this.mercuryTicketsCategorizedScrubNotes = mercuryTicketsCategorizedScrubNotes;
	}
	public String getMercuryTndScrubedTicketsFileLoc() {
		return mercuryTndScrubedTicketsFileLoc;
	}
	public void setMercuryTndScrubedTicketsFileLoc(
			String mercuryTndScrubedTicketsFileLoc) {
		this.mercuryTndScrubedTicketsFileLoc = mercuryTndScrubedTicketsFileLoc;
	}
	public String getMercuryTndScrubedTicketsNotes() {
		return mercuryTndScrubedTicketsNotes;
	}
	public void setMercuryTndScrubedTicketsNotes(
			String mercuryTndScrubedTicketsNotes) {
		this.mercuryTndScrubedTicketsNotes = mercuryTndScrubedTicketsNotes;
	}
	public String getTnMarkupPercent() {
		return tnMarkupPercent;
	}
	public void setTnMarkupPercent(String tnMarkupPercent) {
		this.tnMarkupPercent = tnMarkupPercent;
	}
	public String getTnMercuryScrubPercentCheck() {
		return tnMercuryScrubPercentCheck;
	}
	public void setTnMercuryScrubPercentCheck(String tnMercuryScrubPercentCheck) {
		this.tnMercuryScrubPercentCheck = tnMercuryScrubPercentCheck;
	}
	
	public String getTnowInstantScrubPercentCheck() {
		return tnowInstantScrubPercentCheck;
	}
	public void setTnowInstantScrubPercentCheck(String tnowInstantScrubPercentCheck) {
		this.tnowInstantScrubPercentCheck = tnowInstantScrubPercentCheck;
	}
	public String getTnowInstantTicketsNotes() {
		return tnowInstantTicketsNotes;
	}
	public void setTnowInstantTicketsNotes(String tnowInstantTicketsNotes) {
		this.tnowInstantTicketsNotes = tnowInstantTicketsNotes;
	}
	public String getTnowScrubedSHFileLoc() {
		return tnowScrubedSHFileLoc;
	}
	public void setTnowScrubedSHFileLoc(String tnowScrubedSHFileLoc) {
		this.tnowScrubedSHFileLoc = tnowScrubedSHFileLoc;
	}
	public String getTnowScrubedTNFileLoc() {
		return tnowScrubedTNFileLoc;
	}
	public void setTnowScrubedTNFileLoc(String tnowScrubedTNFileLoc) {
		this.tnowScrubedTNFileLoc = tnowScrubedTNFileLoc;
	}
	
}
