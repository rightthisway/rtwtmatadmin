package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;

public class InstantFilesPropertiesController extends SimpleFormController {

	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		// TODO Auto-generated method stub
		InstantFilesPropertiesCommand command = new InstantFilesPropertiesCommand();
		
		Property filePath = DAORegistry.getPropertyDAO().get("file.path");
		Property shInstantFileName = DAORegistry.getPropertyDAO().get("instantTickets.scrubed.tn.withthreshold.fileLoc");
		String fileName=shInstantFileName.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		
		command.setInstantTicketsScrubedTNWithThresholdFileLoc(fileName);
		
		Property shInstantTicketsNotes = DAORegistry.getPropertyDAO().get("instantTickets.notes");
		command.setInstantTicketsNotes(shInstantTicketsNotes.getValue());
		
		Property shInstantPercentCheck= DAORegistry.getPropertyDAO().get("shinstant.tn.scrub.percent.check");
		command.setShinstantTnScrubPercentCheck(shInstantPercentCheck.getValue());

		command.setShMarkupPercent(DAORegistry.getPropertyDAO().get("sh.markup.percent").getValue());
		
		Property tnMercuryNoThresholdFile = DAORegistry.getPropertyDAO().get("mercuryTickets.scrubed.nothreshold.fileLoc");
		fileName=tnMercuryNoThresholdFile.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		command.setMercuryTicketsScrubedNothresholdFileLoc(fileName);
		
		
		
		
		Property tnMercuryNotes = DAORegistry.getPropertyDAO().get("mercuryTickets.notes");
		command.setMercuryTicketsNotes(tnMercuryNotes.getValue());
		
		Property mercuryCategorizedFile = DAORegistry.getPropertyDAO().get("mercuryTickets.categorized.scrub.fileLoc");
		fileName=mercuryCategorizedFile.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		command.setMercuryTicketsCategorizedScrubFileLoc(fileName);
		
		Property mercuryCategorizedNotes = DAORegistry.getPropertyDAO().get("mercuryTickets.categorized.scrub.notes");
		command.setMercuryTicketsCategorizedScrubNotes(mercuryCategorizedNotes.getValue());
		
		Property mercuryTNDScrubbedFile = DAORegistry.getPropertyDAO().get("mercury.tnd.ScrubedTickets.fileLoc");
		fileName=mercuryTNDScrubbedFile.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		command.setMercuryTndScrubedTicketsFileLoc(fileName);
		
		Property mercuryTNDScrubbedNotes = DAORegistry.getPropertyDAO().get("mercury.tnd.ScrubedTickets.notes");
		command.setMercuryTndScrubedTicketsNotes(mercuryTNDScrubbedNotes.getValue());
		
		command.setTnMarkupPercent(DAORegistry.getPropertyDAO().get("tn.markup.percent").getValue());
		command.setTnMercuryScrubPercentCheck(DAORegistry.getPropertyDAO().get("tn.mercury.scrub.percent.check").getValue());
		
		Property ticketsNowNotes = DAORegistry.getPropertyDAO().get("tnow.instant.tickets.notes");
		command.setTnowInstantTicketsNotes(ticketsNowNotes.getValue());
		
		Property ticketsNowScrubbedSHFile = DAORegistry.getPropertyDAO().get("tnow.scrubed.sh.fileLoc");
		fileName=ticketsNowScrubbedSHFile.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		command.setTnowScrubedSHFileLoc(fileName);
		Property ticketsNowScrubbedTNFile = DAORegistry.getPropertyDAO().get("tnow.scrubed.tn.fileLoc");
		fileName=ticketsNowScrubbedTNFile.getValue();
		if(fileName!=null){
			fileName = fileName.replace(filePath.getValue(), "");
		}else{
			fileName="";
		}
		
		command.setTnowScrubedTNFileLoc(fileName);
		
		Property ticketsNowPercentCheck = DAORegistry.getPropertyDAO().get("tnow.instant.scrub.percent.check");
		command.setTnowInstantScrubPercentCheck(ticketsNowPercentCheck.getValue());
		
		return command;
	}
	
	@Override
	protected ModelAndView onSubmit(Object command) throws Exception {
		InstantFilesPropertiesCommand cmd = (InstantFilesPropertiesCommand)command;
		List<Property> properties = new ArrayList<Property>();
		
		Property filePath = DAORegistry.getPropertyDAO().get("file.path");
		
		Property shInstantFileName = new Property(); 
		
		shInstantFileName.setName("instantTickets.scrubed.tn.withthreshold.fileLoc");
		
		String fileName=cmd.getInstantTicketsScrubedTNWithThresholdFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		shInstantFileName.setValue(fileName);
		properties.add(shInstantFileName);
		
		Property shInstantTicketsNotes = new Property();
		shInstantTicketsNotes.setName("instantTickets.notes");
		shInstantTicketsNotes.setValue(cmd.getInstantTicketsNotes());
		properties.add(shInstantTicketsNotes);
		
		Property shInstantPercentCheck= new Property();
		shInstantPercentCheck.setName("shinstant.tn.scrub.percent.check");
		shInstantPercentCheck.setValue(cmd.getShinstantTnScrubPercentCheck());
		properties.add(shInstantPercentCheck);
		
		Property shMarkupPercent= new Property();
		shMarkupPercent.setName("sh.markup.percent");
		shMarkupPercent.setValue(cmd.getShMarkupPercent());
		properties.add(shMarkupPercent);
		
		Property tnMercuryNoThresholdFile = new Property();
		tnMercuryNoThresholdFile.setName("mercuryTickets.scrubed.nothreshold.fileLoc");
		fileName=cmd.getMercuryTicketsScrubedNothresholdFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		tnMercuryNoThresholdFile.setValue(fileName);
		properties.add(tnMercuryNoThresholdFile);
		
		Property tnMercuryNotes = new Property();
		tnMercuryNotes.setName("mercuryTickets.notes");
		tnMercuryNotes.setValue(cmd.getMercuryTicketsNotes());
		properties.add(tnMercuryNotes);
		
		Property mercuryCategorizedFile = new Property();
		mercuryCategorizedFile.setName("mercuryTickets.categorized.scrub.fileLoc");
		fileName=cmd.getMercuryTicketsCategorizedScrubFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		mercuryCategorizedFile.setValue(fileName);
		properties.add(mercuryCategorizedFile);
		
		Property mercuryCategorizedNotes = new Property();
		mercuryCategorizedNotes.setName("mercuryTickets.categorized.scrub.notes");
		mercuryCategorizedNotes.setValue(cmd.getMercuryTicketsCategorizedScrubNotes());
		properties.add(mercuryCategorizedNotes);
		
		Property mercuryTNDScrubbedFile = new Property();
		mercuryTNDScrubbedFile.setName("mercury.tnd.ScrubedTickets.fileLoc");
		fileName=cmd.getMercuryTndScrubedTicketsFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		mercuryTNDScrubbedFile.setValue(fileName);
		properties.add(mercuryTNDScrubbedFile);
		
		Property mercuryTNDScrubbedNotes = new Property();
		mercuryTNDScrubbedNotes.setName("mercury.tnd.ScrubedTickets.notes");
		mercuryTNDScrubbedNotes.setValue(cmd.getMercuryTndScrubedTicketsNotes());
		properties.add(mercuryTNDScrubbedNotes);
		
		Property ticketsNowScrubbedSHFile = new Property();
		fileName=cmd.getTnowScrubedSHFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		ticketsNowScrubbedSHFile.setName("tnow.scrubed.sh.fileLoc");
		ticketsNowScrubbedSHFile.setValue(fileName);
		properties.add(ticketsNowScrubbedSHFile);
		
		Property ticketsNowScrubbedTNFile = new Property();
		fileName=cmd.getTnowScrubedTNFileLoc();
		if(fileName!=null){
			fileName = filePath.getValue() + fileName;
		}else{
			fileName="";
		}
		ticketsNowScrubbedTNFile.setName("tnow.scrubed.tn.fileLoc");
		ticketsNowScrubbedTNFile.setValue(fileName);
		properties.add(ticketsNowScrubbedTNFile);
		
		Property ticketsNowPercentCheck = new Property();
		ticketsNowPercentCheck.setName("tnow.instant.scrub.percent.check");
		ticketsNowPercentCheck.setValue(cmd.getTnowInstantScrubPercentCheck());
		properties.add(ticketsNowPercentCheck);
		
		Property ticketsNowNotes = new Property();
		ticketsNowNotes.setName("tnow.instant.tickets.notes");
		ticketsNowNotes.setValue(cmd.getTnowInstantTicketsNotes());
		properties.add(ticketsNowNotes);
		
		Property tnMarkupPercent = new Property();
		tnMarkupPercent.setName("tn.markup.percent");
		tnMarkupPercent.setValue(cmd.getTnMarkupPercent());
		properties.add(tnMarkupPercent);
		
		Property tnMercuryPercentCheck = new Property();
		tnMercuryPercentCheck.setName("tn.mercury.scrub.percent.check");
		tnMercuryPercentCheck.setValue(cmd.getTnMercuryScrubPercentCheck());
		properties.add(tnMercuryPercentCheck);
		
		DAORegistry.getPropertyDAO().updateAll(properties);
		
		return new ModelAndView(new RedirectView("AdminInstantFileSettings?info=The Instant File Properties has been updated."));
	}
}
