package com.admitone.tmat.web;

import com.admitone.tmat.crawler.TicketListingCrawl;

/**
 * Class that holds all the fields of the Edit Ticket Listing Crawl Form.
 */
public class TicketListingCrawlCommand {
	private TicketListingCrawl ticketListingCrawl;
	private String originalName;
	
	// used by ebay
	// private String queryString;
	// private String queryYear;
	// private String queryMonth;
	// private String queryDay;
	private Boolean automaticCrawlFrequency = true;
	private String queryUrl;
	
	// used by eimarketplace and ticket network direct
	private String queryEventId;

	// used by eibox
	private String queryVenueId;
	private String queryEventDate;
	
	public String getQueryVenueId() {
		return queryVenueId;
	}

	public void setQueryVenueId(String queryVenueId) {
		this.queryVenueId = queryVenueId;
	}

	public String getQueryEventDate() {
		return queryEventDate;
	}

	public void setQueryEventDate(String queryEventDate) {
		this.queryEventDate = queryEventDate;
	}

	private boolean forceRecrawl;
	
	private String backUrl; 
	
	public TicketListingCrawlCommand() {
	}

	public TicketListingCrawlCommand(TicketListingCrawl ticketListingCrawl) {
		this.originalName = ticketListingCrawl.getName();
		this.ticketListingCrawl = ticketListingCrawl;
		this.automaticCrawlFrequency = ticketListingCrawl.getAutomaticCrawlFrequency();
		this.queryUrl = ticketListingCrawl.getQueryUrl();
	}

	public TicketListingCrawl getTicketListingCrawl() {
		return ticketListingCrawl;
	}
	public void setTicketListingCrawl(TicketListingCrawl ticketListingCrawl) {
		this.ticketListingCrawl = ticketListingCrawl;
	}
	
	public String getOriginalName() {
		return originalName;
	}
	
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	/*
	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public String getQueryYear() {
		return queryYear;
	}

	public void setQueryYear(String queryYear) {
		this.queryYear = queryYear;
	}

	public String getQueryMonth() {
		return queryMonth;
	}

	public void setQueryMonth(String queryMonth) {
		this.queryMonth = queryMonth;
	}

	public String getQueryDay() {
		return queryDay;
	}

	public void setQueryDay(String queryDay) {
		this.queryDay = queryDay;
	}
*/
	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	public boolean isForceRecrawl() {
		return forceRecrawl;
	}

	public void setForceRecrawl(boolean forceRecrawl) {
		this.forceRecrawl = forceRecrawl;
	}

	public String getQueryEventId() {
		return queryEventId;
	}

	public void setQueryEventId(String queryEventId) {
		this.queryEventId = queryEventId;
	}

	public String getBackUrl() {
		return backUrl;
	}

	public void setBackUrl(String backUrl) {
		this.backUrl = backUrl;
	}

	public Boolean getAutomaticCrawlFrequency() {
		return automaticCrawlFrequency;
	}

	public void setAutomaticCrawlFrequency(Boolean automaticCrawlFrequency) {
		this.automaticCrawlFrequency = automaticCrawlFrequency;
	}
}