package com.admitone.tmat.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.TourCategory;

public class EditorEditTourCategoryFormController extends SimpleFormController{
	
	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		String action=request.getParameter("action");
		//String action=request.getParameter("info");
		if("create".equalsIgnoreCase(action)){
			return new TourCategory();
		}else {
			String id=request.getParameter("id");
			return DAORegistry.getTourCategoryDAO().get(Integer.parseInt(id));
		}
	}
	
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		// TODO Auto-generated method stub
		return super.showForm(request, response, errors, controlModel);
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		TourCategory tourCategory =(TourCategory) command;
		String info="";
//		TourCategory tourCategory = new GrandChildTourCategory(grandChildTourCategoryCommand);
		TourCategory exist= DAORegistry.getTourCategoryDAO().getTourCategoryByName(tourCategory.getName());
		if(exist!= null){
			if(tourCategory.getName().equals(exist.getName())){
				info="Category already exist.";
			}
			else{
				DAORegistry.getTourCategoryDAO().update(tourCategory);
				info = "Tour Category updated succesfully..";
			}
		}else{
			try{
				tourCategory.setName(tourCategory.getName());
				if(tourCategory.getId()==null){
					DAORegistry.getTourCategoryDAO().save(tourCategory);
					info="Tour Category saved succesfully..";
				}else{
					DAORegistry.getTourCategoryDAO().update(tourCategory);
					info="Tour Category updated succesfully..";
				}
			}catch (Exception e) {
				info="Error occured while saving Tour Category..";
			}
		}
		ModelAndView mav = new ModelAndView("page-editor-edit-tour-category");
		mav.addObject("tourCategory", command);
		
		mav.addObject("info", info);
		mav.addObject("action","create");
		return mav;
	}

}
