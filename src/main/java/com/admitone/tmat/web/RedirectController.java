package com.admitone.tmat.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.maven.surefire.booter.shade.org.codehaus.plexus.util.IOUtil;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.eimarketplace.EIMPUtil;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class RedirectController extends  MultiActionController {
//	private TicketListingCrawler ticketListingCrawler;
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();
	private DefaultHttpClient eimpHttpClient = new DefaultHttpClient();

	public ModelAndView loadRedirectToEimpMapPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer eventId = Integer.valueOf(request.getParameter("id"));

		byte[] content = EIMPUtil.getEimpMap(eventId);
		IOUtils.write(content, response.getOutputStream());
		return null;
	}

	public ModelAndView loadRedirectToSellerPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer ticketId = Integer.valueOf(request.getParameter("id"));
		Ticket ticket = DAORegistry.getTicketDAO().get(ticketId);
		TicketListingFetcher fetcher = ticketListingFetcherMap.get(ticket.getSiteId());
		String content = EIMPUtil.getEimpSellerPage(ticket, fetcher);
		System.out.println("The content is: " + content);
		IOUtils.write("<html> <head>    <title>Broker Information</title>   </head>  <body>    " + content + " </body></html>", response.getOutputStream());
		return null;
	}

	public ModelAndView loadRedirectToItemPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.valueOf(request.getParameter("id"));
		Ticket ticket = DAORegistry.getTicketDAO().get(id);		
		// TODOCHECK
		TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(ticket.getTicketListingCrawlId());
		TicketListingFetcher fetcher = ticketListingFetcherMap.get(crawl.getSiteId());
		
		if (fetcher.getTicketItemUrl(crawl,  ticket) != null) {
			return new ModelAndView("redirect:" + fetcher.getTicketItemUrl(crawl,  ticket));			
		} else {
			return new ModelAndView("redirect:" + fetcher.getTicketListingUrl(crawl));						
		}		
	}

	public ModelAndView loadRedirectToListingPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		int crawlId = Integer.parseInt(request.getParameter("id"));
		// TODOCHECK
		TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
		TicketListingFetcher fetcher = ticketListingFetcherMap.get(crawl.getSiteId());
		return new ModelAndView("redirect:" + fetcher.getTicketListingUrl(crawl));
	}

	/*public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}*/

	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}
}