package com.admitone.tmat.web;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.TourType;

public class EditorEditVenueFormController extends SimpleFormController {
	
	String isVenue=null;
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		String action = request.getParameter("action");
		if (action.equals("create")) {
			Venue venue = new Venue();
			venue.setCountry("US");
			return venue;
		}
		
		int venueId = Integer.parseInt(request.getParameter("id"));
		String tourId = request.getParameter("tourId");
		String venId = request.getParameter("venueId");
		if(venId != null && !venId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venId;
		}else if(tourId != null && !tourId.isEmpty()){
			isVenue = "EditorManageTCAP?tourId="+tourId;
		}
		Venue venue = DAORegistry.getVenueDAO().get(venueId);
		if(venue.getVenueType() != null){
			if(venue.getVenueType().equals(TourType.CONCERT.toString())){
				venue.setVenueType(TourType.SPORT);
			}
		}
		
		return venue;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
	throws Exception  {
		Venue venue = (Venue)command;
		
		String info;
		String tcap = request.getParameter("tcap");
		if (venue.getId() == null) {
			DAORegistry.getVenueDAO().save(venue);
			info = "The venue has been created";
		} else {
			//change event type of all event with this venue
			//Set<Integer> tourSet= new HashSet<Integer>();
			//Integer tourIds;
			if(venue.getVenueType() == null){
				Collection<Event> eventList = DAORegistry.getEventDAO().getAllEventsByVenue(venue.getId());
				if(!eventList.isEmpty()){
					for(Event event:eventList){
						if(event.getEventOverridden() == null || !event.getEventOverridden().equalsIgnoreCase("MENUAL")){
								event.setEventType(venue.getVenueType());
//								System.out.println("eventId====="+event.getId());
						}
					}
						//tourIds = event.getTourId();
						//tourSet.add(tourIds);
					DAORegistry.getEventDAO().updateAll(eventList);
				}
			}
			DAORegistry.getVenueDAO().update(venue);
			info = "The venue has been updated";
		}
			/*if(tourSet.size() > 0){
				List<Integer> tourList = new ArrayList<Integer>();
				tourList.addAll(tourSet);
				Collection<Tour> tour = DAORegistry.getTourDAO().getTourByIds(tourList);
				if(!tour.isEmpty()){
					for(Tour tour1:tour){
						tour1.setTourType(venue.getVenueType());
					}
				}
				DAORegistry.getTourDAO().updateAll(tour);
			}*/
			
			//Collection<Tour> tourList = DAORegistry.getTourDAO()
			DAORegistry.getVenueDAO().update(venue);			
			info = "The venue has been updated";
				
		if(tcap != null && !tcap.isEmpty()){
			return new ModelAndView(new RedirectView(tcap));
		}
		return new ModelAndView(new RedirectView("EditorManageVenues?info=" + info));
	}		
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		String action = request.getParameter("action");
		ModelAndView mav = super.showForm(request, response, errors, controlModel);

		if (action != null && action.equals("create")) {
			return mav;
		} else {
			int venueId = Integer.valueOf(request.getParameter("id"));
			mav.addObject("events", DAORegistry.getEventDAO().getAllEventsByVenue(venueId));
		}
		//mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
		mav.addObject("fromTcap",isVenue);
		return mav;
	}
}	


