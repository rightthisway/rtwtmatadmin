package com.admitone.tmat.web;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawl.AutomatedCrawlCreationInstance1;
import com.admitone.tmat.crawl.AutomatedCrawlCreationInstanceByVenue;
import com.admitone.tmat.crawl.AutomatedCrawlCreationUtilByArtistSecondary;
import com.admitone.tmat.crawl.AutomatedCrawlCreationUtilByVenue;
import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.DefaultExchangeCrawlFrequency;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.eventcreation.AutoEventListingResult;
import com.admitone.tmat.eventcreation.CreationEventHit;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingManager;
import com.admitone.tmat.eventfetcher.EventListingResult;
import com.admitone.tmat.indexer.PreviewTicketHitIndexer;
import com.admitone.tmat.pojo.EventCrawlerDetails;
import com.admitone.tmat.ticketfetcher.SeatWaveFeedTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.web.pojo.WebTicketListingCrawlRow;

public class CrawlerController extends MultiActionController implements InitializingBean {
	private static Logger logger = LoggerFactory.getLogger(CrawlerController.class); 
	
	private static final Long MAX_NUM_DAY_PERIOD = 60L;
	private static final Long MAX_DATE_PERIOD = MAX_NUM_DAY_PERIOD * 24L * 3600L * 1000L;
	private CreationEventListManager creationEventListManager;
	private PreferenceManager preferenceManager;
//	private TicketListingCrawler ticketListingCrawler;
	private EventListingManager eventListingManager;
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();
//	private CrawlerSchedulerManager crawlerSchedulerManager;
	private SharedProperty sharedProperty;
	
	public ModelAndView loadConfirmDeleteCrawlPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-confirm-delete-ticket-listing-crawl");
		int crawlId = Integer.parseInt(request.getParameter("crawlId"));
		TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
		mav.addObject("ticketListingCrawl", crawl);
		return mav;
	}
	
	public ModelAndView loadConfirmDisableCrawlPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-confirm-disable-ticket-listing-crawl");
		int crawlId = Integer.parseInt(request.getParameter("crawlId"));
		TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
		mav.addObject("ticketListingCrawl", crawl);
		return mav;
	}

	/*public ModelAndView loadEditorArtistCrawlsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String artistIdStr = request.getParameter("artistId");
		String filter = request.getParameter("filter");
		ModelAndView mav = new ModelAndView("page-editor-tour-crawls");
		
		Collection<Artist> artists = null;
		
		if (artistIdStr != null && !artistIdStr.trim().isEmpty()) {
			Integer artistId = Integer.parseInt(artistIdStr);
			
			if (filter != null && !filter.trim().isEmpty()) {
				filter = filter.trim();
				artists = DAORegistry.getArtistDAO().filterByName(filter);
			} else {
				Artist  artist = DAORegistry.getArtistDAO().get(artistId); 
				if(artists==null){
					artists = new ArrayList<Artist>(); 
				}
				artists.add(artist);
			}
			
		} else {
			if (filter != null && !filter.trim().isEmpty()) {
				filter = filter.trim();
				artists = DAORegistry.getArtistDAO().filterByName(filter);
			} else {
				artists = DAORegistry.getArtistDAO().getAll();
			}
		}
		
		mav.addObject("artists", artists);
//		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		return mav;
	}*/

	
	public ModelAndView loadManageCrawlsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String action = request.getParameter("crawlFormAction");
//		String selectFrequency = request.getParameter("crawlFormBulkFrequency");
		String autoCrawlerFrequencyStatus = request.getParameter("checkAutoCrawlFrequencyStatus");
//		String checkBoxCrawlStatus = request.getParameter("checkCrawlFormStatus");
		String crawlFrequency = request.getParameter("crawlCrawlFrequency");
		String info = "";
		try{
		if (action!=null && action.equals("updateCrawl")){
//				&& autoCrawlerFrequencyStatus!=null && checkBoxCrawlFrequency!=null) {
			String[] crawlIdsString = request.getParameter("crawlFormIds").split(",");
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action + "&crawls=";
			for (String crawlIdString : crawlIdsString) {
				int crawlId = Integer.parseInt(crawlIdString);
				TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
				if(crawlFrequency!=null && !crawlFrequency.isEmpty()){
					crawl.setCrawlFrequency(Integer.parseInt(crawlFrequency));
				}
				if(autoCrawlerFrequencyStatus!= null) {//&& Boolean.parseBoolean(autoCrawlerFrequencyStatus)){
					crawl.setAutomaticCrawlFrequency(Boolean.parseBoolean(autoCrawlerFrequencyStatus));
				}
				// re activate tickets
				//DAORegistry.getTicketDAO().enableTickets(crawlId);			
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
				url+= crawl.getId() + ",";
			}
			info = "Crawls updated successfully..";
			url=url.substring(0,url.length()-1);
			sendRequest(url);
		}
		}catch (Exception e) {
			System.out.println(e);
		}
		//
		// status filter option
		//
		String statusTime = request.getParameter("statusTime");
//		int statusTimeInSeconds;
		if (statusTime == null) {
			// look for a value in the preferences
			statusTime = preferenceManager.getPreferenceValue(username, "editorCrawlerStatusTime");
		}

		if (
				!"ERROR".equals(statusTime)
				&& !"1mn".equals(statusTime)
				&& !"30mn".equals(statusTime)
				&& !"1hr".equals(statusTime)
				&& !"2hr".equals(statusTime)
				&& !"2+hr".equals(statusTime)) {
			statusTime = "ALL";
		}

		preferenceManager.updatePreference(username, "editorCrawlerStatusTime", statusTime);

		Integer eventId = 0;
		//
		// Artist Id filter
		//
		
		//Tamil : select artist with minimum one valid events only
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtistsWithEvents();
		//Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
		
		//Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Collection<Venue> venues = new ArrayList<Venue>();
		Collection<Event> events = null;
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		Collection<Venue> listofVenue;
		String artistName = new String();
		boolean getArtistFlag = false,getVenueFlag = false;
		ModelAndView mav = new ModelAndView("page-editor-manage-crawls");
		if (artistIdString == null) {
			// look for a value in the preference
			artistIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerArtistId");
		}
		
		Integer artistId = 0;
		if (artistIdString != null && artistIdString.length() > 0 && !artistIdString.equals("0")) {
			getArtistFlag = true;
			artistId = Integer.parseInt(artistIdString);
			venues=new ArrayList<Venue>();
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			for (Event event :events) {
				listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
				for (Venue venue : listofVenue) {
					if(!venues.contains(venue)){
						venues.add(venue);
					}
				}
                 // DAORegistry.getVenueDAO().getVenue(event.getVenue())
			}
			mav.addObject("venues", venues);
		}
		Integer venueId = 0;
		if (venueIdString != null && venueIdString.length() > 0 && !venueIdString.equals("0")) {
			getVenueFlag = true;
			venueId = Integer.parseInt(venueIdString);
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId,EventStatus.ACTIVE);
		}
		
		if(venueIdString!=null && artistIdString!=null && venueIdString.length()>0 && artistIdString.length() > 0 &&!venueIdString.equals("0") && !artistIdString.equals("0")){
	     	try{
	     		getArtistFlag = true;
	     		getVenueFlag = true;
	     		venueId = Integer.parseInt(venueIdString);
	     		artistId = Integer.parseInt(artistIdString);
	     		events = DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId,venueId,EventStatus.ACTIVE);
	     	}catch(Exception e){
	     		e.printStackTrace();
	     	}
	     	
	}
		
		if(getArtistFlag){
			venues=new ArrayList();
			Artist artist = DAORegistry.getArtistDAO().get(artistId);
			artistName = artist.getName();
			mav.addObject("artistName", artistName);
		}
		
		if(getVenueFlag){
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			String venueName = venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry();
			mav.addObject("venueName", venueName);
		}
		
		/*if (artistId != 0 && DAORegistry.getArtistDAO().get(artistId) == null) {
			artistId = 0;
		}*/
		
//		preferenceManager.updatePreference(username, "editorCrawlerTourId", tourId + "");

		//
		// Event Id filter
		//
		String eventIdString = request.getParameter("eventId");
		if (eventIdString == null) {
			// look for a value in the preferences
			eventIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerEventId");
		}

		if (eventIdString != null && eventIdString.length() > 0) {
			eventId = Integer.parseInt(eventIdString);
		}
		
		// check if event id belongs to tour
		Event event = DAORegistry.getEventDAO().get(eventId);
		if (event == null ){//|| (!event.getArtistId().equals(artistId) && event.getArtistId() != artistId)) {
			eventId = 0;
		}

//		preferenceManager.updatePreference(username, "editorCrawlerEventId", Integer.toString(eventId));

		//
		// Site Id filter
		//
		String siteId = request.getParameter("siteId");
		if (siteId == null) {
			// look for a value in the preferences
			siteId = preferenceManager.getPreferenceValue(username, "editorCrawlerSiteId");
			if (siteId == null) {
				siteId = "";
			}
		}
		
		preferenceManager.updatePreference(username, "editorCrawlerSiteId", siteId);

		Boolean showDisabledCrawls = false;
		String showDisabledCrawlsString = request.getParameter("showDisabledCrawls");
		if (showDisabledCrawlsString == null) {
			showDisabledCrawlsString = preferenceManager.getPreferenceValue(username, "editorCrawlerShowDisabledCrawls");
			if (showDisabledCrawlsString == null) {
				showDisabledCrawlsString = "";
			}
		}
		showDisabledCrawls = showDisabledCrawlsString.equals("on");
		
		Boolean showOnlyPresaleCrawls = false;
		String showOnlyPresaleCrawlsString = request.getParameter("showOnlyPresaleCrawls");
		if (showOnlyPresaleCrawlsString == null) {
			showOnlyPresaleCrawlsString = preferenceManager.getPreferenceValue(username, "editorCrawlerShowOnlyPresaleCrawls");
			if (showOnlyPresaleCrawlsString == null) {
				showOnlyPresaleCrawlsString = "";
			}
		}
		showOnlyPresaleCrawls = showOnlyPresaleCrawlsString.equals("on");

		preferenceManager.updatePreference(username, "editorCrawlerShowDisabledCrawls", showDisabledCrawlsString);
		preferenceManager.updatePreference(username, "editorCrawlerShowOnlyPresaleCrawls", showOnlyPresaleCrawlsString);

		
		if (action == null) {
			
			
			
			mav.addObject("artists", artists);
			//mav.addObject("venues", venues );
			
			int disabledCrawlCount = 0;
			int readyCrawlCount = 0;
									
			Map<Integer, Artist> artistById = new HashMap<Integer, Artist>();
			Map<Integer, Integer> artistIdByEventId = new HashMap<Integer, Integer>();
			for (Artist artist: artists) {
				artistById.put(artist.getId(), artist);
			}
			
			Map<Integer, Event> eventById = new HashMap<Integer, Event>();
			for (Event ev: DAORegistry.getEventDAO().getAll()) {
				eventById.put(ev.getId(), ev);
				artistIdByEventId.put(ev.getId(), ev.getArtistId());
			}

			Map<Integer, Venue> venueById = new HashMap<Integer, Venue>();
			for (Venue venue: DAORegistry.getVenueDAO().getAll()) {
				venueById.put(venue.getId(), venue);
			}
			Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
			if(artistId != null && artistId!=0 && venueId != null && venueId!=0){
				crawls = DAORegistry.getTicketListingCrawlDAO().getAllTicketListingCrawlByArtistAndVenue(artistId,venueId);
			}else if(artistId != null && artistId!=0){
				crawls = DAORegistry.getTicketListingCrawlDAO().getAllTicketListingCrawlByArtist(artistId);
			} else if(venueId != null && venueId!=0){
				crawls = DAORegistry.getTicketListingCrawlDAO().getAllTicketListingCrawlByVenue(venueId);
			} else{
				//crawls = DAORegistry.getTicketListingCrawlDAO().getAll();
			}
			
			Date now = new Date();
			Collection<WebTicketListingCrawlRow> ticketListingCrawls = new ArrayList<WebTicketListingCrawlRow>();
			for (TicketListingCrawl crawl : crawls) {
				if (!crawl.isEnabled()) {
					disabledCrawlCount++;
				} else {
					if (crawl.getCrawlState().equals(CrawlState.STOPPED)
							&& (crawl.getEndCrawl() == null
									|| (now.getTime() - crawl.getEndCrawl().getTime()) > crawl.getCrawlFrequency() * 1000L)) {
						readyCrawlCount++;
					}
				}
				// Replace getEvent() method by hashmap
				Integer eventArtistId = artistIdByEventId.get(crawl.getEventId());
				Event ev = eventById.get(crawl.getEventId());
				if ((artistId == 0 || eventArtistId == null || eventArtistId.equals(artistId))
						&& (eventId == 0 || crawl.getEventId() == null || crawl.getEventId().equals(eventId))
						&& (siteId.length() == 0 || crawl.getSiteId().equals(siteId))
						&& (showDisabledCrawls || crawl.isEnabled())
						&& (!showOnlyPresaleCrawls || (ev.getVenueCategory()!=null && crawl.getEvent().getVenueCategory().getCategoryGroup().equals("PRESALE")))
						&& (statusTime.equals("ALL")
								|| (statusTime.equals("ERROR") && crawl.getCrawlState().isError())
								|| (statusTime.equals("1mn") && crawl.getNextCrawlWaitingTime() < 60)
								|| (statusTime.equals("30mn") && crawl.getNextCrawlWaitingTime() > 60 && crawl.getNextCrawlWaitingTime() < 1800)
								|| (statusTime.equals("1hr") && crawl.getNextCrawlWaitingTime() > 1800 && crawl.getNextCrawlWaitingTime() < 3600)
								|| (statusTime.equals("2hr") && crawl.getNextCrawlWaitingTime() > 3600 && crawl.getNextCrawlWaitingTime() < 7200)
								|| (statusTime.equals("2+hr") && crawl.getNextCrawlWaitingTime() > 7200)
						)
				) {
					// ticketListingCrawls.add(crawl);

					
					ticketListingCrawls.add(
							new WebTicketListingCrawlRow(crawl.getId(), crawl.getName(),
							crawl.getSiteId(), ev, 
							(ev == null)?null:venueById.get(ev.getVenueId()),
							crawl.getCrawlFrequency(), crawl.getAutomaticCrawlFrequency(),
							crawl.getNextCrawlWaitingTime(), crawl.getCrawlState(), 
							crawl.isEnabled(), crawl.isBroken(),
							crawl.getStartCrawl(), crawl.getEndCrawl(), crawl.getErrorMessage(),crawl.getQueryUrl(), crawl.getExtraParametersString()));
				}
			}
			

			mav.addObject("statusTime", statusTime);
			// TODOJMS  see jsp
//			mav.addObject("ticketListingCrawler", ticketListingCrawler);
//			mav.addObject("totalTicketListingCrawlCount", ticketListingCrawler.getTicketListingCrawls().size());
//			mav.addObject("failedTicketListingCrawlCount", ticketListingCrawler.getFailedTicketListingCrawlsByEventId().size());
			
			mav.addObject("ticketListingCrawls", ticketListingCrawls);
			mav.addObject("events", events);			
						
			mav.addObject("readyTicketListingCrawlCount", readyCrawlCount);
			mav.addObject("disabledTicketListingCrawlCount", disabledCrawlCount);

			mav.addObject("editorCrawlerSiteId", siteId);
			mav.addObject("editorCrawlerArtistId", Integer.toString(artistId));
			mav.addObject("editorCrawlerVenueId", Integer.toString(venueId));
			mav.addObject("editorCrawlerEventId", Integer.toString(eventId));
			mav.addObject("editorCrawlerShowDisabledCrawls", showDisabledCrawlsString);
			mav.addObject("editorCrawlerShowOnlyPresaleCrawls", showOnlyPresaleCrawlsString);
			return mav;
		} else if (action.equals("enableCrawl")) {
			String crawlIds = request.getParameter("crawlFormIds");
			if(crawlIds != null && !crawlIds.isEmpty()){
				String[] crawlIdsString = crawlIds.split(",");
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action + "&crawls=";
				List<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
				for (String crawlIdString : crawlIdsString) {
					int crawlId = Integer.parseInt(crawlIdString);
					TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
					crawl.setEnabled(true);
					// re activate tickets
					DAORegistry.getTicketDAO().enableTickets(crawlId);			
					crawls.add(crawl);
					// TODOJMS
//					ticketListingCrawler.addTicketListingCrawl(crawl);
					
					creationEventListManager.addSitesInSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());
					url+= crawl.getId() + ",";
				}
				DAORegistry.getTicketListingCrawlDAO().updateAll(crawls);
				url=url.substring(0,url.length()-1);
				sendRequest(url);
				/******/
				// JMSCODE
//				JMSMessageSender sender = JMSUtils.getJmsMessageSender();
//				sender.sendMessageToBrowse("crawl-enable", crawlIds);
				/******/
				info = "Enabled " + crawlIdsString.length + " crawls";
				
			}
			 
//			creationEventListManager.addSitesInSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());
			
		} else if (action.equals("disableCrawl")) {
			String crawlIds = request.getParameter("crawlFormIds");
			List<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
			if(crawlIds!=null && !crawlIds.isEmpty()){
				String[] crawlIdsString = crawlIds.split(",");
				for (String crawlIdString : crawlIdsString) {
					int crawlId = Integer.parseInt(crawlIdString);
					TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
					crawl.setEnabled(false);
					crawl.setEndCrawl(new Date());
					crawl.setLastUpdated(new Date());
					crawl.setLastUpdater(username);
					DAORegistry.getTicketDAO().disableTickets(crawlId);
					crawls.add(crawl);
					creationEventListManager.disableSitesInSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());
				}
				DAORegistry.getTicketListingCrawlDAO().updateAll(crawls);
				/***JMS***/
//				JMSMessageSender sender = JMSUtils.getJmsMessageSender();
//				sender.sendMessageToBrowse("crawl-disable", crawlIds);
				/******/
				/** WS **/
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action + "&crawls=" + crawlIds;
				sendRequest(url);
				info = "Disabled " + crawlIdsString.length + " crawls";
			}
		} else if (action.equals("forceRecrawl")) {
			String crawlIds = request.getParameter("crawlFormIds");
			if(crawlIds!=null && !crawlIds.isEmpty()){
				/******/
//					JMSMessageSender sender=  JMSUtils.getJmsMessageSender();
//					sender.sendMessageToBrowse("crawl-force", crawlIds);
				/******/
				/** WS **/
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action + "&crawls=" + crawlIds;
				sendRequest(url);
				info = "Disabled " + crawlIds.split(",").length + " crawls";
			}
		} else if (action.equals("removeCrawl")) {
			String crawlIds = request.getParameter("crawlFormIds");
			if(crawlIds!=null && !crawlIds.isEmpty()){
				String[] crawlIdsString = crawlIds.split(",");
				for (String crawlIdString : crawlIdsString) {
					int crawlId = Integer.parseInt(crawlIdString);
					TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
	
					// delete all ticket listing associated to that crawl
					DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId);
	
					// delete crawl
					// TODOJMS
//					ticketListingCrawler.removeTicketListingCrawl(crawl);
					creationEventListManager.deleteSitesFromSitesPerEventMap(crawl.getEventId(), crawl.getSiteId());
					try {
						DAORegistry.getTicketListingCrawlDAO().deleteById(crawlId);
					} catch (Exception e) {
						logger.error("Error while deleting crawl from DB", e);
					}
					info += "The crawl " + crawl.getName() + " has been deleted!<br />";
				}
				/******/
				// WS
				/******/
				
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action + "&crawls=" + crawlIds;
				sendRequest(url);
				
				/******/
				// JMS
				/******/
				
//				JMSMessageSender sender=  JMSUtils.getJmsMessageSender();
//				sender.sendMessageToBrowse("crawl-remove", crawlIds);
			}
			
		}  else if (action.equals("disableExpiredCrawl")) {
			// TODOJMS  // Think hard
			/*Collection<TicketListingCrawl> ticketListingCrawls = DAORegistry.getTicketListingCrawlDAO().get;
			if(ticketListingCrawls != null) {
				for(TicketListingCrawl crawl: ticketListingCrawls) {
					if(crawl.isEnabled() && crawl.getEvent() != null && crawl.getEvent().getEventStatus().equals(EventStatus.EXPIRED)){
						crawl.setEnabled(false);
						DAORegistry.getTicketDAO().disableTickets(crawl.getId());
						DAORegistry.getTicketListingCrawlDAO().update(crawl);
					}
				}
			}*/
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=" + action;
			sendRequest(url);
			info += "The Expired Crawl job started successfully..!<br />";
		}
		
		String backUrl = request.getParameter("backUrl");
		if (backUrl == null) {
			String url = "EditorManageCrawls?info=" + info;
			if(artistId!=null && artistId!=0){
				url += "&artistId="+artistId;
			}
			if(venueId!=null && venueId!=0){
				url += "&venueId="+venueId;
			}
			if(eventId!=null && eventId!=0){
				url += "&eventId="+eventId;
			}
			return new ModelAndView(new RedirectView(url, true));
		} else {
			if (backUrl.contains("?")) {
				return new ModelAndView(new RedirectView(backUrl + "&info=" + info, true));
			} else {
				return new ModelAndView(new RedirectView(backUrl + "?info=" + info, true));				
			}		}
	}

	public ModelAndView disableExpiredCrawlForExpiredEvents(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=disableExpiredCrawl";
		System.out.println(url);
		sendRequest(url);
		return new ModelAndView(new RedirectView("EditorManageExpiredEvents?info=The events have been expired"));
	}
	/**
	 * Load Preview Fetch Ticket for a crawl. 
	 */
	public ModelAndView loadPreviewFetchTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			String siteId = request.getParameter("siteId");
			Integer tourId = (request.getParameter("tourId") == null 
					|| request.getParameter("tourId").length() == 0)?null:Integer.parseInt(request.getParameter("tourId"));

		   Integer eventId = (request.getParameter("eventId") == null 
					|| request.getParameter("eventId").length() == 0)?null:Integer.parseInt(request.getParameter("eventId"));
		
		 
			String queryUrl = request.getParameter("queryUrl");

			Integer previewLimitSize = (request.getParameter("previewLimitSize") == null 
					|| request.getParameter("previewLimitSize").length() == 0)?null:Integer.parseInt(request.getParameter("previewLimitSize"));

			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();

			if (siteId.equals(Site.EBAY)) {
				// ticketListingCrawl.setExtraParameter("queryYear", request.getParameter("queryYear"));
				// ticketListingCrawl.setExtraParameter("queryMonth", request.getParameter("queryMonth"));
				// ticketListingCrawl.setExtraParameter("queryDay", request.getParameter("queryDay"));
				// ticketListingCrawl.setExtraParameter("queryString", request.getParameter("queryString"));
			} else if (siteId.equals(Site.EI_MARKETPLACE) || siteId.equals(Site.TICKET_NETWORK_DIRECT)) {
				ticketListingCrawl.setExtraParameter("queryEventId", request.getParameter("queryEventId"));
			} else if (siteId.equals(Site.EIBOX)) {
				ticketListingCrawl.setExtraParameter("queryEventId", request.getParameter("queryEventId"));
				ticketListingCrawl.setExtraParameter("queryVenueId", request.getParameter("queryVenueId"));
				ticketListingCrawl.setExtraParameter("queryEventDate", request.getParameter("queryEventDate"));
			}

			ticketListingCrawl.setSiteId(siteId);
//			ticketListingCrawl.setTourId(tourId);
			ticketListingCrawl.setEventId(eventId);
			ticketListingCrawl.setQueryUrl(queryUrl);

			System.out.println("FETCHING TICKETS with crawl:"  + ticketListingCrawl);
			
			PreviewTicketHitIndexer ticketHitIndexer = new PreviewTicketHitIndexer(previewLimitSize);

			TicketListingFetcher ticketListingFetcher = (TicketListingFetcher)(ticketListingFetcherMap.get(siteId).clone());
			//System.out.println("THEURL: " + queryUrl);
			if (!ticketListingFetcher.isValidUrl(queryUrl)) {
				System.out.println("INVALIDURL!!");
				throw new Exception("Invalid URL");
			}

			try {
				ticketListingFetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			} catch (InterruptedTicketListingCrawlException e) {
			} catch (Exception e) {
				throw e;
			}
			ModelAndView mav = new ModelAndView("page-editor-crawler-preview-fetch-tickets");
			mav.addObject("tickets", ticketHitIndexer.getIndexedTickets());
			return mav;
		} catch (Exception e) {
			response.getOutputStream().write(("<font color='red'><b>Error: " + e.getMessage() + "</b></font>").getBytes());
			e.printStackTrace();
			return null;
		}
	}
	
	public ModelAndView loadCounterPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {		
		ModelAndView mav = new ModelAndView("page-view-crawler-counter");
		String strStartDate=request.getParameter("startDate");
		String strEndDate=request.getParameter("endDate");
		Date startDate=null;
		Date endDate=null;
		DateFormat startDateFormat=new SimpleDateFormat("MM/dd/yyyy");
		DateFormat endDateFormat =new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		try {
			if(strStartDate == null){
				strStartDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			startDate= startDateFormat.parse(strStartDate);
			if(strEndDate == null){
				strEndDate= startDateFormat.format(Calendar.getInstance().getTime());
			}
			endDate= endDateFormat.parse(strEndDate + " 23:59:59");
		}catch (ParseException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<CrawlerHistoryManagement> counter =DAORegistry.getCrawlerHistoryManagementDAO().getCountersOfAllSiteIds(startDate,endDate);
		mav.addObject("siteIds", counter);
		mav.addObject("startDate", strStartDate);
		mav.addObject("endDate", strEndDate);
		
		return mav;
	}
	
	public ModelAndView loadEditorCrawlErrorsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {		
		ModelAndView mav = new ModelAndView("page-editor-crawl-errors");
		// TODOJMS  See jsp
//		mav.addObject("failedCrawls", ticketListingCrawler.getFailedTicketListingCrawlsByEventId());
//		mav.addObject("ticketListingCrawler", ticketListingCrawler);
		return mav;
	}

	
	public ModelAndView loadCrawlerControlsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		String action = request.getParameter("action");
		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		String divideTimeByThreadCountString = request.getParameter("divideTimeByThreadCount");
		if (divideTimeByThreadCountString == null) {
			divideTimeByThreadCountString = preferenceManager.getPreferenceValue(username, "crawlerControlsDivideTimeByThreadCount");
		} else {
			preferenceManager.updatePreference(username, "crawlerControlsDivideTimeByThreadCount", divideTimeByThreadCountString);
		}
		
		boolean divideTimeByThreadCount = "1".equals(divideTimeByThreadCountString);		
		
		if (action == null) {
			ModelAndView mav = new ModelAndView("page-editor-crawler-controls");
			
			Collection<TicketListingCrawl> runningTicketListingCrawls = new ArrayList<TicketListingCrawl>();
			int disabledCrawlCount = 0;
			int readyCrawlCount = 0;

			Map<String, Long> readyTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> failedTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> runningTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> totalTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Date> leastRecentlyUpdatedCrawlDateBySiteId = new HashMap<String, Date>();
			Map<String, Date> mostRecentlyUpdatedCrawlDateBySiteId = new HashMap<String, Date>();
			
			for(String sid: Constants.getInstance().getExtendedSiteIds()) {
				runningTicketListingCrawlCountBySiteId.put(sid, 0L);
				readyTicketListingCrawlCountBySiteId.put(sid, 0L);
				totalTicketListingCrawlCountBySiteId.put(sid, 0L);
				failedTicketListingCrawlCountBySiteId.put(sid, 0L);
			}
									
			TicketListingCrawl leastRecentlyUpdatedCrawl = null;
			TicketListingCrawl mostRecentlyUpdatedCrawl = null;
			Date now = new Date();
			int failedTicketListingCrawlCount = 0;
			int totalTicketListingCrawlCount = 0;
			
			for (TicketListingCrawl crawl : DAORegistry.getTicketListingCrawlDAO().getAll()) {
				totalTicketListingCrawlCount++;
				if (!crawl.isEnabled()) {
					disabledCrawlCount++;
					continue;
				} 
				
				if (leastRecentlyUpdatedCrawl == null || leastRecentlyUpdatedCrawl.getStartCrawl() == null
						|| (crawl.getStartCrawl() != null && crawl.getStartCrawl().before(leastRecentlyUpdatedCrawl.getStartCrawl()))) {
					leastRecentlyUpdatedCrawl = crawl;
				}

				if (leastRecentlyUpdatedCrawlDateBySiteId.get(crawl.getSiteId()) == null
						|| (crawl.getStartCrawl() != null && crawl.getStartCrawl().before(leastRecentlyUpdatedCrawlDateBySiteId.get(crawl.getSiteId())))) {
					leastRecentlyUpdatedCrawlDateBySiteId.put(crawl.getSiteId(), crawl.getStartCrawl());
				}

				if (mostRecentlyUpdatedCrawl == null || mostRecentlyUpdatedCrawl.getStartCrawl() == null
						|| (crawl.getStartCrawl() != null && crawl.getStartCrawl().after(mostRecentlyUpdatedCrawl.getStartCrawl()))) {
					mostRecentlyUpdatedCrawl = crawl;
				}

				if (mostRecentlyUpdatedCrawlDateBySiteId.get(crawl.getSiteId()) == null
						|| (crawl.getStartCrawl() != null && crawl.getStartCrawl().after(mostRecentlyUpdatedCrawlDateBySiteId.get(crawl.getSiteId())))) {
					mostRecentlyUpdatedCrawlDateBySiteId.put(crawl.getSiteId(), crawl.getStartCrawl());
				}

				totalTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), totalTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
				
				if (crawl.getCrawlState().equals(CrawlState.RUNNING)) {
					runningTicketListingCrawls.add(crawl);
					runningTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), runningTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
				} else if (crawl.getCrawlState().isError()) {
					failedTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), failedTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
					failedTicketListingCrawlCount++;
				} else if (crawl.getCrawlState().equals(CrawlState.STOPPED)
						&& (crawl.getEndCrawl() == null
								|| (now.getTime() - crawl.getEndCrawl().getTime()) > crawl.getCrawlFrequency() * 1000L)) {
					readyTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), readyTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
					readyCrawlCount++;
				} 
			} 
			// TODOJMS See what you can do
			/*for (TicketListingCrawlStat stat: ticketListingCrawler.getTicketListingCrawlDispatcher().getStatMap().values()) {
				for (String siteId: totalTicketListingCrawlCountBySiteId.keySet()) {
					stat.getSiteStat(siteId).setTotalTicketListingCrawlCount(totalTicketListingCrawlCountBySiteId.get(siteId));
				}
			}
			
			List<TicketListingCrawlStat> stats = new ArrayList<TicketListingCrawlStat>(ticketListingCrawler.getTicketListingCrawlDispatcher().getStatMap().values());
			Collections.sort(stats, new Comparator<TicketListingCrawlStat>() {
				public int compare(TicketListingCrawlStat t1,
						TicketListingCrawlStat t2) {
					return t1.getNodeId().compareTo(t2.getNodeId());
				}
			});
			
			mav.addObject("stats", stats);
*/
			// TODOJMS See jsp
//			mav.addObject("activeTicketListingCrawlCountBySiteId", ticketListingCrawler.getActiveTicketListingCrawlCountBySiteId());
			
			mav.addObject("leastRecentlyUpdatedCrawlDateBySiteId", leastRecentlyUpdatedCrawlDateBySiteId);
			mav.addObject("mostRecentlyUpdatedCrawlDateBySiteId", mostRecentlyUpdatedCrawlDateBySiteId);

			mav.addObject("totalTicketListingCrawlCountBySiteId", totalTicketListingCrawlCountBySiteId);
			mav.addObject("failedTicketListingCrawlCountBySiteId", failedTicketListingCrawlCountBySiteId);
			mav.addObject("runningTicketListingCrawlCountBySiteId", runningTicketListingCrawlCountBySiteId);
						
			mav.addObject("readyTicketListingCrawlCountBySiteId", readyTicketListingCrawlCountBySiteId);
			mav.addObject("leastRecentlyUpdatedCrawl", leastRecentlyUpdatedCrawl);
			mav.addObject("mostRecentlyUpdatedCrawl", mostRecentlyUpdatedCrawl);

			/*
			Map<String, Double> averageProcessingTimeBySiteId = ticketListingCrawler.getStat().getAverageProcessingTimeBySiteId(); 
			Map<String, Double> averageProcessedTicketCountBySiteId = ticketListingCrawler.getStat().getAverageProcessedTicketCountBySiteId(); 
			Map<String, Double> averageFetchingTimeBySiteId = ticketListingCrawler.getStat().getAverageFetchingTimeBySiteId(); 
			Map<String, Double> averageExtractionTimeBySiteId = ticketListingCrawler.getStat().getAverageExtractionTimeBySiteId();
			Map<String, Double> averageIndexationTimeBySiteId = ticketListingCrawler.getStat().getAverageIndexationTimeBySiteId();
			Map<String, Double> averageExpirationTimeBySiteId = ticketListingCrawler.getStat().getAverageExpirationTimeBySiteId();
			Collection<CrawlerDelayStat> delayByFrequencyValues  = ticketListingCrawler.getStat().getDelayByFrequencyValues();

			mav.addObject("averageProcessingTimeBySiteId", averageProcessingTimeBySiteId);
			mav.addObject("averageProcessedTicketCountBySiteId", averageProcessedTicketCountBySiteId);
			mav.addObject("averageFetchingTimeBySiteId", averageFetchingTimeBySiteId);
			mav.addObject("averageExtractionTimeBySiteId", averageExtractionTimeBySiteId);
			mav.addObject("averageIndexationTimeBySiteId", averageIndexationTimeBySiteId);
			mav.addObject("averageExpirationTimeBySiteId", averageExpirationTimeBySiteId);
			mav.addObject("delayByFrequencyValues", delayByFrequencyValues);

			long timeToProcessAllCrawls = 0;
			double totalProcessedTicketCount = 0;
			long totalFetchingTime = 0;
			long totalExtractionTime = 0;
			long totalIndexationTime = 0;
			long totalExpirationTime = 0;
			for(String siteId: Constants.getInstance().getSiteIds()) {
				timeToProcessAllCrawls += averageProcessingTimeBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
				totalProcessedTicketCount += averageProcessedTicketCountBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
				totalFetchingTime += averageFetchingTimeBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
				totalExtractionTime += averageExtractionTimeBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
				totalIndexationTime += averageIndexationTimeBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
				totalExpirationTime += averageExpirationTimeBySiteId.get(siteId) * totalTicketListingCrawlCountBySiteId.get(siteId);
			}

			mav.addObject("averageProcessingTime", (double)timeToProcessAllCrawls / (totalTicketListingCrawlCount - disabledCrawlCount));
			mav.addObject("averageProcessedTicketCount", (double)totalProcessedTicketCount / (totalTicketListingCrawlCount - disabledCrawlCount));
			mav.addObject("averageFetchingTime", (double)totalFetchingTime / (totalTicketListingCrawlCount - disabledCrawlCount));
			mav.addObject("averageExtractionTime", (double)totalExtractionTime / (totalTicketListingCrawlCount - disabledCrawlCount));
			mav.addObject("averageIndexationTime", (double)totalIndexationTime / (totalTicketListingCrawlCount - disabledCrawlCount));
			mav.addObject("averageExpirationTime", (double)totalExpirationTime / (totalTicketListingCrawlCount - disabledCrawlCount));
*/
			mav.addObject("totalEnabledTicketListingCrawlCount", totalTicketListingCrawlCount - disabledCrawlCount);
			mav.addObject("totalTicketListingCrawlCount", totalTicketListingCrawlCount);
			mav.addObject("failedTicketListingCrawlCount", failedTicketListingCrawlCount);
			mav.addObject("readyTicketListingCrawlCount", readyCrawlCount);
			mav.addObject("disabledTicketListingCrawlCount", disabledCrawlCount);
			// TODOJSP see jsp
//			mav.addObject("ticketListingCrawler", ticketListingCrawler);
			mav.addObject("runningTicketListingCrawls", runningTicketListingCrawls);
			mav.addObject("divideTimeByThreadCount", divideTimeByThreadCount);

			return mav;
		} else if (action.equals("startCrawler")) {
			// TODOJMS see what can be done
//			ticketListingCrawler.start();
			
        	if (DAORegistry.getPropertyDAO().get("crawler.email.startStop.enabled").getValue().equals("1")) {
    			Map<String, Object> map = new HashMap<String, Object>();
    			map.put("username", username);
    			map.put("hostName", Constants.getInstance().getHostName());
    			map.put("hostAddress", Constants.getInstance().getIpAddress());
    			map.put("date", new Date().toString());
        		
				String mailTo = DAORegistry.getPropertyDAO().get("crawler.email.to").getValue();
				String subject = "[" + Constants.getInstance().getHostName() + "] Tixmarket - Crawl Start";
				SpringUtil.getMailManager().sendMail("TMAT Crawler", mailTo, subject, "mail-crawl-start.txt", map, "text/plain");
        	}
        	return new ModelAndView(new RedirectView("EditorCrawlerControls?info=The crawler has been started"));
		} else if (action.equals("stopCrawler")) {
			// TODOJMS see what can be done
//			ticketListingCrawler.stop();
			
        	if (DAORegistry.getPropertyDAO().get("crawler.email.startStop.enabled").getValue().equals("1")) {
    			Map<String, Object> map = new HashMap<String, Object>();
    			map.put("username", username);
    			map.put("hostName", Constants.getInstance().getHostName());
    			map.put("hostAddress", Constants.getInstance().getIpAddress());
    			map.put("date", new Date().toString());
        		
				String mailTo = DAORegistry.getPropertyDAO().get("crawler.email.to").getValue();
				String subject = "[" + Constants.getInstance().getHostName() + "] Tixmarket - Crawl Stop";
				SpringUtil.getMailManager().sendMail("TMAT Crawler", mailTo, subject, "mail-crawl-stop.txt", map, "text/plain");
        	}        	
        	return new ModelAndView(new RedirectView("EditorCrawlerControls?info=The crawler has been stopped"));
		} else if (action.equals("reset")) {
			// TODOJMS see what can be done
//			ticketListingCrawler.resetStats();
        	return new ModelAndView(new RedirectView("EditorCrawlerControls?info=The stats have been reset"));
		} else {
        	return new ModelAndView(new RedirectView("EditorCrawlerControls"));			
		}
	}

	public ModelAndView loadEditorAutoCrawlerCreation(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		
		Date startTime = new Date();
		long preProstime=0,actiontime=0,postProcesstime=0;
		String logLocation="",logKey="";
		
		String artistIdStr = request.getParameter("artistId");
		String eventIdStr = request.getParameter("eventId");
		String venueIdStr = request.getParameter("venueId");
		String keywords = request.getParameter("keywords");
		String action = request.getParameter("action");
		String location = request.getParameter("location");
		String selectedEventId=request.getParameter("selectedEventId");
		List<String> eventIdList= new ArrayList<String>();
		List<Event> eventList= new ArrayList<Event>();
		Map<Integer,Event> eventMap= new HashMap<Integer,Event>();
		if(selectedEventId!=null){
			for(String param:selectedEventId.split(",")){
				if(param==null || param.isEmpty()){
					continue;
				}
				eventIdList.add(param);
				Integer id= Integer.parseInt(param);
				Event event = DAORegistry.getEventDAO().get(id);
				eventList.add(event);
				eventMap.put(id, event);
			}
		}
		String[] preferenceSiteIds;
		
		String autoCrawlCreationSitesString = preferenceManager.getPreferenceValue(username, "autoCrawlCreationSites");
		if (autoCrawlCreationSitesString == null) {
			// check all the sites
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = autoCrawlCreationSitesString.split(",");
		}
		
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		for(String preferenceSiteId: preferenceSiteIds) {
			siteFilters.put(preferenceSiteId, true);
		}

		String[] siteIds = null;

		Integer artistId = null;
		Integer eventId = null;
		Integer venueId = null;
		
		//Tamil : select artist with minimum one valid events only
		//Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtistsWithEvents();
		//Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		
		//Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Collection<Venue> venues = new ArrayList<Venue>();
		Collection<Venue> listofVenue;
		ModelAndView mav = new ModelAndView("page-editor-auto-crawler-creation");
		//mav.addObject("venues", venues);
		String artistName = new String();
		boolean isVenue=false,getArtistFlag = false,getVenueFlag = false;
		List<Event> events = null;
		if (artistIdStr != null && !artistIdStr.isEmpty()&& venueIdStr!=null && !venueIdStr.isEmpty()){
			getArtistFlag = true;
			getVenueFlag = true;
			artistId = Integer.valueOf(artistIdStr);
			venueId= Integer.valueOf(venueIdStr);
			events = (List<Event>) DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId,venueId,EventStatus.ACTIVE);
		}else if (artistIdStr != null && !artistIdStr.isEmpty()) {
			getArtistFlag = true;
			artistId = Integer.valueOf(artistIdStr);
			venues=new ArrayList();
			events = new ArrayList<Event>(DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId));
			isVenue=false;
			for (Event event :events) {
				listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
				for (Venue venue : listofVenue) {
					if(!venues.contains(venue)){
						venues.add(venue);
						}
				}
                 // DAORegistry.getVenueDAO().getVenue(event.getVenue())
			}
			mav.addObject("venues", venues);
		}else if(venueIdStr!=null && !venueIdStr.isEmpty()){
			getVenueFlag = true;
			venueId= Integer.valueOf(venueIdStr);
			events = new ArrayList<Event>(DAORegistry.getEventDAO().getAllEventsByVenue(venueId));
			isVenue=true;
			Collections.sort(events, new Comparator<Event>() {
				public int compare(Event event1, Event event2) {
					return event1.getName().compareTo(event2.getName());
				}
			});
		}/*else{
			if(artists!=null){
				Artist artist = artists.iterator().next();
				artistId=  artist.getId();
				artistIdStr = artistId + "";
				events = new ArrayList<Event>(DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artist.getId()));
			}else{
				events = new ArrayList<Event>();
			}
			
			Collections.sort(events, new Comparator<Event>() {
				public int compare(Event event1, Event event2) {
					return event1.getName().compareTo(event2.getName());
				}
			});
		}*/
		
		if(getArtistFlag){
			artistId = Integer.valueOf(artistIdStr);
			venues=new ArrayList();
			Artist artist = DAORegistry.getArtistDAO().get(artistId);
			artistName = artist.getName();
		}
		
		if(getVenueFlag){
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			String venueName = venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry();
			mav.addObject("venueName", venueName);
		}
		
		Event event;
		if (events == null || events.isEmpty()){
			event = null;
			eventId = null;
		} else if (eventIdStr != null && !eventIdStr.isEmpty()) {
			eventId = Integer.valueOf(eventIdStr);

			event = DAORegistry.getEventDAO().get(eventId);
//			System.out.println(event.getTourId() + " " + tourId + " " + event.getName());
			if (!event.getArtistId().equals(artistId)) {
				event = events.iterator().next();
				eventId = event.getId();
			}
		} else {
			for(Event e:events){
				if(e.getLocalDate()!=null){
					eventId = e.getId();
					break;
				}
			}
			if(eventId == null){
				eventId = events.iterator().next().getId();
			}			
			event = DAORegistry.getEventDAO().get(eventId);
		}
		
		
		Date fromDate = null;
		Date toDate = null;
		
		String error = "";
						
		if (request.getParameter("fromDateYear") != null && action != null) {
			Calendar fromCal = new GregorianCalendar(Integer.valueOf(request.getParameter("fromDateYear")),
													Integer.valueOf(request.getParameter("fromDateMonth")) - 1,
													Integer.valueOf(request.getParameter("fromDateDay")),
													0, 0, 0);
			fromDate = fromCal.getTime();

			Calendar toCal = new GregorianCalendar(Integer.valueOf(request.getParameter("toDateYear")),
					Integer.valueOf(request.getParameter("toDateMonth")) - 1,
					Integer.valueOf(request.getParameter("toDateDay")),
					23, 59, 59);
			fromCal.setTimeZone(TimeZone.getTimeZone("EDT"));
			toDate = toCal.getTime();		
		} else if (event != null) {
			fromDate = new Date();//event.getDate(); 
			toDate = new Date();//event.getDate();
		}
		preProstime = new Date().getTime()-startTime.getTime();
		
		if (action == null) {
			
		} else if (action.equals("Delete")) {
			for (Object obj: request.getParameterMap().keySet()) {
				String key = (String)obj;
				if (key.indexOf("crawler-") >= 0) {
//					String value = request.getParameter(key);
					Integer crawlerId = Integer.valueOf(key.replaceAll("crawler-", ""));
//					TicketListingCrawl ticketListingCrawl = DAORegistry.getTicketListingCrawlDAO().get(crawlerId);
					// TODOJMS 
//					ticketListingCrawler.removeTicketListingCrawl(ticketListingCrawl);
					DAORegistry.getTicketListingCrawlDAO().deleteById(crawlerId);
				}
			}
		} else if (action.equals("Search")) {
			Collection<String> siteList = new ArrayList<String>();
			for(String siteId: Constants.getInstance().getSiteIds()) {
				String value = request.getParameter("checkbox-" + siteId);
				if (value != null && !value.isEmpty()) {
					siteFilters.put(siteId, true);
					siteList.add(siteId);
				} else {
					siteFilters.remove(siteId);
				}
			}
			siteIds = siteList.toArray(new String[1]);
			//Commented  if for bull ACC 
//			if (toDate.getTime() - fromDate.getTime() < 0
//				|| toDate.getTime() - fromDate.getTime() > MAX_DATE_PERIOD) {
//				mav.addObject("dateError", "The date period must be less than " + MAX_NUM_DAY_PERIOD + " days");
//			} else {
			
			Boolean istubhubFromTicketEvolution = false;
			String istubhubFromTicketEvolutionStr = request.getParameter("isStubhubFromTicketEvolution");
			if(istubhubFromTicketEvolutionStr != null && istubhubFromTicketEvolutionStr.equalsIgnoreCase("on")) {
				istubhubFromTicketEvolution = true;
			}
			mav.addObject("isStubhubFromTicketEvolution", istubhubFromTicketEvolution);
			
				Collection<EventListingResult> eventListingResults = eventListingManager.search(keywords, siteIds, location, fromDate, toDate,eventList,isVenue,istubhubFromTicketEvolution);
				mav.addObject("eventListingResults", eventListingResults);
								
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
				System.out.println("EVENT RESULTS LEN=" + eventListingResults.size());
//				List<EventHit> probableEvents = new ArrayList<EventHit>();
				List<EventHit> additionalEvents = new ArrayList<EventHit>();

				//Tamil : Get all crawls with url to check whether crawls already exist
				Map<String,Integer> crawlsUrlwithEventap = DAORegistry.getQueryManagerDAO().getTicketListingCrawlsQueryUrlMap();
												
				for(EventListingResult result: eventListingResults) {
					if (result.getEventHits() != null) {
						System.out.println("EVEN LEN=" + result.getEventHits().size());
						for(EventHit eventhit:result.getEventHits()){
//							if(eventhit.getUrl().toLowerCase().contains("ticketexchangebyticketmaster")){
//								eventhit.setCrawlState(CrawlState.RESTRICTED);	
//							}
							eventhit.setExistingCrawlId(crawlsUrlwithEventap.get(eventhit.getUrl()));
							if(eventhit.getBestMatchEventId()!=null){
								eventHits.add(eventhit);
							}else{
								additionalEvents.add(eventhit);
							}
						}
//						eventHits.addAll(result.getEventHits());
					}
				}
				mav.addObject("eventHits", eventHits);
				mav.addObject("additionalEventHits", additionalEvents);
//			}
		} else if (action.equals("Create")) {
			
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls=";
			String crawlerId="";
			for (Object obj: request.getParameterMap().keySet()) {
				String key = (String)obj;
				Integer assignEventId=0;
				if (key.indexOf("eventHit-") >= 0) {
					String eventHitId = key.replaceAll("eventHit-", "");
					String assignEventIdStr=request.getParameter("event-"+ eventHitId);
					
					if(assignEventIdStr!=null){
						assignEventId= Integer.parseInt(assignEventIdStr);
					}else{
						continue;
					}
					EventHit eventHit = eventListingManager.getEventHit(eventHitId);
					System.out.println("eventhit: " + eventHit);
					if (eventHit != null) {
						Event tevent= eventMap.get(assignEventId);
						TicketListingCrawl ticketListingCrawl = new TicketListingCrawl(eventHit, tevent, username);
						// TODOJMS
						boolean flag=true;
						List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(tevent.getId());
						for(TicketListingCrawl crawl:crawls){
							if(crawl.isEquivalent(ticketListingCrawl)){
								flag=false;
							}
						}
//						if (!ticketListingCrawler.existTicketListingCrawl(ticketListingCrawl)) {
						if(flag){  // Just to avoid above code
//							String location = request.getParameter("location");
							if(eventHit.getId().contains(Site.AOP)){
								Map<String, String> map =new HashMap<String, String>();
								map.put("keywords", eventHit.getName().replace("Tickets", "").trim() + "+" + location);
								map.put("date", String.valueOf(eventHit.getDate().getTime()));
								ticketListingCrawl.setExtraParameterMap(map);
							}
							DAORegistry.getTicketListingCrawlDAO().save(ticketListingCrawl);
							crawlerId+=ticketListingCrawl.getId()+",";
							url+=ticketListingCrawl.getId()+",";
//							ticketListingCrawler.addTicketListingCrawl(ticketListingCrawl);
							creationEventListManager.addSitesInSitesPerEventMap(assignEventId, ticketListingCrawl.getSiteId());
						} else {
							error += "Cannot create crawl for " + eventHit.getName() + " " + eventHit.getLocation() + " because it already exists!<br />";
						}
					}
				}
				
			}
			try {
				System.out.println("TMAT CRL START :"+url+" : "+new Date()+" : "+username);
				url=url.substring(0,url.length()-1);
				/*HttpClient client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				System.out.println(client.execute(get));*/
				
				SimpleHttpClient client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				System.out.println("TMAT CRL EXE: "+client.executeWithOneHit(get,true));
			} catch (Exception e) {
				System.out.println("TMAT CRL ERROR : "+url);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		actiontime = new Date().getTime()-(startTime.getTime()+preProstime);
		// site ids
		String autoCrawlCreationSiteIdsString = "";
		
		for (String siteId: Constants.getInstance().getSiteIds()) {
			if (siteFilters.get(siteId) != null && siteFilters.get(siteId)) {
				autoCrawlCreationSiteIdsString += "," + siteId;
			}
		}
		if (!autoCrawlCreationSiteIdsString.isEmpty()) {
			autoCrawlCreationSiteIdsString = autoCrawlCreationSiteIdsString.substring(1);
		}
		if(events!=null){
			for(Event tempEvent:events){
				Collection<String>sites=creationEventListManager.getSitesPerEventMapByEventId(tempEvent.getId());
				/*Collection<String>sites=creationEventListManager.getSitesPerEventMapByEventId(tempEvent.getId());
				Collection<String> enabled= new ArrayList<String>();
				Collection<String> disabled= new ArrayList<String>();
				if(sites!=null){
					for(String site:sites){
						if(site.split("-")[1].equals("1")){
							enabled.add(site);
						}else{
							disabled.add(site);
						}
					}
				}*/
				
				Collection<TicketListingCrawl> crawls =DAORegistry.getTicketListingCrawlDAO().getAllTicketListingCrawlByEvent(tempEvent.getId());
				Collection<String> enabled= new ArrayList<String>();
				Collection<String> disabled= new ArrayList<String>();
				if(crawls!=null){
					for(TicketListingCrawl crawl : crawls){
						if(crawl.isEnabled()){
							enabled.add(crawl.getSiteId());
						}else{
							disabled.add(crawl.getSiteId());
						}
					}
				}
				tempEvent.setEnabledCrawlSites(enabled);
				tempEvent.setDisabledCrawlSites(disabled);
			}
		}
		
		preferenceManager.updatePreference(username, "autoCrawlCreationSites", autoCrawlCreationSiteIdsString);
		
		if (!error.isEmpty()) {
			mav.addObject("error", error);
		}
		if(keywords ==null || keywords.isEmpty()){
			if(isVenue){
				mav.addObject("keywords", event==null?"":event.getVenue().getBuilding());
			}else{
				mav.addObject("keywords", event==null?"":event.getArtist().getName());
			}
		}else{
			mav.addObject("keywords", keywords);
		}
		mav.addObject("selectedEvents", eventList);
		mav.addObject("selectedEventId", selectedEventId);
		mav.addObject("eventIdList", eventIdList);
		mav.addObject("siteFilters", siteFilters);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		mav.addObject("event", event);
		//mav.addObject("artists", artists);
		mav.addObject("artistId", artistIdStr);
		mav.addObject("venueId", venueIdStr);
		mav.addObject("artistName", artistName);
		if(eventId!=null){
			mav.addObject("marketMaker", DAORegistry.getMarketMakerEventTrackingDAO().getCurrentMarketMakerForEvent(eventId));
		}
		mav.addObject("marketMakers", DAORegistry.getUserDAO().getAll());
		//mav.addObject("crawlers", DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(eventId));
		mav.addObject("events", events);
		
		postProcesstime = new Date().getTime()-(startTime.getTime()+preProstime+actiontime);
		logger.info("EVENT SEARCH CRL CTRL : "+action+" :fnlTime:"+(new Date().getTime()-startTime.getTime())+" pre:"+preProstime+" :act:"+actiontime+" :"+postProcesstime+" : key:"+keywords+" :loc:"+location);
		return mav;		
	}
	
	public ModelAndView loadTicketListingCrawlPopup(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		
		ModelAndView mav = new ModelAndView("page-ticket-listing-crawl-popup");
	
		Integer crawlId = Integer.parseInt(request.getParameter("id"));
		TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
		mav.addObject("crawl", crawl);
		return mav;
	}
	
	/**
	 * This page is useless now as we are not using the seatwave ftp feeds.
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@Deprecated
	public ModelAndView loadSeatWaveFeedCrawlEventMappingPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		
		ModelAndView mav = new ModelAndView("page-editor-seatwave-feed-crawl-event-mapping");
		
		TicketListingCrawl crawl = new TicketListingCrawl();
		crawl.setSiteId(Site.SEATWAVE_FEED);
		
		SeatWaveFeedTicketListingFetcher fetcher = new SeatWaveFeedTicketListingFetcher();
		PreviewTicketHitIndexer ticketHitIndexer = new PreviewTicketHitIndexer(null);		

		String errorMessage = null;
		
		try {
			fetcher.fetchTicketListing(ticketHitIndexer, crawl);
			mav.addObject("eventMappings", fetcher.getLastEventMappings());
		} catch (Exception e) {
			e.printStackTrace();
			errorMessage = e.getMessage();
		}
		
		mav.addObject("errorMessage", errorMessage);
				
		return mav;
	}
	

	public ModelAndView loadEditCrawlerSitesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		
		Map<String, Site> siteById = DAORegistry.getSiteDAO().getSiteByIdMap();
		
		if (action != null && action.equals("update")) {
			
//			Collection<Event> events = DAORegistry.getEventDAO().getAll();			
			for (String siteId: Constants.getInstance().getSiteIds()) {
				Site site = siteById.get(siteId);
				boolean createSite = (site == null);
				
				if (createSite) {
					site = new Site(siteId);					
				}
				
				int maxConcurrentConnections = Integer.parseInt(request.getParameter("maxConcurrentConnections_" + siteId));
				site.setMaxConcurrentConnections(maxConcurrentConnections);
									
				if (createSite) {
					DAORegistry.getSiteDAO().save(site);
				} else {
					DAORegistry.getSiteDAO().update(site);					
				}
			}
			
			String backUrl = request.getParameter("backUrl");
			if (backUrl == null || backUrl.length() == 0) {
				return new ModelAndView(new RedirectView("EditorEditCrawlerSites?info=The site settings have been updated"));
			} else {
				String url;
				if (backUrl.contains("?")) {
					url = backUrl + "&info=The site settings have been updated";
				} else {
					url = backUrl + "?info=The site settings have been updated";					
				}
				return new ModelAndView(new RedirectView(url));				
			}
		}
		
		ModelAndView mav = new ModelAndView("page-edit-crawler-sites");
		mav.addObject("siteById", siteById);
		return mav;
	}

	public ModelAndView loadEditorSuperCrawlFinish(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		Boolean includeExistingEvents= Boolean.parseBoolean(request.getParameter("includeExistingEvents"));
		Boolean includeStubhubEvents= Boolean.parseBoolean(request.getParameter("includeStubhubEvents"));
		Boolean includeSeatGeekEvents= Boolean.parseBoolean(request.getParameter("includeSeatGeekEvents"));
		Boolean includeSeatwaveEvents= Boolean.parseBoolean(request.getParameter("includeSeatwaveEvents"));
		Boolean includeTicketNetworkEvents= Boolean.parseBoolean(request.getParameter("includeTicketNetworkEvents"));
		Boolean includeTicketNetworkDataFeedEvents= Boolean.parseBoolean(request.getParameter("includeTicketNetworkDataFeedEvents"));
		Boolean includeTicketNetworkPOSEvents = Boolean.parseBoolean(request.getParameter("includeTicketNetworkPOSEvents"));
		if (includeExistingEvents == null) {
			includeExistingEvents = false;
		}

		if (includeStubhubEvents == null) {
			includeStubhubEvents = false;
		}
		
		if (includeSeatGeekEvents == null) {
			includeSeatGeekEvents = false;
		}

		if (includeSeatwaveEvents == null) {
			includeSeatwaveEvents = false;
		}
		if (includeTicketNetworkEvents == null) {
			includeTicketNetworkEvents = false;
		}
		if (includeTicketNetworkDataFeedEvents == null) {
			includeTicketNetworkDataFeedEvents = false;
		}
		
		if (includeTicketNetworkPOSEvents == null) {
			includeTicketNetworkPOSEvents = false;
		}
		
		Collection<AutoEventListingResult> autoEventListingResults = new ArrayList<AutoEventListingResult>();
		for (Entry<String, String[]> entry: (Collection<Entry<String, String[]>>)request.getParameterMap().entrySet()) {
			String key = entry.getKey();
			if (key.indexOf("event-") < 0) {
				continue;
			}
			String id = key.replaceAll("event-", "");
			AutoEventListingResult result = creationEventListManager.getAutoEventListingResult(Integer.valueOf(id));
			if (result == null) {
				continue;
			}
			
			
			autoEventListingResults.add(result);
			
			logger.info("Creating event for " + result.getName() + " " + result.getArtistName());
		}
		creationEventListManager.createEventsFromAutoResults(username, autoEventListingResults,includeExistingEvents,includeStubhubEvents,includeSeatwaveEvents,includeTicketNetworkEvents,includeTicketNetworkDataFeedEvents,includeTicketNetworkPOSEvents,CreationType.SEMIAUTO);

		ModelAndView mav = loadEditorSuperCrawlCreation(request, response);
//		mav.addObject("eventHits", eventHits);
		mav.addObject("results", autoEventListingResults);
		return mav;
	}
	
	public ModelAndView loadEditorSuperCrawlConfirmation(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Collection<Integer> eventIdList = new ArrayList<Integer>();
//		Collection<String> artistList = new ArrayList<String>();
		Map<Integer,String> venueMap = new HashMap<Integer, String>();
		Map<Integer,String> venueCategoryMap = new HashMap<Integer, String>();
//		String stringAutomated= request.getParameter("automated");
//		boolean automated=false; 
//		if(stringAutomated!=null){
//			automated =true;
//		}
		for (Entry<String, String[]> entry: (Collection<Entry<String, String[]>>)request.getParameterMap().entrySet()) {
			String key = entry.getKey();
			if (key.indexOf("event-") < 0) {
				continue;
			}
			Integer id = Integer.valueOf(key.replaceAll("event-", ""));
			eventIdList.add(id);
//			if(!automated){
//				String artistName=request.getParameter("selectedArtist-" + id).trim();
//				if(!artistList.contains(artistName)){
//					artistList.add(artistName);
//				}
//			}
			String venue=request.getParameter("selectedVenue-" + id).trim();
			venueMap.put(id, venue);
			String venueCategoryName=request.getParameter("selectedVenueCategory-" + id).trim();
            venueCategoryMap.put(id, venueCategoryName.replaceAll(id+"-", ""));
			}
		
		Map<String, Boolean> siteMap = getSiteMap(request, response);
		Collection<String> siteList = new ArrayList<String>();
		for (Entry<String, Boolean> entry: siteMap.entrySet()) {
			String siteId = entry.getKey();
			Boolean value = entry.getValue();
			if (value) {
				siteList.add(siteId);
			}
		}
		Boolean includeExistingEvents = Boolean.parseBoolean(request
				.getParameter("includeExistingEventsStr"));
		Boolean includeStubhubEvents = Boolean.parseBoolean(request
				.getParameter("includeStubhubEventsStr"));
		Boolean includeSeatGeekEvents = Boolean.parseBoolean(request
				.getParameter("includeSeatGeekEventsStr"));
		Boolean includeSeatwaveEvents = Boolean.parseBoolean(request
				.getParameter("includeSeatwaveEventsStr"));
		Boolean includeTicketNetworkEvents = Boolean.parseBoolean(request
				.getParameter("includeTicketNetworkEventsStr"));
		Boolean includeTicketNetworkDataFeedEvents = Boolean
				.parseBoolean(request
						.getParameter("includeTicketNetworkDataFeedEventsStr"));
		Boolean includeTicketNetworkPOSEvents = Boolean.parseBoolean(request
				.getParameter("includeTicketNetworkPOSEventsStr"));
		if (includeExistingEvents == null) {
			includeExistingEvents = false;
		}

		if (includeStubhubEvents == null) {
			includeStubhubEvents = false;
		}
		if (includeSeatGeekEvents == null) {
			includeSeatGeekEvents = false;
		}

		if (includeSeatwaveEvents == null) {
			includeSeatwaveEvents = false;
		}
		if (includeTicketNetworkEvents == null) {
			includeTicketNetworkEvents = false;
		}
		if (includeTicketNetworkDataFeedEvents == null) {
			includeTicketNetworkDataFeedEvents = false;
		}
		if (includeTicketNetworkPOSEvents == null) {
			includeTicketNetworkPOSEvents = false;
		}

		String[] siteIds = siteList.toArray(new String[1]);
		Integer[] eventIds = new Integer[eventIdList.size()];
//		String[] tourNameArray = new String[tourNameList.size()==0?1:tourNameList.size()];
		eventIdList.toArray(eventIds);
//		tourNameList.toArray(tourNameArray);
		//fixme consider only sites which have been ticked
		ModelAndView mav = new ModelAndView("page-editor-super-crawl-confirmation");
		mav.addObject("includeExistingEvents", includeExistingEvents);
		mav.addObject("includeStubhubEvents", includeStubhubEvents);
		mav.addObject("includeTicketNetworkEvents", includeTicketNetworkEvents);
		mav.addObject("includeSeatwaveEvents", includeSeatwaveEvents);
		//mav.addObject("includeSeatwaveEvents", includeSeatwaveEvents);
		mav.addObject("includeTicketNetworkEvents", includeTicketNetworkEvents);
		mav.addObject("includeSeatGeekEvents", includeSeatGeekEvents);
		mav.addObject("includeTicketNetworkDataFeedEvents",
				includeTicketNetworkDataFeedEvents);
		mav.addObject("includeTicketNetworkPOSEvents",
				includeTicketNetworkPOSEvents);
		mav.addObject("eventListingResults", creationEventListManager
				.getEventListingResults(eventIds, siteIds, /*tourNameArray,*/
						false/* includeExistingEvents */, includeStubhubEvents,
						includeSeatwaveEvents, includeTicketNetworkEvents,
						includeTicketNetworkDataFeedEvents,
						includeTicketNetworkPOSEvents, venueMap,
						venueCategoryMap));
		return mav;
	}
	
	public ModelAndView loadEditorSuperCrawlCreation(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String tourTypeStr = request.getParameter("tourType");
		String eventFilter = request.getParameter("eventFilter");
		String locationFilter = request.getParameter("locationFilter");

		String includeExistingEventsStr = request
				.getParameter("includeExistingEvents");
		String includeTicketNetworkEventsStr = request
				.getParameter("includeTicketNetworkEvents");
		String includeStubhubEventsStr = request
				.getParameter("includeStubhubEvents");
		String includeSeatwaveEventsStr = request
				.getParameter("includeSeatwaveEvents");
		String includeStubhubDuplicatedEventsStr = request
				.getParameter("includeStubhubDuplicatedEvents");
		String includeTicketNetworkDataFeedEventsStr = request
				.getParameter("includeTicketNetworkDataFeedEvents");
		String includeTicketNetworkPOSEventsStr = request
				.getParameter("includeTicketNetworkPOSEvents");
		String includeSeatGeekEventsStr = request
		.getParameter("includeSeataGeekEvents");
		String hideEventsWithACrawlStr = request
				.getParameter("hideEventsWithACrawl");

		String fromYearStr = request.getParameter("fromYear");
		String fromMonthStr = request.getParameter("fromMonth");
		String fromDayStr = request.getParameter("fromDay");

		String toYearStr = request.getParameter("toYear");
		String toMonthStr = request.getParameter("toMonth");
		String toDayStr = request.getParameter("toDay");
		
		String matchAllStr = request.getParameter("matchAll");
		String locationMatchAllStr = request.getParameter("locationMatchAll");
		boolean matchAll = false;
		if (matchAllStr != null) {
			matchAll = true;
		}

		boolean locationMatchAll = false;
		if (locationMatchAllStr != null) {
			locationMatchAll = true;
		}

		Date fromDate = null;
		Date toDate = null;

		ModelAndView mav = new ModelAndView("page-editor-super-crawl-creation");
		Map<String, Boolean> siteMap = getSiteMap(request, response);
		Collection<String> siteList = new ArrayList<String>();
		for (Entry<String, Boolean> entry : siteMap.entrySet()) {
			String siteId = entry.getKey();
			Boolean value = entry.getValue();
			if (value) {
				siteList.add(siteId);
			}
		}

		// Temp fix for EBAY crawl length
//		siteMap.remove("ebay");

		// String[] siteIds = siteList.toArray(new String[1]);

		if (fromYearStr != null) {
			Calendar fromCal = new GregorianCalendar(
					Integer.valueOf(fromYearStr),
					Integer.valueOf(fromMonthStr) - 1,
					Integer.valueOf(fromDayStr));
			fromDate = fromCal.getTime();

			Calendar toCal = new GregorianCalendar(Integer.valueOf(toYearStr),
					Integer.valueOf(toMonthStr) - 1, Integer.valueOf(toDayStr));
			toDate = toCal.getTime();
		} else {
			Calendar fromCal = new GregorianCalendar();
			fromDate = fromCal.getTime();

			Calendar toCal = new GregorianCalendar();
			toCal.add(Calendar.MONTH, 2);
			toDate = toCal.getTime();
		}

		boolean includeExistingEvents = false;
		boolean includeStubhubEvents = false;
		boolean includeStubhubDuplicatedEvents = false;
		boolean includeSeatwaveEvents = false;
		boolean includeTicketNetworkEvents = false;
		boolean hideEventsWithACrawl = false;
		boolean includeTicketNetworkDataFeedEvents = false;
		boolean includeTicketNetworkPOSEvents = false;
		boolean includeSeatGeekEvents=false;
		if (action != null) {
			if (includeExistingEventsStr != null) {
				includeExistingEvents = true;
			}
			if (includeTicketNetworkEventsStr != null) {
				includeTicketNetworkEvents = true;
			}
			
			if (includeSeatGeekEventsStr != null) {
				includeTicketNetworkEvents = true;
			}
			if (includeStubhubEventsStr != null) {
				includeStubhubEvents = true;
			}

			if (includeSeatwaveEventsStr != null) {
				includeSeatwaveEvents = true;
			}

			if (includeStubhubDuplicatedEventsStr != null) {
				includeStubhubDuplicatedEvents = true;
			}

			if (hideEventsWithACrawlStr != null) {
				hideEventsWithACrawl = true;
			}
			if (includeTicketNetworkDataFeedEventsStr != null) {
				includeTicketNetworkDataFeedEvents = true;
			}

			if (includeTicketNetworkPOSEventsStr != null) {
				includeTicketNetworkPOSEvents = true;
			}

		} else {
			includeExistingEvents = false;
			includeStubhubEvents = false;
			includeSeatwaveEvents = false;
			includeSeatGeekEvents=false;
			includeTicketNetworkEvents = false;
			includeTicketNetworkDataFeedEvents = false;
			includeTicketNetworkPOSEvents = false;
			hideEventsWithACrawl = true;
		}

		TourType tourType = null;
		if (tourTypeStr != null && tourTypeStr.length() > 0) {
			tourType = TourType.valueOf(tourTypeStr);
		} else {
			tourType = null;
		}
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Collection<CreationEventHit> eventHits = creationEventListManager
				.getEventHits(tourType, eventFilter, locationFilter,
						includeExistingEvents, includeStubhubEvents,
						includeStubhubDuplicatedEvents, includeSeatwaveEvents,
						includeTicketNetworkEvents,
						includeTicketNetworkDataFeedEvents,
						includeTicketNetworkPOSEvents, hideEventsWithACrawl,
						fromDate, toDate, matchAll, locationMatchAll,/* artists,*/
						venues);
		// List<CreationEventHit> eventHitsList=new
		// ArrayList<CreationEventHit>(eventHits);
		// List<Artist> toursList = new ArrayList<Artist>(artists);
		mav.addObject("artists", artists);
		mav.addObject("venues", venues);
		mav.addObject("fromDate", fromDate);
		mav.addObject("toDate", toDate);
		// mav.addObject("eventHits", eventHitsList);
		mav.addObject("eventHits", eventHits);
		mav.addObject("siteFilters", siteMap);
		mav.addObject("includeExistingEvents", includeExistingEvents);
		mav.addObject("includeStubhubEvents", includeStubhubEvents);
		mav.addObject("includeTicketNetworkEvents", includeTicketNetworkEvents);
		mav.addObject("includeSeatwaveEvents", includeSeatwaveEvents);
		mav.addObject("includeStubhubDuplicatedEvents",
				includeStubhubDuplicatedEvents);
		mav.addObject("includeTicketNetworkDataFeedEvents",
				includeTicketNetworkDataFeedEvents);
		mav.addObject("includeTicketNetworkPOSEvents",
				includeTicketNetworkPOSEvents);
		mav.addObject("includeSeatGeekEvents",
				includeSeatGeekEvents);
		mav.addObject("hideEventsWithACrawl", hideEventsWithACrawl);
		mav.addObject("includeExistingEvents", includeExistingEvents);
		mav.addObject("includeStubhubEvents", includeStubhubEvents);
		mav.addObject("includeSeatwaveEvents", includeSeatwaveEvents);
		return mav;
	}

	public void getVenueCategoryByVenue(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String venueId = request.getParameter("venueId");
		String result = "";
		ServletOutputStream out = response.getOutputStream();
		if (venueId != null && !venueId.isEmpty()) {
			List<VenueCategory> list = DAORegistry.getVenueCategoryDAO()
					.getVenueCategoriesByVenueId(Integer.parseInt(venueId));
			if (list != null) {

				for (VenueCategory venueCategory : list) {
					result += venueCategory.getCategoryGroup() + "-"
							+ venueCategory.getId() + ",";
				}
			}
		}
	//	System.out.println(result);
		out.write(result.getBytes());
	}
	private Map<String, Boolean> getSiteMap(HttpServletRequest request, HttpServletResponse response) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		String[] preferenceSiteIds;
		
		String autoCrawlCreationSitesString = preferenceManager.getPreferenceValue(username, "autoCrawlCreationSites");
		if (autoCrawlCreationSitesString == null) {
			// check all the sites
			preferenceSiteIds = Constants.getInstance().getSiteIds();
		} else {
			preferenceSiteIds = autoCrawlCreationSitesString.split(",");
		}
		
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		for(String preferenceSiteId: preferenceSiteIds) {
			siteFilters.put(preferenceSiteId, true);
		}

		if (request.getParameter("action") != null) {
			Collection<String> siteList = new ArrayList<String>();
			for(String siteId: Constants.getInstance().getSiteIds()) {
				String value = request.getParameter("site-" + siteId);
				if (value != null && !value.isEmpty()) {
					siteFilters.put(siteId, true);
					siteList.add(siteId);
				} else {
					siteFilters.remove(siteId);
				}
			}
		}

		// site ids
		String autoCrawlCreationSiteIdsString = "";
		
		for (String siteId: Constants.getInstance().getSiteIds()) {
			if (siteFilters.get(siteId) != null && siteFilters.get(siteId)) {
				autoCrawlCreationSiteIdsString += "," + siteId;
			}
		}
		
		if (!autoCrawlCreationSiteIdsString.isEmpty()) {
			autoCrawlCreationSiteIdsString = autoCrawlCreationSiteIdsString.substring(1);
		}

		preferenceManager.updatePreference(username, "autoCrawlCreationSites", autoCrawlCreationSiteIdsString);
		
		return siteFilters;
	}

	/**
	 * Events with two or less than 2 crawls report
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void downloadEventWithLessCrawls(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try{
		response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; fileName=EventsWithTwoOrLessThanTwoCrawls.csv");
		ServletOutputStream out = response.getOutputStream();

		StringBuffer sb = new StringBuffer();
		sb.append("EventId,EventName,EventDate,EventTime,Venue,City,State,Country,Artist,CrawlCount,EventType,EventCreatedFrom(In days),Ticketnetwork,Stubhub,Ticketevlotion,Ticketsnow" +
				",Vividseat,Seatgeek,FlashSeats \n");
		
		String isLatestEventStr = request.getParameter("latestEvent");
		Boolean isLatestEventOnly = false;
		if(null != isLatestEventStr && isLatestEventStr.equals("true")) {
			isLatestEventOnly = true;			
		} 
		List<EventCrawlerDetails> eventCrawlList = DAORegistry.getQueryManagerDAO().getEventsWithLessCrawlCount(isLatestEventOnly);	
		
		if(null != eventCrawlList && !eventCrawlList.isEmpty()) {
			Map<Integer,EventCrawlerDetails> eventCrawlMap = new HashMap<Integer, EventCrawlerDetails>();
			EventCrawlerDetails eventCrawl = null;
			
			for (EventCrawlerDetails eventCrawlerDetails : eventCrawlList) {
				eventCrawl =eventCrawlMap.get(eventCrawlerDetails.getEventId());
				
				if(eventCrawl != null) {
					eventCrawl.setCrawlCount(eventCrawl.getCrawlCount()+1);
					eventCrawl.setActiveCrawlSites(eventCrawl.getActiveCrawlSites()+"-"+eventCrawlerDetails.getSiteId());
					
					eventCrawlMap.put(eventCrawl.getEventId(),eventCrawl);
				} else {
					eventCrawlerDetails.setCrawlCount(1);
					eventCrawlerDetails.setActiveCrawlSites(eventCrawlerDetails.getSiteId());
					
					eventCrawlMap.put(eventCrawlerDetails.getEventId(),eventCrawlerDetails);
				}
			}
			
			for (EventCrawlerDetails event : eventCrawlMap.values()) {
				
				 SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				  Date date=format.parse(event.getEventCreatedDate());
				  Long diff=new Date().getTime()-date.getTime();
				  
				sb.append(event.getEventId() + "," + event.getEventName().replaceAll(",", "-") + "," + event.getEventDate()+ "," + event.getEventTime() + "," + 
					event.getVenueName().replaceAll(",", "-") +","+ (null !=event.getCity()?event.getCity().replaceAll(",", "-"):"") +","+ 
					(null !=event.getState()?event.getState().replaceAll(",", "-"):"") +","+ 
					(null !=event.getCountry()?event.getCountry().replaceAll(",", "-"):"")+","+ 
					event.getTourName().replaceAll(",", "-") +","+ event.getCrawlCount() +","+
					event.getParentCategory()+","+(diff / (1000 * 60 * 60 * 24))+",");
					
				 
				  
					String[] siteYesOrNo = new String[7];
				
					for(String str1 : event.getActiveCrawlSites().split("-")){
						if(str1.equalsIgnoreCase("ticketnetworkdirect")){
							siteYesOrNo[0] = "Y";
						}else{
							if(siteYesOrNo[0] == null)	siteYesOrNo[0] = "N";
						}
						
						if(str1.equalsIgnoreCase("stubhub")){
							siteYesOrNo[1] = "Y";
						}else{
							if(siteYesOrNo[1] == null) siteYesOrNo[1] = "N";
						}
						
						if(str1.equalsIgnoreCase("ticketevolution")){
							siteYesOrNo[2] = "Y";
						}else{
							if(siteYesOrNo[2] == null) siteYesOrNo[2] = "N";
						}
						
						if(str1.equalsIgnoreCase("ticketsnow")){
							siteYesOrNo[3] = "Y";
						}else{
							if(siteYesOrNo[3] == null) siteYesOrNo[3] = "N";
						}
						
						if(str1.equalsIgnoreCase("vividseat")){
							siteYesOrNo[4] = "Y";
						}else{
							if(siteYesOrNo[4] == null) siteYesOrNo[4] = "N";
						}
						
						if(str1.equalsIgnoreCase("seatgeek")){
							siteYesOrNo[5] = "Y";
						}else{
							if(siteYesOrNo[5] == null) siteYesOrNo[5] = "N";
						}
						
						//For flashseats
						if(str1.equalsIgnoreCase("flashseats")){
							siteYesOrNo[6] = "Y";
						}else{
							if(siteYesOrNo[6] == null) siteYesOrNo[6] = "N";
						}
					}
					
					for(String str : siteYesOrNo){
						sb.append(str + ",");
					}
					
					sb.append("\n");
			}
		}
		
		out.write(sb.toString().getBytes());

		out.flush();
        out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
/*	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(
			TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}*/
	
	public EventListingManager getEventListingManager() {
		return eventListingManager;
	}

	public void setEventListingManager(EventListingManager eventListingManager) {
		this.eventListingManager = eventListingManager;
	}

	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}

	public CreationEventListManager getcreationEventListManager() {
		return creationEventListManager;
	}

	public void setcreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}

	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}

	/*public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}*/
	
	public void afterPropertiesSet() throws Exception {
		if (creationEventListManager == null) {
			throw new RuntimeException("Parameter creationEventListManager must be set");
		}

		if (preferenceManager == null) {
			throw new RuntimeException("Parameter preferenceManager must be set");
		}

//		if (ticketListingCrawler == null) {
//			throw new RuntimeException("Parameter ticketListingCrawler must be set");
//		}

		if (eventListingManager == null) {
			throw new RuntimeException("Parameter eventListingManager must be set");
		}

/*		if (crawlerSchedulerManager == null) {
			throw new RuntimeException("Parameter crawlerSchedulerManager must be set");
		}*/

	}
	/**
	 * Load Preview Fetch Ticket for a crawl. 
	 */
	public ModelAndView downloadTicketsAsCsvFromWebService(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			String siteId = request.getParameter("siteId");
			Integer tourId = (request.getParameter("tourId") == null 
					|| request.getParameter("tourId").length() == 0)?null:Integer.parseInt(request.getParameter("tourId"));

			Integer eventId = (request.getParameter("eventId") == null 
					|| request.getParameter("eventId").length() == 0)?null:Integer.parseInt(request.getParameter("eventId"));
			
			String queryUrl = request.getParameter("queryUrl");

			Integer previewLimitSize = (request.getParameter("previewLimitSize") == null 
					|| request.getParameter("previewLimitSize").length() == 0)?null:Integer.parseInt(request.getParameter("previewLimitSize"));

			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();

			if (siteId.equals(Site.EBAY)) {
				// ticketListingCrawl.setExtraParameter("queryYear", request.getParameter("queryYear"));
				// ticketListingCrawl.setExtraParameter("queryMonth", request.getParameter("queryMonth"));
				// ticketListingCrawl.setExtraParameter("queryDay", request.getParameter("queryDay"));
				// ticketListingCrawl.setExtraParameter("queryString", request.getParameter("queryString"));
			} else if (siteId.equals(Site.EI_MARKETPLACE) || siteId.equals(Site.TICKET_NETWORK_DIRECT)) {
				ticketListingCrawl.setExtraParameter("queryEventId", request.getParameter("queryEventId"));
			} else if (siteId.equals(Site.EIBOX)) {
				ticketListingCrawl.setExtraParameter("queryEventId", request.getParameter("queryEventId"));
				ticketListingCrawl.setExtraParameter("queryVenueId", request.getParameter("queryVenueId"));
				ticketListingCrawl.setExtraParameter("queryEventDate", request.getParameter("queryEventDate"));
			}

			ticketListingCrawl.setSiteId(siteId);
//			ticketListingCrawl.setTourId(tourId);
			ticketListingCrawl.setEventId(eventId);
			ticketListingCrawl.setQueryUrl(queryUrl);

			System.out.println("FETCHING TICKETS with crawl:"  + ticketListingCrawl);
			
			PreviewTicketHitIndexer ticketHitIndexer = new PreviewTicketHitIndexer(previewLimitSize);

			TicketListingFetcher ticketListingFetcher = (TicketListingFetcher)(ticketListingFetcherMap.get(siteId).clone());
			//System.out.println("THEURL: " + queryUrl);
			if (!ticketListingFetcher.isValidUrl(queryUrl)) {
				System.out.println("INVALIDURL!!");
				throw new Exception("Invalid URL");
			}
			StringBuffer buffer = new StringBuffer();
			try {
				
				buffer =ticketListingFetcher.fetchTicketListingTest(ticketHitIndexer, ticketListingCrawl);
				OutputStream os = response.getOutputStream();
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=TNWebServices_Tickets_"+request.getParameter("queryEventId")+".csv");
				os.write(buffer.toString().getBytes());
				
				return null;
				
			} catch (InterruptedTicketListingCrawlException e) {
			} catch (Exception e) {
				throw e;
			}
			return null;
		} catch (Exception e) {
			response.getOutputStream().write(("<font color='red'><b>Error: " + e.getMessage() + "</b></font>").getBytes());
			e.printStackTrace();
			return null;
		}
	}
	
	public ModelAndView loadExchangeCrawlFrequency(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		ModelAndView mav = new ModelAndView("page-default-exchange-crawl-frequency");
		String action=request.getParameter("action");
		String edit=request.getParameter("edit");
		String info=null;
		String error=null;
		DefaultExchangeCrawlFrequency defaultExchangeCrawlFrequency =  null;
		if(edit != null && edit.trim().length() >0){
			Integer id =Integer.parseInt(request.getParameter("edit"));
			defaultExchangeCrawlFrequency=  DAORegistry.getDefaultExchangeCrawlFrequencyDAO().get(id);
			mav.addObject("exchange", defaultExchangeCrawlFrequency.getExchange());
			mav.addObject("crawlFrequency", defaultExchangeCrawlFrequency.getDefaultCrawlFrequency());
			mav.addObject("autoFrequency", defaultExchangeCrawlFrequency.isAutomaticCrawlFrequency());
			mav.addObject("id", defaultExchangeCrawlFrequency.getId());
			mav.addObject("enableDelete",true);
		}else{
			mav.addObject("autoFrequency",false);
			mav.addObject("enableDelete",false);
		}
		if(action!=null && action.trim().length() >0 && action.equalsIgnoreCase("save")){
			String exchange=request.getParameter("exchangeSelect");
			String defaultFrequency=request.getParameter("selectFrequency");
			String acf=request.getParameter("acf");
			boolean autoFrquency = false;
			if(acf.equalsIgnoreCase("true")){
				autoFrquency=true;
			}
			defaultExchangeCrawlFrequency =  DAORegistry.getDefaultExchangeCrawlFrequencyDAO().getDefaultExchangeCrawlFrequencyByExchange(exchange);
			if(defaultExchangeCrawlFrequency !=null){
				defaultExchangeCrawlFrequency.setAutomaticCrawlFrequency(autoFrquency);
				defaultExchangeCrawlFrequency.setDefaultCrawlFrequency(Integer.parseInt(defaultFrequency));
			}else{
				defaultExchangeCrawlFrequency= new DefaultExchangeCrawlFrequency();
				defaultExchangeCrawlFrequency.setExchange(exchange);
				defaultExchangeCrawlFrequency.setAutomaticCrawlFrequency(autoFrquency);
				defaultExchangeCrawlFrequency.setDefaultCrawlFrequency(Integer.parseInt(defaultFrequency));
			}
			DAORegistry.getDefaultExchangeCrawlFrequencyDAO().saveOrUpdate(defaultExchangeCrawlFrequency);
			info="Default Exchange Crawl Frequency declared successfully.";
		}else if(action!=null && action.trim().length() >0 && action.equalsIgnoreCase("delete")){
			String id=request.getParameter("id");
			defaultExchangeCrawlFrequency = DAORegistry.getDefaultExchangeCrawlFrequencyDAO().get(Integer.parseInt(id.trim()));
			DAORegistry.getDefaultExchangeCrawlFrequencyDAO().delete(defaultExchangeCrawlFrequency);
			info="Deleted successfully.";
		}
		List<DefaultExchangeCrawlFrequency> exchangeCrawlFrquencies = DAORegistry.getDefaultExchangeCrawlFrequencyDAO().getAllDefaultExchangeCrawlFrequency();
		mav.addObject("exchangeCrawlFrquencies", exchangeCrawlFrquencies);
		mav.addObject("info",info);
		mav.addObject("error",error );
		return mav;
	}
	
	
	
	public void sendRequest(String url){
		try {
			
			/*HttpClient client = HttpClientStore.createHttpClient();
			HttpGet get = new HttpGet(url);
			System.out.println(client.execute(get));*/
			
			SimpleHttpClient client = HttpClientStore.createHttpClient();
			HttpGet get = new HttpGet(url);
			System.out.println("TMAT CRL 2 EXE: "+client.executeWithOneHit(get,true));
			
		} catch (Exception e) {
			System.out.println("TMAT CRL 2 ERR: "+url);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

}
