package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Class which handles the Edit Ticket Listing Crawl Form.
 *
 */
public class EditorEditTicketListingCrawlFormController extends SimpleFormController implements InitializingBean {
//	private TicketListingCrawler ticketListingCrawler;
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();
	private PreferenceManager preferenceManager;
	private SharedProperty sharedProperty;
	
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {	
		String action = request.getParameter("action");
		if (action.equals("create")) {			
			TicketListingCrawlCommand command = new TicketListingCrawlCommand(new TicketListingCrawl());
			command.setBackUrl(request.getParameter("backUrl"));
			command.getTicketListingCrawl().setSiteId(request.getParameter("siteId"));
			command.getTicketListingCrawl().setEnabled(true);
			
			/*String tourIdString = request.getParameter("tourId");
			if (tourIdString != null) {
				command.getTicketListingCrawl().setTourId(Integer.parseInt(tourIdString));
			}*/

			String eventIdString = request.getParameter("eventId");
			if (eventIdString != null) {
				command.getTicketListingCrawl().setEventId(Integer.parseInt(eventIdString));
			}

			return command;
		}		
		int ticketListingCrawlId = Integer.parseInt(request.getParameter("id"));
		// TODOJMS
//		TicketListingCrawl ticketListingCrawl = ticketListingCrawler.getTicketListingCrawlById(ticketListingCrawlId);
		TicketListingCrawl ticketListingCrawl = DAORegistry.getTicketListingCrawlDAO().get(ticketListingCrawlId);
		try {
			TicketListingCrawl ticketListingCrawlCopy = (TicketListingCrawl)ticketListingCrawl.clone();
			TicketListingCrawlCommand command = new TicketListingCrawlCommand(ticketListingCrawlCopy);

			command.setBackUrl(request.getParameter("backUrl"));

			if (ticketListingCrawl.getSiteId().equals(Site.EBAY)) {
				// command.setQueryDay(ticketListingCrawl.getExtraParameter("queryDay"));
				// command.setQueryMonth(ticketListingCrawl.getExtraParameter("queryMonth"));
				// command.setQueryYear(ticketListingCrawl.getExtraParameter("queryYear"));
				// command.setQueryString(ticketListingCrawl.getExtraParameter("queryString"));
			} else if (ticketListingCrawl.getSiteId().equals(Site.EI_MARKETPLACE) ) {
				command.setQueryEventId(ticketListingCrawl.getExtraParameter("queryEventId"));				
			}else if(ticketListingCrawl.getSiteId().equals(Site.TICKET_EVOLUTION)){ 
				command.setQueryEventId(ticketListingCrawl.getExtraParameter("queryEventId"));				
			} else if (ticketListingCrawl.getSiteId().equals(Site.EIBOX)) {
				command.setQueryEventId(ticketListingCrawl.getExtraParameter("queryEventId"));				
				command.setQueryVenueId(ticketListingCrawl.getExtraParameter("queryVenueId"));				
				command.setQueryEventDate(ticketListingCrawl.getExtraParameter("queryEventDate"));				
			} else if (ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK_DIRECT)) {
				command.setQueryEventId(ticketListingCrawl.getExtraParameter("queryEventId"));				
			}
			command.setForceRecrawl(true);
			return command;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	@Override
	protected ModelAndView onSubmit(Object command) {
		TicketListingCrawlCommand ticketListingCrawlCommand = (TicketListingCrawlCommand)command;
		TicketListingCrawl ticketListingCrawl = ticketListingCrawlCommand.getTicketListingCrawl();
		
		if (ticketListingCrawl.getSiteId().equals(Site.EBAY)) {
			// ticketListingCrawl.setExtraParameter("queryString", ticketListingCrawlCommand.getQueryString());
			// ticketListingCrawl.setExtraParameter("queryYear", ticketListingCrawlCommand.getQueryYear());
			// ticketListingCrawl.setExtraParameter("queryMonth", ticketListingCrawlCommand.getQueryMonth());
			// ticketListingCrawl.setExtraParameter("queryDay", ticketListingCrawlCommand.getQueryDay());
		} else if (ticketListingCrawl.getSiteId().equals(Site.EI_MARKETPLACE) || ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK_DIRECT) ) {
			ticketListingCrawl.setExtraParameter("queryEventId", ticketListingCrawlCommand.getQueryEventId());			
		}else if(ticketListingCrawl.getSiteId().equals(Site.TICKET_EVOLUTION)){ 
			ticketListingCrawl.setExtraParameter("queryEventId", ticketListingCrawlCommand.getQueryUrl().split("/events/")[1]);
		}else if (ticketListingCrawl.getSiteId().equals(Site.EIBOX)) {
			ticketListingCrawl.setExtraParameter("queryEventId", ticketListingCrawlCommand.getQueryEventId());					
			ticketListingCrawl.setExtraParameter("queryVenueId", ticketListingCrawlCommand.getQueryVenueId());					
			ticketListingCrawl.setExtraParameter("queryEventDate", ticketListingCrawlCommand.getQueryEventDate());					
		}
		
		String info="";
		Date now = new Date();
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (ticketListingCrawlCommand.getOriginalName() == null) {
			boolean flag = false;
			
			boolean duplicatFlag = DAORegistry.getTicketListingCrawlDAO().isQueryUrlAlreadyExist(ticketListingCrawl.getQueryUrl(), null);
			if(duplicatFlag) {
				info="Query URL already Exist in System";
				flag = true;
			} else {
				List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(ticketListingCrawl.getEventId());
				if(crawls!=null && !crawls.isEmpty()){
					for(TicketListingCrawl crawl : crawls){
						if(crawl.getSiteId().equals(ticketListingCrawl.getSiteId())){
							info = "Event alrady has a crawl with same exchange.";
							flag = true;
							break;
						}
					}
				}
			}
			if(!flag){ 
				// this is a crawl creation
				ticketListingCrawl.setEnabled(true);
				ticketListingCrawl.setAutomaticCrawlFrequency(ticketListingCrawlCommand.getAutomaticCrawlFrequency());
				ticketListingCrawl.setCreator(username);
				ticketListingCrawl.setCreated(now);
				ticketListingCrawl.setLastUpdater(username);
				ticketListingCrawl.setLastUpdated(now);
				// TODOJMS if and else both
				//			if (!ticketListingCrawler.ticketListingCrawlExists(ticketListingCrawl)) {
					DAORegistry.getTicketListingCrawlDAO().save(ticketListingCrawl);							
					//				ticketListingCrawler.addTicketListingCrawl(ticketListingCrawl);
					info = "The ticket listing crawl " + ticketListingCrawl.getName() + " has been created";
					//			} else {
					//				info = "The ticket listing crawl " + ticketListingCrawl.getName() + "  already exists";				
					//			}	
					
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls="+ticketListingCrawl.getId();;
				sendRequest(url);
				
			} 
			
		} else {
			// this is a crawl update
			// TODOJMS
			TicketListingCrawl tc = null;
			
			List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(ticketListingCrawl.getEventId());
			boolean flag = false;
			if(crawls!=null && !crawls.isEmpty()){
				for(TicketListingCrawl crawl : crawls){
					if(crawl.getId().equals(ticketListingCrawl.getId())){
						tc =crawl;
					}else if(crawl.getSiteId().equals(ticketListingCrawl.getSiteId())){
						info = "Event alrady has a crawl with same exchange.";
						flag = true;
						break;
					}
				}
			}
			if(!flag){
				boolean duplicatFlag = DAORegistry.getTicketListingCrawlDAO().isQueryUrlAlreadyExist(ticketListingCrawl.getQueryUrl(), ticketListingCrawl.getId());
				if(duplicatFlag) {
					info="Query URL already Exist in System";
					flag = true;
				} 
			}
			if(!flag){
				if(tc==null){
					// Disable ticket for crawl as event has been changed
					DAORegistry.getTicketDAO().disableTickets(ticketListingCrawl.getId());
					tc = ticketListingCrawl;
				}
				// FIXME: maybe set a condition if we change tour and stay in event AUTO mode.
				/*if ((tc.getEventId() == null && ticketListingCrawl.getEventId() != null)
						|| (tc.getEventId() != null && !tc.getEventId().equals(ticketListingCrawl.getEventId()))) {
					DAORegistry.getTicketDAO().deleteTicketsByCrawlId(tc.getId());
				}*/
				
				tc.setAutomaticCrawlFrequency(ticketListingCrawlCommand.getAutomaticCrawlFrequency());
				tc.setName(ticketListingCrawl.getName());
				tc.setCrawlFrequency(ticketListingCrawl.getCrawlFrequency());
//				tc.setTourId(ticketListingCrawl.getArtistId());
				tc.setEventId(ticketListingCrawl.getEventId());
				
				Event event = ticketListingCrawl.getEvent();
				if (event != null) {
					ticketListingCrawl.getExtraParameterMap().put("location", event.getVenue().getLocation());				
				}
				tc.setExtraParameterMap(ticketListingCrawl.getExtraParameterMap());
				tc.setQueryUrl(ticketListingCrawl.getQueryUrl());

				tc.setLastUpdater(username);
				tc.setLastUpdated(now);
				
				/*boolean isEnlableFlag= false;
				if(tc.isEnabled() != ticketListingCrawl.isEnabled()) {
					isEnlableFlag = true;
				}*/
				
				tc.setEnabled(ticketListingCrawl.isEnabled());
				
				DAORegistry.getTicketListingCrawlDAO().update(tc);
				
				//If crawl is disabled then call disableCrawl method else call updateCrawl method in Browse 
				if(tc.isEnabled()) {
					String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=updateCrawl&crawls="+ticketListingCrawl.getId();;
					sendRequest(url);
				} else {
					String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=disableCrawl&crawls="+ticketListingCrawl.getId();;
					sendRequest(url);
				}
				
				/*if(isEnlableFlag) {
					String crawlAction = "";
					if(ticketListingCrawl.isEnabled()) {
						crawlAction = "enableCrawl";
					} else {
						crawlAction = "disableCrawl";
					}
					String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action="+crawlAction+"&crawls="+ticketListingCrawl.getId();;
					sendRequest(url);
				}*/
				
				if (ticketListingCrawlCommand.isForceRecrawl()) {
//					ticketListingCrawler.forceRecrawl(tc);
					
					String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=forceRecrawl&crawls=" + tc.getId();
					sendRequest(url);
				}
				info = "The ticket listing crawl " + ticketListingCrawl.getName() + " has been updated";
			}
						
		}		

		if (ticketListingCrawlCommand.getBackUrl() == null) {
			return new ModelAndView(new RedirectView("EditorManageCrawls?info=" + info));
		} else {
			if (ticketListingCrawlCommand.getBackUrl().contains("?")) {
				return new ModelAndView(new RedirectView(ticketListingCrawlCommand.getBackUrl() + "&info=" + info));
			} else {
				return new ModelAndView(new RedirectView(ticketListingCrawlCommand.getBackUrl() + "?info=" + info));				
			}
		}
	}
	
	public void sendRequest(String url){
		try {
			
			/*HttpClient client = HttpClientStore.createHttpClient();
			HttpGet get = new HttpGet(url);
			System.out.println(client.execute(get));*/
			
			SimpleHttpClient client = HttpClientStore.createHttpClient();
			HttpGet get = new HttpGet(url);
			System.out.println("TMAT CRL 3 EXE: "+client.executeWithOneHit(get,true));
			
		} catch (Exception e) {
			System.out.println("TMAT CRL 3 ERR: "+url);
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		// 
		// populate ticket listing crawls appearing in the top right drop down menu
		//
		
		String eventIdString = request.getParameter("eventId");
		int eventId = 0;
		if (eventIdString == null) {
			// look for a value in the preference
			eventIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerEventId");
		}
		
		if (eventIdString != null && eventIdString.length() > 0) {
			eventId = Integer.parseInt(eventIdString);
		}
		
		//
		// Tour Id filter
		//
		Collection<Event> events = null;
		String artistIdString = request.getParameter("artistId");
		if (artistIdString == null) {
			// look for a value in the preference
			artistIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerArtistId");
		} else {
			eventId = 0;
		}
		
		int artistId = 0;
		if (artistIdString != null && artistIdString.length() > 0) {
			artistId = Integer.parseInt(artistIdString);
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);			
		}
		
		preferenceManager.updatePreference(username, "editorCrawlerArtistId", Integer.toString(artistId));
		preferenceManager.updatePreference(username, "editorCrawlerEventId", Integer.toString(eventId));

		//
		// Site Id filter
		//
		String siteId = request.getParameter("siteId");
		if (siteId == null) {			
			// look for a value in the preference
			siteId = preferenceManager.getPreferenceValue(username, "editorCrawlerSiteId");
			if (siteId == null) { 
				siteId = "";
			}
		}
		
		Boolean showDisabledCrawls = false;
		String showDisabledCrawlsString = request.getParameter("showDisabledCrawls");
		if (showDisabledCrawlsString == null) {
			showDisabledCrawlsString = preferenceManager.getPreferenceValue(username, "editorCrawlerShowDisabledCrawls");
			if (showDisabledCrawlsString == null) {
				showDisabledCrawlsString = "";
			}
		}		
		showDisabledCrawls = showDisabledCrawlsString.equals("on");

		ModelAndView mav = super.showForm(request, response, errors, controlModel);

		String crawlIdString = request.getParameter("id");
		Integer crawlId = null;
		// TODOJMS
		/*if (crawlIdString != null) {
			crawlId = Integer.parseInt(crawlIdString);
			TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
			TicketListingFetcher fetcher = ticketListingFetcherMap.get(crawl.getSiteId());
			mav.addObject("ticketListingUrl", fetcher.getTicketListingUrl(crawl));			
		}*/

		List<TicketListingCrawl> ticketListingCrawls = new ArrayList<TicketListingCrawl>();
		int currentCrawlPosition = -1;
		// TODOJMS
		/*for (TicketListingCrawl crawl: ticketListingCrawler.getTicketListingCrawls()) {
			if ((tourId == 0 || crawl.getTourId() == null || crawl.getTourId() == tourId)
					&& (eventId == 0 || crawl.getEventId() == null || crawl.getEventId() == eventId)
					&& (siteId.length() == 0 || crawl.getSiteId().equals(siteId))
					&& (showDisabledCrawls || crawl.isEnabled())) {				
				ticketListingCrawls.add(crawl);
				if (crawl.getId().equals(crawlId)) {
					currentCrawlPosition = i;
				}
				i++;
			}
		}*/
		
		TicketListingCrawl previousCrawl = null; 
		TicketListingCrawl nextCrawl = null;
		
		if (currentCrawlPosition != -1) {
			if (currentCrawlPosition > 0) {
				previousCrawl = ticketListingCrawls.get(currentCrawlPosition - 1);
			}
	
			if (currentCrawlPosition < ticketListingCrawls.size() - 1) {
				nextCrawl = ticketListingCrawls.get(currentCrawlPosition + 1);			
			}
		}

		mav.addObject("previousCrawl", previousCrawl);
		mav.addObject("nextCrawl", nextCrawl);

		mav.addObject("artists", DAORegistry.getArtistDAO().getAll());
		mav.addObject("ticketListingCrawls", ticketListingCrawls);
//		mav.addObject("ticketListingCrawler", SpringUtil.getTicketListingCrawler());
		
		TicketListingCrawlCommand command = (TicketListingCrawlCommand)getCommand(request);
		mav.addObject("backUrl", command.getBackUrl());

		return mav;
	}
	
//	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
//		this.ticketListingCrawler = ticketListingCrawler;
//	}	
	
	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}

	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}

	public void afterPropertiesSet() throws Exception {
//		if (ticketListingCrawler == null) {
//			throw new RuntimeException("Property ticketListingCrawler must be set");
//		}
		
		if (ticketListingFetcherMap == null) {
			throw new RuntimeException("Property ticketListingFetcherMap must be set");
		}

		if (preferenceManager == null) {
			throw new RuntimeException("Property preferenceManager must be set");
		}
	}
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
}