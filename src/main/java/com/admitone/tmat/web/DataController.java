package com.admitone.tmat.web;

//import java.io.BufferedReader;
//import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.analytics.ValuationFactorParser;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.AdmitoneEventLocal;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.ArtistPriceAdjustment;
import com.admitone.tmat.data.ArtistPriceAdjustmentPk;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.EventPriceAdjustmentPk;
import com.admitone.tmat.data.ExcludedCheapestTicket;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.PresaleEvent;
import com.admitone.tmat.data.ProbableVenue;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.TourCategory;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.data.VenueSynonym;
import com.admitone.tmat.data.VenueSynonymInfo;
import com.admitone.tmat.enums.ArtistStatus;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.PriceRounding;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.eventcreation.CreationEventHit;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.eventcreation.SuperAutomatedEventCreator;
import com.admitone.tmat.utils.EventExpirationManager;
import com.admitone.tmat.utils.PreferenceManager;
import com.admitone.tmat.utils.QueryUtil;

public class DataController extends MultiActionController implements InitializingBean {
	private ValuationFactorParser valuationFactorParser;
	private SuperAutomatedEventCreator superAutomatedEventCreator;
	private EventExpirationManager eventExpirationManager;
	private PreferenceManager preferenceManager;
	private CreationEventListManager creationEventListManager;
	
	public CreationEventListManager getCreationEventListManager() {
		return creationEventListManager;
	}

	public void setCreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}

	public EventExpirationManager getEventExpirationManager() {
		return eventExpirationManager;
	}

	public void setEventExpirationManager(
			EventExpirationManager eventExpirationManager) {
		this.eventExpirationManager = eventExpirationManager;
	}

	public SuperAutomatedEventCreator getSuperAutomatedEventCreator() {
		return superAutomatedEventCreator;
	}

	public void setSuperAutomatedEventCreator(
			SuperAutomatedEventCreator superAutomatedEventCreator) {
		this.superAutomatedEventCreator = superAutomatedEventCreator;
	}

	public ValuationFactorParser getValuationFactorParser() {
		return valuationFactorParser;
	}

	public void setValuationFactorParser(ValuationFactorParser valuationFactorParser) {
		this.valuationFactorParser = valuationFactorParser;
	}

	public ModelAndView loadManageDataPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-editor-manage-tcap");
		return mav;
		
	}

	public ModelAndView loadManageVenuesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-editor-manage-venues");
		mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
		
		//String filter = request.getParameter("filter");
		String action = request.getParameter("action");
		
		if (action != null && action.equals("remove")) {
			int venueId = Integer.parseInt(request.getParameter("id"));
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			Collection<Event> events = DAORegistry.getEventDAO()
					.getAllEventsByVenue(venueId);
			if (events.isEmpty()) {
				DAORegistry.getVenueDAO().deleteById(venueId);
				String info = "The venue " + venue.getBuilding() + ", "
						+ venue.getCity() + ", " + venue.getState() + ", "
						+ venue.getCountry() + " has been deleted!";
				return new ModelAndView(new RedirectView(
						"EditorManageVenues?info=" + info, true));
			} else {
				String info = "The venue " + venue.getBuilding() + ", "
				+ venue.getCity() + ", " + venue.getState() + ", "
				+ venue.getCountry() + " cannot be deleted because it has the following events associated to it:<br/>";
				for (Event event: events) {
					info += event.getName() + " " + event.getFormattedEventDate() + "<br/>";
				}
				return new ModelAndView(new RedirectView(
						"EditorManageVenues?info=" + info, true));				
			}

		}
		
		Collection<Venue> venues;
		
		/*if (filter != null && !filter.trim().isEmpty()) {
			filter = filter.trim();
			venues = DAORegistry.getVenueDAO().filterByVenue(filter);
		} else {
			venues = DAORegistry.getVenueDAO().getAll();
		}
		mav.addObject("venues", venues);
		return mav;*/
		if(action != null && action.equalsIgnoreCase("search")){
			String venue=request.getParameter("venue");
			String city=request.getParameter("city");
			String country=request.getParameter("country");
			String state=request.getParameter("state");
			String venueType=request.getParameter("venueType");
			venues = DAORegistry.getVenueDAO().filterVenue(venue, city, state, country, venueType);
			
			mav.addObject("venue", venue);
			mav.addObject("city", city);
			mav.addObject("state", state);
			mav.addObject("country", country);
			mav.addObject("venueType", venueType);
		}else{
			venues = DAORegistry.getVenueDAO().getAll();
		}
		mav.addObject("venues", venues);
		return mav;
	}

	public ModelAndView loadArtistsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-list-artists");
		String name = request.getParameter("name");
		String idStr = request.getParameter("id");
		
		Collection<Artist> artists =null;
		if(name!=null && !name.isEmpty()){
			artists = new ArrayList<Artist>();
			Artist artist = null;
			
			if(null != idStr) {
				artist = DAORegistry.getArtistDAO().get(Integer.parseInt(idStr.trim()));	
			} else {
				artist = DAORegistry.getArtistDAO().getArtistByName(name);
			}
			if(null != artist) {
				artists.add(artist);	
			}
		}else{
			artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		}
		mav.addObject("artists", artists);
		return mav;
	}
	
	public ModelAndView mergeArtistsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-merge-artist");
		String artistId = request.getParameter("artistId");
		String mergeToArtistId = request.getParameter("mergeToArtistId");
		Collection<Artist> 	artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		mav.addObject("artists", artists);
		return mav;
	}
	public ModelAndView artistCreateConfirmationPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		Artist artist = new Artist();
		artist.setArtistStatus(ArtistStatus.ACTIVE);
		artist.setName(name);
		DAORegistry.getArtistDAO().save(artist);
		String info = "The artist " + artist.getName() + " has been created";
		ModelAndView mav = new ModelAndView("page-editor-manage-artists");
//		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		mav.addObject("info", info);
		return mav;
	}
	
	public ModelAndView loadManageArtistsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-editor-manage-artists");
		
		String filter = request.getParameter("filter");
		String action = request.getParameter("action");

		if (action != null && action.equals("remove")) {
			int artistId = Integer.parseInt(request.getParameter("id"));
			Artist artist = DAORegistry.getArtistDAO().get(artistId);

			// Collection<Tour> tours =
			// DAORegistry.getTourDAO().getAllToursByArtist(artistId);
			// if (tours != null && tours.size() > 0) {
			// String info = "The venue " + artist.getName() +
			// " is used by some tour(s)";
			// return new ModelAndView(new
			// RedirectView("EditorManageArtists?info=" + info, true));
			// }

			DAORegistry.getArtistDAO().deleteById(artistId);

			String info = "The artist " + artist.getName()
					+ " has been deleted!";
			return new ModelAndView(new RedirectView(
					"EditorManageArtists?info=" + info, true));
		}
		
		Collection<Artist> artists = null;
		
		if (filter != null && !filter.trim().isEmpty()) {
			filter = filter.trim();
			artists = DAORegistry.getArtistDAO().filterByName(filter);
		}/* else {
			artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		}*/

		mav.addObject("artists", artists);

		return mav;
	}

	/*public ModelAndView loadMergeToursPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-merge-tour");
		mav.addObject("tours", DAORegistry.getTourDAO().getAllActiveTours());
		return mav;
	}*/
	/*public ModelAndView loadManageToursPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-editor-manage-tours");
		
		//String filter = request.getParameter("filter");
		String action = request.getParameter("action");

		if (action != null && action.equals("remove")) {
			int tourId = Integer.parseInt(request.getParameter("id"));
			Tour tour = DAORegistry.getTourDAO().get(tourId);

			// Collection<Event> events =
			// DAORegistry.getEventDAO().getAllEventsByTour(tourId);
			// if (events != null && events.size() > 0) {
			// String info = "The tour " + tour.getName() +
			// " is used by some events(s)";
			// return new ModelAndView(new
			// RedirectView("EditorManageTours?info=" + info, true));
			// }
			
			DAORegistry.getTourDAO().deleteById(tourId);

			String info = "The tour " + tour.getName() + " has been deleted!";
			return new ModelAndView(new RedirectView("EditorManageTours?info="
					+ info, true));
		}

		Collection<Tour> tours = null;
		
//		if (filter != null && !filter.trim().isEmpty()) {
//			filter = filter.trim();
//			tours = DAORegistry.getTourDAO().filterByName(filter);
//		} else {
//			tours = DAORegistry.getTourDAO().getAllActiveTours();
//		}
		if(action != null && action.equalsIgnoreCase("filter")){
			String tourName = request.getParameter("tourName");
			String artist = request.getParameter("artist");
//			String location = request.getParameter("location");
//			String startDate = request.getParameter("dateRange1");
//			String endDate = request.getParameter("dateRange2");
//			String charSeq = "[\\'\\-\\+\\.\\^:,]";
//			tourName = tourName.replaceAll(charSeq, "");
//			artist = artist.replaceAll(charSeq, "");
//			location = location.replaceAll(charSeq, "");

			tours = DAORegistry.getTourDAO().filterTour(tourName, artist);
			
			mav.addObject("tourName", tourName);
			mav.addObject("artist", artist);
//			mav.addObject("location", location);
//			mav.addObject("dateRange1", startDate);
//			mav.addObject("dateRange2", endDate);
//		}else{
//			tours = DAORegistry.getTourDAO().getAllActiveTours();
		}
		int tourCount=0;		
		int eventCount=0;
		if(tours!=null){
			for(Tour tour:tours){
				if(tour.getEventCount()>0){
					tourCount++;
				}
				for(Event event:tour.getEvents()){
					if(event.getCrawlCount()>0)
					eventCount++;	
				}
			}	
		}
		mav.addObject("tours", tours);
		mav.addObject("toursCount", tourCount);
		mav.addObject("eventsCount", eventCount);

		return mav;
	}
*/
	public ModelAndView loadDetailArtistPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Integer artistId = Integer.parseInt(request.getParameter("artistId"));
//		System.out.println("Artistid"+artistId);
		String action = request.getParameter("action");
		if(action != null && !action.isEmpty()){
			if(action.equalsIgnoreCase("removeArtist")){
//				Artist tour = DAORegistry.getArtistDAO().get(artistId);
				DAORegistry.getArtistDAO().deleteById(artistId);
				String info ="Artist having ID:"+artistId+" has been Deleted.";
				return new ModelAndView(new RedirectView("EditorEditArtist?id="+artistId+"&info="+info));
				
			}
		}
		
		ModelAndView mav = new ModelAndView("page-editor-detail-artist");
		mav.addObject("artist", DAORegistry.getArtistDAO().get(artistId));
		mav.addObject("events", DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId));
		
		if (action != null && action.equals("remove")) {
			int eventId = Integer.parseInt(request.getParameter("id"));
			Event event = DAORegistry.getEventDAO().get(eventId);

			DAORegistry.getEventDAO().deleteById(eventId);
			Venue venue = event.getVenue();
			Artist artist = event.getArtist();
			CreationEventHit eventHit = new CreationEventHit(event.getId(), event.getArtistId(), event.getVenueId(), 
					event.getName(), artist.getName(), new java.sql.Date(event.getLocalDate().getTime()), event.getLocalTime(), event.getEventType(), venue.getBuilding(), venue.getCity(), venue.getState(), venue.getCountry(),venue.getZipcode(), "");
			CreationEventListManager.removeFromExistingEvents(eventHit,event.getAdmitoneId());
			String info = "The event " +event.getName()+" : "+ ((event.getLocalDate()== null)?"TBD":(event.getLocalDate()))
					+ " has been deleted!";
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			event.setUpdatedBy(username);
			return new ModelAndView(new RedirectView("EditorDetailArtist?artistId="
					+ artistId + "&info=" + info, true));
		}else if(action != null && action.equals("removeMultiple")){
			String tobeRemoved = request.getParameter("id");
			
			if(tobeRemoved != null && !tobeRemoved.isEmpty()){
				String[] removeList=tobeRemoved.split(",");
				for(String eventId:removeList){
					DAORegistry.getEventDAO().deleteById(Integer.parseInt(eventId));
				}
				String info = removeList.length+ " Events Has been Removed.";
				
				return new ModelAndView(new RedirectView("EditorDetailArtist?artistId="
						+ artistId + "&info=" + info, true));
			}
		}
			
		String info=request.getParameter("info");
		if(info!=null){
			mav.addObject("info", info);
		}
		
		return mav;
	} 
	public ModelAndView loadEditMultipleEvents(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ParseException {
		ModelAndView mav = new ModelAndView("page-editor-multiple-event");
		
		Collection<Event> events = new ArrayList<Event>();
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		EventPriceAdjustment eventPriceAdjustment = null;
		AdmitoneEvent admitoneEvent=null;
		
		Integer eventId;
		Integer admitoneId;
		
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
		
	
		//List<Venue> venues = (List<Venue>)DAORegistry.getVenueDAO().getAll();
		List<Venue> venues = new ArrayList<Venue>();
	
		/*Collections.sort(artists, new Comparator<Artist>(){
			public int compare(Artist t1,	Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		}
		);*/
		
		/*mav.addObject("artists", artists);
		mav.addObject("venues", venues);*/
		
		if (artistIdString == null && venueIdString == null) {
			// look for a value in the preference
			//tourIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerTourId");
			return mav;
		}
		
		int artistId = 0;
		//List listOfvenueId=new ArrayList();
		
		Collection<Venue> listofVenue;
		Collection<Event> listOfevents;
		int venueId = 0;
		boolean getArtistFlag = false,getVenueFlag = false;
		
		if(venueIdString!=null && artistIdString!=null && venueIdString.length()>0 && artistIdString.length() > 0 &&!venueIdString.equals("0") && !artistIdString.equals("0")){
	     	try{
	     		getArtistFlag = true;
	     		getVenueFlag = true;
	     		venueId = Integer.parseInt(venueIdString);
	     		artistId = Integer.parseInt(artistIdString);
	     	}catch(Exception e){
	     		e.printStackTrace();
	     	}
	     	events=new ArrayList();
			listOfevents =DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId,venueId,EventStatus.ACTIVE);
		    for (Event event : listOfevents) {
		    	event.getVenue().setVenueCategoryList(DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(event.getVenueId()));
		    	events.add(event);
			} 
		    
		    Collection<Event> listOfeventsTemp = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		    List<Event> eventsTemp = new ArrayList();
		    for (Event event : listOfeventsTemp) {
		    	event.getVenue().setVenueCategoryList(DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(event.getVenueId()));
		    	eventsTemp.add(event);
			} 
			for (Event event :eventsTemp) {
				listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
				for (Venue venue : listofVenue) {
					if(!venues.contains(venue)){
					venues.add(venue);
					}
				}
                 // DAORegistry.getVenueDAO().getVenue(event.getVenue())
			}
			mav.addObject("venues", venues);
		    
	   }else if (artistIdString != null && artistIdString.length() > 0 && !artistIdString.equals("0")) {
			try{	
				getArtistFlag = true;
				artistId = Integer.parseInt(artistIdString);
				venues=new ArrayList();
			}catch(Exception e){
				e.printStackTrace();
			}
			if (artistId != 0 && artistId != -1 && DAORegistry.getArtistDAO().get(artistId) == null) {
				artistId = 0;
			}else{
				if(artistId == -1){
					events = DAORegistry.getEventDAO().getAllActiveEvents();
				}else{
					events=new ArrayList();
					listOfevents = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
				    for (Event event : listOfevents) {
				    	event.getVenue().setVenueCategoryList(DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(event.getVenueId()));
				    	events.add(event);
					} 
					for (Event event :events) {
						listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
						for (Venue venue : listofVenue) {
							if(!venues.contains(venue)){
							venues.add(venue);
							}
						}
                         // DAORegistry.getVenueDAO().getVenue(event.getVenue())
					}
					mav.addObject("venues", venues);
				}
			}
		}else if (venueIdString!=null && venueIdString.length()>0 &&!venueIdString.equals("0")){
			try{
			getVenueFlag = true;
			venueId = Integer.parseInt(venueIdString);
			}catch(Exception e){
				e.printStackTrace();
			}
			if (venueId != 0 && DAORegistry.getVenueDAO().get(venueId) == null) {
				venueId = 0;
			}else{
			events=new ArrayList();
			listOfevents = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId,EventStatus.ACTIVE);
		    for (Event event : listOfevents) {
		    	event.getVenue().setVenueCategoryList(DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(event.getVenueId()));
		    	events.add(event);
			} 
		}
		}
		
		if(getArtistFlag){
			mav.addObject("artistName", DAORegistry.getArtistDAO().get(artistId).getName());
		}
		
		if(getVenueFlag){
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
		}
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		for(Event event:events){
			String check=request.getParameter(event.getId()+"-checkbox");
			String venueCategoryId=request.getParameter(event.getId()+"-venueCategoryId");
			String presaleEventStr = request.getParameter(event.getId()+"-presaleEvent");
			Boolean isPresaleEvent=Boolean.FALSE;
			if(presaleEventStr != null && "on".equals(presaleEventStr)) {
				isPresaleEvent = Boolean.TRUE;
			}
			if(check!=null && "on".equals(check)){
				if(!venueCategoryId.equals("0")) {
		           event.setVenueCategoryId(Integer.valueOf(venueCategoryId));
		           event.setPresaleEvent(isPresaleEvent);
		           event.setUpdatedBy(username);
		           event.setLastUpdate(new Date());
		           DAORegistry.getEventDAO().update(event);
		          
		           Date now = new Date();
		           //Tamil 04/27/2018 : Removed Presale Logic 
					/*PresaleEvent presaleEvent = DAORegistry.getPresaleEventDAO().getActivePresaleEventByEventId(event.getId());
					if(event.getPresaleEvent()) {
						if(presaleEvent == null) {
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.DATE, 7);
							cal.set(Calendar.HOUR_OF_DAY, 0);
							cal.set(Calendar.MINUTE, 0);
							cal.set(Calendar.SECOND, 0);
							Date applyTime = new Date(cal.getTimeInMillis());
							
							presaleEvent = new PresaleEvent();
							presaleEvent.setEvent(event);
							presaleEvent.setApplyDate(applyTime);
							presaleEvent.setApplyTime(applyTime);
							presaleEvent.setStatus("ACTIVE");
							presaleEvent.setCreatedBy(username);
							presaleEvent.setCreatedDate(now);
							presaleEvent.setLastUpdatedBy(username);
							presaleEvent.setLastUpdated(now);
							DAORegistry.getPresaleEventDAO().save(presaleEvent);
						}
					} else {
						if(presaleEvent != null) {
							presaleEvent.setStatus("DELETED");
							presaleEvent.setLastUpdatedBy(username);
							presaleEvent.setLastUpdated(now);
							DAORegistry.getPresaleEventDAO().save(presaleEvent);
						}
					}*/
					
					EventAudit ea = new EventAudit();
					ea.setEventId(event.getId());
					ea.setUserName(username);
					ea.setAction("Updated");
					ea.setModifiedDate(now);
					ea.setEventName(event.getName());
					ea.setArtistId(event.getArtistId());
					ea.setArtistName(event.getArtist().getName());
					ea.setEventDate(event.getLocalDate());
					ea.setEventTime(event.getLocalTime());
					ea.setPresaleEvent(event.getPresaleEvent());
					ea.setVenueName(DAORegistry.getVenueDAO().get(event.getVenueId()).getBuilding());
					ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
					if(event.getVenueCategoryId()!= null) {
						ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
					}
					if(event.getEventType()!=null && event.getEventType().name()!= null) {
						ea.setEventType(event.getEventType().name());
					}
					if(event.getNoPrice()!= null) {
						ea.setNoPrice("YES");
					} else {
						ea.setNoPrice("NO");
					}
					if(event.getAdmitoneId()!= null) {
					    ea.setAdmitoneId(event.getAdmitoneId());
					}
					ea.setVenueId(event.getVenueId());
					DAORegistry.getEventAuditDAO().save(ea);
				}
			} 
		}
		
		String siteId = "stubhub";
		ArtistPriceAdjustment tourPriceAdjustment = DAORegistry.getArtistPriceAdjustmentDAO().get(new ArtistPriceAdjustmentPk(artistId, siteId));
		Double percentAdjustment = 0.0;
		String admitoneEventNameDateTime=null;
	
		if (events!=null){
			for (Event event:events){
				eventId = event.getId();
				admitoneId=event.getAdmitoneId();
				
				eventPriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(new EventPriceAdjustmentPk(eventId, siteId));
				admitoneEvent =DAORegistry.getAdmitoneEventDAO().getEventByEventId(admitoneId);
				if(admitoneEvent!=null)
				{
				 DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				 DateFormat tf = new SimpleDateFormat("HH:mm");
				  admitoneEventNameDateTime = admitoneEvent.getEventName()+","+df.format(admitoneEvent.getEventDate())+","+tf.format(admitoneEvent.getEventTime());		
				  event.setAdmitoneEventDetails(admitoneEventNameDateTime);
				}
				if (eventPriceAdjustment!=null){
					percentAdjustment = eventPriceAdjustment.getPercentAdjustment();
				}
				else {
					if (tourPriceAdjustment != null){
						percentAdjustment = tourPriceAdjustment.getPercentAdjustment();
					}
				}
				mav.addObject("shPercentAdjustment",percentAdjustment);
			}
		}
		
//		if(tourPriceAdjustment != null){
//			mav.addObject("shtourPriceAdjustment",tourPriceAdjustment.getPercentAdjustment());
//		}
		mav.addObject("events", events);
		mav.addObject("editorArtistId", Integer.toString(artistId));
		//mav.addObject("editorTCAPEventId", Integer.toString(eventId));	
		mav.addObject("editorVenueId", Integer.toString(venueId));
		return mav;		
		
	}
	public ModelAndView loadEditMultipleEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException, ParseException {

		int artistId = 0;
		String artistName = new String();
		if(null != request.getParameter("artistId") && request.getParameter("artistId").trim().length() >0){
			artistId = Integer.parseInt(request.getParameter("artistId"));
			Artist artist = DAORegistry.getArtistDAO().get(artistId);
			artistName = artist.getName();
		}

		
		String action = request.getParameter("action");
		String checkName = request.getParameter("name-checkbox");
		String name = request.getParameter("name");
		
//		String dateTbd1 = request.getParameter("dateTbd1");
//		String dateTbd2 = request.getParameter("dateTbd2");
		String dateTbd = request.getParameter("dateTbd");
		
		String checkDate = request.getParameter("date-checkbox");
		String date = request.getParameter("localDateCalendar");
		
		
//		String timeTbd1 = request.getParameter("timeTbd1");
//		String timeTbd2 = request.getParameter("timeTbd2");
		String timeTbd = request.getParameter("timeTbd");
		
		String checkTime = request.getParameter("time-checkbox");
		String localTimeHours = request.getParameter("localTimeHours")==null?"":request.getParameter("localTimeHours");
		String localTimeMinutes = request.getParameter("localTimeMinutes")==null?"":request.getParameter("localTimeMinutes");
		String time = ((localTimeHours.length()==1?"0"+localTimeHours:localTimeHours) + ":" + (localTimeMinutes.length()==1?"0"+localTimeMinutes:localTimeMinutes));
		
		String checkVenue = request.getParameter("venue-checkbox");
		String venueId = request.getParameter("venueId");
		String checkVenueCategory = request.getParameter("venueCategory-checkbox");
		String venueCategoryId = request.getParameter("selectedVenueCategory");
		String checkArtist = request.getParameter("artist-checkbox");
		String selectedArtistId = request.getParameter("selectedArtistId");
		Collection<Event> events = new ArrayList<Event>();
		if(artistId !=0){
			events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		}
		Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAllActiveArtists();
		String checkType = request.getParameter("type-checkbox");
		String type = request.getParameter("eventType");
		if (action != null && action.equals("edit")){
			Map<Integer,Venue> venueMap=new HashMap<Integer, Venue>();
			if(checkVenue!=null && "on".equals(checkVenue)){
				for(Venue venue:venues){
					venueMap.put(venue.getId(), venue);
				}
			}
			Map<Integer,Artist> tourMap=new HashMap<Integer, Artist>();
			if(checkArtist!=null && "on".equals(checkArtist)){
				for(Artist artist:artists){
					tourMap.put(artist.getId(), artist);
				}
			}
			VenueCategory venueCategory = null;
			if(checkVenueCategory!=null && "on".equals(checkVenueCategory)){
				if(!venueCategoryId.equals("NoCategory"))
					venueCategory= DAORegistry.getVenueCategoryDAO().get(Integer.parseInt(venueCategoryId));
			}
			List<Event> saveList = new ArrayList<Event>(); 
			for(Event event:events){
				String check=request.getParameter(event.getId()+"-checkbox");
				if(check!=null && "on".equals(check)){
					if(checkName!=null && "on".equals(checkName)){
						event.setName(name);
					}
					if(checkDate!=null && "on".equals(checkDate)){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						if(Boolean.parseBoolean(dateTbd)){
							event.setDateTbd(true);
							event.setLocalDate(null);
						}else{
							date = date + " 00:00:00";
							Date dateTime = df.parse(date);
							event.setLocalDate(dateTime);
						}
					}
					if(checkTime!=null && "on".equals(checkTime)){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						if(Boolean.parseBoolean(timeTbd)){
							event.setTimeTbd(true);
							event.setLocalTime(null);
						}else{
							time = "1970-01-01 " + time + ":00";
							Date dateTime = df.parse(time);
							event.setLocalTime(new Time(dateTime.getTime()));
						}
					}
					if(checkVenue!=null && "on".equals(checkVenue)){
						Venue venue=venueMap.get(Integer.parseInt(venueId));
						event.setVenue(venue);
						event.setVenueId(venue.getId());
					}
					if(checkVenueCategory!=null && "on".equals(checkVenueCategory)){
						if(!venueCategoryId.equals("NoCategory")){
							event.setVenueCategory(venueCategory);
							event.setVenueCategoryId(venueCategory.getId());
						}else{
							event.setVenueCategoryId(null);
						}						
					}
					if(checkArtist!=null && "on".equals(checkArtist)){
						Artist artist = tourMap.get(selectedArtistId);
						event.setArtistId(artist.getId());
					}
					if(checkType!=null && "on".equals(checkType)){
						TourType tourType=null;
						if(type.equals(TourType.CONCERT.toString())){
							tourType=TourType.CONCERT;
						}else if(type.equals(TourType.SPORT.toString())){
							tourType=TourType.SPORT;
						}else if(type.equals(TourType.THEATER.toString())){
							tourType=TourType.THEATER;
						}else {
							tourType=TourType.OTHER;
						}
						
						event.setEventType(tourType);
						event.setEventOverridden("MENUAL");
					}
					event.setLastUpdate(new Date());
					saveList.add(event);
				}
			}
			if(!saveList.isEmpty()){
				DAORegistry.getEventDAO().updateAll(saveList);
			}
			return new ModelAndView(new RedirectView("EditorDetailArtist?artistId="
					+ artistId + "&info=" , true));
		}else{
			ModelAndView mav = new ModelAndView("page-editor-multiple-events");
			
			if(artistId != 0){
				mav.addObject("artist", DAORegistry.getArtistDAO().get(artistId));
			}
			
			mav.addObject("artistName", artistName);
			mav.addObject("events", events);	
			mav.addObject("venues", venues);
			mav.addObject("artists", artists);
			return mav;
		}
//			int eventId = Integer.parseInt(request.getParameter("id"));
//			Event event = DAORegistry.getEventDAO().get(eventId);
//
//			DAORegistry.getEventDAO().deleteById(eventId);
//			/***/
//			
//			/***/
//			String info = "The event " +event.getName()+" : "+ ((event.getLocalDate()== null)?"TBD":(event.getLocalDate()))
//					+ " has been deleted!";
//			return new ModelAndView(new RedirectView("EditorDetailTour?tourId="
//					+ tourId + "&info=" + info, true));
//		}
//		String info=request.getParameter("info");
//		if(info!=null){
//			mav.addObject("info", info);
//		}
//		
	}

	
	public ModelAndView loadEditorUpdateAutoCorrectEvent(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		String artistIdStr= request.getParameter("artistId");
		Integer artistId=Integer.parseInt(artistIdStr);
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		for(Event event:events){
			String autoCorrectStr =  request.getParameter(event.getId()+"-autoCorrect");
			String ignoreTBDTimeStr =  request.getParameter(event.getId()+"-ignoreTBDTime");
			if(autoCorrectStr!=null){
				boolean autoCorrect = "on".equalsIgnoreCase(autoCorrectStr)?true:false;
				if(autoCorrect){
					event.setAutoCorrect(autoCorrect);
				}
			}else{
				event.setAutoCorrect(false);
			}
			
			if(ignoreTBDTimeStr!=null){
				boolean ignoreTBDTime = "on".equalsIgnoreCase(ignoreTBDTimeStr)?true:false;
				if(ignoreTBDTime){
					event.setIgnoreTBDTime(ignoreTBDTime);
				}
			}else{
				event.setIgnoreTBDTime(false);
			}
		}
		DAORegistry.getEventDAO().saveOrUpdateAll(events);
		return new ModelAndView(new RedirectView(
				"EditorDetailArtist?artistId="+artistId+"&info=Events are saved.", true));
	}
	/*public ModelAndView loadEditorUploadEventCsvFilePage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-manage-data");
		if (request instanceof MultipartHttpServletRequest) {
			DateFormat formatDate = new SimpleDateFormat("M/d/y");
			DateFormat format12Time = new SimpleDateFormat("hh:mm aa");
			DateFormat format24Time = new SimpleDateFormat("HH:mm");

			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					multi.getFile("file").getInputStream()));
			String line;
			int lineNum = 0;
			String info = "";

			Collection<EventInfo> eventInfoList = new ArrayList<EventInfo>();
			while ((line = reader.readLine()) != null) {
				lineNum++;
				if (line.trim().isEmpty()) {
					continue;
				}

				if (lineNum == 1) {
					continue;
				}
				String[] tokens = line.split(",");

				if (tokens.length != 8) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid line.");
					return mav;
				}
				Integer eventId = null;
				if (!TextUtil.trimQuotes(tokens[0]).isEmpty()) {
					eventId = Integer.valueOf(TextUtil.trimQuotes(tokens[0]));					
				}
				
				String eventName = TextUtil.trimQuotes(tokens[1]);
				String typeStr = TextUtil.trimQuotes(tokens[2]);
				String eventDate = tokens[3].trim().split(" ")[0]; // only get the
															// date, ignore the
															// day name
				String time = tokens[4].trim();
				String building = TextUtil.trimQuotes(tokens[5]);
				
				String state = TextUtil.trimQuotes(tokens[6]);
				String country = TextUtil.trimQuotes(tokens[7]);

				eventDate = eventDate.trim();
				if (eventName.charAt(0) == '\"'
						&& eventName.charAt(eventName.length() - 1) == '\"') {
					eventName = eventName.substring(1, eventName.length() - 2);
				}
				
				TourType type;
				
				try {
					typeStr = typeStr.toUpperCase();
					if (typeStr.indexOf("SPORT") >= 0) {
						type = TourType.SPORT;
					} else if (typeStr.indexOf("CONCERT") >= 0) {
						type = TourType.CONCERT;						
					} else if (typeStr.indexOf("THEATER") >= 0) {
						type = TourType.THEATER;						
					} else {
						type = TourType.OTHER;
					}
				} catch (Exception e) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid type (sport, theater, other).");
					return mav;
				}

				Date date = null;
				Time eventTime = null;

				try {
					date = formatDate.parse(eventDate);

					if (time.equalsIgnoreCase("tbd")) {
						eventTime = null;
					} else if (time.toLowerCase().charAt(time.length() - 1) == 'm') {
						eventTime = new Time(format12Time.parse(time).getTime());
					} else {
						eventTime = new Time(format24Time.parse(time).getTime());
					}
				} catch (ParseException e) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid date or time.");
					e.printStackTrace();
					return mav;
				}
				
				System.out.println("** " + eventName);
				EventInfo eventInfo = new EventInfo(eventId, eventName, null, null, new java.sql.Date(date.getTime()), eventTime, type,
						building, "", state, country);
				eventInfoList.add(eventInfo);
			}
			
			EventCreator.createEvents(eventInfoList);
			mav.addObject("eventInfoList", eventInfoList);
			if (info.length() > 0) {
				mav.addObject("info", info
						+ "<b>Note: </b> The other events have been added.");
			}
		}

		return mav;
	}
*/
	/*public ModelAndView loadEditorUploadArtistTourEventCsvFilePage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-manage-data");
		if (request instanceof MultipartHttpServletRequest) {
			DateFormat formatDate = new SimpleDateFormat("M/d/y");
			DateFormat format12Time = new SimpleDateFormat("hh:mm aa");
			DateFormat format24Time = new SimpleDateFormat("HH:mm");

			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					multi.getFile("file").getInputStream()));
			String line;
			int lineNum = 0;
			String info = "";

			Collection<EventInfo> eventInfoList = new ArrayList<EventInfo>();
			while ((line = reader.readLine()) != null) {
				lineNum++;
				if (line.trim().isEmpty()) {
					continue;
				}

				if (lineNum == 1) {
					continue;
				}
				String[] tokens = line.split(",");

				if (tokens.length != 10) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid line. Incorrect number of fields.");
					return mav;
				}
				
				String artistName = TextUtil.trimQuotes(tokens[0]);
				String tourName = TextUtil.trimQuotes(tokens[1]);

				Integer eventId = null;
				if (!TextUtil.trimQuotes(tokens[2]).isEmpty()) {
					eventId = Integer.valueOf(TextUtil.trimQuotes(tokens[2]));					
				}
				
				String eventName = TextUtil.trimQuotes(tokens[3]);
				String typeStr = TextUtil.trimQuotes(tokens[4]);
				String eventDate = tokens[5].trim().split(" ")[0]; // only get the
															// date, ignore the
															// day name
				String time = tokens[6].trim();
				String building = TextUtil.trimQuotes(tokens[7]);
				
				String state = TextUtil.trimQuotes(tokens[8]);
				String country = TextUtil.trimQuotes(tokens[9]);

				eventDate = eventDate.trim();
				if (eventName.charAt(0) == '\"'
						&& eventName.charAt(eventName.length() - 1) == '\"') {
					eventName = eventName.substring(1, eventName.length() - 2);
				}
				
				TourType type;
				
				try {
					typeStr = typeStr.toUpperCase();
					if (typeStr.indexOf("SPORT") >= 0) {
						type = TourType.SPORT;
					} else if (typeStr.indexOf("CONCERT") >= 0) {
						type = TourType.CONCERT;						
					} else if (typeStr.indexOf("THEATER") >= 0) {
						type = TourType.THEATER;						
					} else {
						type = TourType.OTHER;
					}
				} catch (Exception e) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid type (sport, theater, other).");
					return mav;
				}

				if(tourName.isEmpty()) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid line. Tour Name cannot be empty.");
					return mav;
				}
				
				//if tour is null and artist is not found/empty
				Tour tour = DAORegistry.getTourDAO().getTourByName(tourName);
				
				if(tour == null){
					if(artistName.isEmpty()){
						mav.addObject("error", "Error Line " + lineNum + ": "
								+ line + ": Invalid line. Artist Name cannot be empty.");
						return mav;
					} 
					tour = new Tour();
					Artist artist = DAORegistry.getArtistDAO().getArtistByName(artistName);
					if(artist == null){
						artist = new Artist();
						artist.setName(artistName);
						DAORegistry.getArtistDAO().save(artist);
						artist = DAORegistry.getArtistDAO().getArtistByName(artistName);
					}
					tour.setName(tourName);
					tour.setArtist(artist);
					tour.setTourType(type);
					DAORegistry.getTourDAO().save(tour);
					tour = DAORegistry.getTourDAO().getTourByName(tourName);
				}
						
				Date date = null;
				Time eventTime = null;

				try {
					date = formatDate.parse(eventDate);

					if (time.equalsIgnoreCase("tbd")) {
						eventTime = null;
					} else if (time.toLowerCase().charAt(time.length() - 1) == 'm') {
						eventTime = new Time(format12Time.parse(time).getTime());
					} else {
						eventTime = new Time(format24Time.parse(time).getTime());
					}
				} catch (ParseException e) {
					mav.addObject("error", "Error Line " + lineNum + ": "
							+ line + ": Invalid date or time.");
					e.printStackTrace();
					return mav;
				}
				
				System.out.println("** " + eventName);
				EventInfo eventInfo = new EventInfo(eventId, eventName, tourName, artistName, new java.sql.Date(date.getTime()), eventTime, type,
						building, "", state, country);
				eventInfo.setTourId(tour.getId());
				eventInfoList.add(eventInfo);
			}
			
			EventCreator.createEvents(eventInfoList);
			mav.addObject("eventInfoList", eventInfoList);
			if (info.length() > 0) {
				mav.addObject("info", info
						+ "<b>Note: </b> The other events have been added.");
			}
		}

		return mav;
	}
*/
	public ModelAndView loadEditorDownloadEventCsvFilePage(
			HttpServletRequest request, HttpServletResponse response) 
	throws ServletException, IOException {
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		String today = format.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=events_" + today + ".csv");
//		response.getOutputStream().write("[unused field], event date, event time, building, sections, rows, category\n".getBytes());
		//event name, event type, event date, event time, venue
		response.getOutputStream().write("event_id, event_name, type, date, time, venue, state, country\n".getBytes());
		for (Event event: DAORegistry.getEventDAO().getAll()) {
			String timeStr;
			if (event.getTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(event.getTime());
			}
			String dateStr="TBD";
			if(event.getDate()!=null){
				dateStr=format.format(event.getDate());
			}
			String row = event.getId() + ", " + event.getName() + ", " + event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getTourType() + ", "
						+ dateStr + ", "
						+ timeStr + ", "
						+ event.getVenue().getBuilding() + ", " 
						+ event.getVenue().getState() + ", "
						+ event.getVenue().getCountry() 
						+ "\n";
			response.getOutputStream().write(row.getBytes());
		}
		
		return null;
	}
	
	
	public ModelAndView loadEditorAutomatedEventCreator(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		Property startHourProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.startHour");
		Property stopHourProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.stopHour");
		Property eventCreationModeProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.mode");
		if (action != null) {
			if (action.equals("Update")) {
				String startHour = request.getParameter("startHour");
				String stopHour = request.getParameter("stopHour");
				String mode = request.getParameter("mode");

				startHourProperty.setValue(startHour);
				stopHourProperty.setValue(stopHour);
				eventCreationModeProperty.setValue(mode);
				
				DAORegistry.getPropertyDAO().update(startHourProperty);
				DAORegistry.getPropertyDAO().update(stopHourProperty);
				DAORegistry.getPropertyDAO().update(eventCreationModeProperty);

				superAutomatedEventCreator.resetProperties();
			}
		}
		ModelAndView mav = new ModelAndView("page-editor-super-automated-event-creator");
		mav.addObject("automatedEventCreator", superAutomatedEventCreator);
		mav.addObject("startHour", startHourProperty.getValue());
		mav.addObject("stopHour", stopHourProperty.getValue());
		mav.addObject("mode", eventCreationModeProperty.getValue());
		return mav;
	}

	public ModelAndView loadEditSitesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		
		Map<String, Site> siteById = DAORegistry.getSiteDAO().getSiteByIdMap();
		
		if (action != null && action.equals("update")) {
			
//			Collection<Event> events = DAORegistry.getEventDAO().getAll();			
			for (String siteId: Constants.getInstance().getSiteIds()) {
				Site site = siteById.get(siteId);
				boolean createSite = (site == null);
				
				if (createSite) {
					site = new Site(siteId);					
				}
			
				PriceRounding oldPriceRounding = site.getPriceRounding();
				if (oldPriceRounding == null) {
					oldPriceRounding = PriceRounding.NONE;
				}
				
				PriceRounding priceRounding = PriceRounding.valueOf(request.getParameter("priceRounding_" + siteId));
				site.setPriceRounding(priceRounding);
				
				if (createSite) {
					DAORegistry.getSiteDAO().save(site);
				} else {
					DAORegistry.getSiteDAO().update(site);					
				}
			}
			
			String backUrl = request.getParameter("backUrl");
			if (backUrl == null || backUrl.length() == 0) {
				return new ModelAndView(new RedirectView("EditorEditSites?info=The site settings have been updated"));
			} else {
				String url;
				if (backUrl.contains("?")) {
					url = backUrl + "&info=The site settings have been updated";
				} else {
					url = backUrl + "?info=The site settings have been updated";					
				}
				return new ModelAndView(new RedirectView(url));				
			}
		}
		
		ModelAndView mav = new ModelAndView("page-edit-sites");
		mav.addObject("siteById", siteById);
		return mav;
	}

	public ModelAndView loadEditorManageEmptyArtists(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		ModelAndView mav = new ModelAndView("page-edit-empty-artists");
		if(action != null && !action.isEmpty()){
			if(action.equals("delete")){
				Set<String> params = request.getParameterMap().keySet();
				List<Integer> ids = new ArrayList<Integer>();
				for(String param : params){
					if(param.contains("artist-")){
						ids.add(Integer.parseInt(param.replaceAll("artist-", "")));
					}
				
				}
				if(!ids.isEmpty()){
					DAORegistry.getArtistDAO().bulkDelete(ids);
					mav.addObject("info", "Artists has been deleted.");
				}
				
			}//else if(action.equals("search")){
				String artistName = request.getParameter("artist");
				List<Artist> artists = new ArrayList<Artist>();
				Collection<Artist> artistList=null;
				if(artistName!=null && !artistName.isEmpty()){
					artistList = DAORegistry.getArtistDAO().filterByName(artistName);
				}else{
					artistList = DAORegistry.getArtistDAO().getAllActiveArtists();
				}
				for(Artist artist:artistList){
					if(DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artist.getId()).isEmpty()){
						artists.add(artist);
					}
				}
				mav.addObject("artists", artists);
				mav.addObject("artistName", artistName);
			//}
		}else{
			mav.addObject("Artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		}
		return mav;
	}
	public ModelAndView loadEditorManageExpiredEvents(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		String action = request.getParameter("action");
		if ("expire".equals(action)) {
			String eventIdsString = request.getParameter("eventIds");
			System.out.println("EVENTIDS=" + eventIdsString);
			
			String[] eventIdStrings = eventIdsString.split(",");
			Integer[] eventIds = new Integer[eventIdStrings.length];
			int i = 0;
			for (String eventIdString: eventIdStrings) {
				int eventId = Integer.parseInt(eventIdString);
				eventIds[i++] = eventId;
			}
			eventExpirationManager.expireEvents(eventIds);
//			CrawlerController.disableExpiredCrawl();
			return new ModelAndView(new RedirectView("DisableCrawlForExpiredEvents")); 
		} else {
			
			String showExpiredEventsString = request.getParameter("showExpiredEvents");
			boolean showExpiredEvents;
			if (showExpiredEventsString == null) {
				// read in the preference
				showExpiredEvents = "1".equals(preferenceManager.getPreferenceValue(username, "editorExpiredEventsShowExpiredEvents"));
			} else {
				showExpiredEvents = "1".equals(showExpiredEventsString);
			}
			
			preferenceManager.updatePreference(username, "editorExpiredEventsShowExpiredEvents", showExpiredEvents?"1":"0");
			
			ModelAndView mav = new ModelAndView("page-editor-manage-expired-events");
			Collection<Event> expiredEvents;
			
			if (showExpiredEvents) {
				expiredEvents = DAORegistry.getEventDAO().getAllEventsByDates(null, new Date(new Date().getTime() - 24L * 60L * 60L * 1000L), null);
			} else {
				expiredEvents = DAORegistry.getEventDAO().getAllEventsByDates(null, new Date(new Date().getTime() - 24L * 60L * 60L * 1000L), EventStatus.ACTIVE);
			}
				
			mav.addObject("events",expiredEvents);
			mav.addObject("showExpiredEvents", showExpiredEvents);
			return mav;
		}
	}

	/*public ModelAndView loadEditorValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-valuation-factors");
		Collection<Tour> tours = DAORegistry.getTourDAO().getAllToursWithEvents();
		mav.addObject("tours", tours);
		
		String action = request.getParameter("action");		
		Integer tourId;
		String tourIdStr = request.getParameter("tourId");
		if (tourIdStr == null) {
			tourId = tours.iterator().next().getId();
		} else {
			tourId = Integer.valueOf(tourIdStr);
		}

		Integer eventId = null;
		String eventIdStr = request.getParameter("eventId");
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByTour(tourId);
		mav.addObject("events", events);
		
		if (eventIdStr == null) {
			eventId = events.iterator().next().getId();
		} else {
			eventId = Integer.valueOf(eventIdStr);
		}
		
		if (action != null && action.equals("deleteAll")) {
			DAORegistry.getValuationFactorDAO().deleteAll(DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromTour(tourId));
			ValuationFactorUtil.update(tourId);
			return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
					+ tourId + "&eventId=" + eventId + "&info=Factors deleted", true));
		}
		
		mav.addObject("tourId", tourId);
		mav.addObject("eventId", eventId);
		mav.addObject("event", DAORegistry.getEventDAO().get(eventId));
		mav.addObject("valuationFactors", DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromEvent(eventId));
		return mav;
	}
	

	public ModelAndView loadEditorUploadSuperValuationFactorsFinish(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer tourId = Integer.valueOf(request.getParameter("tourId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		String valuationFactorsStr = request.getParameter("valuation_factors");
		
		Tour tour = DAORegistry.getTourDAO().get(tourId);
		try {
			parseValuationFactorFile(tourId, new ByteArrayInputStream(valuationFactorsStr.getBytes()));
		} catch (Exception e) {
			ModelAndView mav = new ModelAndView("page-editor-valuation-factors-preview");
			mav.addObject("valuationFactorText", valuationFactorsStr);
			mav.addObject("tour", tour);
			mav.addObject("eventId", eventId);
			mav.addObject("error", e.getMessage());
			e.printStackTrace();
			return mav;
		}
		return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
				+ tourId + "&eventId=" + eventId + "&info=Valuation Factors uploaded", true));
	}
			
	public ModelAndView loadEditorUploadSuperValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Integer tourId = Integer.valueOf(request.getParameter("tourId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));

		Tour tour = DAORegistry.getTourDAO().get(tourId);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (request instanceof MultipartHttpServletRequest) {
	
				MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
				ValuationFactorComputation valuationFactorComputation = valuationFactorParser.parse(multi.getFile("file").getInputStream());
				Collection<ValuationFactorComputation.ValuationFactor> valuationFactors = valuationFactorComputation.computeValuationFactors();
				String valuationFactorText = "";
				for (ValuationFactorComputation.ValuationFactor factor: valuationFactors) {
					String dateStr = "";
					if (factor.getStartDate().equals(factor.getEndDate())) {
						dateStr = dateFormat.format(factor.getStartDate());
					} else {
						dateStr = dateFormat.format(factor.getStartDate()) + "-" + dateFormat.format(factor.getEndDate());
					}
					
					valuationFactorText += dateStr + ", " + factor.getVenue() + ", " + factor.getSection() + ", " + factor.getRow() + ", " + factor.getFactor() + "\n"; 
				}
				
				ModelAndView mav = new ModelAndView("page-editor-valuation-factors-preview");
				mav.addObject("valuationFactorText", valuationFactorText);
				mav.addObject("eventId", eventId);
				mav.addObject("tour", tour);
				return mav;
			}
		} catch (Exception e) {
			return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
					+ tourId + "&eventId=" + eventId + "&error=" + e.getMessage(), true));					
		}

		return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
				+ tourId + "&eventId=" + eventId, true));
	}
	
	public ModelAndView loadEditorUploadValuationFactors(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Integer tourId = Integer.valueOf(request.getParameter("tourId"));
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		if (request instanceof MultipartHttpServletRequest) {
			MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
			try {
				parseValuationFactorFile(tourId, multi.getFile("file").getInputStream());
			} catch(Exception e) {
				return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
						+ tourId + "&eventId=" + eventId + "&error=" + e.getMessage(), true));
			}
			String info = "Valuation Factors uploaded sucessfully";
			return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
					+ tourId + "&eventId=" + eventId + "&info=" + info, true));
		}
		return new ModelAndView(new RedirectView("EditorValuationFactors?tourId="
				+ tourId + "&eventId=" + eventId, true));
	}
	
	private void parseValuationFactorFile(Integer tourId, InputStream inputStream) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		String line;
		int lineNum = 0;

		Collection<ValuationFactor> factors = new ArrayList<ValuationFactor>();
		while ((line = reader.readLine()) != null) {
			lineNum++;
			
			if (line.trim().length() == 0) {
				continue;
			}
			
			String[] tokens = line.split(",");
			
			if (tokens.length != 5) {
				String error = "Line:" + lineNum + ": " + line + ": Line is invalid ";
				throw new Exception(error);
			}
			
			String dateStr = TextUtil.trimQuotes(tokens[0]);
			String venueStr = TextUtil.trimQuotes(tokens[1]);
			String sections = TextUtil.trimQuotes(tokens[2]);
			String rows = TextUtil.trimQuotes(tokens[3]);
			String value = TextUtil.trimQuotes(tokens[4]);

			Date startDate = null;
			Date endDate = null;
			// parse dates
			if (dateStr.indexOf("-") > 0) {
				String[] dateTokens = dateStr.split("-");
				try {
					startDate = dateFormat.parse(dateTokens[0].trim());
					endDate = dateFormat.parse(dateTokens[1].trim());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				try {
					startDate = dateFormat.parse(dateStr);
					endDate = startDate;
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			Venue venue = DAORegistry.getVenueDAO().getVenue(venueStr);
			if (venue == null) {
				throw new Exception("No venue for '" + venueStr +"' found!");
			}
			
		 	Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByTourVenueAndDates(tourId, venue.getId(), startDate, endDate);
		 	for (Event event: events) {
				String startSection;
				String endSection;
				
				if (sections.indexOf("-") >= 0) {
					String[] sectionTokens = sections.split("-");
					startSection = sectionTokens[0];
					endSection = sectionTokens[1];
				} else {
					startSection = sections;
					endSection = sections;
				}

				String startRow;
				String endRow;
				
				if (rows.indexOf("-") >= 0) {
					String[] rowTokens = rows.split("-");
					startRow = rowTokens[0];
					endRow = rowTokens[1];
				} else {
					startRow = rows;
					endRow = rows;
				}

				ValuationFactor factor = new ValuationFactor(event.getId(), startSection, endSection,
											startRow, endRow, Integer.valueOf(value));
				factors.add(factor);
		 	}
		}
		
		// delete the old factors
		DAORegistry.getValuationFactorDAO().deleteAll(DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromTour(tourId));
		
		DAORegistry.getValuationFactorDAO().saveOrUpdateAll(factors);
		ValuationFactorUtil.update(tourId);
		
	}*/
	
	public ModelAndView loadEditorQuickCreate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Collection<Artist> artists = null;
//		Collection<Tour> tours = null;
		Collection<Venue> venues = null;
		Collection<TourCategory> parentCategories = null;
		Collection<ChildTourCategory> childCategories = null;
		Collection<GrandChildTourCategory> grandChildCategories = null;
		String venueId = request.getParameter("venueId");
		if( !(venueId == null) && !venueId.isEmpty()){
			venues = new ArrayList<Venue>(1);
			venues.add(DAORegistry.getVenueDAO().get(Integer.parseInt(venueId)));
		} else {
			venues = DAORegistry.getVenueDAO().getAll();
		}
		parentCategories = DAORegistry.getTourCategoryDAO().getAll();
		childCategories = DAORegistry.getChildTourCategoryDAO().getAll();
		grandChildCategories = DAORegistry.getGrandChildTourCategoryDAO().getAll();
//		String tourId = request.getParameter("tourId");
		String artistId = request.getParameter("artistId");
//		if( !(tourId == null) && !tourId.isEmpty()){
//			tours = new ArrayList<Tour>(1);
//			Tour tour = DAORegistry.getTourDAO().get(Integer.parseInt(tourId));
//			tours.add(tour);
//			
//			artists = new ArrayList<Artist>(1);
//			artists.add(DAORegistry.getArtistDAO().get(tour.getArtistId()));
//		} else {
			
			if( !(artistId == null) && !artistId.isEmpty()){
				artists = new ArrayList<Artist>(1);
				artists.add(DAORegistry.getArtistDAO().get(Integer.parseInt(artistId)));
				
//				tours = DAORegistry.getTourDAO().getAllToursByArtist(Integer.parseInt(artistId));
			} else {
				artists = DAORegistry.getArtistDAO().getAllActiveArtists();
//				tours = DAORegistry.getTourDAO().getAll();
			}			
//		}
		
		ModelAndView mav = new ModelAndView("page-editor-quick-create");
		mav.addObject("artists", artists);
//		mav.addObject("tours", tours);
		mav.addObject("parentCategories", parentCategories);
		mav.addObject("childCategories", childCategories);
		mav.addObject("grandChildCategories", grandChildCategories);
		mav.addObject("venues", venues);
		mav.addObject("artistId", artistId);
//		mav.addObject("tourId", tourId);
		mav.addObject("venueId", venueId);
		return mav;
	}
	
	public ModelAndView loadEditorLinkAdmitone(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String firstLetter = request.getParameter("firstChar");
		String getFromDB = request.getParameter("getFromDB");
		String eventId = request.getParameter("eventId");
		if(firstLetter ==null || firstLetter.isEmpty()){
			firstLetter="a%";
		}
		boolean isGetFromDB=false;
		if(getFromDB!=null && getFromDB.equals("on")){
			isGetFromDB=true;
		}
		Event toBeChangedevent=null;
		if(eventId!=null && !eventId.isEmpty()){
			try{
				toBeChangedevent = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
			}catch (Exception e) {
				System.out.println(e.fillInStackTrace());
			}
		}
		Collection<Event> events =null;
		if(toBeChangedevent==null){
			events = DAORegistry.getEventDAO().getUnLinkedEmptyEvents(firstLetter);
		}else{
			events = new ArrayList<Event>();
			events.add(toBeChangedevent);
			isGetFromDB=true;
		}

		/*String eventId = request.getParameter("eventId");
		if(eventId != null && !eventId.isEmpty() ){
			events = new ArrayList<Event>();
			for(String eventIdStr: eventId.split(",")) {
				events.add(DAORegistry.getEventDAO().get(Integer.parseInt(eventIdStr)));
			}
		}
		*/
		Map<Integer, Collection<AdmitoneEvent>> eventListMap = null;
		Integer admitoneEventId = null;
		if(events != null) {
			eventListMap = new HashMap<Integer,Collection<AdmitoneEvent>>();
			Collection<CreationEventHit> tnEvents = null;
			List<AdmitoneEvent> admitoneEventList = new ArrayList<AdmitoneEvent>();
			if(isGetFromDB){
				admitoneEventList = (List<AdmitoneEvent>) DAORegistry.getAdmitoneEventDAO().getAll();
				//admitoneEventList = (List<AdmitoneEvent>) DAORegistry.getAdmitoneEventDAO().getEventsByFirstChar(firstLetter);
				Collection<AdmitoneEventLocal> admitoneEventsLocal = DAORegistry.getAdmitoneEventLocalDAO().getAll();
				for(AdmitoneEventLocal admitoneEventLocal: admitoneEventsLocal){
					AdmitoneEvent admitoneEvent = new AdmitoneEvent(admitoneEventLocal);
					admitoneEventList.add(admitoneEvent);
				}
				//System.out.println("Now admitone EventList :"+admitoneEventList.size());
				/*for(AdmitoneEvent admitoneEvent: admitoneEvents){
					Collection<AdmitoneEvent> list = eventMaps.get(admitoneEvent.getVenueName());
					if(list==null){
						list = new ArrayList<AdmitoneEvent>();
					}
					list.add(admitoneEvent);
					eventMaps.put(admitoneEvent.getVenueName(), list);
				}*/
			}else{
				tnEvents = creationEventListManager.getTicketNetworkEventHits();
				System.out.println("tnEvents size"+tnEvents.size());
				
				for(CreationEventHit event: tnEvents){
					AdmitoneEvent admitoneEvent= new AdmitoneEvent();
					admitoneEvent.setEventDate(event.getDate());
					admitoneEvent.setEventId(event.getEventId());
					admitoneEvent.setEventName(event.getName());
					admitoneEvent.setEventTime(event.getTime());
					admitoneEvent.setVenueName(event.getBuilding());
					admitoneEventList.add(admitoneEvent);
				}
			////////
				admitoneEventList = (List<AdmitoneEvent>) DAORegistry.getAdmitoneEventDAO().getAll();
				//admitoneEventList = (List<AdmitoneEvent>) DAORegistry.getAdmitoneEventDAO().getEventsByFirstChar(firstLetter);
				Collection<AdmitoneEventLocal> admitoneEventsLocal = DAORegistry.getAdmitoneEventLocalDAO().getAll();
				for(AdmitoneEventLocal admitoneEventLocal: admitoneEventsLocal){
					AdmitoneEvent admitoneEvent = new AdmitoneEvent(admitoneEventLocal);
					admitoneEventList.add(admitoneEvent);
				}
				//////////
			}
			System.out.println("Begin :"+new Date());
			for(Event event : events) {
				Collection<AdmitoneEvent> similarEvents =new HashSet<AdmitoneEvent>(); 
//				Collection<AdmitoneEvent> dbEvents = DAORegistry.getAdmitoneEventDAO().getAllEventsNearDate(event.getDate());
//				if(dbEvents!=null){
//					similarEvents.addAll(dbEvents);
//				}
				if(event.getLocalDate()==null){
					continue;
				}
				/*Collection<AdmitoneEvent> list = eventMaps.get(event.getVenue().getBuilding());				
				if(list==null){
					continue;
				}*/
				for (AdmitoneEvent similarEvent: admitoneEventList) {
					if(similarEvent.getEventName().equalsIgnoreCase(event.getName()) && similarEvent.getVenueName().equalsIgnoreCase(event.getVenue().getBuilding())){
						similarEvents.add(similarEvent);
						if(similarEvent.getEventDate().equals(event.getLocalDate()) && similarEvent.getEventTime().equals(event.getLocalTime()))
							admitoneEventId = similarEvent.getEventId();
					}else{
						int score=QueryUtil.getMatchingScore(event.getName(), similarEvent.getEventName());
						if (score > 0) {
							//int venueScore = QueryUtil.getMatchingScore(event.getVenue().getBuilding(), similarEvent.getVenueName());
							//if(venueScore > 0)								
								similarEvents.add(similarEvent);
						}						
					}
				}		
				//sorting on date
				List<AdmitoneEvent> admitoneEvents = new ArrayList<AdmitoneEvent>(similarEvents);
				Collections.sort(admitoneEvents, new Comparator<AdmitoneEvent>() {

					public int compare(AdmitoneEvent o1, AdmitoneEvent o2) {
						return o1.getEventDate().compareTo(o2.getEventDate());
					}
					
				});
				eventListMap.put(event.getId(), admitoneEvents);
			}
			System.out.println("End :"+new Date());
		}
		
		ModelAndView mav = new ModelAndView("page-editor-link-admitone");
		mav.addObject("events", events);
		mav.addObject("admitoneEventId", admitoneEventId);
		mav.addObject("eventListMap", eventListMap);
		mav.addObject("firstChar", firstLetter);
		mav.addObject("getFromDB", isGetFromDB);
		return mav;
	}

	
	
	public ModelAndView loadEditorLinkDate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-link-date");
		String firstLetter = request.getParameter("firstChar");
		if(firstLetter ==null || firstLetter.isEmpty()){
			firstLetter="a%";
		}
		Collection<Event> events = DAORegistry.getEventDAO().getTBDEvents(firstLetter);

		Map<Integer, Collection<AdmitoneEvent>> eventListMap = null;
		
		if(events != null) {
			eventListMap = new HashMap<Integer,Collection<AdmitoneEvent>>();
			Map<Integer, AdmitoneEvent> eventMaps= new HashMap<Integer, AdmitoneEvent>();
			Collection<AdmitoneEvent> admitoneEvents = DAORegistry.getAdmitoneEventDAO().getAll();
			for(AdmitoneEvent admitoneEvent: admitoneEvents){
				eventMaps.put(admitoneEvent.getEventId(), admitoneEvent);
			}
			
			for(Event event : events) {
				Collection<AdmitoneEvent> similarEvents =new HashSet<AdmitoneEvent>(); 
				AdmitoneEvent admitoneEvent  = eventMaps.get(event.getAdmitoneId());
				if(admitoneEvent ==null){
					continue;
				}
				similarEvents.add(admitoneEvent);
				
				eventListMap.put(event.getId(), similarEvents);
			}
		}
		
		mav.addObject("events", events);
		mav.addObject("eventListMap", eventListMap);
		mav.addObject("firstChar", firstLetter);
		return mav;
	}
	public ModelAndView loadEditorManagePresaleEvent(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-editor-manage-presale");
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
		
		//List<Venue> venues = (List<Venue>)DAORegistry.getVenueDAO().getAll();
		List<Venue> venues = new ArrayList<Venue>();
				
		Integer artistId = null;
		Integer venueId = null;
		String artistName = new String();
		if(artistIdString!=null && !artistIdString.isEmpty()){
			artistId = Integer.parseInt(artistIdString);
			Artist artist = DAORegistry.getArtistDAO().get(artistId);
			artistName = artist.getName();
			
			Collection<Event> listOfeventsTemp = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		    List<Event> eventsTemp = new ArrayList();
		    for (Event event : listOfeventsTemp) {
		    	event.getVenue().setVenueCategoryList(DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(event.getVenueId()));
		    	eventsTemp.add(event);
			} 
			for (Event event :eventsTemp) {
				Collection<Venue> listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
				for (Venue venue : listofVenue) {
					if(!venues.contains(venue)){
					venues.add(venue);
					}
				}
                 // DAORegistry.getVenueDAO().getVenue(event.getVenue())
			}
			mav.addObject("venues", venues);
		}
		if(venueIdString!=null && !venueIdString.isEmpty()){
			venueId = Integer.parseInt(venueIdString);
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
		}

		
		String action = request.getParameter("action");
		
		
		
		Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getAll();
		
		if(action!=null && action.equalsIgnoreCase("update")){
			Map<Integer,Event>eventMap = new HashMap<Integer, Event>();
			Collection<Event> events  = null;
			if(artistId!=null && venueId!=null){
				events = DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId, venueId, EventStatus.ACTIVE);
			}else if(artistId!=null){
				events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			}else if(venueId!=null){
				events = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
			}else{
				events = DAORegistry.getEventDAO().getAllEligiblePresaleEventsByArtistIdAndVenueId(artistId, venueId);
			}
			for(Event event: events){
				eventMap.put(event.getId(), event);
			}
			Collection<PresaleEvent> presaleEvents = DAORegistry.getPresaleEventDAO().getAllPresaleEventsByArtistIdAndVenueId(artistId, venueId);
			Map<Integer,PresaleEvent> presaleEventMap = new HashMap<Integer, PresaleEvent>();
			for(PresaleEvent presaleEvent:presaleEvents){
				presaleEventMap.put(presaleEvent.getEvent().getId(), presaleEvent);
			}
			
			
			Map<Integer,VenueCategory> venueCategoryMap = new HashMap<Integer, VenueCategory>();
			for(VenueCategory venueCategory:venueCategories){
				venueCategoryMap.put(venueCategory.getId(), venueCategory);
			}
			
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List<PresaleEvent> presaleEventUpdateList = new ArrayList<PresaleEvent>();
			for(Object object :request.getParameterMap().keySet()){
				String key = (String)object;
//				String param = request.getParameter(key);
				
				if(!key.contains("checkbox")){
					continue;
				}
				
				Integer venueCategryId = null;
				Integer eventId = null;
				Integer paramVenueId = null;
				String applyDate = null;
				Integer applyDateHour = null;
				Integer applyDateMinute = null;
				
				String temp[] = key.split("-");
				eventId = Integer.parseInt(temp[0]);
				paramVenueId = Integer.parseInt(temp[1]);
				applyDate = request.getParameter(eventId + "-" + paramVenueId +"-" + "applyDate");
				applyDateHour = Integer.parseInt(request.getParameter(eventId + "-" + paramVenueId +"-" + "applyDateHour"));
				applyDateMinute = Integer.parseInt(request.getParameter(eventId + "-" + paramVenueId +"-" + "applyDateMinute"));
				
				String venueCategoryIdStr = request.getParameter(eventId + "-" + paramVenueId +"-" + "venueCategory");
				if(venueCategoryIdStr != null && !venueCategoryIdStr.isEmpty() && !venueCategoryIdStr.equals("0")) {
					venueCategryId = Integer.parseInt(venueCategoryIdStr);	
				}
				
				
				Date now = new Date();
				if(eventId!=null){
					
					if(null != applyDate && !applyDate.isEmpty() && !applyDate.equals("")){
						Calendar cal = new GregorianCalendar();
						try {
							cal.setTime(df.parse(applyDate));
							PresaleEvent presaleEvent =  presaleEventMap.get(eventId);
							if(presaleEvent==null){
								presaleEvent = new PresaleEvent();
								presaleEvent.setEvent(eventMap.get(eventId));
								presaleEvent.setStatus("ACTIVE");
								presaleEvent.setCreatedBy(username);
								presaleEvent.setCreatedDate(now);
							}
							if(venueCategryId != null) {
								presaleEvent.setAssignToVenueCategory(venueCategoryMap.get(venueCategryId));	
							} else {
								presaleEvent.setAssignToVenueCategory(null);
							}
							
							presaleEvent.setApplyDate(cal.getTime());
							String applyDateTemp = dateFormat.format(cal.getTime());
							applyDateTemp = applyDateTemp+" "+(applyDateHour < 9?"0"+applyDateHour:applyDateHour)+":"+(applyDateMinute < 9?"0"+applyDateMinute:applyDateMinute)+":00";
							presaleEvent.setApplyTime(sdfTime.parse(applyDateTemp));
							
							presaleEvent.setLastUpdatedBy(username);
							presaleEvent.setLastUpdated(now);
							
							presaleEventUpdateList.add(presaleEvent);
						} catch (ParseException e) {
							System.out.println("Error :"+e.getLocalizedMessage());
							e.printStackTrace();
						}
					}
				}
			}
			String info = "Presale Events Updated Successfully.";
			mav.addObject("info", info);
			try{
				DAORegistry.getPresaleEventDAO().saveOrUpdateAll(presaleEventUpdateList);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		Collection<Event> eligiblePresaleEvents = null;
		eligiblePresaleEvents = DAORegistry.getEventDAO().getAllEligiblePresaleEventsByArtistIdAndVenueId(artistId,venueId);
		Map<Integer,Event> elegibleEventMap = new HashMap<Integer, Event>();
		for(Event eligiblePresaleEvent: eligiblePresaleEvents){
			elegibleEventMap.put(eligiblePresaleEvent.getId(), eligiblePresaleEvent);
		}
		Collection<PresaleEvent> presaleEvents = DAORegistry.getPresaleEventDAO().getAllPresaleEventsByArtistIdAndVenueId(artistId, venueId);
		Collection<PresaleEvent> presaleEventList = new ArrayList<PresaleEvent>();
		
//		Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getAll();
		Map<Integer,List<VenueCategory>> venueCategoryMap = new HashMap<Integer, List<VenueCategory>>();
		
		for(VenueCategory venueCategory:venueCategories){
			List<VenueCategory> list = venueCategoryMap.get(venueCategory.getVenue().getId());
			if(list ==null){
				list= new ArrayList<VenueCategory>();
			}
			list.add(venueCategory);
			venueCategoryMap.put(venueCategory.getVenue().getId(), list);
		}
		
		for(PresaleEvent presaleEvent: presaleEvents){
			elegibleEventMap.remove(presaleEvent.getEvent().getId());
			presaleEvent.setVenueCategoryList(venueCategoryMap.get(presaleEvent.getEvent().getVenueId()));
			presaleEventList.add(presaleEvent);
		}
		
		for(Event event:elegibleEventMap.values()){
			PresaleEvent  presaleEvent = new PresaleEvent();
			presaleEvent.setEvent(event);
			presaleEvent.setVenueCategoryList(venueCategoryMap.get(presaleEvent.getEvent().getVenueId()));
			presaleEventList.add(presaleEvent);
		}
		mav.addObject("presaleEvents", presaleEventList);
		//mav.addObject("artists", artists);
		mav.addObject("venues", venues);
		mav.addObject("artistId", artistIdString);
		mav.addObject("artistName", artistName);
		mav.addObject("venueId", venueIdString);
		
		return mav;
	
	}
	public ModelAndView loadEditorManageTCAP(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
//		String username = (String) request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
//		String action = request.getParameter("action");
		Collection<Event> events = null;
		String artistIdString = request.getParameter("artistId");
		String venueIdString = request.getParameter("venueId");
		EventPriceAdjustment eventPriceAdjustment = null;
		AdmitoneEvent admitoneEvent=null;
		
		Integer eventId;
		Integer admitoneId;
		ModelAndView mav = new ModelAndView("page-editor-manage-tcap");
		
		//Tamil : select artist with minimum one valid events only
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtistsWithEvents();
		//List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
	
		//List<Venue> venues = (List<Venue>)DAORegistry.getVenueDAO().getAll();
		List<Venue> venues = new ArrayList<Venue>();
		
		/*Collections.sort(artists, new Comparator<Artist>(){
			public int compare(Artist t1,	Artist t2) {
				AlphanumericSorting ans = new AlphanumericSorting();
				return ans.compare(t1.getName(), t2.getName());
			}
		}
		);*/
		
		//mav.addObject("artists", artists);
		//mav.addObject("venues", venues);
		
		if (artistIdString == null && venueIdString == null) {
			// look for a value in the preference
			//tourIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerTourId");
			mav.addObject("events", "");
			mav.addObject("artistName","");
			mav.addObject("editorTCAPArtistId", "");
			//mav.addObject("editorTCAPEventId", Integer.toString(eventId));	
			mav.addObject("editorTCAPVenueId", "");
			return mav;
		}
		
		int artistId = 0;
		//List listOfvenueId=new ArrayList();
		String artistName = new String();
		String venueName = new String();
		Collection<Venue> listofVenue;
		if (artistIdString != null && artistIdString.length() > 0 && !artistIdString.equals("0")) {
			try{	
				artistId = Integer.parseInt(artistIdString);
				venues=new ArrayList();
			}catch(Exception e){
				e.printStackTrace();
			}
			Artist artist = DAORegistry.getArtistDAO().get(artistId);
			artistName = artist.getName();
			if (artistId != 0 && artistId != -1 && artist == null) {
				artistId = 0;
			}else{
				if(artistId == -1){
					events = DAORegistry.getEventDAO().getAllActiveEvents();
				}else{
					events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
					for (Event event :events) {
						listofVenue=DAORegistry.getVenueDAO().getVenueById(event.getVenueId());
						for (Venue venue : listofVenue) {
							if(!venues.contains(venue)){
							venues.add(venue);
							}
						}
                         // DAORegistry.getVenueDAO().getVenue(event.getVenue())
					}
					mav.addObject("venues", venues);
				}
			}
		}
		
		int venueId = 0;
		if (venueIdString!=null && venueIdString.length()>0 &&!venueIdString.equals("0")){
			Venue venue = null;
			try{
			venueId = Integer.parseInt(venueIdString);
			venue = DAORegistry.getVenueDAO().get(venueId);
			venueName = venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry();
			mav.addObject("venueName", venueName);
			}catch(Exception e){
				e.printStackTrace();
			}
			if (venueId != 0 && venue == null) {
				venueId = 0;
			}else{
			events = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId,EventStatus.ACTIVE);
		}
		}
		
		if(venueIdString!=null && artistIdString!=null && venueIdString.length()>0 && artistIdString.length() > 0 &&!venueIdString.equals("0") && !artistIdString.equals("0")){
		     	try{
		     		venueId = Integer.parseInt(venueIdString);
		     		artistId = Integer.parseInt(artistIdString);
		     	}catch(Exception e){
		     		e.printStackTrace();
		     	}
		     	events = DAORegistry.getEventDAO().getAllEventsByArtistAndVenueAndStatus(artistId,venueId,EventStatus.ACTIVE);
		}
		String siteId = "stubhub";
		ArtistPriceAdjustment tourPriceAdjustment = DAORegistry.getArtistPriceAdjustmentDAO().get(new ArtistPriceAdjustmentPk(artistId, siteId));
		Double percentAdjustment = 0.0;
		String admitoneEventNameDateTime=null;
	
		if (events!=null){
			for (Event event:events){
				eventId = event.getId();
				admitoneId=event.getAdmitoneId();
				
				eventPriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(new EventPriceAdjustmentPk(eventId, siteId));
				admitoneEvent =DAORegistry.getAdmitoneEventDAO().getEventByEventId(admitoneId);
				if(admitoneEvent!=null)
				{
				 DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				 DateFormat tf = new SimpleDateFormat("HH:mm");
				  admitoneEventNameDateTime = admitoneEvent.getEventName()+","+df.format(admitoneEvent.getEventDate())+","+tf.format(admitoneEvent.getEventTime());		
				  event.setAdmitoneEventDetails(admitoneEventNameDateTime);
				}
				if (eventPriceAdjustment!=null){
					percentAdjustment = eventPriceAdjustment.getPercentAdjustment();
				}
				else {
					if (tourPriceAdjustment != null){
						percentAdjustment = tourPriceAdjustment.getPercentAdjustment();
					}
				}
				mav.addObject("shPercentAdjustment",percentAdjustment);
			}
		}
		
//		if(tourPriceAdjustment != null){
//			mav.addObject("shtourPriceAdjustment",tourPriceAdjustment.getPercentAdjustment());
//		}
		mav.addObject("events", events);
		mav.addObject("artistName",artistName);
		mav.addObject("editorTCAPArtistId", Integer.toString(artistId));
		//mav.addObject("editorTCAPEventId", Integer.toString(eventId));	
		mav.addObject("editorTCAPVenueId", Integer.toString(venueId));
		return mav;				
	}
	public void loadEditorManageTCAPAllTourCsvGeneration(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
//		List<ManageTcapAllTourCsvGenerate> events = DAORegistry.getEventDAO().getAllActiveEventCsvData();
//		String priceAdjustment = request.getParameter("priceAdjustment");		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"TCAP.csv\"");
		response.setHeader("Content-Transfer-Encoding", "binary");
		PrintWriter pw = response.getWriter();				
		pw.println("Name,Date,Venue,Tour,Parent,Child,GrandChile,Crawl,Zones,AutoCorrect,IgnoreTBDTime,IsMap,Link,PriceAdjustment");
		
		/*for(ManageTcapAllTourCsvGenerate event : events){
			catGroupName = DAORegistry.getCategoryDAO().getCategorieGroupByVenueCategoryId(event.getVenueCategoryId());
			File file = new File("C:/TMATIMAGESFINAL/" + event.getVenueId() + "_" +catGroupName+ ".gif");		
			if (!file.exists()) {
					map = "no";
				}else{
					map="yes";
				}
			
			pw.println(event.getEventName()+","+event.getEventDateTime()+","+event.getVenueName()+","+event.getTourName()+","+event.getTourCategoryName()
					+","+event.getChildCategoryName()+","+event.getGrandChildCategory()+","+event.getCrawlCount()+","+event.getCategoryGroupName()
					+","+event.getAutoCorrect()+","+event.getIgnoreTbdTime()+","+map+","+event.getLink()+","+priceAdjustment);
			
		}*/
		pw.flush();
		pw.close();
		
	}
	
	
	// for browsing cheapest excluded tickets
	public ModelAndView loadExcludedCheapestTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action");		
		String artistIdStr = request.getParameter("artistId");
		String eventIdStr = request.getParameter("eventId");
		String sectionSelected = request.getParameter("section");
		String rowSelected = request.getParameter("row");
		String seatSelected = request.getParameter("seat");
		String categorySelected = request.getParameter("category");
		Map<String, Boolean> siteFilters = new HashMap<String, Boolean>();
		
		ModelAndView mav = new ModelAndView("page-editor-view-excluded-tickets");
		List<Artist> artists = new ArrayList<Artist>();
		ArrayList<Integer> artistIds = new ArrayList<Integer>();
//		List<Integer> eventIds = new ArrayList<Integer>();
		Collection<Event> events = new ArrayList<Event>();
		List<String> siteIds = new ArrayList<String>();
		List<String> sections = new ArrayList<String>();
		List<String> rows = new ArrayList<String>();
		List<String> seats = new ArrayList<String>();
		List<String> categorys = new ArrayList<String>();
		//add here change for getting tour and events
		if(artistIdStr==null || artistIdStr.equals("")){
			artistIdStr = "ALL";
		}
		if (artistIdStr.equals("ALL") && "".equals(action)) {
			eventIdStr = "ALL";
		}
		categorys = DAORegistry.getExcludedCheapestTicketDAO().getCategorys(artistIdStr,eventIdStr);
		sections = DAORegistry.getExcludedCheapestTicketDAO().getDistinctSections(artistIdStr,eventIdStr,categorySelected);
		rows = DAORegistry.getExcludedCheapestTicketDAO().getDistinctRows(artistIdStr,eventIdStr,categorySelected,sectionSelected);
		seats = DAORegistry.getExcludedCheapestTicketDAO().getDistinctSeats(artistIdStr,eventIdStr,categorySelected,sectionSelected,rowSelected);
		
		events = DAORegistry.getExcludedCheapestTicketDAO().getDistinctEvent();
		for(Event event:events){
			if(!artistIds.contains(event.getArtist().getId())){
				artistIds.add(event.getArtist().getId());
				artists.add(event.getArtist());
			}	
		}
		
		Collections.sort(artists, new TourComparator());
		
		siteIds = DAORegistry.getExcludedCheapestTicketDAO().getDistinctSiteIds();		
		Collection<String> siteList = new ArrayList<String>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			String value = request.getParameter("checkbox-" + siteId);
			if (value != null && !value.isEmpty()) {
				siteFilters.put(siteId, true);
				siteList.add(siteId);
			} else {
				siteFilters.remove(siteId);
			}
		}
		if(siteFilters.size()==0){
		for(String site:siteIds){
			siteFilters.put(site, true);	
		}
		}
		mav.addObject("siteFilters", siteFilters);
		mav.addObject("siteIds", siteIds);
		if (artists.isEmpty()){
			return mav;
		}
		
		mav.addObject("artists", artists);
		mav.addObject("sections", sections);
		mav.addObject("rows", rows);
		mav.addObject("seats", seats);
		mav.addObject("categorys", categorys);
		mav.addObject("sectionSelected", sectionSelected);
		mav.addObject("rowSelected", rowSelected);
		mav.addObject("seatSelected", seatSelected);
		mav.addObject("categorySelected", categorySelected);		
		
		Integer artistId = null;
		Integer eventId = null;
		
		
		
		if (artistIdStr == null || artistIdStr.isEmpty()) {

			return mav;
		} 
		else if (artistIdStr.equals("ALL") && "".equals(action)) {
			artistId = null;
			eventIdStr = "ALL";
			mav.addObject("artistId", "ALL");
			mav.addObject("eventId", "ALL");			
			return mav;
		}else if(artistIdStr.equals("ALL") && !"".equals(action)){
			artistId = null;
			eventIdStr = "ALL";
			mav.addObject("tourId", "ALL");
			mav.addObject("eventId", "ALL");
		}
		
		else {
			artistId = Integer.valueOf(artistIdStr);
//			if (tours.isEmpty()) {
//				tourId = null;
//				mav.addObject("tourId", "ALL");
//				mav.addObject("eventId", "ALL");
//			}
			if (artistId!=0 && DAORegistry.getArtistDAO().get(artistId) == null){
				artistId = 0;
			}
			
			
		}
		

		if (artistId != null && artistId!=0) {			
			events.clear();			
			events = DAORegistry.getExcludedCheapestTicketDAO().getDistinctEvents(artistId);			
			
			if (events.isEmpty()){
				return mav;
			}
			mav.addObject("artistId", artistId);
			mav.addObject("events", events);

			if (eventIdStr == null || eventIdStr.isEmpty()) {
//				
					return mav;
				}
			 else if (eventIdStr.equals("ALL")) {
				eventId = null;
				mav.addObject("eventId", "ALL");
			} 
			else {
				eventId = Integer.valueOf(eventIdStr);			
				mav.addObject("eventId",eventId);
			} 
		}
		if (action == null || action.equals("")||(artistId ==null && eventId ==null && !"ALL".equals(artistIdStr))){
				return mav;
		}else if ("search".equals(action) && (siteList.size()>0)) {
		Collection<ExcludedCheapestTicket> excludeCheapestTickets= new ArrayList<ExcludedCheapestTicket>();		
		if(artistId!=null && eventId==null){			
			excludeCheapestTickets = DAORegistry.getExcludedCheapestTicketDAO().getAllExcludedTicketsByTourID(artistId,categorySelected,sectionSelected,rowSelected,seatSelected,siteList);	
		}else if(artistId!=null && eventId!=null){
			excludeCheapestTickets = DAORegistry.getExcludedCheapestTicketDAO().getAllExcludedTicketsByEventID(eventId,categorySelected,sectionSelected,rowSelected,seatSelected,siteList);
		}else if("ALL".equals(artistIdStr)){
			excludeCheapestTickets = DAORegistry.getExcludedCheapestTicketDAO().getAllExcludedTickets(categorySelected,sectionSelected,rowSelected,seatSelected,siteList);	
		}	
		
		mav.addObject("excludedcheapesttickets",excludeCheapestTickets);
		}
			
		return mav;
	}
	
	
	
	public ModelAndView loadManageTourCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("tourCategoryId");
		List<ChildTourCategory> childTourCatList;
//		List<GrandChildTourCategory> grandChildTourCatList;
		String info="";
		if(id != null){
			childTourCatList = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(id));
			if(childTourCatList.size() == 0){
				DAORegistry.getTourCategoryDAO().deleteById(Integer.parseInt(id));
				info="Tour Category Removed Successfully.";
				//grandChildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().get
			}else{
				info="Cannot Remove it Contains  "+childTourCatList.size()+"  Child Tour Category First Remove It.";
			}
			System.out.println("tour category id==:"+id);
		}
		ModelAndView mav = new ModelAndView("page-editor-manage-tour-category");
		mav.addObject("tourCategorys", DAORegistry.getTourCategoryDAO().getAll());
		mav.addObject("info", info);
		return mav;
	}
	public ModelAndView loadManageChildTourCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-editor-manage-child-tour-category");
		Collection<ChildTourCategory> childTourCategories=null;
		String info="";
		String category= request.getParameter("category");
		String action = request.getParameter("action");
		if(action!=null && action.equals("remove")){
			String tobeRemoved = request.getParameter("tobeRemoved");
			if(tobeRemoved != null && !"".equals(tobeRemoved)){
				
				String[] removeList=tobeRemoved.split(",");
				for(String temp:removeList){
					String params[]=temp.split(":");
					Collection<GrandChildTourCategory> grandChildTourCatList = DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(params[0]));
					if(!grandChildTourCatList.isEmpty()){
						System.out.println("empty");
						if(params.length == 2){
							info+=params[1]+", " ;
						}
						
					}else{
						DAORegistry.getChildTourCategoryDAO().deleteById(Integer.parseInt(params[0]));
					}
				}
				
				if("".equals(info)){
					info="Selected Child Category Removed.";
				}else{
					info+=" Child Categories are not removed as it contains Grand Child Tour Category.";
				}
			}else{
				info="Please Select Atleast One Category.";
			}
		}
		
		if(category==null ){
			category="0";
		}
		if( category.equals("0")){
			childTourCategories=DAORegistry.getChildTourCategoryDAO().getChildTourCategoryOrderByName();
		}else{
			childTourCategories=DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(category));
		}
		mav.addObject("categoryList",DAORegistry.getTourCategoryDAO().getAllTourCategoryOrderByName());
		mav.addObject("categoryString",category);
		
		mav.addObject("childTourCategories", childTourCategories);
		mav.addObject("info", info);
		return mav;
	}
	public ModelAndView loadManageGrandChildTourCategory(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			String childCategory= request.getParameter("childCategory");
			String parentCategoryString= request.getParameter("parentCategory");
			String action = request.getParameter("action");
			String info="";
			if(action!=null && action.equals("remove")){
				String tobeRemoved = request.getParameter("tobeRemoved");
				if(tobeRemoved != null && !"".equals(tobeRemoved)){
					String[] removeList=tobeRemoved.split(",");
					List<Integer> ids= new ArrayList<Integer>();
					for(String temp:removeList){
						String params[]=temp.split(":");
						Collection<Artist> artists= DAORegistry.getArtistDAO().getAllActiveArtistsByGrandChildCategoryId(Integer.parseInt(params[0]));
						if(artists!= null && artists.size()!=0){
							info+= params[1] + ",";
						}else{
							ids.add(Integer.parseInt(params[0]));
						}
					
					}
					if(ids.size()!=0){
						DAORegistry.getGrandChildTourCategoryDAO().deleteAllById(ids);
					}
					if(!"".equals(info)){
						info = "Artists exists for " + info.substring(0,info.length()-1) + ". Can not be removed."; 
					}else{
						info ="Grand ChildCategory removed successfully";
					}
				}else{
					info="Please Select Atleast One Category.";
				}
			}
 			Collection<GrandChildTourCategory> grandChildTourCategories=null;
 			if(parentCategoryString==null){
 				parentCategoryString="0";
 			}
			if(childCategory==null ){
				childCategory="0";
			}
			if( childCategory.equals("0")){
				grandChildTourCategories=DAORegistry.getGrandChildTourCategoryDAO().getAllGrandChildTourOrderByName();
			}else{
				grandChildTourCategories=DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByChildTourCategory(Integer.parseInt(childCategory));
			}
			ModelAndView mav = new ModelAndView("page-editor-manage-grand-child-tour-category");
			mav.addObject("info", info);
			List<ChildTourCategory> childTourCategoryList ;
			if(parentCategoryString!=null && !"0".equals(parentCategoryString)){
				childTourCategoryList=DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(parentCategoryString));
			}else{
				childTourCategoryList=DAORegistry.getChildTourCategoryDAO().getChildTourCategoryOrderByName();
			}
				
			mav.addObject("childCategoryList",childTourCategoryList);
			mav.addObject("parentCategoryList",DAORegistry.getTourCategoryDAO().getAllTourCategoryOrderByName());
			mav.addObject("childCategoryString",childCategory);
			mav.addObject("parentCategoryString",parentCategoryString);
			mav.addObject("grandChildTourCategorys",grandChildTourCategories );
		return mav;
	}
	public ModelAndView loadEventAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	ModelAndView mav = new ModelAndView("page-event-audit-popup");
	String eventId= request.getParameter("eventId");
	System.out.println("eventId : "+eventId);
	Event event = DAORegistry.getEventDAO().get(Integer.parseInt(eventId));
	List<EventAudit> list=DAORegistry.getEventAuditDAO().getEventAuditByEventId(Integer.parseInt(eventId));
	mav.addObject("auditList", list);	
	mav.addObject("event", event);
	return mav;	
}
	public ModelAndView loadRemovedEventAudit(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	String artistIdString = request.getParameter("artistId");
	String venueIdString = request.getParameter("venueId");
	List<EventAudit> list=null;
	ModelAndView mav = new ModelAndView("page-removed-event-audit");
	//List<EventAudit> list=DAORegistry.getEventAuditDAO().getEventRemovedByAction();
	//mav.addObject("auditList", list);	
	/*List<Artist> artists = (List<Artist>) DAORegistry.getArtistDAO().getAllActiveArtists();
	List<Venue> venues = (List<Venue>)DAORegistry.getVenueDAO().getAll();
	Collections.sort(artists, new Comparator<Artist>(){
		public int compare(Artist t1,	Artist t2) {
			AlphanumericSorting ans = new AlphanumericSorting();
			return ans.compare(t1.getName(), t2.getName());
		}
	}
	);
	mav.addObject("tours", artists);
	mav.addObject("venues", venues);*/
	if (artistIdString == null && venueIdString == null) {
		// look for a value in the preference
		//tourIdString = preferenceManager.getPreferenceValue(username, "editorCrawlerTourId");
		return mav;
	}
	
	int artistId = 0;
	if (artistIdString != null && artistIdString.length() > 0) {
		try{	
			artistId = Integer.parseInt(artistIdString);
		}catch(Exception e){
			e.printStackTrace();
		}
		Artist artist = DAORegistry.getArtistDAO().get(artistId);
		if (artistId != 0 && artistId != -1 && artist == null) {
			artistId = 0;
		}else{
			if(artistId == -1){
				//list = DAORegistry.getEventAuditDAO().getEventRemovedByAction();
			}else{
				mav.addObject("artistName", artist.getName());
				list = DAORegistry.getEventAuditDAO().getAllEventsByTourId(artistId);
				mav.addObject("selectedArtistId",
						Integer.valueOf(artistIdString.trim()));
			}
		}
	}
	
	int venueId = 0;
	if (venueIdString!=null && venueIdString.length()>0){
		try{
		venueId = Integer.parseInt(venueIdString);
		}catch(Exception e){
			e.printStackTrace();
		}
		Venue venue = DAORegistry.getVenueDAO().get(venueId);
		if (venueId != 0 && venue == null) {
			venueId = 0;
		}else{
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
			list = DAORegistry.getEventAuditDAO().getAllEventsByVenueId(venueId);
			mav.addObject("selectedVenueId",
					Integer.valueOf(venueIdString.trim()));
	}
	}
	mav.addObject("auditList", list);	
	return mav;	
	}	
	
	
	
	public PreferenceManager getPreferenceManager() {
		return preferenceManager;
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}

	public void afterPropertiesSet() throws Exception {
		if (valuationFactorParser == null) {
			throw new RuntimeException("Property valuationFactorParser must be set");
		}

		if (superAutomatedEventCreator == null) {
			throw new RuntimeException("Property superAutomatedEventCreator must be set");
		}

		if (eventExpirationManager == null) {
			throw new RuntimeException("Property eventExpirationManager must be set");
		}

		if (preferenceManager == null) {
			throw new RuntimeException("Property preferenceManager must be set");
		}
	}	
	class TourComparator implements Comparator<Artist>{

		@Override
		public int compare(Artist o1, Artist o2) {
			 return o1.getName().compareTo(o2.getName());
		}
		
	}
	
	
	public ModelAndView loadEditorEditVenueSynonym(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-editor-edit-venue-synonym");
		
		String venueIdStr =  request.getParameter("venueId");
		
		Integer venueId = null;
		if(null != venueIdStr && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr.trim());
		}else{
		}
		Collection<ProbableVenue> probableVenues = DAORegistry.getProbableVenueDAO().getAll();
		
		Map<Integer, List<ProbableVenue>> probableVenuMap = new HashMap<Integer, List<ProbableVenue>>();
		Map<Integer, ProbableVenue> probableVenueById = new HashMap<Integer, ProbableVenue>();
		for (ProbableVenue obj : probableVenues) {
			List<ProbableVenue> list = probableVenuMap.get(obj.getVenueId());
			if(list != null && !list.isEmpty()){
				probableVenuMap.get(obj.getVenueId()).add(obj);
			}else{
				list = new ArrayList<ProbableVenue>();
				list.add(obj);
				probableVenuMap.put(obj.getVenueId(), list);
			}
			probableVenueById.put(obj.getId(), obj);
		}
				
		String action = request.getParameter("action");
		if(action!=null && action.equalsIgnoreCase("update")){
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			List<ProbableVenue> toBeUpdatedProbableList = new ArrayList<ProbableVenue>();
			for(Object object :request.getParameterMap().keySet()){
				String key = (String)object;
				
				if(!key.contains("checkbox")){
					continue;
				}
				
				Integer gridVenueId = null;
				
				String temp[] = key.split("-");
				gridVenueId = Integer.parseInt(temp[0]); 
				String[] synonymIds = request.getParameterValues(gridVenueId+"-" + "synonym");
				
				if(gridVenueId!=null){
					if(null != synonymIds && !synonymIds.equals("") && synonymIds.length > 0){
						for (String idStr : synonymIds) {
							try{
								Integer probableVenueId = Integer.parseInt(idStr);
								ProbableVenue probableVenue = probableVenueById.get(probableVenueId);
								probableVenue.setMarkedAsSynonym(true);
								probableVenue.setUpdatedDate(new Date());
								probableVenue.setCreator(username);
								toBeUpdatedProbableList.add(probableVenue);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}
			String info = "Venue Synonyms Added Successfully.";
			mav.addObject("info", info);
			try{
				DAORegistry.getProbableVenueDAO().updateAll(toBeUpdatedProbableList);
			}catch(Exception e){
				e.printStackTrace();
			}
			return new ModelAndView(new RedirectView(
					"EditorEditManageVenueSynonym?info=" + info, true));
		}
		
		Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();
		for (Venue venue : venues) {
			venueMap.put(venue.getId(), venue);
		}
		
		List<VenueSynonymInfo> venueSynonymInfoList = new ArrayList<VenueSynonymInfo>();
		List<VenueSynonymInfo> gridVenueSynonymsList = new ArrayList<VenueSynonymInfo>();
		VenueSynonymInfo venueSynonymInfo = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY hh:mm aa");
		for (Integer vId : probableVenuMap.keySet()) {
			List<ProbableVenue> list = probableVenuMap.get(vId);
			
			String creator = "", createdTime="";
			Date createdDate = null; 
			
			boolean isEligibleToShow = false;
			for (ProbableVenue probableVenue : list) {
				if(!probableVenue.getMarkedAsSynonym()){
					isEligibleToShow = true;
				}else{
					creator = probableVenue.getCreator();
					createdDate = probableVenue.getUpdatedDate();
				}
			}
			
			if(isEligibleToShow){
				if(null != createdDate){
					createdTime = dateFormat.format(createdDate);
				}
				venueSynonymInfo = new VenueSynonymInfo();
				venueSynonymInfo.setVenueId(vId);
				venueSynonymInfo.setVenue(venueMap.get(vId));
				venueSynonymInfo.setProbableVenues(list);
				venueSynonymInfo.setCreator(creator);
				venueSynonymInfo.setCreatedTime(createdTime);
				venueSynonymInfoList.add(venueSynonymInfo);
				if(venueId != null){
					if(venueId.equals(vId)){
						gridVenueSynonymsList.add(venueSynonymInfo);
					}
				}else{
					gridVenueSynonymsList.add(venueSynonymInfo);
				}
			}
		}
		
		mav.addObject("venueId", venueIdStr);
		mav.addObject("venueSynonymInfoList", gridVenueSynonymsList);
		mav.addObject("allVenues", venueSynonymInfoList);
		return mav;
	
	}
	
	public ModelAndView loadEditorEditManageVenueSynonym(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ModelAndView mav = new ModelAndView("page-editor-manage-venue-synonym");
		
		String venueIdStr =  request.getParameter("venueId");
		
		Integer venueId = null;
		if(null != venueIdStr && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr.trim());
		}else{
		}
		
		List<ProbableVenue> probableVenues = DAORegistry.getProbableVenueDAO().getAllAddedVenueSynonymsByMarkedAsSynonym(Boolean.TRUE);
		Map<Integer, ProbableVenue> probableVenueById = new HashMap<Integer, ProbableVenue>();
		for (ProbableVenue obj : probableVenues) {
			probableVenueById.put(obj.getId(), obj);
		}
		
		List<Integer> tobeRemovedIds = new ArrayList<Integer>();
		String action = request.getParameter("action");
		if(action!=null && action.equalsIgnoreCase("update")){
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			List<ProbableVenue> toBeUpdatedProbableList = new ArrayList<ProbableVenue>();
			for(Object object :request.getParameterMap().keySet()){
				String key = (String)object;
				
				if(!key.contains("checkbox")){
					continue;
				}
				
				Integer gridVenueId = null;
				
				String temp[] = key.split("-");
				gridVenueId = Integer.parseInt(temp[0]); 
				String[] synonymIds = request.getParameterValues(gridVenueId+"-" + "removeVenue");
				
				if(gridVenueId!=null){
					if(null != synonymIds && !synonymIds.equals("") && synonymIds.length > 0){
						for (String idStr : synonymIds) {
							try{
								Integer probableVenueId = Integer.parseInt(idStr);
								ProbableVenue probableVenue = probableVenueById.get(probableVenueId);
								probableVenue.setMarkedAsSynonym(false);
								probableVenue.setUpdatedDate(new Date());
								probableVenue.setCreator(username);
								toBeUpdatedProbableList.add(probableVenue);
								tobeRemovedIds.add(probableVenueId);
								
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}
			String info = "Venue Synonyms Removed Successfully.";
			mav.addObject("info", info);
			try{
				DAORegistry.getProbableVenueDAO().updateAll(toBeUpdatedProbableList);
			}catch(Exception e){
				e.printStackTrace();
			} 
		}
		probableVenues = DAORegistry.getProbableVenueDAO().getAllAddedVenueSynonymsByMarkedAsSynonym(Boolean.TRUE);
		
		Map<Integer, List<ProbableVenue>> probableVenuMap = new HashMap<Integer, List<ProbableVenue>>();
		for (ProbableVenue obj : probableVenues) {
			List<ProbableVenue> list = probableVenuMap.get(obj.getVenueId());
			if(list != null && !list.isEmpty()){
				probableVenuMap.get(obj.getVenueId()).add(obj);
			}else{
				list = new ArrayList<ProbableVenue>();
				list.add(obj);
				probableVenuMap.put(obj.getVenueId(), list);
			}
		}
		Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();
		for (Venue venue : venues) {
			venueMap.put(venue.getId(), venue);
		}
		
		List<VenueSynonymInfo> venueSynonymInfoList = new ArrayList<VenueSynonymInfo>();
		List<VenueSynonymInfo> gridVenueSynonymsList = new ArrayList<VenueSynonymInfo>();
		VenueSynonymInfo venueSynonymInfo = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY hh:mm aa");
		for (Integer vId : probableVenuMap.keySet()) {
			List<ProbableVenue> list = probableVenuMap.get(vId);
			venueSynonymInfo = new VenueSynonymInfo();
			venueSynonymInfo.setVenueId(vId);
			venueSynonymInfo.setVenue(venueMap.get(vId));
			venueSynonymInfo.setProbableVenues(list);
			venueSynonymInfo.setCreator(list.get(0).getCreator());
			venueSynonymInfo.setCreatedTime(dateFormat.format(list.get(0).getUpdatedDate()));
			venueSynonymInfoList.add(venueSynonymInfo);
			if(venueId != null){
				if(venueId.equals(vId)){
					gridVenueSynonymsList.add(venueSynonymInfo);
				}
			}else{
				gridVenueSynonymsList.add(venueSynonymInfo);
			}
		}
		mav.addObject("venueId", venueIdStr);
		mav.addObject("venueSynonymInfoList", gridVenueSynonymsList);
		mav.addObject("allVenues", venueSynonymInfoList);
		return mav;
	
	}
}