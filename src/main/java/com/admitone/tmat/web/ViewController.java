package com.admitone.tmat.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.acegisecurity.userdetails.UserDetails;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.maven.surefire.shade.org.codehaus.plexus.util.IOUtil;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.pojo.TMATSvgMapDetails;
import com.admitone.tmat.pojo.WebServiceProperties;
import com.admitone.tmat.pojo.ZoneRGBColor;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.DashboardManager;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.StatCache;
import com.admitone.tmat.utils.StatHelper;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.utils.eimarketplace.EIMPUtil;
import com.admitone.tmat.utils.jira.JiraAttachment;
import com.admitone.tmat.utils.jira.JiraClient;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.redmine.RedmineAttachment;
import com.admitone.tmat.utils.redmine.WebRedmineClient;
import com.admitone.tmat.zones.ZonesManager;

/**
 * Class that handle the views for pages that can be accessed with the USER role.
 */
public class ViewController extends  MultiActionController implements InitializingBean {
	private final Logger logger = LoggerFactory.getLogger(ViewController.class);
	private StatCache statCache;
//	private LatestEventCache latestEventCache;
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();
	private VelocityEngine velocityEngine;
	private JiraClient jiraClient;	
	private WebRedmineClient redmineClient;
	private DashboardManager dashboardManager;
	private ZonesManager zonesManager;
	private WebServiceProperties webServiceProperties;
	
	public WebServiceProperties getWebServiceProperties() {
		return webServiceProperties;
	}
	
	public void setWebServiceProperties(WebServiceProperties webServiceProperties) {
		this.webServiceProperties = webServiceProperties;
	}

	/**
	 * output the event image:
	 * - the venue image if found in e:\TMATIMAGESFINAL
	 * - the eimp image if there is one EIMP crawl
	 * - default (not found) image otherwise 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public ModelAndView loadEventImage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		String catScheme = request.getParameter("catScheme");
		
		String filename = "";
		if(eventId != 0){
			Event event = DAORegistry.getEventDAO().get(eventId);
			Integer venueId = event.getVenueId();
			filename =  "C:/TMATIMAGESFINAL/" + venueId + "_" + catScheme + ".gif";
		}else{
			String fName = request.getParameter("fileName");
			filename = fName;
		}
				
		File file = new File(filename);
		
		if (!file.exists()) {
			// try to get the eimp map
			byte[] content = EIMPUtil.getEimpMap(eventId);
			if (content != null) {
				IOUtil.copy(content, response.getOutputStream());
				return null;
			}
			
			filename = "C:/TMATIMAGESFINAL/000_THUMB_AO.gif";
			file = new File(filename);
		}
		
		response.setContentType("image/gif");
		IOUtil.copy(new FileInputStream(file), response.getOutputStream());
		return null;
	}*/
	
	/**
	 * output the event image:
	 * - the venue image if found in e:\TMATIMAGESFINAL
	 * - the eimp image if there is one EIMP crawl
	 * - default (not found) image otherwise 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEventImage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		String catScheme = request.getParameter("catScheme");
		
		ModelAndView mav = new ModelAndView("page-svg-map-popup");
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		if(event == null){
			return null;
		}
		String categories = "";
		// Mehul: in tmatprodscale we have stored categories as cat1,cat2.. i.e without '_' and that is causing problem so we are retriving all cat colors here.
		//if(!event.getVenueCategory().getCategoryGroup().equalsIgnoreCase("PRESALE")){
			for(Category cat : event.getCategory()){
				categories += cat.getSymbol() + ",";
			}
		//}
		if(!categories.isEmpty()){
			categories = categories.substring(0, categories.length() - 1);
		}
		TMATSvgMapDetails tmatSvgMapDetails = null;
		try{
		String url = webServiceProperties.getWebServiceUrl() + "GetSvgForTMAT.json" + "?configId=" + webServiceProperties.getConfigId();
		url += "&venueId=" + event.getVenue().getId() + "&categoryGroupName=" + event.getVenueCategory().getCategoryGroup() + "&categories=" + categories;
		HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);

        /*method.add
        method.addParameter("venueId", "" + event.getVenue().getId());
        method.addParameter("categoryGroupName", event.getVenueCategory().getCategoryGroup());
        method.addParameter("categories", categories);*/
        client.executeMethod(method);
        String jsonData = method.getResponseBodyAsString();
        JSONObject jsonObject = new JSONObject(jsonData);
		jsonData = jsonObject.getString("TMATSvgMapDetails");
		ObjectMapper mapper = new ObjectMapper();
		tmatSvgMapDetails = mapper.readValue(jsonData, TMATSvgMapDetails.class);
		}catch(Exception e){
			e.printStackTrace();
			tmatSvgMapDetails = null;
		}
		if(tmatSvgMapDetails != null && tmatSvgMapDetails.getStatus().equals(1) && tmatSvgMapDetails.isMapWithSvg()){
			Map<String, String> colorsMap = new HashMap<String, String>();
			for(ZoneRGBColor zrc : tmatSvgMapDetails.getZoneRGBColors()){
				colorsMap.put(zrc.getZone().toLowerCase(), zrc.getColor());
			}
			mav.addObject("tmatSvgMapDetails", tmatSvgMapDetails);
			mav.addObject("colorsMap", colorsMap);
			//System.out.println(colorsMap.get("h"));
			return mav;
		}else{		
		String filename = "";
		if(eventId != 0){
			Integer venueId = event.getVenueId();
			filename =  "C:/TMATIMAGESFINAL/SvgMaps/" + venueId + "_" + catScheme + ".gif";
		}else{
			String fName = request.getParameter("fileName");
			filename = fName;
		}
				
		File file = new File(filename);
		
		if (!file.exists()) {
			// try to get the eimp map
			byte[] content = EIMPUtil.getEimpMap(eventId);
			if (content != null) {
				IOUtil.copy(content, response.getOutputStream());
				return null;
			}
			
			filename = "C:/TMATIMAGESFINAL/000_THUMB_AO.gif";
			file = new File(filename);
		}
		
		response.setContentType("image/gif");
		IOUtil.copy(new FileInputStream(file), response.getOutputStream());
		return null;
		}
	}
	
	/**
	 * Output the tour image which is stored in C:/TMATIMAGESFINAL/
	 */
	public ModelAndView loadVenueImage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String venueId = request.getParameter("venueId");
		String catScheme = request.getParameter("catScheme");
		String venueCatId = request.getParameter("venueCatId");
		
		ModelAndView mav = new ModelAndView("page-svg-map-popup");
		
		VenueCategory venueCat = null;
		if(venueCatId != null && !venueCatId.isEmpty()) {
			venueCat = DAORegistry.getVenueCategoryDAO().get(Integer.parseInt(venueCatId));			
		} else {
			venueCat = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(Integer.parseInt(venueId), catScheme);
		}
		if(venueCat == null){
			return null;
		}
		String categories = "";
		// Mehul: in tmatprodscale we have stored categories as cat1,cat2.. i.e without '_' and that is causing problem so we are retriving all cat colors here.
		//if(!venueCat.getCategoryGroup().equalsIgnoreCase("PRESALE")){
			Collection<Category> catsList = Categorizer.getCategoryByVenueCategoryId(venueCat.getId());;
			for(Category cat : catsList){
				categories += cat.getSymbol() + ",";
			}
		//}
		if(!categories.isEmpty()){
			categories = categories.substring(0, categories.length() - 1);
		}
		TMATSvgMapDetails tmatSvgMapDetails = null;
		try{
		String url = webServiceProperties.getWebServiceUrl() + "GetSvgForTMAT.json" + "?configId=" + webServiceProperties.getConfigId();
		url += "&venueId=" + venueCat.getVenue().getId() + "&categoryGroupName=" + venueCat.getCategoryGroup() + "&categories=" + categories;
		HttpClient client = new HttpClient();
        GetMethod method = new GetMethod(url);

        /*method.add
        method.addParameter("venueId", "" + event.getVenue().getId());
        method.addParameter("categoryGroupName", event.getVenueCategory().getCategoryGroup());
        method.addParameter("categories", categories);*/
        client.executeMethod(method);
        String jsonData = method.getResponseBodyAsString();
        JSONObject jsonObject = new JSONObject(jsonData);
		jsonData = jsonObject.getString("TMATSvgMapDetails");
		ObjectMapper mapper = new ObjectMapper();
		tmatSvgMapDetails = mapper.readValue(jsonData, TMATSvgMapDetails.class);
		}catch(Exception e){
			e.printStackTrace();
			tmatSvgMapDetails = null;
		}
		if(tmatSvgMapDetails != null && tmatSvgMapDetails.getStatus().equals(1) && tmatSvgMapDetails.isMapWithSvg()){
			Map<String, String> colorsMap = new HashMap<String, String>();
			for(ZoneRGBColor zrc : tmatSvgMapDetails.getZoneRGBColors()){
				colorsMap.put(zrc.getZone().toLowerCase(), zrc.getColor());
			}
			mav.addObject("tmatSvgMapDetails", tmatSvgMapDetails);
			mav.addObject("colorsMap", colorsMap);
			//System.out.println(colorsMap.get("h"));
			return mav;
		}else{		
		String filename = "";
		if(venueCat.getVenue().getId() != 0){
			//Integer venueId = event.getVenueId();
			filename =  "C:/TMATIMAGESFINAL/SvgMaps/" + venueCat.getVenue().getId() + "_" + venueCat.getCategoryGroup() + ".gif";
			//filename = "C:/TMATIMAGESFINAL/000_THUMB_AO.gif";
		}else{
			String fName = request.getParameter("fileName");
			filename = fName;
		}
				
		File file = new File(filename);
		
		if (!file.exists()) {
			// try to get the eimp map
			/*byte[] content = EIMPUtil.getEimpMap(eventId);
			if (content != null) {
				IOUtil.copy(content, response.getOutputStream());
				return null;
			}*/
			
			filename = "C:/TMATIMAGESFINAL/000_THUMB_AO.gif";
			file = new File(filename);
		}
		
		response.setContentType("image/gif");
		IOUtil.copy(new FileInputStream(file), response.getOutputStream());
		return null;
		}
	}
	public ModelAndView loadTMATVenueImage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String venueId = request.getParameter("venueId");
		String catScheme = request.getParameter("catScheme");
		
		String filename =  "C:/TMATIMAGESFINAL/SvgMaps/" + venueId + "_" + catScheme + ".gif";
				
		File file = new File(filename);
		
		if (!file.exists()) {
			filename = "C:/TMATIMAGESFINAL/000_THUMB_AO.gif";
			file = new File(filename);
		}
		
		response.setContentType("image/gif");
		IOUtil.copy(new FileInputStream(file), response.getOutputStream());
		return null;
	}


	/**
	 * Home Page.
	 */
	public String loadIndexPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return "redirect:EditorTCAP";
	}
	/*public ModelAndView loadIndexPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-home");
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().get(username);
		if (user.hasPrivilege("PRIV_CRAWLER")) {
			
			Property crawlerThreadCountProperty = DAORegistry.getPropertyDAO().get("crawler.thread.count");
			int expectedCrawlThreadCount = Integer.parseInt(crawlerThreadCountProperty.getValue());
			mav.addObject("expectedCrawlThreadCount", expectedCrawlThreadCount * ticketListingCrawler.getTicketListingCrawlDispatcher().getNumNodes());
			mav.addObject("activeCrawlThreadCount", ticketListingCrawler.getActiveThreadCount());

			mav.addObject("ticketListingCrawler", ticketListingCrawler);
			mav.addObject("createdCrawls", ticketListingCrawler.getTicketListingCrawlByCreator(username));
			mav.addObject("lastUpdatedCrawls", ticketListingCrawler.getTicketListingCrawlByLastUpdater(username));
			
			mav.addObject("failedCrawls", ticketListingCrawler.getFailedTicketListingCrawlsByEventId());
			mav.addObject("eventsToExpire", DAORegistry.getEventDAO().getAllEventsByDates(null, new Date(new Date().getTime() - 24L * 60L * 60L * 1000L), EventStatus.ACTIVE));
			
			List<TicketListingCrawlStat> stats = new ArrayList<TicketListingCrawlStat>(ticketListingCrawler.getTicketListingCrawlDispatcher().getStatMap().values());
			Date now = new Date();
			String clockError = "";
			for(TicketListingCrawlStat stat: stats) {
				int diffMinutes = (int)((now.getTime() - stat.getLastUpdate().getTime()) / (60L * 1000L)); 
				if (diffMinutes > 5) {
					clockError += stat.getHostname() + " runs " + diffMinutes + " mn backward.<br />";
				} else if (diffMinutes < -5) {
					clockError += stat.getHostname() + " runs " + diffMinutes + " mn ahead.<br />";					
				}
			}
			mav.addObject("clockError", clockError);
		}
		
		if (user.hasRole("ROLE_EBAY")) {
			mav.addObject("numEbayInventoryGroupToTakeAction", DAORegistry.getEbayInventoryGroupDAO().getInventoryGroupToTakeActionCount());
		}
				
		// next 10 upcoming events that have A1 inventory
		
//		/* CS1
		List<Event> events; 
		
		if(latestEventCache.getLatestEvents()==null || ((new Date().getTime()- latestEventCache.getLastUpdate().getTime()) > 1000 * 60L * 60L * 4L)){
			latestEventCache.refreshLatestEventsEntry();
		}
		events = latestEventCache.getLatestEvents();
		
//		 CS1 * /
		List<Event> top10TicketCountEvents = dashboardManager.getTopTicketCountEvents();
		if (top10TicketCountEvents.size() > 10) {
			top10TicketCountEvents = top10TicketCountEvents.subList(0, 10);
		}
		mav.addObject("topTicketCountEvents", top10TicketCountEvents);
		mav.addObject("username", username);
		mav.addObject("events", events);
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		return mav;
	}*/

	public ModelAndView loadUpcomingEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		ModelAndView mav = new ModelAndView("page-upcoming-events");
		Date startDate = new Date();
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByDates(startDate, null, EventStatus.ACTIVE);
		mav.addObject("events", events);
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		return mav;
	}

	public ModelAndView loadTopTicketCountEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		ModelAndView mav = new ModelAndView("page-top-ticket-count-events");
		mav.addObject("topTicketCountEvents", dashboardManager.getTopTicketCountEvents());
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		return mav;
	}

	public ModelAndView loadUsefulLinksPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-useful-links");
		return mav;
	}

	/**
	 * My account page.
	 */
	public ModelAndView loadMyAccountPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		ModelAndView mav = new ModelAndView("page-my-account");

		if (request.getParameter("updateInfo") != null) {
			user.setEmail(request.getParameter("email"));
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setPhone(request.getParameter("phone"));
			DAORegistry.getUserDAO().update(user);
			mav.addObject("info", "User information updated.");
		} else if (request.getParameter("updatePassword") != null) {
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			if (!password.equals(password2)) {
				mav.addObject("error", "Passwords don't match.");
			} else {
				user.setAndEncryptPassword(password);			
				DAORegistry.getUserDAO().update(user);
				mav.addObject("info", "Password updated.");
			}
		} else if (request.getParameter("updateEmailSettings") != null) {
			Boolean emailCrawlErrorCreatorEnabled = "on".equals(request.getParameter("emailCrawlErrorCreatorEnabled"));
			Boolean emailCrawlErrorLastUpdaterEnabled = "on".equals(request.getParameter("emailCrawlErrorLastUpdaterEnabled"));
			user.setEmailCrawlErrorCreatorEnabled(emailCrawlErrorCreatorEnabled);
			user.setEmailCrawlErrorLastUpdaterEnabled(emailCrawlErrorLastUpdaterEnabled);
			DAORegistry.getUserDAO().update(user);
			mav.addObject("info", "Email settings updated.");
		}
		mav.addObject("user", user);
		return mav;
	}	
	


	/**
	 * Change password page.
	 */
	public ModelAndView loadChangePasswordPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		if (user == null) {
			return new ModelAndView(new RedirectView("Login"));			
		}

		String code = request.getParameter("code");
		if (username == null || code == null || user.getCode() == null || !user.getCode().equals(code)) {
			return new ModelAndView(new RedirectView("Login"));
		}
		
		ModelAndView mav = new ModelAndView("page-change-password");
		mav.addObject("username", request.getParameter("username"));
		mav.addObject("code", request.getParameter("code"));

		if (request.getParameter("submit") != null) {
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");

			if (password.length() < 6) {
				mav.addObject("error", "Password must be at least 6 characters long.");				
				return mav;
			}
			
			if (!password.equals(password2)) {
				mav.addObject("error", "Passwords do not match.");
				return mav;
			}
			
			mav.addObject("info", "Password updated!");
			user.setCode(null);
			user.setAndEncryptPassword(password);
			DAORegistry.getUserDAO().update(user);
			mav.addObject("success", "true");
		}
		
		return mav;
	}
	
	/**
	 * Login Page.
	 */
	public ModelAndView loadLoginPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-login");
		mav.addObject("showError", request.getParameter("error"));
		
		return mav;
	}	


	/**
	 * Reset password Page.
	 */
	public ModelAndView loadResetPasswordPage(HttpServletRequest request,
			   HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-login");
		
		String usernameOrEmail = request.getParameter("username");
		
		if (usernameOrEmail == null || usernameOrEmail.trim().isEmpty()) {
			mav.addObject("forgotPasswordError", "Please provide a Username or Email address");
			return mav;
		}
		
		User user = DAORegistry.getUserDAO().getUserByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
		if (user == null) {
			mav.addObject("forgotPasswordError", "Username or Email address does not exist");
			return mav;
		}
		
		user.setCode(RandomStringUtils.randomAlphanumeric(10));
		DAORegistry.getUserDAO().update(user);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("user", user);
		SpringUtil.getMailManager().sendMail("TMAT Forgot Password", user.getEmail(),
											 "TMAT: Instructions to reset your password", "mail-forgot-password.html", map, "text/html");
		mav.addObject("forgotPasswordInfo", "Instructions to reset your password have been sent to your email address");

		mav.addObject("success", "true");
		return mav;
	}
	
	public ModelAndView loadBookmarksPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-bookmarks");
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();

		mav.addObject("artistBookmarks", DAORegistry.getBookmarkDAO().getAllArtistBookmarks(username));
//		mav.addObject("tourBookmarks", DAORegistry.getBookmarkDAO().getAllTourBookmarks(username));
		mav.addObject("eventBookmarks", DAORegistry.getBookmarkDAO().getAllEventBookmarks(username));
		mav.addObject("ticketBookmarks", DAORegistry.getBookmarkDAO().getAllTicketBookmarks(username));
		return mav;
	}	
	
	public ModelAndView loadTourStatBySitePage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String removeDuplicateString = request.getParameter("removeDuplicates");
		
		String action = request.getParameter("action");
		if (action != null && action.equals("refreshCache")) {
			statCache.refreshTourStatCacheEntry();
			return new ModelAndView(new RedirectView("TourStatBySite?removeDuplicates=" + removeDuplicateString));
		}

		ModelAndView mav = new ModelAndView("page-tour-stats");		


		if (statCache.isEmpty()) {
			mav.addObject("emptyStatCache", true);			
		} else {
			mav.addObject("artists", DAORegistry.getArtistDAO().getAll());

			mav.addObject("emptyStatCache", false);			
		
			if (removeDuplicateString == null || removeDuplicateString.equals("1")) {
				mav.addObject("tourStats", statCache.getNonDuplicateTourStatsBySiteId());
				mav.addObject("eventStats", statCache.getNonDuplicateEventStatsBySiteId());
				mav.addObject("totalStats", statCache.getNonDuplicateTotalStatsBySiteId());
				mav.addObject("ticketCount", statCache.getNonDuplicateTicketCount());
				mav.addObject("ticketEntryCount", statCache.getNonDuplicateTicketEntryCount());
				mav.addObject("removeDuplicates", true);
			} else {
				mav.addObject("tourStats", statCache.getTourStatsBySiteId());
				mav.addObject("eventStats", statCache.getEventStatsBySiteId());
				mav.addObject("totalStats", statCache.getTotalStatsBySiteId());
				mav.addObject("ticketCount", statCache.getTicketCount());
				mav.addObject("ticketEntryCount", statCache.getTicketEntryCount());
				mav.addObject("removeDuplicates", false);
			}
		}

		if (statCache.getStartUpdate() != null) {
			mav.addObject("lastUpdateDelay", (new Date().getTime() - statCache.getStartUpdate().getTime()));
		}
		mav.addObject("startUpdate", statCache.getStartUpdate());
		mav.addObject("columnKeys", Constants.getInstance().getSiteIds());
		mav.addObject("columnParamName", "site");
		mav.addObject("criteria", "site");

		mav.addObject("refreshingTourStatCache", statCache.isRefreshingTourStatCache());
		mav.addObject("progressPercent", statCache.getProgressPercent());

		return mav;		
	}
			
	public ModelAndView loadTourStatByQuantityPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String removeDuplicateString = request.getParameter("removeDuplicates");

		String action = request.getParameter("action");
		if (action != null && action.equals("refreshCache")) {
			statCache.refreshTourStatCacheEntry();
			return new ModelAndView(new RedirectView("TourStatBySite?removeDuplicates=" + removeDuplicateString));
		}

		ModelAndView mav = new ModelAndView("page-tour-stats");		
		
		if (statCache.isEmpty()) {
			mav.addObject("emptyStatCache", true);			
		} else {
			mav.addObject("artists", DAORegistry.getArtistDAO().getAll());

			mav.addObject("emptyStatCache", false);			
		
			if (removeDuplicateString == null || removeDuplicateString.equals("1")) {
				mav.addObject("tourStats", statCache.getNonDuplicateTourStatsByQuantity());
				mav.addObject("eventStats", statCache.getNonDuplicateEventStatsByQuantity());
				mav.addObject("totalStats", statCache.getNonDuplicateTotalStatsByQuantity());
				mav.addObject("ticketCount", statCache.getNonDuplicateTicketCount());
				mav.addObject("ticketEntryCount", statCache.getNonDuplicateTicketEntryCount());
				mav.addObject("removeDuplicates", true);
			} else {
				mav.addObject("tourStats", statCache.getTourStatsByQuantity());
				mav.addObject("eventStats", statCache.getEventStatsByQuantity());
				mav.addObject("totalStats", statCache.getTotalStatsByQuantity());
				mav.addObject("ticketCount", statCache.getTicketCount());
				mav.addObject("ticketEntryCount", statCache.getTicketEntryCount());
				mav.addObject("removeDuplicates", false);
			}
		}

		if (statCache.getStartUpdate() != null) {
			mav.addObject("lastUpdateDelay", (new Date().getTime() - statCache.getStartUpdate().getTime()));
		}
		mav.addObject("columnKeys", new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9+"});
		mav.addObject("columnParamName", "quantity");
		mav.addObject("criteria", "quantity");

		mav.addObject("refreshingTourStatCache", statCache.isRefreshingTourStatCache());
		mav.addObject("progressPercent", statCache.getProgressPercent());

		return mav;		
	}

	public ModelAndView loadEventStatPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ModelAndView mav = new ModelAndView("page-event-stats");		
		
		int eventId = Integer.parseInt(request.getParameter("eventId"));
		String catScheme = request.getParameter("catScheme");
		Event event = DAORegistry.getEventDAO().get(eventId);
//		if(catScheme == null){
//			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);
//		}
//		if(catScheme == ""){
//			catScheme = Categorizer.getCategoryGroupsByEvent(eventId).get(0);
//		}
		if(catScheme==null || catScheme.isEmpty()){
			catScheme = DAORegistry.getVenueCategoryDAO().get(event.getVenueCategory().getId()).getCategoryGroup();
		}
		
		
		String removeDuplicateString = request.getParameter("removeDuplicates");
		boolean removeDuplicates = false;
		if (removeDuplicateString == null || removeDuplicateString.equals("1")) {
			removeDuplicates = true;
		}

		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		if (removeDuplicates) {
			tickets = TicketUtil.removeDuplicateTickets(tickets); 
		}		
			
		int ticketCount = 0;
		for (Ticket ticket: tickets) {
			ticketCount += ticket.getRemainingQuantity();
		}
		
		Map<String, Map<String, Integer>> siteStatByCategory = StatHelper.getTicketCountByCategoryAndSite(tickets, catScheme);
		Map<String, Integer> totalStatBySite = new HashMap<String, Integer>();
		if (siteStatByCategory != null) {
			for (String categorySymbol: siteStatByCategory.keySet()) {
				for (String siteId: siteStatByCategory.get(categorySymbol).keySet()) {
					int totalStatCount = ((totalStatBySite.get(siteId) == null)?0:totalStatBySite.get(siteId)) + siteStatByCategory.get(categorySymbol).get(siteId);
					totalStatBySite.put(siteId, totalStatCount);
				}
			}
		}
		
		Map<String, Map<String, Integer>> quantityStatBySiteId = StatHelper.getTicketEntryCountBySiteAndQuantity(tickets);
		Map<String, Integer> totalStatByQuantity = new HashMap<String, Integer>();
		if (quantityStatBySiteId != null) {
			for (String siteId: quantityStatBySiteId.keySet()) {
				for (String quantity: quantityStatBySiteId.get(siteId).keySet()) {
					int totalStatCount = ((totalStatByQuantity.get(quantity) == null)?0:totalStatByQuantity.get(quantity)) + quantityStatBySiteId.get(siteId).get(quantity);
					totalStatByQuantity.put(quantity, totalStatCount);
				}
			}
		}
		
		Map<String, Map<String, Integer>> quantityStatByCategory = StatHelper.getTicketEntryCountByCategoryAndQuantity(tickets, catScheme);
		
		mav.addObject("eventId", request.getParameter("eventId"));
		mav.addObject("catGroupName", catScheme);
		mav.addObject("catGroups", Categorizer.getCategoryGroupsByVenueId(event.getVenueId()));
		mav.addObject("ticketCount", ticketCount);
		mav.addObject("removeDuplicates", removeDuplicates);
		mav.addObject("ticketEntryCount", tickets.size());
		mav.addObject("event", event);
		mav.addObject("siteStatByCategory", siteStatByCategory);
		mav.addObject("quantityStatBySiteId", quantityStatBySiteId);
		mav.addObject("quantityStatByCategory", quantityStatByCategory);
		mav.addObject("totalStatBySite", totalStatBySite);
		mav.addObject("totalStatByQuantity", totalStatByQuantity);
						
		return mav;				
	}
	
	/*public ModelAndView loadEditArtistPriceAdjustmentPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");

		int artistId = Integer.parseInt(request.getParameter("artistId"));
		String siteId = request.getParameter("siteId");
		Artist artist = DAORegistry.getArtistDAO().get(artistId);

		if ("update".equals(action)) {
			// update artist price adjustment
			double artistPriceAdjustmentValue = Double.parseDouble(request.getParameter("artistPriceAdjustment"));
			ArtistPriceAdjustment artistPriceAdjustment = DAORegistry.getArtistPriceAdjustmentDAO().get(new ArtistPriceAdjustmentPk(artistId, siteId));
			if (artistPriceAdjustment == null) {
				artistPriceAdjustment = new ArtistPriceAdjustment();
				artistPriceAdjustment.setArtistId(artistId);
				artistPriceAdjustment.setSiteId(siteId);
				artistPriceAdjustment.setPercentAdjustment(artistPriceAdjustmentValue);
				DAORegistry.getArtistPriceAdjustmentDAO().save(artistPriceAdjustment);
			} else {
				artistPriceAdjustment.setPercentAdjustment(artistPriceAdjustmentValue);
				DAORegistry.getArtistPriceAdjustmentDAO().update(artistPriceAdjustment);
			}
			
			// update tour price adjustment
			for (Tour tour: artist.getTours()) {
				double tourPriceAdjustmentValue = Double.parseDouble(request.getParameter("tourPriceAdjustment_" + tour.getId()));
				TourPriceAdjustment tourPriceAdjustment = DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(tour.getId(), siteId));
				if (tourPriceAdjustment == null) {
					tourPriceAdjustment = new TourPriceAdjustment();
					tourPriceAdjustment.setTourId(tour.getId());
					tourPriceAdjustment.setSiteId(siteId);
					tourPriceAdjustment.setPercentAdjustment(tourPriceAdjustmentValue);
					DAORegistry.getTourPriceAdjustmentDAO().save(tourPriceAdjustment);
				} else {
					tourPriceAdjustment.setPercentAdjustment(tourPriceAdjustmentValue);
					DAORegistry.getTourPriceAdjustmentDAO().update(tourPriceAdjustment);
				}
				
				for (Event event: tour.getEvents()) {
					double eventPriceAdjustmentValue = Double.parseDouble(request.getParameter("eventPriceAdjustment_" + event.getId()));

					EventPriceAdjustment eventPriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(event.getId(), siteId);
					if (eventPriceAdjustment == null) {
						eventPriceAdjustment = new EventPriceAdjustment();
						eventPriceAdjustment.setEventId(event.getId());
						eventPriceAdjustment.setSiteId(siteId);
						eventPriceAdjustment.setPercentAdjustment(eventPriceAdjustmentValue);
						DAORegistry.getEventPriceAdjustmentDAO().save(eventPriceAdjustment);
					} else {
						eventPriceAdjustment.setPercentAdjustment(eventPriceAdjustmentValue);
						DAORegistry.getEventPriceAdjustmentDAO().update(eventPriceAdjustment);
					}
					
				}				
			}			
			return new ModelAndView(new RedirectView("BrowseTours?artistId=" + artistId + "&info=The price adjustments had been updated."));			
		}
		
		ModelAndView mav = new ModelAndView("page-edit-artist-price-adjustment");		

		mav.addObject("site", DAORegistry.getSiteDAO().get(siteId));
		mav.addObject("artist", artist);
		mav.addObject("artistPriceAdjustment", DAORegistry.getArtistPriceAdjustmentDAO().get(new ArtistPriceAdjustmentPk(artistId, siteId)));
		
		Map<Integer, TourPriceAdjustment> tourPriceAdjustments = new HashMap<Integer, TourPriceAdjustment>();
		Map<Integer, EventPriceAdjustment> eventPriceAdjustments = new HashMap<Integer, EventPriceAdjustment>();
		for (Tour tour: artist.getTours()) {
			tourPriceAdjustments.put(tour.getId(), DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(tour.getId(), siteId)));
			for (Event event: tour.getEvents()) {
				eventPriceAdjustments.put(event.getId(), DAORegistry.getEventPriceAdjustmentDAO().get(event.getId(), siteId));
			}			
		}
		
		mav.addObject("tourPriceAdjustments", tourPriceAdjustments);
		mav.addObject("eventPriceAdjustments", eventPriceAdjustments);
		return mav;				
	}*/
	
	/*public ModelAndView loadEditTourPriceAdjustmentPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");

		int tourId = Integer.parseInt(request.getParameter("tourId"));
		String siteId = request.getParameter("siteId");
		Tour tour = DAORegistry.getTourDAO().get(tourId);

		if ("update".equals(action)) {
			// TODO: check argument
			double tourPriceAdjustmentValue = Double.parseDouble(request.getParameter("tourPriceAdjustment"));
			TourPriceAdjustment tourPriceAdjustment = DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(tourId, siteId));
			if (tourPriceAdjustment == null) {
				tourPriceAdjustment = new TourPriceAdjustment();
				tourPriceAdjustment.setTourId(tourId);
				tourPriceAdjustment.setSiteId(siteId);
				tourPriceAdjustment.setPercentAdjustment(tourPriceAdjustmentValue);
				DAORegistry.getTourPriceAdjustmentDAO().save(tourPriceAdjustment);
			} else {
				tourPriceAdjustment.setPercentAdjustment(tourPriceAdjustmentValue);
				DAORegistry.getTourPriceAdjustmentDAO().update(tourPriceAdjustment);
			}
			
			for (Event event: tour.getEvents()) {
				double eventPriceAdjustmentValue = Double.parseDouble(request.getParameter("eventPriceAdjustment_" + event.getId()));

				EventPriceAdjustment eventPriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(event.getId(), siteId);
				if (eventPriceAdjustment == null) {
					eventPriceAdjustment = new EventPriceAdjustment();
					eventPriceAdjustment.setEventId(event.getId());
					eventPriceAdjustment.setSiteId(siteId);
					eventPriceAdjustment.setPercentAdjustment(eventPriceAdjustmentValue);
					DAORegistry.getEventPriceAdjustmentDAO().save(eventPriceAdjustment);
				} else {
					eventPriceAdjustment.setPercentAdjustment(eventPriceAdjustmentValue);
					DAORegistry.getEventPriceAdjustmentDAO().update(eventPriceAdjustment);
				}

			}
			return new ModelAndView(new RedirectView("BrowseEvents?tourId=" + tourId + "&info=The price adjustments had been updated."));			
		}
		
		ModelAndView mav = new ModelAndView("page-edit-tour-price-adjustment");
		
		Map<Integer, EventPriceAdjustment> eventPriceAdjustments = new HashMap<Integer, EventPriceAdjustment>();
		for (Event event: tour.getEvents()) {
			eventPriceAdjustments.put(event.getId(), DAORegistry.getEventPriceAdjustmentDAO().get(event.getId(), siteId));
		}
		
		mav.addObject("tour", tour);
		mav.addObject("site", DAORegistry.getSiteDAO().get(siteId));
		mav.addObject("tourPriceAdjustment", DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(tourId, siteId)));
		mav.addObject("eventPriceAdjustments", eventPriceAdjustments);
		return mav;				
	}*/
	
	
	public ModelAndView loadAutoZoneCompleteSearch(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String q = request.getParameter("q");
		
		String searchType = request.getParameter("t");
		
		if (TextUtil.removeExtraWhitespaces(q).isEmpty()) {
			return null;
		}
		
		if(searchType == null ||"zone".equals(searchType))
		{
			List<String>  tourList = zonesManager.findEventNameList();
			for(String tour : tourList)
			{
				response.getOutputStream().println("TOUR|"+ tour + "|" + tour+  "|" + tour );
			}
			
		}
		
		return null;
		
	}
	public ModelAndView loadAutoCompleteSearch(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String q = request.getParameter("q");
		
		String searchType = request.getParameter("t");
		
		if (TextUtil.removeExtraWhitespaces(q).isEmpty()) {
			return null;
		}

		if (searchType == null || searchType.equals("artist") || !searchType.equals("tour&Events")) {
			Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(q, EventStatus.ACTIVE);		
			for (Artist artist: artists) {
				response.getOutputStream().println("ARTIST|" + artist.getName() + "|" + artist.getId());
			}
		}

		/*if (searchType == null || searchType.equals("tour") || searchType.equals("tour&Events")) {
			Collection<Tour> tours = DAORegistry.getTourDAO().getAllToursByNameExcerpt(q, EventStatus.ACTIVE);
			for (Tour tour: tours) {
				response.getOutputStream().println("TOUR|" + tour.getName() + "|" + tour.getId() + "|" + ((tour.getStartDate() == null)?null:tour.getStartDate().getTime()) + "|" + ((tour.getEndDate() == null)?null:tour.getEndDate().getTime()));
			}
		}*/

		if (searchType == null || searchType.equals("venue") || !searchType.equals("tour&Events")) {
			Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(q);
			for (Venue venue: venues) {
				response.getOutputStream().println("VENUE|" + venue.getBuilding()+","+venue.getCity()+","+venue.getState()+","+venue.getCountry()+ "|" + venue.getId());
			}
		}
		
		
		if (searchType == null || searchType.equals("event")|| searchType.equals("tour&Events")) {
			
			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByNameExcerpt(q, EventStatus.ACTIVE);		
			for (Event event: events) {
				//System.out.println("EVENT|" + event.getName() + "|" + event.getId() + "|" + event.getLocalDateTime().getTime());
				response.getOutputStream().println("EVENT|" + event.getName() + "|" + event.getId() + "|" + ((event.getLocalDateTime() == null)?"TBD":event.getLocalDateTime().getTime()) +"|"+ event.getVenue().getBuilding()+","+event.getVenue().getCity()+","+event.getVenue().getState()+","+event.getVenue().getCountry());
			}
		}

		return null;
	}

	/*	
	public void loadDownloadTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DateFormat format = new SimpleDateFormat("MM-dd-yyyy_HH:mm:ss");
		
		response.setContentType("application/force-download");
	    response.setHeader("Content-Disposition", "attachment; filename=\"export_" + format.format(new Date()) + ".csv\"");
	    
		Writer writer = response.getWriter();
		String eventIdStr = request.getParameter("eventId");
		if (eventIdStr != null) {
			Integer eventId = Integer.valueOf(eventIdStr);
			writer.write("Rem. Qty, Cat, Section, Row, Wholesale Price, Online Price, Title/Notes, Seller");
			TicketExportUtil.exportTickets(writer, eventId);
			writer.close();
		} else {
			Collection<Integer> eventIds = new ArrayList<Integer>();
			for (Object param: request.getParameterMap().keySet()) {
				String eventIdParam = param.toString().replace("event_", "");
				Integer eventId = Integer.valueOf(eventIdParam);
				eventIds.add(eventId);
			}
			writer.write("Rem. Qty, Cat, Section, Row, Wholesale Price, Online Price, Title/Notes, Seller");
			TicketExportUtil.exportTickets(writer, eventIds);
			writer.close();
		}
	}
	*/
	public ModelAndView loadSendFeedback(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String feedbackProject = request.getParameter("feedbackProject");
		String feedbackType = request.getParameter("feedbackType");
		Integer feedbackPriority = Integer.valueOf(request.getParameter("feedbackPriority"));
		String feedbackScope = request.getParameter("feedbackScope");
		String feedbackSummary = request.getParameter("feedbackSummary");
		String feedbackMessage = request.getParameter("feedbackMessage");
		
		System.out.println("FEEDBACK MESSAGE=" + feedbackMessage);
		String currentUrl = request.getParameter("currentUrl");
		String referrerUrl = request.getParameter("referrerUrl");
		String userAgent = request.getHeader("user-agent");
		String clientIpAddress = request.getRemoteAddr(); 
		
        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
        MultipartFile multipartFile = multipartHttpServletRequest.getFile("feedbackAttachment");
        byte[] attachmentContent = null;
        
        MailAttachment[] mailAttachments = null;
        JiraAttachment[] jiraAttachments = null;
        RedmineAttachment[] redmineAttachments = null;

        if (multipartFile != null && multipartFile.getOriginalFilename() != null && multipartFile.getOriginalFilename().length() > 0) {
            System.out.println("FILENAME="  + multipartFile.getOriginalFilename());
            
            //StringBuffer attachmentContent = new StringBuffer();
            InputStream inputStream = multipartFile.getInputStream();
            
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int len;
            while ((len = inputStream.read(bytes)) > 0) {
            	outputStream.write(bytes, 0, len);
            }
            inputStream.close();
            outputStream.close();
            
            attachmentContent = outputStream.toByteArray();
            
    		String mimeType = "text/plain";
    		String lowerCaseFilename = multipartFile.getOriginalFilename().toLowerCase();
    		if (lowerCaseFilename.endsWith(".gif")) {
    			mimeType = "image/gif";
    		} else if (lowerCaseFilename.endsWith(".jpg")) {			
    			mimeType = "image/jpeg";
    		} else {
    			mimeType = "text/plain";
    		}
    		            
            mailAttachments = new MailAttachment[] {new MailAttachment(attachmentContent, mimeType, multipartFile.getOriginalFilename())};
            jiraAttachments = new JiraAttachment[] {new JiraAttachment(attachmentContent, mimeType, multipartFile.getOriginalFilename())};
            redmineAttachments = new RedmineAttachment[] {new RedmineAttachment(attachmentContent, mimeType, multipartFile.getOriginalFilename())};
        }
        
        
		// String feedbackScope, String feedbackMessage, String currentUrl, String referrerUrl, String userAgent, String clientIpAddress		
		
		Property feedbackRecipientsProperty = DAORegistry.getPropertyDAO().get("feedback.email.to");
		if (feedbackRecipientsProperty == null || feedbackRecipientsProperty.getValue() == null || feedbackRecipientsProperty.getValue().length() == 0) {
			response.getOutputStream().print("ERR - No recipients defined for this feedback");
			return null;
		} 
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("submitter", username);
		map.put("submissionDate", new Date().toString());
		map.put("url", currentUrl);
		map.put("referrerUrl", referrerUrl);
		map.put("userAgent", userAgent);
		map.put("clientIpAddress", clientIpAddress);
		map.put("feedbackSummary", feedbackSummary);
		map.put("feedbackScope", feedbackScope);
		map.put("feedbackType", feedbackType);
		map.put("feedbackMessage", feedbackMessage);
		map.put("feedbackPriority", feedbackPriority);
		map.put("headerMessage", "");
		
		String project;
		if (feedbackProject == null || feedbackProject.isEmpty()) {
			if (currentUrl != null && currentUrl.contains("/Ebay")) {
				project = DAORegistry.getPropertyDAO().get("feedback.redmine.ebay.project").getValue();
			} else {
				project = DAORegistry.getPropertyDAO().get("feedback.redmine.project").getValue();
			}
		} else {
			project = feedbackProject;
		}

		Property feedbackJiraEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.jira.enabled");
		if ("1".equals(feedbackJiraEnabledProperty.getValue())) {
		
			String description = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/jira-feedback.txt", map);
			
			String issueType;
			if (feedbackType.equals("Improvement")) {
				issueType = JiraClient.ISSUE_TYPE_IMPROVEMENT;
			} else {
				issueType = JiraClient.ISSUE_TYPE_BUG;				
			}

			try {
				String issueKey = jiraClient.postJiraIssue(project, issueType, "FEEDBACK:" + feedbackSummary, description, jiraAttachments);			
				map.put("headerMessage", "This issue was posted on JIRA in the issue #" + issueKey);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("headerMessage", "This issue cannot be posted to jira" + e.getMessage());				
			}
		}				
		Property feedbackRedmineEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.enabled");
		if ("1".equals(feedbackRedmineEnabledProperty.getValue())) {
		
			String description = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/jira-feedback.txt", map);
			
			String issueType;
			if (feedbackType.equals("Improvement")) {
				issueType = WebRedmineClient.FEATURE_TRACKER_ID;
			} else {
				issueType = WebRedmineClient.BUG_TRACKER_ID;
			}

			try {
				String issueKey = redmineClient.postRedmineIssue(project, issueType, "FEEDBACK:" + feedbackSummary, description, redmineAttachments, feedbackPriority);				
				map.put("headerMessage", map.get("headerMessage") + "\nThis issue was posted on Redmine in the issue #" + issueKey);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("headerMessage", map.get("headerMessage") + "\nThis issue cannot be posted to Redmine:" + e.getMessage());				
			}
		}				

		String[] emails = feedbackRecipientsProperty.getValue().split(",");
		for(String email: emails) {			
			SpringUtil.getMailManager().sendMail(user.getFirstName() + " " + user.getLastName() + " (" + username + ")", email.trim(), "[" + Constants.getInstance().getHostName() + "] Feedback", "mail-feedback.txt", map, "text/plain", mailAttachments);
		}
		
		response.getOutputStream().print("OK");
		return null;
	}
	
	/*
	public ModelAndView loadEbayDownloadPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if (action != null && action.equalsIgnoreCase("download")) {
			Collection<Long> groupIds = new ArrayList<Long>();
			for (Object param: request.getParameterMap().keySet()) {
				if (param.toString().startsWith("group_")) {
					String groupIdStr = param.toString().replaceAll("group_", "");
					Long groupId = Long.valueOf(groupIdStr);
					groupIds.add(groupId);
				}				
			}
			response.setContentType("application/force-download");
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy-HH:mm");
		    response.setHeader("Content-Disposition", "attachment; filename=\"export_" + dateFormat.format(new Date()) + ".csv\"");
			TicketExportUtil.downloadCsvEbayFileFromIds(response.getWriter(), groupIds);
			response.getWriter().close();
			return null;
		} else {
		
			String eventIdStr = request.getParameter("eventId");
			Collection<EventContainer> eventContainers; 
			
			if (eventIdStr == null) {
				Collection<Integer> eventIds = new ArrayList<Integer>();
				for (Object param: request.getParameterMap().keySet()) {
					if (param.toString().startsWith("event_")) {
						eventIdStr = param.toString().replaceAll("event_", "");
						Integer eventId = Integer.valueOf(eventIdStr);
						eventIds.add(eventId);
					}
				}
				eventContainers = TicketExportUtil.exportToEbayFromEventIds(eventIds);
			} else {
				Integer eventId = Integer.valueOf(eventIdStr);
				eventContainers = TicketExportUtil.exportToEbayFromEventId(eventId);
			}
			ModelAndView mav = new ModelAndView("page-ebay-download");
			mav.addObject("eventContainers", eventContainers);
			return mav;
		}
	}
*/
	
	public void setStatCache(StatCache statCache) {
		this.statCache = statCache;
	}
	
/*	public void setLatestEventCache(LatestEventCache latestEventCache) {
		this.latestEventCache = latestEventCache;
	}
*/
	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}

	public void afterPropertiesSet() throws Exception {
		if (ticketListingFetcherMap == null) {
			throw new Exception("The property ticketListingFetcherMap must be set");
		}

	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}	

	public void setJiraClient(JiraClient jiraClient) {
		this.jiraClient = jiraClient;
	}
		
	public void setRedmineClient(WebRedmineClient redmineClient) {
		this.redmineClient = redmineClient;
	}

	public void setDashboardManager(DashboardManager dashboardManager) {
		this.dashboardManager = dashboardManager;
	}

	public ZonesManager getZonesManager() {
		return zonesManager;
	}

	public void setZonesManager(ZonesManager zonesManager) {
		this.zonesManager = zonesManager;
	}

}
