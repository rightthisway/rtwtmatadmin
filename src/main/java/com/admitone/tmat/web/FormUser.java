package com.admitone.tmat.web;

import com.admitone.tmat.data.User;

public class FormUser extends User {
	private static final long serialVersionUID = 1L;
	private String password2;
	private String []rolesStrList;
	private String []brokerId;
	private Boolean multiBroker=Boolean.FALSE;
	
	public FormUser() {
		super();
		setLocked(false);

		setRolesStrList(new String[]{"ROLE_USER"});
	}
	
	public String getPassword2() {
		return password2;
	}
	
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	public String[] getRolesStrList() {
		return rolesStrList;
	}

	public void setRolesStrList(String[] rolesStrList) {
		this.rolesStrList = rolesStrList;
	}

	

	public String[] getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String[] brokerId) {
		this.brokerId = brokerId;
	}

	public Boolean getMultiBroker() {
		return multiBroker;
	}

	public void setMultiBroker(Boolean multiBroker) {
		this.multiBroker = multiBroker;
	}
	
}
