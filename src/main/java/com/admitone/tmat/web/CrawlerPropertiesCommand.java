package com.admitone.tmat.web;


public class CrawlerPropertiesCommand {
	private int crawlerThreadCount;
	
	private String crawlerEmailTo;
	private boolean crawlerEmailCreatorEnabled;
	private boolean crawlerEmailLastUpdaterEnabled;
	private boolean crawlerEmailErrorEnabled;
	private boolean crawlerEmailStartStopEnabled;
	private boolean crawlerStartupEnabled;
	private int crawlerTicketTimeToLive;
	private String crawlerSchedulerPropertiesString;
	
	private String crawlerSportsSchedulerPropertiesString;
	private String crawlerConcertsSchedulerPropertiesString;
	private String crawlerTheaterSchedulerPropertiesString;
	private String crawlerDefaultSchedulerPropertiesString;
	private String crawlerLasVegasSchedulerPropertiesString;
	
	private String crawlerSchedulerMode;
	
	private Integer crawlerProactiveFactor;
	private Integer crawlerSortingTimeSlice;
	private Integer crawlerSchedulerStartHour;
	private Integer crawlerSchedulerStopHour;
	private Integer crawlMaxDuration;

	private boolean crawlerIdlePeriodEnabled;
	private Integer crawlerIdlePeriodStopHour;
	private Integer crawlerIdlePeriodStartHour;
	
	private Integer crawlerMinCrawlPeriod;
	
	private Integer crawlDurationThreshold;
	private Integer readyCrawlCountThreshold;
	private Boolean noTicketsFoundError;
	private Integer checkFrequency;

	private String admit1NotePattern;
	
	public String getCrawlerSchedulerMode() {
		return crawlerSchedulerMode;
	}

	public void setCrawlerSchedulerMode(String crawlerSchedulerMode) {
		this.crawlerSchedulerMode = crawlerSchedulerMode;
	}

	public boolean isCrawlerIdlePeriodEnabled() {
		return crawlerIdlePeriodEnabled;
	}

	public void setCrawlerIdlePeriodEnabled(boolean crawlerIdlePeriodEnabled) {
		this.crawlerIdlePeriodEnabled = crawlerIdlePeriodEnabled;
	}

	public CrawlerPropertiesCommand() {
	}

	public int getCrawlerThreadCount() {
		return crawlerThreadCount;
	}

	public void setCrawlerThreadCount(int crawlerThreadCount) {
		this.crawlerThreadCount = crawlerThreadCount;
	}

	public boolean isCrawlerEmailErrorEnabled() {
		return crawlerEmailErrorEnabled;
	}

	public void setCrawlerEmailErrorEnabled(boolean crawlerEmailErrorEnabled) {
		this.crawlerEmailErrorEnabled = crawlerEmailErrorEnabled;
	}

	public String getCrawlerEmailTo() {
		return crawlerEmailTo;
	}

	public void setCrawlerEmailTo(String crawlerEmailTo) {
		this.crawlerEmailTo = crawlerEmailTo;
	}

	public boolean isCrawlerStartupEnabled() {
		return crawlerStartupEnabled;
	}

	public void setCrawlerStartupEnabled(boolean crawlerStartupEnabled) {
		this.crawlerStartupEnabled = crawlerStartupEnabled;
	}

	public boolean isCrawlerEmailCreatorEnabled() {
		return crawlerEmailCreatorEnabled;
	}

	public void setCrawlerEmailCreatorEnabled(boolean crawlerEmailCreatorEnabled) {
		this.crawlerEmailCreatorEnabled = crawlerEmailCreatorEnabled;
	}

	public boolean isCrawlerEmailLastUpdaterEnabled() {
		return crawlerEmailLastUpdaterEnabled;
	}

	public void setCrawlerEmailLastUpdaterEnabled(
			boolean crawlerEmailLastUpdaterEnabled) {
		this.crawlerEmailLastUpdaterEnabled = crawlerEmailLastUpdaterEnabled;
	}

	public String getAdmit1NotePattern() {
		return admit1NotePattern;
	}

	public void setAdmit1NotePattern(String admit1NotePattern) {
		this.admit1NotePattern = admit1NotePattern;
	}

	public int getCrawlerTicketTimeToLive() {
		return crawlerTicketTimeToLive;
	}

	public void setCrawlerTicketTimeToLive(int crawlerTicketTimeToLive) {
		this.crawlerTicketTimeToLive = crawlerTicketTimeToLive;
	}

	public boolean isCrawlerEmailStartStopEnabled() {
		return crawlerEmailStartStopEnabled;
	}

	public void setCrawlerEmailStartStopEnabled(boolean crawlerEmailStartStopEnabled) {
		this.crawlerEmailStartStopEnabled = crawlerEmailStartStopEnabled;
	}
	
	public String getCrawlerSchedulerPropertiesString() {
		return crawlerSchedulerPropertiesString;
	}

	public void setCrawlerSchedulerPropertiesString(String crawlerSchedulerPropertiesString) {
		this.crawlerSchedulerPropertiesString = crawlerSchedulerPropertiesString;
	}

	public Integer getCrawlerSchedulerStartHour() {
		return crawlerSchedulerStartHour;
	}

	public void setCrawlerSchedulerStartHour(Integer crawlerSchedulerStartHour) {
		this.crawlerSchedulerStartHour = crawlerSchedulerStartHour;
	}

	public Integer getCrawlerSchedulerStopHour() {
		return crawlerSchedulerStopHour;
	}

	public void setCrawlerSchedulerStopHour(Integer crawlerSchedulerStopHour) {
		this.crawlerSchedulerStopHour = crawlerSchedulerStopHour;
	}

	public Integer getCrawlerIdlePeriodStopHour() {
		return crawlerIdlePeriodStopHour;
	}

	public void setCrawlerIdlePeriodStopHour(Integer crawlerIdlePeriodStopHour) {
		this.crawlerIdlePeriodStopHour = crawlerIdlePeriodStopHour;
	}

	public Integer getCrawlerIdlePeriodStartHour() {
		return crawlerIdlePeriodStartHour;
	}

	public void setCrawlerIdlePeriodStartHour(Integer crawlerIdlePeriodStartHour) {
		this.crawlerIdlePeriodStartHour = crawlerIdlePeriodStartHour;
	}
	
	public Integer getCrawlerSortingTimeSlice() {
		return crawlerSortingTimeSlice;
	}

	public void setCrawlerSortingTimeSlice(Integer crawlerSortingTimeSlice) {
		this.crawlerSortingTimeSlice = crawlerSortingTimeSlice;
	}

	public Integer getCrawlerProactiveFactor() {
		return crawlerProactiveFactor;
	}

	public void setCrawlerProactiveFactor(Integer crawlerProactiveFactor) {
		this.crawlerProactiveFactor = crawlerProactiveFactor;
	}

	public Integer getCrawlerMinCrawlPeriod() {
		return crawlerMinCrawlPeriod;
	}

	public void setCrawlerMinCrawlPeriod(Integer crawlerMinCrawlPeriod) {
		this.crawlerMinCrawlPeriod = crawlerMinCrawlPeriod;
	}

	public Integer getCrawlDurationThreshold() {
		return crawlDurationThreshold;
	}

	public void setCrawlDurationThreshold(Integer crawlDurationThreshold) {
		this.crawlDurationThreshold = crawlDurationThreshold;
	}

	public Integer getReadyCrawlCountThreshold() {
		return readyCrawlCountThreshold;
	}

	public void setReadyCrawlCountThreshold(Integer readyCrawlCountThreshold) {
		this.readyCrawlCountThreshold = readyCrawlCountThreshold;
	}

	public Boolean getNoTicketsFoundError() {
		return noTicketsFoundError;
	}

	public void setNoTicketsFoundError(Boolean noTicketsFoundError) {
		this.noTicketsFoundError = noTicketsFoundError;
	}

	public Integer getCheckFrequency() {
		return checkFrequency;
	}

	public void setCheckFrequency(Integer checkFrequency) {
		this.checkFrequency = checkFrequency;
	}
	
	public Integer getCrawlMaxDuration() {
		return crawlMaxDuration;
	}

	public void setCrawlMaxDuration(Integer crawlMaxDuration) {
		this.crawlMaxDuration = crawlMaxDuration;
	}

	public String getCrawlerSportsSchedulerPropertiesString() {
		return crawlerSportsSchedulerPropertiesString;
	}

	public void setCrawlerSportsSchedulerPropertiesString(
			String crawlerSportsSchedulerPropertiesString) {
		this.crawlerSportsSchedulerPropertiesString = crawlerSportsSchedulerPropertiesString;
	}

	public String getCrawlerConcertsSchedulerPropertiesString() {
		return crawlerConcertsSchedulerPropertiesString;
	}

	public void setCrawlerConcertsSchedulerPropertiesString(
			String crawlerConcertsSchedulerPropertiesString) {
		this.crawlerConcertsSchedulerPropertiesString = crawlerConcertsSchedulerPropertiesString;
	}

	public String getCrawlerTheaterSchedulerPropertiesString() {
		return crawlerTheaterSchedulerPropertiesString;
	}

	public void setCrawlerTheaterSchedulerPropertiesString(
			String crawlerTheaterSchedulerPropertiesString) {
		this.crawlerTheaterSchedulerPropertiesString = crawlerTheaterSchedulerPropertiesString;
	}

	public String getCrawlerDefaultSchedulerPropertiesString() {
		return crawlerDefaultSchedulerPropertiesString;
	}

	public void setCrawlerDefaultSchedulerPropertiesString(
			String crawlerDefaultSchedulerPropertiesString) {
		this.crawlerDefaultSchedulerPropertiesString = crawlerDefaultSchedulerPropertiesString;
	}

	public String getCrawlerLasVegasSchedulerPropertiesString() {
		return crawlerLasVegasSchedulerPropertiesString;
	}

	public void setCrawlerLasVegasSchedulerPropertiesString(
			String crawlerLasVegasSchedulerPropertiesString) {
		this.crawlerLasVegasSchedulerPropertiesString = crawlerLasVegasSchedulerPropertiesString;
	}	
	
}