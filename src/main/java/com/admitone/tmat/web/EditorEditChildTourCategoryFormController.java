package com.admitone.tmat.web;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.TourCategory;

public class EditorEditChildTourCategoryFormController extends SimpleFormController {
	public EditorEditChildTourCategoryFormController() {
		// TODO Auto-generated constructor stub
	}
	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		String action=request.getParameter("action");
		if("create".equalsIgnoreCase(action)){
			return new ChildTourCategoryCommand();
		}else{
			String id=request.getParameter("id");
			ChildTourCategory childTourCategory= DAORegistry.getChildTourCategoryDAO().get(Integer.parseInt(id));
			return new ChildTourCategoryCommand(childTourCategory);
		}
	}
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
			String info="";
			ChildTourCategoryCommand childTourCategoryCommand =(ChildTourCategoryCommand) command;
			childTourCategoryCommand.setName(childTourCategoryCommand.getName());
			ChildTourCategory childTourCategory = new ChildTourCategory(childTourCategoryCommand);
			ChildTourCategory exist= DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByNameAndTourCategoryName(childTourCategory.getName(),childTourCategory.getTourCategory().getName());
			if(exist!= null){				
				if(exist.getName().equals(childTourCategory.getName())){
					info="Child Category already exist.";
				}
				else{
					DAORegistry.getChildTourCategoryDAO().update(childTourCategory);
					info = "Child Category updated successfully.";
				}
			}else{
				try{
					
					if(childTourCategory.getId()==null){
						DAORegistry.getChildTourCategoryDAO().save(childTourCategory);
						info = "Child Category saved successfully.";
					}else{
						DAORegistry.getChildTourCategoryDAO().update(childTourCategory);
						info = "Child Category updated successfully.";
					}
				}catch (Exception e) {
					info="Error occured while saving Tour Category..";
				}
			}
			ModelAndView mav = new ModelAndView("page-editor-edit-child-tour-category");
			Collection<TourCategory> tourCategoryList = DAORegistry.getTourCategoryDAO().getAll();
			mav.addObject("childTourCategory", command);
			mav.addObject("tourCategoryList", tourCategoryList);
			mav.addObject("info", info);
			mav.addObject("action","create");
			return mav;
	}
	@Override
	protected Map<String, Collection<TourCategory>> referenceData(HttpServletRequest request) throws Exception {
		 Map<String, Collection<TourCategory>> map= new HashMap<String, Collection<TourCategory>>();
		 map.put("tourCategoryList", DAORegistry.getTourCategoryDAO().getAll());
		 return map;
	}
	
		
	
}
