package com.admitone.tmat.web;

import java.sql.Time;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
//import java.util.TimeZone;

//import javax.persistence.OneToMany;
//import javax.crypto.Mac;
//import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



//import org.apache.commons.codec.binary.Base64;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
import org.acegisecurity.context.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
//import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.ManageBrokerAudit;
import com.admitone.tmat.data.PresaleEvent;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.TourPriceAdjustment;
import com.admitone.tmat.data.TourPriceAdjustmentPk;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.eventcreation.CreationEventHit;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.utils.SectionRowStripper;
import com.admitone.tmat.web.pojo.Setting;



public class EditorEditEventFormController extends SimpleFormController {
	private final Logger logger = LoggerFactory.getLogger(EditorEditEventFormController.class);
	String isVenue = null;
	String isVenueAndArtist=null;
	Setting setting = null;
	public EditorEditEventFormController() {
		// TODO Auto-generated constructor stub
	}
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		String action = request.getParameter("action");
		ApplicationContext applicationContext =this.getApplicationContext();

		setting = (Setting) applicationContext.getBean("setting");
		if (action.equals("create")) {			
			EventCommand event = new EventCommand();
			event.setEventStatus(EventStatus.ACTIVE);
			event.setLocalDate(new java.sql.Date(new Date().getTime()));
			event.setLocalTime(new Time(0, 0, 0));
			event.setTimeZoneId("America/New_York");
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			event.setUpdatedBy(username);
			String artistId = request.getParameter("artistId");
			if(artistId!=null){
				event.setArtistId(Integer.parseInt(artistId));
			}else{
				event.setArtistId(0);
			}
			logger.info(new Date() + ":" + event.getId() +":" + event.getArtistId() + ": from formBackingObject in EditorEditEventFormController");
//			System.out.println(new Date() + ":" + event.getId() +":" + event.getTourId() + ": from formBackingObject in EditorEditEventFormController");
			return event;
		}
		
		int eventId = Integer.parseInt(request.getParameter("id"));
		Event baseEvent = DAORegistry.getEventDAO().get(eventId); 
		String venueId  = request.getParameter("venueId");
		String artistId  = request.getParameter("artistId");
		String multipleEventUrl=request.getParameter("multiEveUrl");
		String emeUrl=request.getParameter("emeUrl");
		
		if(venueId != null && !venueId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venueId;
		}else if(artistId != null && !artistId.isEmpty()){
			isVenue = "EditorManageTCAP?artistId="+artistId;
		}
		if(venueId != null && !venueId.isEmpty() && artistId != null && !artistId.isEmpty()){
			isVenue = "EditorManageTCAP?artistId="+artistId+"&venueId="+venueId;
		}
		if(emeUrl!=null){
		  if(venueId != null && !venueId.isEmpty()){
			 isVenue = "EditorEditMultipleEvent?venueId="+venueId;
		  }else if(artistId != null && !artistId.isEmpty()){
			 isVenue = "EditorEditMultipleEvent?artistId="+artistId;
		  }
		  if(venueId != null && !venueId.isEmpty() && artistId != null && !artistId.isEmpty()){
			 isVenue = "EditorEditMultipleEvent?artistId="+artistId+"&venueId="+venueId;
		  }
		}
		if(multipleEventUrl != null && !multipleEventUrl.isEmpty()){
			 isVenue = multipleEventUrl;
		}
		
		
		if (baseEvent.getLocalTime() == null) {
			Time time = new Time(0, 0, 0);
			baseEvent.setLocalTime(time);
			baseEvent.setTimeTbd(true);
		}
		if (baseEvent.getLocalDate() == null) {			
			baseEvent.setLocalDate(new java.sql.Date(new Date().getTime()));
			baseEvent.setDateTbd(true);
		}
		if(baseEvent.getEventType() != null){
			if(baseEvent.getEventType().equals(TourType.CONCERT.toString())){
				baseEvent.setEventType(TourType.SPORT);
			}

		}
		EventCommand event = new EventCommand(baseEvent);
		event.setId(eventId);
		return event; 
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		EventCommand eventCommand = (EventCommand)command;
		Event event = new Event(eventCommand);
		Date fromDate=null;
		Date eventDate=null;
		String hourStr= request.getParameter("localTime.hours");
		String minStr= request.getParameter("localTime.minutes");
		Integer hour = hourStr==null?0:Integer.parseInt(hourStr);
		Integer min = minStr==null?0:Integer.parseInt(minStr);
		if (request.getParameter("eventDateYear") != null) {
			Calendar fromCal = new GregorianCalendar(Integer.valueOf(request.getParameter("eventDateYear")),
													Integer.valueOf(request.getParameter("eventDateMonth")) - 1,
													Integer.valueOf(request.getParameter("eventDateDay")),
													hour, min, 0);
			Calendar eventCal = new GregorianCalendar(Integer.valueOf(request.getParameter("eventDateYear")),
					Integer.valueOf(request.getParameter("eventDateMonth")) - 1,
					Integer.valueOf(request.getParameter("eventDateDay")),
					0, 0, 0);
			fromDate = fromCal.getTime();
			eventDate = eventCal.getTime();
//			fromCal.setTimeZone(TimeZone.getTimeZone("EST"));
				
		} else if (event != null) {
			fromDate = new Date();//event.getDate(); 			
		}
		event.setLocalDate(new java.sql.Date(eventDate.getTime()));
		String info="";
		String action = request.getParameter("action");
		String tcap = request.getParameter("tcap");
		
//		System.out.println("*** " + event.getTimeTbd());
		
		if (event.getTimeTbd()) {
			event.setLocalTime(null);
		}
		if(event.getDateTbd()){
			event.setLocalDate(null);
		}
		if (action != null && action.equalsIgnoreCase("create")) {
			// save it before to get its id
			try {
				
				if(event.getName() == null || event.getName().equals("")){
				    errors.rejectValue("name", "error.nullValue", "Name cannot be empty");
				    ModelAndView mav = showForm(request, errors, getFormView());
					//mav.addObject("artists", DAORegistry.getArtistDAO().getAll());
					//mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
					//mav.addObject("events", DAORegistry.getEventDAO().getAllEventsByStatusAndName(EventStatus.ACTIVE));
					return mav;
				}
				
				Integer eventId = null;
				if(event.getId() == null || event.getId().equals(0)){
					eventId = new Integer(DAORegistry.getEventDAO().getMaxEventId() + 1);
					event.setId(eventId);
				}
				boolean flag=true;
				Collection<Event> dbEvents = new ArrayList<Event>();
				if(event.getDate() == null && event.getTime() == null){
					Event tempevent = DAORegistry.getEventDAO().getEventWithSameArtistAndVenueAndNameAndDateTBDAndTimeTBD(event.getArtistId(), event.getVenueId(), event.getName());
					if(tempevent !=  null && !tempevent.getId().equals(event.getId())) {
						info = "ERR|An event with the same name, date and a conflicting time(" + tempevent.getFormattedEventDate() + ") already exists";
						flag = false;
					}
				}else{
					dbEvents = DAORegistry.getEventDAO().getAllEventsByNameAndDate(event.getName(), fromDate);
				}	
				
				String tempVenueId = request.getParameter("tempVenueId");
				if(null != tempVenueId && !tempVenueId.isEmpty() ){
					event.setVenueId(Integer.valueOf(tempVenueId));
				}
				
				String tempArtistId = request.getParameter("tempArtistId");
				
				if(null != tempArtistId && !tempArtistId.isEmpty() ){
					event.setArtistId(Integer.valueOf(tempArtistId));
				}else{
					info = "ERR|Event must have a artist.";
					flag = false;
				}
				
				
				for(Event tempEvent: dbEvents) {
					if(event.getDate() != null && tempEvent.getDate()!= null){
					if (event.getDate() != null && tempEvent.getDate().equals(event.getDate())) {
						// if the event is TBD,then we cannot have an existing event on the same date
						// if an existing event with TBD time exists, then ERROR
						if (event.getTime() == null || tempEvent.getTime() == null) { 
							info = "ERR|An event with the same name, date and a conflicting time(" + event.getFormattedEventDate() + ") already exists";
							flag = false;
							break;
						}
						
						if (tempEvent.getTime().equals(event.getTime()) && tempEvent.getTimeZoneId() != null && tempEvent.getTimeZoneId().equals(event.getTimeZoneId())) {
							info = "ERR|An event with the same name, date and time already exists";
							flag = false;
							break;
						}
						
						if (!tempEvent.getVenueId().equals(event.getVenueId())) {
							info = "ERR|This event cannot happen on the same date than another event with the same name but at a different venue (" + tempEvent.getFormattedVenueDescription() + ")";
							flag = false;
					
							break;
						}
					}
					}
				}
				
				if(flag){
//					System.out.println("before");
					
					Venue venue = event.getVenue();
					Artist artist = event.getArtist();
					
//					tour.getArtistType();
					String tourtype =artist.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName();
					System.out.println(tourtype);
					if ( tourtype.equalsIgnoreCase("Sports"))
					{
						event.setBrokerId(2);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourtype.equalsIgnoreCase("Concerts"))
					{
						event.setBrokerId(2);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourtype.equalsIgnoreCase("Theater"))
					{
						event.setBrokerId(3);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourtype.equalsIgnoreCase("Other"))
					{
						event.setBrokerId(3);
						event.setBrokerStatus("ACTIVE");
					}
					
					event.setCreationType(CreationType.MANUAL);
					
					DAORegistry.getEventDAO().save(event);
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					
					//ManageBrokerAudit managebrokeraudit = new ManageBrokerAudit();
					//managebrokeraudit = new ManageBrokerAudit();
					//managebrokeraudit.setEventId(event.getId());
					//managebrokeraudit.setBrokerId(event.getBrokerId());
					//managebrokeraudit.setBrokerStatus(event.getBrokerStatus());
					//managebrokeraudit.setUserName(username);
					//managebrokeraudit.setCreatedDate(new Date());
					//managebrokeraudit.setAction("Manual Event Creation");
					//DAORegistry.getManageBrokerAuditDAO().save(managebrokeraudit);
					
					//Tamil 02/27/2018 : Removed Presale Logic
					/*if(event.getPresaleEvent()) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, 7);
						cal.set(Calendar.HOUR_OF_DAY, 0);
						cal.set(Calendar.MINUTE, 0);
						cal.set(Calendar.SECOND, 0);
						Date applyTime = new Date(cal.getTimeInMillis());
						
						PresaleEvent presaleEvent = new PresaleEvent();
						presaleEvent.setEvent(event);
						presaleEvent.setApplyDate(applyTime);
						presaleEvent.setApplyTime(applyTime);
						presaleEvent.setStatus("ACTIVE");
						presaleEvent.setCreatedBy(username);
						presaleEvent.setCreatedDate(event.getCreationDate());
						presaleEvent.setLastUpdatedBy(username);
						presaleEvent.setLastUpdated(event.getCreationDate());
						DAORegistry.getPresaleEventDAO().save(presaleEvent);
					}*/
					
					Date now = new Date();
					EventAudit ea=new EventAudit();
					ea.setEventId(eventId);
					ea.setUserName(username);
					ea.setAction("Manual Create");
					ea.setModifiedDate(now);
					ea.setEventName(event.getName());
					ea.setArtistId(event.getArtistId());
					ea.setArtistName(event.getArtist().getName());
					ea.setEventDate(event.getLocalDate());
					ea.setEventTime(event.getLocalTime());
					ea.setVenueId(event.getVenueId());
					ea.setVenueName(event.getVenue().getBuilding());
					if(event.getVenueCategoryId()!= null) {
						ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
					}
					if(event.getEventType().name()!= null) {
						ea.setEventType(event.getEventType().name());
					}
					
					if(event.getNoPrice()!= null){
						ea.setNoPrice("YES");
					} else {
						ea.setNoPrice("NO");
					}
					if(event.getAdmitoneId()!= null) {
						ea.setAdmitoneId(event.getAdmitoneId());
					}
					ea.setPresaleEvent(event.getPresaleEvent());
					DAORegistry.getEventAuditDAO().save(ea);

					java.sql.Date sqlEventDate = null;
					if(event.getLocalDate() != null) {
						sqlEventDate = new java.sql.Date(event.getLocalDate().getTime());
					}
					CreationEventHit eventHit = new CreationEventHit(event.getAdmitoneId(), /*event.getTourId(), */event.getArtistId(), event.getVenueId(), 
							event.getName(),/* tour.getName(), */event.getArtist().getName(), sqlEventDate, event.getLocalTime(), event.getEventType(), venue.getBuilding(), venue.getCity(), venue.getState(), venue.getCountry(),venue.getZipcode(), "");
					
					CreationEventListManager.addInExistingEvents(eventHit,event.getId());
//					System.out.println("after");
					Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
					if(property!=null){
						String dependents = property.getValue();
						if(dependents!= null && !dependents.isEmpty()){
							String dependentList[] = dependents.split(",");
							List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
							for(String dependent:dependentList){
								if(dependent == null || dependent.isEmpty()){
									continue;
								}
								TMATDependentEvent dependentEvent = new TMATDependentEvent();
								dependentEvent.setEventId(event.getId());
								dependentEvent.setDependent(dependent);
								dependentEvent.setAdd(true);
								tmatDependents.add(dependentEvent);
							}
							if(!tmatDependents.isEmpty()){
								DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
							}
						}
					}
					
				
				
					
				}
			} catch (Exception e) {
				e.printStackTrace();
				// duplicate key
		       errors.rejectValue("id", "error.duplicateId", "Id already in use");
		       return showForm(request, errors, getFormView());
	        }
			if("".equals(info)){
			info = "The event has been created";
			}
		} else {
			Event beforeUpateEvent= DAORegistry.getEventDAO().get(event.getId());
			TourType beforeUpdateEventType = beforeUpateEvent.getEventType();
			
			event.setArtistId(beforeUpateEvent.getArtistId());
			event.setVenueId(beforeUpateEvent.getVenueId());
			
			//to set event overridden
			if(!beforeUpdateEventType.equals(event.getEventType())){
				event.setEventOverridden("MENUAL");
			}
			String artistId = request.getParameter("artistId");
			if(!"".equals(artistId) && event.getArtistId().equals(artistId)){
//				System.out.println("tourID====="+tourId);
				event.setArtistId(Integer.parseInt(artistId));
			}
			
			String tempVenueId = request.getParameter("tempVenueId");
			if(null != tempVenueId && !tempVenueId.isEmpty() ){
				event.setVenueId(Integer.valueOf(tempVenueId));
			}
			
			String tempArtistId = request.getParameter("tempArtistId");
			
			if(null != tempArtistId && !tempArtistId.isEmpty() ){
				event.setArtistId(Integer.valueOf(tempArtistId));
			}
			
			
			
//			System.out.println("tour====="+tourId);
			
			event.setLastUpdate(new Date());
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			event.setUpdatedBy(username);
			
			boolean flag=true;
			Collection<Event> dbEvents = new ArrayList<Event>();
			if(event.getDate() == null && event.getTime() == null){
				Event tempevent = DAORegistry.getEventDAO().getEventWithSameArtistAndVenueAndNameAndDateTBDAndTimeTBD(event.getArtistId(), event.getVenueId(), event.getName());
				if(tempevent !=  null && !tempevent.getId().equals(event.getId())){
					info = "ERR|An event with the same name, date and a conflicting time(" + event.getFormattedEventDate() + ") already exists";
					flag = false;
				}				
			}else{
				dbEvents = DAORegistry.getEventDAO().getAllEventsByNameAndDate(event.getName(), fromDate);
			}			
			for(Event tempEvent: dbEvents) {
				if(tempEvent.getId().equals(event.getId())){
					continue;
				}
				
				if (tempEvent.getVenueId().equals(event.getVenueId())) {
					info = "ERR|This event cannot happen on the same date than another event with the same name but at a different venue (" + tempEvent.getFormattedVenueDescription() + ")";
					flag = false;
					break;
				}
				
			}
			if(flag){
				DAORegistry.getEventDAO().update(event);
				
				Date now = new Date();
				//Tamil 04/27/2018 : Removed Presale Logic
				/*PresaleEvent presaleEvent = DAORegistry.getPresaleEventDAO().getActivePresaleEventByEventId(event.getId());
				if(event.getPresaleEvent()) {
					if(presaleEvent == null) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, 7);
						cal.set(Calendar.HOUR_OF_DAY, 0);
						cal.set(Calendar.MINUTE, 0);
						cal.set(Calendar.SECOND, 0);
						Date applyTime = new Date(cal.getTimeInMillis());
						
						presaleEvent = new PresaleEvent();
						presaleEvent.setEvent(event);
						presaleEvent.setApplyDate(applyTime);
						presaleEvent.setApplyTime(applyTime);
						presaleEvent.setStatus("ACTIVE");
						presaleEvent.setCreatedBy(username);
						presaleEvent.setCreatedDate(now);
						presaleEvent.setLastUpdatedBy(username);
						presaleEvent.setLastUpdated(now);
						DAORegistry.getPresaleEventDAO().save(presaleEvent);
					}
				} else {
					if(presaleEvent != null) {
						presaleEvent.setStatus("DELETED");
						presaleEvent.setLastUpdatedBy(username);
						presaleEvent.setLastUpdated(now);
						DAORegistry.getPresaleEventDAO().save(presaleEvent);
					}
				}*/
				EventAudit ea = new EventAudit();
				ea.setEventId(event.getId());
				ea.setUserName(username);
				ea.setAction("Updated");
				ea.setModifiedDate(now);
				ea.setEventName(event.getName());
				ea.setArtistId(event.getArtistId());
				ea.setArtistName(event.getArtist().getName());
				ea.setEventDate(event.getLocalDate());
				ea.setEventTime(event.getLocalTime());
				ea.setVenueName(DAORegistry.getVenueDAO().get(event.getVenueId()).getBuilding());
				ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
				if(event.getVenueCategoryId()!= null) {
					ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
				}
				if(event.getEventType()!=null && event.getEventType().name()!= null) {
					ea.setEventType(event.getEventType().name());
				}
				if(event.getNoPrice()!= null){
					ea.setNoPrice("YES");
				} else {
					ea.setNoPrice("NO");
				}
				ea.setAdmitoneId(event.getAdmitoneId());
				ea.setVenueId(event.getVenueId());
				ea.setPresaleEvent(event.getPresaleEvent());
				DAORegistry.getEventAuditDAO().save(ea);
				
				Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
				if(property!=null){
					String dependents = property.getValue();
					if(dependents!= null && !dependents.isEmpty()){
						List<TMATDependentEvent> list = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
						List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
						for(TMATDependentEvent dependent:list){
							if(dependents.contains(dependent.getDependent())){
//								if(dependent.isAdd()){
//									dependents =dependents.replace(dependent.getDependent(),"");
//								}else{
									dependents = dependents.replace(dependent.getDependent(),"");
									dependent.setAdd(true);
									tmatDependents.add(dependent);
//								}
							}
						}
						String depenedentsList[] = dependents.split(",");
						
						for(String dependent:depenedentsList){
							if(dependent == null || dependent.isEmpty()){
								continue;
							}
							
							TMATDependentEvent tmatDependent = new TMATDependentEvent();
							tmatDependent.setDependent(dependent);
							tmatDependent.setEventId(event.getId());
							tmatDependent.setAdd(true);
							tmatDependents.add(tmatDependent);
							
						}
						if(!tmatDependents.isEmpty()){
							DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
						}
					}
				}
				if (!beforeUpdateEventType.equals(event.getEventType())){
					SectionRowStripper.recomputeEventShortSectionsAndRows(event.getId());
				}
				info = "The event has been updated";
			}
			
		}
		/*if(tcap != null && !tcap.isEmpty()){
			return new ModelAndView(new RedirectView(""+tcap));
		}*/
		return new ModelAndView(new RedirectView("EditorDetailArtist?artistId=" + event.getArtistId() + "&info=" + info));
	}

	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		
		//mav.addObject("artists", DAORegistry.getArtistDAO().getAllArtistsWithGrandChildBySqlQuery());
		mav.addObject("venueCategories",DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(((EventCommand)mav.getModelMap().get("event")).getVenueId()));
		//mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
//		mav.addObject("tnExchangeEvents", DAORegistry.getAdmitoneEventDAO().getAll());
		//mav.addObject("events", DAORegistry.getEventDAO().getAllActiveEventsBySqlQuery());
		mav.addObject("currDate", new Date());
		mav.addObject("fromTcap",isVenue);
		return mav;
	}
	public Setting getSetting() {
		return setting;
	}
	public void setSetting(Setting setting) {
		this.setting = setting;
	}
}