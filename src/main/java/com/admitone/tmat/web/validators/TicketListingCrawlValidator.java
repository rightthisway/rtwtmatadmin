package com.admitone.tmat.web.validators;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.web.TicketListingCrawlCommand;

/**
 * Validator class to validate ticket listing crawl form when creating/editing ticket listing crawl.
 */
public class TicketListingCrawlValidator implements Validator{
	
//	private TicketListingCrawler ticketListingCrawler;
	private Map<String, TicketListingFetcher> ticketListingFetcherMap = new HashMap<String, TicketListingFetcher>();	

	public boolean supports(Class clazz) {
		return clazz.equals(TicketListingCrawlCommand.class); 
	}

	public void validate(Object command, Errors errors) {
		TicketListingCrawlCommand crawlCommand = (TicketListingCrawlCommand)command;
		
		if (crawlCommand == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 

		TicketListingCrawl crawl = crawlCommand.getTicketListingCrawl();
		
		if (crawl.getName() == null || crawl.getName().length() == 0) {
			errors.rejectValue("ticketListingCrawl.name", "error.invalidName",
					"Please enter a name.");
		}
		// TODOJMS
		/*if (!crawl.getName().equals(crawlCommand.getOriginalName()) && ticketListingCrawler.getTicketListingCrawlByName(crawl.getName()) != null) {
			errors.rejectValue("ticketListingCrawl.name", "error.invalidName",
					"Please enter another name. It is already in use.");
		}*/

		// event id should belong to the selected tour
		if (crawlCommand.getTicketListingCrawl().getEvent() != null
				&& !crawlCommand.getTicketListingCrawl().getEventId().equals(crawlCommand.getTicketListingCrawl().getEventId())) { 
			errors.rejectValue("ticketListingCrawl.eventId", "error.invalidId",
					"Choose an event belonging to the tour that you had selected.");
		}

		// only ebay, eventinventory and seatwavefeed can use event AUTO
		if (!crawlCommand.getTicketListingCrawl().getSiteId().equals(Site.EBAY) 
				&& !crawlCommand.getTicketListingCrawl().getSiteId().equals(Site.EVENT_INVENTORY) 
				&& !crawlCommand.getTicketListingCrawl().getSiteId().equals(Site.SEATWAVE_FEED)
				&& crawlCommand.getTicketListingCrawl().getEventId() == null) {
			errors.rejectValue("ticketListingCrawl.eventId", "error.invalidId",
					"You can select AUTO only for eBay and Event Inventory crawler.");
		}
		
		TicketListingFetcher fetcher = ticketListingFetcherMap.get(crawlCommand.getTicketListingCrawl().getSiteId());
		if (!fetcher.isValidUrl(crawlCommand.getTicketListingCrawl().getQueryUrl())) {
			errors.rejectValue("ticketListingCrawl.queryUrl", "error.invalidUrl",
			"The URL you entered is invalid.");			
		}

		if (crawlCommand.getTicketListingCrawl().getSiteId().equals(Site.EI_MARKETPLACE)) { 
			try {
				Integer.parseInt(crawlCommand.getQueryEventId());
			} catch (Exception e) {
				errors.rejectValue("queryEventId", "error.invalidId",
				"The event id you entered is invalid.");							
			}			
		}
	}

//	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
//		this.ticketListingCrawler = ticketListingCrawler;
//	}	

	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherMap;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherMap) {
		this.ticketListingFetcherMap = ticketListingFetcherMap;
	}

}
