package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.web.MailerPropertiesCommand;

/**
 * Validator class to validate mailer properties form.
 */
public class MailerPropertiesValidator implements Validator{

	public boolean supports(Class clazz) {
		try {
			return MailerPropertiesCommand.class.isInstance(clazz.newInstance());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void validate(Object command, Errors errors) {
		MailerPropertiesCommand mailerPropertiesCommand = (MailerPropertiesCommand)command;
		
		if (mailerPropertiesCommand == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 

		if (mailerPropertiesCommand.getSmtpHost() == null
				|| mailerPropertiesCommand.getSmtpHost().trim().length() == 0) {
			errors.rejectValue("smtpHost", "error.invalid",
					"Please enter a smtp host.");
		}

		if (mailerPropertiesCommand.getSmtpPort() < 0 || mailerPropertiesCommand.getSmtpPort() > 65535) {
			errors.rejectValue("smtpHost", "error.invalid",
					"Invalid smtp port.");
		}

		
		if (mailerPropertiesCommand.getSmtpFrom() == null
				|| mailerPropertiesCommand.getSmtpFrom().trim().length() == 0) {
			errors.rejectValue("smtpFrom", "error.invalid",
					"Please enter an email from.");
		}

		if (mailerPropertiesCommand.isSmtpAuth()) {
			
			if (mailerPropertiesCommand.getSmtpUsername() == null
					|| mailerPropertiesCommand.getSmtpUsername().length() == 0) {
				errors.rejectValue("smtpUsername", "error.invalid",
						"Please enter a username.");
			}

			/*
			if (mailerPropertiesCommand.getSmtpPassword() == null
					|| mailerPropertiesCommand.getSmtpPassword().length() == 0) {
				errors.rejectValue("smtpPassword", "error.invalid",
						"Please enter a password.");
			}
			*/
		}
		
		if (mailerPropertiesCommand.getSmtpSecureConnection() == null
				|| (
						!mailerPropertiesCommand.getSmtpSecureConnection().equals("no")
						&& !mailerPropertiesCommand.getSmtpSecureConnection().equals("tls")
						&& !mailerPropertiesCommand.getSmtpSecureConnection().equals("ssl")
					)
		) {
			errors.rejectValue("smtpSecureConnection", "error.invalid",
					"Invalid Secure Connection.");
		}
	}
}
