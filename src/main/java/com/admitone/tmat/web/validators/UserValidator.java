package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.User;
import org.springframework.validation.Validator;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.User;
import com.admitone.tmat.web.FormUser;

/**
 * Validator class to validate user form when creating/editing user.
 */
public class UserValidator implements Validator {

	public boolean supports(Class givenClass) {
		return givenClass.equals(FormUser.class);
	}

	public void validate(Object obj, Errors errors) {
		FormUser givenUser = (FormUser) obj;

		if (givenUser == null) {
	         errors.reject("error.nullpointer", "Please fill all the fields.");
	         return;			
		}

		if (givenUser.getUsername().isEmpty()) {
			   errors.rejectValue("username", "error.invalidUsername", "Please provide a username");
		} else if (!givenUser.getUsername().matches("^\\w{2,25}$")){
			   errors.rejectValue("username", "error.invalidUsername", "Username must be composed of between 2 and 25 letters or numbers.");
		}
		
		if (givenUser.getEmail().isEmpty()) {
		   errors.rejectValue("email", "error.invalidEmail", "Please provide an email address");
		} else if (!givenUser.getEmail().matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")){
		   errors.rejectValue("email", "error.invalidEmail", "Incorrect email address");
		}
		
		if (!givenUser.getUsername().isEmpty())
		{
			String newuser= givenUser.getUsername();
			User olduser =DAORegistry.getUserDAO().getUserByUsername(newuser);
			if(olduser==null )
			{
				;
			}
			else
			
			{
				errors.rejectValue("username", "error.invalidUsername", "Username already exist");
			}
			
		}
		
		if (givenUser.getPassword().length() < 6) {
		    errors.rejectValue("password", "error.invalidPassword", "Password must have at least 6 characters.");			
		}

		if (!givenUser.getPassword2().equals(givenUser.getPassword())) {
		    errors.rejectValue("password2", "error.invalidPassword", "Passwords do not match.");		
		}

		if (givenUser.getRolesStrList().length == 0) {
		    errors.rejectValue("roles", "error.invalidRolesOptions", "Please select at least a role.");					
		}
	}
}
