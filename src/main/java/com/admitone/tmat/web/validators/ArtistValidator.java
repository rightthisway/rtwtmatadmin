package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.data.Artist;

/**
 * Validator class to validate artist form when creating/editing artist.
 */
public class ArtistValidator implements Validator{

	public boolean supports(Class clazz) {
		return clazz.equals(Artist.class);
	}

	public void validate(Object command, Errors errors) {
		Artist artist = (Artist)command;
		
		if (artist == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 
		
		if (artist.getName() == null || artist.getName().length() == 0) {
			errors.rejectValue("artist", "error.invalidArtist",
					"Please enter an artist name.");
		}
	}	
}
