package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.web.TourCommand;

/**
 * Validator class to validate tour form when creating/editing tour.
 */
public class TourValidator implements Validator{

	public boolean supports(Class clazz) {
		return clazz.equals(TourCommand.class);
	}

	public void validate(Object command, Errors errors) {
		TourCommand tourCommand = (TourCommand)command;
		
		if (tourCommand == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 

		/*
		private Integer id;
		private Integer artistId;
		private String name;
		private String location;
		private String startDate;
		private String endDate;
		*/
		
		if (tourCommand.getName() == null || tourCommand.getName().length() == 0) {
			errors.rejectValue("name", "error.invalidName",
					"Please enter a name.");
		}

		/*
		if (tourCommand.getLocation() == null || tourCommand.getLocation().length() == 0) {
			errors.rejectValue("location", "error.invalidLocation",
					"Please enter a location.");
		}
		*/
	}	
}
