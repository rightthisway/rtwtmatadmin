package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.data.Venue;

/**
 * Validator class to validate venue form when creating/editing venue.
 */
public class VenueValidator implements Validator{

	public boolean supports(Class clazz) {
		return clazz.equals(Venue.class);
	}

	public void validate(Object command, Errors errors) {
		Venue venue = (Venue)command;
		
		if (venue == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 
		
		if (venue.getBuilding() == null || venue.getBuilding().length() == 0) {
			errors.rejectValue("building", "error.invalidBuilding",
					"Please enter a building name.");
		}

		if (venue.getCity() == null || venue.getCity().length() == 0) {
			errors.rejectValue("city", "error.invalidCity",
					"Please enter a city name.");
		}

		if (venue.getState() == null || venue.getState().length() == 0) {
			errors.rejectValue("state", "error.invalidState",
					"Please enter a state name.");
		}

		if (venue.getCountry() == null || venue.getCountry().length() == 0) {
			errors.rejectValue("country", "error.invalidCountry",
					"Please enter a country name.");
		}
	}
	
}
