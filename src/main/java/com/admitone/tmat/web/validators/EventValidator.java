package com.admitone.tmat.web.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.admitone.tmat.web.EventCommand;

/**
 * Validator class to validate event form when creating/editing event.
 */
public class EventValidator implements Validator{

	public boolean supports(Class clazz) {
		return clazz.equals(EventCommand.class);
	}

	public void validate(Object command, Errors errors) {
		EventCommand event = (EventCommand)command;
		if (event == null) {
			errors.reject("error.nullpointer", "Null data received");
			return;
		} 
	}
	
}
