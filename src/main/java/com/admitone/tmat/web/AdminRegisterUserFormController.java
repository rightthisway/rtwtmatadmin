package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.Role;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserBrokerDetails;
import com.admitone.tmat.web.FormUser;

public class AdminRegisterUserFormController extends SimpleFormController {
	protected ModelAndView onSubmit(
            HttpServletRequest request,
            HttpServletResponse response, 
            Object command,
            BindException errors)
            throws Exception {
	  FormUser givenUser = (FormUser) command;
//	  int flag=0;
	  User user = new User();
	  Broker broker=DAORegistry.getBrokerDAO().get(1);
	  user.setEmail(givenUser.getEmail());
	  user.setFirstName(givenUser.getFirstName());
	  user.setLastName(givenUser.getLastName());
//	  user.setLocked(givenUser.getLocked());
	  user.setAndEncryptPassword(givenUser.getPassword());
	  user.setPhone(givenUser.getPhone());
	  user.setUsername(givenUser.getUsername());
	  user.setLocked(givenUser.getLocked());
	  user.setEmailCrawlErrorCreatorEnabled(true);
	  user.setEmailCrawlErrorLastUpdaterEnabled(true);
	  user.setBroker(broker);
//	  Set<UserBrokerDetails> userBrokerDetails = new HashSet<UserBrokerDetails>();
//	  String []brokerIds=givenUser.getBrokerId();
	  
	  /*if(brokerIds.length!=0){
	     if(brokerIds.length==1){
		   if(brokerIds[0].equals("0")){
			for(int i=1;i<6;i++){
				  UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
				  userBrokerDetail.setUser(user);
				  userBrokerDetail.setBrokerId(i);
				  userBrokerDetails.add(userBrokerDetail);
				  	
			}	
		   }
	   }
		  user.setUserBrokerDetails(userBrokerDetails);
	  }
	  if(brokerIds.length!=0 && !brokerIds[0].equals("0")){
	        for(String brokerId:givenUser.getBrokerId()){
		        UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
		        userBrokerDetail.setUser(user);
		        userBrokerDetail.setBrokerId(Integer.valueOf(brokerId));
		        userBrokerDetails.add(userBrokerDetail);
	   }
	  }*/
	 
//	      user.setUserBrokerDetails(userBrokerDetails);
	 
	   /* if(givenUser.getBrokerId().length==0){
	    	UserBrokerDetails userBrokerDetail=new UserBrokerDetails();
	    	userBrokerDetail.setBrokerId(1);
	    	userBrokerDetail.setUser(user);
	    	userBrokerDetails.add(userBrokerDetail);
	    	user.setUserBrokerDetails(userBrokerDetails);
		 // broker=DAORegistry.getBrokerDAO().get(1);
	  }else{
		  for(String brokerId:givenUser.getBrokerId())
		    broker=DAORegistry.getBrokerDAO().get(Integer.valueOf(brokerId));
	  }*/
	  /*String emailSuffix = "@"+givenUser.getEmail().split("@")[1];
	  broker = DAORegistry.getBrokerDAO().getBrokerByEmailSuffix(emailSuffix); 
	  if(broker == null){
		  errors.rejectValue("email", "error.duplicateEmail", "Email does not belongs to a valid broker.");
		  return showForm(request, errors, getFormView());
	  }
	  if(broker!=null && broker.getId()!=1){
		  for(String role:givenUser.getRolesStrList()){
			  if(role!=null  && !role.toUpperCase().contains("LITE")){
				  errors.rejectValue("email", "error.roles", givenUser.getEmail().split("@")[1] + " Broker Can not have this role.");
				  return showForm(request, errors, getFormView()); 
			  }
		  }
	  }*/
	  //user.setBroker(broker);
	//user.setBroker(givenUser.getBroker());
	        Set<Role> roles = new HashSet<Role>();
	  for (String roleStr: givenUser.getRolesStrList()) {
		    Role role = DAORegistry.getRoleDAO().getRoleByName(roleStr);
		    roles.add(role);
        }
	        user.setRoles(roles);
	 
	  /*for (String roleStr: givenUser.getRolesStrList()) {
	         if(roleStr.equals("role_super_admin")|| givenUser.getMultiBroker()==true){
	        	 flag=1;
		          user.setMultiBrokerAccess(1);
	       }else{
	    	   if(flag==0){
		          user.setMultiBrokerAccess(0);
	    	   }
	     }
	  }
	 
	  if(givenUser.getRolesStrList().equals("role_lite_user") ||givenUser.getRolesStrList().equals("role_lite_admin"))
	  {
		if(givenUser.getBrokerId()==null)
		 // if(user.getBroker() == null)
		  {
			  errors.rejectValue("brokerId", "error.brokerId", "Please select a broker."); 
			  return showForm(request, errors, getFormView());	
		  }
	  }
	  if (givenUser.getRolesStrList().equals("role_super_admin") || givenUser.getRolesStrList().equals("role_admin") || givenUser.getRolesStrList().equals("role_user") ||givenUser.getRolesStrList().equals("role_editor"))
	  {
		 // user.setBroker(DAORegistry.getBrokerDAO().get(1));
	  }*/
	  try {
	  	DAORegistry.getUserDAO().save(user);
	  } catch(Exception e) {
    	  User duplicateUser = DAORegistry.getUserDAO().getUserByUsernameOrEmail(user.getUsername(), user.getEmail());
    	  e.printStackTrace();
    	  if (user.getUsername().equals(duplicateUser.getUsername())) {
        	  errors.rejectValue("username", "error.duplicateUsername", "Username already in use");    		  
    	  }

    	  if (user.getEmail().equals(duplicateUser.getEmail())) {
    		  errors.rejectValue("email", "error.duplicateEmail", "Email already in use");
    	  }
    	
    	  return showForm(request, errors, getFormView());		  
	  }
	  return new ModelAndView(new RedirectView("AdminManageUsers"));
	}
	
	protected Object formBackingObject(HttpServletRequest request)
	throws ServletException {
		request.setAttribute("roles", DAORegistry.getRoleDAO().getAll());
		
		/*List<Object[]> brokerList = DAORegistry.getBrokerDAO().getAllBroker();
		
		List<Broker> allBroker = new ArrayList<Broker>();
		
		for(Object[] o : brokerList)
		{
			// we do not want mztix broker to display.
		//	if(!o[1].toString().toLowerCase().contains("mztix"))
			{
			Broker b = new Broker();
			b.setId(Integer.parseInt(o[0].toString()));
			b.setName(o[1].toString());
			allBroker.add(b);
			}
		}*/
		
//		request.setAttribute("brokers",  allBroker);
		return new FormUser();		
	}
}
