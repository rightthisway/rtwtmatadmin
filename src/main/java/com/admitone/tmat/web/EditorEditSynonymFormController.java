package com.admitone.tmat.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.utils.SectionRowStripper;
import com.admitone.tmat.utils.SpringUtil;

public class EditorEditSynonymFormController extends SimpleFormController {
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		String action = request.getParameter("action");
		if (action != null && action.equals("createTheater")) {
			Synonym synonym = new Synonym();
			synonym.setType("Theater");
			return synonym;
		}
		if (action != null && action.equals("createConcert")) {
			Synonym synonym = new Synonym();
			synonym.setType("Concert");
			return synonym;
		}
		String value1 = request.getParameter("type");
		return DAORegistry.getSynonymDAO().getSynonymByNameAndTypeNew(request.getParameter("id"), request.getParameter("type")).get(0);
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		Synonym synonym = (Synonym)command;
		
		List<Synonym> synonymFlag = DAORegistry.getSynonymDAO().getSynonymByNameAndTypeNew(synonym.getName(), synonym.getType());
		
		if(synonymFlag != null && !synonymFlag.isEmpty())
			DAORegistry.getSynonymDAO().updateSynonymByNameAndType(synonym);
		else
			DAORegistry.getSynonymDAO().save(synonym);
		
		SectionRowStripper.init();
		if(synonym.getType().equalsIgnoreCase("Theater")){
			return new ModelAndView(new RedirectView("EditorManageSynonyms"));
		}else{
			return new ModelAndView(new RedirectView("EditorManageConcertSynonyms"));
		}
		
	}				
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		return mav;
	}

}