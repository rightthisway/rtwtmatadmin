package com.admitone.tmat.web;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EbayInventoryEvent;
import com.admitone.tmat.data.EbayInventoryGroup;
import com.admitone.tmat.data.EbayInventoryTour;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.ebay.EbayManager;
import com.admitone.tmat.ebay.ListEasyManager;
import com.admitone.tmat.enums.EbayInventoryStatus;
import com.admitone.tmat.utils.TicketUtil;

/**
 * Spring MVC Controller in charge of the ebay pages under the tab ebay 
 *
 */
public class EbayController extends  MultiActionController {
	/*
	private EbayManager ebayManager;
	private ListEasyManager listEasyManager;

	/* *
	 * display ebay inventory page
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * /
	  
	public ModelAndView loadEbayInventoryPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Date startDate = new Date();
		
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		String action = request.getParameter("action");
		String tourIdStr = request.getParameter("tourId");
		String eventIdStr = request.getParameter("eventId");
		String showRemovedGroup = request.getParameter("showRemovedGroup");
		String hideValidGroup = request.getParameter("hideValidGroup");
		
		if (hideValidGroup != null && hideValidGroup.isEmpty()) {
			hideValidGroup = null;
		}
		
		String selUsername = request.getParameter("username");
		
		if (selUsername == null) {
			selUsername = username;
		}
				
		ModelAndView mav = new ModelAndView("page-ebay-inventory");
		mav.addObject("username", selUsername);
		mav.addObject("users", DAORegistry.getUserDAO().getAllUsersByRole("ROLE_EBAY"));
		mav.addObject("invalidAddition", DAORegistry.getPropertyDAO().get("ebay.invalid.addition").getValue());
		Property lastUpdateProperty = DAORegistry.getPropertyDAO().get("ebay.check.lastRefresh");
		if (lastUpdateProperty != null) {
			mav.addObject("lastRefresh", lastUpdateProperty.getValue());			
		}
		
		
		Property lastDownloadDateProperty = DAORegistry.getPropertyDAO().get("ebay.lastDownload.date");
		Property lastDownloadUserProperty = DAORegistry.getPropertyDAO().get("ebay.lastDownload.username");
		
		if (lastDownloadDateProperty != null) {
			mav.addObject("lastDownloadDate", lastDownloadDateProperty.getValue());
			mav.addObject("lastDownloadUsername", lastDownloadUserProperty.getValue());
		}
		
		Date startLoadTourDate = new Date(); 
		Collection<Tour> tours;
		if (selUsername == null || selUsername.equals("ALL")) {
			tours = DAORegistry.getEbayInventoryTourDAO().getAllToursWithEbayInventoryEvents();
		} else {
			tours = DAORegistry.getEbayInventoryTourDAO().getAllToursWithEbayInventoryEventsByUsername(selUsername);
		}
		logger.info("LoadToursDate: " + (new Date().getTime() - startLoadTourDate.getTime()) + " ms");
		
		mav.addObject("tours", tours);
		
		Integer tourId = null;
		if (tourIdStr == null || tourIdStr.isEmpty()) {
			if (!tours.isEmpty()) {
				tourId = tours.iterator().next().getId();
			}
			mav.addObject("tourId", tourId);
		} else if (tourIdStr.equals("ALL")) {
			tourId = null;
			eventIdStr = "ALL";
			mav.addObject("tourId", "ALL");
			mav.addObject("eventId", "ALL");
		} else {
			tourId = Integer.valueOf(tourIdStr);
			if (tours.isEmpty()) {
				tourId = null;
				mav.addObject("tourId", "ALL");
				mav.addObject("eventId", "ALL");
			}
			mav.addObject("tourId", tourId);
		}

		Integer eventId = null;

		Date startToursDate = new Date(); 
		if (tourId != null) {
			Collection<Event> events;
			if (selUsername.equals("ALL")) {
				events = DAORegistry.getEbayInventoryEventDAO().getAllEventsByTourIdWithEbayInventoryEvents(tourId);				
			} else {
				events = DAORegistry.getEbayInventoryEventDAO().getAllEventsByTourIdWithEbayInventoryEventsByUsername(tourId, selUsername);				
			}
			mav.addObject("events", events);

			if (eventIdStr == null || eventIdStr.isEmpty()) {
				if (!events.isEmpty()) {
					eventId = events.iterator().next().getId();
				} else {
					return mav;
				}
				EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
				mav.addObject("eventId", eventId);
				mav.addObject("manager", ebayInventoryEvent.getUsername());
			} else if (eventIdStr.equals("ALL")) {
				eventId = null;
				mav.addObject("eventId", "ALL");
			} else {
				eventId = Integer.valueOf(eventIdStr);
				Event event = DAORegistry.getEventDAO().get(eventId);
				if (!event.getTourId().equals(tourId)) {
					eventId = events.iterator().next().getId();
				}
				EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
				mav.addObject("eventId", eventId);
				mav.addObject("manager", ebayInventoryEvent.getUsername());
			}
		}
		
		logger.info("ToursDate: " + (new Date().getTime() - startToursDate.getTime()) + " ms");
		
		if (action != null) {
			Collection<Integer> groupIds = new ArrayList<Integer>();
			for (Object param: request.getParameterMap().keySet()) {
				if (!param.toString().startsWith("group_")) {
					continue;
				}
				String groupIdParam = param.toString().replace("group_", "");
				Integer groupId = Integer.valueOf(groupIdParam);
				groupIds.add(groupId);
			}
		
			if (action.equals("changeNotes") || action.equals("changeRptFactor")
					|| action.equals("updateTourExposure") || action.equals("updateEventExposure")) {
				String tourNotes = request.getParameter("tourNotes").trim();
				EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
				ebayInventoryEvent.setNotes(request.getParameter("eventNotes").trim());
				ebayInventoryEvent.setRptFactor(Double.valueOf(request.getParameter("rptFactor")));
				ebayInventoryEvent.setExposure(Integer.valueOf(request.getParameter("eventExposure")));
				boolean enableEventZoneMaps = false;
				if (request.getParameter("enableEventZoneMaps") != null) {
					enableEventZoneMaps = true;
				}
				ebayInventoryEvent.setEnableZoneMaps(enableEventZoneMaps);
				DAORegistry.getEbayInventoryEventDAO().update(ebayInventoryEvent);

				EbayInventoryTour ebayInventoryTour = DAORegistry.getEbayInventoryTourDAO().get(tourId);

				boolean enableTourZoneMaps = false;
				if (request.getParameter("enableTourZoneMaps") != null) {
					enableTourZoneMaps = true;
				} 

				if ((ebayInventoryTour.getNotes() == null && !tourNotes.isEmpty())
					|| (ebayInventoryTour.getNotes() != null && (!ebayInventoryTour.getNotes().equalsIgnoreCase(tourNotes))
							|| enableTourZoneMaps != ebayInventoryTour.getEnableZoneMaps())) {
					for (EbayInventoryEvent ebayEvent: DAORegistry.getEbayInventoryEventDAO().getAllEbayInventoryEventsByTourId(tourId)) {
						ebayEvent.setNotes(tourNotes);
						ebayEvent.setEnableZoneMaps(enableTourZoneMaps);
						DAORegistry.getEbayInventoryEventDAO().update(ebayEvent);
					}					
					ebayInventoryTour.setNotes(tourNotes);
					ebayInventoryTour.setEnableZoneMaps(enableTourZoneMaps);
					DAORegistry.getEbayInventoryTourDAO().update(ebayInventoryTour);
				}
				
				Integer tourExposure = Integer.valueOf(request.getParameter("tourExposure"));

				
				if (action.equals("updateTourExposure")) {
						for (EbayInventoryEvent ebayEvent: DAORegistry.getEbayInventoryEventDAO().getAllEbayInventoryEventsByTourId(tourId)) {
							ebayEvent.setExposure(tourExposure);
							DAORegistry.getEbayInventoryEventDAO().update(ebayEvent);
							ebayManager.refreshEvent(ebayEvent.getEventId());
						}					
						ebayInventoryTour.setNotes(tourNotes);
						ebayInventoryTour.setExposure(tourExposure);
						DAORegistry.getEbayInventoryTourDAO().update(ebayInventoryTour);
				} else {
					ebayManager.refreshEvent(eventId);					
				}
				mav.addObject("info", "Event(s) updated");
			} else if (action.equals("remove")) {
				ebayManager.removeGroups(groupIds);					
				mav.addObject("info", "Ticket group(s) removed.");
			} else if (action.equals("add")) {
				if (!ebayManager.addGroups(groupIds)) {
					mav.addObject("error", "Ticket group(s) not added because you were trying to make 2 groups of the same event, category, quantity active.");					
				} else {
					mav.addObject("info", "Ticket group(s) added.");				
				}
			} else if (action.equals("refresh")) {
				String text = ebayManager.refreshAllEvents();
				mav.addObject("info", text.replaceAll("\n", "<br/>")); 
			} else if (action.equals("deleteEvent")) {
				DAORegistry.getEbayInventoryEventDAO().deleteById(eventId);
				ebayManager.updateEbayCrawlersWithAutoFrequency(eventId);
				return new ModelAndView(new RedirectView("EbayInventory?info=Event deleted"));
			} else if (action.equals("changeManager")) {
				String manager = request.getParameter("manager");
				EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
				ebayInventoryEvent.setUsername(manager);
				DAORegistry.getEbayInventoryEventDAO().update(ebayInventoryEvent);
				mav.addObject("manager", ebayInventoryEvent.getUsername());
				return new ModelAndView(new RedirectView("EbayInventory?info=Event manager is now <img src='../images/ico-user.gif' align='absbottom'/>" + manager));
			}
		}
		
		if (eventId != null && !eventId.equals("ALL") && tourId != null) {
			EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId); 
			String eventNotes = ebayInventoryEvent.getNotes();
			if (eventNotes != null) {
				eventNotes = eventNotes.trim();
			}

			EbayInventoryTour ebayInventoryTour = DAORegistry.getEbayInventoryTourDAO().get(tourId); 
			String tourNotes = ebayInventoryTour.getNotes();
			if (tourNotes != null) {
				tourNotes = tourNotes.trim();
			}

			mav.addObject("enableTourZoneMaps", ebayInventoryTour.getEnableZoneMaps());
			mav.addObject("enableEventZoneMaps", ebayInventoryEvent.getEnableZoneMaps());
			mav.addObject("tourExposure", ebayInventoryTour.getExposure());
			mav.addObject("eventExposure", ebayInventoryEvent.getExposure());
			mav.addObject("tourNotes", tourNotes);
			mav.addObject("eventNotes", eventNotes);
			mav.addObject("imageDirUrl", DAORegistry.getPropertyDAO().get("ebay.imageDirUrl").getValue());
			mav.addObject("rptFactor", ebayInventoryEvent.getRptFactor());
		}

		Collection<EbayInventoryGroup> groups = new ArrayList<EbayInventoryGroup>();
		
		Date startInventoryGroup = new Date();
		if (showRemovedGroup != null && showRemovedGroup.equals("on")) {
			if (tourId != null && eventId == null) {
				groups = ebayManager.removeReplacedGroup(DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByTourId(tourId));				
			} else {
				groups = ebayManager.removeReplacedGroup(DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(eventId));
			}
			mav.addObject("showRemovedGroup", "on");
		} else {
			if (tourId != null && eventId == null) {
				groups = ebayManager.removeReplacedGroup(DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByTourId(tourId, new Object[]{EbayInventoryStatus.ACTIVE, EbayInventoryStatus.SUGGESTED, EbayInventoryStatus.REPLACEMENT}));
			} else {
				groups = ebayManager.removeReplacedGroup(DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(eventId, new Object[]{EbayInventoryStatus.ACTIVE, EbayInventoryStatus.SUGGESTED, EbayInventoryStatus.REPLACEMENT}));
			}
		}
		logger.info("InventoryGroups: " + (new Date().getTime() - startInventoryGroup.getTime()) + " ms");
		
		Collection<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();
		if (hideValidGroup != null) {
			mav.addObject("hideValidGroup", "on");
			for (EbayInventoryGroup group: groups) {
				if (group.getStatus().equals(EbayInventoryStatus.DELETED)) {
					continue;
				}
				
				if (!group.isValid()) {
					result.add(group);
				}
			}
		} else {
			for (EbayInventoryGroup group: groups) {
				if (group.getStatus().equals(EbayInventoryStatus.DELETED)) {
					continue;
				}
				result.add(group);
			}
		}

		Collection<EbayInventoryGroup> result2 = new ArrayList<EbayInventoryGroup>();
		if (!selUsername.equals("ALL")) {
			for (EbayInventoryGroup group: result) {
				if (group.getEbayInventoryEvent().getUsername().equals(selUsername)) {
					result2.add(group);
				}
			}
		} else {
			result2 = result;
		}

		mav.addObject("ebayInventoryGroups", ebayManager.sort(result2));
		
		logger.info("loadEbayInventoryPage: " + (new Date().getTime() - startDate.getTime()) + " ms");
				
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		Map<Integer, Tour> tourMap = new HashMap<Integer, Tour>();
		Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();
		
		if (result2 != null) {
			for (EbayInventoryGroup group: result2) {
				if (eventMap.get(group.getEventId()) == null) {
					Event event = DAORegistry.getEventDAO().get(group.getEventId());
					eventMap.put(event.getId(), event);
					
					if (tourMap.get(event.getTourId()) == null) {
						Tour tour = DAORegistry.getTourDAO().get(event.getTourId());
						tourMap.put(tour.getId(), tour);
					}

					if (venueMap.get(event.getVenueId()) == null) {
						Venue venue = DAORegistry.getVenueDAO().get(event.getVenueId());
						venueMap.put(venue.getId(), venue);
					}
				}
			}
		}
		
		mav.addObject("eventMap", eventMap);
		mav.addObject("tourMap", tourMap);
		mav.addObject("venueMap", venueMap);
		
		return mav;
	}
	
	
	/ **
	 * Display page to automatically upload the inventory
	 * to listeasy (WARNING: untested)
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * /
	public ModelAndView loadEbayUploadPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = DAORegistry.getPropertyDAO().get("ebay.listeasy.login").getValue();
		String password = DAORegistry.getPropertyDAO().get("ebay.listeasy.password").getValue();
	    File tempDir = (File) getServletContext().getAttribute( "javax.servlet.context.tempdir" );

	    // create a temporary file in that directory
	    File tempFile = File.createTempFile("tmat-ebay", ".tmp", tempDir );
	    FileWriter writer = new FileWriter(tempFile);
	    ebayManager.downloadCsvEbayFile(writer, false);
	    writer.close();
		listEasyManager.upload(username, password, tempFile);
		return null;
	}
	
	/* *
	 * Generate the CSV output (when someone clicks on the link Download CSV)
	 * containing the ebay inventory 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 *  /
	public ModelAndView loadEbayDownloadPage(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		String type = request.getParameter("type");
		boolean onlyValid = false;
		if (type != null && type.equalsIgnoreCase("VALID")) {
			onlyValid = true;
		}
		response.setContentType("application/force-download");
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy-HH:mm");
	    response.setHeader("Content-Disposition", "attachment; filename=\"export_" + dateFormat.format(new Date()) + ".csv\"");
		ebayManager.downloadCsvEbayFile(response.getWriter(), onlyValid);
		response.getWriter().close();
		
		DateFormat datePropertyFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Property lastDownloadDateProperty = new Property("ebay.lastDownload.date", datePropertyFormat.format(new Date()));
		DAORegistry.getPropertyDAO().saveOrUpdate(lastDownloadDateProperty);
		
		Property lastDownloadUserProperty = new Property("ebay.lastDownload.username", username);
		DAORegistry.getPropertyDAO().saveOrUpdate(lastDownloadUserProperty);
		
		return null;
	}

	public void loadDownloadTicketsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DateFormat format = new SimpleDateFormat("MM-dd-yyyy_HH:mm:ss");
		
		response.setContentType("application/force-download");
	    response.setHeader("Content-Disposition", "attachment; filename=\"export_" + format.format(new Date()) + ".csv\"");
	    
		Writer writer = response.getWriter();
		String eventIdStr = request.getParameter("eventId");
		if (eventIdStr != null) {
			Integer eventId = Integer.valueOf(eventIdStr);
			writer.write("Rem. Qty, Cat, Section, Row, Wholesale Price, Online Price, Title/Notes, Seller");
			ebayManager.exportTickets(writer, eventId);
			writer.close();
		} else {
			Collection<Integer> eventIds = new ArrayList<Integer>();
			for (Object param: request.getParameterMap().keySet()) {
				String eventIdParam = param.toString().replace("event_", "");
				Integer eventId = Integer.valueOf(eventIdParam);
				eventIds.add(eventId);
			}
			writer.write("Rem. Qty, Cat, Section, Row, Wholesale Price, Online Price, Title/Notes, Seller");
			ebayManager.exportTickets(writer, eventIds);
			writer.close();
		}
	}
	
	public ModelAndView loadEbayBrowseEventsPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);

		String selUsername = request.getParameter("manager");
		String tourIdStr = request.getParameter("tourId");
		String hideNoPending = request.getParameter("hideNoPending");
		String action = request.getParameter("action");

		ModelAndView mav = new ModelAndView("page-ebay-browse-events");

		if (selUsername == null) {
			selUsername = username;
		}
		mav.addObject("manager", selUsername);

		if (tourIdStr == null) {
			tourIdStr = "ALL";
		}
		
		if (action != null && action.equalsIgnoreCase("remove")) {
			String info = "";
			for (Object obj: request.getParameterMap().keySet()) {
				String param = (String) obj;
				if (!param.startsWith("event_")) {
					continue;
				}
				
				Integer eventId = Integer.valueOf(param.replaceAll("event_", ""));
				DAORegistry.getEbayInventoryEventDAO().deleteById(eventId);
			}
			info += "The selected ebay events have been removed.";
		}
			
		Collection<EbayInventoryEvent> ebayInventoryEvents;
		
		if (tourIdStr.equals("ALL")) {
			if (selUsername.equals("ALL")) {
				ebayInventoryEvents = DAORegistry.getEbayInventoryEventDAO().getAll();				
			} else {
				ebayInventoryEvents = DAORegistry.getEbayInventoryEventDAO().getAllEbayInventoryEventsByCreator(selUsername);				
			}
			mav.addObject("tourId", "ALL");
		} else {
			Integer tourId = Integer.valueOf(tourIdStr);
			if (selUsername.equals("ALL")) {
				ebayInventoryEvents = DAORegistry.getEbayInventoryEventDAO().getAllEbayInventoryEventsByTourId(tourId);
			} else {
				ebayInventoryEvents = DAORegistry.getEbayInventoryEventDAO().getAllEbayInventoryEventsByTourIdAndUsername(tourId, selUsername);				
			}
			mav.addObject("tourId", tourId);
		}
		
		Collection<EbayInventoryEvent> filteredEbayInventoryEvents;
		
		if (hideNoPending != null && hideNoPending.equalsIgnoreCase("on")) {
			mav.addObject("hideNoPending", "on");
			filteredEbayInventoryEvents = new ArrayList<EbayInventoryEvent>();
			for (EbayInventoryEvent ebayInventoryEvent: ebayInventoryEvents) {
				if (ebayInventoryEvent.getNumPendingGroups() > 0) {
					filteredEbayInventoryEvents.add(ebayInventoryEvent);
				}
			}
		} else {
			filteredEbayInventoryEvents = ebayInventoryEvents;
		}
		
		Collection<Tour> tours;
		if (selUsername.equals("ALL")) {
			tours = DAORegistry.getEbayInventoryTourDAO().getAllToursWithEbayInventoryEvents();
		} else {
			tours = DAORegistry.getEbayInventoryTourDAO().getAllToursWithEbayInventoryEventsByUsername(selUsername);
		}
		
		mav.addObject("ebayInventoryEvents", filteredEbayInventoryEvents);
		mav.addObject("users", DAORegistry.getUserDAO().getAllUsersByRole("ROLE_EBAY"));
		mav.addObject("tours", tours);

		Property lastDownloadDateProperty = DAORegistry.getPropertyDAO().get("ebay.lastDownload.date");
		Property lastDownloadUserProperty = DAORegistry.getPropertyDAO().get("ebay.lastDownload.username");
		
		if (lastDownloadDateProperty != null) {
			mav.addObject("lastDownloadDate", lastDownloadDateProperty.getValue());
			mav.addObject("lastDownloadUsername", lastDownloadUserProperty.getValue());
		}
		Property lastUpdateProperty = DAORegistry.getPropertyDAO().get("ebay.check.lastRefresh");
		if (lastUpdateProperty != null) {
			mav.addObject("lastRefresh", lastUpdateProperty.getValue());			
		}		
		
		if(action != null && action.equalsIgnoreCase("refresh")) {
			String text = ebayManager.refreshAllEvents();
			mav.addObject("info", text.replaceAll("\n", "<br/>")); 			
		}

		mav.addObject("invalidAddition", DAORegistry.getPropertyDAO().get("ebay.invalid.addition").getValue());		
		return mav;
	}

	public ModelAndView loadEbayTestInventoryPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Integer eventId = Integer.valueOf(request.getParameter("eventId"));
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		
		String text1 = "With no net profit filter:<br/>============================<br/>";
		for (EbayInventoryGroup group: ebayManager.computeEbayInventoryFromTickets(eventId, 0D)) {
			text1 += "cat=" + group.getCategory() + " qty=" + group.getQuantity() + " profit=" + group.getEbayInventory1().getNetProfit() + "<br/>";
		}

		String text2 = "With net profit filter:<br/>============================<br/>";
		for (EbayInventoryGroup group: ebayManager.computeEbayInventoryFromTickets(eventId)) {
			text2 += "cat=" + group.getCategory() + " qty=" + group.getQuantity() + " profit=" + group.getEbayInventory1().getNetProfit() + "<br/>";
		}

		System.out.println(text1 + "<br/><br/>" + text2);
		ModelAndView mav = loadEbayInventoryPage(request, response);
		mav.addObject("info", text1 + "<br/><br/>" + text2);
		return mav;
	}

	public ModelAndView loadEbayAddInventoryPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String)request.getSession().getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		String eventIdStr = request.getParameter("eventId");
		if (eventIdStr != null) {
			Integer eventId = Integer.valueOf(eventIdStr);
			ebayManager.saveEbayInventoryFromEventId(eventId, username);
			return  new ModelAndView(new RedirectView("BrowseTours"));
		} else {
			Collection<Integer> eventIds = new ArrayList<Integer>();
			for (Object param: request.getParameterMap().keySet()) {
				if (!param.toString().startsWith("event_")) {
					continue;
				}
				String eventIdParam = param.toString().replace("event_", "");
				Integer eventId = Integer.valueOf(eventIdParam);
				eventIds.add(eventId);
			}
			ebayManager.saveEbayInventoryFromEventIds(eventIds, username);
			return  new ModelAndView(new RedirectView("BrowseTours"));
		}
	}

	public EbayManager getEbayManager() {
		return ebayManager;
	}

	public void setEbayManager(EbayManager ebayManager) {
		this.ebayManager = ebayManager;
	}

	public ListEasyManager getListEasyManager() {
		return listEasyManager;
	}

	public void setListEasyManager(ListEasyManager listEasyManager) {
		this.listEasyManager = listEasyManager;
	}	
	*/
}
