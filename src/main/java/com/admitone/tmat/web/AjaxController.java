package com.admitone.tmat.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.TourCategory;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.utils.TextUtil;

public class AjaxController extends  MultiActionController {

	
	public void getAutoCompleteGrandChildCategory(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
		String strGrandChildCategory=request.getParameter("q");
		
		if (TextUtil.removeExtraWhitespaces(strGrandChildCategory).isEmpty()) {
			return ;
		}
		Collection<GrandChildTourCategory> grandChildCategories= DAORegistry.getGrandChildTourCategoryDAO().getGrandChildCategoriesByName(strGrandChildCategory);
		if(grandChildCategories== null){
			return;
		}
		for(GrandChildTourCategory grandChildTourCategory: grandChildCategories){
			response.getOutputStream().println(grandChildTourCategory.getName()+ ","+ grandChildTourCategory.getChildTourCategory().getName() +","+ grandChildTourCategory.getChildTourCategory().getTourCategory().getName() +"|" + grandChildTourCategory.getId());
		}
	}
	
	public void getAutoCompleteTNPOSEvents(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		String strEventName=request.getParameter("q");
		
		if (TextUtil.removeExtraWhitespaces(strEventName).isEmpty()) {
			return ;
		}
		Collection<AdmitoneEvent> admitoneEvents= DAORegistry.getAdmitoneEventDAO().getEventsByName(strEventName);
		if(admitoneEvents== null){
			return;
		}
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
		for(AdmitoneEvent admitoneEvent: admitoneEvents){
			String date= df.format(admitoneEvent.getEventDate());
			String time = tf.format(admitoneEvent.getEventTime());
			response.getOutputStream().println(admitoneEvent.getEventName() + "," + date + "," + time + "," + admitoneEvent.getVenueName() + "|"+ admitoneEvent.getEventId());
		}
	}
	
	public void getVenueCategoriesByVenue(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		String venueIdStr=request.getParameter("venueId");
		
		if (TextUtil.removeExtraWhitespaces(venueIdStr).isEmpty()) {
			return ;
		}
		Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(Integer.parseInt(venueIdStr));
		if(venueCategories== null){
			return;
		}
		String venueCategoryStr ="";
		for(VenueCategory venueCategory: venueCategories){
			venueCategoryStr += "{\"id\":\"" + venueCategory.getId() + "\",\"name\":\""+ venueCategory.getCategoryGroup() + "\"},";
		}
		if(!venueCategoryStr.isEmpty()){
			venueCategoryStr = "[" + venueCategoryStr.substring(0,venueCategoryStr.length()-1) + "]";
		}
		response.getOutputStream().println(venueCategoryStr);
	}
	public void getAutoCompleteVenue(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");

		Collection<Venue> venues = DAORegistry.getVenueDAO().filterByVenue(param);

		/*if (venues == null) {
			return;
		}
		if (venues != null) {
			for (Venue venue : venues) {
				response.getOutputStream().println(
						venue.getId() + "|"+ venue.getLocation());
			}
		}*/
		String location = "";
		PrintWriter writer =  response.getWriter();
		String res = "";
		if (venues != null) {
			for (Venue venue : venues) {
				location= venue.getLocation().replaceAll("\\�", "");
				res +=  venue.getId()+ "|"+ location+ "\n";
			}
		}
		writer.write(res);

	}
	public void getAutoCompleteArtist(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");

		Collection<Artist> artists = DAORegistry.getArtistDAO().filterByName(param);

		/*if (artists == null) {
			return;
		}
		if (artists != null) {
			for (Artist artist : artists) {
				response.getOutputStream().println(
						artist.getId() + "|" + artist.getName());
			}
		}*/
		String artistName = "";
		PrintWriter writer =  response.getWriter();
		String res = "";
		if (artists != null) {
			for (Artist artist: artists) {
				artistName = artist.getName();//.replaceAll("\\�", "");
				
				res += artist.getId() + "|" + artistName+ "\n";;
			}
		}
		writer.write(res);
	}
	public void getAutoCompleteMergeEvents(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String param = request.getParameter("q");

		Collection<Event> events = DAORegistry.getEventDAO().filterByName(param);

		if (events == null) {
			return;
		}
		if (events != null) {
			for (Event event : events) {
				response.getOutputStream().println(
						event.getId() + "|" + event.getName()+"-"+event.getFormatedDate()+"-"+event.getId());
			}
		}
	}
	public void getTourCategoryByArtist(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		String param = request.getParameter("aId");
		
		Artist artist = DAORegistry.getArtistDAO().get(Integer.parseInt(param));
		
		if (artist == null) {
			return;
		}
		if (artist != null) {
			TourCategory tourCategory = artist.getGrandChildTourCategory().getChildTourCategory().getTourCategory();
				response.getOutputStream().println(
						tourCategory.getId() + "|"+ tourCategory.getName());
		}
		
	}
}
