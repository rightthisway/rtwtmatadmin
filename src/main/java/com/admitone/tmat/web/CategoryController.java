package com.admitone.tmat.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.CsvUploadRemoveAudit;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.util.VenueMapUtil;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.TextUtil;

/**
 * Spring MVC Controller in charge of the Category pages 
 *
 */
public class CategoryController extends MultiActionController implements InitializingBean {
	private static final Pattern numberPattern = Pattern.compile("^\\d+$");
//	private static final Pattern datePeriodPattern = Pattern.compile("([\\d]+/[\\d]+/[\\d]+)[\\s]*-[\\s]*([\\d]+/[\\d]+/[\\d]+)");
//	private TicketListingCrawlDispatcher ticketListingCrawlDispatcher;
	private Map<String, String> validSection = new HashMap<String, String>();
	/**
	 * Homepage of Categories
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public ModelAndView loadEditorCategoriesPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String artistIdStr = request.getParameter("artistId");
		String filter = request.getParameter("filter");
		ModelAndView mav = new ModelAndView("page-editor-categories");
		
		Collection<Tour> tours;
		
		if (artistIdStr != null && !artistIdStr.trim().isEmpty()) {
			Integer artistId = Integer.parseInt(artistIdStr);
			
			if (filter != null && !filter.trim().isEmpty()) {
				filter = filter.trim();
				tours = DAORegistry.getTourDAO().filterToursByArtistByName(artistId, filter);
			} else {
				tours = DAORegistry.getTourDAO().getAllToursByArtist(artistId);
			}
			
		} else {
			if (filter != null && !filter.trim().isEmpty()) {
				filter = filter.trim();
				tours = DAORegistry.getTourDAO().filterByName(filter);
			} else {
				tours = DAORegistry.getTourDAO().getAll();
			}
		}
		
		mav.addObject("tours", tours);
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		return mav;
	}*/

	/**
	 * Tour page for Categories
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditorEditCategoriesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer venueId = null;
		String isVenue = null;
		String venueIdStr = request.getParameter("venueIdStr");
		System.out.println("venueIdStr :"+venueIdStr);
		String catScheme = request.getParameter("catScheme");
		String info = request.getParameter("info");
		String artistId = request.getParameter("artistId");
		String venId = request.getParameter("venueId");
		String remove = request.getParameter("remove");
		ModelAndView mav = new ModelAndView("page-editor-edit-categories");
		
		if(venId != null && !venId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venId;
		}else if(artistId != null && !artistId.isEmpty()){
			isVenue = "EditorManageTCAP?artistId="+artistId;
		}
		String zoneCatGroup="";
		Collection<Event> eventList = null;
		if (venueIdStr != null) {
			try{
				venueId = Integer.valueOf(venueIdStr);
			}
			catch(Exception e){e.printStackTrace();}
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
			if(remove!=null && remove.equalsIgnoreCase("true")){
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venueId, catScheme);
				String building=venueCategory.getVenue().getBuilding();
				System.out.println("building : "+building);
				String category=venueCategory.getCategoryGroup();
				System.out.println("category group : "+category);
				String username = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal()).getUsername();
				//System.out.println("username : "+username);
				try{
				DAORegistry.getVenueCategoryDAO().delete(venueCategory);
				CsvUploadRemoveAudit csv=new CsvUploadRemoveAudit();
				csv.setUsername(username);
				csv.setAction("removed");
				Date currentdate=new Date();
				int intialLineCount=DAORegistry.getCategoryDAO().countByVenueCategoryID(venueCategory.getId());
				csv.setInitialLineCount(intialLineCount);
				csv.setCreatedDate(currentdate);
				csv.setBuilding(building);
				csv.setZonesgroup(category);
				DAORegistry.getCsvUploadRemoveAuditDAO().save(csv);
				}catch(Exception e){e.printStackTrace();}
				info="Venue Category removed successfully.";
				/*CsvUploadRemoveAudit csv=new CsvUploadRemoveAudit();
				csv.setUsername(username);
				csv.setAction("removed");
				DAORegistry.getCsvUploadRemoveAuditDAO().save(csv);*/
			}
			mav.addObject("venueId", venueId);
			try{
				eventList = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
			}catch(Exception e){e.printStackTrace();}
		}
		
		List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venueId);		
		mav.addObject("catGroupName", catScheme);
		mav.addObject("info", info);
		mav.addObject("zoneCatGroup",zoneCatGroup);
		mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
		mav.addObject("events", eventList);
		mav.addObject("catGroups", venueCategories);
		mav.addObject("fromTcap",isVenue);
		return mav;	
	}

	
	
	/**
	 * Event page for categories (category mappings)
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditorEditEventCategoriesPage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer eventId = null;
		String eventIdStr = request.getParameter("eventId");
		if (eventIdStr != null) {
			eventId = Integer.valueOf(eventIdStr);
		}

		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<String> catGroups = Categorizer.getCategoryGroupsByVenueId(event.getVenueId());
//		Collection<String> catGroups = DAORegistry.getCategoryDAO().getAllCategoryGroupsByTour(event.getTourId());
//		
		String catScheme = request.getParameter("catScheme");
//		if(catScheme == null || catScheme.isEmpty()){
//			if (catGroups.iterator().hasNext()) {
//				catScheme = catGroups.iterator().next();
//			} else {
//				catScheme = "";
//			}
//		}
		if(catScheme == null || catScheme.isEmpty()){
			if(null != event.getVenueCategory()){
				catScheme = event.getVenueCategory().getCategoryGroup();
			}			
		}
		ModelAndView mav = new ModelAndView("page-editor-edit-event-categories");
		mav.addObject("event", event);
		mav.addObject("eventId", eventIdStr);
		mav.addObject("catGroupName", catScheme);
		mav.addObject("categories", DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme));
		mav.addObject("catGroups", catGroups);
		// mav.addObject("categoryMappings",
		// DAORegistry.getCategoryMappingDAO().getAllCategoryMappings(eventId));
		return mav;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditorManageSynonyms(
			HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		String action = request.getParameter("action");
		ModelAndView mav = new ModelAndView("page-editor-manage-synonyms");
		
		if (action != null && action.equalsIgnoreCase("remove")) {
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			DAORegistry.getSynonymDAO().deleteSynonymByNameAndType(id, type);
		}else if(action != null && action.equalsIgnoreCase("search")){
			String filterBy = request.getParameter("filterBy");
			mav.addObject("synonyms",DAORegistry.getSynonymDAO().getSynonymByNameAndType(filterBy, "Theater"));
			return mav;
		}else if(action != null && action.equalsIgnoreCase("copy")){
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			List<Synonym> copySynonym = DAORegistry.getSynonymDAO().getSynonymByName(id);
			if(copySynonym != null && !copySynonym.isEmpty() && copySynonym.size() < 2){
				Synonym copySynonyms = copySynonym.get(0);
				copySynonyms.setType("Concert");
				DAORegistry.getSynonymDAO().save(copySynonyms);
			}
			
			
		}
		
		
		mav.addObject("synonyms", DAORegistry.getSynonymDAO().getSynonymByType("Theater"));
		return mav;
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditorManageConcertSynonyms(
			HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		
		String action = request.getParameter("action");
		ModelAndView mav = new ModelAndView("page-editor-manage-concert-synonyms");
		
		if (action != null && action.equalsIgnoreCase("remove")) {
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			DAORegistry.getSynonymDAO().deleteSynonymByNameAndType(id, type);
			
		}else if(action != null && action.equalsIgnoreCase("search")){
			String filterBy = request.getParameter("filterBy");
			mav.addObject("concertSynonyms",DAORegistry.getSynonymDAO().getSynonymByNameAndType(filterBy, "Concert"));
			return mav;
		}else if(action != null && action.equalsIgnoreCase("copy")){
			String id = request.getParameter("id");
			String type = request.getParameter("type");
			List<Synonym> copySynonym = DAORegistry.getSynonymDAO().getSynonymByName(id);
			if(copySynonym != null && !copySynonym.isEmpty() && copySynonym.size() < 2){
				Synonym copySynonyms = copySynonym.get(0);
				copySynonyms.setType("Theater");
				DAORegistry.getSynonymDAO().save(copySynonyms);
			}			
			
		}
		
		mav.addObject("concertSynonyms",DAORegistry.getSynonymDAO().getSynonymByType("Concert"));
		return mav;
	}

	/**
	 * Method invoked when someone uploads the csv file containing all the category mappings for a tour 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public ModelAndView loadEditorUploadCategoryMappingCsvFilePage(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String tourIdStr = request.getParameter("tourId");
		Integer eventId = null;
		Integer tourId = null;
		ModelAndView mav = null;
		if(tourIdStr == null || tourIdStr.isEmpty()){
			tourId = null;
			eventId = Integer.parseInt(request.getParameter("eventId"));
			mav = new ModelAndView(new RedirectView(
					"EditorEditEventCategories?eventId=" + eventId));
		} else {
			tourId = Integer.parseInt(tourIdStr);
			mav = new ModelAndView(new RedirectView(
					"EditorEditCategories?tourId=" + tourId));
		}
		Collection<Event> events = new ArrayList<Event>();

		// if we don't upload a file, leave it
		if (!(request instanceof MultipartHttpServletRequest)) {
			return mav;
		}
		
		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;

//		boolean deletedOldCats = false;
		HashMap<String, Boolean> deletedCatsMap = new HashMap<String, Boolean>();
		if(tourIdStr == null || tourIdStr.isEmpty()) {
			for(String groupName : Categorizer.getCategoryGroupsByEvent(eventId)){
				deletedCatsMap.put(eventId + " " + groupName, new Boolean(false));
			}
		} else {
			for(Event event : DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE)){
				for(String groupName : Categorizer.getCategoryGroupsByEvent(event.getId())){
					deletedCatsMap.put(event.getId() + " " + groupName, new Boolean(false));
				}
			}
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				multi.getFile("file").getInputStream()));
		String line;
		int lineCount = 0;
		HashMap<Integer,Collection<CategoryMapping>> mappings = new HashMap<Integer,Collection<CategoryMapping>>();

		// parse file
		Map<String, Venue> venueMap = new HashMap<String, Venue>();
		Map<String, TMATDependentZonesCategory> zonesMap = new HashMap<String, TMATDependentZonesCategory>();
		List<TMATDependentZonesCategory> list = DAORegistry.getTmatDependentZonesCategoryDAO().getTmatDependentsByTourId(tourId);
		if(list!=null && !list.isEmpty()){
			for(TMATDependentZonesCategory cat:list){
				zonesMap.put(cat.getTourId()+"-"+cat.getVenueId(), cat);
			}
		}
		Map<String, Integer> dupLineMap = new HashMap<String, Integer>();
		Map<String, List<String>> lineMap = new HashMap<String, List<String>>();
		String commonVenue="";
		String commonCategory="";
//		Set<Category> catList = new ListOrderedSet();
		while ((line = reader.readLine()) != null) {
			try {
				lineCount++;
				if (line.trim().isEmpty()) {
					continue;
				}
				
				String[] lineTokens = line.split(",");
				
				if (lineTokens.length != 8) {
					mav.addObject("error", "Error Line " + lineCount + ": "
							+ line + ": Invalid format.");
					return mav;						
				}
				String key="";
				for(String token:lineTokens){
					key+=token.toUpperCase() + "-" ;
				}
				if(dupLineMap.containsKey(key)){
					Integer lineNo=dupLineMap.get(key);
					mav.addObject("error", "Line " + lineNo + " and "
							+ lineCount + " is duplicate.");
					return mav;	
				}
				dupLineMap.put(key, lineCount);
				
				String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
				String section = TextUtil.trimQuotes(lineTokens[1].toUpperCase());
				String row = TextUtil.trimQuotes(lineTokens[2].toUpperCase());
				String seat = TextUtil.trimQuotes(lineTokens[3].toUpperCase());
				String categorySymbol = TextUtil.trimQuotes(lineTokens[4].toUpperCase());
				String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
				String catSeatQty = TextUtil.trimQuotes(lineTokens[6].toUpperCase());
				String equivalentCats = TextUtil.trimQuotes(lineTokens[7].toUpperCase());

				if(lineCount==1){
					commonVenue=venueStr;
					commonCategory=catScheme;
				}else if(!commonCategory.equals(catScheme)){
					mav.addObject("error", "Line: " + lineCount + ", Everything in column F has to be same.");
					return mav;
				}else if(!commonVenue.equals(venueStr)){
					mav.addObject("error", "Line: " + lineCount + ", Everything in column A has to be same.");
					return mav;
				}else if(categorySymbol == null || categorySymbol.isEmpty()){
					mav.addObject("error", "Line: " + lineCount + ", Category Symbol can not be blank.");
					return mav;
				}
				String startSection = "";
				String endSection = "";
				String startRow = "";
				String endRow = "";
				String startSeat = "";
				String endSeat = "";

				if (section.indexOf('-') > 0) {
					String[] tokens = section.split("-");
					if(tokens.length==2){
						startSection = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSection = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						return mav;
					}
				} else {
					startSection = section;
					endSection = section;
				}

				if (row.indexOf('-') > 0) {
					String[] tokens = row.split("-");
					if(tokens.length==2){
						startRow = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endRow = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						return mav;
					}
				} else {
					startRow = row.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					endRow = row.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
				}

				if (seat.indexOf('-') > 0) {
					String[] tokens = seat.split("-");
					if(tokens.length==2){
						startSeat = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSeat = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Seat Range is not valid.");
						return mav;
					}
					
				} else {
					startSeat = seat;
					endSeat = seat;
				}
				boolean isSectionInt=false;
				Integer intStartSection =0;
				Integer intEndSection =0; 
				String prefix="";
				try{
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					if(intEndSection<intStartSection){
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						return mav;
					}
					isSectionInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startSection) || !validSection.containsKey(endSection)){
						if(!startSection.equals(endSection)){
							Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
							Matcher startSectionMatcher = pattern.matcher(startSection);
							Matcher endSectionMatcher = pattern.matcher(endSection);
							if(!startSectionMatcher.find() || !endSectionMatcher.find() || !startSectionMatcher.group(1).equals(endSectionMatcher.group(1))){
								mav.addObject(
										"error",
										"Invalid line "
										+ lineCount
										+ ": "
										+ line
										+ ": Section Range is not valid.");
								return mav;
							}else{
								prefix=startSectionMatcher.group(1);
								intStartSection= Integer.parseInt(startSectionMatcher.group(2));
								intEndSection= Integer.parseInt(endSectionMatcher.group(2));
								if(intEndSection<intStartSection){
									mav.addObject(
											"error",
											"Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Section Range is not valid.");
									return mav;
								}
								isSectionInt=true;
							}
							
						}
					}else if(!validRange(startSection,endSection)){
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						return mav;
					}
					
				}
				*//*****************//*
				
//				boolean isRowInt=false;
				Integer intStartRow =0;
				Integer intEndRow =0; 
//				String rowPrefix="";
				try{
					intStartRow= Integer.parseInt(startRow);
					intEndRow= Integer.parseInt(endRow);
					if(intEndRow<intStartRow){
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						return mav;
					}
//					isRowInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startRow) || !validSection.containsKey(endRow)){
						if(!startRow.equals(endRow)){
							Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
							Matcher startRowMatcher = pattern.matcher(startRow);
							Matcher endRowMatcher = pattern.matcher(endRow);
							if(!startRowMatcher.find() || !endRowMatcher.find() || !startRowMatcher.group(1).equals(endRowMatcher.group(1))){
								mav.addObject(
										"error",
										"Invalid line "
										+ lineCount
										+ ": "
										+ line
										+ ": Row Range is not valid.");
								return mav;
							}else{
//								rowPrefix=startRowMatcher.group(1);
								intStartRow= Integer.parseInt(startRowMatcher.group(2));
								intEndRow= Integer.parseInt(endRowMatcher.group(2));
								if(intEndRow<intStartRow){
									mav.addObject(
											"error",
											"Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Row Range is not valid.");
									return mav;
								}
//								isRowInt=true;
							}
							
						}
					}else if(!validRange(startRow,endRow)){
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						return mav;
					}
				}
				
				*//********************//*
				// if start section and end section cannot be compared
				// (e.g., a number and string with letters) => error
				if (endSection.length() > 0
						&& !startSection.equals("*")
						&& numberPattern.matcher(startSection).find() != numberPattern
								.matcher(endSection).find()) {
					mav.addObject(
							"error",
							"Invalid line "
							+ lineCount
							+ ": "
							+ line
							+ ": Cannot compare number and string sections");
					return mav;
				}

				if (endRow.length() > 0
						&& !startRow.equals("*")
						&& numberPattern.matcher(startRow).find() != numberPattern
								.matcher(endRow).find()) {
					mav.addObject("error", "Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string rows");
					return mav;
				}

				if (endSeat.length() > 0
						&& !startSeat.equals("*")
						&& numberPattern.matcher(startSeat).find() != numberPattern
								.matcher(endSeat).find()) {
					mav.addObject("error", "Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string seats");
					return mav;
				}
				
				Venue venue =venueMap.get(venueStr);
				if(venue==null){
					venue= DAORegistry.getVenueDAO().getVenue(venueStr);
					if (venue == null){
						mav.addObject("error", "Invalid line " + lineCount
								+ ": " + line
								+ ": Cannot find venue:" + venueStr);
						return mav;
					}
					venueMap.put(venueStr, venue);
				}
				
//			 	events = null;
			 	
			 	if(tourId != null){
//			 		events = new ArrayList<Event>();
			 		if(events.isEmpty()){
			 			events.addAll(DAORegistry.getEventDAO().getAllEventsByTourVenue(tourId, venue.getId()));
			 		}			 			
			 	} else if (eventId != null) {
			 		events = new ArrayList<Event>();
			 		events.add(DAORegistry.getEventDAO().get(eventId));
			 	}	
				
			 	if(events == null || events.isEmpty()){
					mav.addObject("error", "Invalid line " + lineCount
							+ ": " + line
							+ ": Could not find events for venue in this tour/event!");
					return mav;
			 	}

			 	
			 	//System.out.println("Using tourId:" + tourId + " venue:" + venueStr + " eventId" + eventId);
			 	//System.out.println("EVENTLIST IS: " + events);
//			 	List<String> rowList = null;
//				String sec ="";
				Map<String,Category> categoryMap = new HashMap<String, Category>();
				Map<Integer,Boolean> tourMap = new HashMap<Integer,Boolean>();
				Category category = null;
			 	for (Event event: events) {
			 		if(deletedCatsMap.get(event.getId() + " " + catScheme) != null) {
						if(!deletedCatsMap.get(event.getId() + " " + catScheme).booleanValue()){
							deletedCatsMap.put(event.getId() + " " + catScheme, new Boolean(true));
							
//							for (CategoryMapping catMap : DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsFromEventAndScheme(event.getId(), catScheme)) {
//								if(catMap != null) {
//									DAORegistry.getCategoryMappingDAO().delete(catMap);
//								}
//							}
							DAORegistry.getCategoryMappingDAO().deleteAllByEvent(event.getId(), catScheme);
//							for (Category category : DAORegistry.getCategoryDAO().getAllEventCategories(event.getId(), catScheme)) {
//								if(category != null) {
//									DAORegistry.getCategoryDAO().delete(category);
//								}
//							}
						}
			 		}
			 		// System.out.println("event " + event.getName() + " " + event.getLocalDate() + " " + event.getLocalTime());
			 		
					boolean newCat = false;
					if(tourMap.get(event.getTourId())== null || !tourMap.get(event.getTourId())){
						tourMap.put(event.getTourId(), true);
						String categoryMapKey = event.getTourId() + "-" + categorySymbol + "-" + catScheme;
						category = categoryMap.get(categoryMapKey);
						if(category==null){
							category = DAORegistry.getCategoryDAO().getCategoryBySymbol(event.getTourId(), categorySymbol, catScheme);
							categoryMap.put(categoryMapKey, category);
						}
						if (category == null) {
							category = new Category(event.getTourId(), categorySymbol,
									"Category " + categorySymbol, equivalentCats, catScheme);
							if(!catSeatQty.equals("")){
								category.setSeatQuantity(Integer.parseInt(catSeatQty));
							}
							
//							catList.add(category);
							DAORegistry.getCategoryDAO().save(category);
							newCat = true;
						}
					}
					
//						
//						if (DAORegistry.getCategoryMappingDAO().getCategoryMapping(
//								category.getId(), event.getId(), startSection,
//								endSection, startRow, endRow) != null) {
//							continue;
//						}
//					
					if(isSectionInt){
						for(int start=intStartSection;start<=intEndSection;start++){
							String sec = prefix + String.valueOf(start);
							String lineMapKey = event.getId() + "-" + sec;
							List<String> rowList  =  lineMap.get(lineMapKey);
							if(rowList==null){
								rowList= new ArrayList<String>();
							}
							if(checkConflict(rowList, startRow, endRow,startSeat,endSeat)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Sections conflicts with other line in csv.");
								return mav;
							}
							rowList.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat);
							lineMap.put(lineMapKey, rowList);
							CategoryMapping mapping = new CategoryMapping(category.getId(), 
									event.getId(), sec, sec,startRow, endRow, startSeat, endSeat);
							if(mappings.get(event.getId()) == null){
								mappings.put(event.getId(), new ArrayList<CategoryMapping>());
							}
							mappings.get(event.getId()).add(mapping);
						}
						
					}else{
						if(validSection.containsKey(startSection)){
							String sec = startSection;
							while(validSection.containsKey(sec)){
								String lineMapKey = event.getId() + "-" + sec;
								List<String> rowList  =  lineMap.get(lineMapKey);
								if(rowList==null){
									rowList= new ArrayList<String>();
								}
								if(checkConflict(rowList, startRow, endRow,startSeat,endSeat)){
									mav.addObject("error", "Invalid line " + lineCount
											+ ": " + line
											+ ": Row is not valid or conflicts with other line in csv.");
									return mav;
								}
								rowList.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat);
								lineMap.put(lineMapKey, rowList);
								CategoryMapping mapping = new CategoryMapping(category
										.getId(), event.getId(), sec, sec,
										startRow, endRow, startSeat, endSeat);
								if(mappings.get(event.getId()) == null){
									mappings.put(event.getId(), new ArrayList<CategoryMapping>());
								}
								mappings.get(event.getId()).add(mapping);
								if(sec.equals(endSection)){
									sec="--";
								}else{
									sec = validSection.get(sec);
								}
							}
						}else{
							String sec = startSection;
							String lineMapKey = event.getId() + "-" + sec;
							List<String> rowList  =  lineMap.get(lineMapKey);
							if(rowList==null){
								rowList= new ArrayList<String>();
							}
							
							if(checkConflict(rowList, startRow, endRow,startSeat,endSeat)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Row is not valid or conflicts with other line in csv.");
								return mav;
							}
							rowList.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat);
							lineMap.put(lineMapKey, rowList);
							
							CategoryMapping mapping = new CategoryMapping(category
									.getId(), event.getId(), startSection, endSection,
									startRow, endRow, startSeat, endSeat);
							if(mappings.get(event.getId()) == null){
								mappings.put(event.getId(), new ArrayList<CategoryMapping>());
							}
							mappings.get(event.getId()).add(mapping);
						}
						
					}
					//System.out.println("Created Mapping for event: " + event + " cat: " + categorySymbol);
					
					if(newCat) {
						//add cat aliases
						CategorySynonym catSyn = new CategorySynonym();

						catSyn.setCatId(category.getId());
						catSyn.setStartSynonym(startSection);
						if(endSection != null && !endSection.equals(startSection)){
							catSyn.setEndSynonym(endSection);
						}
			 			DAORegistry.getCategorySynonymDAO().save(catSyn);
					}
					TMATDependentZonesCategory dependentZonesCategory= zonesMap.get(event.getTourId() + "-" + event.getVenueId());
					if(dependentZonesCategory == null){
						dependentZonesCategory = new TMATDependentZonesCategory();
						dependentZonesCategory.setAdd(true);
						dependentZonesCategory.setDependent("zones");
						dependentZonesCategory.setTourId(event.getTourId());
						dependentZonesCategory.setVenueId(event.getVenueId());
						zonesMap.put(event.getTourId() + "-" + event.getVenueId(), dependentZonesCategory);
						DAORegistry.getTmatDependentZonesCategoryDAO().save(dependentZonesCategory);
					}
			 	}
//			 	rowList.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat);
//				lineMap.put(sec, rowList);
				
//			 	Collection<TMATDependentZonesCategory> list = zonesMap.values();
//			 	DAORegistry.getTmatDependentZonesCategoryDAO().saveAll(list);
			} catch (Exception e) {
				e.printStackTrace();
				mav.addObject("error", "Invalid line: " + lineCount + ": "
						+ line);
				return mav;
			}
		}
		reader.close();
//		if(catList!= null && !catList.isEmpty()){
//			DAORegistry.getCategoryDAO().saveAll(catList);
//		}
		for (Category category : DAORegistry.getCategoryDAO().getEmptyCategoryByTourId(tourId)){
			if(category != null) {
				DAORegistry.getCategoryDAO().delete(category);
			}
		}
		
		if(mappings.size() > 0){
			ArrayList<Integer> eventIds = new ArrayList<Integer>();
			eventIds.addAll(mappings.keySet());
			for(Integer mapEventId : eventIds){
				DAORegistry.getCategoryMappingDAO().saveOrUpdateAll(mappings.get(mapEventId));
			}
			if(tourId != null) {
				Categorizer.update(tourId);
			} else {
				Categorizer.update(events.iterator().next().getTourId());
			}
		}
		
		
		return mav;
	}
*/
	
	
	
	public ModelAndView loadManageCategoryMapping(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView mav = new ModelAndView("page-admin-manage-category");
		String venueIdStr = request.getParameter("venueId");
		Integer venueId= 0;
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
			mav.addObject("venueId", venueId);
			Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venueId);
			mav.addObject("venueCategories", venueCategories);
		}
		Venue venue = DAORegistry.getVenueDAO().get(venueId);
		String categoryGroup = request.getParameter("categoryGroup");
		if(categoryGroup==null){
			categoryGroup = "";
		}
		mav.addObject("categoryGroup", categoryGroup);
		//Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
		//mav.addObject("venues", venues);
		Collection<Category> categories = null;
		if(venueId!= 0 && !categoryGroup.isEmpty()){
			categories =  DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(venueId, categoryGroup);
		}
//		Gson gson  =new Gson();
//		gson.toJson(src)
		String line="";
		if(categories!=null && !categories.isEmpty()){
			System.out.println("Category Size : "+categories.size());
			for(Category category:categories){
				for (CategoryMapping mapping : DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId())) {
					String sectionStr;
					if(!line.isEmpty()){
						line= line +",";
					}
					if (mapping.getStartSection().equals(mapping.getEndSection())) {
						sectionStr = mapping.getStartSection().trim().replaceAll("-", "%MINUS%");
					} else {
						sectionStr = mapping.getStartSection().trim().replaceAll("-", "%MINUS%") + "-"
								+ mapping.getEndSection().trim().replaceAll("-", "%MINUS%");
					}

					String rowStr;
					if (mapping.getStartRow().equals(mapping.getEndRow())) {
						rowStr = mapping.getStartRow().trim().replaceAll("-", "%MINUS%");
					} else {
						rowStr = mapping.getStartRow().trim().replaceAll("-", "%MINUS%") + "-" + mapping.getEndRow().trim().replaceAll("-", "%MINUS%");
					}
					
					String seatStr;
					if (mapping.getStartSeat().equals(mapping.getEndSeat())) {
						seatStr = mapping.getStartSeat();
					} else {
						seatStr = mapping.getStartSeat() + "-" + mapping.getEndSeat();
					}

					String categoryStr = category.getSymbol();
					String equalCats = category.getEqualCats();
					if (equalCats == null) {
						equalCats = "";
					}
					line += "[\"" + venue.getBuilding() +  "\", \"" + sectionStr
							+ "\", \"" + rowStr + "\", \"" + seatStr +"\", \""+ categoryStr + "\", \"" + category.getGroupName() + "\",\"" + (mapping.getLarrySection()==null?"":mapping.getLarrySection()) + "\", \"" + equalCats + "\", \"" + (category.getNormalizedZone()==null?"":category.getNormalizedZone()) + "\", \"" + (mapping.getRowRange()==null?"":mapping.getRowRange()) + "\", \"" + (mapping.getLastRow()==null?"":mapping.getLastRow()) + "\", \"" + (mapping.getAlternateRow()==null?"":mapping.getAlternateRow())  +"\"]";
//					response.getOutputStream().write(line.getBytes());
				}	
			}	
		}else{
			System.out.println(" no categories : "+new Date());
			line += "[\"\", \"\", \"\", \"\", \"\", \"\",\"\", \"\", \"\", \"\", \"\", \"\"]";
		}
		
		line = "[" + line + "]";
		mav.addObject("categories", line);
		return mav;
		
	}
	
	public void saveManageCategoryMapping(
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String data = request.getParameter("data");
//		ModelAndView mav = new ModelAndView(new RedirectView("EditorLoadCategoryMapping"));
		String venueIdStr = request.getParameter("venueId");
		Integer venueId= 0;
		if(venueIdStr!=null && !venueIdStr.isEmpty()){
			venueId = Integer.parseInt(venueIdStr);
		}
		Venue venue = DAORegistry.getVenueDAO().get(venueId);
		String categoryGroup = request.getParameter("categoryGroup");
		if(categoryGroup==null){
			categoryGroup = "";
		}
		
		
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(venueId, categoryGroup);
		String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
		// if we don't upload a file, leave it
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(data);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String url = "";
		
		String line="";
		int lineCount = 0;
		int intialLineCount=0;
		
		Map<String, Integer> dupLineMap = new HashMap<String, Integer>();
		Map<String,List<String>> sectionMap = new HashMap<String, List<String>>();
		
		Map<String, Category> categoryKeyMap = new HashMap<String, Category>();
		
		
		Map<String, String> larrySectionMap = new HashMap<String, String>();
		String commonVenue="";
		String commonCategory="";
		String info ="";
		VenueCategory venueCategory =null;
		String[] lineTokens=null;
		Map<String,Category> catMap = new HashMap<String, Category>();
		int length = jsonArray.length();
		for(lineCount=1;lineCount<=length ;lineCount++ ) {
			try {
//				lineCount++;
				line = jsonArray.getString(lineCount-1);
				if (line.trim().isEmpty()) {
					continue;
				}
				
				lineTokens = line.split(",");
				
				
				if (lineTokens.length != 11 && lineTokens.length != 12) {
//					mav.addObject("error", "Error Line " + lineCount + ": " + line + ": Invalid format.");
					info = "Error: Line " + lineCount + ": " + line + ": Invalid format.";
					String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase().replaceAll("\\[", ""));
					String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line " + lineCount + ": "+ line + ": Invalid format.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}
				String key="";
				for(String token:lineTokens){
					token = token.toUpperCase().replaceAll("\\[", "").replaceAll("\\]", "");
					if(null != token && !token.isEmpty() && !token.equalsIgnoreCase("null")){
						key+= token + "-" ;
					}
				}
				if(key.isEmpty()){
					continue;
				}
				if(dupLineMap.containsKey(key)){
					Integer lineNo=dupLineMap.get(key);
//					mav.addObject("error", "Line " + lineNo + " and " + lineCount + " is duplicate.");
					info = "Error:Line " + lineNo + " and " + lineCount + " is duplicate.";
					String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase().replaceAll("\\[", ""));
					String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error : Line " + lineNo + " and "
							+ lineCount + " is duplicate.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}
				dupLineMap.put(key, lineCount);
				
				String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase().replaceAll("\\[", ""));
				String section = TextUtil.trimQuotes(lineTokens[1].toUpperCase());
				String row = TextUtil.trimQuotes(lineTokens[2].toUpperCase());
				String seat = TextUtil.trimQuotes(lineTokens[3].toUpperCase());
				String categorySymbol = TextUtil.trimQuotes(lineTokens[4].toUpperCase());
				String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
				/*String catSeatQty = TextUtil.trimQuotes(lineTokens[6].toUpperCase());*/
				String larrySection = "";
//				if(lineTokens[6] != null && !lineTokens[6].isEmpty())
					larrySection = TextUtil.trimQuotes(lineTokens[6].toUpperCase());
				
				String equivalentCats = TextUtil.trimQuotes(lineTokens[7].toUpperCase());
				String normalizedZone = TextUtil.trimQuotes(lineTokens[8].toUpperCase());
				String rowRange = TextUtil.trimQuotes(lineTokens[9].toUpperCase());
				String lastRow = TextUtil.trimQuotes(lineTokens[10].toUpperCase());
				String alternateRow = null;
				
//				if(lineTokens.length >11){
					alternateRow = TextUtil.trimQuotes(lineTokens[11].toUpperCase().replaceAll("\\]", ""));
//				}
				
				if(lineCount==1){
					commonVenue=venueStr;
					commonCategory=catScheme;
					
					venue= DAORegistry.getVenueDAO().getVenue(venueStr);
					if (venue == null){
//						mav.addObject("error", "Invalid line " + lineCount
//								+ ": " + line
//								+ ": Cannot find venue:" + venueStr);
						info = "Error:Invalid line " + lineCount + ": " + line + ": Cannot find venue:" + venueStr;
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction("Invalid line " + lineCount
								+ ": " + line
								+ ": Cannot find venue:" + venueStr);
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
					url = "venueId=" + venue.getId() + "&categoryGroup=" + catScheme;
//					mav.addObject("venueIdStr", venue.getId());
//					mav.addObject("catScheme",catScheme);
//					url = url + "?venueIdStr=" + venue.getId() + "catScheme=" + catScheme; 
					venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venue.getId(), commonCategory);
					if(venueCategory==null){
						venueCategory = new VenueCategory();
						venueCategory.setCategoryGroup(catScheme);
						venueCategory.setVenue(venue);
						venueCategory.setLastUpdatedDate(new Date());
						venueCategory.setLastUpdatedBy(username);
						DAORegistry.getVenueCategoryDAO().save(venueCategory);
					}else{
						Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
						intialLineCount=DAORegistry.getCategoryDAO().countByVenueCategoryID(venueCategory.getId());
						System.out.println("intialLineCount : "+intialLineCount);

						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						for(Category cat:catList){
							if(catMap.containsKey(cat.getSymbol())){
								Category oldCat =null;
								if(cat.getNormalizedZone().equals(normalizedZone)){
									oldCat = catMap.remove(cat.getSymbol());
								}else{
									oldCat = cat;
								}
								DAORegistry.getCategoryDAO().delete(oldCat);
							}
							catMap.put(cat.getSymbol(), cat);
						}
						venueCategory.setLastUpdatedDate(new Date());
						venueCategory.setLastUpdatedBy(username);
						DAORegistry.getVenueCategoryDAO().update(venueCategory);
					}
					
				}else if(!commonCategory.equals(catScheme)){
//					mav.addObject("error", "Line: " + lineCount + ", Everything in column F has to be same.");
					info = "Error:Line: " + lineCount + ", Everything in column F has to be same.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Everything in column F has to be same.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}else if(!commonVenue.equals(venueStr)){
//					mav.addObject("error", "Line: " + lineCount + ", Everything in column A has to be same.");
					info = "Error:Line: " + lineCount + ", Everything in column A has to be same.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Everything in column A has to be same.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}else if(categorySymbol == null || categorySymbol.isEmpty()){
//					mav.addObject("error", "Line: " + lineCount + ", Category Symbol can not be blank.");
					info = "Error:Line: " + lineCount + ", Category Symbol can not be blank.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Category Symbol can not be blank.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}
				
				String startSection = "";
				String endSection = "";
				String startRow = "";
				String endRow = "";
				String startSeat = "";
				String endSeat = "";
				
				boolean isMergeSection=false;
				boolean isSectionOR = false;
				boolean isSectionAND = false;
				boolean isMergeRow=false;
				boolean isRowOR = false;
				boolean isRowAND = false;
				boolean isSectionInt = false;
				if (section.indexOf('-') > 0) {
					String[] tokens = section.split("-");
					if(tokens.length==2){
						startSection = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSection = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Section Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Section Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
				}else if(section.toUpperCase().indexOf("%MINUS%")>0){ // 309%minus%310
					isMergeSection = true;
					String sec = section.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					if(sec.toLowerCase().contains(" or ")){ // 309%minus%310 or 323%minus%324
						isSectionOR = true;
						isMergeSection = false;
						sec = sec.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						String temp[] = sec.toLowerCase().split(" or ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " OR ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionOR = false;
						}else{
							startSection = sec.toLowerCase().split(" or ")[0].trim();
							endSection = sec.toLowerCase().split(" or ")[1].trim();
						}
						
						
					}else if(sec.toLowerCase().contains(" and ")){ //309%minus%310 and 323%minus%324
						isSectionAND = true;
						isMergeSection = false;
						sec = sec.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						String temp[] = sec.toLowerCase().split(" and ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " AND ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionAND = false;
						}else{
							startSection = sec.toLowerCase().split(" and ")[0].trim();
							endSection = sec.toLowerCase().split(" and ")[1].trim();
						}
					}else{
						try{   // 309%minus%310
							String temp = sec.split("-")[0].trim();
							String temp1 = sec.split("-")[1].trim();
							Integer.parseInt(temp);
							Integer.parseInt(temp1);
							startSection = temp;
							endSection = temp1;
						}catch (Exception e) {  // Floor%minus%b
							startSection = sec;
							endSection = sec;
							isMergeSection = false;
						}
					}
				}else {
					String sec = section.trim();
					if(sec.toLowerCase().contains(" or ")){
						isSectionOR = true;
						String temp[] = sec.toLowerCase().split(" or ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " OR ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionOR = false;
						}else{
							startSection = sec.toLowerCase().split(" or ")[0].trim();
							endSection = sec.toLowerCase().split(" or ")[1].trim();
						}
					}else if(sec.toLowerCase().contains(" and ")){
						isSectionAND = true;
						String temp[] = sec.toLowerCase().split(" and ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " AND ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionAND = false;
						}else{
							startSection = sec.toLowerCase().split(" and ")[0].trim();
							endSection = sec.toLowerCase().split(" and ")[1].trim();
						}
					}else{
						startSection = section;
						endSection = section;
					}
					
				}

				if (row.indexOf('-') > 0) {
					String[] tokens = row.split("-");
					if(tokens.length==2){
						startRow = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endRow = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Row Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
				}else if(row.toUpperCase().indexOf("%MINUS%")>0){
					isMergeRow = true;
					String rowTemp = row.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					if(rowTemp.toLowerCase().contains(" or ")){
						isRowOR = true;
						isMergeRow = false;
						rowTemp = rowTemp.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						startRow = rowTemp.split(" or ")[0].trim();
						endRow = rowTemp.split(" or ")[1].trim();
					}else if(rowTemp.toLowerCase().contains(" and ")){
						isRowAND = true;
						isMergeRow = false;
						rowTemp = rowTemp.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						startRow = rowTemp.split(" and ")[0].trim();
						endRow = rowTemp.split(" and ")[1].trim();
					}else{
						startRow = rowTemp.split("-")[0].trim();
						endRow = rowTemp.split("-")[1].trim();
					}
					
				} else {
					String rowTemp = row.trim();
					if(rowTemp.toLowerCase().contains(" or ")){
						isRowOR = true;
						rowTemp = rowTemp.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						startRow = rowTemp.split(" or ")[0].trim();
						endRow = rowTemp.split(" or ")[1].trim();
					}else if(rowTemp.toLowerCase().contains(" and ")){
						isRowAND = true;
						rowTemp = rowTemp.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						startRow = rowTemp.split(" and ")[0].trim();
						endRow = rowTemp.split(" and ")[1].trim();
					}else{
						startRow = row.trim();
						endRow = row.trim();
					}
					
				}

				if (seat.indexOf('-') > 0) {
					String[] tokens = seat.split("-");
					if(tokens.length==2){
						startSeat = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSeat = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Seat Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Seat Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Seat Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
					
				} else {
					startSeat = seat;
					endSeat = seat;
				}
//				boolean isSectionInt=false;
				Integer intStartSection =0;
				Integer intEndSection =0; 
//				String prefix="";
				try{
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					if(!isSectionOR && !isSectionAND){
						if(intEndSection<intStartSection){
//							mav.addObject(
//									"error",
//									"Invalid line "
//									+ lineCount
//									+ ": "
//									+ line
//									+ ": Section Range is not valid.");
							info = "Error:Invalid line " + lineCount + ": " + line + ": Section Range is not valid.";
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction(" Error : Invalid line "
									+ lineCount
									+ ": "
									+ line
									+ ": Section Range is not valid.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
//							return mav;
							response.getOutputStream().write((url+"&info="+info).getBytes());
							return;
						}
					}
					isSectionInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startSection) || !validSection.containsKey(endSection)){
						if(!isMergeSection && !isSectionAND && !isSectionOR){
							if(!startSection.equals(endSection)){
								Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
								Matcher startSectionMatcher = pattern.matcher(startSection);
								Matcher endSectionMatcher = pattern.matcher(endSection);
								if(!startSectionMatcher.find() || !endSectionMatcher.find() || !startSectionMatcher.group(1).equals(endSectionMatcher.group(1))) {
//									mav.addObject(
//											"error",
//											"Invalid line "
//											+ lineCount
//											+ ": "
//											+ line
//											+ ": Section Range is not valid.");
									info = "Error:Invalid line "+ lineCount + ": " + line + ": Section Range is not valid.";
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Section Range is not valid.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
//									return mav;
									response.getOutputStream().write((url+"&info="+info).getBytes());
									return;
								}else{
	//								prefix=startSectionMatcher.group(1);
									intStartSection= Integer.parseInt(startSectionMatcher.group(2));
									intEndSection= Integer.parseInt(endSectionMatcher.group(2));
									if(intEndSection<intStartSection){
//										mav.addObject(
//												"error",
//												"Invalid line "
//												+ lineCount
//												+ ": "
//												+ line
//												+ ": Section Range is not valid.");
										info = "Error:Invalid line " + lineCount + ": " + line + ": Section Range is not valid.";
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Section Range is not valid.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
//										return mav;
										response.getOutputStream().write((url+"&info="+info).getBytes());
										return;
									}
	//								isSectionInt=true;
								}
								
							}
						}
					}else if(!isMergeSection && !isSectionAND && !isSectionOR && !validRange(startSection,endSection)){ // if section contains and,or,%minus% do not validate range
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Section Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Section Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
					
				}
				/*****************/
				
//				boolean isRowInt=false;
				Integer intStartRow =0;
				Integer intEndRow =0; 
//				String rowPrefix="";
				try{
					intStartRow= Integer.parseInt(startRow);
					intEndRow= Integer.parseInt(endRow);
					if(intEndRow<intStartRow){
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Row Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
//					isRowInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startRow) || !validSection.containsKey(endRow)){
						if(!isMergeRow && !isRowOR && !isRowAND){
							if(!startRow.equals(endRow)){
								Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
								Matcher startRowMatcher = pattern.matcher(startRow);
								Matcher endRowMatcher = pattern.matcher(endRow);
								if(!startRowMatcher.find() || !endRowMatcher.find() || !startRowMatcher.group(1).equals(endRowMatcher.group(1))){
//									mav.addObject(
//											"Invalid line "
//											+ lineCount
//											+ ": "
//											+ line
//											+ ": Row Range is not valid.");
									info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid."; 
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Row Range is not valid.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
//									return mav;
									response.getOutputStream().write((url+"&info="+info).getBytes());
									return;
								}else{
//									rowPrefix=startRowMatcher.group(1);
									intStartRow= Integer.parseInt(startRowMatcher.group(2));
									intEndRow= Integer.parseInt(endRowMatcher.group(2));
									if(intEndRow<intStartRow){
//										mav.addObject(
//												"error",
//												"Invalid line "
//												+ lineCount
//												+ ": "
//												+ line
//												+ ": Row Range is not valid.");
										info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid.";
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Row Range is not valid.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
//										return mav;
										response.getOutputStream().write((url+"&info="+info).getBytes());
										return;
									}
//									isRowInt=true;
								}
								
							}	
						}
						
					}else if(!isMergeRow && !validRange(startRow,endRow)){ // if row contains %minus% don not validate range
//						mav.addObject(
//								"error",
//								"Invalid line "
//								+ lineCount
//								+ ": "
//								+ line
//								+ ": Row Range is not valid.");
						info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
				}
				
				/********************/
				// if start section and end section cannot be compared
				// (e.g., a number and string with letters) => error
				if (endSection.length() > 0
						&& !startSection.equals("*")
						&& numberPattern.matcher(startSection).find() != numberPattern.matcher(endSection).find() 
						&& !isMergeSection
						&& !isSectionAND 
						&& !isSectionOR) {
//					mav.addObject(
//							"error",
//							"Invalid line "
//							+ lineCount
//							+ ": "
//							+ line
//							+ ": Cannot compare number and string sections");
					info = "Error:Invalid line " + lineCount + ": " + line + ": Row Range is not valid.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line "
							+ lineCount
							+ ": "
							+ line
							+ ": Cannot compare number and string sections");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}

				if (endRow.length() > 0
						&& !startRow.equals("*")
						&& numberPattern.matcher(startRow).find() != numberPattern.matcher(endRow).find()
						&& !isMergeRow) {
//					mav.addObject("error", "Invalid line " + lineCount
//							+ ": " + line
//							+ ": Cannot compare number and string for rows.");
					info = "Error:Invalid line " + lineCount + ": " + line + ": Cannot compare number and string for rows.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for rows.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}
				boolean isMerged = false;
				if (endSeat.length() > 0
						&& !startSeat.equals("*")
						&& numberPattern.matcher(startSeat).find() != numberPattern
								.matcher(endSeat).find()) {
//					mav.addObject("error", "Invalid line " + lineCount
//							+ ": " + line
//							+ ": Cannot compare number and string for seats.");
					info = "Error:Invalid line " + lineCount + ": " + line + ": Cannot compare number and string for seats.";
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for seats.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
//					return mav;
					response.getOutputStream().write((url+"&info="+info).getBytes());
					return;
				}
				
				if(isSectionInt){
					if(isMergeSection){
						// Ignore for now..
						String sectionStr = startSection + "-" + endSection;
						List<String> existingRows = sectionMap.get(sectionStr);
						if(existingRows==null){
							existingRows = new ArrayList<String>();
						}
						if(isMergeSection){
							startSection = startSection + "-" + endSection;
							endSection = startSection;
						}
						if(isSectionAND){
							startSection = startSection + " AND " + endSection;
							endRow = startRow;
						}
						if(isSectionOR){
							startSection = startSection + " OR " + endSection;
							endSection = startSection;
						}
						if(isMergeRow){
							startRow = startRow + "-" + endRow;
							endRow = startRow;
						}
						if(isRowAND){
							startRow = startRow + " AND " + endRow;
							endRow = startRow;
						}
						if(isRowOR){
							startRow = startRow + " OR " + endRow;
							endRow = startRow;
						}
						isMerged =true;
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(sectionStr, existingRows);
					}else if(isSectionOR || isSectionAND){
						//Uncomment below code once auto update zones in POS program is start working..
						// Support and/or format for number too.. for ex. 1 or 3 ,1 and 3
						/*List<String> existingRows = sectionMap.get(startSection);
						if(existingRows!=null && !isMergeRow && !isMergeSection){
							if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Line is conflicting with other line.");
								return mav;
							}
						}
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(startSection, existingRows);
						
						existingRows = sectionMap.get(endSection);
						if(existingRows!=null && !isMergeRow && !isMergeSection){
							if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Line is conflicting with other line.");
								return mav;
							}
						}
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(endSection, existingRows);*/
						String sectionStr = "";
						if(isSectionAND){
							sectionStr = startSection + " AND " + endSection;
						}else{
							sectionStr = startSection + " OR " + endSection;
						}
						List<String> existingRows = sectionMap.get(sectionStr);
						if(existingRows==null){
							existingRows = new ArrayList<String>();
						}
						if(isMergeSection){
							startSection = startSection + "-" + endSection;
							endSection = startSection;
						}
						if(isSectionAND){
							startSection = startSection + " AND " + endSection;
							endRow = startRow;
						}
						if(isSectionOR){
							startSection = startSection + " OR " + endSection;
							endSection = startSection;
						}
						if(isMergeRow){
							startRow = startRow + "-" + endRow;
							endRow = startRow;
						}
						if(isRowAND){
							startRow = startRow + " AND " + endRow;
							endRow = startRow;
						}
						if(isRowOR){
							startRow = startRow + " OR " + endRow;
							endRow = startRow;
						}
						isMerged =true;
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(sectionStr, existingRows);
					}else{
						for(Integer i=Integer.parseInt(startSection);i<=Integer.parseInt(endSection);i++){
							List<String> existingRows = sectionMap.get(i.toString());
							if(existingRows!=null && !isMergeRow && !isMergeSection){
								if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
//									mav.addObject("error", "Invalid line " + lineCount
//											+ ": " + line
//											+ ": Line is conflicting with other line.");
									info = "Error:Invalid line " + lineCount + ": " + line + ": Line is conflicting with other line.";
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line " + lineCount
											+ ": " + line
											+ ": Line is conflicting with other line.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
//									return mav;
									response.getOutputStream().write((url+"&info="+info).getBytes());
									return;
								}
							}else{
								if(existingRows!=null){
									if(isMergeSection || isMergeRow){
										for(String exRow:existingRows){
											String[] tempExRow= exRow.split("-:-");
											String exStartRow = tempExRow[0].split(":-:")[0];
											String exEndRow = tempExRow[0].split(":-:")[1];
											boolean exIsMergeSection = Boolean.parseBoolean(tempExRow[3]);
											if((exStartRow.equals("*") || exEndRow.equals("*")) && exIsMergeSection){
//												mav.addObject("error", "Invalid line " + lineCount
//														+ ": " + line
//														+ ": Line is conflicting with other line.");
												info = "Error:Invalid line " + lineCount + ": " + line + ": Line is conflicting with other line.";
												DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
												Date currentdate=new Date();
												CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
												csvaudit.setUsername(username);
												csvaudit.setAction(" Error : Invalid line " + lineCount
														+ ": " + line
														+ ": Line is conflicting with other line.");
												csvaudit.setBuilding(venueStr);
												csvaudit.setZonesgroup(catScheme);
												csvaudit.setCreatedDate(currentdate);
												csvaudit.setLinecount(lineCount);
												try{
												DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
												}catch(Exception e){e.printStackTrace();}
//												return mav;
												response.getOutputStream().write((url+"&info="+info).getBytes());
												return;
											}
										}	
									}
									
								}else{
									existingRows=new ArrayList<String>();
								}
							}
							if(isMergeSection){
								startSection = startSection + "-" + endSection;
								endSection = startSection;
							}
							if(isSectionAND){
								startSection = startSection + " AND " + endSection;
								endRow = startRow;
							}
							if(isSectionOR){
								startSection = startSection + " OR " + endSection;
								endSection = startSection;
							}
							if(isMergeRow){
								startRow = startRow + "-" + endRow;
								endRow = startRow;
							}
							if(isRowAND){
								startRow = startRow + " AND " + endRow;
								endRow = startRow;
							}
							if(isRowOR){
								startRow = startRow + " OR " + endRow;
								endRow = startRow;
							}
							existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
							sectionMap.put(i.toString(), existingRows);
						}
						isMerged =true;
					}
				}else{
					List<String> existingRows = sectionMap.get(startSection);
					if(existingRows!=null && !isMergeRow && !isMergeSection && !isSectionAND && !isSectionOR){ // do not check for conflict if row contains  %minus% or section contains %minus%,and,  or.   
						if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
//							mav.addObject("error", "Invalid line " + lineCount
//									+ ": " + line
//									+ ": Line is conflicting with other line.");
							info = "Error:Invalid line " + lineCount + ": " + line + ": Line is conflicting with other line.";
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction(" Error : Invalid line " + lineCount
									+ ": " + line
									+ ": Line is conflicting with other line.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
//							return mav;
							response.getOutputStream().write((url+"&info="+info).getBytes());
							return;
						}
					}else{
						if(existingRows!=null){
							if(isMergeSection || isMergeRow){
								for(String exRow:existingRows){
									String[] tempExRow= exRow.split("-:-");
									String exStartRow = tempExRow[0].split(":-:")[0];
									String exEndRow = tempExRow[0].split(":-:")[1];
									boolean exIsMergeSection = Boolean.parseBoolean(tempExRow[3]);
//									boolean exIsMergeRow = Boolean.parseBoolean(tempExRow[4]);
									if((exStartRow.equals("*") || exEndRow.equals("*")) && exIsMergeSection){
//										mav.addObject("error", "Invalid line " + lineCount
//												+ ": " + line
//												+ ": Line is conflicting with other line.");
										info = "Error:Invalid line " + lineCount + ": " + line + ": Line is conflicting with other line.";
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line " + lineCount
												+ ": " + line
												+ ": Line is conflicting with other line.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
//										return mav;
										response.getOutputStream().write((url+"&info="+info).getBytes());
										return;
									}
								}	
							}
							
						}else{
							existingRows=new ArrayList<String>();
						}
					}
					
					if(isMergeSection){
						startSection = startSection + "-" + endSection;
						endSection = startSection;
					}
					if(isSectionAND){
						startSection = startSection + " AND " + endSection;
						endRow = startRow;
					}
					if(isSectionOR){
						startSection = startSection + " OR " + endSection;
						endSection = startSection;
					}
					if(isMergeRow){
						startRow = startRow + "-" + endRow;
						endRow = startRow;
					}
					if(isRowAND){
						startRow = startRow + " AND " + endRow;
						endRow = startRow;
					}
					if(isRowOR){
						startRow = startRow + " OR " + endRow;
						endRow = startRow;
					}
					isMerged =true;
					existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
					sectionMap.put(startSection, existingRows);
				}
				if(null != larrySection && !larrySection.isEmpty()){
					
					String larrySectionValue = larrySection.replaceAll(" +", "") +"-"+ lastRow.replaceAll(" +", "");
					
					if(null != larrySectionMap.get(larrySection) && !larrySectionMap.get(larrySection).isEmpty()){
						
						if(larrySectionMap.get(larrySection).equalsIgnoreCase(larrySectionValue)){
						
						}else{
//							mav.addObject("error", "Line: " + lineCount + ", In correct CSV : Column G and Column K values are not unique.");
							info = "Error:Line: " + lineCount + ", In correct CSV : Column G and Column K values are not unique.";
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction("Error Line: " + lineCount + ", In correct CSV : Column G and Column K values are not unique.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
//							return mav;
							response.getOutputStream().write((url+"&info="+info).getBytes());
							return;
						}
					}else{
						larrySectionMap.put(larrySection, larrySectionValue);
					}
				
				}
				
				String categoryKey = venue.getId() + "_" + categorySymbol + "_" + catScheme;
				Category category  = categoryKeyMap.get(categoryKey);
				Category existingCat = catMap.get(categorySymbol);
				boolean isExist = false;
				if(category==null){
					category  = existingCat;
					isExist = true;
				}
				if(category == null){
					category = new Category();
					category.setDescription("Category " + categorySymbol);
					category.setEqualCats(equivalentCats);
					category.setGroupName(catScheme);
					category.setSymbol(categorySymbol);
					category.setVenueCategoryId(venueCategory.getId());
					category.setNormalizedZone(normalizedZone);
					DAORegistry.getCategoryDAO().save(category);
				}else if(!category.getNormalizedZone().equals(normalizedZone)){
					if(isExist){
						DAORegistry.getCategoryDAO().delete(category);
						category.setId(null);
						category.setNormalizedZone(normalizedZone);
						DAORegistry.getCategoryDAO().save(category);
					}else{
//						mav.addObject("error", "Line: " + lineCount + ", There should be only one combination of category symbol and normalized zone.");
						info = "Error:Line: " + lineCount + ", There should be only one combination of category symbol and normalized zone.";
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Line: " + lineCount + ", There should be only one combination of category symbol and normalized zone.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
//						return mav;
						response.getOutputStream().write((url+"&info="+info).getBytes());
						return;
					}
				}
				if(existingCat!= null){
					catMap.remove(existingCat.getSymbol());
				}
				categoryKeyMap.put(categoryKey, category);
				// 309%minus%310 or 323%minus%324  -- > 309-310 : 323-324
				if(!isMerged){
					if(isMergeSection){
						startSection = startSection + "-" + endSection;
						endSection = startSection;
					}
					if(isSectionAND){
						startSection = startSection + " AND " + endSection;
						endRow = startRow;
					}
					if(isSectionOR){
						startSection = startSection + " OR " + endSection;
						endSection = startSection;
					}
					if(isMergeRow){
						startRow = startRow + "-" + endRow;
						endRow = startRow;
					}
					if(isRowAND){
						startRow = startRow + " AND " + endRow;
						endRow = startRow;
					}
					if(isRowOR){
						startRow = startRow + " OR " + endRow;
						endRow = startRow;
					}
				}
				CategoryMapping  cm = new CategoryMapping();
				cm.setCategoryId(category.getId());
				cm.setStartSection(startSection);
				cm.setEndSection(endSection);
				cm.setStartRow(startRow);
				cm.setEndRow(endRow);
				cm.setStartSeat(startSeat);
				cm.setEndSeat(endSeat);
				cm.setLarrySection(larrySection);
				cm.setLastRow(lastRow);
				cm.setRowRange(rowRange);
				cm.setAlternateRow(alternateRow);
				DAORegistry.getCategoryMappingDAO().save(cm);
				
			} catch (Exception e) {
				e.printStackTrace();
//				mav.addObject("error", "Invalid line: " + lineCount + ": " + line);
				info = "Error:Invalid line: " + lineCount + ": " + line;
				DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
				Date currentdate=new Date();
				String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
				String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
				CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
				csvaudit.setUsername(username);
				csvaudit.setAction(" Error : Invalid line: " + lineCount + ": "
						+ line);
				csvaudit.setBuilding(venueStr);
				csvaudit.setZonesgroup(catScheme);
				csvaudit.setCreatedDate(currentdate);
				csvaudit.setLinecount(lineCount);
				try{
				DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
				}catch(Exception ee){ee.printStackTrace();}
//				return mav;
				response.getOutputStream().write((url+"&info="+info).getBytes());
				return;
			}
		}
		Collection<Category> removeCatList = catMap.values();
		DAORegistry.getCategoryDAO().deleteAll(removeCatList);
		
//		mav.addObject( "info", "CSV updated successfully.");
		info = "Info:CSV updated successfully for Venue:" + venue.getBuilding() + " and Category:" + categoryGroup; 
		System.out.println("username : "+username);
		String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
		System.out.println("venueStr : "+venueStr);
		String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
		System.out.println("catScheme : "+catScheme);
		Date currentdate=new Date();
		System.out.println("currentdate : "+currentdate);
		System.out.println("IntialLineCount : "+intialLineCount);
		System.out.println("After Upload Linecount : "+lineCount);
		CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
		csvaudit.setUsername(username);
		if(intialLineCount==0)
		{
			csvaudit.setAction("Added");
		}
		else{
			csvaudit.setAction("Updated");
		}
		csvaudit.setBuilding(venueStr);
		csvaudit.setZonesgroup(catScheme);
		csvaudit.setCreatedDate(currentdate);
		csvaudit.setLinecount(lineCount);
		csvaudit.setInitialLineCount(intialLineCount);
		try{
		DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
		}catch(Exception e){e.printStackTrace();}
//		return mav;
		response.getOutputStream().write((url+"&info="+info).getBytes());
		
	}
	
	public ModelAndView loadEditorUploadCategoryMappingCsvFile(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		Integer tourId = null; venueIdStr
		ModelAndView mav = new ModelAndView(new RedirectView("EditorEditCategories"));
//		Collection<Event> events = new ArrayList<Event>();
		String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
		// if we don't upload a file, leave it
		if (!(request instanceof MultipartHttpServletRequest)) {
			return mav;
		}
		
		MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
		
//		boolean deletedOldCats = false;
//		HashMap<String, Boolean> deletedCatsMap = new HashMap<String, Boolean>();
		/*if(tourIdStr == null || tourIdStr.isEmpty()) {
			for(String groupName : Categorizer.getCategoryGroupsByEvent(eventId)){
				deletedCatsMap.put(eventId + " " + groupName, new Boolean(false));
			}
		} else {
			for(Event event : DAORegistry.getEventDAO().getAllEventsByTour(tourId, EventStatus.ACTIVE)){
				for(String groupName : Categorizer.getCategoryGroupsByEvent(event.getId())){
					deletedCatsMap.put(event.getId() + " " + groupName, new Boolean(false));
				}
			}
		}*/
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				multi.getFile("file").getInputStream()));
		String line;
		int lineCount = 0;
		int intialLineCount=0;
//		HashMap<Integer,Collection<CategoryMapping>> mappings = new HashMap<Integer,Collection<CategoryMapping>>();

		// parse file
//		Map<String, Venue> venueMap = new HashMap<String, Venue>();
//		Map<String, TMATDependentZonesCategory> zonesMap = new HashMap<String, TMATDependentZonesCategory>();
//		List<TMATDependentZonesCategory> list = DAORegistry.getTmatDependentZonesCategoryDAO().getTmatDependentsByTourId(tourId);
//		if(list!=null && !list.isEmpty()){
//			for(TMATDependentZonesCategory cat:list){
//				zonesMap.put(cat.getTourId()+"-"+cat.getVenueId(), cat);
//			}
//		}
		
		Map<String, Integer> dupLineMap = new HashMap<String, Integer>();
		Map<String,List<String>> sectionMap = new HashMap<String, List<String>>();
		
		Map<String, Category> categoryKeyMap = new HashMap<String, Category>();
		
		
		Map<String, String> larrySectionMap = new HashMap<String, String>();
//		Map<String, List<String>> lineMap = new HashMap<String, List<String>>();
		String commonVenue="";
		String commonCategory="";
//		String commonNormalizedZone="";
		
//		Set<Category> catList = new ListOrderedSet();
		VenueCategory venueCategory =null;
		Venue venue = null;
		String[] lineTokens=null;
		Map<String,Category> catMap = new HashMap<String, Category>();
		while ((line = reader.readLine()) != null) {
			try {
				lineCount++;
				if (line.trim().isEmpty()) {
					continue;
				}
				
				//String[] lineTokens = line.split(",");
				lineTokens = line.split(",");
				
				if(lineTokens.length <1) {
					continue;
				}
				
				if (lineTokens.length != 11 && lineTokens.length != 12 && lineTokens.length != 13) {
					mav.addObject("error", "Error Line " + lineCount + ": Column Count : "+lineTokens.length+" "
							+ line + ": Invalid format.");
					String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
					String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line " + lineCount + ": "+ line + ": Invalid format.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;						
				}
				String key="";
				for(String token:lineTokens){
					if(null != token && !token.isEmpty()){
						key+=token.toUpperCase() + "-" ;
					}
				}
				if(dupLineMap.containsKey(key)){
					Integer lineNo=dupLineMap.get(key);
					mav.addObject("error", "Line " + lineNo + " and "
							+ lineCount + " is duplicate.");
					String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
					String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error : Line " + lineNo + " and "
							+ lineCount + " is duplicate.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;	
				}
				dupLineMap.put(key, lineCount);
				
				String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
				String section = TextUtil.trimQuotes(lineTokens[1].toUpperCase());
				String row = TextUtil.trimQuotes(lineTokens[2].toUpperCase());
				String seat = TextUtil.trimQuotes(lineTokens[3].toUpperCase());
				String categorySymbol = TextUtil.trimQuotes(lineTokens[4].toUpperCase());
				String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
				/*String catSeatQty = TextUtil.trimQuotes(lineTokens[6].toUpperCase());*/
				String larrySection = "";
				if(lineTokens[6] != null && !lineTokens[6].isEmpty())
					larrySection = TextUtil.trimQuotes(lineTokens[6].toUpperCase());
				
				String equivalentCats = TextUtil.trimQuotes(lineTokens[7].toUpperCase());
				String normalizedZone = TextUtil.trimQuotes(lineTokens[8].toUpperCase());
				String rowRange = TextUtil.trimQuotes(lineTokens[9].toUpperCase());
				String lastRow = TextUtil.trimQuotes(lineTokens[10].toUpperCase());
				String alternateRow = null;
				String vipLastRow = null;
				
				if(lineTokens.length >11){
					alternateRow = TextUtil.trimQuotes(lineTokens[11].toUpperCase());
				}
				
				if(lineTokens.length >12){
					vipLastRow = TextUtil.trimQuotes(lineTokens[12].toUpperCase());
				}
				
				if(lineCount==1){
					commonVenue=venueStr;
					commonCategory=catScheme;
					venue= DAORegistry.getVenueDAO().getVenue(venueStr);
					if (venue == null){
						mav.addObject("error", "Invalid line " + lineCount
								+ ": " + line
								+ ": Cannot find venue:" + venueStr);
						
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction("Invalid line " + lineCount
								+ ": " + line
								+ ": Cannot find venue:" + venueStr);
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
					mav.addObject("venueIdStr", venue.getId());
					mav.addObject("catScheme",catScheme);
					venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venue.getId(), commonCategory);
					if(venueCategory==null){
						venueCategory = new VenueCategory();
						venueCategory.setCategoryGroup(catScheme);
						venueCategory.setVenue(venue);
						venueCategory.setLastUpdatedDate(new Date());
						venueCategory.setLastUpdatedBy(username);
						venueCategory.setIsVenuMap(false);
						DAORegistry.getVenueCategoryDAO().save(venueCategory);
					}else{
						Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
						intialLineCount=DAORegistry.getCategoryDAO().countByVenueCategoryID(venueCategory.getId());
						System.out.println("intialLineCount : "+intialLineCount);

						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						for(Category cat:catList){
							if(catMap.containsKey(cat.getSymbol())){
								Category oldCat =null;
								if(cat.getNormalizedZone().equals(normalizedZone)){
									oldCat = catMap.remove(cat.getSymbol());
								}else{
									oldCat = cat;
								}
								DAORegistry.getCategoryDAO().delete(oldCat);
							}
							catMap.put(cat.getSymbol(), cat);
						}
						venueCategory.setLastUpdatedDate(new Date());
						venueCategory.setLastUpdatedBy(username);
						DAORegistry.getVenueCategoryDAO().update(venueCategory);
					}
					
				}else if(!commonCategory.equals(catScheme)){
					mav.addObject("error", "Line: " + lineCount + ", Everything in column F has to be same.");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Everything in column F has to be same.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}else if(!commonVenue.equals(venueStr)){
					mav.addObject("error", "Line: " + lineCount + ", Everything in column A has to be same.");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Everything in column A has to be same.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}else if(categorySymbol == null || categorySymbol.isEmpty()){
					mav.addObject("error", "Line: " + lineCount + ", Category Symbol can not be blank.");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction("Error Line: " + lineCount + ", Category Symbol can not be blank.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}
				
				/*if(catMap.containsKey(categorySymbol)){
					Category updateCategory = catMap.get(categorySymbol);
					if(updateCategory.getNormalizedZone().equalsIgnoreCase(normalizedZone)){
						if((updateCategory.getLarrySection() == null || updateCategory.getLarrySection().isEmpty()) && 
								(larrySection != null && !larrySection.isEmpty())){
							updateCategory.setLarrySection(larrySection);
							DAORegistry.getCategoryDAO().update(updateCategory);
						}else if((updateCategory.getLarrySection() != null && !updateCategory.getLarrySection().isEmpty()) && 
								!updateCategory.getLarrySection().equalsIgnoreCase(larrySection)){
							updateCategory.setLarrySection(larrySection);
							DAORegistry.getCategoryDAO().update(updateCategory);
						}
					}
				}*/
				
				String startSection = "";
				String endSection = "";
				String startRow = "";
				String endRow = "";
				String startSeat = "";
				String endSeat = "";
				
				boolean isMergeSection=false;
				boolean isSectionOR = false;
				boolean isSectionAND = false;
				boolean isMergeRow=false;
				boolean isRowOR = false;
				boolean isRowAND = false;
				boolean isSectionInt = false;
				if (section.indexOf('-') > 0) {
					String[] tokens = section.split("-");
					if(tokens.length==2){
						startSection = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSection = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
				}else if(section.toUpperCase().indexOf("%MINUS%")>0){ // 309%minus%310
					isMergeSection = true;
					String sec = section.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					if(sec.toLowerCase().contains(" or ")){ // 309%minus%310 or 323%minus%324
						isSectionOR = true;
						isMergeSection = false;
						sec = sec.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						String temp[] = sec.toLowerCase().split(" or ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " OR ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionOR = false;
						}else{
							startSection = sec.toLowerCase().split(" or ")[0].trim();
							endSection = sec.toLowerCase().split(" or ")[1].trim();
						}
						
						
					}else if(sec.toLowerCase().contains(" and ")){ //309%minus%310 and 323%minus%324
						isSectionAND = true;
						isMergeSection = false;
						sec = sec.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						/*startSection = sec.split(" and ")[0].trim();
						endSection = sec.split(" and ")[1].trim();*/
						String temp[] = sec.toLowerCase().split(" and ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " AND ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionAND = false;
						}else{
							startSection = sec.toLowerCase().split(" and ")[0].trim();
							endSection = sec.toLowerCase().split(" and ")[1].trim();
						}
					}else{
						try{   // 309%minus%310
							String temp = sec.split("-")[0].trim();
							String temp1 = sec.split("-")[1].trim();
							Integer.parseInt(temp);
							Integer.parseInt(temp1);
							startSection = temp;
							endSection = temp1;
						}catch (Exception e) {  // Floor%minus%b
							startSection = sec;
							endSection = sec;
							isMergeSection = false;
						}
					}
				}else {
					String sec = section.trim();
					if(sec.toLowerCase().contains(" or ")){
						isSectionOR = true;
						/*sec = sec.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						startSection = sec.split(" or ")[0].trim();
						endSection = sec.split(" or ")[1].trim();*/
						String temp[] = sec.toLowerCase().split(" or ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " OR ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionOR = false;
						}else{
							startSection = sec.toLowerCase().split(" or ")[0].trim();
							endSection = sec.toLowerCase().split(" or ")[1].trim();
						}
					}else if(sec.toLowerCase().contains(" and ")){
						isSectionAND = true;
						/*sec = sec.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						startSection = sec.split(" and ")[0].trim();
						endSection = sec.split(" and ")[1].trim();*/
						String temp[] = sec.toLowerCase().split(" and ");
						if(temp.length>2){
							for(int i=0;i<=temp.length-1;i++){
								startSection += temp[i].trim() + " AND ";
							}
							startSection=startSection.substring(0, startSection.length()-4);
							endSection = startSection;
							isSectionAND = false;
						}else{
							startSection = sec.toLowerCase().split(" and ")[0].trim();
							endSection = sec.toLowerCase().split(" and ")[1].trim();
						}
					}else{
						startSection = section;
						endSection = section;
					}
					
				}

				if (row.indexOf('-') > 0) {
					String[] tokens = row.split("-");
					if(tokens.length==2){
						startRow = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endRow = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
				}else if(row.toUpperCase().indexOf("%MINUS%")>0){
					isMergeRow = true;
					String rowTemp = row.replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					if(rowTemp.toLowerCase().contains(" or ")){
						isRowOR = true;
						isMergeRow = false;
						rowTemp = rowTemp.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						startRow = rowTemp.split(" or ")[0].trim();
						endRow = rowTemp.split(" or ")[1].trim();
					}else if(rowTemp.toLowerCase().contains(" and ")){
						isRowAND = true;
						isMergeRow = false;
						rowTemp = rowTemp.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						startRow = rowTemp.split(" and ")[0].trim();
						endRow = rowTemp.split(" and ")[1].trim();
					}else{
						startRow = rowTemp.split("-")[0].trim();
						endRow = rowTemp.split("-")[1].trim();
					}
					
				} else {
					String rowTemp = row.trim();
					if(rowTemp.toLowerCase().contains(" or ")){
						isRowOR = true;
						rowTemp = rowTemp.replaceAll(CategoryMapping.OR_MATCH_STRING, " or ").trim();
						startRow = rowTemp.split(" or ")[0].trim();
						endRow = rowTemp.split(" or ")[1].trim();
					}else if(rowTemp.toLowerCase().contains(" and ")){
						isRowAND = true;
						rowTemp = rowTemp.replaceAll(CategoryMapping.AND_MATCH_STRING, " and ").trim();
						startRow = rowTemp.split(" and ")[0].trim();
						endRow = rowTemp.split(" and ")[1].trim();
					}else{
						startRow = row.trim();
						endRow = row.trim();
					}
					
				}

				if (seat.indexOf('-') > 0) {
					String[] tokens = seat.split("-");
					if(tokens.length==2){
						startSeat = tokens[0].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
						endSeat = tokens[1].replaceAll(CategoryMapping.MINUS_ESCAPE_MATCH_STRING, "-").trim();
					}else{
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Seat Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Seat Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
					
				} else {
					startSeat = seat;
					endSeat = seat;
				}
//				boolean isSectionInt=false;
				Integer intStartSection =0;
				Integer intEndSection =0; 
//				String prefix="";
				try{
					intStartSection= Integer.parseInt(startSection);
					intEndSection= Integer.parseInt(endSection);
					if(!isSectionOR && !isSectionAND){
						if(intEndSection<intStartSection){
							mav.addObject(
									"error",
									"Invalid line "
									+ lineCount
									+ ": "
									+ line
									+ ": Section Range is not valid.");
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction(" Error : Invalid line "
									+ lineCount
									+ ": "
									+ line
									+ ": Section Range is not valid.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
							return mav;
						}
					}
					isSectionInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startSection) || !validSection.containsKey(endSection)){
						if(!isMergeSection && !isSectionAND && !isSectionOR){
							if(!startSection.equals(endSection)){
								Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
								Matcher startSectionMatcher = pattern.matcher(startSection);
								Matcher endSectionMatcher = pattern.matcher(endSection);
								if(!startSectionMatcher.find() || !endSectionMatcher.find() || !startSectionMatcher.group(1).equals(endSectionMatcher.group(1))) {
									mav.addObject(
											"error",
											"Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Section Range is not valid.");
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Section Range is not valid.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
									return mav;
								}else{
	//								prefix=startSectionMatcher.group(1);
									intStartSection= Integer.parseInt(startSectionMatcher.group(2));
									intEndSection= Integer.parseInt(endSectionMatcher.group(2));
									if(intEndSection<intStartSection){
										mav.addObject(
												"error",
												"Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Section Range is not valid.");
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Section Range is not valid.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
										return mav;
									}
	//								isSectionInt=true;
								}
								
							}
						}
					}else if(!isMergeSection && !isSectionAND && !isSectionOR && !validRange(startSection,endSection)){ // if section contains and,or,%minus% do not validate range
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Section Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
					
				}
				/*****************/
				
//				boolean isRowInt=false;
				Integer intStartRow =0;
				Integer intEndRow =0; 
//				String rowPrefix="";
				try{
					intStartRow= Integer.parseInt(startRow);
					intEndRow= Integer.parseInt(endRow);
					if(intEndRow<intStartRow){
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
//					isRowInt=true;
				}catch (Exception ex) {
					if(validSection.isEmpty()){
						fillValidSection();
					}
					if(!validSection.containsKey(startRow) || !validSection.containsKey(endRow)){
						if(!isMergeRow && !isRowOR && !isRowAND){
							if(!startRow.equals(endRow)){
								Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
								Matcher startRowMatcher = pattern.matcher(startRow);
								Matcher endRowMatcher = pattern.matcher(endRow);
								if(!startRowMatcher.find() || !endRowMatcher.find() || !startRowMatcher.group(1).equals(endRowMatcher.group(1))){
									mav.addObject(
											"error",
											"Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Row Range is not valid.");
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line "
											+ lineCount
											+ ": "
											+ line
											+ ": Row Range is not valid.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
									return mav;
								}else{
//									rowPrefix=startRowMatcher.group(1);
									intStartRow= Integer.parseInt(startRowMatcher.group(2));
									intEndRow= Integer.parseInt(endRowMatcher.group(2));
									if(intEndRow<intStartRow){
										mav.addObject(
												"error",
												"Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Row Range is not valid.");
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line "
												+ lineCount
												+ ": "
												+ line
												+ ": Row Range is not valid.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
										return mav;
									}
//									isRowInt=true;
								}
								
							}	
						}
						
					}else if(!isMergeRow && !validRange(startRow,endRow)){ // if row contains %minus% don not validate range
						mav.addObject(
								"error",
								"Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Invalid line "
								+ lineCount
								+ ": "
								+ line
								+ ": Row Range is not valid.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
				}
				
				/********************/
				// if start section and end section cannot be compared
				// (e.g., a number and string with letters) => error
				if (endSection.length() > 0
						&& !startSection.equals("*")
						&& numberPattern.matcher(startSection).find() != numberPattern.matcher(endSection).find() 
						&& !isMergeSection
						&& !isSectionAND 
						&& !isSectionOR) {
					mav.addObject(
							"error",
							"Invalid line "
							+ lineCount
							+ ": "
							+ line
							+ ": Cannot compare number and string sections");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line "
							+ lineCount
							+ ": "
							+ line
							+ ": Cannot compare number and string sections");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}

				if (endRow.length() > 0
						&& !startRow.equals("*")
						&& numberPattern.matcher(startRow).find() != numberPattern.matcher(endRow).find()
						&& !isMergeRow) {
					mav.addObject("error", "Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for rows.");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for rows.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}
				boolean isMerged = false;
				if (endSeat.length() > 0
						&& !startSeat.equals("*")
						&& numberPattern.matcher(startSeat).find() != numberPattern
								.matcher(endSeat).find()) {
					mav.addObject("error", "Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for seats.");
					DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
					Date currentdate=new Date();
					CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
					csvaudit.setUsername(username);
					csvaudit.setAction(" Error : Invalid line " + lineCount
							+ ": " + line
							+ ": Cannot compare number and string for seats.");
					csvaudit.setBuilding(venueStr);
					csvaudit.setZonesgroup(catScheme);
					csvaudit.setCreatedDate(currentdate);
					csvaudit.setLinecount(lineCount);
					try{
					DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
					}catch(Exception e){e.printStackTrace();}
					return mav;
				}
				
				if(isSectionInt){
					if(isMergeSection){
						// Ignore for now..
						String sectionStr = startSection + "-" + endSection;
						List<String> existingRows = sectionMap.get(sectionStr);
						if(existingRows==null){
							existingRows = new ArrayList<String>();
						}
						if(isMergeSection){
							startSection = startSection + "-" + endSection;
							endSection = startSection;
						}
						if(isSectionAND){
							startSection = startSection + " AND " + endSection;
							endRow = startRow;
						}
						if(isSectionOR){
							startSection = startSection + " OR " + endSection;
							endSection = startSection;
						}
						if(isMergeRow){
							startRow = startRow + "-" + endRow;
							endRow = startRow;
						}
						if(isRowAND){
							startRow = startRow + " AND " + endRow;
							endRow = startRow;
						}
						if(isRowOR){
							startRow = startRow + " OR " + endRow;
							endRow = startRow;
						}
						isMerged =true;
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(sectionStr, existingRows);
					}else if(isSectionOR || isSectionAND){
						//Uncomment below code once auto update zones in POS program is start working..
						// Support and/or format for number too.. for ex. 1 or 3 ,1 and 3
						/*List<String> existingRows = sectionMap.get(startSection);
						if(existingRows!=null && !isMergeRow && !isMergeSection){
							if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Line is conflicting with other line.");
								return mav;
							}
						}
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(startSection, existingRows);
						
						existingRows = sectionMap.get(endSection);
						if(existingRows!=null && !isMergeRow && !isMergeSection){
							if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
								mav.addObject("error", "Invalid line " + lineCount
										+ ": " + line
										+ ": Line is conflicting with other line.");
								return mav;
							}
						}
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(endSection, existingRows);*/
						String sectionStr = "";
						if(isSectionAND){
							sectionStr = startSection + " AND " + endSection;
						}else{
							sectionStr = startSection + " OR " + endSection;
						}
						List<String> existingRows = sectionMap.get(sectionStr);
						if(existingRows==null){
							existingRows = new ArrayList<String>();
						}
						if(isMergeSection){
							startSection = startSection + "-" + endSection;
							endSection = startSection;
						}
						if(isSectionAND){
							startSection = startSection + " AND " + endSection;
							endRow = startRow;
						}
						if(isSectionOR){
							startSection = startSection + " OR " + endSection;
							endSection = startSection;
						}
						if(isMergeRow){
							startRow = startRow + "-" + endRow;
							endRow = startRow;
						}
						if(isRowAND){
							startRow = startRow + " AND " + endRow;
							endRow = startRow;
						}
						if(isRowOR){
							startRow = startRow + " OR " + endRow;
							endRow = startRow;
						}
						isMerged =true;
						existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
						sectionMap.put(sectionStr, existingRows);
					}else{
						for(Integer i=Integer.parseInt(startSection);i<=Integer.parseInt(endSection);i++){
							List<String> existingRows = sectionMap.get(i.toString());
							if(existingRows!=null && !isMergeRow && !isMergeSection){
								if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
									mav.addObject("error", "Invalid line " + lineCount
											+ ": " + line
											+ ": Line is conflicting with other line.");
									DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
									Date currentdate=new Date();
									CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
									csvaudit.setUsername(username);
									csvaudit.setAction(" Error : Invalid line " + lineCount
											+ ": " + line
											+ ": Line is conflicting with other line.");
									csvaudit.setBuilding(venueStr);
									csvaudit.setZonesgroup(catScheme);
									csvaudit.setCreatedDate(currentdate);
									csvaudit.setLinecount(lineCount);
									try{
									DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
									}catch(Exception e){e.printStackTrace();}
									return mav;
								}
							}else{
								if(existingRows!=null){
									if(isMergeSection || isMergeRow){
										for(String exRow:existingRows){
											String[] tempExRow= exRow.split("-:-");
											String exStartRow = tempExRow[0].split(":-:")[0];
											String exEndRow = tempExRow[0].split(":-:")[1];
											boolean exIsMergeSection = Boolean.parseBoolean(tempExRow[3]);
	//										boolean exIsMergeRow = Boolean.parseBoolean(tempExRow[4]);
											if((exStartRow.equals("*") || exEndRow.equals("*")) && exIsMergeSection){
												mav.addObject("error", "Invalid line " + lineCount
														+ ": " + line
														+ ": Line is conflicting with other line.");
												DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
												Date currentdate=new Date();
												CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
												csvaudit.setUsername(username);
												csvaudit.setAction(" Error : Invalid line " + lineCount
														+ ": " + line
														+ ": Line is conflicting with other line.");
												csvaudit.setBuilding(venueStr);
												csvaudit.setZonesgroup(catScheme);
												csvaudit.setCreatedDate(currentdate);
												csvaudit.setLinecount(lineCount);
												try{
												DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
												}catch(Exception e){e.printStackTrace();}
												return mav;
											}
										}	
									}
									
								}else{
									existingRows=new ArrayList<String>();
								}
							}
							if(isMergeSection){
								startSection = startSection + "-" + endSection;
								endSection = startSection;
							}
							if(isSectionAND){
								startSection = startSection + " AND " + endSection;
								endRow = startRow;
							}
							if(isSectionOR){
								startSection = startSection + " OR " + endSection;
								endSection = startSection;
							}
							if(isMergeRow){
								startRow = startRow + "-" + endRow;
								endRow = startRow;
							}
							if(isRowAND){
								startRow = startRow + " AND " + endRow;
								endRow = startRow;
							}
							if(isRowOR){
								startRow = startRow + " OR " + endRow;
								endRow = startRow;
							}
							existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
							sectionMap.put(i.toString(), existingRows);
						}
						isMerged =true;
					}
				}else{
					List<String> existingRows = sectionMap.get(startSection);
					if(existingRows!=null && !isMergeRow && !isMergeSection && !isSectionAND && !isSectionOR){ // do not check for conflict if row contains  %minus% or section contains %minus%,and,  or.   
						if(checkConflict(existingRows, startRow, endRow, startSeat, endSeat,isMergeSection,isMergeRow)){
							mav.addObject("error", "Invalid line " + lineCount
									+ ": " + line
									+ ": Line is conflicting with other line.");
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction(" Error : Invalid line " + lineCount
									+ ": " + line
									+ ": Line is conflicting with other line.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
							return mav;
						}
					}else{
						if(existingRows!=null){
							if(isMergeSection || isMergeRow){
								for(String exRow:existingRows){
									String[] tempExRow= exRow.split("-:-");
									String exStartRow = tempExRow[0].split(":-:")[0];
									String exEndRow = tempExRow[0].split(":-:")[1];
									boolean exIsMergeSection = Boolean.parseBoolean(tempExRow[3]);
//									boolean exIsMergeRow = Boolean.parseBoolean(tempExRow[4]);
									if((exStartRow.equals("*") || exEndRow.equals("*")) && exIsMergeSection){
										mav.addObject("error", "Invalid line " + lineCount
												+ ": " + line
												+ ": Line is conflicting with other line.");
										DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
										Date currentdate=new Date();
										CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
										csvaudit.setUsername(username);
										csvaudit.setAction(" Error : Invalid line " + lineCount
												+ ": " + line
												+ ": Line is conflicting with other line.");
										csvaudit.setBuilding(venueStr);
										csvaudit.setZonesgroup(catScheme);
										csvaudit.setCreatedDate(currentdate);
										csvaudit.setLinecount(lineCount);
										try{
										DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
										}catch(Exception e){e.printStackTrace();}
										return mav;
									}
								}	
							}
							
						}else{
							existingRows=new ArrayList<String>();
						}
					}
					
					if(isMergeSection){
						startSection = startSection + "-" + endSection;
						endSection = startSection;
					}
					if(isSectionAND){
						startSection = startSection + " AND " + endSection;
						endRow = startRow;
					}
					if(isSectionOR){
						startSection = startSection + " OR " + endSection;
						endSection = startSection;
					}
					if(isMergeRow){
						startRow = startRow + "-" + endRow;
						endRow = startRow;
					}
					if(isRowAND){
						startRow = startRow + " AND " + endRow;
						endRow = startRow;
					}
					if(isRowOR){
						startRow = startRow + " OR " + endRow;
						endRow = startRow;
					}
					isMerged =true;
					existingRows.add(startRow + ":-:" + endRow + "-:-" + startSeat + ":-:" + endSeat + "-:-" + isMergeSection + "-:-" + isMergeRow + "-:-" + lineCount);
					sectionMap.put(startSection, existingRows);
				}
				if(null != larrySection && !larrySection.isEmpty()){
					
					String larrySectionValue = larrySection.replaceAll(" +", "") +"-"+ lastRow.replaceAll(" +", "");
					
					if(null != larrySectionMap.get(larrySection) && !larrySectionMap.get(larrySection).isEmpty()){
						
						if(larrySectionMap.get(larrySection).equalsIgnoreCase(larrySectionValue)){
						
						}else{
							mav.addObject("error", "Line: " + lineCount + ", In correct CSV : Column G and Column K values are not unique.");
							DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
							Date currentdate=new Date();
							CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
							csvaudit.setUsername(username);
							csvaudit.setAction("Error Line: " + lineCount + ", In correct CSV : Column G and Column K values are not unique.");
							csvaudit.setBuilding(venueStr);
							csvaudit.setZonesgroup(catScheme);
							csvaudit.setCreatedDate(currentdate);
							csvaudit.setLinecount(lineCount);
							try{
							DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
							}catch(Exception e){e.printStackTrace();}
							return mav;
						}
					}else{
						larrySectionMap.put(larrySection, larrySectionValue);
					}
				
				}
				
				String categoryKey = venue.getId() + "_" + categorySymbol + "_" + catScheme;
				Category category  = categoryKeyMap.get(categoryKey);
				Category existingCat = catMap.get(categorySymbol);
				boolean isExist = false;
				if(category==null){
					category  = existingCat;
					isExist = true;
				}
				if(category == null){
					category = new Category();
					category.setDescription("Category " + categorySymbol);
					category.setEqualCats(equivalentCats);
					category.setGroupName(catScheme);
					category.setSymbol(categorySymbol);
					category.setVenueCategoryId(venueCategory.getId());
					category.setNormalizedZone(normalizedZone);
					//category.setLarrySection(larrySection);
					/*if(catSeatQty!=null && !catSeatQty.isEmpty()){
						category.setSeatQuantity(Integer.parseInt(catSeatQty));
					}*/
					DAORegistry.getCategoryDAO().save(category);
				}else if(!category.getNormalizedZone().equals(normalizedZone)){
					if(isExist){
						DAORegistry.getCategoryDAO().delete(category);
						category.setId(null);
						category.setNormalizedZone(normalizedZone);
						DAORegistry.getCategoryDAO().save(category);
					}else{
						mav.addObject("error", "Line: " + lineCount + ", There should be only one combination of category symbol and normalized zone.");
						DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
						Date currentdate=new Date();
						CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
						csvaudit.setUsername(username);
						csvaudit.setAction(" Error : Line: " + lineCount + ", There should be only one combination of category symbol and normalized zone.");
						csvaudit.setBuilding(venueStr);
						csvaudit.setZonesgroup(catScheme);
						csvaudit.setCreatedDate(currentdate);
						csvaudit.setLinecount(lineCount);
						try{
						DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
						}catch(Exception e){e.printStackTrace();}
						return mav;
					}
				}
				if(existingCat!= null){
					catMap.remove(existingCat.getSymbol());
				}
				categoryKeyMap.put(categoryKey, category);
				// 309%minus%310 or 323%minus%324  -- > 309-310 : 323-324
				if(!isMerged){
					if(isMergeSection){
						startSection = startSection + "-" + endSection;
						endSection = startSection;
					}
					if(isSectionAND){
						startSection = startSection + " AND " + endSection;
						endRow = startRow;
					}
					if(isSectionOR){
						startSection = startSection + " OR " + endSection;
						endSection = startSection;
					}
					if(isMergeRow){
						startRow = startRow + "-" + endRow;
						endRow = startRow;
					}
					if(isRowAND){
						startRow = startRow + " AND " + endRow;
						endRow = startRow;
					}
					if(isRowOR){
						startRow = startRow + " OR " + endRow;
						endRow = startRow;
					}
				}
				CategoryMapping  cm = new CategoryMapping();
				cm.setCategoryId(category.getId());
				cm.setStartSection(startSection);
				cm.setEndSection(endSection);
				cm.setStartRow(startRow);
				cm.setEndRow(endRow);
				cm.setStartSeat(startSeat);
				cm.setEndSeat(endSeat);
				cm.setLarrySection(larrySection);
				cm.setLastRow(lastRow);
				cm.setRowRange(rowRange);
				cm.setAlternateRow(alternateRow);
				cm.setVipLastRow(vipLastRow);
				DAORegistry.getCategoryMappingDAO().save(cm);
				
			} catch (Exception e) {
				e.printStackTrace();
				mav.addObject("error", "Invalid line: " + lineCount + ": "
						+ line);
				DAORegistry.getCategoryDAO().deleteByVenueCategoryId(venueCategory.getId());
				Date currentdate=new Date();
				String venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
				String catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
				CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
				csvaudit.setUsername(username);
				csvaudit.setAction(" Error : Invalid line: " + lineCount + ": "
						+ line);
				csvaudit.setBuilding(venueStr);
				csvaudit.setZonesgroup(catScheme);
				csvaudit.setCreatedDate(currentdate);
				csvaudit.setLinecount(lineCount);
				try{
				DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
				}catch(Exception ee){ee.printStackTrace();}
				return mav;
			}
		}
		reader.close();
		Collection<Category> removeCatList = catMap.values();
		DAORegistry.getCategoryDAO().deleteAll(removeCatList);
		
//		List<Event> events = DAORegistry.getEventDAO().getAllEventsByVenueCategoryId(venueCategory.getId());
//		if(events!=null && !events.isEmpty()){
//			DAORegistry.getTmatDependentEventDAO().saveorUpdateTMATDependentEvent(events);
//		}
//		if(catList!= null && !catList.isEmpty()){
//			DAORegistry.getCategoryDAO().saveAll(catList);
//		}
		/*for (Category category : DAORegistry.getCategoryDAO().getEmptyCategoryByTourId(tourId)){
			if(category != null) {
				DAORegistry.getCategoryDAO().delete(category);
			}
		}
		
		if(mappings.size() > 0){
			ArrayList<Integer> eventIds = new ArrayList<Integer>();
			eventIds.addAll(mappings.keySet());
			for(Integer mapEventId : eventIds){
				DAORegistry.getCategoryMappingDAO().saveOrUpdateAll(mappings.get(mapEventId));
			}
			if(tourId != null) {
				Categorizer.update(tourId);
			} else {
				Categorizer.update(events.iterator().next().getTourId());
			}
		}*/
		String venueStr = null;
		if(lineTokens.length > 0) {
			venueStr = TextUtil.trimQuotes(lineTokens[0].toUpperCase());
		} else {
			venueStr = commonVenue;
		}
		
		String catScheme = null;
		if(lineTokens.length > 5) {
			catScheme = TextUtil.trimQuotes(lineTokens[5].toUpperCase());
		} else {
			catScheme = commonCategory;
		}
		mav.addObject( "info", "CSV updated successfully.");
		System.out.println("username : "+username);
		//String venueStr = commonVenue;// TextUtil.trimQuotes(lineTokens[0].toUpperCase());
		System.out.println("venueStr : "+venueStr);
		//String catScheme = commonCategory;//TextUtil.trimQuotes(lineTokens[5].toUpperCase());
		System.out.println("catScheme : "+catScheme);
		Date currentdate=new Date();
		System.out.println("currentdate : "+currentdate);
		System.out.println("IntialLineCount : "+intialLineCount);
		System.out.println("After Upload Linecount : "+lineCount);
		CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
		csvaudit.setUsername(username);
		if(intialLineCount==0)
		{
			csvaudit.setAction("Added");
		}
		else{
			csvaudit.setAction("Updated");
		}
		csvaudit.setBuilding(venueStr);
		csvaudit.setZonesgroup(catScheme);
		csvaudit.setCreatedDate(currentdate);
		csvaudit.setLinecount(lineCount);
		csvaudit.setInitialLineCount(intialLineCount);
		try{
		DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
		}catch(Exception e){e.printStackTrace();}
		return mav;
	}
	
	public ModelAndView loadEditorManageCsvAudit(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer venueId = null;
		String venueIdStr = request.getParameter("venueIdStr");
		System.out.println("venueIdStr : "+venueIdStr);
		String catScheme = request.getParameter("catScheme");
		System.out.println();
		String info = request.getParameter("info");
		String audit = request.getParameter("audit");
		System.out.println("audit : "+audit);
		
		ModelAndView mav = new ModelAndView("page-editor-csvaudit");
		/*if(venId != null && !venId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venId;
		}else if(tId != null && !tId.isEmpty()){
			isVenue = "EditorManageTCAP?tourId="+tId;
		}*/
		String zoneCatGroup="";
		Collection<Event> eventList = null;
		List<CsvUploadRemoveAudit> place=null;
		if (venueIdStr != null) {
			try{
			venueId = Integer.valueOf(venueIdStr);
			}catch(Exception e){e.printStackTrace();}
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
			if(audit!=null && audit.equalsIgnoreCase("true")){
				try{
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venueId, catScheme);
				//DAORegistry.getVenueCategoryDAO().delete(venueCategory);
				String building=null;
				
			     building=venueCategory.getVenue().getBuilding();
				
			     place = DAORegistry.getCsvUploadRemoveAuditDAO().getAllByVenueAndCategoryGroup(building, catScheme);
				}catch(Exception e){e.printStackTrace();}
				info="Zones Audit";
			}
			mav.addObject("venueId", venueId);
			try{
			eventList = DAORegistry.getEventDAO().getAllEventsByVenue(venueId);
			}
			catch(Exception e){e.printStackTrace();}
		}
		
		List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venueId);		
		mav.addObject("catGroupName", catScheme);
		mav.addObject("info", info);
		mav.addObject("zoneCatGroup",zoneCatGroup);
		mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
		mav.addObject("place",place);
		mav.addObject("events", eventList);
		mav.addObject("catGroups", venueCategories);
		return mav;
	}
	
	public ModelAndView loadEditorVenueMapStatusUpdater(
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, Exception {
		Integer venueId = null;
		String venueIdStr = request.getParameter("venueId");
		String selecteCatScheme = request.getParameter("catScheme");
		String action = request.getParameter("action");
		String processType = request.getParameter("processType");
		
		ModelAndView mav = new ModelAndView("page-editor-venu-map-ststus-updater");
		/*if(venId != null && !venId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venId;
		}else if(tId != null && !tId.isEmpty()){
			isVenue = "EditorManageTCAP?tourId="+tId;
		}*/
		if (venueIdStr != null && !venueIdStr.isEmpty()) {
			venueId = Integer.valueOf(venueIdStr);
		}
		String info = null;
		
		if(processType != null && action != null && action.equals("update")) {
			
			if(processType.equals("All")) {
				
				if(!VenueMapUtil.isRunning()) {
					VenueMapUtil.updateVenueMapStatus();
				} else {
					info = "Job is Running Now. Please try again some time later.";
				}
				
			} else if(processType.equals("Venue")) {
				List<String> categoryGroups = new ArrayList<String>();
				
				Map<String, String[]> requestParams = request.getParameterMap();
				for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
					String key = entry.getKey();
					if(key.contains("checkbox_")){
						String[] keyArr =  key.replace("checkbox_", "").split("_");
						Integer catsVenueId = Integer.parseInt(keyArr[0]);
						String categoryGroup = key.replace("checkbox_"+catsVenueId+"_", "");;
						System.out.println("catsVenueId : "+catsVenueId+" : "+categoryGroup);
						//temp = StringUtil.removeFromArray(temp,eventId.toString());
						categoryGroups.add(categoryGroup);
					}
				}
				if(categoryGroups != null && !categoryGroups.isEmpty()) {
					VenueMapUtil.updateVenueMapStatusByVenueAndCategoryGroup(venueId, categoryGroups);
				}
			}
			if(info == null) {
				info = "Updated Successfully.";
			}
		}
		
		if (venueId != null) {
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			
			List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venueId);
			mav.addObject("venueCategoryList", venueCategories);
			mav.addObject("venueId", venueId);
			mav.addObject("venueName", venue.getBuilding()+", "+venue.getCity()+", "+venue.getState()+", "+venue.getCountry());
		}
		mav.addObject("lastProcessedTime", VenueMapUtil.getLastUpdateTimeStr());
		mav.addObject("nextProcessTime", VenueMapUtil.getNextRunTimeStr());
		mav.addObject("isRunningNow", VenueMapUtil.isRunning());
		
				
		//mav.addObject("catGroupName", catScheme);
		mav.addObject("info", info);
		mav.addObject("processType", processType);
		//mav.addObject("zoneCatGroup",zoneCatGroup);
		//mav.addObject("venues", DAORegistry.getVenueDAO().getAll());
		//mav.addObject("place",place);
		//mav.addObject("events", eventList);
		
		return mav;
	}
	/**
	 * Return a CSV file containing all the category mappings for the tour
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public ModelAndView loadEditorDownloadCategoryMappingCsvFilePage(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		try {
		Integer venueId = null;
		String categoryGroup = request.getParameter("categoryGroup");
		try {
			venueId = Integer.parseInt(request.getParameter("venueId"));
		} catch (Exception e) {
			return new ModelAndView(new RedirectView(
					"EditorEditCategories?tourId="
							+ request.getParameter("tourId")));
		}
		Venue venue = DAORegistry.getVenueDAO().get(venueId);

		DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
//		DateFormat timeFormat = new SimpleDateFormat("HH:mm");
		String today = format.format(new Date());
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=category_mapping_" + URLEncoder.encode(venue.getBuilding()) + "_" +categoryGroup+"_"
						+ today + ".csv");
		int lineCount=0;
//		response.getOutputStream().write("[unused field], event date, event time, building, sections, rows, category\n".getBytes());
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(venueId, categoryGroup);
		for(Category category:categories){
			for (CategoryMapping mapping : DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(category.getId())) {
//				Event event = DAORegistry.getEventDAO().get(
//						mapping.getEventId());
//				Category category = DAORegistry.getCategoryDAO().get(
//						mapping.getCategoryId());
				String sectionStr;
				if (mapping.getStartSection().equals(mapping.getEndSection())) {
					sectionStr = mapping.getStartSection().replace("-", "%MINUS%");
				} else {
					sectionStr = mapping.getStartSection().replace("-", "%MINUS%") + "-"
							+ mapping.getEndSection().replace("-", "%MINUS%");
				}

				String rowStr;
				if (mapping.getStartRow().equals(mapping.getEndRow())) {
					rowStr = mapping.getStartRow().replace("-", "%MINUS%");
				} else {
					rowStr = mapping.getStartRow().replace("-", "%MINUS%") + "-" + mapping.getEndRow().replace("-", "%MINUS%");
				}
				
				String seatStr;
				if (mapping.getStartSeat().equals(mapping.getEndSeat())) {
					seatStr = mapping.getStartSeat().replace("-", "%MINUS%");
				} else {
					seatStr = mapping.getStartSeat().replace("-", "%MINUS%") + "-" + mapping.getEndSeat().replace("-", "%MINUS%");
				}

				String categoryStr = category.getSymbol();
//				String dateStr = "TBD";
/*				if (event.getLocalDate() != null) {
					dateStr = format.format(event.getLocalDate());
				}
				String timeStr = "TBD";
				if (event.getTime() != null) {
					timeStr = timeFormat.format(event.getTime());
				}
*/				
				String equalCats = category.getEqualCats();
				if (equalCats == null) {
					equalCats = "";
				}
//				String line = venue.getId() + ", "
//						+ dateStr + ", "
//						+ timeStr + ", "
				String line =  venue.getBuilding() + ", " +
						((sectionStr != null)?sectionStr : "" )+", " + 
						((rowStr != null)?rowStr : "") +", " + 
						((seatStr != null)?seatStr : "") +", " + 
						((categoryStr != null)?categoryStr : "") +", " +
						((category.getGroupName() != null)?category.getGroupName() : "") +", " +
						((mapping.getLarrySection() != null)?mapping.getLarrySection() : "") +", " +
						((equalCats != null)?equalCats : "") +", " +
						((category.getNormalizedZone() != null)?category.getNormalizedZone() : "") +", " +
						((mapping.getRowRange() != null)?mapping.getRowRange() : "") +", " +
						((mapping.getLastRow() != null)?mapping.getLastRow() : "") +", " +
						((mapping.getAlternateRow() != null)?mapping.getAlternateRow() : "") +", " +
						((mapping.getVipLastRow() != null)?mapping.getVipLastRow() : "") +" " +
						"\n";
				response.getOutputStream().write(line.getBytes());
				lineCount++;
			}	
		}
		
		if(lineCount== 0) {
			System.out.println("CSV Download with zero lines : "+lineCount+" : "+venueId+" : "+categoryGroup+" : "+new Date());
		}
		//try{
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Date currentdate=new Date();
		CsvUploadRemoveAudit csvaudit=new CsvUploadRemoveAudit();
		csvaudit.setUsername(username);
		csvaudit.setAction("DOWNLOAD");
		csvaudit.setBuilding(venue.getBuilding());
		csvaudit.setZonesgroup(categoryGroup);
		csvaudit.setCreatedDate(currentdate);
		csvaudit.setInitialLineCount(lineCount);
		csvaudit.setLinecount(lineCount);
		DAORegistry.getCsvUploadRemoveAuditDAO().save(csvaudit);
		
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return null;
	}

//	public TicketListingCrawlDispatcher getTicketListingCrawlDispatcher() {
//		return ticketListingCrawlDispatcher;
//	}
//
//	public void setTicketListingCrawlDispatcher(
//			TicketListingCrawlDispatcher ticketListingCrawlDispatcher) {
//		this.ticketListingCrawlDispatcher = ticketListingCrawlDispatcher;
//	}
//
//	/**
//	 * Method invoked after the bean has been instanciated and all properties injected by spring
//	 */
	public void afterPropertiesSet() throws Exception {
//		if (ticketListingCrawlDispatcher == null) {
//			throw new Exception("The property ticketListingCrawlDispatcher must be set");
//		}
	}
	
	
	public void fillValidSection(){
		String[] sectionGroup = "A:B,B:C,C:D,D:E,E:F,F:G,G:H,H:I,I:J,J:K,K:L,L:M,M:N,N:O,O:P,P:Q,Q:R,R:S,S:T,T:U,U:V,V:W,W:X,X:Y,Y:Z,Z:--,AA:BB,BB:CC,CC:DD,DD:EE,EE:FF,FF:GG,GG:HH,HH:II,II:JJ,JJ:KK,KK:LL,LL:MM,MM:NN,NN:OO,OO:PP,PP:QQ,QQ:RR,RR:SS,SS:TT,TT:UU,UU:VV,VV:WW,WW:XX,XX:YY,YY:ZZ,ZZ:--,AAA:BBB,BBB:CCC,CCC:DDD,DDD:EEE,EEE:FFF,FFF:GGG,GGG:HHH,HHH:III,III:JJJ,JJJ:KKK,KKK:LLL,LLL:MMM,MMM:NNN,NNN:OOO,OOO:PPP,PPP:QQQ,QQQ:RRR,RRR:SSS,SSS:TTT,TTT:UUU,UUU:VVV,VVV:WWW,WWW:XXX,XXX:YYY,YYY:ZZZ,ZZZ:--".split(",");
		for(String section:sectionGroup){
			if(section!=null && !section.isEmpty()){
				String[] keyValue = section.split(":");
				if(keyValue.length==2){
					validSection.put(keyValue[0], keyValue[1]);
				}
			}
		}
	}
	public static boolean validRange(String start,String end){
//		System.out.println(start.compareTo(end));
		if(start.length()!=end.length()){
			return false;
		}
		int len=0;
		int rem=0;
		for(int i=0;i<start.length();i++){
			if(i==0){
				len=start.charAt(i)-end.charAt(i);
				rem=start.charAt(i)-end.charAt(i);
			}else{
				rem=start.charAt(i)-end.charAt(i);
			}
//			System.out.println(start.charAt(i) + ":" + end.charAt(i));
//			System.out.println(rem+ ":" + len);
			if(rem>0 || len!=rem ){
				return false;
			}
		}
		return true;
	}
	
	public boolean inBetween(String type,String compare,String start,String end){
		int typeCase=0;
		if(type.equals("String")){
			typeCase=1;
		}else{
			typeCase=2;
		}
		switch (typeCase) {
		case 1:
			String current=start;
			while(validSection.containsKey(current)){
				if(current.equalsIgnoreCase(compare)){
					return true;
				}else if(current.equalsIgnoreCase(end)){
					return false;
				}
				current=validSection.get(current);
			}
			return false;
		case 2:
			Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
			Matcher startMatcher=pattern.matcher(start);
			Matcher endMatcher = pattern.matcher(end);
			Matcher compareMatcher = pattern.matcher(compare);
			if(!startMatcher.find() || !startMatcher.find() || !compareMatcher.find()){
				return false;
			}else if(!startMatcher.group(1).equalsIgnoreCase(compareMatcher.group(1)) || !endMatcher.group(1).equalsIgnoreCase(compareMatcher.group(1))){
				return false;
			}else{
				try{
					int startInt = Integer.parseInt(startMatcher.group(2));
					int endInt = Integer.parseInt(endMatcher.group(2));
					int compareInt = Integer.parseInt(compareMatcher.group(2));
					if(compareInt<=endInt && compareInt>=startInt){
						return true;
					}else{
						return false;
					}
				}catch (Exception e) {
					return false;
				}
				
			}
		default:
			return false;
		}
		
	}
	
	public boolean  checkConflict(List<String>rowList,String startRow,String endRow,String startSeat,String endSeat,boolean isMergeSection,boolean isMergeRow){
		if(rowList != null){
			for(String tempLine:rowList){
				String[] tempRows = tempLine.split("-:-");
				String tempRow=tempRows[0];
				String tempSeats=tempRows[1];
				boolean tempIsMergeSection = Boolean.parseBoolean(tempRows[2]);
				boolean tempIsMergeRow = Boolean.parseBoolean(tempRows[3]);
				if(tempIsMergeRow!=isMergeRow && tempIsMergeSection!=isMergeSection){
					continue;
				}
				if(tempRow.equals("*:-:*")){
					return true;
				}else{
					String temp[]= tempRow.split(":-:");
					String tempSeat[]=tempSeats.split(":-:");
					try{ 
//						1-10
						int intStartRow= Integer.parseInt(temp[0]);
						int intEndRow= Integer.parseInt(temp[1]);
						int intCompareStartRow = Integer.parseInt(startRow);
						int intCompareEndRow = Integer.parseInt(endRow);
						if((intStartRow>= intCompareStartRow && intStartRow <= intCompareEndRow)
								||(intEndRow>= intCompareStartRow && intEndRow <= intCompareEndRow)
								||(intCompareStartRow>= intStartRow && intCompareStartRow <= intEndRow)
								||(intCompareEndRow>= intStartRow && intCompareEndRow <= intEndRow)){
							if((tempSeat[0].equals("*") && startSeat.equals("^"))|| (tempSeat[0].equals("^") && startSeat.equals("*"))){
								continue;
							}else if((tempSeat[0].equals("*") || startSeat.equals("*"))|| (tempSeat[1].equals("*") || endSeat.equals("*"))){
								return true;
//								continue;
							}else{
								try{
									int tempStartSeat= Integer.parseInt(tempSeat[0]);
									int tempEndSeat= Integer.parseInt(tempSeat[1]);
									
									int intStartSeat= Integer.parseInt(startSeat);
									int intEndSeat= Integer.parseInt(endSeat);
									if(intEndSeat<intStartSeat){
										return true;
									}else if((intStartSeat<=tempStartSeat && intStartSeat>=tempEndSeat) ||( intEndSeat<=tempStartSeat && intEndSeat>=tempEndSeat)){
										return true;
									}
								}catch (Exception ex) {
									if((tempSeat[0].equals("^") || startSeat.equals("^"))|| (tempSeat[1].equals("^") || endSeat.equals("^"))){
										continue;
									}
									return true;
								}
							}
							
						}
					}catch (Exception e) {
						Pattern pattern = Pattern.compile("^([a-zA-Z]+)(\\d+)$");
						Matcher startRowMatcher=pattern.matcher(temp[0]);
						Matcher startEndMatcher = pattern.matcher(temp[1]);
						
						if(!startRowMatcher.find() || !startEndMatcher.find()){ //A-Z AA-ZZ XYX-XYZ 
							if((validSection.containsKey(temp[0]) && validSection.containsKey(temp[1]))|| (!temp[0].equals(temp[1]))){ // A-Z AA-ZZ
								if(inBetween("String",startRow,temp[0],temp[1]) || inBetween("String",endRow,temp[0],temp[1])){
									
										
									if((tempSeat[0].equals("*") && startSeat.equals("^"))|| (tempSeat[0].equals("^") && startSeat.equals("*"))){
										continue;
									}else if((tempSeat[0].equals("*") || startSeat.equals("*"))|| (tempSeat[1].equals("*") || endSeat.equals("*"))){
										return true;
//										continue;
									}else{
										try{
											int tempStartSeat= Integer.parseInt(tempSeat[0]);
											int tempEndSeat= Integer.parseInt(tempSeat[1]);
											
											int intStartSeat= Integer.parseInt(startSeat);
											int intEndSeat= Integer.parseInt(endSeat);
											if(intEndSeat<intStartSeat){
												return true;
											}else if((intStartSeat>=tempStartSeat && intStartSeat<=tempEndSeat) ||( intEndSeat>=tempStartSeat && intEndSeat<=tempEndSeat)){
												return true;
											}
										}catch (Exception ex) {
											if((tempSeat[0].equals("^") || startSeat.equals("^"))|| (tempSeat[1].equals("^") || endSeat.equals("^"))){
												continue;
											}
											return true;
										}
									}
//									return true;
								}
							}else{ //XYZ-XYZ
								if(startRow.equals(temp[0]) || startRow.equals(temp[1]) || endRow.equals(temp[0]) || endRow.equals(temp[1])){
									return true;
								}
							}
						}else{ // A1-A11
							if(inBetween("AlphaNumeric",startRow,temp[0],temp[1]) || inBetween("AlphaNumeric",endRow,temp[0],temp[1])){
								return true;
							}
							
						}
					}
				}
			}
		}
		return false;
	}
	
	

}
