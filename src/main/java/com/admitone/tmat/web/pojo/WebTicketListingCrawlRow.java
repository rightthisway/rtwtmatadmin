package com.admitone.tmat.web.pojo;

import java.io.Serializable;
import java.util.Date;

import com.admitone.tmat.data.Event;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.CrawlState;

public class WebTicketListingCrawlRow implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int crawlFrequency;
	private boolean automaticCrawlFrequency;
	
	private int nextCrawlWaitingTime;
	private CrawlState crawlState;
	private boolean enabled;
	private Date startCrawl;
	private Date endCrawl;
	private String errorMessage;
	private String siteId;
	private String queryUrl;
	private String extraParameters;
	private Boolean broken;
	
//	private Artist artist;
	private Event event;
	private Venue venue;

	public WebTicketListingCrawlRow(Integer id, String name,
			String siteId,
			Event event, Venue venue,
			int crawlFrequency, boolean automaticCrawlFrequency, int nextCrawlWaitingTime,
			CrawlState crawlState,  boolean enabled, boolean broken, Date startCrawl, Date endCrawl, String errorMessage, String queryUrl,
			String extraParameters) {
		this.id = id;
		this.name = name;
		this.siteId = siteId;
		this.crawlFrequency = crawlFrequency;
		this.automaticCrawlFrequency = automaticCrawlFrequency;
		this.nextCrawlWaitingTime = nextCrawlWaitingTime;		
		this.crawlState = crawlState;
		this.enabled = enabled;
		this.startCrawl = startCrawl;
		this.endCrawl = endCrawl;
		this.errorMessage = errorMessage;
//		this.tour = tour;
		this.event = event;
		this.broken = broken;
		this.queryUrl = queryUrl;
		this.extraParameters = extraParameters;
	}
			
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public CrawlState getCrawlState() {
		return crawlState;
	}
	
	public int getNextCrawlWaitingTime() {
		return nextCrawlWaitingTime;
	}
	
	public Date getStartCrawl() {
		return startCrawl;
	}
	
	public Date getEndCrawl() {
		return endCrawl;
	}
	
	public int getCrawlFrequency() {
		return crawlFrequency;
	}

	/*public Tour getTour() {
		return tour;
	}*/

	public Event getEvent() {
		return event;
	}

	public String getSiteId() {
		return siteId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isBroken() {
		return broken;
	}

	public Venue getVenue() {
		return venue;
	}

	public String getQueryUrl() {
		return queryUrl;
	}

	public String getExtraParameters() {
		return extraParameters;
	}

	public boolean isAutomaticCrawlFrequency() {
		return automaticCrawlFrequency;
	}	
}