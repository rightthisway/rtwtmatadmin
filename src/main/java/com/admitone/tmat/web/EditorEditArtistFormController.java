package com.admitone.tmat.web;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;

public class EditorEditArtistFormController extends SimpleFormController {
	
	
	protected  Object formBackingObject(HttpServletRequest request) {
//		String action = request.getParameter("action");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		if(name !=null && !name.isEmpty()){
			ArtistCommand artist = new ArtistCommand();
			artist.setName(name);
			return artist;
		}else if(id!=null && !id.isEmpty()){
			int artistId = Integer.parseInt(id);
			Artist artist =  DAORegistry.getArtistDAO().get(artistId);
			return new ArtistCommand(artist);
			
		}	
		return new ArtistCommand();
		
	}
		
	
	protected ModelAndView onSubmit(HttpServletRequest request,
	        HttpServletResponse response, Object command, BindException errors) {
		ArtistCommand artistCommand = (ArtistCommand)command;
		Artist artist = new Artist(artistCommand);
		String info;
		Artist art = DAORegistry.getArtistDAO().getArtistByName(artist.getName());
		if(art!=null){
			info = "Artist already exist..";
			return new ModelAndView(new RedirectView("EditorGetArtists?info=" + info));
		}else{
			Collection<Artist> artsitDb = DAORegistry.getArtistDAO().searchArtistsByWord(artist.getName());
			if (artsitDb == null || artsitDb.isEmpty()) {
				DAORegistry.getArtistDAO().saveOrUpdate(artist);
				info = "The artist " + artist.getName() + " has been created";
				return new ModelAndView(new RedirectView("EditorGetArtists?info=" + info + "&id="+artist.getId()+"&name="+ artist.getName()));
			} else {
				info = "Found some matching Artists. Click on contiune to create Artist anyway.";
				ModelAndView mav = new ModelAndView("page-artist-creation-confirmation");
				mav.addObject("info", info);
				mav.addObject("artists", artsitDb);
				mav.addObject("name", artist.getName());
				return mav;
			}
		}
	}				

	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String id =request.getParameter("id");
		String info =request.getParameter("info");
		String forceContinue =request.getParameter("continue");
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		if(forceContinue!=null && forceContinue.equals("true")){
			
		}else{
			if(action != null && action.equals("create")){
				return mav;
			}else if(name != null && !name.isEmpty()){
				Artist art = DAORegistry.getArtistDAO().getArtistByName(name);
				if(art!=null){
					info = "Artist already exist..";
					return new ModelAndView(new RedirectView("EditorManageArtists?submit=Search&info=" + info + "&filter=" + art.getName() + "&action=searched"));
				}else{
					Collection<Artist> artsitDb = DAORegistry.getArtistDAO().searchArtists(name);
					mav.addObject("artists", artsitDb);
				}
			}else if(id!=null && !id.isEmpty()){
				int artistId = Integer.valueOf(id);
				mav.addObject("events", DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId));
			}
			mav.addObject("info", info);
		}
		return mav;
	}

}