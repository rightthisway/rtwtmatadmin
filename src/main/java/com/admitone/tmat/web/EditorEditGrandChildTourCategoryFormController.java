package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.TourCategory;

public class EditorEditGrandChildTourCategoryFormController extends SimpleFormController{
	@Override
	protected Object formBackingObject(HttpServletRequest request)
			throws Exception {
		String action=request.getParameter("action");
		if("create".equalsIgnoreCase(action)){
			return new GrandChildTourCategoryCommand();
		}else{
			String id=request.getParameter("id");
			GrandChildTourCategory graqndChildTourCategory=  DAORegistry.getGrandChildTourCategoryDAO().get(Integer.parseInt(id));
			return new GrandChildTourCategoryCommand(graqndChildTourCategory);
		}
		
	}
	
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		// TODO Auto-generated method stub
		return super.showForm(request, response, errors, controlModel);
	}
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		GrandChildTourCategoryCommand grandChildTourCategoryCommand =(GrandChildTourCategoryCommand) command;
		grandChildTourCategoryCommand.setName(grandChildTourCategoryCommand.getName());
		String info="";
		String tourCatId = request.getParameter("tourCatId");
		GrandChildTourCategory grandChildTourCategory = new GrandChildTourCategory(grandChildTourCategoryCommand);
		GrandChildTourCategory exist= DAORegistry.getGrandChildTourCategoryDAO().getGrandChildTourCategoryByNameAndChildTourCategoryNameAndGrandChildTourCategoryName(grandChildTourCategory.getName(),grandChildTourCategory.getChildTourCategory().getName(),grandChildTourCategory.getChildTourCategory().getTourCategory().getName());
		if(exist!= null){			
			if(exist.getName().equals(grandChildTourCategory.getName())){
				info="Grand Child Category already exist.";
			}
			else{
				DAORegistry.getGrandChildTourCategoryDAO().update(grandChildTourCategory);
				info = "Grand Child Category updated successfully.";
			}
		}else if(tourCatId == null || "".equals(tourCatId)){
			info="Please Select Tour Category.";
		}else{
			try{
				if(grandChildTourCategory.getId()==null){
					DAORegistry.getGrandChildTourCategoryDAO().save(grandChildTourCategory);
					info = "Grand Child Category saved successfully.";
				}else{
					DAORegistry.getGrandChildTourCategoryDAO().update(grandChildTourCategory);
					info = "Grand Child Category updated successfully.";
				}
				
			}catch (Exception e) {
				info="Error occured while saving Tour Category..";
			}
		}
		request.getSession().setAttribute("tourId",tourCatId);
		ModelAndView mav = new ModelAndView("page-editor-edit-grand-child-tour-category");
		Collection<ChildTourCategory> childTourCategoryList = DAORegistry.getChildTourCategoryDAO().getAll();
		mav.addObject("childTourCategoryList", childTourCategoryList);
		mav.addObject("grandChildTourCategory", command);
		mav.addObject("tourCategoryList", DAORegistry.getTourCategoryDAO().getAll());
		mav.addObject("info", info);
		mav.addObject("action","create");
		return mav;
	}
	@Override
	protected Map<String, Collection> referenceData(HttpServletRequest request) throws Exception {
		 Map<String, Collection> map= new HashMap<String, Collection>();
		 //map.put("childTourCategoryList", DAORegistry.getChildTourCategoryDAO().getAll());
		 Collection<ChildTourCategory> childTourCategoryList=null;
		 //Collection<TourCategory> tourCategoryList=null;
		 String id =request.getParameter("id"); 
		 String tourCatId = request.getParameter("tourCatId");
		 if(tourCatId != null && !"".equals(tourCatId)){
			request.getSession().setAttribute("tourId", tourCatId);
			childTourCategoryList = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(Integer.parseInt(tourCatId));
		 }
		
		 if(id!=null){ 
			 map.put("artists", DAORegistry.getArtistDAO().getAllActiveArtistsByGrandChildCategoryId(Integer.parseInt(id)));
		 }
		 map.put("childTourCategoryList",childTourCategoryList);
		 map.put("tourCategoryList", DAORegistry.getTourCategoryDAO().getAll());
		 return map;
	}
		
}
