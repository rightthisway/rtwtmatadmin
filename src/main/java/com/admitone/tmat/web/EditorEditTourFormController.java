package com.admitone.tmat.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.GrandChildTourCategory;
//import com.admitone.tmat.data.Property;
//import com.admitone.tmat.data.TMATDependentTour;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.SectionRowStripper;

public class EditorEditTourFormController extends SimpleFormController {
	
	String isVenue = null;
	@Override
	protected  Object formBackingObject(HttpServletRequest request) {
		/*String action = request.getParameter("action");
		if (action.equals("create")) {
			TourCommand tourCommand = new TourCommand();
			tourCommand.setArtistId(DAORegistry.getArtistDAO().getAll().iterator().next().getId());
			return tourCommand;
		}
		
		int tourId = Integer.parseInt(request.getParameter("id"));
		String tId = request.getParameter("tourId");
		String venId = request.getParameter("venueId");
		if(venId != null && !venId.isEmpty()){
			isVenue = "EditorManageTCAP?venueId="+venId;
		}else if(tId != null && !tId.isEmpty()){
			isVenue = "EditorManageTCAP?tourId="+tId;
		}
		Tour tour = DAORegistry.getTourDAO().get(tourId);
		if(tour.getTourType() != null){
			if(tour.getTourType().equals(TourType.CONCERT.toString())){
				tour.setTourType(TourType.SPORT);
			}
		}*/
		
//		return new TourCommand(tour);
		return null;
	}

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		
		TourCommand tourCommand = (TourCommand)command;
		/*String info ="Tour can't be saved/updated";
		String action=request.getParameter("action");
		if(action==null){
			action="";
		}
		String image = request.getParameter("image");
		String tcap = request.getParameter("tcap");
		System.out.println("IMAGE=" + image);
	//Create a new tour	
		if (tourCommand.getId() == null) {
			Tour tour = new Tour();
			tour.setArtist(DAORegistry.getArtistDAO().get(tourCommand.getArtistId()));
			tour.setName(tourCommand.getName());
			Tour tourDb = DAORegistry.getTourDAO().getTourByName(tourCommand.getName());
			if(tourDb==null){
				tour.setLocation(tourCommand.getLocation());
				tour.setTourType(tourCommand.getTourType());
				tour.setGrandChildTourCategory(DAORegistry.getGrandChildTourCategoryDAO().get(tourCommand.getGrandChildCategory()));
//				if(!tourCommand.getTourType().equals(tour.getTourType())||(tour.getTourType()==null)){
//					Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByTour(tour.getId());
//					if(events != null && !events.isEmpty()) {
//						for(Event event : events){
//							event.setEventType(tourCommand.getTourType());
//							DAORegistry.getEventDAO().update(event);
//						}
//					}
//				} 
				if(tourCommand.getTourCategory()!=null && tourCommand.getTourCategory()!=0 && tourCommand.getChildCategory()!=null && tourCommand.getChildCategory()!=0 && tourCommand.getGrandChildCategory()!=null && tourCommand.getGrandChildCategory()!=0){
					DAORegistry.getTourDAO().save(tour);
					info = "The tour has been created";
				}	
			}else{
				info = "The tour already exists.";
			}
			return new ModelAndView(new RedirectView("EditorManageTours?info=" + info));
		}
		//Update the tour
		else {
			Tour tourDb = DAORegistry.getTourDAO().getTourByName(tourCommand.getName());
			if((tourDb==null || !tourDb.getId().equals(tourCommand.getId())) ||(tourDb.getId().equals(tourCommand.getId()) && action.equalsIgnoreCase("edit"))){
				Tour tour = DAORegistry.getTourDAO().get(tourCommand.getId());
				tour.setArtist(DAORegistry.getArtistDAO().get(tourCommand.getArtistId()));
				tour.setName(tourCommand.getName());
				tour.setLocation(tourCommand.getLocation());
				tour.setGrandChildTourCategory(DAORegistry.getGrandChildTourCategoryDAO().get(tourCommand.getGrandChildCategory()));
				if(tourCommand.getTourCategory()!=null && tourCommand.getTourCategory()!=0 && tourCommand.getChildCategory()!=null && tourCommand.getChildCategory()!=0 && tourCommand.getGrandChildCategory()!=null && tourCommand.getGrandChildCategory()!=0){
				if(!tourCommand.getTourType().equals(tour.getTourType())){
					SectionRowStripper.recomputeTourShortSectionsAndRows(tour.getId());
				}
				}
				tour.setTourType(tourCommand.getTourType());
				if(tourCommand.getTourCategory()!=null && tourCommand.getTourCategory()!=0 && tourCommand.getChildCategory()!=null && tourCommand.getChildCategory()!=0 && tourCommand.getGrandChildCategory()!=null && tourCommand.getGrandChildCategory()!=0){
					DAORegistry.getTourDAO().update(tour);
					info = "The tour has been updated";
				}
	
				if(tcap != null && !tcap.isEmpty()){
					return new ModelAndView(new RedirectView(tcap));
				}	
			}else{
				info = "The tour already exists.";
			}
			
			return new ModelAndView(new RedirectView("EditorDetailTour?tourId=" + tourCommand.getId() + "&info=" + info));
		}*/
		return null;
	}				
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request,
			HttpServletResponse response, BindException errors, Map controlModel)
			throws Exception {
		ModelAndView mav = super.showForm(request, response, errors, controlModel);
		/*System.out.println(request.getParameter("id"));
		mav.addObject("tours", DAORegistry.getTourDAO().getAll());
		mav.addObject("artists", DAORegistry.getArtistDAO().getAllActiveArtists());
		 mav.addObject("fromTcap",isVenue);*/
		return mav;
	}

	protected Map<String, Collection> referenceData(HttpServletRequest request) throws Exception {
		 Map<String, Collection> map= new HashMap<String, Collection>();
		/* map.put("tourCategoryList", DAORegistry.getTourCategoryDAO().getAll());
		 String tourId=request.getParameter("id");
		 int id;
		 GrandChildTourCategory grandChildTourCategory=null;
		 List<ChildTourCategory> childTourCategories=null;
		 List<GrandChildTourCategory> grandChildTourCategories=new ArrayList<GrandChildTourCategory>();
		 if(tourId!=null){
			 try{
				 id=Integer.parseInt(tourId);
				 Tour tour= DAORegistry.getTourDAO().get(id);
				 if(tour!=null){
					 grandChildTourCategory=tour.getGrandChildTourCategory();
					 grandChildTourCategories.add(grandChildTourCategory);
					 int categoryId=grandChildTourCategory.getChildTourCategory().getTourCategory().getId();
					 childTourCategories = DAORegistry.getChildTourCategoryDAO().getChildTourCategoryByTourCategory(categoryId);
					 
				 }
			 }catch(Exception ex){
				 
			 }
		 }
		 
		 map.put("childTourCategoryList", childTourCategories);
		 map.put("grandChildTourCategoryList", grandChildTourCategories);
		 */
		 return map;
	}
}

