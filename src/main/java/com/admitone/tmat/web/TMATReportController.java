package com.admitone.tmat.web;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.http.client.methods.HttpGet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONException;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ActiveEventAndActiveCrawls;
import com.admitone.tmat.data.NewPOSEvents;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.VividSeatsTicketInventory;
import com.admitone.tmat.pojo.ClientBroker;
import com.admitone.tmat.pojo.Invoice;
import com.admitone.tmat.pojo.POSTicketGroup;
import com.admitone.tmat.pojo.PosEvent;
import com.admitone.tmat.pojo.PosTicket;
import com.admitone.tmat.pojo.RTFTicketGroup;
import com.admitone.tmat.pojo.SVGFileMissingZoneData;
import com.admitone.tmat.pojo.ShortLongTicketGroup;
import com.admitone.tmat.pojo.ShortTicketGroup;
import com.admitone.tmat.pojo.TNDEventSalesReportDetail;
import com.admitone.tmat.utils.AllInOneCategorySalesReport;
import com.admitone.tmat.utils.Connections;
import com.admitone.tmat.utils.PosUnsentSoldTicketsReport;
import com.admitone.tmat.utils.TNDRtfNoSaleReportScheduler;
import com.admitone.tmat.utils.TixcityUnfilledOrderSummaryScheduler;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/*
 * Web Controller to handle pages related to the autoPricing
 */
public class TMATReportController extends MultiActionController {

	private MailIdProperty mailIdProperty;
	private SharedProperty sharedProperty;
	
	protected static DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
	
	private static boolean isVividRunningNow = false;
	//private static Date lastReportProcessedat = null;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}

	
	public ModelAndView testReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		ModelAndView mav = new ModelAndView("page-tmat-admin-report");
		return mav;
	}
	
	
	public void downloadRTWPosUnfilledShortswithLongInventory(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
				"attachment; filename=RTWPosUnfilledShortswithPossibleLongInventory." + today
						+ ".xlsx");
		try {
			 List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getPOSAllSoldShortEvents();
			 
			 List<ShortTicketGroup> shortTicketGroups = DAORegistry.getQueryManagerDAO().getPOSAllSoldShortTickets();
			 List<ShortLongTicketGroup> longTicketGroups = DAORegistry.getQueryManagerDAO().getPOSAllUnSoldLongTicketsByEvent();
			 
			 Map<Integer,List<ShortTicketGroup>> shortTicketGroupMap = new HashMap<Integer, List<ShortTicketGroup>>();
			 Map<Integer,List<ShortLongTicketGroup>> longTicketGroupMap = new HashMap<Integer, List<ShortLongTicketGroup>>();
			 
			 for (ShortTicketGroup shortTicketGroup : shortTicketGroups) {
				 List<ShortTicketGroup> shortTickets = shortTicketGroupMap.get(shortTicketGroup.getExchangeEventId());
				 if(shortTickets == null) {
					 shortTickets = new ArrayList<ShortTicketGroup>();
				 }
				 shortTickets.add(shortTicketGroup);
				 shortTicketGroupMap.put(shortTicketGroup.getExchangeEventId(),shortTickets);
			}
			 for (ShortLongTicketGroup longTicketGroup : longTicketGroups) {
				 List<ShortLongTicketGroup> longTickets = longTicketGroupMap.get(longTicketGroup.getExchangeEventId());
				 if(longTickets == null) {
					 longTickets = new ArrayList<ShortLongTicketGroup>();
				 }
				 longTickets.add(longTicketGroup);
				 longTicketGroupMap.put(longTicketGroup.getExchangeEventId(),longTickets);
			}
			 /*Event Name	Event Date	Venue Name		Short Section	Short Row	Short qty	Short Price		
			 Long Section	Long Row	Long Qty	Long Retail Price	Long WholeSale Price	Long Face Price	Long Cost
			 */
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet("Short_Invoice_with_Possible_Long");
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
		    rowhead.createCell((int) j).setCellValue("Event Name");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Date");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("Venue Name");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("Short Broker");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Short Section");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("Short Row");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Short Qty");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Short Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Broker");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Section");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Row");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Qty");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Retail Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Wholesale Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Face Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Long Cost");
		    j++;
		    
		    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			 int rowvariable = 1;
			 for (PosEvent event : posEvents) {
				 
				 shortTicketGroups = shortTicketGroupMap.get(event.getExchangeEventId());
				 longTicketGroups = longTicketGroupMap.get(event.getExchangeEventId());
				 
				 if(null == longTicketGroups || longTicketGroups.isEmpty()){
					 continue;
				 }
				 
				 Map<Integer, ShortTicketGroup> shortMap = new HashMap<Integer, ShortTicketGroup>();
				 int i=1;
				 for (ShortTicketGroup shortTicketGroup : shortTicketGroups) {
					 shortMap.put(i, shortTicketGroup);
					 i++;
				 }
				 Map<Integer, ShortLongTicketGroup> longMap = new HashMap<Integer, ShortLongTicketGroup>();
				 i=1;
				 for (ShortLongTicketGroup longTicketGroup : longTicketGroups) {
					 longMap.put(i, longTicketGroup);
					 i++;
				 }
				 int rowSize = shortTicketGroups.size();
				 if(null != longTicketGroups && !longTicketGroups.isEmpty() &&  longTicketGroups.size() > rowSize){
					 rowSize =longTicketGroups.size();
				 }
				 int eventRow = 0;
				 
				 for (int k = 1; k <= rowSize; k++) {
					
					 ShortTicketGroup shortTicketGroup = shortMap.remove(k);
					 ShortLongTicketGroup longTicketGroup = longMap.remove(k);
					 
					 String eventName ="",venueName="",eventDate="",shortBreak="",shortSection="",shortBroker="",
					 shortRow="",shortQty="",shortSoldPrice="",longBreak="",longSection="",longRow="",longQty="",
					 longRetailsPrice="",longWholPrice="",longFacePrice="",longcost="",longBroker="";
					 
					 if(eventRow == 0){
						 eventName = event.getEventName();
						 venueName = event.getVenueName();
						 //eventDate = xlDateFormat.format(dbDateTimeFormat.parse(event.getEventDateStr()));
						 eventDate = xlDateFormat.format(event.getEventDate());
					 }
					 eventRow++;
					 
					 if(null != shortTicketGroup){
						 shortBroker = shortTicketGroup.getBroker();
						 shortSection = shortTicketGroup.getSection();
						 shortRow = shortTicketGroup.getRow();
						 shortQty = String.valueOf(shortTicketGroup.getQty());
						 shortSoldPrice = String.valueOf(shortTicketGroup.getActualSoldPrice());
					 }
					 
					 if(null != longTicketGroup){
						 longBroker = longTicketGroup.getBroker();
						 longSection = longTicketGroup.getSection();
						 longRow = longTicketGroup.getRow();
						 longQty = String.valueOf(longTicketGroup.getRemainingTicketCount());
						 longRetailsPrice = String.valueOf(longTicketGroup.getRetailPrice());
						 longWholPrice = String.valueOf(longTicketGroup.getWholesalePrice());
						 longFacePrice = String.valueOf(longTicketGroup.getFacePrice());
						 longcost = String.valueOf(longTicketGroup.getCost());
					 }
					 
					int column = 0;
					Row row = ssSheet.createRow(rowvariable);
			        row.createCell((int) column).setCellValue(eventName.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(eventDate.replaceAll(",", "-"));
			        column++;
		        	row.createCell((int) column).setCellValue(venueName.replaceAll(",", "-"));
			    	column++;
			    	row.createCell((int) column).setCellValue(shortBreak);
			    	column++;
			    	
			    	Cell shortBrokerCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortBrokerCell.setCellValue(shortBroker.replaceAll(",", "-"));
			        column++;
			        
			    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortSectionCell.setCellValue(shortSection.replaceAll(",", "-"));
			        column++;
			        
			        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        shortRowCell.setCellValue(shortRow.replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(shortQty.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(shortSoldPrice.replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(longBreak);
			        column++;
			        
			        Cell brokerCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        brokerCell.setCellValue(longBroker.replaceAll(",", "-"));
			        column++;
			        
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(longSection.replaceAll(",", "-"));
			        column++;
			        
			        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(longRow.replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(longQty);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longRetailsPrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longWholPrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longFacePrice);
			        column++;
			        
			        row.createCell((int) column).setCellValue(longcost);
			        column++;
			        rowvariable++;
				}
			}
			 System.out.println("PosUnfilledShortswithLongInventory...completed........"+new Date());
			 workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}
	public void downloadRtwPosUnSentOrderDetails(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
				"attachment; filename=RtwNext15DaysUnSentOrderDetails." + today
						+ ".xlsx");
		try {
			SXSSFWorkbook workbook = PosUnsentSoldTicketsReport.getRtwPosUnsentOrderDetails();
			 System.out.println("RtwNext10DaysUnSentOrderDetails...completed........"+new Date());
			 workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}
	public void downloadTNDRtfNoSaleReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
				"attachment; filename=TND_RTF_NO_SALE_Report." + today
						+ ".xlsx");
		try {
			SXSSFWorkbook workbook = TNDRtfNoSaleReportScheduler.getTNDRtfNoSaleReport();
			 System.out.println("TND_RTF_No_SALE_Report...completed........"+new Date());
			 workbook.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}
	public void generateTixcityUnfilledSummaryReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws Exception {
		
		ServletOutputStream out = response.getOutputStream();
		try {
			String str = TixcityUnfilledOrderSummaryScheduler.processTixcityUnfilledOrderSummary();
			if(str == null) {
				str = "TixCity Unfilled Summary Report Generated Successfully.";
			}
			logger.info("TixCity Unfilled Summary Report...Called........"+new Date());
			
			 out.print(str);
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}
	public ModelAndView inventoryReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		ModelAndView mav = new ModelAndView("page-tmat-admin-posinventory-report");
		mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveBrokers());
		return mav;
	}
	
	public void downloadPosInventoryReport(HttpServletRequest request,
			HttpServletResponse response, ModelMap map) throws JSONException {
		
		    response.setContentType("text/csv");
		    ModelAndView mav = new ModelAndView("page-tmat-admin-posinventory-report");
	        String brokerId=request.getParameter("brokerId"); 
	         
	        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
			String today = dateFormat.format(new Date());
			
			if(brokerId.equals("2")){
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=JKTPosInventoryReport." + today
							+ ".csv");
			}
			else if(brokerId.equals("3")){
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=ROTPosInventoryReport." + today
								+ ".csv");
			} else if(brokerId.equals("5")){
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=RTWPosInventoryReport." + today
								+ ".csv");
			} else if(brokerId.equals("7")){
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=TixCityPosInventoryReport." + today
								+ ".csv");
			} else if(brokerId.equals("10")){
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=RTW-2_PosInventoryReport." + today
								+ ".csv");
			}

			try {
				    ServletOutputStream out = response.getOutputStream();
				    out.write("Autopricing Products,Total Active Tickets,Broadcasted Tickets,Unbrocasted Tickets \n"
						.getBytes());
	    		      
				    List<Object[]> report=null;				 		
					System.out.println("BrokerId:"+brokerId);
					report=DAORegistry.getQueryManagerDAO().getCategoryTicketListingCount(brokerId);
					
					String total="";
					String minicats="";
					String vipMinicats="";
					String lastRowMinictas="";
					String zonedLastRowMinicats="";
					String zonesPricing="";
					String larryLast="";
					String presale="";
					Integer totalActiveListing=0;
					Integer totalBroadcastListing=0;
					Integer totalUnbroadCastListing=0;
					int length=0;
					for(Object[] object:report){
				    	length++;
				    	System.out.println(object);
				    }		
					
					int loop=1;
					int i=0;
					String product []=new String[10];
					Integer totalListing []=new Integer[10];
					Integer broadListing []=new Integer[10];
					
						for(Object[] object:report)
						{
						 
							if (loop <=length/2)
							{
							product[i]=(String )object[2];
							totalListing[i]=(Integer) object[0];
							loop++;
							i++;
							}
							else
							{
								product[i]=(String )object[2];
								broadListing[i]=(Integer)object[0];
								loop++;
								i++;
								
							}
														
						}	
						int val =0;
						for (val=0; val < length/2; val++)
						{
							
							int brod = val+length/2;
							String outString = "";
							
							
							if(product[val].equalsIgnoreCase("MINICATS")){
								Integer unbrodListing=totalListing[val]-broadListing[brod];
								product[val]="MINICATS";
								totalBroadcastListing+=broadListing[brod];
								 totalActiveListing+=totalListing[val];
								 totalUnbroadCastListing+=unbrodListing;
								minicats += product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
							}
							if(product[val].equalsIgnoreCase("VIPMINICATS")){
								Integer unbrodListing=totalListing[val]-broadListing[brod];
								totalActiveListing+=totalListing[val];
								totalBroadcastListing+=broadListing[brod];
								totalUnbroadCastListing+=unbrodListing;
								product[val]="VIPMINICATS";
								vipMinicats = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
							}
							if(product[val].equalsIgnoreCase("LASTROW MINICATS")){
								Integer unbrodListing=totalListing[val]-broadListing[brod];
								totalActiveListing+=totalListing[val];
								totalBroadcastListing+=broadListing[brod];
								totalUnbroadCastListing+=unbrodListing;
								product[val]="LASTROW MINICATS";
								lastRowMinictas = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
							}
							
							if(product[val].equalsIgnoreCase("SCZP")){
								Integer unbrodListing=totalListing[val]-broadListing[brod];
								totalActiveListing+=totalListing[val];
								totalBroadcastListing+=broadListing[brod];
								totalUnbroadCastListing+=unbrodListing;
								product[val]="ZONES PRICING";
								zonesPricing = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
							}
							if(product[val].equalsIgnoreCase("AUTOCAT")){
								Integer unbrodListing=totalListing[val]-broadListing[brod];
								totalActiveListing+=totalListing[val];
								totalBroadcastListing+=broadListing[brod];
								totalUnbroadCastListing+=unbrodListing;
								product[val]="PRESALE AUTOCAT";
								presale = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
							}
							  
								
						//	out.write(outString.getBytes());
						}
						
						if(minicats.isEmpty()){
							product[val]="MINICATS";					
							minicats= product[val] + "," +0+ "," +0+"," +0+ "\n";	
						}
						if(vipMinicats.isEmpty()){
							product[val]="VIPMINICATS";					
							vipMinicats= product[val] + "," +0+ "," +0+"," +0+ "\n";	
						}
						if(lastRowMinictas.isEmpty()){
							product[val]="LASTROW MINICATS";					
							lastRowMinictas= product[val] + "," +0+ "," +0+"," +0+ "\n";	
						}
						if(zonesPricing.isEmpty()){
							product[val]="ZONES PRICING";					
							zonesPricing= product[val] + "," +0+ "," +0+"," +0+ "\n";	
						}						
						if(presale.isEmpty()){
							product[val]="PRESALE AUTOCAT";					
							presale= product[val] + "," +0+ "," +0+"," +0+ "\n";	
						}
						
					     out.write(minicats.getBytes());
					     out.write(vipMinicats.getBytes());
					     out.write(lastRowMinictas.getBytes());			
					     out.write(presale.getBytes());
					   
				 report=DAORegistry.getQueryManagerDAO().getTicketGroupListingCount(brokerId);
				 length=0;
				 
				for(Object[] object:report){
			    	length++;
			    }		
				
				 loop=1;
				 i=0;
				
					for(Object[] object:report)
					{
					 
						if (loop <=length/2)
						{
						product[i]=(String )object[2];
						totalListing[i]=(Integer) object[0];
						loop++;
						i++;
						}
						else
						{
							product[i]=(String )object[2];
							broadListing[i]=(Integer)object[0];
							loop++;
							i++;
							
						}
													
					}	
					 val =0;
					for (val=0; val < length/2; val++)
					{
						int brod = val+length/2;
						
						if(product[val].equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")){
							Integer unbrodListing=totalListing[val]-broadListing[brod];
							totalActiveListing+=totalListing[val];
							totalBroadcastListing+=broadListing[brod];
							totalUnbroadCastListing+=unbrodListing;
							product[val]="ZONEDLASTROW MINICATS";
							zonedLastRowMinicats = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
						}
						if(product[val].equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")){
							Integer unbrodListing=totalListing[val]-broadListing[brod];
							totalActiveListing+=totalListing[val];
							totalBroadcastListing+=broadListing[brod];
							totalUnbroadCastListing+=unbrodListing;
							product[val]="LARRYLAST";
							larryLast = product[val] + "," +totalListing[val]+ "," + broadListing[brod] +"," + unbrodListing + "\n";
						}
						
						//out.write(outString.getBytes());
					}
					if(zonedLastRowMinicats.isEmpty()){
						zonedLastRowMinicats= "ZONEDLASTROW MINICATS" + "," +0+ "," +0+"," +0+ "\n";	
					}
					if(larryLast.isEmpty()){
						larryLast= "LARRY LAST" + "," +0+ "," +0+"," +0+ "\n";	
					}
				 total="Total"+","+totalActiveListing+","+totalBroadcastListing+","+totalUnbroadCastListing;
					out.write(zonedLastRowMinicats.getBytes());
					out.write(zonesPricing.getBytes());
					out.write(larryLast.getBytes());
					out.write(total.getBytes());					
					out.close();
			} catch (IOException e) {
				System.out.println(e.fillInStackTrace());
			}
			 
	         mav.addObject("brokers", DAORegistry.getBrokerDAO().getAllActiveBrokers());
	         mav.addObject("brokerId", brokerId);
	
	}	
	public void downloadDuplicateCrawlsReport(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
			response.setContentType("text/csv");
	        response.setHeader("Content-Disposition", "attachment; fileName=eventduplicatecrawls.csv");
			ServletOutputStream out = response.getOutputStream();

			StringBuffer sb = new StringBuffer();
			sb.append("EventId,EventName,EventDate,EventTime,Artist,Venue,City,State,Country,SiteId,QueryUrl,Create Time,Creator \n");
			/*
			List<EventCrawlerDetails> eventCrawlList = DAORegistry.getQueryManagerDAO().getEventsWithDuplicateCrawls();
			System.out.println("Count is -> "+eventCrawlList.size());
			for(EventCrawlerDetails event : eventCrawlList){
				sb.append(event.getEventId()+","+event.getEventName().replaceAll(",", "-") + "," + event.getEventDate()+ "," + event.getEventTime() + "," +
						event.getVenueName().replaceAll(",", "-") +","+ event.getTourName().replaceAll(",", "-") +","+ event.getCrawlCount() +","+ 
						event.getActiveCrawlSites().replaceAll(",", "-")+"\n");
			}
			
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();
			*/
			
			
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getEventsWithDuplicateCrawls();
		
			
			if(list!=null && !list.isEmpty()){
			for(Object[] event : list){
				
				for(int i=0 ; i<event.length ;i++){
					if(event[i] == null){
						event[i] = "";
					}
				}
				
				sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")+
						"," + event[3].toString().replaceAll(",", "-") + "," +
						event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-") +","+ event[6].toString().replaceAll(",", "-") +","+ 
						event[7].toString().replaceAll(",", "-")+","+event[8].toString().replaceAll(",", "-")+","+event[9].toString().replaceAll(",", "-")+","+event[10].toString().replaceAll(",", "-")+
						","+event[11].toString().replaceAll(",", "-")+","+event[12].toString().replaceAll(",", "-")+"\n");
			}
			}
			
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();
			
			//List<Object[]> list2 = DAORegistry.getQueryManagerDAO().getEventsWithZeroCrawls();
			//System.out.println("size is ->>>> "+list2.size());
			
		}
		
		public void downloadZeroCrawlsReport(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
				response.setContentType("text/csv");
		        response.setHeader("Content-Disposition", "attachment; fileName=eventsWithZeroCrawls.csv");
				ServletOutputStream out = response.getOutputStream();

				StringBuffer sb = new StringBuffer();
				sb.append("EventId,EventName,EventDate,EventTime,Venue,Artist \n");
				
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getEventsWithZeroCrawls();
				//System.out.println("size is ->>>> "+list.size());
				
				if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")+ "\n");
				}
				}
				
				out.write(sb.toString().getBytes());
				out.flush();
				out.close();
			
		}
		
		public void downloadZeroCrawlsReportForShTevoTnow(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
				response.setContentType("text/csv");
		        response.setHeader("Content-Disposition", "attachment; fileName=eventsWithZeroCrawlsForShTevoTnow.csv");
				ServletOutputStream out = response.getOutputStream();

				StringBuffer sb = new StringBuffer();
				sb.append("EventId,EventName,EventDate,EventTime,Venue,Artist,SH,TEVo,TNOW,Vivid,FlashSeat \n");
				
				List<Object[]> list = DAORegistry.getQueryManagerDAO().geteventsWithZeroCrawlsForShTevoTnow();
				//System.out.println("size is ->>>> "+list.size());
				
				if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = ""; 
						}
					}
					
					sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")+ ","+"No"+","+"No"+","+"No"+","+"No"+","+"No"+ "\n");
				}
				}
				
				out.write(sb.toString().getBytes());
				out.flush();
				out.close();
			
		}
		
		public ModelAndView loadBrokerEventsReport(HttpServletRequest request,
				HttpServletResponse response, ModelMap map) throws IOException{
			ModelAndView mav = new ModelAndView("page-tmat-admin-broker-event-report");
			String brokerId = request.getParameter("brokerId");
			String brokerName="";
			
			if(brokerId == null) return new ModelAndView("page-tmat-admin-report");
			else if(brokerId.equals("2")) brokerName = "JKT";
			else if(brokerId.equals("3")) brokerName = "ROT";
			else if(brokerId.equals("5")) brokerName = "TIXCITY";
			
			//System.out.println(brokerName);
			//map.put("brokerName", brokerName);
			Date date = new Date();
			
			mav.addObject("brokerName", brokerName);
			mav.addObject("fromDate", "1");
			mav.addObject("fromMonth", "1");
			mav.addObject("fromYear", "2015");
			mav.addObject("toDate", date.getDate());
			mav.addObject("toMonth", date.getMonth()+1);
			mav.addObject("toYear", date.getYear()+1900);
			//tn.indux.db.url com.admitone.tmat.jdbc.url
			return mav;
			//return "page-tmat-admin-broker-event-report";
		}
		
		public void downloadBrokerEventsReport(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
			
			String brokerName = request.getParameter("brokerName");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String fromDate = fromMonth+"/"+fromDay+"/"+fromYear;
			String toDate = toMonth+"/"+toDay+"/"+toYear;
			
			System.out.println("From Date is "+fromDate);
			System.out.println("To Date is "+toDate);
			
			if(brokerName.equals("JKT")) downloadJKTEventsReport(request , response , fromDate , toDate);
			else if(brokerName.equals("ROT")) downloadROTEventsReport(request , response , fromDate , toDate);
			else if(brokerName.equals("TIXCITY")) downloadTixCityEventsReport(request , response , fromDate , toDate);
			
		}
		
		public void downloadJKTEventsReport(HttpServletRequest request,
				HttpServletResponse response, String fromDate, String toDate) throws IOException{
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; fileName=JKTEvents.csv");
			ServletOutputStream out = response.getOutputStream();
			
			StringBuffer sb = new StringBuffer();
			sb.append("Event Id,Event Name,Event Date,Event Time,Parent Category,Child Category,Grand Child Category,Venue,Artist," +
					"Auto Cats TN,Lastrow Mini Cats TN,Mini Cats TN,Vip Auto Cats TN,Vip Mini Cats TN,Mini Cats Vivid,Vip Mini Cats Vivid," +
					"Vip Auto Cats Vivid,Auto Cats Vivid,Vip Mini Cats ScoreBig,Vip Auto Cats ScoreBig,Auto Cats ScoreBig,Mini Cats ScoreBig \n");
			
			// broker id for JKT is 2
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getBrokerEventsInTmat("2",fromDate,toDate);
			//System.out.println("JKT events count is "+list.size());
			
			if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")
							+","+ event[6].toString().replaceAll(",", "-")+","+ event[7].toString().replaceAll(",", "-")+","+ event[8].toString().replaceAll(",", "-")+","+
							event[9].toString().replaceAll(",", "-")+","+event[10].toString().replaceAll(",", "-")+","+event[11].toString().replaceAll(",", "-")
							+","+event[12].toString().replaceAll(",", "-")+","+event[13].toString().replaceAll(",", "-")+
							","+event[14].toString().replaceAll(",", "-")+","+event[15].toString().replaceAll(",", "-")+","+event[16].toString().replaceAll(",", "-")
							+","+event[17].toString().replaceAll(",", "-")+","+event[18].toString().replaceAll(",", "-")+
							","+event[19].toString().replaceAll(",", "-")+","+event[20].toString().replaceAll(",", "-")+","+event[21].toString().replaceAll(",", "-")+"\n");
				}
			}
			
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();
		}
		
		public void downloadROTEventsReport(HttpServletRequest request,
				HttpServletResponse response, String fromDate, String toDate) throws IOException{
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; fileName=ROTEvents.csv");
			ServletOutputStream out = response.getOutputStream();
			
			StringBuffer sb = new StringBuffer();
			sb.append("Event Id,Event Name,Event Date,Event Time,Parent Category,Child Category,Grand Child Category,Venue,Artist," +
					"Auto Cats TN,Lastrow Mini Cats TN,Mini Cats TN,Vip Auto Cats TN,Vip Mini Cats TN,Mini Cats Vivid,Vip Mini Cats Vivid," +
					"Vip Auto Cats Vivid,Auto Cats Vivid,Vip Mini Cats ScoreBig,Vip Auto Cats ScoreBig,Auto Cats ScoreBig,Mini Cats ScoreBig \n");
			
			// broker id for ROT is 3
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getBrokerEventsInTmat("3",fromDate,toDate);
			//System.out.println("ROT events count is "+list.size());
			
			if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")
							+","+ event[6].toString().replaceAll(",", "-")+","+ event[7].toString().replaceAll(",", "-")+","+ event[8].toString().replaceAll(",", "-")+","+
							event[9].toString().replaceAll(",", "-")+","+event[10].toString().replaceAll(",", "-")+","+event[11].toString().replaceAll(",", "-")
							+","+event[12].toString().replaceAll(",", "-")+","+event[13].toString().replaceAll(",", "-")+
							","+event[14].toString().replaceAll(",", "-")+","+event[15].toString().replaceAll(",", "-")+","+event[16].toString().replaceAll(",", "-")
							+","+event[17].toString().replaceAll(",", "-")+","+event[18].toString().replaceAll(",", "-")+
							","+event[19].toString().replaceAll(",", "-")+","+event[20].toString().replaceAll(",", "-")+","+event[21].toString().replaceAll(",", "-")+"\n");
				}
			}
			
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();
		}
		

		public void downloadTixCityEventsReport(HttpServletRequest request,
				HttpServletResponse response, String fromDate, String toDate) throws IOException{
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; fileName=TixcityEvents.csv");
			ServletOutputStream out = response.getOutputStream();
			
			StringBuffer sb = new StringBuffer();
			sb.append("Event Id,Event Name,Event Date,Event Time,Parent Category,Child Category,Grand Child Category,Venue,Tour," +
					"Auto Cats TN,Lastrow Mini Cats TN,Mini Cats TN,Vip Auto Cats TN,Vip Mini Cats TN,Mini Cats Vivid,Vip Mini Cats Vivid," +
					"Vip Auto Cats Vivid,Auto Cats Vivid,Vip Mini Cats ScoreBig,Vip Auto Cats ScoreBig,Auto Cats ScoreBig,Mini Cats ScoreBig \n");
			
			// broker id for JKT is 2
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getBrokerEventsInTmat("5",fromDate,toDate);
			//System.out.println("JKT events count is "+list.size());
			
			if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(event[0].toString().replaceAll(",", "-")+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")
							+","+ event[6].toString().replaceAll(",", "-")+","+ event[7].toString().replaceAll(",", "-")+","+ event[8].toString().replaceAll(",", "-")+","+
							event[9].toString().replaceAll(",", "-")+","+event[10].toString().replaceAll(",", "-")+","+event[11].toString().replaceAll(",", "-")
							+","+event[12].toString().replaceAll(",", "-")+","+event[13].toString().replaceAll(",", "-")+
							","+event[14].toString().replaceAll(",", "-")+","+event[15].toString().replaceAll(",", "-")+","+event[16].toString().replaceAll(",", "-")
							+","+event[17].toString().replaceAll(",", "-")+","+event[18].toString().replaceAll(",", "-")+
							","+event[19].toString().replaceAll(",", "-")+","+event[20].toString().replaceAll(",", "-")+","+event[21].toString().replaceAll(",", "-")+"\n");
				}
			}
			
			out.write(sb.toString().getBytes());
			out.flush();
		}
		
		public ModelAndView loadEventsWithoutMapReport(HttpServletRequest request,
				HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-events-without-map-report");
			
			return mav;
		}
		
		public void downloadNoMapEvents(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
			response.setContentType("text/csv");
			
			ServletOutputStream out = response.getOutputStream();
			
			StringBuffer sb = new StringBuffer();
			sb.append("EventName,EventDate,EventTime,Venue,City,State,Country,Zone Group,VenueId,Parent Category,Total Orders \n");
			
			String parentCategoryName = request.getParameter("parentCategory");
			/** Country filter request params**/
			List<String> country = new ArrayList<String>();
			String[] countries = request.getParameterValues("country");
			if(countries != null && !"".equals(countries)){
				for(String value : countries){
					country.add(value);
				}
			}
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getAllActiveTmatEventsByParentCategory(parentCategoryName,country);
			
			//System.out.println("Total size for "+parentCategoryName+" is "+list.size());
			//List<String> mapImageName = new ArrayList<String>();
			
			if(list!=null && !list.isEmpty()){
				//System.out.println(list.size());
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						System.out.println(event[i]);
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					/*File mapFile = new File("C:\\TMATIMAGESFINAL\\"+event[9]+"_"+event[8]+".gif");
					if(mapFile.exists()){
						continue;
					}
					else{
						count++;
						//mapImageName.add(event[6]+"_"+event[5]);
						
						String reason = "";
						if(event[5] == null && event[6] == null){
							reason = "Venue and Zone group not found.";
							event[5] = "";
							event[6] = "";
						}
						else if(event[5] == null && event[6] != null){
							reason = "Zone group not found.";
							event[5] = "";
						}
						else if(event[5] != null && event[6] == null){
							reason = "Venue not found.";
							event[6] = "";
						}
						else reason = "No map found.";
						
						//System.out.println("id is "+event[0]);
						sb.append(event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
								+ "," + event[3].toString().replaceAll(",", "-") + "," +
								event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")+","+ event[6].toString().replaceAll(",", "-")
								+ "," + event[7].toString().replaceAll(",", "-")+ "," + event[8].toString().replaceAll(",", "-")+ "," + event[9].toString().replaceAll(",", "-")
								+ "," + event[10].toString().replaceAll(",", "-")+ "," + event[11].toString().replaceAll(",", "-")+"\n");
					}*/
					
					sb.append(event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")+","+ event[6].toString().replaceAll(",", "-")
							+ "," + event[7].toString().replaceAll(",", "-")+ "," + event[8].toString().replaceAll(",", "-")+ "," + event[9].toString().replaceAll(",", "-")
							+ "," + event[10].toString().replaceAll(",", "-")+ "," + event[11].toString().replaceAll(",", "-")+"\n");
				}
			}
			String csvFileName = "";
			
			if(parentCategoryName.equals("Sports")) csvFileName = "Sports_Events_Without_Map";
			else if(parentCategoryName.equals("Concerts")) csvFileName = "Concerts_Events_Without_Map";
			else if(parentCategoryName.equals("Theater")) csvFileName = "Theater_Events_Without_Map";
			else if(parentCategoryName.equals("Other")) csvFileName = "Other_Events_Without_Map";
			else csvFileName = "Events_Without_Map";
			
			response.setHeader("Content-Disposition", "attachment; fileName="+csvFileName+".csv");
			//System.out.println("Final count is "+count);
			//System.out.println("ArrayList size is "+mapImageName.size());
			out.write(sb.toString().getBytes());
			out.flush();
			out.close();
			
		}
		
		public ModelAndView loadEventsNotInTmatReport(HttpServletRequest request,
				HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-event-not-in-tmat-report");
			
			return mav;
		}
		
		public void downloadEventsNotInTmatReport(HttpServletRequest request,
				HttpServletResponse response) throws IOException {
			response.setContentType("text/csv");
			
			ServletOutputStream out = response.getOutputStream();
			
			String parentCategory = request.getParameter("parentCategory");
			System.out.println("Parent Category is "+parentCategory);
			
			StringBuffer sb = getEventsNotInTmatReport(parentCategory, null);
			response.setHeader("Content-Disposition", "attachment; fileName="+parentCategory+"_EventsNotInTmatButInPOS.csv");
			out.write(sb.toString().getBytes());

			out.flush();
	        out.close();
		}
		
		public void downloadLastTwoDaysEventsNotInTmatReport(HttpServletRequest request, HttpServletResponse response) throws IOException {
			response.setContentType("text/csv");
	        response.setHeader("Content-Disposition", "attachment; fileName=EventsNotInTmatButInPOS.csv");
			ServletOutputStream out = response.getOutputStream();
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			Date date = new Date();
			date.setDate(date.getDate()-1);
			String fromDateTimeStr = df.format(date)+" 00:00:00";	
			
			StringBuffer buffer = getEventsNotInTmatReport(null,fromDateTimeStr);
			out.write(buffer.toString().getBytes());

			out.flush();
	        out.close();
		}
		
		public StringBuffer getEventsNotInTmatReport(String parentCategory,String fromDateTimeStr) throws IOException{
			
			List<NewPOSEvents> list = DAORegistry.getQueryManagerDAO().getAllEventsNotInTmat(parentCategory,fromDateTimeStr);
			
			StringBuffer sb = new StringBuffer();
			sb.append("EventId,Event Created DateAndTime,TMAT ArtistName,EventName,EventDate,Venue,EventTime,City,State,Parent Category,Child Category,GrandChild Category \n");
			/*df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("hh:mm aa");
			DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");*/
			
			if(null != list) {
				for(NewPOSEvents event:list){
					sb.append(event.getId()  + "," + event.getCreatedDateStr() + "," + event.getTmatArtistName().replaceAll(",", "-")+ "," + event.getName().replaceAll(",", "-") + "," + event.getDateStr() + "," + 
					event.getVenue().replaceAll(",", "-") + "," + event.getTimeStr()+","+ event.getCity().replaceAll(",", "-") +","+ event.getState().replaceAll(",", "-") +","+ 
					event.getParentCategory().replaceAll(",", "-")+","+ event.getChildCategory().replaceAll(",", "-")+","+ event.getGrandChildCategory().replaceAll(",", "-")+"\n");
				}
			}
			return sb;
		}
		
		public ModelAndView loadEventsWithoutCSVReport(HttpServletRequest request,
				HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-events-without-csv-report");
			
			Date date = new Date();
			/*Map<String, String> countryMap = new HashMap<String, String>();
			countryMap.put("US&CA", "US & CA");
			countryMap.put("others", "Other countries");*/
			
			mav.addObject("fromDate", "1");
			mav.addObject("fromMonth", "1");
			mav.addObject("fromYear", "2015");
			mav.addObject("toDate", date.getDate());
			mav.addObject("toMonth", date.getMonth()+1);
			mav.addObject("toYear", date.getYear()+1900);
			/*mav.addObject("countries",countryMap);*/
			
			return mav;
		}
		
		public void downloadEventsWithoutCSVReport(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
			response.setContentType("text/csv");
			
			ServletOutputStream out = response.getOutputStream();
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String fromDate = fromMonth+"/"+fromDay+"/"+fromYear;
			String toDate = toMonth+"/"+toDay+"/"+toYear;
			
			/** Country filter request params**/
			List<String> country = new ArrayList<String>();
			String[] countries = request.getParameterValues("country");
			if(countries != null && !"".equals(countries)){
				for(String value : countries){
					country.add(value);
				}
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append("EventId,EventName,EventDate,EventTime,EventCreationDate,ArtistName,Venue,City,State,Country,ParentCategory \n");
			
			String parentCategoryName = request.getParameter("parentCategory");
			List<Object[]> list = DAORegistry.getQueryManagerDAO()
								  .getAllActiveTmatEventsWithoutCSVByParentCategory(parentCategoryName,fromDate,toDate, country);
			
			if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					// to avoid null pointer exception
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(event[0].toString()+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-")
							+ "," + event[8].toString().replaceAll(",", "-") + "," + event[9].toString().replaceAll(",", "-")
							+ "," + event[4].toString().replaceAll(",", "-") +  "," + event[10].toString().replaceAll(",", "-")
							+ "," + event[11].toString().replaceAll(",", "-") + "," + event[12].toString().replaceAll(",", "-")
							+ "," + event[5].toString().replaceAll(",", "-")+"\n");
				}
			}
			
			response.setHeader("Content-Disposition", "attachment; fileName="+parentCategoryName+"_Events_Without_CSV.csv");
			
			out.write(sb.toString().getBytes());
			out.flush();
	        out.close();
		}
		
		public void downloadTmatPOSParentCategoryMissmatchEvents(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment; fileName=ParentCategoryMissmatchEvents.csv");
			ServletOutputStream out = response.getOutputStream();
			
			StringBuffer sb = new StringBuffer();
			sb.append("Event Creation Date and Time,Event Name,Event Date,Event Time,Venue," +
					"TMAT Parent Category,POS Parent Category \n");
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTmatPOSParentCategoryMissmatchEvents();
			DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			
			if(list!=null && !list.isEmpty()){
				for(Object[] event : list){
					
					for(int i=0 ; i<event.length ;i++){
						if(event[i] == null){
							event[i] = "";
						}
					}
					
					sb.append(dateTimeformat.format((Date)event[0])+","+event[1].toString().replaceAll(",", "-") + "," + event[2].toString().replaceAll(",", "-")
							+ "," + event[3].toString().replaceAll(",", "-") + "," +
							event[4].toString().replaceAll(",", "-") +","+ event[5].toString().replaceAll(",", "-")
							+","+ event[6].toString().replaceAll(",", "-") + "\n");
				}
				}
				
				out.write(sb.toString().getBytes());
				out.flush();
				out.close();
		}
		
		public void downloadExchangeEventsWithoutLocalEvents(HttpServletRequest request,
				HttpServletResponse response) throws IOException{
			response.setContentType("text/csv");
			ServletOutputStream out = response.getOutputStream();
			
			String brokerId = request.getParameter("brokerId");
			String fileName = "";
			if(brokerId.equals("2")) fileName = "JKT_ExchangeEventsWithoutLocalEvents";
			if(brokerId.equals("3")) fileName = "ROT_ExchangeEventsWithoutLocalEvents";
			if(brokerId.equals("5")) fileName = "RTW_ExchangeEventsWithoutLocalEvents";
			if(brokerId.equals("10")) fileName = "RTW-2_ExchangeEventsWithoutLocalEvents";
			
			response.setHeader("Content-Disposition", "attachment; fileName="+fileName+".csv");
			
			StringBuffer sb = new StringBuffer();
			sb.append("EventId,Event Created DateAndTime,EventName,EventDate,EventTime,Venue,City,State,Parent Category,Child Category,Grand Child Category \n");
			
			List <NewPOSEvents> list = DAORegistry.getQueryManagerDAO().getExchangeEventsWithoutLocalEvents(brokerId);
			
			if(null != list) {
				for(NewPOSEvents event:list){
					sb.append(event.getId()  + "," + event.getCreatedDateStr() + "," + event.getName().replaceAll(",", "-") + "," + event.getDateStr()+ "," + event.getTimeStr() + "," + 
					event.getVenue().replaceAll(",", "-") +","+ event.getCity().replaceAll(",", "-") +","+ event.getState().replaceAll(",", "-") +","+ 
					event.getParentCategory().replaceAll(",", "-")+","+ event.getChildCategory().replaceAll(",", "-")+","+ event.getGrandChildCategory().replaceAll(",", "-")+"\n");
				}
			}
			out.write(sb.toString().getBytes());

			out.flush();
	        out.close();
		}
		
		public ModelAndView loadCategorySalesReportEntry(HttpServletRequest request,
				HttpServletResponse response){
			ModelAndView mav;
			String brokerId = request.getParameter("brokerId");
			
			if(brokerId.equals("2"))	mav = new ModelAndView("page-tmat-admin-jkt-category-sales-report");
			else if(brokerId.equals("7"))	 mav = new ModelAndView("page-tmat-admin-tixcity-category-sales-report");
			else if(brokerId.equals("10"))	 mav = new ModelAndView("page-tmat-admin-rot-category-sales-report");
			else if(brokerId.equals("4"))	 mav = new ModelAndView("page-tmat-admin-manhattan-category-sales-report");
			else if(brokerId.equals("5"))	 mav = new ModelAndView("page-tmat-admin-rtw-category-sales-report");
			else mav = new ModelAndView("page-tmat-admin-report");
			Date date = new Date();
			
			mav.addObject("fromDate", "1");
			mav.addObject("fromMonth", "1");
			mav.addObject("fromYear", "2015");
			mav.addObject("toDate", date.getDate());
			mav.addObject("toMonth", date.getMonth()+1);
			mav.addObject("toYear", date.getYear()+1900);
			return mav;
		}
		
		public void getRotCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			//String startDate = request.getParameter("startDate")+" 00:00:00";
			//String endDate = request.getParameter("endDate")+" 23:59:59";
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			
			System.out.println("From Date is "+startDate);
			System.out.println("To Date is "+endDate);
			System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getRotCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					System.out.println("ROT Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,vipLastRowCount=0.0,lastRowMinicatsCount = 0.00,sgLastFiveRowCount = 0.00,larryLastCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00/*,scoreBigAutocatsCount = 0.00*/,
					scoreBigLastRowMinicatsCount = 0.00,vividLarryInvoiceCount = 0.00,
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, 
							ticketCityLastRowMiniCatsInvoiceCount=0.00, fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
					
					for(Object[] obj : list){
					
						Integer invoiceId = (Integer) obj[5];
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String catType = (String) obj[12];
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("VIPLR")){
								catType = "TN VIP LASTROW";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("SGK")){
								catType = "SGK";
							}else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("RTWZP") || catType.equalsIgnoreCase("SCZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								catType = "MANUAL";
							}
						}else{
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else{
								catType = "MANUAL";
							}							
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								reportType.equalsIgnoreCase("MANUAL") ||
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") || reportType.equalsIgnoreCase("SGK") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							if(type != null && (type.equalsIgnoreCase("VividCats@reserveonetickets")
									|| type.equalsIgnoreCase("ScoreBig2@Reserveone") || type.equalsIgnoreCase("ScoreBigLarry@Reserveone")
									|| type.equalsIgnoreCase("VividSpecs@reserveonetickets.com"))){
								//System.out.println("Continued loop : "+obj[0] + " : "+type);
								continue;
							}
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals(""))							
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP LASTROW")){
									vipLastRowCount = vipLastRowCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}//else if(catType.equalsIgnoreCase("SGK")){
								//	sgLastFiveRowCount = sgLastFiveRowCount + Double.parseDouble(String.valueOf(obj[10]));
								//}
								else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LARRY")){
									vividLarryInvoiceCount = vividLarryInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMinicatsCount = scoreBigLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN VIP LASTROW(%) : "+Math.round(vipLastRowCount*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								//"\n,SGK LASTFIVEROW(%) : "+Math.round(sgLastFiveRowCount*100/invoiceTotal)+
								"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								"\n,VIVID LARRY(%) : "+Math.round(vividLarryInvoiceCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMinicatsCount*100/invoiceTotal)+
								"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal)+
								"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
						
		}
		
		public void getJktCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			
			System.out.println("From Date is "+startDate);
			System.out.println("To Date is "+endDate);
			System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getJktCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					System.out.println("JKT Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,lastRowMinicatsCount = 0.00,larryLastCount = 0.00,vividLastRowMiniCatsCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00,scoreBigLastRowMinicatsCount = 0.00,/*scoreBigAutocatsCount = 0.00,*/
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, 
					ticketCityLastRowMiniCatsInvoiceCount=0.00, fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
					
					for(Object[] obj : list){
					
						Integer invoiceId = (Integer) obj[5];
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String catType = (String) obj[12];
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
								catType = "VIVID LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
								catType = "SCOREBIG MINICATS";
							}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
								catType = "SCOREBIG AUTO";
							}*/
							else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("ZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								catType = "MANUAL";
							}
						}else{
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
								catType = "VIVID LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
								catType = "SCOREBIG MINICATS";
							}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
								catType = "SCOREBIG AUTO";
							}*/
							else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else{
								catType = "MANUAL";
							}							
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								reportType.equalsIgnoreCase("MANUAL") ||
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							if(type != null && (type.equalsIgnoreCase("Vivid Seats Cats")
									|| type.equalsIgnoreCase("Scorebig2") || type.equalsIgnoreCase("Scorebig3")
									|| type.equalsIgnoreCase("Vivid Seats Specs"))){
								continue;
							}
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals(""))							
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
									vividLastRowMiniCatsCount = vividLastRowMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMinicatsCount = scoreBigLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								"\n,VIVID LASTROW MINICATS(%) : "+Math.round(vividLastRowMiniCatsCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMinicatsCount*100/invoiceTotal)+
								"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal)+
								"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
		public void getRtwCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			//String startDate = request.getParameter("startDate")+" 00:00:00";
			//String endDate = request.getParameter("endDate")+" 23:59:59";
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			
			//System.out.println("From Date is "+startDate);
			//System.out.println("To Date is "+endDate);
			//System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getRtwCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				if(reportType != null && (reportType.equalsIgnoreCase("ALL") || reportType.equalsIgnoreCase("SGK"))){
					List<Object[]> sgList = DAORegistry.getQueryManagerDAO().getSeatGeekCategorySalesReportToday(dateFormat.parse(startDate), dateFormat.parse(endDate));
					if(sgList != null && !sgList.isEmpty()) {
						if(list == null) {
							list = new ArrayList<Object[]>();
						}
						list.addAll(sgList);
					}
				}
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					//System.out.println("TixCity Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,vipLastRow=0.0,lastRowMinicatsCount = 0.00,sgLastFiveRowCount = 0.00,larryLastCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00/*,scoreBigAutocatsCount = 0.00*/,
					scoreBigLastRowMinicatsCount = 0.00,vividLarryInvoiceCount = 0.00,
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, ticketCityLastRowMiniCatsInvoiceCount=0.00,
							fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<String> invoiceNo = new ArrayList<String>();
					
					for(Object[] obj : list){

						String catType = (String) obj[12];
						String invoiceId = null;
						if(catType != null && catType.equals("SGK")) {
							invoiceId = (String) obj[5];//OrderId
						} else {
							invoiceId = ""+(Integer) obj[5];
						}
						
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}*/
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}*/
							//else 
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("VIPLR")){
								catType = "TN VIP LASTROW";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("SGK")){
								catType = "SGK";
							}else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("SCZP") || catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("RTWZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								//catType = "MANUAL";
							}
						}else{
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}							
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								/*reportType.equalsIgnoreCase("MANUAL") ||*/
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") || reportType.equalsIgnoreCase("SGK") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							if(type != null && (type.equalsIgnoreCase("VividCats@reserveonetickets")
									|| type.equalsIgnoreCase("ScoreBig2@Reserveone") || type.equalsIgnoreCase("ScoreBigLarry@Reserveone")
									|| type.equalsIgnoreCase("VividSpecs@reserveonetickets.com"))){
								//System.out.println("Continued loop : "+obj[0] + " : "+type);
								continue;
							}
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals("")) {	
							if(catType.equals("SGK") && obj[15] != null && !obj[15].equals("")) {
								Date eventDate = new Date(((Timestamp) obj[1]).getTime());
								Date eventTime = new Date(((Timestamp) obj[15]).getTime());
								eventDate.setHours(eventTime.getHours());
								eventDate.setMinutes(eventTime.getMinutes());
								excelData.append(String.valueOf(format.format(eventDate)) + ",");
							} else {
								excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
							}
							
						} else {
							excelData.append(",");
						}
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP LASTROW")){
									vipLastRow = vipLastRow + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}//else if(catType.equalsIgnoreCase("SGK")){
								//	sgLastFiveRowCount = sgLastFiveRowCount + Double.parseDouble(String.valueOf(obj[10]));
								//}
								else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LARRY")){
									vividLarryInvoiceCount = vividLarryInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMinicatsCount = scoreBigLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								} else if(catType.equalsIgnoreCase("SGK")){
									sgLastFiveRowCount = sgLastFiveRowCount + Double.parseDouble(String.valueOf(obj[10]));
								}
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							/*if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else*/
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN VIP LASTROW(%) : "+Math.round(vipLastRow*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								"\n,SGK LASTFIVEROW(%) : "+Math.round(sgLastFiveRowCount*100/invoiceTotal)+
								"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								"\n,VIVID LARRY(%) : "+Math.round(vividLarryInvoiceCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMinicatsCount*100/invoiceTotal)+
								"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
								//"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal));
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
						
		}
		
		public void getTixcityCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			//String startDate = request.getParameter("startDate")+" 00:00:00";
			//String endDate = request.getParameter("endDate")+" 23:59:59";
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			
			//System.out.println("From Date is "+startDate);
			//System.out.println("To Date is "+endDate);
			//System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getTixcityCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				/*if(reportType != null && (reportType.equalsIgnoreCase("ALL") || reportType.equalsIgnoreCase("SGK"))){
					List<Object[]> sgList = DAORegistry.getQueryManagerDAO().getSeatGeekCategorySalesReportToday(dateFormat.parse(startDate), dateFormat.parse(endDate));
					if(sgList != null && !sgList.isEmpty()) {
						if(list == null) {
							list = new ArrayList<Object[]>();
						}
						list.addAll(sgList);
					}
				}*/
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					//System.out.println("TixCity Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,vipLastRow=0.0,lastRowMinicatsCount = 0.00,sgLastFiveRowCount = 0.00,larryLastCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00/*,scoreBigAutocatsCount = 0.00*/,
					scoreBigLastRowMinicatsCount = 0.00,vividLarryInvoiceCount = 0.00,
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, ticketCityLastRowMiniCatsInvoiceCount=0.00,
							fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<String> invoiceNo = new ArrayList<String>();
					
					for(Object[] obj : list){

						String catType = (String) obj[12];
						String invoiceId = null;
						if(catType != null && catType.equals("SGK")) {
							invoiceId = (String) obj[5];//OrderId
						} else {
							invoiceId = ""+(Integer) obj[5];
						}
						
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}*/
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}*/
							//else 
							
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else*/ if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("VIPLR")){
								catType = "TN VIP LASTROW";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}/*else if(catType.equalsIgnoreCase("SGK")){
								catType = "SGK";
							}*/else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("SCZP") || catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("RTWZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								//catType = "MANUAL";
							}
						}else{
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}*/							
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								/*reportType.equalsIgnoreCase("MANUAL") ||*/
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") || reportType.equalsIgnoreCase("SGK") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							/*if(type != null && (type.equalsIgnoreCase("VividCats@reserveonetickets")
									|| type.equalsIgnoreCase("ScoreBig2@Reserveone") || type.equalsIgnoreCase("ScoreBigLarry@Reserveone")
									|| type.equalsIgnoreCase("VividSpecs@reserveonetickets.com"))){
								//System.out.println("Continued loop : "+obj[0] + " : "+type);
								continue;
							}*/
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals("")) {	
							/*if(catType.equals("SGK") && obj[15] != null && !obj[15].equals("")) {
								Date eventDate = new Date(((Timestamp) obj[1]).getTime());
								Date eventTime = new Date(((Timestamp) obj[15]).getTime());
								eventDate.setHours(eventTime.getHours());
								eventDate.setMinutes(eventTime.getMinutes());
								excelData.append(String.valueOf(format.format(eventDate)) + ",");
							} else {*/
								excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
							//}
							
						} else {
							excelData.append(",");
						}
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP LASTROW")){
									vipLastRow = vipLastRow + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}//else if(catType.equalsIgnoreCase("SGK")){
								//	sgLastFiveRowCount = sgLastFiveRowCount + Double.parseDouble(String.valueOf(obj[10]));
								//}
								else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								/*else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LARRY")){
									vividLarryInvoiceCount = vividLarryInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMinicatsCount = scoreBigLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								} else if(catType.equalsIgnoreCase("SGK")){
									sgLastFiveRowCount = sgLastFiveRowCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							/*if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else*/
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN VIP LASTROW(%) : "+Math.round(vipLastRow*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								//"\n,SGK LASTFIVEROW(%) : "+Math.round(sgLastFiveRowCount*100/invoiceTotal)+
								//"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								//"\n,VIVID LARRY(%) : "+Math.round(vividLarryInvoiceCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								//"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMinicatsCount*100/invoiceTotal)+
								//"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								//"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								///"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								//"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								//"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
								//"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal));
								"");
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
						
		}
		
// tix city new category sales report
		
		public void getTixCityNewCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			//String startDate = request.getParameter("startDate")+" 00:00:00";
			//String endDate = request.getParameter("endDate")+" 23:59:59";
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			System.out.println("Coming to tixcity new category sales report");
			//System.out.println("From Date is "+startDate);
			//System.out.println("To Date is "+endDate);
			//System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().getTixcityNewCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					//System.out.println("TixCity Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,lastRowMinicatsCount = 0.00,larryLastCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00/*,scoreBigAutocatsCount = 0.00*/,
					scoreBigLastRowMinicatsCount = 0.00,vividLarryInvoiceCount = 0.00,
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, ticketCityLastRowMiniCatsInvoiceCount=0.00,
							fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
					
					for(Object[] obj : list){
					
						Integer invoiceId = (Integer) obj[5];
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String catType = (String) obj[12];
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}*/
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}*/
							//else 
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("ZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								//catType = "MANUAL";
							}
						}else{
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}							
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								/*reportType.equalsIgnoreCase("MANUAL") ||*/
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							if(type != null && (type.equalsIgnoreCase("VividCats@reserveonetickets")
									|| type.equalsIgnoreCase("ScoreBig2@Reserveone") || type.equalsIgnoreCase("ScoreBigLarry@Reserveone")
									|| type.equalsIgnoreCase("VividSpecs@reserveonetickets.com"))){
								//System.out.println("Continued loop : "+obj[0] + " : "+type);
								continue;
							}
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals(""))							
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LARRY")){
									vividLarryInvoiceCount = vividLarryInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMinicatsCount = scoreBigLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							/*if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else*/
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								"\n,VIVID LARRY(%) : "+Math.round(vividLarryInvoiceCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMinicatsCount*100/invoiceTotal)+
								"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
								//"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal));
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
						
		}
		
// manhattan category sales report
		public void getManhattanCategorySalesReport(HttpServletRequest request,HttpServletResponse response) {
			//String startDate = request.getParameter("startDate")+" 00:00:00";
			//String endDate = request.getParameter("endDate")+" 23:59:59";
			String reportType = request.getParameter("reportType");
			String poInfo = request.getParameter("poInfo");
			
			String fromDay = request.getParameter("fromDay");
			String fromMonth = request.getParameter("fromMonth");
			String fromYear = request.getParameter("fromYear");
			
			String toDay = request.getParameter("toDay");
			String toMonth = request.getParameter("toMonth");
			String toYear = request.getParameter("toYear");
			
			String startDate = fromMonth+"/"+fromDay+"/"+fromYear+" 00:00:00";
			String endDate = toMonth+"/"+toDay+"/"+toYear+" 23:59:59";
			
			//System.out.println("From Date is "+startDate);
			//System.out.println("To Date is "+endDate);
			//System.out.println("Report Type is "+reportType);
			//System.out.println("po info is "+poInfo);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
			
			try{
				List<Object[]> list = DAORegistry.getQueryManagerDAO().ManhattanCategorySalesReport(dateFormat.parse(startDate), dateFormat.parse(endDate), reportType,poInfo);
				
				// adding dummy data only for testing.
				/*List<Object[]> dummySales = dummySalesData();
				for(int i=0;i<dummySales.size();i++){
					list.add(dummySales.get(i));
				}*/
				
				if(list != null && !list.isEmpty()){
					//System.out.println("Manhattan Records are : "+list.size());
					StringBuilder excelData = new StringBuilder();
					int recordCount=0;
					boolean zoneFlag = false,flag = true;
					double retailPrice=0.00,actualPrice=0.00,invoiceTotal=0.00,poTotal=0.00,autoCount=0.00,manualCount=0.00,vipcatsCount=0.00,
					zoneCount = 0.00,tgCatsCount = 0.00,
					vipAuto = 0.00,lastRowMinicatsCount = 0.00,larryLastCount = 0.00,
					vividMiniCatsCount = 0.00,scoreBigMinicatsCount = 0.00/*,scoreBigAutocatsCount = 0.00*/,
					scoreBigLastRowMiniCatsInvoiceCount = 0.00,vividLarryInvoiceCount = 0.00,
					zonedLastRowMinicatsCount=0.00,zonesCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, ticketCityLastRowMiniCatsInvoiceCount=0.00,
					fanXchangeLastRowMiniCatsInvoiceCount=0.00;
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
					
					for(Object[] obj : list){
					
						Integer invoiceId = (Integer) obj[5];
						if(invoiceNo.contains(invoiceId))
							continue;
						
						invoiceNo.add(invoiceId);
						
						String catType = (String) obj[12];
						String type = (String) obj[13];
						String ebayType = (String) obj[14];
						
						if(catType != null && !catType.isEmpty()){
							/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
								catType = "VIVID MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
								catType = "VIVID LARRY";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
								catType = "SCOREBIG MINICATS";
							}*/
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG AUTO";
							}*/
							
							/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
								catType = "SCOREBIG LASTROW MINICATS";
							}*/
							//else 
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("AUTOCAT")){
								catType = "TN AUTO";								
							}else if(catType.equalsIgnoreCase("MINICATS")){
								catType = "TN MINICATS";
							}else if(catType.equalsIgnoreCase("VIPMINICATS")){
								catType = "TN VIP MINICATS";
							}else if(catType.equalsIgnoreCase("VIPAUTO")){
								catType = "TN VIP AUTOCATS";
							}else if(catType.equalsIgnoreCase("LASTROW MINICATS")){
								catType = "TN LASTROW MINICATS";
							}else if(catType.equalsIgnoreCase("LarryLast NoStub")){
								catType = "TN LARRYLAST";
							}else if(catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("MZP")){
								catType = "ZONES PRICING";
							}else if(catType.equalsIgnoreCase("ZLR NOStub")){
								catType = "ZONED LASTROW MINICATS";
							}else{
								//catType = "MANUAL";
							}
						}else{
							if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
								catType = "TICKET CITY MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
								catType = "TICKET CITY VIPMINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
								catType = "TICKET CITY LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
								catType = "FANXCHANGE LASTROW MINICATS";
							}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
								catType = "SCOREBIG LASTROW MINICATS";
							}					
						}
						if(reportType.equalsIgnoreCase("AUTOCAT") || reportType.equalsIgnoreCase("MINICATS") || reportType.equalsIgnoreCase("VIPMINICATS") ||
								reportType.equalsIgnoreCase("VIPAUTO") || reportType.equalsIgnoreCase("LarryLast NoStub") ||
								/*reportType.equalsIgnoreCase("MANUAL") ||*/
								reportType.equalsIgnoreCase("ZONES PRICING") || reportType.equalsIgnoreCase("LASTROW MINICATS") ||
								reportType.equalsIgnoreCase("ZONED LASTROW MINICATS") || reportType.equalsIgnoreCase("ZONES")){
							if(type != null && (type.equalsIgnoreCase("Ticket City41")
									|| type.equalsIgnoreCase("Ticket City42") || type.equalsIgnoreCase("Ticket City43")
									|| type.equalsIgnoreCase("fanexchange41") || type.equalsIgnoreCase("scorebig41"))){
								//System.out.println("Continued loop : "+obj[0] + " : "+type);
								continue;
							}
						}
						if(obj[0] != null && !obj[0].equals(""))
							excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[1] != null && !obj[1].equals(""))							
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[2] != null && !obj[2].equals("")){
							String section = String.valueOf(obj[2]).toLowerCase();
							if(section.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
						}							
						else{
							excelData.append(",");
						}							
						if(obj[3] != null && !obj[3].equals("")){
							String row = String.valueOf(obj[3]).toLowerCase();
							if(row.contains("zone") && catType.equals("MANUAL"))
								zoneFlag = true;
							excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
						}
						else{
							excelData.append(",");
						}
						if(obj[4] != null && !obj[4].equals(""))
							excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[5] != null && !obj[5].equals(""))
							excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[6] != null && !obj[6].equals(""))
							excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
						else
							excelData.append(",");
						if(obj[7] != null && !obj[7].equals(""))
							excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
						else
							excelData.append(",");
						if(obj[8] != null && !obj[8].equals("")){
							excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
							retailPrice = retailPrice + Double.parseDouble(String.valueOf(obj[8]));
						}
						else{
							excelData.append(",");
						}
						if(obj[9] != null && !obj[9].equals("")){
							excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
							actualPrice = actualPrice + Double.parseDouble(String.valueOf(obj[9]));
						}
						else{
							excelData.append(",");
						}
						if(obj[10] != null && !obj[10].equals("")){
							excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
							invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));									
							if(reportType.equalsIgnoreCase("ALL")){
								if(catType.equalsIgnoreCase("TN AUTO")){
									autoCount = autoCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN MINICATS")){
									tgCatsCount = tgCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
									vipcatsCount = vipcatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
									vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
									lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
									larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(zoneFlag){
									zonesCount = zonesCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONES PRICING")){
									zoneCount = zoneCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
									zonedLastRowMinicatsCount = zonedLastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("MANUAL")){
									manualCount = manualCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/else if(catType.equalsIgnoreCase("VIVID MINICATS")){
									vividMiniCatsCount = vividMiniCatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("VIVID LARRY")){
									vividLarryInvoiceCount = vividLarryInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
									scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
									scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
								}*/
								else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
									scoreBigLastRowMiniCatsInvoiceCount = scoreBigLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
									ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
									ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
									ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
									fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
								}
							}
						}
						else{
							excelData.append(",");
						}
						if(obj[11] != null && !obj[11].equals("")){
							excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
							poTotal = poTotal + Double.parseDouble(String.valueOf(obj[11]));
						}
						else{
							excelData.append(",");
						}
						if(catType != null && !catType.isEmpty()){
							/*if(zoneFlag && catType.equals("MANUAL"))
								excelData.append("ZONE");
							else*/
								excelData.append(catType.replaceAll(",", " "));
						}
						
						excelData.append("\n");	
						zoneFlag = false;
					}
					DecimalFormat df = new DecimalFormat("#.##");
					excelData.append(",,,,,,,Total:-,"+df.format(retailPrice)+","+df.format(actualPrice)+","+df.format(invoiceTotal)+","+df.format(poTotal)+",");
					if(reportType.equalsIgnoreCase("ALL")){
						excelData.append("\n");
						excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoCount*100/invoiceTotal)+
								"\n,TN MINICATS(%) : "+Math.round(tgCatsCount*100/invoiceTotal)+
								"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsCount*100/invoiceTotal)+
								"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
								"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
								"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
								"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
								"\n,VIVID LARRY(%) : "+Math.round(vividLarryInvoiceCount*100/invoiceTotal)+
								/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
								"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
								"\n,ZONES PRICING(%) : "+Math.round(zoneCount*100/invoiceTotal)+
								"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonedLastRowMinicatsCount*100/invoiceTotal)+
								"\n,ZONE CATS(%) : "+Math.round(zonesCount*100/invoiceTotal)+
								"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
								"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
								//"\n,MANUAL CATS(%) : "+Math.round(manualCount*100/invoiceTotal));
					}
					DateFormat dateFormat1 = new SimpleDateFormat("MM-dd-yyyy");
					String fileName = "Category_sales_Report_"+ dateFormat1.format(new Date()) + ".csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}else{
					StringBuilder excelData = new StringBuilder();			
					excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
					excelData.append("\n");
					String fileName = "Category_sales_Report.csv";
					response.setHeader("Content-Type", "application/vnd.ms-excel");
					response.setHeader("Content-disposition", "attachment; filename="+fileName);
					response.getOutputStream().write(excelData.toString().getBytes());
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
						
		}
		
		
  public void downloadTMATStatisticsReport(HttpServletRequest request,
			HttpServletResponse response) {
	  
	   
		String applyDate=request.getParameter("applyDate");
		
		     try{
		
			
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			
			response.setHeader("Content-disposition","attachment; filename=TMATStatisticsReport." + applyDate+ ".csv");
			
			int eventCount=DAORegistry.getEventDAO().getEventCount();
			StringBuilder sb = new StringBuilder();
      
			OutputStream out = response.getOutputStream();
				
				
			 List<ActiveEventAndActiveCrawls> activeEventAndActiveCrawlList=DAORegistry.getActiveEventAndActiveCrawlsDAO().getActiveEventAndActiveCrawl(applyDate);
			  ActiveEventAndActiveCrawls activeEventAndActiveCrawl=null;
			  
			  if(activeEventAndActiveCrawlList!=null){
				  	 
				for (ActiveEventAndActiveCrawls activeEventAndActiveCrawls : activeEventAndActiveCrawlList) {
					activeEventAndActiveCrawl=activeEventAndActiveCrawls;
				}	
			  }
			
				
			  if(activeEventAndActiveCrawl!=null){
				  System.out.println( "***Checck each crawl**" );
				  int totalTnd=activeEventAndActiveCrawl.getTndExchangeCrawlsSports()+activeEventAndActiveCrawl.getTndExchangeCrawlsTheater()+activeEventAndActiveCrawl.getTndExchangeCrawlsConcert()+activeEventAndActiveCrawl.getTndExchangeCrawlsOther();
				  int totalStubHub=activeEventAndActiveCrawl.getShExchangeCrawlsSports()+activeEventAndActiveCrawl.getShExchangeCrawlsTheater()+activeEventAndActiveCrawl.getShExchangeCrawlsConcert()+activeEventAndActiveCrawl.getShExchangeCrawlsOther();
				  int totalTe=activeEventAndActiveCrawl.getTeExchangeCrawlsSports()+activeEventAndActiveCrawl.getTeExchangeCrawlsTheater()+activeEventAndActiveCrawl.getTeExchangeCrawlsConcert()+activeEventAndActiveCrawl.getTeExchangeCrawlsOther();
				  int totalTnow=activeEventAndActiveCrawl.getTnowExchangeCrawlsSports()+activeEventAndActiveCrawl.getTnowExchangeCrawlsTheater()+activeEventAndActiveCrawl.getTnowExchangeCrawlsConcert()+activeEventAndActiveCrawl.getTnowExchangeCrawlsOther();
				  int totalFlseat=activeEventAndActiveCrawl.getFsExchangeCrawlsSports()+activeEventAndActiveCrawl.getFsExchangeCrawlsTheater()+activeEventAndActiveCrawl.getFsExchangeCrawlsConcert()+activeEventAndActiveCrawl.getFsExchangeCrawlsOther();
				  int totalVs=activeEventAndActiveCrawl.getVsExchangeCrawlsSports()+activeEventAndActiveCrawl.getVsExchangeCrawlsTheater()+activeEventAndActiveCrawl.getVsExchangeCrawlsConcert()+activeEventAndActiveCrawl.getVsExchangeCrawlsOther();
				  
				  int totalSport=activeEventAndActiveCrawl.getTndExchangeCrawlsSports()+activeEventAndActiveCrawl.getShExchangeCrawlsSports()+activeEventAndActiveCrawl.getTeExchangeCrawlsSports()+activeEventAndActiveCrawl.getTnowExchangeCrawlsSports()+activeEventAndActiveCrawl.getFsExchangeCrawlsSports()+activeEventAndActiveCrawl.getVsExchangeCrawlsSports();
				  int totalTheater= activeEventAndActiveCrawl.getTndExchangeCrawlsTheater()+ activeEventAndActiveCrawl.getShExchangeCrawlsTheater()+ activeEventAndActiveCrawl.getTeExchangeCrawlsTheater()+ activeEventAndActiveCrawl.getTnowExchangeCrawlsTheater()+activeEventAndActiveCrawl.getFsExchangeCrawlsTheater()+activeEventAndActiveCrawl.getVsExchangeCrawlsTheater();
				  int totalConcert= activeEventAndActiveCrawl.getTndExchangeCrawlsConcert()+ activeEventAndActiveCrawl.getShExchangeCrawlsConcert()+ activeEventAndActiveCrawl.getTeExchangeCrawlsConcert()+ activeEventAndActiveCrawl.getTnowExchangeCrawlsConcert()+activeEventAndActiveCrawl.getFsExchangeCrawlsConcert()+activeEventAndActiveCrawl.getVsExchangeCrawlsConcert();
				  int totalOther= activeEventAndActiveCrawl.getTndExchangeCrawlsOther()+activeEventAndActiveCrawl.getShExchangeCrawlsOther()+activeEventAndActiveCrawl.getTeExchangeCrawlsOther()+activeEventAndActiveCrawl.getTnowExchangeCrawlsOther()+activeEventAndActiveCrawl.getFsExchangeCrawlsOther()+activeEventAndActiveCrawl.getVsExchangeCrawlsOther();
				  int totalCrawls=totalTnd+totalStubHub+totalTe+totalTnow+totalFlseat+totalVs;
				  
				  int tndSportPer=(activeEventAndActiveCrawl.getTndExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int tndTheaterPer=(activeEventAndActiveCrawl.getTndExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int tndConcertPer=(activeEventAndActiveCrawl.getTndExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int tndOtherPer=(activeEventAndActiveCrawl.getTndExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				  int shSportPer=(activeEventAndActiveCrawl.getShExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int shTheaterPer=(activeEventAndActiveCrawl.getShExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int shConcertPer=(activeEventAndActiveCrawl.getShExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int shOtherPer=(activeEventAndActiveCrawl.getShExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				  int teSportPer=(activeEventAndActiveCrawl.getTeExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int teTheaterPer=(activeEventAndActiveCrawl.getTeExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int teConcertPer=(activeEventAndActiveCrawl.getTeExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int teOtherPer=(activeEventAndActiveCrawl.getTeExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				  int tnowSportPer=(activeEventAndActiveCrawl.getTnowExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int tnowTheaterPer=(activeEventAndActiveCrawl.getTnowExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int tnowConcertPer=(activeEventAndActiveCrawl.getTnowExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int tnowOtherPer=(activeEventAndActiveCrawl.getTnowExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				  int flseatSportPer=(activeEventAndActiveCrawl.getFsExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int flseatTheaterPer=(activeEventAndActiveCrawl.getFsExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int flseatConcertPer=(activeEventAndActiveCrawl.getFsExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int flseatOtherPer=(activeEventAndActiveCrawl.getFsExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				  int vsseatSportPer=(activeEventAndActiveCrawl.getVsExchangeCrawlsSports()*100)/activeEventAndActiveCrawl.getActiveEventSports();
				  int vsseatTheaterPer=(activeEventAndActiveCrawl.getVsExchangeCrawlsTheater()*100)/activeEventAndActiveCrawl.getActiveEventTheater();
				  int vsseatConcertPer=(activeEventAndActiveCrawl.getVsExchangeCrawlsConcert()*100)/activeEventAndActiveCrawl.getActiveEventConcert();
				  int vsseatOtherPer=(activeEventAndActiveCrawl.getVsExchangeCrawlsOther()*100)/activeEventAndActiveCrawl.getActiveEventOther();
				  
				
				  
				    String outString = "Total no of Active Events in TMAT ,"+eventCount+"\n";
				    outString += "Sports,Theater,Concert,Other,Total"+"\n";
					outString += activeEventAndActiveCrawl.getActiveEventSports() + ",";
					outString += activeEventAndActiveCrawl.getActiveEventConcert() + ",";
					outString += activeEventAndActiveCrawl.getActiveEventTheater() + ",";
					outString += activeEventAndActiveCrawl.getActiveEventOther() + ",";
					outString +=eventCount+"\n\n";
					
					
					outString += "Total no of Active Crawls in TMAT ,"+totalCrawls+"\n\n";
					outString +="Exchanges,Sports,Theater,Concert,Other,Total"+"\n";			
					outString += "TicketNetWorkDirect,"+activeEventAndActiveCrawl.getTndExchangeCrawlsSports() +"("+tndSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTndExchangeCrawlsTheater() +"("+tndTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getTndExchangeCrawlsConcert() + "("+tndConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTndExchangeCrawlsOther() + "("+tndOtherPer+"%)"+",";
					outString += totalTnd+"\n";
					
					outString += "StubHub,"+activeEventAndActiveCrawl.getShExchangeCrawlsSports() +"("+shSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getShExchangeCrawlsTheater() +"("+shTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getShExchangeCrawlsConcert()+"("+shConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getShExchangeCrawlsOther()+"("+shOtherPer+"%)"+",";
					outString += totalStubHub+"\n";
										
					outString += "Ticket Evaluation,"+activeEventAndActiveCrawl.getTeExchangeCrawlsSports()  +"("+teSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTeExchangeCrawlsTheater() +"("+teTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getTeExchangeCrawlsConcert()+"("+teConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTeExchangeCrawlsOther()+"("+teOtherPer+"%)"+",";
					outString += totalTe+"\n";
					
					outString += "TicketsNow,"+activeEventAndActiveCrawl.getTnowExchangeCrawlsSports()  +"("+tnowSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTnowExchangeCrawlsTheater()+"("+tnowTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getTnowExchangeCrawlsConcert()+"("+tnowConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getTnowExchangeCrawlsOther()+"("+tnowOtherPer+"%)"+",";
					outString += totalTnow+"\n";
					
					outString += "Flash Seat,"+activeEventAndActiveCrawl.getFsExchangeCrawlsSports()  +"("+flseatSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getFsExchangeCrawlsTheater()+"("+flseatTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getFsExchangeCrawlsConcert()+"("+flseatConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getFsExchangeCrawlsOther()+"("+flseatOtherPer+"%)"+",";
					outString += totalFlseat+"\n";
					
					outString += "Vivid  Seat,"+activeEventAndActiveCrawl.getVsExchangeCrawlsSports()  +"("+vsseatSportPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getVsExchangeCrawlsTheater()+"("+vsseatTheaterPer+"%)"+ ",";
					outString += activeEventAndActiveCrawl.getVsExchangeCrawlsConcert()+"("+vsseatConcertPer+"%)"+",";
					outString += activeEventAndActiveCrawl.getVsExchangeCrawlsOther()+"("+vsseatOtherPer+"%)"+",";
					outString += totalVs+"\n";
					
					
					
					outString +="Total,"+totalSport+","+totalTheater+","+totalConcert+","+totalOther+","+totalCrawls;					
					out.write(outString.getBytes());
				
				
			   }else{
				   response.sendRedirect("/a1/tmatStatisticsReport?fileNotFound=true"+"&applyDate="+applyDate);
			   }
			} catch (Exception e) {
				System.out.println(e.fillInStackTrace());
			}
			
			
		}
		
 		
		public ModelAndView loadTMATStatisticsReport(HttpServletRequest request,
				HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-tmat-statistics-report");
		  
			String fileNotFound=request.getParameter("fileNotFound");
			String applyDate=request.getParameter("applyDate");
			
			int list=DAORegistry.getEventDAO().getEventCount();
			int sport=DAORegistry.getEventDAO().getEventCountSport();
			int concert=DAORegistry.getEventDAO().getEventCountConcerts();
			//int defaults=DAORegistry.getEventDAO().getEventCountDefault();
			int theater=DAORegistry.getEventDAO(). getEventCountTheater();
			//int lasvegas=DAORegistry.getEventDAO().getEventCountLasVegas();
			int other=DAORegistry.getEventDAO().getEventCountOther();
			int totalenabledcrawl=DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlCountByEnabled();
			
			//stubhub
			int sportStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubSportsCrawlCount();
			int theaterStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubTheaterCrawlCount();
			int concertStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubConcertCrawlCount();
			int otherStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubOtherCrawlCount();
			//int lasVegasStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubLasVegasCrawlCount();
			//int defaultStubhub=DAORegistry.getTicketListingCrawlDAO().getStubhubDefaultCrawlCount();
			int totalStubhub=sportStubhub+theaterStubhub+concertStubhub+otherStubhub/*+lasVegasStubhub+defaultStubhub*/;
			//TND
			int sportTnd=DAORegistry.getTicketListingCrawlDAO().getTndSportsCrawlCount();
			int theaterTnd=DAORegistry.getTicketListingCrawlDAO().getTndTheaterCrawlCount();
			int concertTnd=DAORegistry.getTicketListingCrawlDAO().getTndConcertCrawlCount();
			int otherTnd=DAORegistry.getTicketListingCrawlDAO().getTndOtherCrawlCount();
			//int lasVegasTnd=DAORegistry.getTicketListingCrawlDAO().getTndLasVegasCrawlCount();
			//int defaultTnd=DAORegistry.getTicketListingCrawlDAO().getTndDefaultCrawlCount();
			int totalTnd=sportTnd+theaterTnd+concertTnd+otherTnd/*+lasVegasTnd+defaultTnd*/;
			
			//Tnow
			int sportTnow=DAORegistry.getTicketListingCrawlDAO().getTnowSportsCrawlCount();
			int theaterTnow=DAORegistry.getTicketListingCrawlDAO().getTnowTheaterCrawlCount();
			int concertTnow=DAORegistry.getTicketListingCrawlDAO().getTnowConcertCrawlCount();
			int otherTnow=DAORegistry.getTicketListingCrawlDAO().getTnowOtherCrawlCount();
			//int lasVegasTnow=DAORegistry.getTicketListingCrawlDAO().getTnowLasVegasCrawlCount();
			//int defaultTnow=DAORegistry.getTicketListingCrawlDAO().getTnowDefaultCrawlCount();
			int totalTnow=sportTnow+theaterTnow+concertTnow+otherTnow/*+lasVegasTnow+defaultTnow*/;
			//Te
			int sportTe=DAORegistry.getTicketListingCrawlDAO().getTeSportsCrawlCount();
			int theaterTe=DAORegistry.getTicketListingCrawlDAO().getTeTheaterCrawlCount();
			int concertTe=DAORegistry.getTicketListingCrawlDAO().getTeConcertCrawlCount();
			int otherTe=DAORegistry.getTicketListingCrawlDAO().getTeOtherCrawlCount();
			//int lasVegasTe=DAORegistry.getTicketListingCrawlDAO().getTeLasVegasCrawlCount();
			//int defaultTe=DAORegistry.getTicketListingCrawlDAO().getTeDefaultCrawlCount();
			int totalTe=sportTe+theaterTe+concertTe+otherTe/*+lasVegasTe+defaultTe*/;
			
					
			//Flash Seat
			int sportFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsSportsCrawlCount();
			int theaterFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsTheaterCrawlCount();
			int concertFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsConcertCrawlCount();
			int otherFlSheet=DAORegistry.getTicketListingCrawlDAO().getFsOtherCrawlCount();
			int totalFlSheet=sportFlSheet+theaterFlSheet+concertFlSheet+otherFlSheet;
			
			//Vivid Seat
			int sportVs=DAORegistry.getTicketListingCrawlDAO().getVsSportsCrawlCount();
			int theaterVs=DAORegistry.getTicketListingCrawlDAO().getVsTheaterCrawlCount();
			int concertVs=DAORegistry.getTicketListingCrawlDAO().getVsConcertCrawlCount();
			int otherVs=DAORegistry.getTicketListingCrawlDAO().getVsOtherCrawlCount();
			int totalVs=sportVs+theaterVs+concertVs+otherVs;
			
			//Total
			int totalSport=sportStubhub+sportTnd+sportTnow+sportTe+sportFlSheet+sportVs;
			int totalConcert=concertStubhub+concertTnd+concertTnow+concertTe+concertFlSheet+concertVs;
			int totalTheater=theaterStubhub+theaterTnd+theaterTnow+theaterTe+theaterFlSheet+theaterVs;
			int totalOther=otherStubhub+otherTnd+otherTnow+otherTe+otherFlSheet+otherVs;
			//int totalLasVegas=lasVegasStubhub+lasVegasTnd+lasVegasTnow+lasVegasTe;
			//int totalDefault=defaultStubhub+defaultTnd+defaultTnow+defaultTe;
			int total=totalenabledcrawl;
			if(list==0)
			{
				System.out.println("no data available");
			}
			else{
			//Map<String, Object> map = new HashMap<String, Object>();
				  int tndSportPer=(sportTnd*100)/sport;  
				  int tndTheaterPer=(theaterTnd*100)/theater;
				  int tndConcertPer=(concertTnd*100)/concert;
				  int tndOtherPer=(otherTnd*100)/other;
				  
				  int shSportPer=(sportStubhub*100)/sport;  
				  int shTheaterPer=(theaterStubhub*100)/theater;
				  int shConcertPer=(concertStubhub*100)/concert;
				  int shOtherPer=(otherStubhub*100)/other;
				  
				  int teSportPer=(sportTe*100)/sport;  
				  int teTheaterPer=(theaterTe*100)/theater;
				  int teConcertPer=(concertTe*100)/concert;
				  int teOtherPer=(otherTe*100)/other;
				  
				  int tnowSportPer=(sportTnow*100)/sport;  
				  int tnowTheaterPer=(theaterTnow*100)/theater;
				  int tnowConcertPer=(concertTnow*100)/concert;
				  int tnowOtherPer=(otherTnow*100)/other;
				  
				  int fsSportPer=(sportFlSheet*100)/sport;  
				  int fsTheaterPer=(theaterFlSheet*100)/theater;
				  int fsConcertPer=(concertFlSheet*100)/concert;
				  int fsOtherPer=(otherFlSheet*100)/other;
				  
				  int vsSportPer=(sportVs*100)/sport;  
				  int vsTheaterPer=(theaterVs*100)/theater;
				  int vsConcertPer=(concertVs*100)/concert;
				  int vsOtherPer=(otherVs*100)/other;
			
			mav.addObject("applyDate", applyDate);
			mav.addObject("fileNotFound", fileNotFound);
			mav.addObject("tndSportPer", tndSportPer);
			mav.addObject("tndTheaterPer", tndTheaterPer);
			mav.addObject("tndConcertPer", tndConcertPer);
			mav.addObject("tndOtherPer", tndOtherPer);
			mav.addObject("shSportPer", shSportPer);
			mav.addObject("shTheaterPer",shTheaterPer);			
			mav.addObject("shConcertPer", shConcertPer);
			mav.addObject("shOtherPer", shOtherPer);
			mav.addObject("teSportPer", teSportPer);
			mav.addObject("teTheaterPer",teTheaterPer);			
			mav.addObject("teConcertPer", teConcertPer);
			mav.addObject("teOtherPer",teOtherPer);
			mav.addObject("tnowSportPer", tnowSportPer);
			mav.addObject("tnowTheaterPer",tnowTheaterPer);			
			mav.addObject("tnowConcertPer", tnowConcertPer);
			mav.addObject("tnowOtherPer",tnowOtherPer);
			
			mav.addObject("fsSportPer", fsSportPer);
			mav.addObject("fsTheaterPer",fsTheaterPer);			
			mav.addObject("fsConcertPer", fsConcertPer);
			mav.addObject("fsOtherPer",fsOtherPer);
			
			mav.addObject("vsSportPer", vsSportPer);
			mav.addObject("vsTheaterPer",vsTheaterPer);			
			mav.addObject("vsConcertPer", vsConcertPer);
			mav.addObject("vsOtherPer",vsOtherPer);
			
			
			mav.addObject("numevents", list);
			mav.addObject("sports", sport);
			mav.addObject("concert", concert);
			//map.put("defaults", defaults);
			mav.addObject("theater", theater);
			mav.addObject("other", other);
			//map.put("lasvegas", lasvegas);
			mav.addObject("totalenabledcrawl", totalenabledcrawl);
			//stubhub
			mav.addObject("sportStubhub", sportStubhub);
			mav.addObject("theaterStubhub", theaterStubhub);
			mav.addObject("concertStubhub", concertStubhub);
			mav.addObject("otherStubhub", otherStubhub);
			//map.put("lasVegasStubhub", lasVegasStubhub);
			//map.put("defaultStubhub", defaultStubhub);
			mav.addObject("totalStubhub", totalStubhub);
			//TND
			mav.addObject("sportTnd", sportTnd);
			mav.addObject("theaterTnd", theaterTnd);
			mav.addObject("concertTnd", concertTnd);
			mav.addObject("otherTnd", otherTnd);
			//map.put("lasVegasTnd", lasVegasTnd);
			//map.put("defaultTnd", defaultTnd);
			mav.addObject("totalTnd", totalTnd);
			//Tnow
			mav.addObject("sportTnow", sportTnow);
			mav.addObject("theaterTnow", theaterTnow);
			mav.addObject("concertTnow", concertTnow);
			mav.addObject("otherTnow", otherTnow);
			//map.put("lasVegasTnow", lasVegasTnow);
			//map.put("defaultTnow", defaultTnow);
			mav.addObject("totalTnow", totalTnow);
			//Te
			mav.addObject("sportTe", sportTe);
			mav.addObject("theaterTe", theaterTe);
			mav.addObject("concertTe", concertTe);
			mav.addObject("otherTe", otherTe);
			//map.put("lasVegasTe", lasVegasTe);
			//map.put("defaultTe", defaultTe);
			mav.addObject("totalTe", totalTe);
			
							
			//Flash Seat
			mav.addObject("sportFlSheet", sportFlSheet);
			mav.addObject("theaterFlSheet", theaterFlSheet);
			mav.addObject("concertFlSheet", concertFlSheet);
			mav.addObject("otherFlSheet", otherFlSheet);
			mav.addObject("totalFlSheet", totalFlSheet);
			
			//Vivid Seat
			mav.addObject("sportVs", sportVs);
			mav.addObject("theaterVs", theaterVs);
			mav.addObject("concertVs", concertVs);
			mav.addObject("otherVs", otherVs);
			mav.addObject("totalVs", totalVs);
			
			
			//Total
			mav.addObject("totalSport", totalSport);
			mav.addObject("totalTheater", totalTheater);
			mav.addObject("totalConcert", totalConcert);
			mav.addObject("totalOther", totalOther);
			//map.put("totalLasVegas", totalLasVegas);
			//map.put("totalDefault", totalDefault);
			mav.addObject("total", total);
			}
			return mav;
		}
		public ModelAndView loadTnEventsSalesReport(HttpServletRequest request, HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-tn-events-sales-report");
			Date fromDate = new Date();
			Date toDate = new Date();
			
			//fromDate.setDate(fromDate.getDate()-1);
			mav.addObject("fromDate", fromDate.getDate());
			mav.addObject("fromMonth", fromDate.getMonth()+1);
			mav.addObject("fromYear", fromDate.getYear()+1900);
			mav.addObject("fromHour", 0);
			
			mav.addObject("toDate", toDate.getDate());
			mav.addObject("toMonth", toDate.getMonth()+1);
			mav.addObject("toYear", toDate.getYear()+1900);
			mav.addObject("toHour", 23);
			
			
			return mav;
		}
		
		public ModelAndView mailBrokerCategorySalesReport(HttpServletRequest request, HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-mail-broker-category-sales-report");
			String action = request.getParameter("action");
			
			if(action != null && action.equals("action")){
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyy hh a");
				String broker = request.getParameter("broker");
				String fromMonth = request.getParameter("fromMonth");
				String fromDay = request.getParameter("fromDay");
				String fromYear = request.getParameter("fromYear");
				String fromHour = request.getParameter("fromHour");
				String fromDate = fromMonth + "/" + fromDay + "/" + fromYear + " "+ fromHour + ":00:00";
				Date startDate = new Date(fromDate);
				
				String toMonth = request.getParameter("toMonth");
				String toDay = request.getParameter("toDay");
				String toYear = request.getParameter("toYear");
				String toHour = request.getParameter("toHour");
				String toDate = toMonth + "/" + toDay + "/" + toYear + " "+ toHour + ":59:59";
				Date endDate = new Date(toDate);
				
				// Mehul : all mails are merged in one mail
				String subject = "Statistics of Cats and zones ProductLines From " + format.format(startDate) + " To " + format.format(endDate);
				AllInOneCategorySalesReport.categorySalesMail(startDate, endDate, subject);
				
				/*if(broker != null && broker.equals("2")){
					String subject = "Statistics of JKT Cats and zones ProductLines From "+ format.format(startDate) + " To " + format.format(endDate);
					try{
						JktCategorySalesReport.categorySalesMail(startDate, endDate, subject);
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(broker != null && broker.equals("3")){
					String subject = "Statistics of ROT Cats and zones ProductLines From "+ format.format(startDate) + " To " + format.format(endDate);
					try{
						RotCategorySalesReport.categorySalesMail(startDate, endDate, subject);
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(broker != null && broker.equals("4")){
					String subject = "Statistics of Manhattan Cats and zones ProductLines From "+ format.format(startDate) + " To " + format.format(endDate);
					try{
						ManhattanCategorySalesReport.categorySalesMail(startDate, endDate, subject);
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(broker != null && broker.equals("5")){
					String subject = "Statistics of TixCity Cats and zones ProductLines From "+ format.format(startDate) + " To " + format.format(endDate);
					try{
						TixCityCategorySalesReport.categorySalesMail(startDate, endDate, subject);
					}catch(Exception e){
						e.printStackTrace();
					}
				}else if(broker != null && broker.equals("7")){
					String subject = "Statistics of TixCity New Cats and zones ProductLines From "+ format.format(startDate) + " To " + format.format(endDate);
					try{
						TixCityNewCategorySalesReport.categorySalesMail(startDate, endDate, subject);
					}catch(Exception e){
						e.printStackTrace();
					}
				}*/
				
				mav.addObject("returnMessage", "Category Sales mail has been Sent");
			}
			Date date = new Date();
			mav.addObject("fromDate", "1");
			mav.addObject("fromMonth", "1");
			mav.addObject("fromYear", "2015");
			mav.addObject("toDate", date.getDate() - 1);
			mav.addObject("toMonth", date.getMonth()+1);
			mav.addObject("toYear", date.getYear()+1900);
			
			return mav;
		}
		
		// Mehul : added for todays sales list event wise on 11-25-2015
		public void eventsWithSalesReport(HttpServletRequest request, HttpServletResponse response) throws Exception{
			
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyy HH:mm:ss");
			SimpleDateFormat dbDateTimefmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String fromMonth = request.getParameter("fromMonth");
			String fromDay = request.getParameter("fromDay");
			String fromYear = request.getParameter("fromYear");
			String fromHour = request.getParameter("fromHour");
			String fromDate = fromMonth + "/" + fromDay + "/" + fromYear + " "+ fromHour + ":00:00";
			//Date startDate = format.parse(fromDate);
			fromDate = dbDateTimefmt.format(format.parse(fromDate));
			
			String toMonth = request.getParameter("toMonth");
			String toDay = request.getParameter("toDay");
			String toYear = request.getParameter("toYear");
			String toHour = request.getParameter("toHour");
			String toDate = toMonth + "/" + toDay + "/" + toYear + " "+ toHour + ":59:59";
			//Date endDate = new Date(toDate);
			toDate = dbDateTimefmt.format(format.parse(toDate));
			
			StringBuilder excelData = new StringBuilder();			
			excelData.append("Event Id,Event Name,Event Date,Event Time,Venue,City,State,Totoal Sales,Tmat Event,TicketNetwork Direct,StubHub,Ticket Evolution,CSV Attached");
			excelData.append("\n");
			
			//List<Object[]> list = DAORegistry.getQueryManagerDAO().getTodaysEventSalesList();
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTnEventSalesListByDateRange(fromDate, toDate);
			if(list != null && list.size() > 0){
				for(Object[] obj : list){
					excelData.append(obj[0] + "," + obj[1] + "," + obj[2] + "," + obj[3] + "," + obj[4] + "," + obj[5] + "," + obj[6] + "," + obj[7] + "," + obj[8]
							+ "," + obj[9] + "," + obj[10] + "," + obj[11] + "," + obj[12]);
					excelData.append("\n");
				}
			}
			
			String fileName = "TodaysEventSalesList.csv";
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename="+fileName);
			response.getOutputStream().write(excelData.toString().getBytes());
		}
		public void downloadTnEventsSalesReport(HttpServletRequest request, HttpServletResponse response) throws Exception{
			
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyy HH:mm:ss");
			SimpleDateFormat dbDateTimefmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String fromMonth = request.getParameter("fromMonth");
			String fromDay = request.getParameter("fromDay");
			String fromYear = request.getParameter("fromYear");
			String fromHour = request.getParameter("fromHour");
			String fromDate = fromMonth + "/" + fromDay + "/" + fromYear + " "+ fromHour + ":00:00";
			//Date startDate = format.parse(fromDate);
			fromDate = dbDateTimefmt.format(format.parse(fromDate));
			
			String toMonth = request.getParameter("toMonth");
			String toDay = request.getParameter("toDay");
			String toYear = request.getParameter("toYear");
			String toHour = request.getParameter("toHour");
			String toDate = toMonth + "/" + toDay + "/" + toYear + " "+ toHour + ":59:59";
			//Date endDate = new Date(toDate);
			toDate = dbDateTimefmt.format(format.parse(toDate));
			
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			List<TNDEventSalesReportDetail> tndEventSalesReportList = DAORegistry.getQueryManagerDAO().getTNDEventSalesDetailReport(fromDate,toDate);
			SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet();
			int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("TN EventId");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Event Name");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Date");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event Time");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Venue");
		    j++;
		    rowhead.createCell((int) j).setCellValue("City");
		    j++;
		    rowhead.createCell((int) j).setCellValue("State");
		    j++;
		    //rowhead.createCell((int) j).setCellValue("Country");
		    //j++;
		    rowhead.createCell((int) j).setCellValue("Sales Count");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Recent Sale Date");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TN Event Created Date");
		    j++;
		    
		    rowhead.createCell((int) j).setCellValue("TMAT Event");
		    j++;
		    rowhead.createCell((int) j).setCellValue("CSV");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TND Crawl");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TEvol Crawl");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Stubhub Crawl");
		    j++;
		    
		    int i =1;
		    for (TNDEventSalesReportDetail tndEventSalesDetail : tndEventSalesReportList) {
			
		    	int column = 0;
		    	Row row = ssSheet.createRow(i);
				row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventId());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventName());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventDateStr());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getEventTimeStr());
		        column++;
		       
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getVenue());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getCity());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getState());
		        column++;
		        //row.createCell((int) column).setCellValue(tndEventSalesDetail.getCountry());
		        //column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getSalesCount());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getRecentSaleDateStr());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getTnEventCreatedDateStr());
		        column++;
		        
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getTmatEvent());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getCsvUploaded());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getTicketNetworkDirectCrawl());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getTicketEvolutionCrawl());
		        column++;
		        row.createCell((int) column).setCellValue(tndEventSalesDetail.getStubhubCrawl());
		        column++;
		        
		        i++;
			}
			
		    String fileName = "TnEventsSalesReport-"+fromDate+"_to_"+toDate+".xlsx";
			System.out.println(fileName);
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename="+fileName);
			
			workbook.write(response.getOutputStream());
		}
		
		public ModelAndView loadRtwProfitAndLossReport(HttpServletRequest request, HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-rtw-profit-loss-report");
			try{
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);//show yesterday date
				
				mav.addObject("fromDate", "1");
				mav.addObject("fromMonth", "1");
				mav.addObject("fromYear", cal.get(Calendar.YEAR));
				mav.addObject("toDate", cal.get(Calendar.DAY_OF_MONTH));
				mav.addObject("toMonth", cal.get(Calendar.MONTH)+1);
				mav.addObject("toYear", cal.get(Calendar.YEAR));
			}catch(Exception e){
				e.printStackTrace();
			}
			return mav;
		}
		
		public void downloadRtwProfitAndLossReport(HttpServletRequest request, HttpServletResponse response){
			try{
				String fromMonth = request.getParameter("fromMonth");
				String fromDay = request.getParameter("fromDay");
				String fromYear = request.getParameter("fromYear");
				String fromDate = fromYear + "-" + fromMonth + "-" + fromDay + " 00:00:00.000";
				
				String toMonth = request.getParameter("toMonth");
				String toDay = request.getParameter("toDay");
				String toYear = request.getParameter("toYear");
				String toDate = toYear + "-" + toMonth + "-" + toDay + " 23:59:59.999";
				
				String brokerStr = request.getParameter("broker");
				String fileNameStr = "";
				
				List<String> brokersList = new ArrayList<String>();
				if(brokerStr != null) {
					if(brokerStr.equals("RTW")) {
						brokersList.add("RTW");
						fileNameStr = "RTW";
					} else if(brokerStr.equals("RTW-2")) {
						brokersList.add("RTW-2");
						fileNameStr = "RTW-2";
					} else {
						brokersList.add("RTW");
						brokersList.add("RTW-2");
						fileNameStr = "RTW_RTW-2";
					}
				}
				
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				String fileName = fileNameStr+"_ProfitAndLossReport-From-"+ fromMonth + "/" +fromDay + "/" + fromYear + "-To-" + 
								  toMonth +"/" + toDay + "/" + toYear + ".xlsx";
				System.out.println(fileName);
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition", "attachment; filename="+fileName);
				getInvoiceList(fromDate, toDate, workbook,brokersList);
				workbook.write(response.getOutputStream());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		public static void getInvoiceList(String startDateStr,String endDateStr, SXSSFWorkbook workbook,List<String> brokersList) throws Exception{
			
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			/* 
			List<String> brokersList = new ArrayList<String>();
			brokersList.add("RTW");
			brokersList.add("RTW-2");*/
			
			List<POSTicketGroup> isCatsInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isZonesInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isLongInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isSHTEInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isRetailInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isUnfilledInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isWholeSaleCSRInvoices = new ArrayList<POSTicketGroup>();
			 
			 
			 for (String brokerName : brokersList) {
				
			 List<Invoice> invoices = DAORegistry.getQueryManagerDAO().getRTWFulfiledInvoiceList(startDateStr, endDateStr, Connections.getPosConnectionByBrokername(brokerName));
			 Map<Integer, PosEvent> mzTixLocalEventObjMap = new HashMap<Integer, PosEvent>();
			 
			 List<PosEvent> mzTixEvents = DAORegistry.getQueryManagerDAO().getAllLocalAEvents(Connections.getPosConnectionByBrokername(brokerName));
			 
			 for (PosEvent mzTixEvent : mzTixEvents) {
				 mzTixLocalEventObjMap.put(mzTixEvent.getId(), mzTixEvent);
			}
			 
			 
		 	Map<Integer, PosEvent> jktExchangeEventMap = new HashMap<Integer, PosEvent>();
		 	
			List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getAllEventsWithExchangeEvent(Connections.getPosConnectionByBrokername(brokerName));
			System.out.println("RTW : Total Number of pos active events...."+posEvents.size());
			
			for (PosEvent posEvent : posEvents) {
				jktExchangeEventMap.put(posEvent.getExchangeEventId(), posEvent);
			}
			
			if(jktExchangeEventMap.isEmpty()){
				System.out.println("RTW : There is no exchange event found in pos .");
			}
			 
			 String missingInvoice = "";
			 int invoiceSize = invoices.size(),currentSize=0;
			 System.out.println(brokerName+" : Total Invoices------->"+invoiceSize);
			 
			 
			 for (Invoice invoice : invoices) {
				 currentSize++;
				 List<POSTicketGroup> invoiceTicketGroups = new ArrayList<POSTicketGroup>();
				 
				 //System.out.println(brokerName+" : "+invoiceSize+"=====================PROCESSING======================>"+currentSize);
				 
				 //System.out.println(brokerName+" : InvoiceId----->"+invoice.getInvoiceId());
				 
				 int rtwComission = 9;
				
				 
				 if(invoice.getInvoiceId().equals(24854)){
					 System.out.println(invoice.getInvoiceId());
				 }
				 
					 List<POSTicketGroup> soldTicketgroups = DAORegistry.getQueryManagerDAO().
					 getRTWShortSalesByInvoiceId(invoice.getInvoiceId(),Connections.getPosConnectionByBrokername(brokerName));
					 
					 POSTicketGroup ticketGroup  =  null;
					 if(soldTicketgroups != null && !soldTicketgroups.isEmpty() && soldTicketgroups.size() > 0){
						 
						 ticketGroup  = soldTicketgroups.get(0);
						 PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
						 
						 if(null == mzTixEvent){
							 
							 mzTixEvent =new PosEvent();
							 mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
						 }
						 
						 ticketGroup.setInvoiceId(invoice.getInvoiceId());
						 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
						 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
						 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
						 ticketGroup.setEventName(mzTixEvent.getEventName());
						 ticketGroup.setVenueName(mzTixEvent.getVenueName());
						 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
						 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
						 ticketGroup.setIsVoidedInvoice("NO");
						 ticketGroup.setInvoiceVoidDateStr("");
						 
						 
						 if(null != ticketGroup.getSection() && (ticketGroup.getSection().contains("ZONE") || ticketGroup.getSection().contains("zone")
								 || ticketGroup.getSection().contains("Zone"))){
							 ticketGroup.setIsZones(true);
						 }else{
							 ticketGroup.setIsCats(true);
						 }
						 
						 if(null != ticketGroup.getFillDate()){
							 ticketGroup.setFillDateStr(xlDateFormat.format(ticketGroup.getFillDate()));
						 }
						 
						 
						 if(null != ticketGroup.getActualWholeSalePrice() && ticketGroup.getActualWholeSalePrice() > 0){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualWholeSalePrice());
						 }
						 
						 String seat = "";
						 
						 if(null != ticketGroup.getCatSeatLow() && !ticketGroup.getCatSeatLow().isEmpty() &&
								 !ticketGroup.getCatSeatLow().equals("")){
							 
							 seat = ticketGroup.getCatSeatLow();
							 
							 if(null != ticketGroup.getCatSeatHigh() && !ticketGroup.getCatSeatHigh().isEmpty() &&
									 !ticketGroup.getCatSeatHigh().equals("")){
								 seat = seat +"-"+ticketGroup.getCatSeatHigh();
							 }
						 }
						 Double price = ticketGroup.getWholesalePrice();
						 if(price > 0){
							 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
						 }else{
							 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
						 }
						 
						 if(ticketGroup.getWholesalePrice() <= 0 || 
								 ticketGroup.getWholesalePrice().equals(0.00)){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 }
						 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*rtwComission)/100);				 
						 ticketGroup.setSeat(seat);
						 
					 }
					 
					 List<PosTicket> posTickets = new ArrayList<PosTicket>();
					 posTickets = DAORegistry.getQueryManagerDAO().getJKTTicketsByInvoiceId(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 Map<Integer, List<PosTicket>> ticketGroupMap =new HashMap<Integer, List<PosTicket>>();
					 
					 for (PosTicket ticket : posTickets) {
						 
						 List<PosTicket> tempCatTickets = ticketGroupMap.get(ticket.getTicketGroupId());
						 
						 if(null != tempCatTickets && !tempCatTickets.isEmpty()){
							 ticketGroupMap.get(ticket.getTicketGroupId()).add(ticket);
						 }else{
							 tempCatTickets = new ArrayList<PosTicket>();
							 tempCatTickets.add(ticket);
							 ticketGroupMap.put(ticket.getTicketGroupId(), tempCatTickets);
						 }
					 }
					 
					 
					 for (Integer ticketGroupId : ticketGroupMap.keySet()) {
							
						 List<PosTicket> tickets = ticketGroupMap.get(ticketGroupId);
						 
						 if(null != ticketGroup ){
							 
							 POSTicketGroup ticketGroup2 = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
							 Double price = ticketGroup2.getCost();
							 ticketGroup.setCost(price);
							 if(price > 0){
								 ticketGroup.setTotalCost(tickets.size() * price);
							 }else{
								 ticketGroup.setTotalCost(tickets.size() * tickets.get(0).getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 
						 }else{
							 
							 ticketGroup = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
							 if(null == ticketGroup){
								 continue;
							 }
							 
							 PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
							 
							 if(null == mzTixEvent){
								 mzTixEvent =new PosEvent();
								 mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
							 }
							 
							 ticketGroup.setInvoiceId(invoice.getInvoiceId());
							 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
							 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
							 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
							 ticketGroup.setEventName(mzTixEvent.getEventName());
							 ticketGroup.setVenueName(mzTixEvent.getVenueName());
							 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
							 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 if( null != invoice.getInvoiceVoidDate() ){
								 ticketGroup.setInvoiceVoidDateStr(xlDateFormat.format(invoice.getInvoiceVoidDate()));
							 }
							 ticketGroup.setQty(tickets.size());
							 ticketGroup.setIsLong(true);
							 ticketGroup.setFillDate(null);
							 
							 String seat = "";
							 
							 int i= 1 , j = tickets.size();
							 for (PosTicket ticket : tickets) {
								 
								 if(i==1 ){
									 ticketGroup.setActualSoldPrice(ticket.getActualSoldPrice());
									 seat = String.valueOf(ticket.getSeatNumber());
								 }else if (i == j){
									 seat = seat + "-" + String.valueOf(ticket.getSeatNumber());
								 }
								i++;
							}
							 if(ticketGroup.getWholesalePrice() <= 0 || 
									 ticketGroup.getWholesalePrice().equals(0.00)){
								 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 }
							 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 
							 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 
							 Double price = ticketGroup.getCost();
							 if(price > 0){
								 ticketGroup.setTotalCost(tickets.size() * price);
							 }else{
								 ticketGroup.setTotalCost(tickets.size() * ticketGroup.getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setSeat(seat);
						 }
						 
					 }
					 
					 
								
					 
					//invoiceTicketGroups.add(ticketGroup);
				if(null != ticketGroup){
					 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 ticketGroup.setClientBrokerId(clientBroker.getClientId());
					 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
					 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
					 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
					 ticketGroup.setCity(clientBroker.getCity());
					 ticketGroup.setState(clientBroker.getState());
					 ticketGroup.setCountry(clientBroker.getCountry());
					 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
					 ticketGroup.setClientMail(clientBroker.getClientMail());
					 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
					 ticketGroup.setClientType(clientBroker.getClientType());
					 
					 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
					 ticketGroup.setTmatBrokerName(brokerName);
					 
					 boolean isDeadWood = false,isrewardthefan = false;
					 if(ticketGroup.getSalesPerson() != null && 
							 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
						 isDeadWood = true; 
					 }
					 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
						 if(ticketGroup.getClientCompanyName() != null && 
								 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
							 isDeadWood = true;
						 }
						 if(ticketGroup.getClientCompanyName() != null && 
								 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
							 isrewardthefan = true;
						 }
					 } else {
						 if(clientBroker.getClientFirstName() != null && 
								 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
							 isDeadWood = true;
						 }
						 if(clientBroker.getClientFirstName() != null && 
								 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
							 isrewardthefan = true;
						 }
					 }
					 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
					 
					 if(isDeadWood) {
						 ticketGroup.setWholesalePrice(0.0);
						 ticketGroup.setRetailPrice(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
						 ticketGroup.setActualSoldPrice(0.0);
					 }
					/* if(isrewardthefan) {
						 ticketGroup.setWholesalePrice(0.0);
						 ticketGroup.setRetailPrice(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
						 ticketGroup.setCost(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
					 }*/
					//Do not consider rewardthefan invoices in profit loss and filled reports
					 if(isrewardthefan) {
						continue; 
					 }
					 
					 if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
							 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
						 ticketGroup.setIsRetail(true);
						 
					 }else if (invoice.getSystemUserId() == 37 ){
						 ticketGroup.setIsWholeSaleCsr(true);
					 }
					 
					 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
						 
						 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
							 ticketGroup.setIsTnow(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
								 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
							 ticketGroup.setIsSH(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
								 ticketGroup.getClientBrokerId() == 3761){
							 ticketGroup.setIsTE(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 } else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
							 ticketGroup.setIsLong(false);
							 ticketGroup.setIsSH(false);
							 ticketGroup.setIsTE(false);
							 ticketGroup.setIsTnow(false);
							 ticketGroup.setIsZones(false);
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
							 ticketGroup.setIsCats(true);
							 
							 ticketGroup.setIsRightThisWayOrder(true);
						 }
					 }
					 
					 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
						 
						 ticketGroup.setIsLong(false);
						 ticketGroup.setIsSH(false);
						 ticketGroup.setIsTE(false);
						 ticketGroup.setIsTnow(false);
						 ticketGroup.setIsZones(false);
						 
					 }
					 
					 	if(ticketGroup.getIsUnfilled()){
					 		isUnfilledInvoices.add(ticketGroup);
					 	} else if(ticketGroup.getIsSH() || ticketGroup.getIsTE() || ticketGroup.getIsTnow()){
					 		
					 		//Tamil : for TNow,TEvol and Stubhub tab consider actual sold price as sold price instead of considering wholesale price
							ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							
							isSHTEInvoices.add(ticketGroup);
						} else if(ticketGroup.getIsRetail()){

					 		//Tamil : for Retail tab consider actual sold price as sold price instead of considering wholesale price
							 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 
					 		isRetailInvoices.add(ticketGroup);
						} else if(ticketGroup.getIsWholeSaleCsr()){
					 		isWholeSaleCSRInvoices.add(ticketGroup);
						} else if(ticketGroup.getIsLong()){
							isLongInvoices.add(ticketGroup);
						}else if(ticketGroup.getIsZones()){
							isZonesInvoices.add(ticketGroup);
						}else{
							isCatsInvoices.add(ticketGroup);
						}
						
				}else{
					
					missingInvoice = missingInvoice +","+invoice.getInvoiceId();
				}
					
				
			}
			 System.out.println("broker : "+brokerName+" : missing Invoices : "+missingInvoice);
			 }
			 
			 //SXSSFWorkbook workbook = new SXSSFWorkbook();
			 
			 List<String> sheetNameList = new ArrayList<String>();
			 Map<String,Double> summaryMap = new HashMap<String, Double>();
			
			 String sheetName = "TN Retail";
			 generateJKTWeeklyInvoiceSheet(workbook, isRetailInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "SH and TE and TNow";
			 generateJKTWeeklyInvoiceSheet(workbook, isSHTEInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Long";
			 generateJKTWeeklyInvoiceSheet(workbook, isLongInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Cats";
			 generateJKTWeeklyInvoiceSheet(workbook, isCatsInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Zones";
			 generateJKTWeeklyInvoiceSheet(workbook, isZonesInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "Wholesale CSR";
			 generateJKTWeeklyInvoiceSheet(workbook, isWholeSaleCSRInvoices,sheetName,false,false,false,true,false,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 generateUnfilledInvoiceSheet(workbook, isUnfilledInvoices,"Unfilled Orders",false,false,false,true);
			 
			 Double tixcityFees = 0.0;//getTixCityInvoiceList(startDateStr, endDateStr, workbook, sheetNameList, summaryMap);
			 
			 getRewardTheFanInvoiceList(startDateStr, endDateStr, workbook, sheetNameList, summaryMap);
			 
			 CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			     
			 SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet("Summary");
			 int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("Type");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Sold Price");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Net Sold Price");
			 j++;
			 
			 Double totalProfitLoss = 0.0,totalActualSoldPrice=0.0,totalNetSoldPrice=0.0;
			 int i = 1;
			
			 for (String tempSheetName : sheetNameList) {
				 Row row = ssSheet.createRow(i);
				 row.createCell((int) 0).setCellValue(tempSheetName);
					
				Double profitloss = summaryMap.get("ProfitLoss_"+tempSheetName);
				profitloss= (profitloss==null)? 0.0:profitloss;
				totalProfitLoss += profitloss;
				
		        Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(profitloss);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double actualSoldPrice = summaryMap.get("ActualSoldPrice_"+tempSheetName);
		        actualSoldPrice= (actualSoldPrice==null)? 0.0:actualSoldPrice;
		        totalActualSoldPrice += actualSoldPrice;
		        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(actualSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double netSoldPrice = summaryMap.get("NetSoldPrice_"+tempSheetName);
		        netSoldPrice= (netSoldPrice==null)? 0.0:netSoldPrice;
				totalNetSoldPrice += netSoldPrice;
		        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(netSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        i++;
			}
	        
			 i++;
	        Row row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Total");
			Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalProfitLoss);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalActualSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalNetSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        i++;
	        Double totProftLossWithFees =  totalProfitLoss;
	        
	        Double reserveOneFees = 2000.0;
	        row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Reserve One Fees");
			amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(reserveOneFees);
	        amountCell.setCellStyle(cellamountStyle);
	        totProftLossWithFees += reserveOneFees;
	        i++;
	        
	        
	       // Double reserveOneFees = 2000.0;
	        row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Tix City Fees");
			amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(tixcityFees);
	        amountCell.setCellStyle(cellamountStyle);
	        totProftLossWithFees += tixcityFees;
	        i++;
	        
	        Double ticketMEFes = 0.0;
	        row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("TicketME Fees");
			amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(ticketMEFes);
	        amountCell.setCellStyle(cellamountStyle);
	        totProftLossWithFees += ticketMEFes;
	        i++;
	        i++;
	        
	        row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Total Profit Sales + Fees");
			amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totProftLossWithFees);
	        amountCell.setCellStyle(cellamountStyle);
	        
			 /*FileOutputStream fileOut = new FileOutputStream("C:\\UNCAT\\RTWProfitLossReportFinal_1.xlsx");
			 workbook.write(fileOut);
		     fileOut.close();*/
		     
			 System.out.println("RTW : JOB COMPLETED SUCESSFULLY........");
			 
			 //return workbook;
		 }
		
public static Double getTixCityInvoiceList(String startDateStr,String endDateStr, SXSSFWorkbook workbook,List<String> sheetNameList,Map<String,Double> summaryMap) throws Exception{
			
	 		Double tixCityFees = 0.0;
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			 
			List<String> brokersList = new ArrayList<String>();
			brokersList.add("Tix City");
			
			List<POSTicketGroup> isCatsInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isLongInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isUnfilledInvoices = new ArrayList<POSTicketGroup>();
			 
			 for (String brokerName : brokersList) {
				
			 List<Invoice> invoices = DAORegistry.getQueryManagerDAO().getTixCityFulfiledInvoiceList(startDateStr, endDateStr, Connections.getPosConnectionByBrokername(brokerName));
			 Map<Integer, PosEvent> mzTixLocalEventObjMap = new HashMap<Integer, PosEvent>();
			 
			 List<PosEvent> mzTixEvents = DAORegistry.getQueryManagerDAO().getAllLocalAEvents(Connections.getPosConnectionByBrokername(brokerName));
			 
			 for (PosEvent mzTixEvent : mzTixEvents) {
				 mzTixLocalEventObjMap.put(mzTixEvent.getId(), mzTixEvent);
			}
			 
			 
		 	/*Map<Integer, PosEvent> jktExchangeEventMap = new HashMap<Integer, PosEvent>();
		 	
			List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getAllEventsWithExchangeEvent(Connections.getPosConnectionByBrokername(brokerName));
			System.out.println("RTW : Total Number of pos active events...."+posEvents.size());
			
			for (PosEvent posEvent : posEvents) {
				jktExchangeEventMap.put(posEvent.getExchangeEventId(), posEvent);
			}
			
			if(jktExchangeEventMap.isEmpty()){
				System.out.println("RTW : There is no exchange event found in pos .");
			}*/
			 
			 String missingInvoice = "";
			 int invoiceSize = invoices.size(),currentSize=0;
			 System.out.println(brokerName+" : Total Invoices------->"+invoiceSize);
			 
			 
			 for (Invoice invoice : invoices) {
				 currentSize++;
				 List<POSTicketGroup> invoiceTicketGroups = new ArrayList<POSTicketGroup>();
				 
				 //System.out.println(brokerName+" : "+invoiceSize+"=====================PROCESSING======================>"+currentSize);
				 
				 //System.out.println(brokerName+" : InvoiceId----->"+invoice.getInvoiceId());
				 
				 int rtwComission = 5;
				
				 
				 if(invoice.getInvoiceId().equals(24854)){
					 System.out.println(invoice.getInvoiceId());
				 }
				 
					 List<POSTicketGroup> soldTicketgroups = DAORegistry.getQueryManagerDAO().
					 getTixCityShortSalesByInvoiceId(invoice.getInvoiceId(),Connections.getPosConnectionByBrokername(brokerName));
					 
					 POSTicketGroup ticketGroup  =  null;
					 if(soldTicketgroups != null && !soldTicketgroups.isEmpty() && soldTicketgroups.size() > 0){
						 
						 ticketGroup  = soldTicketgroups.get(0);
						/* PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
						 if(null == mzTixEvent){
							 mzTixEvent =new PosEvent();
							 mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
						 }*/
						 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
						 
						 ticketGroup.setInvoiceId(invoice.getInvoiceId());
						 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
						 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
						 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
						 ticketGroup.setEventName(mzTixEvent.getEventName());
						 ticketGroup.setVenueName(mzTixEvent.getVenueName());
						 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
						 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
						 ticketGroup.setIsVoidedInvoice("NO");
						 ticketGroup.setInvoiceVoidDateStr("");
						 
						 
						 if(null != ticketGroup.getSection() && (ticketGroup.getSection().contains("ZONE") || ticketGroup.getSection().contains("zone")
								 || ticketGroup.getSection().contains("Zone"))){
							 ticketGroup.setIsZones(true);
						 }else{
							 ticketGroup.setIsCats(true);
						 }
						 
						 if(null != ticketGroup.getFillDate()){
							 ticketGroup.setFillDateStr(xlDateFormat.format(ticketGroup.getFillDate()));
						 }
						 
						 
						 if(null != ticketGroup.getActualWholeSalePrice() && ticketGroup.getActualWholeSalePrice() > 0){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualWholeSalePrice());
						 }
						 
						 String seat = "";
						 
						 if(null != ticketGroup.getCatSeatLow() && !ticketGroup.getCatSeatLow().isEmpty() &&
								 !ticketGroup.getCatSeatLow().equals("")){
							 
							 seat = ticketGroup.getCatSeatLow();
							 
							 if(null != ticketGroup.getCatSeatHigh() && !ticketGroup.getCatSeatHigh().isEmpty() &&
									 !ticketGroup.getCatSeatHigh().equals("")){
								 seat = seat +"-"+ticketGroup.getCatSeatHigh();
							 }
						 }
						 Double price = ticketGroup.getWholesalePrice();
						 if(price > 0){
							 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
						 }else{
							 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
						 }
						 
						 if(ticketGroup.getWholesalePrice() <= 0 || 
								 ticketGroup.getWholesalePrice().equals(0.00)){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 }
						 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*rtwComission)/100);				 
						 ticketGroup.setSeat(seat);
						 
					 }
					 
					 List<PosTicket> posTickets = new ArrayList<PosTicket>();
					 posTickets = DAORegistry.getQueryManagerDAO().getJKTTicketsByInvoiceId(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 Map<Integer, List<PosTicket>> ticketGroupMap =new HashMap<Integer, List<PosTicket>>();
					 
					 for (PosTicket ticket : posTickets) {
						 
						 List<PosTicket> tempCatTickets = ticketGroupMap.get(ticket.getTicketGroupId());
						 
						 if(null != tempCatTickets && !tempCatTickets.isEmpty()){
							 ticketGroupMap.get(ticket.getTicketGroupId()).add(ticket);
						 }else{
							 tempCatTickets = new ArrayList<PosTicket>();
							 tempCatTickets.add(ticket);
							 ticketGroupMap.put(ticket.getTicketGroupId(), tempCatTickets);
						 }
					 }
					 
					 
					 for (Integer ticketGroupId : ticketGroupMap.keySet()) {
							
						 List<PosTicket> tickets = ticketGroupMap.get(ticketGroupId);
						 
						 if(null != ticketGroup ){
							 
							 POSTicketGroup ticketGroup2 = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
							 Double price = ticketGroup2.getCost();
							 ticketGroup.setCost(price);
							 if(price > 0){
								 ticketGroup.setTotalCost(tickets.size() * price);
							 }else{
								 ticketGroup.setTotalCost(tickets.size() * tickets.get(0).getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 
						 }else{
							 
							 ticketGroup = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
							 if(null == ticketGroup){
								 continue;
							 }
							 
							 /*PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
							 if(null == mzTixEvent){
								 mzTixEvent =new PosEvent();
								 mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
							 }*/
							 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
							 
							 ticketGroup.setInvoiceId(invoice.getInvoiceId());
							 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
							 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
							 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
							 ticketGroup.setEventName(mzTixEvent.getEventName());
							 ticketGroup.setVenueName(mzTixEvent.getVenueName());
							 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
							 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 if( null != invoice.getInvoiceVoidDate() ){
								 ticketGroup.setInvoiceVoidDateStr(xlDateFormat.format(invoice.getInvoiceVoidDate()));
							 }
							 ticketGroup.setQty(tickets.size());
							 ticketGroup.setIsLong(true);
							 ticketGroup.setFillDate(null);
							 
							 String seat = "";
							 
							 int i= 1 , j = tickets.size();
							 for (PosTicket ticket : tickets) {
								 
								 if(i==1 ){
									 ticketGroup.setActualSoldPrice(ticket.getActualSoldPrice());
									 seat = String.valueOf(ticket.getSeatNumber());
								 }else if (i == j){
									 seat = seat + "-" + String.valueOf(ticket.getSeatNumber());
								 }
								i++;
							}
							 if(ticketGroup.getWholesalePrice() <= 0 || 
									 ticketGroup.getWholesalePrice().equals(0.00)){
								 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 }
							 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 
							 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 
							 Double price = ticketGroup.getCost();
							 if(price > 0){
								 ticketGroup.setTotalCost(tickets.size() * price);
							 }else{
								 ticketGroup.setTotalCost(tickets.size() * ticketGroup.getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setSeat(seat);
						 }
						 
					 }
					 
					 
								
					 
					//invoiceTicketGroups.add(ticketGroup);
				if(null != ticketGroup){
					 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 ticketGroup.setClientBrokerId(clientBroker.getClientId());
					 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
					 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
					 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
					 ticketGroup.setCity(clientBroker.getCity());
					 ticketGroup.setState(clientBroker.getState());
					 ticketGroup.setCountry(clientBroker.getCountry());
					 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
					 ticketGroup.setClientMail(clientBroker.getClientMail());
					 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
					 ticketGroup.setClientType(clientBroker.getClientType());
					 
					 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
					 ticketGroup.setTmatBrokerName(brokerName);
					 
					 boolean isDeadWood = false,isrewardthefan = false;
					 /*if(ticketGroup.getSalesPerson() != null && 
							 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
						 isDeadWood = true; 
					 }
					 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
						 if(ticketGroup.getClientCompanyName() != null && 
								 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
							 isDeadWood = true;
						 }
						 if(ticketGroup.getClientCompanyName() != null && 
								 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
							 isrewardthefan = true;
						 }
					 } else {
						 if(clientBroker.getClientFirstName() != null && 
								 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
							 isDeadWood = true;
						 }
						 if(clientBroker.getClientFirstName() != null && 
								 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
							 isrewardthefan = true;
						 }
					 }
					 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
					 
					 if(isDeadWood) {
						 ticketGroup.setWholesalePrice(0.0);
						 ticketGroup.setRetailPrice(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
						 ticketGroup.setActualSoldPrice(0.0);
					 }*/
					 
					 
					/* if(isrewardthefan) {
						 ticketGroup.setWholesalePrice(0.0);
						 ticketGroup.setRetailPrice(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
						 ticketGroup.setCost(0.0);
						 ticketGroup.setInvoiceTotal(0.0);
					 }*/
					//Do not consider rewardthefan invoices in profit loss and filled reports
					 if(isrewardthefan) {
						continue; 
					 }
					 
					 /*if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
							 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
						 ticketGroup.setIsRetail(true);
						 
					 }else if (invoice.getSystemUserId() == 37 ){
						 ticketGroup.setIsWholeSaleCsr(true);
					 }
					 
					 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
						 
						 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
							 ticketGroup.setIsTnow(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
								 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
								 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
							 ticketGroup.setIsSH(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
								 ticketGroup.getClientBrokerId() == 3761){
							 ticketGroup.setIsTE(true);
							 
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
						 } else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
							 ticketGroup.setIsLong(false);
							 ticketGroup.setIsSH(false);
							 ticketGroup.setIsTE(false);
							 ticketGroup.setIsTnow(false);
							 ticketGroup.setIsZones(false);
							 ticketGroup.setIsRetail(false);
							 ticketGroup.setIsWholeSaleCsr(false);
							 ticketGroup.setIsCats(true);
							 
							 ticketGroup.setIsRightThisWayOrder(true);
						 }
					 }
					 
					 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
						 
						 ticketGroup.setIsLong(false);
						 ticketGroup.setIsSH(false);
						 ticketGroup.setIsTE(false);
						 ticketGroup.setIsTnow(false);
						 ticketGroup.setIsZones(false);
						 
					 }*/
					 
					 ticketGroup.setIsTixcityOrder(true);
					 if(ticketGroup.getIsUnfilled()){
						 isUnfilledInvoices.add(ticketGroup);
					 } else if(ticketGroup.getIsLong()){
						 if(ticketGroup.getOfficeId()!= null && ticketGroup.getOfficeId().equals(1)) {
							continue; 
						 }
						 isLongInvoices.add(ticketGroup);
					 } else {
						 isCatsInvoices.add(ticketGroup);
					 }
						
				}else{
					
					missingInvoice = missingInvoice +","+invoice.getInvoiceId();
				}
					
				
			}
			 System.out.println("broker : "+brokerName+" : missing Invoices : "+missingInvoice);
			 }
			 
			 //SXSSFWorkbook workbook = new SXSSFWorkbook();
			 
			 Map<String,Double> tixcitySummaryMap = new HashMap<String, Double>();
			 String sheetName = "Tix City Long";
			 generateJKTWeeklyInvoiceSheet(workbook, isLongInvoices,sheetName,false,false,false,true,false,tixcitySummaryMap);
			//sheetNameList.add(sheetName);
			 
			 Double feesPerSheet = tixcitySummaryMap.get("RTWCommission_"+sheetName);
			 if(feesPerSheet != null) {
				 tixCityFees = tixCityFees + feesPerSheet;
			 }
			 
			 sheetName = "Tix City Cats";
			 generateJKTWeeklyInvoiceSheet(workbook, isCatsInvoices,sheetName,false,false,false,true,false,tixcitySummaryMap);
			 //sheetNameList.add(sheetName);
			 feesPerSheet = tixcitySummaryMap.get("RTWCommission_"+sheetName);
			 if(feesPerSheet != null) {
				 tixCityFees = tixCityFees + feesPerSheet;
			 }
			 
			 generateUnfilledInvoiceSheet(workbook, isUnfilledInvoices,"Tix City Unfilled Orders",false,false,false,true);
			 
			 /*FileOutputStream fileOut = new FileOutputStream("C:\\UNCAT\\RTWProfitLossReportFinal_1.xlsx");
			 workbook.write(fileOut);
		     fileOut.close();*/
		     
			 System.out.println("RTW : JOB COMPLETED SUCESSFULLY........");
			 
			 //return workbook;
			 return tixCityFees;
		 }

public static void getRewardTheFanInvoiceList(String startDateStr,String endDateStr, SXSSFWorkbook workbook,List<String> sheetNameList,Map<String,Double> summaryMap) throws Exception{
	
	List<String> brokersList = new ArrayList<String>();
	brokersList.add("RewardTheFan");
	
	List<RTFTicketGroup> isCatsInvoices = new ArrayList<RTFTicketGroup>();
	 List<RTFTicketGroup> isLongInvoices = new ArrayList<RTFTicketGroup>();
	 List<RTFTicketGroup> isUnfilledInvoices = new ArrayList<RTFTicketGroup>();
	 
	 
	 List<RTFTicketGroup> rtfTixGroups = DAORegistry.getQueryManagerDAO().getRewardTheFanOrderDetails(startDateStr, endDateStr,Connections.getRewardTheFanConnection());
	 
	 for (RTFTicketGroup rtfTix : rtfTixGroups) {
		 if(!rtfTix.getIsLongSale() && 
				 rtfTix.getIsRealTixUploaded() != null && rtfTix.getIsRealTixUploaded().equalsIgnoreCase("Yes")){
			 isCatsInvoices.add(rtfTix);
		 } else if(rtfTix.getIsLongSale()){
			 isLongInvoices.add(rtfTix);
		 } else {
			 isUnfilledInvoices.add(rtfTix);
		 }
	 }
		
	 String sheetName = "RTF Long";
	 generateRTFWeeklyInvoiceSheet(workbook, isLongInvoices,sheetName,true,false,true,summaryMap);
	 sheetNameList.add(sheetName);
	 
	 sheetName = "RTF Cats";
	 generateRTFWeeklyInvoiceSheet(workbook, isCatsInvoices,sheetName,true,false,true,summaryMap);
	 sheetNameList.add(sheetName);
	 
	 generateRTFWeeklyInvoiceSheet(workbook, isUnfilledInvoices,"RTF Unfilled Orders",true,true,false,null);
	 
	 /*FileOutputStream fileOut = new FileOutputStream("C:\\UNCAT\\RTWProfitLossReportFinal_1.xlsx");
	 workbook.write(fileOut);
     fileOut.close();*/
     
	 System.out.println("RTW : JOB COMPLETED SUCESSFULLY........");
	 
	 //return workbook;
 	}
public static void generateRTFWeeklyInvoiceSheet(SXSSFWorkbook workbook ,List<RTFTicketGroup> rtfTixGroups,String sheetName,
		 boolean showClientInfo,boolean isUnfilledSheet,boolean showPODate,Map<String,Double> summaryMap) throws Exception{
	 
		SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
		
		int j = 0;
	
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Invoice No");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Invoice Date");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Order No");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Invoice Status");
	    j++;
	    if(showPODate) {
		    rowhead.createCell((int) j).setCellValue("PO No");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("PO Date");
		    j++;
		    	    	
	    }
	    rowhead.createCell((int) j).setCellValue("Sales Person");
	    j++;
	    /*rowhead.createCell((int) j).setCellValue("Parent Catgeory");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Is Zone Event");
	    j++;*/
	    rowhead.createCell((int) j).setCellValue("Event Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Date");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Time");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Venue");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Internal Notes");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Row");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Seat");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section Range");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Row Range");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Qty");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Sold Price Per Ticket");
	    j++;
	    if(!isUnfilledSheet) {
	    	rowhead.createCell((int) j).setCellValue("Cost Per ticket");
	    	j++;
	    }
	    rowhead.createCell((int) j).setCellValue("Invoice Total");
	    j++;
	    if(!isUnfilledSheet) {
		    rowhead.createCell((int) j).setCellValue("Total Cost");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Total Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("CC Fees");
		    j++;
		   /* rowhead.createCell((int) j).setCellValue("TN Fees");
		    j++;*/
		    rowhead.createCell((int) j).setCellValue("Net Sold Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Gross Margin %");
		    j++;
	    }
	    rowhead.createCell((int) j).setCellValue("Customer Type");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Customer Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Customer Email");
	    j++;
	    rowhead.createCell((int) j).setCellValue(" Phone Number");
	    j++;
   	
	   	/*rowhead.createCell((int) j).setCellValue("Street Address1");
	   	j++;
	   	rowhead.createCell((int) j).setCellValue("Street Address2");
	   	j++;
	   	rowhead.createCell((int) j).setCellValue("City");
	   	j++;
	   	rowhead.createCell((int) j).setCellValue("State");
	   	j++;*/
	    
	    CellStyle cellamountStyle = workbook.createCellStyle();
	    cellamountStyle.setDataFormat(
	    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
	    
	    
	    int i=1,totalQty=0;
	    Double totalCost =0.0,totalAcutalPrice=0.0,totalRetailPrice=0.0,tgCommission=0.0,wholeSalePrice=0.0,tnfees=0.0 ,actualProfitLoss=0.0;
	    Double totalQtyCost=0.0,totalQtyActualPrice=0.0,cost=0.0,actualSoldPrice=0.0,totalTnFees=0.0,
	    totalActualProfitLoss=0.0,tnCommissionPerc=0.0,invoiceTotal=0.00,totalccFees=0.00,grossMargin=0.0,netSoldPrice=0.0,totalNetSoldPrice=0.0;
	    Double ccFeesPerc=0.0,ccFees=0.0,totSoldPrice=0.0;
	    
		for (RTFTicketGroup rtfTix :rtfTixGroups) {
			
			totalQty=totalQty+rtfTix.getQuantity();
			//totalAcutalPrice=totalAcutalPrice+ticketGroups.getWholesalePrice();
			totalCost=totalCost+rtfTix.getCost();
			totalRetailPrice=totalRetailPrice+rtfTix.getRetailPrice();
			wholeSalePrice=wholeSalePrice+rtfTix.getWholesalePrice();
			//tgCommission=tgCommission+ticketGroups.getTgCommission();

			totSoldPrice = totSoldPrice + rtfTix.getSoldPrice();
			cost = rtfTix.getQuantity() * rtfTix.getCost();
			actualSoldPrice = rtfTix.getQuantity() * rtfTix.getSoldPrice();
			invoiceTotal = invoiceTotal + rtfTix.getInvoiceTotal();
			
			//tnCommissionPerc = 3.00;//3.35;
			//tnfees = (actualSoldPrice * tnCommissionPerc ) / 100;
			
			ccFeesPerc = 3.00;//3.35;
			ccFees = (actualSoldPrice * ccFeesPerc ) / 100;
			
			//ccFees = (ticketGroups.getInvoiceTotal() * ccFeePerc) / 100;
			actualProfitLoss = actualSoldPrice - (cost + tnfees + ccFees);
			actualProfitLoss = Math.round(actualProfitLoss * 100.0) / 100.0;
			
			if(actualSoldPrice == 0) {
				grossMargin = 0.0;
			} else {
				grossMargin = Math.round((actualProfitLoss * 100 / actualSoldPrice)*100.0)/100.0;						
			}
			
			netSoldPrice = actualSoldPrice - (tnfees+ccFees); 
			
			
			totalTnFees = totalTnFees + tnfees;
			totalccFees = totalccFees + ccFees;
			totalActualProfitLoss = totalActualProfitLoss + actualProfitLoss;
			totalQtyCost = totalQtyCost+cost;
			totalQtyActualPrice = totalQtyActualPrice+actualSoldPrice;
			totalNetSoldPrice = totalNetSoldPrice + netSoldPrice;
			
			//System.out.println("Broker:"+ticketGroups.getTmatBrokerName()+", Invoice ID:"+ticketGroups.getInvoiceId()+", Invoice Total:"+ticketGroups.getInvoiceTotal()+", Sold Price:"+actualSoldPrice+", Cost:"+cost+/*", CCFees:"+ccFees+*/", TnFees:"+tnfees+", ProfitLoss:"+actualProfitLoss);
			
			int column = 0;
			
			if(i%1000 == 0) {

			}
			
			Row row = ssSheet.createRow(i);
	        row.createCell((int) column).setCellValue(rtfTix.getInvoiceId());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getInvoiceDateStr());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getOrderId());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getInvoiceStatus());
	        column++;
	        
	        //Show po date in RTW P&L Filled Report
		    if(showPODate) {
		    	if(rtfTix.getPurchaseOrderId() != null) {
		    		row.createCell((int) column).setCellValue(rtfTix.getPurchaseOrderId());
			        column++;
		    	} else {
		    		row.createCell((int) column).setCellValue("");
			        column++;
		    	}
		    	if(rtfTix.getPurchaseOrderDateStr() != null) {
		    		row.createCell((int) column).setCellValue(rtfTix.getPurchaseOrderDateStr());
			        column++;
		    	} else {
		    		row.createCell((int) column).setCellValue("");
			        column++;
		    	}
		    	
		    }
	        row.createCell((int) column).setCellValue(rtfTix.getSalesPerson());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getEventName());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getEventDateStr());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getEventTimeStr());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getVenue());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getInternalNotes());
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getSection());
	        column++;
	        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        rowCell.setCellValue((rtfTix.getRow() != null)?rtfTix.getRow():"");
	        column++;
	        String seat="";
	        if(rtfTix.getSeatLow() != null && rtfTix.getSeatLow().length() >0) {
	        	seat = ""+rtfTix.getSeatLow();
	        }
	        if(rtfTix.getSeatHigh() != null && rtfTix.getSeatHigh().length()  > 0) {
	        	if(!seat.equals("")) {
	        		seat = seat + "-"+rtfTix.getSeatHigh();
	        	} else {
	        		seat = ""+rtfTix.getSeatHigh();
	        	}
	        }
	        rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        rowCell.setCellValue(seat);
	        column++;
	        
	        row.createCell((int) column).setCellValue((rtfTix.getSectionRange() != null)?rtfTix.getSectionRange():"");
	        column++;
	        row.createCell((int) column).setCellValue((rtfTix.getRowRange() != null)?rtfTix.getRowRange():"");
	        column++;
	        row.createCell((int) column).setCellValue(rtfTix.getQuantity());
	        column++;
	        
	        row.createCell((int) column).setCellValue(rtfTix.getSoldPrice());
	        column++;
	        if(!isUnfilledSheet) {
		        row.createCell((int) column).setCellValue(rtfTix.getCost());
		        column++;
	        }
	        row.createCell((int) column).setCellValue(rtfTix.getInvoiceTotal());
		    column++;
		    if(!isUnfilledSheet) {
			    Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(cost.toString()));//Cost
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
			    amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(actualSoldPrice.toString()));//Total Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		    	
		    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(ccFees.toString()));//CC Fees
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		    	
		    	/*amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(tnfees.toString()));//TN Fees
		        amountCell.setCellStyle(cellamountStyle);
		        column++;*/
		    	
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(netSoldPrice.toString()));//Net Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		    	
		    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(actualProfitLoss.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(grossMargin.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		    }
	        
	        row.createCell((int) column).setCellValue(rtfTix.getCustomerName());
	    	column++;
	    	row.createCell((int) column).setCellValue(rtfTix.getCustomerEmail());
	    	column++;
	    	row.createCell((int) column).setCellValue(rtfTix.getCustomerPhone());
	    	column++;
	    	
	    	/*row.createCell((int) column).setCellValue(rtfTix.getStreetAddress1());
	    	column++;
	    	row.createCell((int) column).setCellValue(rtfTix.getStreetAddress2());
	    	column++;
	    	row.createCell((int) column).setCellValue(rtfTix.getCity());
	    	column++;
	    	row.createCell((int) column).setCellValue(rtfTix.getState());
	    	column++;*/
	    	
	        i++;
	        
		}
		
		if(summaryMap != null) {
			summaryMap.put("ProfitLoss_"+sheetName, totalActualProfitLoss);
			summaryMap.put("ActualSoldPrice_"+sheetName, totalQtyActualPrice);
			summaryMap.put("NetSoldPrice_"+sheetName, totalNetSoldPrice);
		}
		
		j=0;
		
		if(null != rtfTixGroups  && rtfTixGroups.size() >0){
			Row row = ssSheet.createRow(i);
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        if(showPODate) {
	        	row.createCell((int) j).setCellValue("");
		        j++;
		        row.createCell((int) j).setCellValue("");
		        j++;
	        }
       	 	row.createCell((int) j).setCellValue("");
		     j++;
		     row.createCell((int) j).setCellValue("");
		     j++;
		     
		     row.createCell((int) j).setCellValue("");
		     j++;
		     row.createCell((int) j).setCellValue("");
		     j++;
		     
		     row.createCell((int) j).setCellValue("");
		     j++;
		     
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        Cell seatCell = row.createCell(j,Cell.CELL_TYPE_STRING);
	        seatCell.setCellValue("TOTAL");
	        j++;
	        row.createCell((int) j).setCellValue(totalQty);
	        j++;
	        Cell amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(totSoldPrice.toString()));//Actual Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        j++;
	        
	        if(!isUnfilledSheet) {
		        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalCost.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
	        }
	        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(invoiceTotal.toString()));//Actual Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        j++;

	        if(!isUnfilledSheet) {
		    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalQtyCost.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
		    	
	
			    amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalQtyActualPrice.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
		    	
		    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalccFees.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
		    	
		    	
		    	/*amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalTnFees.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;*/
		        
		        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalNetSoldPrice.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
		    	
		    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalActualProfitLoss.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
		        
		        Double totalGrossMargin = 0.0;
		        if(totalQtyActualPrice == 0) {
		        	totalGrossMargin = 0.0;
				} else {
					totalGrossMargin = Math.round((totalActualProfitLoss * 100 / totalQtyActualPrice)*100.0)/100.0;						
				}
		        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(Double.valueOf(totalGrossMargin.toString()));//Actual Sold Price
		        amountCell.setCellStyle(cellamountStyle);
		        j++;
	        }
	        
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
		    
	        /*row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;
	        row.createCell((int) j).setCellValue("");
	        j++;*/
		}
		
	}
		public ModelAndView loadRtwProfitAndLossFilledReport(HttpServletRequest request, HttpServletResponse response){
			ModelAndView mav = new ModelAndView("page-tmat-admin-rtw-profit-loss-filled-report");
			try{
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);//show yesterday date
				
				mav.addObject("fromDate", "1");
				mav.addObject("fromMonth", "1");
				mav.addObject("fromYear", cal.get(Calendar.YEAR));
				mav.addObject("toDate", cal.get(Calendar.DAY_OF_MONTH));
				mav.addObject("toMonth", cal.get(Calendar.MONTH)+1);
				mav.addObject("toYear", cal.get(Calendar.YEAR));
			}catch(Exception e){
				e.printStackTrace();
			}
			return mav;
		}
		public void downloadRtwProfitAndLossFilledReport(HttpServletRequest request, HttpServletResponse response){
			try{
				String fromMonth = request.getParameter("fromMonth");
				String fromDay = request.getParameter("fromDay");
				String fromYear = request.getParameter("fromYear");
				String fromDate = fromYear + "-" + fromMonth + "-" + fromDay + " 00:00:00.000";
				
				String toMonth = request.getParameter("toMonth");
				String toDay = request.getParameter("toDay");
				String toYear = request.getParameter("toYear");
				String toDate = toYear + "-" + toMonth + "-" + toDay + " 23:59:59.999";
				String brokerStr = request.getParameter("broker");
				String fileNameStr = "";
				
				List<String> brokersList = new ArrayList<String>();
				if(brokerStr != null) {
					if(brokerStr.equals("RTW")) {
						brokersList.add("RTW");
						fileNameStr = "RTW";
					} else if(brokerStr.equals("RTW-2")) {
						brokersList.add("RTW-2");
						fileNameStr = "RTW-2";
					} else {
						brokersList.add("RTW");
						brokersList.add("RTW-2");
						fileNameStr = "RTW_RTW-2";
					}
				}
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				String fileName = fileNameStr+"_ProfitAndLossFilledReport-From-"+ fromMonth + "/" +fromDay + "/" + fromYear + "-To-" + 
								  toMonth +"/" + toDay + "/" + toYear + ".xlsx";
				System.out.println(fileName);
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition", "attachment; filename="+fileName);
				getRTFFilledInvoiceList(fromDate, toDate, workbook,brokersList);
				workbook.write(response.getOutputStream());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		public static void getRTFFilledInvoiceList(String startDateStr,String endDateStr, SXSSFWorkbook workbook,List<String> brokersList) throws Exception{
			
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			 
			/*List<String> brokersList = new ArrayList<String>();
			brokersList.add("RTW");
			brokersList.add("RTW-2");*/
			
			List<POSTicketGroup> isCatsInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isZonesInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isLongInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isSHTEInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isRetailInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isUnfilledInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isWholeSaleCSRInvoices = new ArrayList<POSTicketGroup>();
			 
			 
			 for (String brokerName : brokersList) {
				
			 
			 Map<Integer, PosEvent> mzTixLocalEventObjMap = new HashMap<Integer, PosEvent>();
			 
			 List<PosEvent> mzTixEvents = DAORegistry.getQueryManagerDAO().getAllLocalAEvents(Connections.getPosConnectionByBrokername(brokerName));
			 
			 for (PosEvent mzTixEvent : mzTixEvents) {
				 mzTixLocalEventObjMap.put(mzTixEvent.getId(), mzTixEvent);
			 }
			 System.out.println("RTW : Total Number of pos active local events...."+mzTixEvents.size());
			 
		 	/*Map<Integer, PosEvent> jktExchangeEventMap = new HashMap<Integer, PosEvent>();
			List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getAllEventsWithExchangeEvent(Connections.getPosConnectionByBrokername(brokerName));
			System.out.println("RTW : Total Number of pos active events...."+posEvents.size());
			
			for (PosEvent posEvent : posEvents) {
				jktExchangeEventMap.put(posEvent.getExchangeEventId(), posEvent);
			}
			
			if(jktExchangeEventMap.isEmpty()){
				System.out.println("RTW : There is no exchange event found in pos .");
			}*/
			
			//List<PurchaseOrder> purchaseORderList = DAORegistry.getQueryManagerDAO().getRTWFulfiledPurchaseOrderList(startDateStr, endDateStr, Connections.getPosConnectionByBrokername(brokerName));
			//int poSize = purchaseORderList.size(),currentSize=0; 
			//String missingPO = "";
			//String missingInvoice = "";
			//System.out.println("RTW P&L FILLED : "+brokerName+" : Total PO------->"+poSize);
			
			//for (PurchaseOrder purchaseOrder : purchaseORderList) {

				//currentSize++;
				 //System.out.println("PL FILLED : "+brokerName+" : "+poSize+"=====================PROCESSING======================>"+currentSize+" : PO Id : "+purchaseOrder.getPurchaseOrderId());
				 
				List<Invoice> invoices = DAORegistry.getQueryManagerDAO().getRTWFilledProfitandLossReportInvoiceList(startDateStr,endDateStr,Connections.getPosConnectionByBrokername(brokerName));
				String missingInvoice = "";
				int invoiceSize = invoices.size(),currentSize=0;
				System.out.println(brokerName+" : RTW P&L FILLED Total Invoices------->"+invoiceSize);
			 
			 for (Invoice invoice : invoices) {
				 
				 //List<POSTicketGroup> invoiceTicketGroups = new ArrayList<POSTicketGroup>();
				 
				 currentSize++;
				 //System.out.println(brokerName+" : "+invoiceSize+"=====================PROCESSING======================>"+currentSize+" : invoice ID : "+invoice.getInvoiceId());
				 
				 //System.out.println(brokerName+" : InvoiceId----->"+invoice.getInvoiceId());
				 
				 int rtwComission = 9;
				
				 
				 if(invoice.getInvoiceId().equals(24854)){
					 System.out.println(invoice.getInvoiceId());
				 }
				 
					 List<POSTicketGroup> soldTicketgroups = DAORegistry.getQueryManagerDAO().
					 getRTWShortSalesByInvoiceIdandFillDateRange(invoice.getInvoiceId(),startDateStr,endDateStr,Connections.getPosConnectionByBrokername(brokerName));
					 
					// POSTicketGroup ticketGroup  =  null;
					 if(soldTicketgroups != null && !soldTicketgroups.isEmpty() && soldTicketgroups.size() > 0){
						 
						 for (POSTicketGroup ticketGroup : soldTicketgroups) {
							
						 //ticketGroup  = soldTicketgroups.get(0);
						 //PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
						 //if(null == mzTixEvent){
							// mzTixEvent =new PosEvent();
							 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
						 //}
						 
						 ticketGroup.setInvoiceId(invoice.getInvoiceId());
						 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
						 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
						 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
						 ticketGroup.setEventName(mzTixEvent.getEventName());
						 ticketGroup.setVenueName(mzTixEvent.getVenueName());
						 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
						 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
						 ticketGroup.setIsVoidedInvoice("NO");
						 ticketGroup.setInvoiceVoidDateStr("");
						 
						 
						 if(null != ticketGroup.getSection() && (ticketGroup.getSection().contains("ZONE") || ticketGroup.getSection().contains("zone")
								 || ticketGroup.getSection().contains("Zone"))){
							 ticketGroup.setIsZones(true);
						 }else{
							 ticketGroup.setIsCats(true);
						 }
						 
						 if(null != ticketGroup.getFillDate()){
							 ticketGroup.setFillDateStr(xlDateFormat.format(ticketGroup.getFillDate()));
						 }
						 
						 
						 if(null != ticketGroup.getActualWholeSalePrice() && ticketGroup.getActualWholeSalePrice() > 0){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualWholeSalePrice());
						 }
						 
						 String seat = "";
						 
						 if(null != ticketGroup.getCatSeatLow() && !ticketGroup.getCatSeatLow().isEmpty() &&
								 !ticketGroup.getCatSeatLow().equals("")){
							 
							 seat = ticketGroup.getCatSeatLow();
							 
							 if(null != ticketGroup.getCatSeatHigh() && !ticketGroup.getCatSeatHigh().isEmpty() &&
									 !ticketGroup.getCatSeatHigh().equals("")){
								 seat = seat +"-"+ticketGroup.getCatSeatHigh();
							 }
						 }
						 Double price = ticketGroup.getWholesalePrice();
						 if(price > 0){
							 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
						 }else{
							 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
						 }
						 
						 if(ticketGroup.getWholesalePrice() <= 0 || 
								 ticketGroup.getWholesalePrice().equals(0.00)){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 }
						 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*rtwComission)/100);				 
						 ticketGroup.setSeat(seat);
						 
						 
						 List<PosTicket> posTickets = new ArrayList<PosTicket>();
						 
						 //if invoice is mapped with category ticket then select tickets only that categorytickets related tickets. 
						 posTickets = DAORegistry.getQueryManagerDAO().getJKTTicketsByInvoiceIdandFillDateRange(invoice.getInvoiceId(),ticketGroup.getJktPoId(),startDateStr,endDateStr, Connections.getPosConnectionByBrokername(brokerName));
						 
						 Map<Integer, List<PosTicket>> ticketGroupMap =new HashMap<Integer, List<PosTicket>>();
						 
						 for (PosTicket ticket : posTickets) {
							 
							 List<PosTicket> tempCatTickets = ticketGroupMap.get(ticket.getTicketGroupId());
							 
							 if(null != tempCatTickets && !tempCatTickets.isEmpty()){
								 ticketGroupMap.get(ticket.getTicketGroupId()).add(ticket);
							 }else{
								 tempCatTickets = new ArrayList<PosTicket>();
								 tempCatTickets.add(ticket);
								 ticketGroupMap.put(ticket.getTicketGroupId(), tempCatTickets);
							 }
						 }
						 
						 
						 for (Integer ticketGroupId : ticketGroupMap.keySet()) {
								
							 List<PosTicket> tickets = ticketGroupMap.get(ticketGroupId);
							 
							 if(null != ticketGroup ){
								 
								 POSTicketGroup ticketGroup2 = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
								 price = ticketGroup2.getCost();
								 ticketGroup.setCost(price);
								 if(price > 0){
									 ticketGroup.setTotalCost(tickets.size() * price);
								 }else{
									 ticketGroup.setTotalCost(tickets.size() * tickets.get(0).getActualSoldPrice());
								 }
								 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
								 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
								// ticketGroup.setPurchaseOrderDate(tickets.get(0).getPurchaseOrderDate());
								 
							 }
						 }
						 if(null != ticketGroup){
							 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
							 ticketGroup.setClientBrokerId(clientBroker.getClientId());
							 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
							 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
							 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
							 ticketGroup.setCity(clientBroker.getCity());
							 ticketGroup.setState(clientBroker.getState());
							 ticketGroup.setCountry(clientBroker.getCountry());
							 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
							 ticketGroup.setClientMail(clientBroker.getClientMail());
							 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
							 ticketGroup.setClientType(clientBroker.getClientType());
							 
							 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
							 ticketGroup.setTmatBrokerName(brokerName);
							 
							 //ticketGroup.setJktPoId(purchaseOrder.getPurchaseOrderId());
							 ticketGroup.setPurchaseOrderDateStr(xlDateFormat.format(ticketGroup.getPurchaseOrderDate()));
							 
							 
							 boolean isDeadWood = false,isrewardthefan = false;
							 if(ticketGroup.getSalesPerson() != null && 
									 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
								 isDeadWood = true; 
							 }
							 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
								 if(ticketGroup.getClientCompanyName() != null && 
										 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true;
								 }
								 if(ticketGroup.getClientCompanyName() != null && 
										 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
									 isrewardthefan = true;
								 }
							 } else {
								 if(clientBroker.getClientFirstName() != null && 
										 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true;
								 }
								 if(clientBroker.getClientFirstName() != null && 
										 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
									 isrewardthefan = true;
								 }
							 }
							 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
							 
							 if(isDeadWood) {
								 ticketGroup.setWholesalePrice(0.0);
								 ticketGroup.setActualSoldPrice(0.0);
								 ticketGroup.setRetailPrice(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
							 }
							 /*if(isrewardthefan) {
								 ticketGroup.setWholesalePrice(0.0);
								 ticketGroup.setRetailPrice(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
								 ticketGroup.setCost(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
							 }*/
							//Do not consider rewardthefan invoices in profit loss and filled reports
							 if(isrewardthefan) {
								continue; 
							 }
							 
							 if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
									 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
								 ticketGroup.setIsRetail(true);
								 
							 }else if (invoice.getSystemUserId() == 37 ){
								 ticketGroup.setIsWholeSaleCsr(true);
							 }
							 
							 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
								 
								 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
									 ticketGroup.setIsTnow(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
										 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
									 ticketGroup.setIsSH(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
										 ticketGroup.getClientBrokerId() == 3761){
									 ticketGroup.setIsTE(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 } else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
									 ticketGroup.setIsLong(false);
									 ticketGroup.setIsSH(false);
									 ticketGroup.setIsTE(false);
									 ticketGroup.setIsTnow(false);
									 ticketGroup.setIsZones(false);
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
									 ticketGroup.setIsCats(true);
									 
									 ticketGroup.setIsRightThisWayOrder(true);
								 }
							 }
							 
							 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
								 
								 ticketGroup.setIsLong(false);
								 ticketGroup.setIsSH(false);
								 ticketGroup.setIsTE(false);
								 ticketGroup.setIsTnow(false);
								 ticketGroup.setIsZones(false);
								 
							 }
							 
							 
							 	if(ticketGroup.getIsUnfilled()){
							 		isUnfilledInvoices.add(ticketGroup);
							 	} else if(ticketGroup.getIsSH() || ticketGroup.getIsTE() || ticketGroup.getIsTnow()){
							 		
							 		//Tamil : for TNow,TEvol and Stubhub tab consider actual sold price as sold price instead of considering wholesale price
									ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
									
									isSHTEInvoices.add(ticketGroup);
								} else if(ticketGroup.getIsRetail()){

							 		//Tamil : for Retail tab consider actual sold price as sold price instead of considering wholesale price
									 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
									 
							 		isRetailInvoices.add(ticketGroup);
								}else if(ticketGroup.getIsWholeSaleCsr()){
							 		isWholeSaleCSRInvoices.add(ticketGroup);
								}else if(ticketGroup.getIsLong()){
									isLongInvoices.add(ticketGroup);
								}else if(ticketGroup.getIsZones()){
									isZonesInvoices.add(ticketGroup);
								}else{
									isCatsInvoices.add(ticketGroup);
								}
								
						}/*else{
							
							missingInvoice = missingInvoice +","+invoice.getInvoiceId();
						}*/
						 }
					 } else {
						 
					 List<POSTicketGroup> posTicketGroups = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByInvoiceId(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 
					 if(posTicketGroups != null && !posTicketGroups.isEmpty()) {
						 
						 for (POSTicketGroup ticketGroup : posTicketGroups) {
							 
							// PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
							 //if(null == mzTixEvent){
								// mzTixEvent =new PosEvent();
							 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
							 //}
							 
							 ticketGroup.setInvoiceId(invoice.getInvoiceId());
							 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
							 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
							 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
							 ticketGroup.setEventName(mzTixEvent.getEventName());
							 ticketGroup.setVenueName(mzTixEvent.getVenueName());
							 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
							 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 if( null != invoice.getInvoiceVoidDate() ){
								 ticketGroup.setInvoiceVoidDateStr(xlDateFormat.format(invoice.getInvoiceVoidDate()));
							 }
							 ticketGroup.setFillDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 //ticketGroup.setQty(tickets.size());
							 ticketGroup.setIsLong(true);
							 //for long inventory invoice createe date is the fill date
							 ticketGroup.setFillDate(invoice.getCreateDate());
							 
							 String seat = ticketGroup.getSeatLow()+"-"+ticketGroup.getSeatHigh();
							 
							 if(ticketGroup.getWholesalePrice() <= 0 || 
									 ticketGroup.getWholesalePrice().equals(0.00)){
								 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 }
							 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 
							 //ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 //ticketGroup.setPurchaseOrderDate(tickets.get(0).getPurchaseOrderDate());
							 
							 Double price = ticketGroup.getCost();
							 if(price > 0){
								 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
							 }else{
								 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setSeat(seat);
						
							 if(null != ticketGroup){
								 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
								 ticketGroup.setClientBrokerId(clientBroker.getClientId());
								 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
								 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
								 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
								 ticketGroup.setCity(clientBroker.getCity());
								 ticketGroup.setState(clientBroker.getState());
								 ticketGroup.setCountry(clientBroker.getCountry());
								 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
								 ticketGroup.setClientMail(clientBroker.getClientMail());
								 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
								 ticketGroup.setClientType(clientBroker.getClientType());
								 
								 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
								 ticketGroup.setTmatBrokerName(brokerName);
								 
								 //ticketGroup.setJktPoId(purchaseOrder.getPurchaseOrderId());
								 ticketGroup.setPurchaseOrderDateStr(xlDateFormat.format(ticketGroup.getPurchaseOrderDate()));
								 
								 boolean isDeadWood = false,isrewardthefan = false;
								 if(ticketGroup.getSalesPerson() != null && 
										 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true; 
								 }
								 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
									 if(ticketGroup.getClientCompanyName() != null && 
											 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
										 isDeadWood = true;
									 }
									 if(ticketGroup.getClientCompanyName() != null && 
											 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
										 isrewardthefan = true;
									 }
								 } else {
									 if(clientBroker.getClientFirstName() != null && 
											 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
										 isDeadWood = true;
									 }
									 if(clientBroker.getClientFirstName() != null && 
											 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
										 isrewardthefan = true;
									 }
								 }
								 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
								 
								 if(isDeadWood) {
									 ticketGroup.setWholesalePrice(0.0);
									 ticketGroup.setActualSoldPrice(0.0);
									 ticketGroup.setRetailPrice(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
								 }
								 /*if(isrewardthefan) {
									 ticketGroup.setWholesalePrice(0.0);
									 ticketGroup.setRetailPrice(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
									 ticketGroup.setCost(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
								 }*/
								 //Do not consider rewardthefan invoices in profit loss and filled reports
								 if(isrewardthefan) {
									continue; 
								 }
								 
								 if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
										 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
									 ticketGroup.setIsRetail(true);
									 
								 }else if (invoice.getSystemUserId() == 37 ){
									 ticketGroup.setIsWholeSaleCsr(true);
								 }
								 
								 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
									 
									 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
										 ticketGroup.setIsTnow(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
											 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
										 ticketGroup.setIsSH(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
											 ticketGroup.getClientBrokerId() == 3761){
										 ticketGroup.setIsTE(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
										 ticketGroup.setIsLong(false);
										 ticketGroup.setIsSH(false);
										 ticketGroup.setIsTE(false);
										 ticketGroup.setIsTnow(false);
										 ticketGroup.setIsZones(false);
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
										 ticketGroup.setIsCats(true);
										 
										 ticketGroup.setIsRightThisWayOrder(true);
									 }
								 }
								 
								 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
									 
									 ticketGroup.setIsLong(false);
									 ticketGroup.setIsSH(false);
									 ticketGroup.setIsTE(false);
									 ticketGroup.setIsTnow(false);
									 ticketGroup.setIsZones(false);
									 
								 }
								 
								 	if(ticketGroup.getIsUnfilled()){
								 		isUnfilledInvoices.add(ticketGroup);
								 	}else if(ticketGroup.getIsSH() || ticketGroup.getIsTE() || ticketGroup.getIsTnow()){
								 		
								 		//Tamil : for TNow,TEvol and Stubhub tab consider actual sold price as sold price instead of considering wholesale price
										ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
										
										isSHTEInvoices.add(ticketGroup);
									} else if(ticketGroup.getIsRetail()){

								 		//Tamil : for Retail tab consider actual sold price as sold price instead of considering wholesale price
										 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
										 
								 		isRetailInvoices.add(ticketGroup);
									}else if(ticketGroup.getIsWholeSaleCsr()){
								 		isWholeSaleCSRInvoices.add(ticketGroup);
									} else if(ticketGroup.getIsLong()){
										isLongInvoices.add(ticketGroup);
									}else if(ticketGroup.getIsZones()){
										isZonesInvoices.add(ticketGroup);
									}else{
										isCatsInvoices.add(ticketGroup);
									}
									
							}
						}
					 } else {
							missingInvoice = missingInvoice +","+invoice.getInvoiceId();
					 }
					 
					 }
					 
								
					 
					//invoiceTicketGroups.add(ticketGroup);
				
					
				
			}
		//System.out.println("P&L FILLED broker : "+brokerName+" : missing PO : "+missingPO);
		System.out.println("P&L FILLED broker : "+brokerName+" : missing Invoices : "+missingInvoice);
		}
			 
			 //SXSSFWorkbook workbook = new SXSSFWorkbook();
			 
			 List<String> sheetNameList = new ArrayList<String>();
			 
			 Map<String,Double> summaryMap = new HashMap<String, Double>();
			 String sheetName = "TN Retail";
			 generateJKTWeeklyInvoiceSheet(workbook, isRetailInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "SH and TE and TNow";
			 generateJKTWeeklyInvoiceSheet(workbook, isSHTEInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Long";
			 generateJKTWeeklyInvoiceSheet(workbook, isLongInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Cats";
			 generateJKTWeeklyInvoiceSheet(workbook, isCatsInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Zones";
			 generateJKTWeeklyInvoiceSheet(workbook, isZonesInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "Wholesale CSR";
			 generateJKTWeeklyInvoiceSheet(workbook, isWholeSaleCSRInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 //generateUnfilledInvoiceSheet(workbook, isUnfilledInvoices,"Unfilled Orders",false,false,false,true);
			
			 
			 CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			 SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet("Summary");
			 int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("Type");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Sold Price");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Net Sold Price");
			 j++;
			 
			 Double grandTotal = 0.0,totalActualSoldPrice=0.0,totalNetSoldPrice=0.0;
			 int i = 1;
			
			 for (String tempSheetName : sheetNameList) {
				 Row row = ssSheet.createRow(i);
				 row.createCell((int) 0).setCellValue(tempSheetName);
					
				Double profitloss = summaryMap.get("ProfitLoss_"+tempSheetName);
				profitloss= (profitloss==null)? 0.0:profitloss;
				grandTotal += profitloss;
				
		        Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(profitloss);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double actualSoldPrice = summaryMap.get("ActualSoldPrice_"+tempSheetName);
		        actualSoldPrice= (actualSoldPrice==null)? 0.0:actualSoldPrice;
		        totalActualSoldPrice += actualSoldPrice;
		        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(actualSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double netSoldPrice = summaryMap.get("NetSoldPrice_"+tempSheetName);
		        netSoldPrice= (netSoldPrice==null)? 0.0:netSoldPrice;
				totalNetSoldPrice += netSoldPrice;
		        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(netSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        i++;
			}
	        
	        Row row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Total");
			Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(grandTotal);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalActualSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalNetSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        
			 
			 
			 /*FileOutputStream fileOut = new FileOutputStream("C:\\UNCAT\\RTWProfitLossReportFinal_1.xlsx");
			 workbook.write(fileOut);
		     fileOut.close();*/
		     
			 System.out.println("P&L FILLED RTW : FILLED JOB COMPLETED SUCESSFULLY........");
			 
			 //return workbook;
		 }
		
		public static void getRTFFilledInvoiceListForNY(String startDateStr,String endDateStr, SXSSFWorkbook workbook,List<String> brokersList) throws Exception{
			
			DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			 
			/*List<String> brokersList = new ArrayList<String>();
			brokersList.add("RTW");
			brokersList.add("RTW-2");*/
			
			List<POSTicketGroup> isCatsInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isZonesInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isLongInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isSHTEInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isRetailInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isUnfilledInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isWholeSaleCSRInvoices = new ArrayList<POSTicketGroup>();
			 List<POSTicketGroup> isSSOrderInvoices = new ArrayList<POSTicketGroup>();
			 
			 
			 for (String brokerName : brokersList) {
				
			 
			 Map<Integer, PosEvent> mzTixLocalEventObjMap = new HashMap<Integer, PosEvent>();
			 
			 List<PosEvent> mzTixEvents = DAORegistry.getQueryManagerDAO().getAllLocalAEvents(Connections.getPosConnectionByBrokername(brokerName));
			 
			 for (PosEvent mzTixEvent : mzTixEvents) {
				 mzTixLocalEventObjMap.put(mzTixEvent.getId(), mzTixEvent);
			 }
			 System.out.println("RTW : Total Number of pos active local events...."+mzTixEvents.size());
			 
		 	Map<Integer, PosEvent> jktExchangeEventMap = new HashMap<Integer, PosEvent>();
			List<PosEvent> posEvents = DAORegistry.getQueryManagerDAO().getAllEventsWithExchangeEvent(Connections.getPosConnectionByBrokername(brokerName));
			System.out.println("RTW : Total Number of pos active events...."+posEvents.size());
			
			for (PosEvent posEvent : posEvents) {
				jktExchangeEventMap.put(posEvent.getExchangeEventId(), posEvent);
			}
			
			if(jktExchangeEventMap.isEmpty()){
				System.out.println("RTW : There is no exchange event found in pos .");
			}
			
			Map<Integer,Boolean> ssListingCsvOrderIdsMap = new HashMap<Integer, Boolean>();
			List<Integer> ssListingCsvOrderIds = DAORegistry.getQueryManagerDAO().getAllSSListingCsvOrderIdsForNY(Connections.getPosConnectionByBrokername(brokerName));
			for (Integer orderId : ssListingCsvOrderIds) {
				ssListingCsvOrderIdsMap.put(orderId, Boolean.TRUE);
			}
			//List<PurchaseOrder> purchaseORderList = DAORegistry.getQueryManagerDAO().getRTWFulfiledPurchaseOrderList(startDateStr, endDateStr, Connections.getPosConnectionByBrokername(brokerName));
			//int poSize = purchaseORderList.size(),currentSize=0; 
			//String missingPO = "";
			//String missingInvoice = "";
			//System.out.println("RTW P&L FILLED : "+brokerName+" : Total PO------->"+poSize);
			
			//for (PurchaseOrder purchaseOrder : purchaseORderList) {

				//currentSize++;
				 //System.out.println("PL FILLED : "+brokerName+" : "+poSize+"=====================PROCESSING======================>"+currentSize+" : PO Id : "+purchaseOrder.getPurchaseOrderId());
				 
				List<Invoice> invoices = DAORegistry.getQueryManagerDAO().getRTWFilledProfitandLossReportInvoiceListForNY(startDateStr,endDateStr,Connections.getPosConnectionByBrokername(brokerName));
				String missingInvoice = "";
				int invoiceSize = invoices.size(),currentSize=0;
				System.out.println(brokerName+" : RTW P&L FILLED Total Invoices------->"+invoiceSize);
			 
			 for (Invoice invoice : invoices) {
				 
				 //List<POSTicketGroup> invoiceTicketGroups = new ArrayList<POSTicketGroup>();
				 
				 currentSize++;
				 //System.out.println(brokerName+" : "+invoiceSize+"=====================PROCESSING======================>"+currentSize+" : invoice ID : "+invoice.getInvoiceId());
				 
				 //System.out.println(brokerName+" : InvoiceId----->"+invoice.getInvoiceId());
				 
				 int rtwComission = 9;
				
				 
				 if(invoice.getInvoiceId().equals(24854)){
					 System.out.println(invoice.getInvoiceId());
				 }
				 
					 List<POSTicketGroup> soldTicketgroups = DAORegistry.getQueryManagerDAO().
					 getRTWShortSalesByInvoiceIdandFillDateRangeForNY(invoice.getInvoiceId(),startDateStr,endDateStr,Connections.getPosConnectionByBrokername(brokerName));
					 
					// POSTicketGroup ticketGroup  =  null;
					 if(soldTicketgroups != null && !soldTicketgroups.isEmpty() && soldTicketgroups.size() > 0){
						 
						 for (POSTicketGroup ticketGroup : soldTicketgroups) {
							
						 //ticketGroup  = soldTicketgroups.get(0);
						 //PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
						 //if(null == mzTixEvent){
							// mzTixEvent =new PosEvent();
							 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
						 //}
						 
						 ticketGroup.setInvoiceId(invoice.getInvoiceId());
						 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
						 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
						 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
						 ticketGroup.setEventName(mzTixEvent.getEventName());
						 ticketGroup.setVenueName(mzTixEvent.getVenueName());
						 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
						 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
						 ticketGroup.setIsVoidedInvoice("NO");
						 ticketGroup.setInvoiceVoidDateStr("");
						 
						 
						 if(null != ticketGroup.getSection() && (ticketGroup.getSection().contains("ZONE") || ticketGroup.getSection().contains("zone")
								 || ticketGroup.getSection().contains("Zone"))){
							 ticketGroup.setIsZones(true);
						 }else{
							 ticketGroup.setIsCats(true);
						 }
						 
						 if(null != ticketGroup.getFillDate()){
							 ticketGroup.setFillDateStr(xlDateFormat.format(ticketGroup.getFillDate()));
						 }
						 
						 
						 if(null != ticketGroup.getActualWholeSalePrice() && ticketGroup.getActualWholeSalePrice() > 0){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualWholeSalePrice());
						 }
						 
						 String seat = "";
						 
						 if(null != ticketGroup.getCatSeatLow() && !ticketGroup.getCatSeatLow().isEmpty() &&
								 !ticketGroup.getCatSeatLow().equals("")){
							 
							 seat = ticketGroup.getCatSeatLow();
							 
							 if(null != ticketGroup.getCatSeatHigh() && !ticketGroup.getCatSeatHigh().isEmpty() &&
									 !ticketGroup.getCatSeatHigh().equals("")){
								 seat = seat +"-"+ticketGroup.getCatSeatHigh();
							 }
						 }
						 Double price = ticketGroup.getWholesalePrice();
						 if(price > 0){
							 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
						 }else{
							 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
						 }
						 
						 if(ticketGroup.getWholesalePrice() <= 0 || 
								 ticketGroup.getWholesalePrice().equals(0.00)){
							 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 }
						 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
						 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*rtwComission)/100);				 
						 ticketGroup.setSeat(seat);
						 
						 
						 List<PosTicket> posTickets = new ArrayList<PosTicket>();
						 
						 //if invoice is mapped with category ticket then select tickets only that categorytickets related tickets. 
						 posTickets = DAORegistry.getQueryManagerDAO().getJKTTicketsByInvoiceIdandFillDateRangeForNY(invoice.getInvoiceId(),ticketGroup.getJktPoId(),startDateStr,endDateStr, Connections.getPosConnectionByBrokername(brokerName));
						 
						 for (PosTicket ticket : posTickets) {
							 price = ticket.getCost();
							 ticketGroup.setCost(price);
							 if(price > 0){
								 ticketGroup.setTotalCost(ticket.getQuantity() * price);
							 }else{
								 ticketGroup.setTotalCost(ticket.getQuantity() * ticket.getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setJktPoId(ticket.getPurchaseOrderId());
							 ticketGroup.setPosTicket(ticket);
						 }
						 
						 /*Map<Integer, List<PosTicket>> ticketGroupMap =new HashMap<Integer, List<PosTicket>>();
						 
						 for (PosTicket ticket : posTickets) {
							 
							 List<PosTicket> tempCatTickets = ticketGroupMap.get(ticket.getTicketGroupId());
							 
							 if(null != tempCatTickets && !tempCatTickets.isEmpty()){
								 ticketGroupMap.get(ticket.getTicketGroupId()).add(ticket);
							 }else{
								 tempCatTickets = new ArrayList<PosTicket>();
								 tempCatTickets.add(ticket);
								 ticketGroupMap.put(ticket.getTicketGroupId(), tempCatTickets);
							 }
						 }
						 
						 
						 for (Integer ticketGroupId : ticketGroupMap.keySet()) {
								
							 List<PosTicket> tickets = ticketGroupMap.get(ticketGroupId);
							 
							 if(null != ticketGroup ){
								 
								 POSTicketGroup ticketGroup2 = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByEventId(ticketGroupId, Connections.getPosConnectionByBrokername(brokerName));
								 price = ticketGroup2.getCost();
								 ticketGroup.setCost(price);
								 if(price > 0){
									 ticketGroup.setTotalCost(tickets.size() * price);
								 }else{
									 ticketGroup.setTotalCost(tickets.size() * tickets.get(0).getActualSoldPrice());
								 }
								 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
								 ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
								// ticketGroup.setPurchaseOrderDate(tickets.get(0).getPurchaseOrderDate());
								 
							 }
						 }*/
						 if(null != ticketGroup){
							 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
							 ticketGroup.setClientBrokerId(clientBroker.getClientId());
							 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
							 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
							 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
							 ticketGroup.setCity(clientBroker.getCity());
							 ticketGroup.setState(clientBroker.getState());
							 ticketGroup.setCountry(clientBroker.getCountry());
							 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
							 ticketGroup.setClientMail(clientBroker.getClientMail());
							 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
							 ticketGroup.setClientType(clientBroker.getClientType());
							 
							 ticketGroup.setInvoiceNotes(invoice.getNotes());
							 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
							 ticketGroup.setTmatBrokerName(brokerName);
							 
							 //ticketGroup.setJktPoId(purchaseOrder.getPurchaseOrderId());
							 ticketGroup.setPurchaseOrderDateStr(xlDateFormat.format(ticketGroup.getPurchaseOrderDate()));
							 
							 ticketGroup.setTicketRequestId(invoice.getTicketRequestId());
							 
							 if(ticketGroup.getFillDate() != null) {
								/* long diff = ticketGroup.getFillDate().getTime()-invoice.getCreateDate().getTime();
								   Long days= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
								   ticketGroup.setFillAge(days.intValue());*/
								   
								   Calendar cal = Calendar.getInstance();
									cal.setTimeInMillis(invoice.getCreateDate().getTime());
									   
									Calendar toCal = Calendar.getInstance();
									toCal.setTimeInMillis(ticketGroup.getFillDate().getTime());
									int days=0;
									if(ticketGroup.getFillDate().getTime() > invoice.getCreateDate().getTime()) {
										while(cal.get(Calendar.YEAR) != toCal.get(Calendar.YEAR) || 
												cal.get(Calendar.MONTH) != toCal.get(Calendar.MONTH) ||
												cal.get(Calendar.DAY_OF_MONTH) != toCal.get(Calendar.DAY_OF_MONTH) ) {
											   cal.add(Calendar.DAY_OF_MONTH, 1);
											   days++;
										}
									}
									ticketGroup.setFillAge(days);
							 }
							 
							 boolean isDeadWood = false,isrewardthefan = false;
							 if(ticketGroup.getSalesPerson() != null && 
									 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
								 isDeadWood = true; 
							 }
							 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
								 if(ticketGroup.getClientCompanyName() != null && 
										 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true;
								 }
								 if(ticketGroup.getClientCompanyName() != null && 
										 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
									 isrewardthefan = true;
								 }
							 } else {
								 if(clientBroker.getClientFirstName() != null && 
										 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true;
								 }
								 if(clientBroker.getClientFirstName() != null && 
										 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
									 isrewardthefan = true;
								 }
							 }
							 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
							 
							 if(isDeadWood) {
								 ticketGroup.setWholesalePrice(0.0);
								 ticketGroup.setRetailPrice(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
								 ticketGroup.setActualSoldPrice(0.0);
							 }
							 /*if(isrewardthefan) {
								 ticketGroup.setWholesalePrice(0.0);
								 ticketGroup.setRetailPrice(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
								 ticketGroup.setCost(0.0);
								 ticketGroup.setInvoiceTotal(0.0);
							 }*/
							//Do not consider rewardthefan invoices in profit loss and filled reports
							 if(isrewardthefan) {
								continue; 
							 }
							 Boolean flag = ssListingCsvOrderIdsMap.get(ticketGroup.getTicketRequestId());
							 if(flag != null && flag) {
								 isSSOrderInvoices.add(ticketGroup);
								 continue;
							 }
							 if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
									 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
								 ticketGroup.setIsRetail(true);
								 
							 }else if (invoice.getSystemUserId() == 37 ){
								 ticketGroup.setIsWholeSaleCsr(true);
							 }
							 
							 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
								 
								 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
									 ticketGroup.setIsTnow(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
										 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
										 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
									 ticketGroup.setIsSH(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
										 ticketGroup.getClientBrokerId() == 3761){
									 ticketGroup.setIsTE(true);
									 
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
								 }else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
									 ticketGroup.setIsLong(false);
									 ticketGroup.setIsSH(false);
									 ticketGroup.setIsTE(false);
									 ticketGroup.setIsTnow(false);
									 ticketGroup.setIsZones(false);
									 ticketGroup.setIsRetail(false);
									 ticketGroup.setIsWholeSaleCsr(false);
									 ticketGroup.setIsCats(true);
									 
									 ticketGroup.setIsRightThisWayOrder(true);
								 }
							 }
							 
							 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
								 
								 ticketGroup.setIsLong(false);
								 ticketGroup.setIsSH(false);
								 ticketGroup.setIsTE(false);
								 ticketGroup.setIsTnow(false);
								 ticketGroup.setIsZones(false);
								 
							 }
							 
							 
							 	if(ticketGroup.getIsUnfilled()){
							 		isUnfilledInvoices.add(ticketGroup);
							 	} else if(ticketGroup.getIsSH() || ticketGroup.getIsTE() || ticketGroup.getIsTnow()){
							 		
							 		//Tamil : for TNow,TEvol and Stubhub tab consider actual sold price as sold price instead of considering wholesale price
									ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
									
									isSHTEInvoices.add(ticketGroup);
								} else if(ticketGroup.getIsRetail()){

							 		//Tamil : for Retail tab consider actual sold price as sold price instead of considering wholesale price
									 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
									 
							 		isRetailInvoices.add(ticketGroup);
								}else if(ticketGroup.getIsWholeSaleCsr()){
							 		isWholeSaleCSRInvoices.add(ticketGroup);
								} else if(ticketGroup.getIsLong()){
									isLongInvoices.add(ticketGroup);
								}else if(ticketGroup.getIsZones()){
									isZonesInvoices.add(ticketGroup);
								}else{
									isCatsInvoices.add(ticketGroup);
								}
								
						}/*else{
							
							missingInvoice = missingInvoice +","+invoice.getInvoiceId();
						}*/
						 }
					 } else {
						 
					 List<POSTicketGroup> posTicketGroups = DAORegistry.getQueryManagerDAO().getBrokerTicketGroupByInvoiceIdForNY(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
					 
					 if(posTicketGroups != null && !posTicketGroups.isEmpty()) {
						 
						 for (POSTicketGroup ticketGroup : posTicketGroups) {
							 
							// PosEvent mzTixEvent =jktExchangeEventMap.get(ticketGroup.getExchangeEventId());
							 //if(null == mzTixEvent){
								// mzTixEvent =new PosEvent();
							 PosEvent mzTixEvent = mzTixLocalEventObjMap.get(ticketGroup.getEventId());
							 //}
							 
							 ticketGroup.setInvoiceId(invoice.getInvoiceId());
							 ticketGroup.setSalesPerson(invoice.getSalesPersonName());
							 ticketGroup.setParentCatId(mzTixEvent.getParentCatId());
							 ticketGroup.setZoneEvent(mzTixEvent.getZoneEvent());
							 ticketGroup.setEventName(mzTixEvent.getEventName());
							 ticketGroup.setVenueName(mzTixEvent.getVenueName());
							 ticketGroup.setEventDateStr(xlDateFormat.format(dbDateTimeFormat.parse(mzTixEvent.getEventDateStr())));
							 ticketGroup.setInvoiceDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 if( null != invoice.getInvoiceVoidDate() ){
								 ticketGroup.setInvoiceVoidDateStr(xlDateFormat.format(invoice.getInvoiceVoidDate()));
							 }
							 ticketGroup.setFillDateStr(xlDateFormat.format(invoice.getCreateDate()));
							 //ticketGroup.setQty(tickets.size());
							 ticketGroup.setIsLong(true);
							 //for long inventory invoice createe date is the fill date
							 ticketGroup.setFillDate(invoice.getCreateDate());
							 
							 String seat = ticketGroup.getSeatLow()+"-"+ticketGroup.getSeatHigh();
							 
							 if(ticketGroup.getWholesalePrice() <= 0 || 
									 ticketGroup.getWholesalePrice().equals(0.00)){
								 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 }
							 //ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
							 
							 //ticketGroup.setJktPoId(tickets.get(0).getPurchaseOrderId());
							 //ticketGroup.setPurchaseOrderDate(tickets.get(0).getPurchaseOrderDate());
							 
							 Double price = ticketGroup.getCost();
							 if(price > 0){
								 ticketGroup.setTotalCost(ticketGroup.getQty() * price);
							 }else{
								 ticketGroup.setTotalCost(ticketGroup.getQty() * ticketGroup.getActualSoldPrice());
							 }
							 ticketGroup.setTgCommission((ticketGroup.getTotalCost()*12)/100);
							 ticketGroup.setSeat(seat);
						
							 if(null != ticketGroup){
								 ClientBroker clientBroker = DAORegistry.getQueryManagerDAO().getInvoiceClientInfo(invoice.getInvoiceId(), Connections.getPosConnectionByBrokername(brokerName));
								 ticketGroup.setClientBrokerId(clientBroker.getClientId());
								 ticketGroup.setClientFirstName(clientBroker.getClientFirstName()+" "+clientBroker.getClientLastName());
								 ticketGroup.setStreetAddress1(clientBroker.getStreetAddress1());
								 ticketGroup.setStreetAddress2(clientBroker.getStreetAddress2());
								 ticketGroup.setCity(clientBroker.getCity());
								 ticketGroup.setState(clientBroker.getState());
								 ticketGroup.setCountry(clientBroker.getCountry());
								 ticketGroup.setPhoneNumber(clientBroker.getPhoneNumber());
								 ticketGroup.setClientMail(clientBroker.getClientMail());
								 ticketGroup.setClientCompanyName(clientBroker.getClientCompanyName());
								 ticketGroup.setClientType(clientBroker.getClientType());
								 
								 ticketGroup.setInvoiceNotes(invoice.getNotes());
								 ticketGroup.setInvoiceTotal(invoice.getInvoiceTotal());
								 ticketGroup.setTmatBrokerName(brokerName);
								 
								 //ticketGroup.setJktPoId(purchaseOrder.getPurchaseOrderId());
								 ticketGroup.setPurchaseOrderDateStr(xlDateFormat.format(ticketGroup.getPurchaseOrderDate()));
								 
								 ticketGroup.setTicketRequestId(invoice.getTicketRequestId());
								 if(ticketGroup.getFillDate() != null) {
										/* long diff = ticketGroup.getFillDate().getTime()-invoice.getCreateDate().getTime();
										   Long days= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
										   ticketGroup.setFillAge(days.intValue());*/
										   
										   Calendar cal = Calendar.getInstance();
											cal.setTimeInMillis(invoice.getCreateDate().getTime());
											   
											Calendar toCal = Calendar.getInstance();
											toCal.setTimeInMillis(ticketGroup.getFillDate().getTime());
											int days=0;
											if(ticketGroup.getFillDate().getTime() > invoice.getCreateDate().getTime()) {
												while(cal.get(Calendar.YEAR) != toCal.get(Calendar.YEAR) || 
														cal.get(Calendar.MONTH) != toCal.get(Calendar.MONTH) ||
														cal.get(Calendar.DAY_OF_MONTH) != toCal.get(Calendar.DAY_OF_MONTH) ) {
													   cal.add(Calendar.DAY_OF_MONTH, 1);
													   days++;
												}
											}
											ticketGroup.setFillAge(days);
									 }
								 
								 boolean isDeadWood = false,isrewardthefan = false;
								 if(ticketGroup.getSalesPerson() != null && 
										 ticketGroup.getSalesPerson().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
									 isDeadWood = true; 
								 }
								 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
									 if(ticketGroup.getClientCompanyName() != null && 
											 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
										 isDeadWood = true;
									 }
									 if(ticketGroup.getClientCompanyName() != null && 
											 ticketGroup.getClientCompanyName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
										 isrewardthefan = true;
									 }
								 } else {
									 if(clientBroker.getClientFirstName() != null && 
											 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("deadwood")) {
										 isDeadWood = true;
									 }
									 if(clientBroker.getClientFirstName() != null && 
											 clientBroker.getClientFirstName().replaceAll("\\s+", "").toLowerCase().contains("rewardthefan")) {
										 isrewardthefan = true;
									 }
								 }
								 ticketGroup.setIsRewardTheFanOrder(isrewardthefan);
								 
								 if(isDeadWood) {
									 ticketGroup.setWholesalePrice(0.0);
									 ticketGroup.setRetailPrice(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
									 ticketGroup.setActualSoldPrice(0.0);
								 }
								 /*if(isrewardthefan) {
									 ticketGroup.setWholesalePrice(0.0);
									 ticketGroup.setRetailPrice(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
									 ticketGroup.setCost(0.0);
									 ticketGroup.setInvoiceTotal(0.0);
								 }*/
								//Do not consider rewardthefan invoices in profit loss and filled reports
								 if(isrewardthefan) {
									continue; 
								 }
								 
								 Boolean flag = ssListingCsvOrderIdsMap.get(ticketGroup.getTicketRequestId());
								 if(flag != null && flag) {
									 isSSOrderInvoices.add(ticketGroup);
									 continue;
								 }
								 
								 if(invoice.getSystemUserId() == 4 || invoice.getSystemUserId() == 7 || 
										 invoice.getSystemUserId() == 34 || invoice.getSystemUserId() == 33 ){
									 ticketGroup.setIsRetail(true);
									 
								 }else if (invoice.getSystemUserId() == 37 ){
									 ticketGroup.setIsWholeSaleCsr(true);
								 }
								 
								 if(ticketGroup.getClientType().contains("CLIENT BROKER")){
									 
									 if(ticketGroup.getClientBrokerId() == 1100 || ticketGroup.getClientBrokerId() == 1315){//|| ticketGroup.getClientBrokerId() == 3205
										 ticketGroup.setIsTnow(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientBrokerId() == 1869 || ticketGroup.getClientBrokerId() == 3751 ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("viagogo")|| 
											 ticketGroup.getClientCompanyName().toLowerCase().contains("rukkus") ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("seatgeek") ||
											 ticketGroup.getClientCompanyName().toLowerCase().contains("concierge group")){
										 ticketGroup.setIsSH(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientBrokerId() == 1714 || ticketGroup.getClientBrokerId() == 2883 ||
											 ticketGroup.getClientBrokerId() == 3761){
										 ticketGroup.setIsTE(true);
										 
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
									 }else if(ticketGroup.getClientCompanyName().toLowerCase().contains("right this way 2")) {
										 ticketGroup.setIsLong(false);
										 ticketGroup.setIsSH(false);
										 ticketGroup.setIsTE(false);
										 ticketGroup.setIsTnow(false);
										 ticketGroup.setIsZones(false);
										 ticketGroup.setIsRetail(false);
										 ticketGroup.setIsWholeSaleCsr(false);
										 ticketGroup.setIsCats(true);
										 
										 ticketGroup.setIsRightThisWayOrder(true);
									 }
								 }
								 
								 if(invoice.getSystemUserId() == 39 || invoice.getSystemUserId() == 35){
									 
									 ticketGroup.setIsLong(false);
									 ticketGroup.setIsSH(false);
									 ticketGroup.setIsTE(false);
									 ticketGroup.setIsTnow(false);
									 ticketGroup.setIsZones(false);
									 
								 }
								 
								
								 	if(ticketGroup.getIsUnfilled()){
								 		isUnfilledInvoices.add(ticketGroup);
								 	}else if(ticketGroup.getIsSH() || ticketGroup.getIsTE() || ticketGroup.getIsTnow()){
								 		
								 		//Tamil : for TNow,TEvol and Stubhub tab consider actual sold price as sold price instead of considering wholesale price
										ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
										 
										isSHTEInvoices.add(ticketGroup);
									} else if(ticketGroup.getIsRetail()){

								 		//Tamil : for Retail tab consider actual sold price as sold price instead of considering wholesale price
										 ticketGroup.setWholesalePrice(ticketGroup.getActualSoldPrice());
										 
								 		isRetailInvoices.add(ticketGroup);
									}else if(ticketGroup.getIsWholeSaleCsr()){
								 		isWholeSaleCSRInvoices.add(ticketGroup);
									} else if(ticketGroup.getIsLong()){
										isLongInvoices.add(ticketGroup);
									}else if(ticketGroup.getIsZones()){
										isZonesInvoices.add(ticketGroup);
									}else{
										isCatsInvoices.add(ticketGroup);
									}
									
							}
						}
					 } else {
							missingInvoice = missingInvoice +","+invoice.getInvoiceId();
					 }
					 
					 }
					 
								
					 
					//invoiceTicketGroups.add(ticketGroup);
				
					
				
			}
		//System.out.println("P&L FILLED broker : "+brokerName+" : missing PO : "+missingPO);
		System.out.println("P&L FILLED broker : "+brokerName+" : missing Invoices : "+missingInvoice);
		}
			 
			 //SXSSFWorkbook workbook = new SXSSFWorkbook();
			 
			 List<String> sheetNameList = new ArrayList<String>();
			 
			 Map<String,Double> summaryMap = new HashMap<String, Double>();
			 String sheetName = "TN Retail";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isRetailInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "SH and TE and TNow";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isSHTEInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Long";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isLongInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Cats";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isCatsInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "TN Zones";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isZonesInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "Wholesale CSR";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isWholeSaleCSRInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 sheetName = "SS Listing";
			 generateJKTWeeklyInvoiceSheetForNY(workbook, isSSOrderInvoices,sheetName,false,false,false,true,true,summaryMap);
			 sheetNameList.add(sheetName);
			 
			 //generateUnfilledInvoiceSheet(workbook, isUnfilledInvoices,"Unfilled Orders",false,false,false,true);
			
			 
			 CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			 SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet("Summary");
			 int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("Type");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Actual Sold Price");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Net Sold Price");
			 j++;
			 
			 Double grandTotal = 0.0,totalActualSoldPrice=0.0,totalNetSoldPrice=0.0;
			 int i = 1;
			
			 for (String tempSheetName : sheetNameList) {
				 Row row = ssSheet.createRow(i);
				 row.createCell((int) 0).setCellValue(tempSheetName);
					
				Double profitloss = summaryMap.get("ProfitLoss_"+tempSheetName);
				profitloss= (profitloss==null)? 0.0:profitloss;
				grandTotal += profitloss;
				
		        Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(profitloss);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double actualSoldPrice = summaryMap.get("ActualSoldPrice_"+tempSheetName);
		        actualSoldPrice= (actualSoldPrice==null)? 0.0:actualSoldPrice;
		        totalActualSoldPrice += actualSoldPrice;
		        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(actualSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        
		        Double netSoldPrice = summaryMap.get("NetSoldPrice_"+tempSheetName);
		        netSoldPrice= (netSoldPrice==null)? 0.0:netSoldPrice;
				totalNetSoldPrice += netSoldPrice;
		        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(netSoldPrice);
		        amountCell.setCellStyle(cellamountStyle);
		        i++;
			}
	        
	        Row row = ssSheet.createRow(i);
			row.createCell((int) 0).setCellValue("Total");
			Cell amountCell = row.createCell(1,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(grandTotal);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(2,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalActualSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        
	        amountCell = row.createCell(3,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(totalNetSoldPrice);
	        amountCell.setCellStyle(cellamountStyle);
	        
			 
			 
			 /*FileOutputStream fileOut = new FileOutputStream("C:\\UNCAT\\RTWProfitLossReportFinal_1.xlsx");
			 workbook.write(fileOut);
		     fileOut.close();*/
		     
			 System.out.println("P&L FILLED RTW : FILLED JOB COMPLETED SUCESSFULLY........");
			 
			 //return workbook;
		 }
		
		public static void generateJKTWeeklyInvoiceSheet(SXSSFWorkbook workbook ,List<POSTicketGroup> jktInvoices,String sheetName,
				 boolean showLastColum,boolean showVoidDate,boolean showClientInfo,boolean showProfitLossInfo,boolean showPODate,Map<String,Double> summaryMap) throws Exception{
			 
				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
				
				int j = 0;
			
				 Row rowhead =   ssSheet.createRow((int)0);
				 rowhead.createCell((int) j).setCellValue("Broker");
				 j++;
				 rowhead.createCell((int) j).setCellValue("Invoice No");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("PO No");
			    j++;
			    
			    //Show po date in RTW P&L Filled Report
			    if(showPODate) {
			    	rowhead.createCell((int) j).setCellValue("PO Date");
				    j++;
			    }
			    
			    rowhead.createCell((int) j).setCellValue("Sales Person");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Fill date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Parent Catgeory");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Is Zone Event");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Internal Notes");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Section");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Row");
			    j++;
			    /*rowhead.createCell((int) j).setCellValue("Seat");
			    j++;*/
			    rowhead.createCell((int) j).setCellValue("Qty");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Sold Price Per Ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Cost Per ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Invoice Total");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Cost");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Sold Price");
			    j++;
			    
			    /*rowhead.createCell((int) j).setCellValue("CC Fees");
			    j++;*/
			    
			    rowhead.createCell((int) j).setCellValue("TN Fees");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Net Sold Price");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Gross Margin %");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Client Type");
		    	j++;
		    	
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Company Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Email");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Phone Number");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address1");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address2");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("State");
		    	j++;
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			    
			    int i=1,totalQty=0;
			    Double totalCost =0.0,totalAcutalPrice=0.0,totalRetailPrice=0.0,tgCommission=0.0,wholeSalePrice=0.0,tnfees=0.0 ,actualProfitLoss=0.0;
			    Double totalQtyCost=0.0,totalQtyActualPrice=0.0,cost=0.0,actualSoldPrice=0.0,totalTnFees=0.0,
			    totalActualProfitLoss=0.0,tnCommissionPerc=0.0,invoiceTotal=0.00,totalccFees=0.00,grossMargin=0.0,netSoldPrice=0.0,totalNetSoldPrice=0.0;
			    
				for (POSTicketGroup ticketGroups :jktInvoices) {
					totalQty=totalQty+ticketGroups.getQty();
					totalAcutalPrice=totalAcutalPrice+ticketGroups.getWholesalePrice();
					totalCost=totalCost+ticketGroups.getCost();
					totalRetailPrice=totalRetailPrice+ticketGroups.getRetailPrice();
					wholeSalePrice=wholeSalePrice+ticketGroups.getWholesalePrice();
					tgCommission=tgCommission+ticketGroups.getTgCommission();
					
					cost = ticketGroups.getQty() * ticketGroups.getCost();
					actualSoldPrice = ticketGroups.getQty() * ticketGroups.getWholesalePrice();
					invoiceTotal = invoiceTotal + ticketGroups.getInvoiceTotal();
					
					
					/*
					 
					  unifilled - 0%
					  TNCATS-12%
					TNZONES-12%
					
					 TNREtail : 3.35%
					
					TNLONG-6.25%
					TNOW&TE&SH - 0%
					WholeSale - 0%
					*/
					if(ticketGroups.getIsTixcityOrder()){ 
						tnCommissionPerc = 5.00;
					}else if(ticketGroups.getIsRetail()){ 
						tnCommissionPerc = 3.00;//3.35;
					}else if(ticketGroups.getIsWholeSaleCsr() ){ 
						tnCommissionPerc = 0.00;
					}else if(ticketGroups.getIsSH() || ticketGroups.getIsTE() ||  ticketGroups.getIsTnow()){
						tnCommissionPerc = 0.00;
					}else if(ticketGroups.getIsLong()){
						tnCommissionPerc = 6.25;
					}else if(ticketGroups.getIsUnfilled()){
						tnCommissionPerc = 0.00;
					}else{
						tnCommissionPerc = 12.00;
					}


					if((ticketGroups.getIsRewardTheFanOrder() != null && ticketGroups.getIsRewardTheFanOrder()) ||
							(ticketGroups.getIsRightThisWayOrder() != null && ticketGroups.getIsRightThisWayOrder())) {
						tnfees = 0.0;
					} else {
						tnfees = (actualSoldPrice * tnCommissionPerc ) / 100;
					}
					
					
					//ccFees = (ticketGroups.getInvoiceTotal() * ccFeePerc) / 100;
					actualProfitLoss = actualSoldPrice - (cost + tnfees /*+ ccFees*/);
					actualProfitLoss = Math.round(actualProfitLoss * 100.0) / 100.0;
					
					if(actualSoldPrice == 0) {
						grossMargin = 0.0;
					} else {
						grossMargin = Math.round((actualProfitLoss * 100 / actualSoldPrice)*100.0)/100.0;						
					}
					
					netSoldPrice = actualSoldPrice - tnfees; 
					
					
					totalTnFees = totalTnFees + tnfees;
					//totalccFees = totalccFees + ccFees;
					totalActualProfitLoss = totalActualProfitLoss + actualProfitLoss;
					totalQtyCost = totalQtyCost+cost;
					totalQtyActualPrice = totalQtyActualPrice+actualSoldPrice;
					totalNetSoldPrice = totalNetSoldPrice + netSoldPrice;
					
					//System.out.println("Broker:"+ticketGroups.getTmatBrokerName()+", Invoice ID:"+ticketGroups.getInvoiceId()+", Invoice Total:"+ticketGroups.getInvoiceTotal()+", Sold Price:"+actualSoldPrice+", Cost:"+cost+/*", CCFees:"+ccFees+*/", TnFees:"+tnfees+", ProfitLoss:"+actualProfitLoss);
					
					int column = 0;
					
					if(i%1000 == 0) {

					}
					Row row = ssSheet.createRow(i);
					row.createCell((int) column).setCellValue(ticketGroups.getTmatBrokerName());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceId());
			        column++;
			        if(ticketGroups.getInvoiceDateStr() != null) {
			        	row.createCell((int) column).setCellValue(ticketGroups.getInvoiceDateStr().replaceAll(",", "-"));
				        column++;	
			        } else {
			        	row.createCell((int) column).setCellValue("");
				        column++;
			        }
			        
			        if(ticketGroups.getJktPoId() != null) {
			        	row.createCell((int) column).setCellValue(ticketGroups.getJktPoId());
				        column++;	
			        } else {
			        	row.createCell((int) column).setCellValue("");
				        column++;
			        }
			        
			        
			        //Show po date in RTW P&L Filled Report
				    if(showPODate) {
				    	if(ticketGroups.getPurchaseOrderDateStr() != null) {
				    		row.createCell((int) column).setCellValue(ticketGroups.getPurchaseOrderDateStr().replaceAll(",", "-"));
					        column++;
				    	} else {
				    		row.createCell((int) column).setCellValue("");
					        column++;
				    	}
				    	
				    }
				    
			        row.createCell((int) column).setCellValue(ticketGroups.getSalesPerson());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getFillDateStr().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventParentCategory().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue((ticketGroups.getZoneEvent())==0?"No":"Yes");
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getEventDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getVenueName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInternalNotes().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getSection());
			        column++;
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(ticketGroups.getRow());
			        column++;
			       
			        row.createCell((int) column).setCellValue(ticketGroups.getQty());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getWholesalePrice());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getCost());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceTotal());
				    column++;
				    
				    Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(cost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
				    
				    
				    amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	/*amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(ccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;*/
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(tnfees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(netSoldPrice.toString()));//Net Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(grossMargin.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getClientType().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientFirstName().replaceAll(",", "-"));
			    	column++;
			    	
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientCompanyName().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientMail().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getPhoneNumber().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress1().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress2().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getCity().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getState().replaceAll(",", "-"));
			    	column++;
			    	
			        i++;
			        
				}
				
				if(summaryMap != null) {
					summaryMap.put("ProfitLoss_"+sheetName, totalActualProfitLoss);
					summaryMap.put("ActualSoldPrice_"+sheetName, totalQtyActualPrice);
					summaryMap.put("NetSoldPrice_"+sheetName, totalNetSoldPrice);
					summaryMap.put("RTWCommission_"+sheetName, totalTnFees);
				}
				
				j=0;
				
				if(null != jktInvoices && !jktInvoices.isEmpty() && jktInvoices.size() >0){
					Row row = ssSheet.createRow(i);
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        
			        if(showPODate) {
			        	row.createCell((int) j).setCellValue("");
				        j++;	
			        }
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        Cell seatCell = row.createCell(j,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue("TOTAL");
			        j++;
			        
			        row.createCell((int) j).setCellValue(totalQty);
			        j++;
			        
			        Cell amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalAcutalPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(invoiceTotal.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	

				    amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyActualPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	/*amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;*/
			    	
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalTnFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalNetSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalActualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        Double totalGrossMargin = 0.0;
			        if(totalQtyActualPrice == 0) {
			        	totalGrossMargin = 0.0;
					} else {
						totalGrossMargin = Math.round((totalActualProfitLoss * 100 / totalQtyActualPrice)*100.0)/100.0;						
					}
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalGrossMargin.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
				}
				
			}
		
		public static void generateJKTWeeklyInvoiceSheetForNY(SXSSFWorkbook workbook ,List<POSTicketGroup> jktInvoices,String sheetName,
				 boolean showLastColum,boolean showVoidDate,boolean showClientInfo,boolean showProfitLossInfo,boolean showPODate,Map<String,Double> summaryMap) throws Exception{
			DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
				
				int j = 0;
			
				 Row rowhead =   ssSheet.createRow((int)0);
				 rowhead.createCell((int) j).setCellValue("Broker");
				 j++;
				 rowhead.createCell((int) j).setCellValue("Order No");
			    	j++;
				 rowhead.createCell((int) j).setCellValue("Invoice No");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("PO No");
			    j++;
			    
			    //Show po date in RTW P&L Filled Report
			    if(showPODate) {
			    	rowhead.createCell((int) j).setCellValue("PO Date");
				    j++;
			    }
			    
			    rowhead.createCell((int) j).setCellValue("Sales Person");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Fill date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Parent Catgeory");
			    j++;
			   /* rowhead.createCell((int) j).setCellValue("Is Zone Event");
			    j++;*/
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Notes");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Listed Section");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Listed Row");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Listed Seat");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Listed Qty");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Sold Price Per Ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Cost Per ticket");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Listed Price Per Ticket");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Total Listed Price");
		    	j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Total");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Cost");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Sold Price");
			    j++;
			    
			    /*rowhead.createCell((int) j).setCellValue("CC Fees");
			    j++;*/
			    
			    rowhead.createCell((int) j).setCellValue("TN Fees");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Net Sold Price");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Gross Margin %");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Client Type");
		    	j++;
		    	
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Company Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Email");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Phone Number");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address1");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address2");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("State");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("External Notes");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Listed Date");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Filled Section");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Filled Row");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Filled Seat");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Filled Quantity");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Fill Age");
		    	j++;
		    	
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			    
			    int i=1,totalQty=0;
			    Double totalCost =0.0,totalAcutalPrice=0.0,totalRetailPrice=0.0,tgCommission=0.0,wholeSalePrice=0.0,tnfees=0.0 ,actualProfitLoss=0.0;
			    Double totalQtyCost=0.0,totalQtyActualPrice=0.0,cost=0.0,actualSoldPrice=0.0,totalTnFees=0.0,
			    totalActualProfitLoss=0.0,tnCommissionPerc=0.0,invoiceTotal=0.00,totalccFees=0.00,grossMargin=0.0,netSoldPrice=0.0,totalNetSoldPrice=0.0;
			    Double totListedPricePerTix =0.0,totListedPriceTotal =0.0,listedPricePerTix,listedPriceTotal=0.0;
			    
				for (POSTicketGroup ticketGroups :jktInvoices) {
					totalQty=totalQty+ticketGroups.getQty();
					totalAcutalPrice=totalAcutalPrice+ticketGroups.getWholesalePrice();
					totalCost=totalCost+ticketGroups.getCost();
					totalRetailPrice=totalRetailPrice+ticketGroups.getRetailPrice();
					wholeSalePrice=wholeSalePrice+ticketGroups.getWholesalePrice();
					tgCommission=tgCommission+ticketGroups.getTgCommission();
					
					cost = ticketGroups.getQty() * ticketGroups.getCost();
					actualSoldPrice = ticketGroups.getQty() * ticketGroups.getWholesalePrice();
					invoiceTotal = invoiceTotal + ticketGroups.getInvoiceTotal();
					
					listedPricePerTix = ticketGroups.getWholesalePrice();
					listedPriceTotal = Math.round(ticketGroups.getWholesalePrice()*ticketGroups.getQty()*100.0) /100.0;
					
					totListedPricePerTix += listedPricePerTix;
					totListedPriceTotal += listedPriceTotal;
					
					/*
					 
					  unifilled - 0%
					  TNCATS-12%
					TNZONES-12%
					
					 TNREtail : 3.35%
					
					TNLONG-6.25%
					TNOW&TE&SH - 0%
					WholeSale - 0%
					*/
					if(ticketGroups.getIsRetail()){ 
						tnCommissionPerc = 3.00;//3.35;
					}else if(ticketGroups.getIsWholeSaleCsr() ){ 
						tnCommissionPerc = 0.00;
					}else if(ticketGroups.getIsSH() || ticketGroups.getIsTE() ||  ticketGroups.getIsTnow()){
						tnCommissionPerc = 0.00;
					}else if(ticketGroups.getIsLong()){
						tnCommissionPerc = 6.25;
					}else if(ticketGroups.getIsUnfilled()){
						tnCommissionPerc = 0.00;
					}else{
						tnCommissionPerc = 12.00;
					}


					if((ticketGroups.getIsRewardTheFanOrder() != null && ticketGroups.getIsRewardTheFanOrder()) ||
							(ticketGroups.getIsRightThisWayOrder() != null && ticketGroups.getIsRightThisWayOrder())) {
						tnfees = 0.0;
					} else {
						tnfees = (actualSoldPrice * tnCommissionPerc ) / 100;
					}
					
					//ccFees = (ticketGroups.getInvoiceTotal() * ccFeePerc) / 100;
					actualProfitLoss = actualSoldPrice - (cost + tnfees /*+ ccFees*/);
					actualProfitLoss = Math.round(actualProfitLoss * 100.0) / 100.0;
					
					if(actualSoldPrice == 0) {
						grossMargin = 0.0;
					} else {
						grossMargin = Math.round((actualProfitLoss * 100 / actualSoldPrice)*100.0)/100.0;						
					}
					
					netSoldPrice = actualSoldPrice - tnfees; 
					
					
					totalTnFees = totalTnFees + tnfees;
					//totalccFees = totalccFees + ccFees;
					totalActualProfitLoss = totalActualProfitLoss + actualProfitLoss;
					totalQtyCost = totalQtyCost+cost;
					totalQtyActualPrice = totalQtyActualPrice+actualSoldPrice;
					totalNetSoldPrice = totalNetSoldPrice + netSoldPrice;
					
					//System.out.println("Broker:"+ticketGroups.getTmatBrokerName()+", Invoice ID:"+ticketGroups.getInvoiceId()+", Invoice Total:"+ticketGroups.getInvoiceTotal()+", Sold Price:"+actualSoldPrice+", Cost:"+cost+/*", CCFees:"+ccFees+*/", TnFees:"+tnfees+", ProfitLoss:"+actualProfitLoss);
					
					int column = 0;
					
					if(i%1000 == 0) {

					}
					Row row = ssSheet.createRow(i);
					row.createCell((int) column).setCellValue(ticketGroups.getTmatBrokerName());
			        column++;
			        
			        if(ticketGroups.getTicketRequestId() != null) {
			        	row.createCell((int) column).setCellValue(ticketGroups.getTicketRequestId());
				        column++;
			        } else {
			        	row.createCell((int) column).setCellValue("");
				        column++;
			        }
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceId());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getJktPoId());
			        column++;
			        
			        //Show po date in RTW P&L Filled Report
				    if(showPODate) {
				    	row.createCell((int) column).setCellValue(ticketGroups.getPurchaseOrderDateStr().replaceAll(",", "-"));
				        column++;
				    }
				    
			        row.createCell((int) column).setCellValue(ticketGroups.getSalesPerson());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getFillDateStr().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventParentCategory().replaceAll(",", "-"));
			        column++;
			        
			       /* row.createCell((int) column).setCellValue((ticketGroups.getZoneEvent())==0?"No":"Yes");
			        column++;*/
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getEventDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getVenueName().replaceAll(",", "-"));
			        column++;
			        if(ticketGroups.getInvoiceNotes() != null) {
			        	row.createCell((int) column).setCellValue(ticketGroups.getInvoiceNotes().replaceAll(",", "-"));
				        column++;	
			        } else {
			        	row.createCell((int) column).setCellValue("");
				        column++;
			        }
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getSection());
			        column++;
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(ticketGroups.getRow());
			        column++;
			       
			        if(ticketGroups.getSeat() != null) {
			        	row.createCell((int) column).setCellValue(ticketGroups.getSection());
				        column++;
			        } else {
			        	row.createCell((int) column).setCellValue("");
				        column++;
			        }
			        row.createCell((int) column).setCellValue(ticketGroups.getQty());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getWholesalePrice());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getCost());
			        column++;
			       
			        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(listedPricePerTix));//Listed Price Per Ticket
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(listedPriceTotal));//Listed Total Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceTotal());
				    column++;
				    
				    amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(cost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
				    
				    
				    amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	/*amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(ccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;*/
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(tnfees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(netSoldPrice.toString()));//Net Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(grossMargin.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getClientType().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientFirstName().replaceAll(",", "-"));
			    	column++;
			    	
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientCompanyName().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientMail().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getPhoneNumber().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress1().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress2().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getCity().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getState().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getExternalNotes());
			    	column++;
			    	row.createCell((int) column).setCellValue(xlDateFormat.format(ticketGroups.getCreatedDate()));
			    	column++;
			    	if(ticketGroups.getPosTicket() != null) {
			    		row.createCell((int) column).setCellValue(ticketGroups.getPosTicket().getSection().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getPosTicket().getRow().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getPosTicket().getSeatRange().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getPosTicket().getQuantity());
				    	column++;
			    	} else {
			    		row.createCell((int) column).setCellValue(ticketGroups.getSection().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getRow().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getRow().replaceAll(",", "-"));
				    	column++;
				    	row.createCell((int) column).setCellValue(ticketGroups.getQty());
				    	column++;
			    	}
			    	
			    	if(ticketGroups.getFillAge() != null) {
			    		row.createCell((int) column).setCellValue(ticketGroups.getFillAge());
				    	column++;
			    	} else {
			    		row.createCell((int) column).setCellValue(0);
				    	column++;
			    	}
			        i++;
			        
				}
				
				if(summaryMap != null) {
					summaryMap.put("ProfitLoss_"+sheetName, totalActualProfitLoss);
					summaryMap.put("ActualSoldPrice_"+sheetName, totalQtyActualPrice);
					summaryMap.put("NetSoldPrice_"+sheetName, totalNetSoldPrice);
				}
				
				j=0;
				
				if(null != jktInvoices && !jktInvoices.isEmpty() && jktInvoices.size() >0){
					Row row = ssSheet.createRow(i);
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        
			        if(showPODate) {
			        	row.createCell((int) j).setCellValue("");
				        j++;	
			        }
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     /*row.createCell((int) j).setCellValue("");
				     j++;*/
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        Cell seatCell = row.createCell(j,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue("TOTAL");
			        j++;
			        
			        row.createCell((int) j).setCellValue(totalQty);
			        j++;
			        
			        Cell amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalAcutalPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totListedPricePerTix.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totListedPriceTotal.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(invoiceTotal.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	

				    amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyActualPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	/*amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;*/
			    	
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalTnFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalNetSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalActualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        Double totalGrossMargin = 0.0;
			        if(totalQtyActualPrice == 0) {
			        	totalGrossMargin = 0.0;
					} else {
						totalGrossMargin = Math.round((totalActualProfitLoss * 100 / totalQtyActualPrice)*100.0)/100.0;						
					}
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalGrossMargin.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
				}
				
			}
		
		
		public static void generateJKTWeeklyInvoiceSheetOldFormat(SXSSFWorkbook workbook ,List<POSTicketGroup> jktInvoices,String sheetName,
				 boolean showLastColum,boolean showVoidDate,boolean showClientInfo,boolean showProfitLossInfo) throws Exception{
			 
				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
				
				int j = 0;
			
				 Row rowhead =   ssSheet.createRow((int)0);
			    rowhead.createCell((int) j).setCellValue("Invoice No");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("PO No");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Sales Person");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Fill date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Internal Notes");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Section");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Row");
			    j++;
			    /*rowhead.createCell((int) j).setCellValue("Seat");
			    j++;*/
			    rowhead.createCell((int) j).setCellValue("Qty");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Sold Price Per Ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Cost Per ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Invoice Total");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Cost");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Total Sold Price");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("CC Fees");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("TN Fees");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Actual Profit & loss");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Client Type");
		    	j++;
		    	
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Company Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Email");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Phone Number");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address1");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address2");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("State");
		    	j++;
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			    
			    int i=1,totalQty=0;
			    Double totalCost =0.0,totalAcutalPrice=0.0,totalRetailPrice=0.0,tgCommission=0.0,wholeSalePrice=0.0,tnfees=0.0 ,actualProfitLoss=0.0;
			    Double totalQtyCost=0.0,totalQtyActualPrice=0.0,cost=0.0,actualSoldPrice=0.0,totalTnFees=0.0,
			    totalActualProfitLoss=0.0,tnCommissionPerc=0.0,invoiceTotal=0.00,ccFees=0.00,totalccFees=0.00,ccFeePerc=0.00;
				for (POSTicketGroup ticketGroups :jktInvoices) {
					totalQty=totalQty+ticketGroups.getQty();
					totalAcutalPrice=totalAcutalPrice+ticketGroups.getWholesalePrice();
					totalCost=totalCost+ticketGroups.getCost();
					totalRetailPrice=totalRetailPrice+ticketGroups.getRetailPrice();
					wholeSalePrice=wholeSalePrice+ticketGroups.getWholesalePrice();
					tgCommission=tgCommission+ticketGroups.getTgCommission();
					
					cost = ticketGroups.getQty() * ticketGroups.getCost();
					actualSoldPrice = ticketGroups.getQty() * ticketGroups.getWholesalePrice();
					invoiceTotal = invoiceTotal + ticketGroups.getInvoiceTotal();
					
					
					/*TNREtail : 3.35%
					TNCATS-12%
					TNZONES-12%
					TNLONG-6.25%
					unifilled - 0%
					TNOW&TE&SH - 0%
					WholeSale - 0%
					*/
					if(ticketGroups.getIsRetail()){ 
						tnCommissionPerc = 0.00;
						ccFeePerc=3.25;
					}else if(ticketGroups.getIsSH() || ticketGroups.getIsTE()){
						tnCommissionPerc = 0.00;
						ccFeePerc=0.00;
					}else if(ticketGroups.getIsLong()){
						tnCommissionPerc = 3.50;
						ccFeePerc=3.25;
					}else{
						tnCommissionPerc = 8.25;
						ccFeePerc=3.25;
					}


					tnfees = (actualSoldPrice * tnCommissionPerc ) / 100;
					ccFees = (ticketGroups.getInvoiceTotal() * ccFeePerc) / 100;
					actualProfitLoss = actualSoldPrice - (cost + tnfees + ccFees);
					
					totalTnFees = totalTnFees + tnfees;
					totalccFees = totalccFees + ccFees;
					totalActualProfitLoss = totalActualProfitLoss + actualProfitLoss;
					totalQtyCost = totalQtyCost+cost;
					totalQtyActualPrice = totalQtyActualPrice+actualSoldPrice;
					
					System.out.println("Invoice ID:"+ticketGroups.getInvoiceId()+", Invoice Total:"+ticketGroups.getInvoiceTotal()+", Sold Price:"+actualSoldPrice+", Cost:"+cost+", CCFees:"+ccFees+", TnFees:"+tnfees+", ProfitLoss:"+actualProfitLoss);
					
					int column = 0;
					
					if(i%1000 == 0) {

					}
					Row row = ssSheet.createRow(i);
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceId());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getJktPoId());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getSalesPerson());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getFillDateStr().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getEventDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getVenueName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInternalNotes().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getSection());
			        column++;
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(ticketGroups.getRow());
			        column++;
			       
			        row.createCell((int) column).setCellValue(ticketGroups.getQty());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getWholesalePrice());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getCost());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceTotal());
				    column++;
				    
				    Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(cost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
				    
				    
				    amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(ccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(tnfees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			    	
			    	
			    	amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getClientType().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientFirstName().replaceAll(",", "-"));
			    	column++;
			    	
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientCompanyName().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientMail().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getPhoneNumber().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress1().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress2().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getCity().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getState().replaceAll(",", "-"));
			    	column++;
			    	
			        i++;
			        
				}
				
				j=0;
				
				if(null != jktInvoices && !jktInvoices.isEmpty() && jktInvoices.size() >0){
					Row row = ssSheet.createRow(i);
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        Cell seatCell = row.createCell(j,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue("TOTAL");
			        j++;
			        
			        row.createCell((int) j).setCellValue(totalQty);
			        j++;
			        
			        Cell amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalAcutalPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(invoiceTotal.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyCost.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	

				    amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyActualPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalccFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalTnFees.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			    	
			    	amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalActualProfitLoss.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
				}
				
			}
		 
		 public static void generateUnfilledInvoiceSheet(SXSSFWorkbook workbook ,List<POSTicketGroup> jktInvoices,String sheetName,
				 boolean showLastColum,boolean showVoidDate,boolean showClientInfo,boolean showProfitLossInfo) throws Exception{
			 
				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
				
				int j = 0;
			
				 Row rowhead =   ssSheet.createRow((int)0);
				 rowhead.createCell((int) j).setCellValue("Broker");
				 j++;	
			    rowhead.createCell((int) j).setCellValue("Invoice No");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Invoice Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Sales Person");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Parent Category");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Is Zone Event");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Internal Notes");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Section");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Row");
			    j++;
			    /*rowhead.createCell((int) j).setCellValue("Seat");
			    j++;*/
			    rowhead.createCell((int) j).setCellValue("Qty");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Sold Price Per Ticket");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Invoice Total");
			    j++;
			    
			    
			    rowhead.createCell((int) j).setCellValue("Total Sold Price");
			    j++;
			    
			    rowhead.createCell((int) j).setCellValue("Client Type");
		    	j++;
		    	
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Company Name");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Client Email");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Phone Number");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address1");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Street Address2");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("State");
		    	j++;
			    
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    cellamountStyle.setDataFormat(
			    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			    
			    int i=1,totalQty=0;
			    Double totalCost =0.0,totalAcutalPrice=0.0,totalActualPriceSold=0.0,totalRetailPrice=0.0,tgCommission=0.0,wholeSalePrice=0.0,tnfees=0.0 ,actualProfitLoss=0.0;
			    Double totalQtyCost=0.0,totalQtyActualPrice=0.0,cost=0.0,actualSoldPrice=0.0,totalTnFees=0.0,
			    totalActualProfitLoss=0.0,tnCommissionPerc=0.0,invoiceTotal=0.00,ccFees=0.00,totalccFees=0.00,ccFeePerc=0.00;
				for (POSTicketGroup ticketGroups :jktInvoices) {
					totalQty=totalQty+ticketGroups.getQty();
					totalAcutalPrice=totalAcutalPrice+ticketGroups.getWholesalePrice();
					totalActualPriceSold=totalActualPriceSold+ticketGroups.getTotalCost();
					totalCost=totalCost+ticketGroups.getCost();
					totalRetailPrice=totalRetailPrice+ticketGroups.getRetailPrice();
					wholeSalePrice=wholeSalePrice+ticketGroups.getWholesalePrice();
					tgCommission=tgCommission+ticketGroups.getTgCommission();
					
					cost = ticketGroups.getQty() * ticketGroups.getCost();
					actualSoldPrice = ticketGroups.getQty() * ticketGroups.getWholesalePrice();
					invoiceTotal = invoiceTotal + ticketGroups.getInvoiceTotal();
					
					if(ticketGroups.getIsTixcityOrder()){
						tnCommissionPerc = 5.0;
						ccFeePerc=0.0;
					}else if(ticketGroups.getIsRetail()){
						tnCommissionPerc = 0.00;
						ccFeePerc=3.25;
					}else if(ticketGroups.getIsSH() || ticketGroups.getIsTE()){
						tnCommissionPerc = 0.00;
						ccFeePerc=0.00;
					}else if(ticketGroups.getIsLong()){
						tnCommissionPerc = 3.50;
						ccFeePerc=3.25;
					}else{
						tnCommissionPerc = 8.25;
						ccFeePerc=3.25;
					}


					tnfees = (actualSoldPrice * tnCommissionPerc ) / 100;
					ccFees = (ticketGroups.getInvoiceTotal() * ccFeePerc) / 100;
					actualProfitLoss = actualSoldPrice - (cost + tnfees + ccFees);
					
					totalTnFees = totalTnFees + tnfees;
					totalccFees = totalccFees + ccFees;
					totalActualProfitLoss = totalActualProfitLoss + actualProfitLoss;
					totalQtyCost = totalQtyCost+cost;
					totalQtyActualPrice = totalQtyActualPrice+actualSoldPrice;
					
					System.out.println("Invoice Total:"+ticketGroups.getInvoiceTotal()+", Sold Price:"+actualSoldPrice+", Cost:"+cost+", CCFees:"+ccFees+", TnFees:"+tnfees+", ProfitLoss:"+actualProfitLoss);
					
					int column = 0;
					
					if(i%1000 == 0) {

					}
					Row row = ssSheet.createRow(i);
			        row.createCell((int) column).setCellValue(ticketGroups.getTmatBrokerName());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceId());
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getSalesPerson());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventParentCategory().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue((ticketGroups.getZoneEvent()) == 0?"No":"Yes");
			        column++;
			        
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getEventName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getEventDateStr().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getVenueName().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getInternalNotes().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(ticketGroups.getSection());
			        column++;
			        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        rowCell.setCellValue(ticketGroups.getRow());
			        column++;
			       
			        row.createCell((int) column).setCellValue(ticketGroups.getQty());
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getWholesalePrice());
			        column++;
			        
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getInvoiceTotal());
				    column++;
				    
				    Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(actualSoldPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        row.createCell((int) column).setCellValue(ticketGroups.getClientType().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientFirstName().replaceAll(",", "-"));
			    	column++;
			    	
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientCompanyName().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getClientMail().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getPhoneNumber().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress1().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getStreetAddress2().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getCity().replaceAll(",", "-"));
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(ticketGroups.getState().replaceAll(",", "-"));
			    	column++;
			    	
			        i++;
			        
				}
				
				j=0;
				
				if(null != jktInvoices && !jktInvoices.isEmpty() && jktInvoices.size() >0){
					Row row = ssSheet.createRow(i);
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     
				     row.createCell((int) j).setCellValue("");
				     j++;
				     row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        Cell seatCell = row.createCell(j,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue("TOTAL");
			        j++;
			        
			        row.createCell((int) j).setCellValue(totalQty);
			        j++;
			        
			        Cell amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalAcutalPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        
			        
			        amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(invoiceTotal.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			    	
			    	

				    amountCell = row.createCell(j,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(Double.valueOf(totalQtyActualPrice.toString()));//Actual Sold Price
			        amountCell.setCellStyle(cellamountStyle);
			        j++;
			        
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
		        	 row.createCell((int) j).setCellValue("");
				     j++;
				     
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			        row.createCell((int) j).setCellValue("");
			        j++;
			    	
				}
				
			}
		
		/*public List<Object[]> dummySalesData() throws ParseException{
			
			List<Object[]> list = new ArrayList<Object[]>();
			
			Object[] zonesPricingDummySale = new Object[15];
			Object[] zonesLastRowDummySale = new Object[15];
			Object[] zonesDummySale = new Object[15];
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			zonesPricingDummySale[0] = (Object)"Zones Pricing Dummy Event";
			Date zonesEventDate = dateFormat.parse("01/29/2015 08:42:23");
			Timestamp zoneeventstsp = new Timestamp(zonesEventDate.getTime());
			zonesPricingDummySale[1] = zoneeventstsp;
			zonesPricingDummySale[2] = (Object)"Upper";
			zonesPricingDummySale[3] = (Object)"A";
			zonesPricingDummySale[4] = (Object)"123456";
			zonesPricingDummySale[5] = 12345;
			
			Date zonesSalesDate = dateFormat.parse("01/30/2015 09:42:23");
			Timestamp zonestsp = new Timestamp(zonesSalesDate.getTime());
			zonesPricingDummySale[6] = zonestsp;
			zonesPricingDummySale[7] = (Object)"1234";
			zonesPricingDummySale[8] = (Object)(Object)"220";
			zonesPricingDummySale[9] = (Object)"200";
			zonesPricingDummySale[10] = (Object)"210.20";
			zonesPricingDummySale[11] = (Object)"0.00";
			zonesPricingDummySale[12] = (Object)"ZP";
			zonesPricingDummySale[13] = (Object)"NULL";
			zonesPricingDummySale[14] = (Object)"Web User";
			
			zonesLastRowDummySale[0] = (Object)"Zones Last Row Dummy Event";
			Date zonesZLREventDate = dateFormat.parse("01/29/2015 08:45:23");
			Timestamp zoneZLReventstsp = new Timestamp(zonesZLREventDate.getTime());
			zonesLastRowDummySale[1] = zoneZLReventstsp;
			zonesLastRowDummySale[2] = (Object)"Lower";
			zonesLastRowDummySale[3] = (Object)"B";
			zonesLastRowDummySale[4] = (Object)"654321";
			zonesLastRowDummySale[5] = 54321;
			Date zoneslastrowSalesDate = dateFormat.parse("01/30/2015 09:45:23");
			zonestsp = new Timestamp(zoneslastrowSalesDate.getTime());
			zonesLastRowDummySale[6] = zonestsp;
			zonesLastRowDummySale[7] = (Object)"4321";
			zonesLastRowDummySale[8] = (Object)"330";
			zonesLastRowDummySale[9] = (Object)"300";
			zonesLastRowDummySale[10] = (Object)"310.20";
			zonesLastRowDummySale[11] = (Object)"0.00";
			zonesLastRowDummySale[12] = (Object)"ZLR";
			zonesLastRowDummySale[13] = (Object)"NULL";
			zonesLastRowDummySale[14] = (Object)"Web User";
			
			zonesDummySale[0] = (Object)"Zones Dummy Event";
			Date zonessEventDate = dateFormat.parse("01/29/2015 08:45:23");
			Timestamp zoneseventstsp = new Timestamp(zonessEventDate.getTime());
			zonesDummySale[1] = zoneseventstsp;
			zonesDummySale[2] = (Object)"zone";
			zonesDummySale[3] = (Object)"B";
			zonesDummySale[4] = (Object)"159753";
			zonesDummySale[5] = 98765;
			Date zoneSalesDate = dateFormat.parse("01/30/2015 09:48:23");
			zonestsp = new Timestamp(zoneSalesDate.getTime());
			zonesDummySale[6] = zonestsp;
			zonesDummySale[7] = (Object)"4321";
			zonesDummySale[8] = (Object)"330";
			zonesDummySale[9] = (Object)"300";
			zonesDummySale[10] = (Object)"310.20";
			zonesDummySale[11] = (Object)"0.00";
			zonesDummySale[12] = (Object)"hello";
			zonesDummySale[13] = (Object)"NULL";
			zonesDummySale[14] = (Object)"Web User";
			
			list.add(zonesPricingDummySale);
			list.add(zonesLastRowDummySale);
			list.add(zonesDummySale);
			
			return list;
		}*/
		 
		 public void downloadRTWUnsoldInventory(HttpServletRequest request,
					HttpServletResponse response, ModelMap map) throws Exception {
				
		        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				
				 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-disposition",
						"attachment; filename=RTWUnsoldInventory." + today
								+ ".xlsx");
				try {
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					getRTWUnSoldInvetory(workbook,5);//RTW
					getRTWUnSoldInvetory(workbook,10);//RTW-2
					 System.out.println("RTW_LONG_INVENTORY...completed........"+new Date());
					 workbook.write(out);
				} catch (Exception e) {
					e.printStackTrace();
				}
					 
			}
		 
		 public static void getRTWUnSoldInvetory(SXSSFWorkbook workbook,Integer brokerId) throws Exception {


				//List<Object[]> list = DAORegistry.getQueryManagerDAO().getRTWLongInventory(Connections.getRTWPOSConnection());
			 List<Object[]> list = DAORegistry.getQueryManagerDAO().getRTWUnSoldLongInventory(brokerId);
			 
			 DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");
			 
			 String sheetName = null;
			 if(brokerId.equals(5)) {//RTW
				 sheetName = "RTW_LONG_INVENTORY";
			 } else if(brokerId.equals(10)) {//RTW-2
				 sheetName = "RTW-2_LONG_INVENTORY";
			 }
				Sheet ssSheet =  workbook.createSheet(sheetName);
				int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
				//rowhead.createCell((int) j).setCellValue("Broker");
			    //j++;
				
				rowhead.createCell((int) j).setCellValue("PurchaseOrderId");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("PO Date");
		    	j++;
				rowhead.createCell((int) j).setCellValue("EventID");
			    j++;
			    rowhead.createCell((int) j).setCellValue("EventName");
			    j++;
			    rowhead.createCell((int) j).setCellValue("EventDate");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("EventTime");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("VenueName");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("VenueCity");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Section");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Row");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Seat");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Original_Ticket_Count");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Remaining_Ticket_Count");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Retail_Price");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Face_Price");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Cost");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Total Cost");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("WholeSale_Price");
		 	    j++;
		    	rowhead.createCell((int) j).setCellValue("InternalNotes");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("OnHand");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("InstandDownload");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("StockType");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("ClientBrokerCompanyName");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("ClientBrokerEmployeeName");
		    	j++;
			    
		    	CellStyle cellamountStyle = workbook.createCellStyle();
		        cellamountStyle.setDataFormat(
		          workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
		        double totOriginalTixCount=0,totRemainingTixCount=0,totRetailPrice=0,totFacePrice=0,totCost=0,totTotalCost=0,totWholesalePrice=0;
		        int totalRecords = 0;
			    int rowvariable = 1;
			    for (Object[] obj : list) {
					 
			    	if(null == obj || obj.length == 0){
			    		continue;
			    	}
					 
			    	double tixTotalcost = 0.0,cost=0.0,tixRetailPrice=0.0,tixFacePrice=0.0,tixWholesalePrice=0.0;
			    	int remainingQty = 0;
			    	
			    	if(obj[10] != null) {
			        	remainingQty = Integer.parseInt(obj[10].toString());
			        	totRemainingTixCount+=remainingQty;
			        }
			    	if(obj[13] != null) {
			    		cost = Double.parseDouble(obj[13].toString());	
			    		totCost += cost;
			        }
			    	tixTotalcost = remainingQty*cost;
			    	tixTotalcost = Math.round(tixTotalcost * 100.0) / 100.0;
			    	
			    	totTotalCost = totTotalCost + tixTotalcost;
			    	totalRecords++;
			    	
			    	if(obj[11] != null) {
			    		tixRetailPrice=Double.valueOf(obj[11].toString());
				        totRetailPrice += tixRetailPrice;
			        }
			    	if(obj[12] != null) {
			    		tixFacePrice=Double.valueOf(obj[12].toString());
				        totFacePrice += tixFacePrice;
			        }
			    	if(obj[14] != null) {
			    		tixWholesalePrice=Double.valueOf(obj[14].toString());
				        totWholesalePrice += tixWholesalePrice;
			        }
			       
			    	int column = 0;
			    	Row row = ssSheet.createRow(rowvariable);
			        //row.createCell((int) column).setCellValue(obj[22].toString().replaceAll(",", "-"));
			        //column++;
					//row = ssSheet.createRow(rowvariable);
			    	row.createCell((int) column).setCellValue(obj[19] != null?obj[19].toString().replaceAll(",", "-"):"");
			        column++;
			        if(obj[22] != null) {
			        	Date poDate = (Date)obj[22];
			        	row.createCell((int) column).setCellValue(dateTimeformat.format(poDate));
				        column++;	
			        } else {
			        	row.createCell((int) column).setCellValue("");
			        	column++;	
			        }
			        
			        row.createCell((int) column).setCellValue(obj[0].toString().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(obj[1].toString());
			        column++;
		        	row.createCell((int) column).setCellValue(obj[2].toString());
			    	column++;
			    	row.createCell((int) column).setCellValue(obj[3].toString().replaceAll(",", "-"));
			    	column++;
			    	
			    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortSectionCell.setCellValue(obj[4].toString().replaceAll(",", "-"));
			        column++;
			        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        shortRowCell.setCellValue(obj[5].toString().replaceAll(",", "-"));
			        column++;
			        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(obj[6].toString().replaceAll(",", "-"));
			        column++;
			        
			        row.createCell((int) column).setCellValue(obj[7].toString().replaceAll(",", "-"));
			        column++;
			        row.createCell((int) column).setCellValue(obj[8].toString().replaceAll(",", "-"));
			        column++;
			        int tixOriginalQty=Integer.valueOf(obj[9].toString());
			        totOriginalTixCount += tixOriginalQty;
			        
			        row.createCell((int) column).setCellValue(tixOriginalQty);
			        column++;
			        
			        seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(remainingQty);
			        column++;
			        
			        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(tixRetailPrice);
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(tixFacePrice);
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(cost);
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(tixTotalcost);
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
			        amountCell.setCellValue(tixWholesalePrice);
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        row.createCell((int) column).setCellValue(obj[15] != null?obj[15].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[16] != null?obj[16].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[17] != null?obj[17].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[18] != null?obj[18].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[20] != null?obj[20].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[21] != null?obj[21].toString().replaceAll(",", "-"):"");
			        column++;
			        rowvariable++;
				}
			    
			    rowvariable++;
			    int column = 0;
		    	Row row = ssSheet.createRow(rowvariable);
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue("");
		    	column++;
		    	row.createCell((int) column).setCellValue(totOriginalTixCount);
		    	column++;
		    	row.createCell((int) column).setCellValue(totRemainingTixCount);
		    	column++;
		    	row.createCell((int) column).setCellValue(totRetailPrice);
		    	column++;
		    	row.createCell((int) column).setCellValue(totFacePrice);
		    	column++;
		    	row.createCell((int) column).setCellValue(totCost);
		    	column++;
		    	row.createCell((int) column).setCellValue(totTotalCost);
		    	column++;
		    	row.createCell((int) column).setCellValue(totWholesalePrice);
		    	column++;
		    	
		    	/* rowvariable++;
			    rowvariable++;
			    row = ssSheet.createRow(rowvariable);
		    	column=16;
		    	row.createCell((int) column).setCellValue("Average Total Cost");
		    	column++;
		    	double averageTotalCost = totTotalCost/totalRecords;
		    	averageTotalCost = Math.round(averageTotalCost * 100.0) / 100.0;
		    	
		    	Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		    	amountCell.setCellValue(averageTotalCost);
		    	amountCell.setCellStyle(cellamountStyle);
		    	column++;*/
				
		    	//return workbook;
			
			}
		 
		 public void downloadRTWUnsoldInventoryAverageCost(HttpServletRequest request,
					HttpServletResponse response, ModelMap map) throws Exception {
				
		        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				
				 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-disposition",
						"attachment; filename=RTWUnsoldInventoryAverage." + today
								+ ".xlsx");
				try {
					SXSSFWorkbook workbook = new SXSSFWorkbook();
					getRTWUnSoldInvetoryAverageCost(workbook);//RTW
					//getRTWUnSoldInvetoryAverage(workbook,10);//RTW-2
					 System.out.println("RTW_LONG_INVENTORY Average...completed........"+new Date());
					 workbook.write(out);
				} catch (Exception e) {
					e.printStackTrace();
				}
					 
			}
		 
		 public static void getRTWUnSoldInvetoryAverageCost(SXSSFWorkbook workbook) throws Exception {


				//List<Object[]> list = DAORegistry.getQueryManagerDAO().getRTWLongInventory(Connections.getRTWPOSConnection());
			 
			 DateFormat dateTimeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			 DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
			 List<Object[]> rtwDataList = DAORegistry.getQueryManagerDAO().getAllRTWUnSoldLongInventoryAvgCost();
			 
			 Sheet ssSheet =  workbook.createSheet();
			 int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("Date");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Total Cost");
			 j++;
			    
			 CellStyle cellamountStyle = workbook.createCellStyle();
			 cellamountStyle.setDataFormat(
					 workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
			    
			 int rowvariable = 1;
			 for (Object[] obj : rtwDataList) {
					 
				 if(null == obj || obj.length == 0){
					 continue;
				 }
				 String dateStr = obj[0].toString();
				 Double avgCost = Double.parseDouble(obj[1].toString());
				 
				 int column = 0;
				 Row row = ssSheet.createRow(rowvariable);
				 row.createCell((int) column).setCellValue(dateformat.format(dateTimeformat.parse(dateStr)));
				 column++;
			    	
				 Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
				 amountCell.setCellValue(avgCost);
				 amountCell.setCellStyle(cellamountStyle);
				 column++;
			    	
				 rowvariable++;
				}
			    
			}
		 
		 public void downloadEventCrawlSalesListOld(HttpServletRequest request,
					HttpServletResponse response, ModelMap map) throws Exception {
				
		        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=EventCrawlSalesList." + today
								+ ".xlsx");
				try {
					getEventCrawlsSalesListOld(workbook, "event_crawl_list6", "Event_Crawls_Sales_Report_1");
					getEventCrawlsSalesListOld(workbook, "event_crawl_list7", "Event_Crawls_Sales_Report_2");
					workbook.write(response.getOutputStream());
					System.out.println("downloadEventCrawlSalesList...completed........"+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
					 
			}
		 
		 public static SXSSFWorkbook getEventCrawlsSalesListOld(SXSSFWorkbook workbook,String procName,String sheetName) throws Exception {

				List<Object[]> list = DAORegistry.getQueryManagerDAO().getEventCrawlSalesListOld(Connections.getTmatConnection(),procName);

				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);

				
				int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
				rowhead.createCell((int) j).setCellValue("Event ID");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Created Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("EventTime");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Parent");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Child");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Grand_child");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Venue Name");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("city");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("state");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("country");
		    	j++;
		    	
		    	rowhead.createCell((int) j).setCellValue("Event_creation_Time");
		 	    j++;
		    	rowhead.createCell((int) j).setCellValue("ticketnetworkdirect");
		    	j++;
			    rowhead.createCell((int) j).setCellValue("stubhub");
			    j++;
			    rowhead.createCell((int) j).setCellValue("ticketevolution");
			    j++;
			    rowhead.createCell((int) j).setCellValue("vividseat");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Sales_hit");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Csv");
			    j++;
			    rowhead.createCell((int) j).setCellValue("2015_Sales_Count");
			    j++;
			    rowhead.createCell((int) j).setCellValue("2016_Sales_Count");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Id");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Category_Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Map");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Last_2_Days_Sales");
			    j++;
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    
			    int rowvariable = 1;
			    for (Object[] obj : list) {
					 
			    	if(null == obj || obj.length == 0){
			    		continue;
			    	}
					 
			    	int column = 0;
					Row row = ssSheet.createRow(rowvariable);
			        row.createCell((int) column).setCellValue(null != obj[0]?obj[0].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(null != obj[1]?obj[1].toString():"");
			        column++;
		        	row.createCell((int) column).setCellValue(null != obj[2]?obj[2].toString():"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[3]?obj[3].toString().replaceAll(",", "-"):"");
			    	column++;
			    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortSectionCell.setCellValue(null != obj[4]?obj[4].toString().replaceAll(",", "-"):"");
			        column++;
			        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        shortRowCell.setCellValue(null != obj[5]?obj[5].toString().replaceAll(",", "-"):"");
			        column++;
			        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(null != obj[6]?obj[6].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(null != obj[7]?obj[7].toString().replaceAll(",", "-"):"");
			        column++;
			        
			        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[8] != null?obj[8].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[9] != null?obj[9].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[10] != null?obj[10].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[11] != null?obj[11].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[12] != null?obj[12].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        row.createCell((int) column).setCellValue(obj[13] != null?obj[13].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[14] != null?obj[14].toString().replaceAll(",", "-"):"");
			        column++;
			        
			        String salesDate = obj[15].toString();
			        if(salesDate != null && !salesDate.isEmpty()) {
			        	 row.createCell((int) column).setCellValue(salesDate);
			        	 column++;
			        } else {
			        	 row.createCell((int) column).setCellValue("");
			        	 column++;
			        }

			        row.createCell((int) column).setCellValue(null != obj[16]?obj[16].toString().replaceAll(",", "-"):"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[17]?obj[17].toString():"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[18]?obj[18].toString():"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[19]?String.valueOf(obj[19]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[20]?String.valueOf(obj[20]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[21]?String.valueOf(obj[21]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[22]?String.valueOf(obj[22]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[23]?String.valueOf(obj[23]):"");
			    	column++;
			    	
			    	
			    	row.createCell((int) column).setCellValue(null != obj[24]?String.valueOf(obj[24]):"");
			    	column++;
			       
			        rowvariable++;
				}
				return workbook;
			
			}
		 
		 public void downloadEventCrawlSalesList(HttpServletRequest request,
					HttpServletResponse response, ModelMap map) throws Exception {
				
		        DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
				String today = dateFormat.format(new Date());
				ServletOutputStream out = response.getOutputStream();
				SXSSFWorkbook workbook = new SXSSFWorkbook();
				 response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Type", "application/vnd.ms-excel");
				response.setHeader("Content-disposition",
						"attachment; filename=EventCrawlSalesList." + today
								+ ".xlsx");
				try {
					getEventCrawlsSalesList(workbook, "Event_Crawls_Sales_Report_1",true);
					getEventCrawlsSalesList(workbook, "Event_Crawls_Sales_Report_2",false);
					workbook.write(response.getOutputStream());
					System.out.println("downloadEventCrawlSalesList...completed........"+new Date());
				} catch (Exception e) {
					e.printStackTrace();
				}
					 
			}
		 
		 public static SXSSFWorkbook getEventCrawlsSalesList(SXSSFWorkbook workbook,String sheetName,boolean isFirstSheet) throws Exception {

				List<Object[]> list = DAORegistry.getQueryManagerDAO().getEventCrawlSalesList(isFirstSheet);
				SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
				
				int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
				rowhead.createCell((int) j).setCellValue("Event ID");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("EventTime");
		    	j++;
			    rowhead.createCell((int) j).setCellValue("Event Created Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event_creation_Time");
		 	    j++;
		    	rowhead.createCell((int) j).setCellValue("Parent");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Child");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Grand_child");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Venue Name");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("city");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("state");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("country");
		    	j++;
		    	
		    	
		    	rowhead.createCell((int) j).setCellValue("ticketnetworkdirect");
		    	j++;
			    rowhead.createCell((int) j).setCellValue("stubhub");
			    j++;
			    rowhead.createCell((int) j).setCellValue("ticketevolution");
			    j++;
			    rowhead.createCell((int) j).setCellValue("vividseat");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Sales_hit");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Csv");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Last_3_Month_Sales");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Id");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Category_Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue_Map");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Last_2_Days_Sales");
			    j++;
			    
			    CellStyle cellamountStyle = workbook.createCellStyle();
			    
			    int rowvariable = 1;
			    for (Object[] obj : list) {
					 
			    	if(null == obj || obj.length == 0){
			    		continue;
			    	}
					 
			    	int column = 0;
					Row row = ssSheet.createRow(rowvariable);
			        row.createCell((int) column).setCellValue(null != obj[0]?obj[0].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(null != obj[1]?obj[1].toString():"");
			        column++;
		        	row.createCell((int) column).setCellValue(null != obj[2]?obj[2].toString():"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[3]?obj[3].toString().replaceAll(",", "-"):"");
			    	column++;
			    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			    	shortSectionCell.setCellValue(null != obj[4]?obj[4].toString().replaceAll(",", "-"):"");
			        column++;
			        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        shortRowCell.setCellValue(null != obj[5]?obj[5].toString().replaceAll(",", "-"):"");
			        column++;
			        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        seatCell.setCellValue(null != obj[6]?obj[6].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(null != obj[7]?obj[7].toString().replaceAll(",", "-"):"");
			        column++;
			        
			        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[8] != null?obj[8].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[9] != null?obj[9].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[10] != null?obj[10].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[11] != null?obj[11].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
			        amountCell.setCellValue(obj[12] != null?obj[12].toString().replaceAll(",", "-"):"");
			        amountCell.setCellStyle(cellamountStyle);
			        column++;
			        row.createCell((int) column).setCellValue(obj[13] != null?obj[13].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[14] != null?obj[14].toString().replaceAll(",", "-"):"");
			        column++;
			        row.createCell((int) column).setCellValue(obj[15] != null?obj[15].toString().replaceAll(",", "-"):"");
			        column++;
			        
			        row.createCell((int) column).setCellValue(null != obj[16]?obj[16].toString().replaceAll(",", "-"):"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[17]?obj[17].toString():"");
			    	column++;
			    	row.createCell((int) column).setCellValue(null != obj[18]?obj[18].toString():"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[19]?String.valueOf(obj[19]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[20]?String.valueOf(obj[20]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[21]?String.valueOf(obj[21]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[22]?String.valueOf(obj[22]):"");
			    	column++;
			    	
			    	row.createCell((int) column).setCellValue(null != obj[23]?String.valueOf(obj[23]):"");
			    	column++;
			       
			        rowvariable++;
				}
				return workbook;
			
			}
		 
	public void downloadTmatPosVenueMismatchEvents(HttpServletRequest request,HttpServletResponse response) throws Exception {
				
				
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
			"attachment; filename=TMAT_andPOS_Venue_Mismatch_Events." + today
					+ ".xlsx");
		
		try {
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTmatandPosVenueMismatchingEvents();
			 
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			Sheet ssSheet =  workbook.createSheet();
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
		    rowhead.createCell((int) j).setCellValue("AdmitOneId");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TMAT_EventId");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("TMAT_EventName");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("TMAT_EventDate");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("TMAT_EventTime");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("TMAT_Venue");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("TMAT_City");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TMAT_State");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TMAT_Country");
		    j++;
		    rowhead.createCell((int) j).setCellValue("TN_VenueId");
		    j++;
		    rowhead.createCell((int) j).setCellValue("POS_Venue");
		    j++;
		    rowhead.createCell((int) j).setCellValue("POS_City");
		    j++;
		    rowhead.createCell((int) j).setCellValue("POS_State");
		    j++;
		    rowhead.createCell((int) j).setCellValue("POS_Country");
		    j++;
		    rowhead.createCell((int) j).setCellValue("POS_VenueId");
		    j++;
		    
		    if(list != null) {
		    	 int rowvariable = 1;
				 for (Object[] obj : list) {
					 
						 
					 int column = 0;
					Row row = ssSheet.createRow(rowvariable);
					if(obj[0] != null) {
						row.createCell((int) column).setCellValue(obj[0].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[1] != null) {
						row.createCell((int) column).setCellValue(obj[1].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[2] != null) {
						row.createCell((int) column).setCellValue(obj[2].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[3] != null) {
						row.createCell((int) column).setCellValue(obj[3].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[4] != null) {
						row.createCell((int) column).setCellValue(obj[4].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[5] != null) {
						row.createCell((int) column).setCellValue(obj[5].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[6] != null) {
						row.createCell((int) column).setCellValue(obj[6].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[7] != null) {
						row.createCell((int) column).setCellValue(obj[7].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[8] != null) {
						row.createCell((int) column).setCellValue(obj[8].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[9] != null) {
						row.createCell((int) column).setCellValue(obj[9].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[10] != null) {
						row.createCell((int) column).setCellValue(obj[10].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[11] != null) {
						row.createCell((int) column).setCellValue(obj[11].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[12] != null) {
						row.createCell((int) column).setCellValue(obj[12].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[13] != null) {
						row.createCell((int) column).setCellValue(obj[13].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(obj[14] != null) {
						row.createCell((int) column).setCellValue(obj[14].toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
			        rowvariable++;
				}
		    }
		    workbook.write(out);
		    System.out.println("PosUnfilledShortswithLongInventory...completed........"+new Date());
		    
		} catch( Exception e) {
			e.printStackTrace();
		}
			 
	}
	/*public void downloadMissingSvgAndCsvZones(HttpServletRequest request,HttpServletResponse response) throws Exception { 
	
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
			"attachment; filename=SVG_and_CSV_Missing_Zones." + today
					+ ".xlsx");
		
		try{
			Map<Integer,List<VenueCategory>> venueCategoriesMap = new HashMap<Integer, List<VenueCategory>>();
			
			 Collection<Venue> venues = DAORegistry.getVenueDAO().getAllVenuesForUSandCA();
			 List<VenueCategory> venueCategoriesList = DAORegistry.getVenueCategoryDAO().getAllVenueCategoriesForUSandCA();
			 
			 for (VenueCategory venueCategory : venueCategoriesList) {
				 Integer venueId = venueCategory.getVenue().getId();
				 List<VenueCategory> tempVenueCats = venueCategoriesMap.get(venueId);
				 if(tempVenueCats == null) {
					 tempVenueCats = new ArrayList<VenueCategory>();
				 }
				 tempVenueCats.add(venueCategory);
				 venueCategoriesMap.put(venueId, tempVenueCats);
			}
			 
			 FileReader fReader = null;
			 BufferedReader reader = null;
			 String svgText = "",str="";
			 List<SVGFileMissingZoneData> missingSvgZonesList = new ArrayList<SVGFileMissingZoneData>();
			 List<SVGFileMissingZoneData> missingCSVZonesList = new ArrayList<SVGFileMissingZoneData>();
			 List<SVGFileMissingZoneData> invalidFormatSVGFiles = new ArrayList<SVGFileMissingZoneData>();
			 
			 int totVenues=venues.size(),processedVenues=0;
			 for (Venue venue : venues) {
				 Integer venueId = venue.getId();
				 
				 processedVenues++;
				 if(processedVenues%100 == 0) {
					 System.out.println(processedVenues+" / "+totVenues+" : SVG and CSV Missing Venue : "+venueId);
				 }
				 
				 List<VenueCategory> venueCategories = venueCategoriesMap.get(venueId);
				 if(venueCategories == null) {
					 continue;
				 }
				 //List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueIdOrderByCategory(venueId);
			
				 for (VenueCategory venueCatObj : venueCategories) {
					 try {
						 String fileName = venueId+"_"+venueCatObj.getCategoryGroup();
						 List<String> missingCsvZones = new ArrayList<String>();
						 List<String> missingSvgZones = new ArrayList<String>();
						
						 
						 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
						 Map<String, Boolean> csvZoneMap = new HashMap<String, Boolean>();
						 
						 List<String> zoneList = DAORegistry.getQueryManagerDAO().getAllZoneByVenueCategory(venueId);
						 if(zoneList != null) {
							 for (String zone : zoneList) {
								 csvZoneMap.put(zone.toUpperCase(), true);
							}
						 }
						 
						 str ="";
						 File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//"+fileName+".txt");
						 if(svgTxtFile != null && svgTxtFile.exists()) {
							fReader = new FileReader(svgTxtFile);
							reader = new BufferedReader(fReader);
							while((str = reader.readLine()) != null) {
								svgText += str;
							}
							try {
								svgZoneMap = getAllZonesFromText(svgText);
							} catch(Exception e) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 invalidFormatSVGFiles.add(zoneData);
								 continue;
							}
						 }
						 
						 for (String key : svgZoneMap.keySet()) {
							 Boolean isZonethere = csvZoneMap.remove(key);
							 if(null == isZonethere || !isZonethere){
								 missingCsvZones.add(key);
							 }
						 }
						 for (String key : csvZoneMap.keySet()) {
							 missingSvgZones.add(key);
						 }
						 
						 if(!missingSvgZones.isEmpty()) {
							 for (String zone : missingSvgZones) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 zoneData.setZone(zone);
								 missingSvgZonesList.add(zoneData);
								 
							}
						 }
						 if(!missingCsvZones.isEmpty()) {
							 for (String zone : missingCsvZones) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 zoneData.setZone(zone);
								 missingCSVZonesList.add(zoneData);
								 
							}
						 }
					 } catch (Exception e) {
						 e.printStackTrace();
					}
				}
			 }
			 SXSSFWorkbook workbook = new SXSSFWorkbook();
			generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),missingCSVZonesList,true);
			generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),missingSvgZonesList,true);
			generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
			 
			workbook.write(out);
		 }catch (Exception e){
			 e.printStackTrace();
		 }
	}*/
	
	public void downloadMissingSvgAndCsvZones(HttpServletRequest request,HttpServletResponse response) throws Exception { 
		
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
			"attachment; filename=SVG_and_CSV_Missing_Zones." + today
					+ ".xlsx");
		
		try{
			 
			 List<SVGFileMissingZoneData> svgZonesnotinCsvList = DAORegistry.getQueryManagerDAO().getAllVenueMapZonesNotInCSV();
			 List<SVGFileMissingZoneData> csvZonesnotinSvgList = DAORegistry.getQueryManagerDAO().getAllCsvZonesNotInVenueMap();
			 
			 //List<SVGFileMissingZoneData> invalidFormatSVGFiles = new ArrayList<SVGFileMissingZoneData>();
			 
			 SXSSFWorkbook workbook = new SXSSFWorkbook();
			generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),svgZonesnotinCsvList,true);
			generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),csvZonesnotinSvgList,true);
			//generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
			 
			workbook.write(out);
		 }catch (Exception e){
			 e.printStackTrace();
		 }
	}
	
	public void generateMissingSVGandCSVzonesFile(Sheet ssSheet,List<SVGFileMissingZoneData> zoneDataList,boolean showZoneColumn) throws Exception {
		
		try {
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
		    rowhead.createCell((int) j).setCellValue("Venue Id");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Building");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("City");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("State");
	 	    j++;
	 	    rowhead.createCell((int) j).setCellValue("Country");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Venue Category Id");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("Venue Category Group");
		    j++;
		    if(showZoneColumn) {
		    	rowhead.createCell((int) j).setCellValue("Zone");
		    	j++;
		    }
		    
		    if(zoneDataList != null) {
		    	 int rowvariable = 1;
				 for (SVGFileMissingZoneData zoneData : zoneDataList) {
					 
						 
					 int column = 0;
					Row row = ssSheet.createRow(rowvariable);
					row.createCell((int) column).setCellValue(zoneData.getVenueId().toString());
					column++;
					row.createCell((int) column).setCellValue(zoneData.getBuilding().toString());
					column++;
					
					if(zoneData.getCity() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCity().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getState() != null) {
						row.createCell((int) column).setCellValue(zoneData.getState().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getCountry() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCountry().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getVenueCategoryId() != null) {
						row.createCell((int) column).setCellValue(zoneData.getVenueCategoryId().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(zoneData.getCategoryGroupName() != null) {
						row.createCell((int) column).setCellValue(zoneData.getCategoryGroupName().toString());
				        column++;
					} else {
						row.createCell((int) column).setCellValue("");
				        column++;
					}
					if(showZoneColumn) {
						if(zoneData.getZone() != null) {
							row.createCell((int) column).setCellValue(zoneData.getZone().toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
					}
			        rowvariable++;
				}
		    }
		    
		} catch( Exception e) {
			e.printStackTrace();
		}
			 
	}
	public static Map<String, Boolean> getAllZonesFromText(String svgText) throws Exception {
		
		Map<String, Boolean> allZonesMap = new HashMap<String, Boolean>();
		try {
		
			Pattern pattern = Pattern.compile("<g id=\"zone_letters\" display=\"none\">(.*?)</g>");
			Matcher matcher = pattern.matcher(svgText);
						
			String value = new String();
			
			if(matcher.find()){
				//System.out.println("ZONE :" + dayMatcher.group(1));
				value = matcher.group(1);
			}
			
			String[] strings = value.split("transform=");
			//System.out.println(value);
			for (String text : strings) {
				
				Pattern dayPattern = Pattern.compile("<text id=\"text_(.*?)\" font-weight");
				Matcher dayMatcher = dayPattern.matcher(text);
				
				if(dayMatcher.find()){
					//System.out.println("ZONE :" + dayMatcher.group(1));
					String zone = dayMatcher.group(1);
					allZonesMap.put(zone.toUpperCase(), true);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return allZonesMap;
		 
	 }
public void downloadSvgZonesWithNoListingsInRTF(HttpServletRequest request,HttpServletResponse response) throws Exception { 
		
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		ServletOutputStream out = response.getOutputStream();
		
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-disposition",
			"attachment; filename=SVG_zones_with_no_Listings_in_RTF." + today
					+ ".xlsx");
		
		try{
			 
			 List<Object[]> list = DAORegistry.getQueryManagerDAO().getAllVenueMapZonesWihoutListingsinRTF();
			 
			 SXSSFWorkbook workbook = new SXSSFWorkbook();
			 Sheet ssSheet = workbook.createSheet();
			 
			 int j = 0;
				Row rowhead =   ssSheet.createRow((int)0);
			    rowhead.createCell((int) j).setCellValue("Event Id");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Date");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Event Time");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Artist Name");
			    j++;
			    rowhead.createCell((int) j).setCellValue("Venue");
			    j++;
		    	rowhead.createCell((int) j).setCellValue("City");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("State");
		 	    j++;
		 	    rowhead.createCell((int) j).setCellValue("Country");
		    	j++;
		    	rowhead.createCell((int) j).setCellValue("Zone");
		    	j++;
			    
			    
			    if(list != null) {
			    	 int rowvariable = 1;
					 for (Object[] objArr : list) {
						 
							 
						 int column = 0;
						Row row = ssSheet.createRow(rowvariable);
						if(objArr[0]!= null) {//Event Id
							row.createCell((int) column).setCellValue(objArr[0].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[1]!= null) {//Event Name
							row.createCell((int) column).setCellValue(objArr[1].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[2]!= null) {//Event Date
							row.createCell((int) column).setCellValue(objArr[2].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[3]!= null) {//Event Time
							row.createCell((int) column).setCellValue(objArr[3].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[4]!= null) {//Artist
							row.createCell((int) column).setCellValue(objArr[4].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[5]!= null) {//venue
							row.createCell((int) column).setCellValue(objArr[5].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[6]!= null) {//City
							row.createCell((int) column).setCellValue(objArr[6].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[7]!= null) {//state
							row.createCell((int) column).setCellValue(objArr[7].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[8]!= null) {//Country
							row.createCell((int) column).setCellValue(objArr[8].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						if(objArr[9]!= null) {//zone
							row.createCell((int) column).setCellValue(objArr[9].toString());
					        column++;
						} else {
							row.createCell((int) column).setCellValue("");
					        column++;
						}
						
						
				        rowvariable++;
					}
			    }
			    
			workbook.write(out);
		 }catch (Exception e){
			 e.printStackTrace();
		 }
	}

public ModelAndView vividSeatTicketInventory(HttpServletRequest request,
		HttpServletResponse response, ModelMap map) throws JSONException {
	ModelAndView mav = new ModelAndView("page-tmat-vivid-ticket-inventory-report");
	String action = request.getParameter("action");
	String optionType = request.getParameter("optionType");
	
	Date date = new Date();
	Calendar toCal = new GregorianCalendar();
	toCal.add(Calendar.DAY_OF_MONTH, 365);
	Date endDate = toCal.getTime();
	
	mav.addObject("fromDate", date.getDate());
	mav.addObject("fromMonth", date.getMonth());
	mav.addObject("fromYear", "2019");
	mav.addObject("toDate", endDate.getDate());
	mav.addObject("toMonth", endDate.getMonth());
	mav.addObject("toYear", endDate.getYear()+1900);
	
	Property property = DAORegistry.getPropertyDAO().get("vivid.ticket.job.last.runtime");
	
	mav.addObject("isRunningNow", isVividRunningNow);
	mav.addObject("lastReportGeneratedAt", property.getValue());

	mav.addObject("optionType", optionType);
	mav.addObject("showEventGrid", false);
	if(null != action && !action.isEmpty() && action.equals("search")) {
		
		String artistName = request.getParameter("artistName");
		String venueName = request.getParameter("venueName");
		String eventName = request.getParameter("eventName");
		String artist = request.getParameter("artist");
		String venue = request.getParameter("venue");
		
		String dateFilter = request.getParameter("dateFilter");
		
		mav.addObject("artistName", artistName);
		mav.addObject("venueName", venueName);
		mav.addObject("selectedArtist", artistName);
		mav.addObject("selectedVenues", venueName);
		mav.addObject("eventName", eventName);
		mav.addObject("artist", artist);
		mav.addObject("venue", venue);
		mav.addObject("dateFilter", dateFilter);
		
		String artistIds = "",venueIds = "";
		if(null != eventName && !eventName.isEmpty() && eventName.length() > 0) {
			
		}else {
			if(null != artist && !artist.isEmpty()) {
				String artistArray[] = artist.split(",");
				int i =0;
				for (String artistId : artistArray) {
					if(artistId.isEmpty() || artistId.equals("")) {
						continue;
					}
					try {
						Integer.parseInt(artistId);
						if(i == 0) {
							artistIds = artistId;
						}else {
							artistIds = artistIds +", "+artistId;
						}
						i++;
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			if(null != venue && !venue.isEmpty()) {
				String venueArray[] = venue.split(",");
				int i =0;
				for (String venueId : venueArray) {
					if(venueId.isEmpty() || venueId.equals("")) {
						continue;
					}
					try {
						Integer.parseInt(venueId);
						if(i == 0) {
							venueIds = venueId;
						}else {
							venueIds = venueIds +", "+venueId;
						}
						i++;
					}catch(Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		}
		
		
		
		String fromDay = request.getParameter("fromDay");
		String fromMonth = request.getParameter("fromMonth");
		String fromYear = request.getParameter("fromYear");
		
		String toDay = request.getParameter("toDay");
		String toMonth = request.getParameter("toMonth");
		String toYear = request.getParameter("toYear");
		
		String fromDate = "";
		String toDate = "";
		if(dateFilter != null && dateFilter.equals("YES")) {
			fromDate = fromMonth+"/"+fromDay+"/"+fromYear;
			toDate = toMonth+"/"+toDay+"/"+toYear;
			
			mav.addObject("fromDate", fromDay);
			mav.addObject("fromMonth", fromMonth);
			mav.addObject("fromYear", fromYear);
			mav.addObject("toDate", toDay);
			mav.addObject("toMonth", toMonth);
			mav.addObject("toYear", toYear);
			
			System.out.println("From Date is "+fromDate);
			System.out.println("To Date is "+toDate);
		}
		 
		List<VividSeatsTicketInventory> eventList = DAORegistry.getQueryManagerDAO().AllEventsCreatedinLast7Days(eventName, artistIds, venueIds, fromDate, toDate);
		
		if(null != eventList && !eventList.isEmpty() && eventList.size() > 0) {
			try {
				eventList.sort( new Comparator<VividSeatsTicketInventory>() {			
					public int compare(VividSeatsTicketInventory ticket1, VividSeatsTicketInventory ticket2) {
						int cmp = ticket1.getEventDateTime().compareTo(
								ticket2.getEventDateTime());
						if (cmp < 0) {
							return -1;
						}
						if (cmp > 0) {
							return 1;
						}
						return ticket1.getEventName().compareTo(ticket2.getEventName());
					}
				});
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		mav.addObject("eventList", eventList);
		mav.addObject("showEventGrid", true);
	}
	
	return mav;
}


public void downloadVividSeatsTicketInventoryReport(HttpServletRequest request,
		HttpServletResponse response) throws IOException{
	 
		String eventName = request.getParameter("eventName");
		String artistName = request.getParameter("artistName");
		String venueName = request.getParameter("venueName");
		String optionType = request.getParameter("optionType");
		//String eventName = request.getParameter("eventName");
		String eventCreatedDaysStr = "5";//request.getParameter("eventCreatedDays");
		
		Integer eventCreatedDays = 5;
		try {
			eventCreatedDays = Integer.parseInt(eventCreatedDaysStr.trim());
		}catch(Exception e) {
			eventCreatedDays = 5;
			//e.printStackTrace();
		}
		
		Set<String> params = request.getParameterMap().keySet();
		List<Integer> eventId_arry = new ArrayList<Integer>();
		List<Integer> eventId_list = new ArrayList<Integer>();

		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		Date now = new Date();
		 
		for (String param : params) {
			if (param.contains("checkbox_")) {
				String temp[] = param.split("_");
				eventId_arry.add(Integer.parseInt(temp[1]));
				eventId_list.add(Integer.parseInt(temp[1]));
			}
		}
		String eventIds = "";
		if(null != eventId_list && !eventId_list.isEmpty() && eventId_list.size() > 0 ) {
			eventIds = eventId_list.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		}
	
		List<Object[]> eventList =  DAORegistry.getQueryManagerDAO().AllEventsCreatedinLast7Days(eventIds, eventCreatedDays);
		
		String crawlIds = "";
		
		Integer eventId = null, crawlId = null, i =0, j = 0, maxBreak = 1000, size = eventList.size();
		
		Map<Integer, Boolean> eventMap = new HashMap<Integer, Boolean>();
		
		for (Object[] object : eventList) {
			
			try {
				i++;
				j++;
				eventId = (Integer)object[0];
				crawlIds = crawlIds + ","+object[1].toString();
				
				eventMap.put(eventId, Boolean.TRUE);
				
				if(i % maxBreak == 0) {
					String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=forceRecrawl&crawls=" + crawlIds;
					sendRequest(url);
					crawlIds = "";
					i =0;
					try {
						Thread.sleep(1000);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				//System.out.println("VividSeats Inventory: Event Size: "+size+", Current Processing Event Size :"+j);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if(null != crawlIds && !crawlIds.isEmpty() && crawlIds.length() > 0) {
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=forceRecrawl&crawls=" + crawlIds;
			sendRequest(url);
			crawlIds = "";
		}
		System.out.println("VividSeats Inventory: Thread Sleep Started at "+ new Date());
		boolean ignoreActionColunm = false;
		if(optionType.equals("ALL")) {
			try {
				Thread.sleep(300000);
			}catch(Exception e) {
				e.printStackTrace();
			}
				
		}else {
			try {
				Thread.sleep(60000);
			}catch(Exception e) {
				e.printStackTrace();
			}
			ignoreActionColunm = true;
		}
		
		Date today= new Date();
		
		System.out.println("VividSeats Inventory: Thread Sleep Revoked at "+ new Date());
		List<VividSeatsTicketInventory> toBeSendTicketList = new ArrayList<VividSeatsTicketInventory>();
		if(null != eventIds && !eventIds.isEmpty()) {
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getVividSeatsLatestInventory(eventIds, eventCreatedDays);
			
			VividSeatsTicketInventory tixObj = null;
			
			for (Object[] object : list) {
				try {
				tixObj = new VividSeatsTicketInventory();
				tixObj.setEventId((Integer)object[0]);
				tixObj.setVendorID(Integer.parseInt(object[1].toString()));
				tixObj.setEventName(object[2].toString());
				tixObj.setVenueName(object[3].toString());
				tixObj.setEventDate(object[4].toString());
				tixObj.setEventTime(object[5].toString());
				tixObj.setQuantity((Integer)object[6]);
				tixObj.setSection(object[7].toString());
				tixObj.setRow(object[8].toString());
				tixObj.setLowSeat(object[9].toString());
				tixObj.setPublicNotes(object[10].toString());
				tixObj.setInternalNotes(object[11].toString());
				tixObj.setGroupCost(Double.valueOf(object[12].toString()));
				tixObj.setUnitListPrice(Double.valueOf(object[13].toString()));
				tixObj.setExpectedValue(object[14].toString());;
				tixObj.setStockType(object[15].toString());
				tixObj.setElectronicTransfer(object[16].toString());
				tixObj.setInHandDate(object[17].toString());
				tixObj.setBroadcast(object[18].toString());
				tixObj.setHideSeatNumbers(object[19].toString());
				tixObj.setSplitType(object[20].toString());
				tixObj.setSplitValue(object[21].toString());
				tixObj.setSeatType(object[22].toString());
				tixObj.setZoneListing(object[23].toString());
				tixObj.setTags(object[24].toString());
				tixObj.setCreatedDate(new Date());
				tixObj.setLastUpdated(new Date());
				tixObj.setStatus("ACTIVE");
				tixObj.setAction("ADD");
				toBeSendTicketList.add(tixObj);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			if(null != toBeSendTicketList && !toBeSendTicketList.isEmpty() && toBeSendTicketList.size() > 0) {
				try {
					toBeSendTicketList.sort( new Comparator<VividSeatsTicketInventory>() {			
						public int compare(VividSeatsTicketInventory ticket1, VividSeatsTicketInventory ticket2) {
							int cmp = ticket1.getEventDateTime().compareTo(
									ticket2.getEventDateTime());
							if (cmp < 0) {
								return -1;
							}
							if (cmp > 0) {
								return 1;
							}
							// if same price get the one with less quantity first
							int cmp2 = ticket1.getSection().compareTo(
									ticket2.getSection());
							if (cmp2 < 0) {
								return -1;
							}
			
							if (cmp2 > 0) {
								return 1;
							}
							return ticket1.getRow().compareTo(ticket2.getRow());
						}
					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			String fileName = "VividSeatsTicketList";
			if(null != artistName && !artistName.isEmpty()) {
				fileName = artistName.replaceAll(" ", "_");
			}else if(null != venueName && !venueName.isEmpty()) {
				fileName = venueName.replaceAll(" ", "_");
			}else if(null != eventName && !eventName.isEmpty()) {
				fileName = eventName.replaceAll(" ", "_");
			}
			fileName= fileName.replaceAll("[^a-zA-Z0-9]", "_");
			
			ServletOutputStream out = response.getOutputStream();
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename=" + fileName + ".xlsx");
			try {
				getVividListingsReports(workbook, "TICKETS", toBeSendTicketList, ignoreActionColunm);
				//getEventCrawlsSalesListOld(workbook, "event_crawl_list6", "Event_Crawls_Sales_Report_1");
				//getEventCrawlsSalesListOld(workbook, "event_crawl_list7", "Event_Crawls_Sales_Report_2");
				workbook.write(response.getOutputStream());
				System.out.println("downloadVividSeatsTicketList...completed........"+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			/*response.setContentType("text/csv");
	        response.setHeader("Content-Disposition", "attachment; fileName=VividSeatsTicketList.csv");
			ServletOutputStream out = response.getOutputStream();
			StringBuffer sb = getVividReport(toBeSendTicketList);
			out.write(sb.toString().getBytes());
			out.flush();
			out.close(); */
			
			return;
			
		}
		
		if(isVividRunningNow) {
			return;
		}
		
		
		isVividRunningNow = true;
		System.out.println("VividSeats Inventory: Existing Inventory Loading Started at "+ new Date());
		Collection<VividSeatsTicketInventory> existingList =DAORegistry.getVividSeatsTicketInventoryDAO().getAllTicketInventory(null);
		
		Map<String, VividSeatsTicketInventory> ticketMap = new HashMap<String, VividSeatsTicketInventory>();
		String key = "";
		for (VividSeatsTicketInventory tixObj : existingList) {
			/*if(null == eventMap.get(tixObj.getEventId())) {
				continue;
			}*/
			key = tixObj.getEventId()+"-"+tixObj.getQuantity()+"-"+tixObj.getSection().replace(" ", "").toLowerCase()+"-"+tixObj.getRow().replace(" ", "").toLowerCase();
			ticketMap.put(key, tixObj);
		}
		
		System.out.println("VividSeats Inventory: Existing Inventory Loading Completed at "+ new Date());
	
		List<Object[]> list = DAORegistry.getQueryManagerDAO().getVividSeatsLatestInventory(null, eventCreatedDays);
		List<VividSeatsTicketInventory> ticketList = new ArrayList<VividSeatsTicketInventory>();
		List<VividSeatsTicketInventory> pastEventList = new ArrayList<VividSeatsTicketInventory>();
		
		for (Object[] object : list) {
			try {
			
			key = object[0].toString()+"-"+object[6].toString()+"-"+object[7].toString().replace(" ", "").toLowerCase()+"-"+object[8].toString().replace(" ", "").toLowerCase();
			
			VividSeatsTicketInventory tixObj = ticketMap.remove(key);
			
			if(null == tixObj) {
				
				if(null == eventMap.get((Integer)object[0])) {
					continue;
				}
				
				
				tixObj = new VividSeatsTicketInventory();
				tixObj.setEventId((Integer)object[0]);
				tixObj.setVendorID(Integer.parseInt(object[1].toString()));
				tixObj.setEventName(object[2].toString());
				tixObj.setVenueName(object[3].toString());
				tixObj.setEventDate(object[4].toString());
				tixObj.setEventTime(object[5].toString());
				tixObj.setQuantity((Integer)object[6]);
				tixObj.setSection(object[7].toString());
				tixObj.setRow(object[8].toString());
				tixObj.setLowSeat(object[9].toString());
				tixObj.setPublicNotes(object[10].toString());
				tixObj.setInternalNotes(object[11].toString());
				tixObj.setGroupCost(Double.valueOf(object[12].toString()));
				tixObj.setUnitListPrice(Double.valueOf(object[13].toString()));
				tixObj.setExpectedValue(object[14].toString());;
				tixObj.setStockType(object[15].toString());
				tixObj.setElectronicTransfer(object[16].toString());
				tixObj.setInHandDate(object[17].toString());
				tixObj.setBroadcast(object[18].toString());
				tixObj.setHideSeatNumbers(object[19].toString());
				tixObj.setSplitType(object[20].toString());
				tixObj.setSplitValue(object[21].toString());
				tixObj.setSeatType(object[22].toString());
				tixObj.setZoneListing(object[23].toString());
				tixObj.setTags(object[24].toString());
				tixObj.setCreatedDate(new Date());
				tixObj.setLastUpdated(new Date());
				tixObj.setStatus("ACTIVE");
				tixObj.setAction("ADD");
				ticketList.add(tixObj);
			}else {
				
				if(null == eventMap.get((Integer)object[0])) {
					pastEventList.add(tixObj);
					continue;
				}else {
					Double latestPrice = Double.valueOf(object[13].toString());
					if(!tixObj.getUnitListPrice().equals(latestPrice)) {
						tixObj.setAction("UPDATE");
						tixObj.setStatus("ACTIVE");
						tixObj.setUnitListPrice(Double.valueOf(object[13].toString()));
						tixObj.setLastUpdated(new Date());
						ticketList.add(tixObj);
					}else {
						tixObj.setAction("NOACTION");
						tixObj.setStatus("ACTIVE");
						tixObj.setUnitListPrice(Double.valueOf(object[13].toString()));
						tixObj.setLastUpdated(new Date());
					}
				}
			}
			toBeSendTicketList.add(tixObj);
			
			if(null != ticketList && !ticketList.isEmpty() && ticketList.size() == maxBreak) {
				DAORegistry.getVividSeatsTicketInventoryDAO().saveOrUpdateAll(ticketList);
				ticketList = new ArrayList<VividSeatsTicketInventory>();
			}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(null != ticketList && !ticketList.isEmpty() && ticketList.size() > 0) {
			DAORegistry.getVividSeatsTicketInventoryDAO().saveOrUpdateAll(ticketList);
		}
		
		if(null != ticketMap && ticketMap.size() > 0) {
			
			ticketList.clear();
			ticketList = new ArrayList<VividSeatsTicketInventory>();
			
			for (VividSeatsTicketInventory tixObj : ticketMap.values()) {
				
				if(tixObj.getStatus().equals("ACTIVE")) {
					tixObj.setLastUpdated(new Date());
					tixObj.setStatus("DELETED");
					tixObj.setAction("DELETE");
					ticketList.add(tixObj);
				}
				if(null == eventMap.get(tixObj.getEventId())) {
					pastEventList.add(tixObj);
					continue;
				}
				toBeSendTicketList.add(tixObj);
			}
			
			if(null != ticketList && !ticketList.isEmpty() && ticketList.size() > 0) {
				DAORegistry.getVividSeatsTicketInventoryDAO().updateAll(ticketList);
			}
		}
		
		isVividRunningNow = false;
		
		Property property = DAORegistry.getPropertyDAO().get("vivid.ticket.job.last.runtime");
		property.setValue(dateTimeFormat.format(new Date()));
		DAORegistry.getPropertyDAO().update(property);
		
		if(null != toBeSendTicketList && !toBeSendTicketList.isEmpty() && toBeSendTicketList.size() > 0) {
			try {
				toBeSendTicketList.sort( new Comparator<VividSeatsTicketInventory>() {			
					public int compare(VividSeatsTicketInventory ticket1, VividSeatsTicketInventory ticket2) {
						int cmp = ticket1.getEventDateTime().compareTo(
								ticket2.getEventDateTime());
						if (cmp < 0) {
							return -1;
						}
						if (cmp > 0) {
							return 1;
						}
						// if same price get the one with less quantity first
						int cmp2 = ticket1.getSection().compareTo(
								ticket2.getSection());
						if (cmp2 < 0) {
							return -1;
						}
		
						if (cmp2 > 0) {
							return 1;
						}
						return ticket1.getRow().compareTo(ticket2.getRow());
					}
				});
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(null != pastEventList && !pastEventList.isEmpty() && pastEventList.size() > 0) {
			try {
				pastEventList.sort( new Comparator<VividSeatsTicketInventory>() {			
					public int compare(VividSeatsTicketInventory ticket1, VividSeatsTicketInventory ticket2) {
						int cmp = ticket1.getEventDateTime().compareTo(
								ticket2.getEventDateTime());
						if (cmp < 0) {
							return -1;
						}
						if (cmp > 0) {
							return 1;
						} 
						return ticket1.getEventId().compareTo(ticket2.getEventId());
					}
				});
			}catch(Exception e) {
				e.printStackTrace();
			}
		} 
		
		ServletOutputStream out = response.getOutputStream();
		SXSSFWorkbook workbook = new SXSSFWorkbook();
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=VividSeatsTicketList." + today
						+ ".xlsx");
		try {
			getVividListingsReports(workbook, "TICKETS", toBeSendTicketList,ignoreActionColunm);
			try {
				getPastEventName(workbook, "TO_BE_REMOVED_EVENTS", pastEventList);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			workbook.write(response.getOutputStream());
			System.out.println("downloadVividSeatsTicketList...completed........"+new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
	}

	public static StringBuffer getVividReport(List<VividSeatsTicketInventory> toBeSendTicketList) {
		StringBuffer sb = new StringBuffer();
		sb.append("VendorID,Event,Venue,EventDate,EventTime,Quantity,Section,Row,LowSeat,PublicNotes,InternalNotes,GroupCost,UnitListPrice,"
				+ "ExpectedValue,StockType,ElectronicTransfer,InHandDate,Broadcast?,HideSeatNumbers,SplitType,SplitValue,SeatType,ZoneListing,Tags,Action \n");
	
		if(toBeSendTicketList !=null && !toBeSendTicketList.isEmpty()){
			for(VividSeatsTicketInventory event : toBeSendTicketList){
				 sb.append(event.getVendorID().toString()+","+event.getEventName().toString().replaceAll(",", "-") + "," + event.getVenueName().toString().replaceAll(",", "-")+
						"," + event.getEventDate().toString().replaceAll(",", "-") + "," +
						event.getEventTime().toString().replaceAll(",", "-") +","+ event.getQuantity().toString().replaceAll(",", "-") +","+ event.getSection().toString().replaceAll(",", "-") +","+ 
						event.getRow().toString().replaceAll(",", "-")+","+event.getLowSeat().toString().replaceAll(",", "-")+","+event.getPublicNotes().toString().replaceAll(",", "-")+","+event.getInternalNotes().toString().replaceAll(",", "-")+
						","+event.getGroupCost().toString().replaceAll(",", "-")+","+event.getUnitListPrice().toString().replaceAll(",", "-")+","+event.getExpectedValue().toString().replaceAll(",", "-")
						+","+event.getStockType().toString().replaceAll(",", "-")+","+event.getElectronicTransfer().toString().replaceAll(",", "-")+","+event.getInHandDate().toString().replaceAll(",", "-")
						+","+event.getBroadcast().toString().replaceAll(",", "-")+","+event.getHideSeatNumbers().toString().replaceAll(",", "-")+","+event.getSplitType().toString().replaceAll(",", "-")
						+","+event.getSplitValue().toString().replaceAll(",", "-")+","+event.getSeatType().toString().replaceAll(",", "-")
						+","+event.getZoneListing().toString().replaceAll(",", "-")
						+","+event.getTags().toString().replaceAll(",", "-")+","+event.getAction().toString().replaceAll(",", "-")+"\n");
			}
		}
		return sb;
	}
	
	
	public void downloadVividSeatsTicketInventoryReportTest(HttpServletRequest request,
			HttpServletResponse response) throws IOException{
		 
			System.out.println("VividSeats Inventory: Existing Inventory Loading Started at "+ new Date()); 
			
			List<VividSeatsTicketInventory> vividList = DAORegistry.getVividSeatsTicketInventoryDAO().getAllTicketInventory(null);
			
			try {
				vividList.sort( new Comparator<VividSeatsTicketInventory>() {			
					public int compare(VividSeatsTicketInventory ticket1, VividSeatsTicketInventory ticket2) {
						int cmp = ticket1.getEventDateTime().compareTo(
								ticket2.getEventDateTime());
						if (cmp < 0) {
							return -1;
						}
						if (cmp > 0) {
							return 1;
						}
						// if same price get the one with less quantity first
						int cmp2 = ticket1.getSection().compareTo(
								ticket2.getSection());
						if (cmp2 < 0) {
							return -1;
						}
		
						if (cmp2 > 0) {
							return 1;
						}
						return ticket1.getRow().compareTo(ticket2.getRow());
					}
				});
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			response.setContentType("text/csv");
	        response.setHeader("Content-Disposition", "attachment; fileName=VividSeatsTicketList.csv");
			ServletOutputStream out = response.getOutputStream();

			StringBuffer sb = getVividReport(vividList);
			
			out.write(sb.toString().getBytes());
			out.flush();
			out.close(); 
		}

	public void sendRequest(String url){
		try {
			
			SimpleHttpClient client = HttpClientStore.createHttpClient();
			HttpGet get = new HttpGet(url);
			System.out.println("TMAT CRL 2 EXE: "+client.executeWithOneHit(get,true));
			
		} catch (Exception e) {
			System.out.println("TMAT CRL 2 ERR: "+url);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	
	 public static SXSSFWorkbook getVividListingsReports(SXSSFWorkbook workbook,String sheetName,List<VividSeatsTicketInventory> list, boolean ignoreActionColunm) throws Exception {
			 
			SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
			
										
					
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0);
			rowhead.createCell((int) j).setCellValue("Vendor ID");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Event");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Venue");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("EventDate");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("EventTime");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Quantity");
	 	    j++;
	    	rowhead.createCell((int) j).setCellValue("Section");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Row");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Low Seat");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Public Notes");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Internal Notes");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Group Cost");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Unit List Price");
	    	j++;
	    	rowhead.createCell((int) j).setCellValue("Expected Value");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("StockType"); 
		    j++;
		    		
		    rowhead.createCell((int) j).setCellValue("Electronic Transfer");
		    j++;
		    rowhead.createCell((int) j).setCellValue("InHandDate");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Broadcast?");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Hide Seat Numbers");
		    j++;
		    rowhead.createCell((int) j).setCellValue("SplitType");
		    j++;
		    rowhead.createCell((int) j).setCellValue("SplitValue");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Seat Type");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Zone Listing");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Tags");
		    j++;
		    if(!ignoreActionColunm) {
		    	rowhead.createCell((int) j).setCellValue("Action");
			    j++;
		    }
		    
		    
		    //CellStyle cellamountStyle = workbook.createCellStyle();
		    
		    CellStyle cellamountStyle = workbook.createCellStyle();
		    cellamountStyle.setDataFormat(
		    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
		    
		    int rowvariable = 1;
		    for (VividSeatsTicketInventory event : list) {
		    	
		    	int column = 0;
				Row row = ssSheet.createRow(rowvariable);
				
				Cell vendorIdCell = row.createCell((int) column,Cell.CELL_TYPE_NUMERIC);
				vendorIdCell.setCellValue(event.getVendorID());
		        column++;
				
		        /*row.createCell((int) column).setCellValue(event.getVendorID().toString());
		        column++;*/
		        
		        row.createCell((int) column).setCellValue(event.getEventName().toString().replaceAll(",", "-"));
		        column++;
		        
	        	row.createCell((int) column).setCellValue(event.getVenueName().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getEventDate().toString().replaceAll(",", "-"));
		    	column++;
		    	
		        row.createCell((int) column).setCellValue(event.getEventTime().toString().replaceAll(",", "-"));
		        column++;
		        
		        Cell quantityCell = row.createCell((int) column,Cell.CELL_TYPE_NUMERIC);
		        quantityCell.setCellValue(event.getQuantity());
		        column++;
		        
		        /*row.createCell((int) column).setCellValue(event.getQuantity().toString().replaceAll(",", "-"));
		        column++;*/
		        
		        row.createCell((int) column).setCellValue(event.getSection().toString().replaceAll(",", "-"));
		        column++;
		        
		       /* Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		        seatCell.setCellValue(event.getSection().toString().replaceAll(",", "-"));
		        column++;*/
		        
		        Cell rowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		        rowCell.setCellValue(event.getRow().toString().replaceAll(",", "-"));
		        column++;
		        
		        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		        seatCell.setCellValue(event.getLowSeat().toString().replaceAll(",", "-"));
		        column++;
		        
		        /*row.createCell((int) column).setCellValue(event.getLowSeat().toString().replaceAll(",", "-"));
		        column++;*/
		        
		        row.createCell((int) column).setCellValue(event.getPublicNotes().toString().replaceAll(",", "-"));
		        column++;
		        
		        row.createCell((int) column).setCellValue(event.getInternalNotes().toString().replaceAll(",", "-"));
		        column++;
		        
		        Cell groupCostCell = row.createCell((int) column,Cell.CELL_TYPE_NUMERIC);
		        groupCostCell.setCellValue(event.getGroupCost());
		        column++;
		         
		        Cell amountCell = row.createCell((int) column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(event.getUnitListPrice());
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		         
		       /* Cell amountCell = row.createCell(column,Cell.);
		        amountCell.setCellValue(event.getGroupCost().toString().replaceAll(",", "-"));
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        
		        amountCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		        amountCell.setCellValue(event.getUnitListPrice().toString().replaceAll(",", "-"));
		        amountCell.setCellStyle(cellamountStyle);
		        column++;*/
		        
		        row.createCell((int) column).setCellValue(event.getExpectedValue().toString().replaceAll(",", "-"));
		        column++;
		        
		        row.createCell((int) column).setCellValue(event.getStockType().toString().replaceAll(",", "-"));
		        column++;
		          
		        row.createCell((int) column).setCellValue(event.getElectronicTransfer().toString().replaceAll(",", "-"));
		        column++;
		        
		        row.createCell((int) column).setCellValue(event.getInHandDate().toString().replaceAll(",", "-"));
		        column++;
		        
		        row.createCell((int) column).setCellValue(event.getBroadcast().toString().replaceAll(",", "-"));
		        column++;
		        
		        row.createCell((int) column).setCellValue(event.getHideSeatNumbers().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getSplitType().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getSplitValue().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getSeatType().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getZoneListing().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getTags().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	if(!ignoreActionColunm) {
		    		row.createCell((int) column).setCellValue(event.getAction().toString().replaceAll(",", "-"));
			    	column++;
		    	}
		    	
		       
		        rowvariable++;
			}
			return workbook;
		
		}
	 
	 
	 public static SXSSFWorkbook getPastEventName(SXSSFWorkbook workbook,String sheetName,List<VividSeatsTicketInventory> pastEventList) throws Exception {
		 
			SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet(sheetName);
			
			int j = 0;
			Row rowhead =   ssSheet.createRow((int)0); 
		    rowhead.createCell((int) j).setCellValue("Event");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Venue");
		    j++;
	    	rowhead.createCell((int) j).setCellValue("Event Date");
	    	j++;
		    rowhead.createCell((int) j).setCellValue("Event Time");
		    j++; 
		    rowhead.createCell((int) j).setCellValue("Action");
		    j++;
		    
		    int rowvariable = 1;
		    
		   Set<Integer> uniqueEventIds = new HashSet<Integer>();
		    
		    for (VividSeatsTicketInventory event : pastEventList) {
		    	
		    	if(uniqueEventIds.add(event.getEventId())) {
		    		//Proceed to add.
		    	}else {
		    		continue;
		    	}
		    	
		    	int column = 0;
				Row row = ssSheet.createRow(rowvariable);
		        
		        row.createCell((int) column).setCellValue(event.getEventName().toString().replaceAll(",", "-"));
		        column++;
		        
	        	row.createCell((int) column).setCellValue(event.getVenueName().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	row.createCell((int) column).setCellValue(event.getEventDate().toString().replaceAll(",", "-"));
		    	column++;
		    	
		    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
		    	shortSectionCell.setCellValue(event.getEventTime().toString().replaceAll(",", "-"));
		        column++;
		        
		    	row.createCell((int) column).setCellValue("REMOVE_LISTINGS");
		    	column++;
		       
		        rowvariable++;
			}
			return workbook;
		
		}
	 
	 public static void main(String[] args) {
		 String artistName = "Kinky Boots";
		 String fileName = artistName.replaceAll(" ", "_");
		 System.out.println(fileName);
	}
}
