package com.admitone.tmat.web;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.utils.TicketListingCrawlerChecker;

/**
 * Class which handles the EditCrawlerProperties Form.
 */
public class AdminEditCrawlerPropertiesFormController extends SimpleFormController {
//	private TicketListingCrawler ticketListingCrawler;
//	private CrawlerSchedulerManager crawlerSchedulerManager;
	private TicketListingCrawlerChecker ticketListingCrawlerChecker;
	private Pattern schedulerPropertyPattern = Pattern.compile("^((\\d+\\s*[mhdy]\\s*=\\s*\\d+\\s*[mhdy]\\s*)(,\\s*\\d+\\s*[mhdy]\\s*=\\s*\\d+\\s*[mhdy]\\s*)*)?$");

	protected  Object formBackingObject(HttpServletRequest request) {
		CrawlerPropertiesCommand crawlerPropertiesCommand = new CrawlerPropertiesCommand();
		crawlerPropertiesCommand.setCrawlerStartupEnabled(DAORegistry.getPropertyDAO().get("crawler.startup.enabled").getValue().equals("1"));
		crawlerPropertiesCommand.setCrawlerThreadCount(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.thread.count").getValue()));

		crawlerPropertiesCommand.setCrawlerEmailTo(DAORegistry.getPropertyDAO().get("crawler.email.to").getValue());

		crawlerPropertiesCommand.setCrawlerEmailCreatorEnabled(DAORegistry.getPropertyDAO().get("crawler.email.creator.enabled").getValue().equals("1"));
		crawlerPropertiesCommand.setCrawlerEmailLastUpdaterEnabled(DAORegistry.getPropertyDAO().get("crawler.email.lastUpdater.enabled").getValue().equals("1"));

		crawlerPropertiesCommand.setAdmit1NotePattern(DAORegistry.getPropertyDAO().get("admit1.note.pattern").getValue());
		
		crawlerPropertiesCommand.setCrawlerEmailErrorEnabled(DAORegistry.getPropertyDAO().get("crawler.email.error.enabled").getValue().equals("1"));
		crawlerPropertiesCommand.setCrawlerEmailStartStopEnabled(DAORegistry.getPropertyDAO().get("crawler.email.startStop.enabled").getValue().equals("1"));

		crawlerPropertiesCommand.setCrawlerTicketTimeToLive(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.ticket.ttl").getValue()));

		crawlerPropertiesCommand.setCrawlMaxDuration(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.crawlMaxDuration").getValue()) / 60);
		crawlerPropertiesCommand.setCrawlerSchedulerMode(DAORegistry.getPropertyDAO().get("crawler.scheduler.mode").getValue());
		
		
		//crawlerPropertiesCommand.setCrawlerSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.scheduler.properties").getValue());
		crawlerPropertiesCommand.setCrawlerConcertsSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.concerts.scheduler.properties").getValue());
		crawlerPropertiesCommand.setCrawlerTheaterSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.theater.scheduler.properties").getValue());
		crawlerPropertiesCommand.setCrawlerSportsSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.sports.scheduler.properties").getValue());
		crawlerPropertiesCommand.setCrawlerDefaultSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.default.scheduler.properties").getValue());
		crawlerPropertiesCommand.setCrawlerLasVegasSchedulerPropertiesString(DAORegistry.getPropertyDAO().get("crawler.lasvegas.scheduler.properties").getValue());
		
		
		
		crawlerPropertiesCommand.setCrawlerSchedulerStartHour(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.startHour").getValue()));
		crawlerPropertiesCommand.setCrawlerSchedulerStopHour(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.stopHour").getValue()));

		crawlerPropertiesCommand.setCrawlerIdlePeriodEnabled(Boolean.parseBoolean(DAORegistry.getPropertyDAO().get("crawler.idle.enabled").getValue()));
		crawlerPropertiesCommand.setCrawlerIdlePeriodStartHour(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.idle.startHour").getValue()));
		crawlerPropertiesCommand.setCrawlerIdlePeriodStopHour(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.idle.stopHour").getValue()));

		crawlerPropertiesCommand.setCrawlerSortingTimeSlice(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.sortingTimeSlice").getValue()));
		crawlerPropertiesCommand.setCrawlerProactiveFactor(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.proActiveFactor").getValue()));

		crawlerPropertiesCommand.setCrawlerMinCrawlPeriod(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.minPeriod").getValue()) / 60);

		crawlerPropertiesCommand.setReadyCrawlCountThreshold(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.readyCrawlCountThreshold").getValue()));
		crawlerPropertiesCommand.setCrawlDurationThreshold(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.crawlDurationThreshold").getValue()));
		
		crawlerPropertiesCommand.setNoTicketsFoundError("1".equals(DAORegistry.getPropertyDAO().get("crawler.error.noTicketsFoundError").getValue()));
		crawlerPropertiesCommand.setCheckFrequency(Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.checkFrequency").getValue()));
		
		return crawlerPropertiesCommand;
	}

	protected ModelAndView onSubmit(Object command) {
		CrawlerPropertiesCommand crawlerPropertiesCommand = (CrawlerPropertiesCommand)command;

		// validation (might do it in a validator class if needs be)
		Matcher matcher = schedulerPropertyPattern.matcher(crawlerPropertiesCommand.getCrawlerSchedulerPropertiesString());
		if (!matcher.find()) {
			return new ModelAndView(new RedirectView("AdminEditCrawlerProperties?error=The Schedule properties is of incorrect format"));
		}

		Property crawlerStartupEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.startup.enabled");
		crawlerStartupEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerStartupEnabled()?"1":"0");
		DAORegistry.getPropertyDAO().update(crawlerStartupEnabledProperty);
		
		Property crawlerThreadCountProperty = DAORegistry.getPropertyDAO().get("crawler.thread.count");
		int oldCrawlerThreadCount = Integer.parseInt(crawlerThreadCountProperty.getValue());
		
		if (oldCrawlerThreadCount != crawlerPropertiesCommand.getCrawlerThreadCount()) {
			crawlerThreadCountProperty.setValue(Integer.toString(crawlerPropertiesCommand.getCrawlerThreadCount()));
			DAORegistry.getPropertyDAO().update(crawlerThreadCountProperty);
			
			// if the crawler is running, restart it to take into account the new thread count parameter
			// TODOJMS
			/*if (ticketListingCrawler.isRunning()) {			
				ticketListingCrawler.stop();
				ticketListingCrawler.start();
			}*/
		}

		Property crawlerEmailToProperty = DAORegistry.getPropertyDAO().get("crawler.email.to");
		crawlerEmailToProperty.setValue(crawlerPropertiesCommand.getCrawlerEmailTo());
		DAORegistry.getPropertyDAO().update(crawlerEmailToProperty);

		Property crawlerEmailErrorEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.email.error.enabled");
		crawlerEmailErrorEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerEmailErrorEnabled()?"1":"0");
		DAORegistry.getPropertyDAO().update(crawlerEmailErrorEnabledProperty);

		Property crawlerEmailStartStopEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.email.startStop.enabled");
		crawlerEmailStartStopEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerEmailStartStopEnabled()?"1":"0");
		DAORegistry.getPropertyDAO().update(crawlerEmailStartStopEnabledProperty);

		Property crawlerEmailCreatorEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.email.creator.enabled");
		crawlerEmailCreatorEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerEmailCreatorEnabled()?"1":"0");
		DAORegistry.getPropertyDAO().update(crawlerEmailCreatorEnabledProperty);

		Property crawlerEmailLastUpdaterEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.email.lastUpdater.enabled");
		crawlerEmailLastUpdaterEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerEmailLastUpdaterEnabled()?"1":"0");
		DAORegistry.getPropertyDAO().update(crawlerEmailLastUpdaterEnabledProperty);
		
		Property admit1NotePatternProperty = DAORegistry.getPropertyDAO().get("admit1.note.pattern");
		admit1NotePatternProperty.setValue(crawlerPropertiesCommand.getAdmit1NotePattern());
		DAORegistry.getPropertyDAO().update(admit1NotePatternProperty);
		Constants.getInstance().setAdmit1NotePattern(crawlerPropertiesCommand.getAdmit1NotePattern());

		Property crawlerTicketTimeToLiveProperty = DAORegistry.getPropertyDAO().get("crawler.ticket.ttl");
		crawlerTicketTimeToLiveProperty.setValue(Integer.toString(crawlerPropertiesCommand.getCrawlerTicketTimeToLive()));
		DAORegistry.getPropertyDAO().update(crawlerTicketTimeToLiveProperty);

		Property readyCrawlCountThresholdProperty = DAORegistry.getPropertyDAO().get("crawler.error.readyCrawlCountThreshold");
		readyCrawlCountThresholdProperty.setValue(Integer.toString(crawlerPropertiesCommand.getReadyCrawlCountThreshold()));
		DAORegistry.getPropertyDAO().update(readyCrawlCountThresholdProperty);

		Property crawlDurationThresholdProperty = DAORegistry.getPropertyDAO().get("crawler.error.crawlDurationThreshold");
		crawlDurationThresholdProperty.setValue(Integer.toString(crawlerPropertiesCommand.getCrawlDurationThreshold()));
		DAORegistry.getPropertyDAO().update(crawlDurationThresholdProperty);

		Property noTicketsFoundErrorProperty = DAORegistry.getPropertyDAO().get("crawler.error.noTicketsFoundError");
		noTicketsFoundErrorProperty.setValue(crawlerPropertiesCommand.getNoTicketsFoundError()?"1":"0");
		DAORegistry.getPropertyDAO().update(noTicketsFoundErrorProperty);

		Property checkFrequencyProperty = DAORegistry.getPropertyDAO().get("crawler.error.checkFrequency");
		if (!crawlerPropertiesCommand.getCheckFrequency().equals(Integer.parseInt(checkFrequencyProperty.getValue()))) {
			checkFrequencyProperty.setValue(Integer.toString(crawlerPropertiesCommand.getCheckFrequency()));			
			DAORegistry.getPropertyDAO().update(checkFrequencyProperty);
		//	ticketListingCrawlerChecker.reset();
			TicketListingCrawlerChecker.getInstance().reset();
		}


		Property crawlMaxDurationProperty = DAORegistry.getPropertyDAO().get("crawler.crawlMaxDuration");
		crawlMaxDurationProperty.setValue((crawlerPropertiesCommand.getCrawlMaxDuration() * 60) + "");
		DAORegistry.getPropertyDAO().update(crawlMaxDurationProperty);

		// updating crawler schedule
		Property crawlerSchedulerModeProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.mode");
		Property crawlerSchedulerStartProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.startHour");
		Property crawlerSchedulerStopProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.stopHour");
		Property crawlerIdlePeriodEnabledProperty = DAORegistry.getPropertyDAO().get("crawler.idle.enabled");
		Property crawlerIdlePeriodStartProperty = DAORegistry.getPropertyDAO().get("crawler.idle.startHour");
		Property crawlerIdlePeriodStopProperty = DAORegistry.getPropertyDAO().get("crawler.idle.stopHour");
		Property crawlerSortingTimeSliceProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.sortingTimeSlice");
		Property crawlerProActiveFactorProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.proActiveFactor");
		Property crawlerMinCrawlPeriodProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.minPeriod");
		
		
		
		//Property crawlerSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.properties");
		Property crawlerSportsSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.sports.scheduler.properties");
		Property crawlerConcertsSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.concerts.scheduler.properties");
		Property crawlerTheaterSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.theater.scheduler.properties");
		Property crawlerDefaultSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.default.scheduler.properties");
		Property crawlerLasVegasSchedulerPropertiesStringProperty = DAORegistry.getPropertyDAO().get("crawler.lasvegas.scheduler.properties");
		
		//crawlerSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerSchedulerPropertiesString());
		crawlerSportsSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerSportsSchedulerPropertiesString());
		crawlerConcertsSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerConcertsSchedulerPropertiesString());
		crawlerTheaterSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerTheaterSchedulerPropertiesString());
		crawlerDefaultSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerDefaultSchedulerPropertiesString());
		crawlerLasVegasSchedulerPropertiesStringProperty.setValue(crawlerPropertiesCommand.getCrawlerLasVegasSchedulerPropertiesString());
		

		crawlerSchedulerModeProperty.setValue(crawlerPropertiesCommand.getCrawlerSchedulerMode());
		crawlerSchedulerStartProperty.setValue(crawlerPropertiesCommand.getCrawlerSchedulerStartHour() + "");
		crawlerSchedulerStopProperty.setValue(crawlerPropertiesCommand.getCrawlerSchedulerStopHour() + "");
		crawlerIdlePeriodEnabledProperty.setValue(crawlerPropertiesCommand.isCrawlerIdlePeriodEnabled() + "");
		crawlerIdlePeriodStartProperty.setValue(crawlerPropertiesCommand.getCrawlerIdlePeriodStartHour() + "");
		crawlerIdlePeriodStopProperty.setValue(crawlerPropertiesCommand.getCrawlerIdlePeriodStopHour() + "");
		crawlerMinCrawlPeriodProperty.setValue((crawlerPropertiesCommand.getCrawlerMinCrawlPeriod() * 60) + "");

		crawlerSortingTimeSliceProperty.setValue(crawlerPropertiesCommand.getCrawlerSortingTimeSlice() + "");
		crawlerProActiveFactorProperty.setValue(crawlerPropertiesCommand.getCrawlerProactiveFactor() + "");
		
		//DAORegistry.getPropertyDAO().update(crawlerSchedulerPropertiesStringProperty);
		DAORegistry.getPropertyDAO().update(crawlerSportsSchedulerPropertiesStringProperty);
		DAORegistry.getPropertyDAO().update(crawlerConcertsSchedulerPropertiesStringProperty);
		DAORegistry.getPropertyDAO().update(crawlerTheaterSchedulerPropertiesStringProperty);
		DAORegistry.getPropertyDAO().update(crawlerDefaultSchedulerPropertiesStringProperty);
		DAORegistry.getPropertyDAO().update(crawlerLasVegasSchedulerPropertiesStringProperty);
		
		DAORegistry.getPropertyDAO().update(crawlerSchedulerModeProperty);
		DAORegistry.getPropertyDAO().update(crawlerSchedulerStartProperty);
		DAORegistry.getPropertyDAO().update(crawlerSchedulerStopProperty);
		DAORegistry.getPropertyDAO().update(crawlerIdlePeriodEnabledProperty);
		DAORegistry.getPropertyDAO().update(crawlerIdlePeriodStartProperty);
		DAORegistry.getPropertyDAO().update(crawlerIdlePeriodStopProperty);
		DAORegistry.getPropertyDAO().update(crawlerSortingTimeSliceProperty);
		DAORegistry.getPropertyDAO().update(crawlerProActiveFactorProperty);
		DAORegistry.getPropertyDAO().update(crawlerMinCrawlPeriodProperty);

		
		// TODOJMS
//		crawlerSchedulerManager.reinit(); // Check why needed..
		/*ticketListingCrawler.reinitProperties();
		ticketListingCrawler.getTicketListingCrawlDispatcher().dispatch(new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_RESET_PROPERTIES));*/
		return new ModelAndView(new RedirectView("AdminEditCrawlerProperties?info=The crawler properties have been saved"));
	}

	/*public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}*/
	
	/*public CrawlerSchedulerManager getCrawlerSchedulerManager() {
		return crawlerSchedulerManager;
	}

	public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}*/
	
	public TicketListingCrawlerChecker getTicketListingCrawlerChecker() {
		return ticketListingCrawlerChecker;
	}

	public void setTicketListingCrawlerChecker(
			TicketListingCrawlerChecker ticketListingCrawlerChecker) {
		this.ticketListingCrawlerChecker = ticketListingCrawlerChecker;
	}
}
