package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.eventcreation.CreationEventHit;
import com.admitone.tmat.eventcreation.CreationEventListManager;

public class UpdatePOSEventManager {

	public UpdatePOSEventManager() {
		Timer timer = new Timer();
		Date nextTime =new Date();
		DateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String time = timeFormat.format(nextTime);
		String date = dateFormat.format(nextTime);
		String hour = time.split(" ")[1].split(":")[0];
		int hourInt = Integer.parseInt(hour) + 1;
		if(hourInt<10){
			hour = "0"+hourInt;
		}
		try {
			nextTime = timeFormat.parse(date + " " + hourInt+":00");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
					Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
					for(Event event:DAORegistry.getEventDAO().getAllEventByAdmitoneId()){
						eventMap.put(event.getAdmitoneId(),event);
					}
					
					Collection<CreationEventHit> localPOSEventHits =  CreationEventListManager.getPOSEventList(eventMap);
					CreationEventListManager.updatePOSEvent(localPOSEventHits);
			}
		}, nextTime , 60 * 60 * 1000);
	}
}

