package com.admitone.tmat.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EiMarketPlaceEvent;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.eimarketplace.EIMPRedirectHandler;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class EiMarketPlaceEventPopulator {
	private static EIMPCredential credential;
	
	private static boolean running = false;
	private static int fetchedEventCount;
	private static Set<Integer> usedEventIds;	
	
	public static void start() {
		if (running) {
			return;
		}
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.execute(new Runnable() {
			
			public void run() {
				running = true;				
				try {
					EiMarketPlaceEventPopulator.run();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					running = false;
				}
			}
		});
	}
	

	/**
	 * Execute the http request again if it fails. Do it 5 times.
	 */
	public static CloseableHttpResponse safeExecuteHttpRequest(SimpleHttpClient httpClient, HttpUriRequest request) {
		for(int i = 0 ; i < 5 ; i++) {
			CloseableHttpResponse response = null;
			try {
				response = httpClient.execute(request);
				if (response != null) {
					return response;
				}
			} catch (Exception e) {
				continue;
			}
		}
		return null;
	}
	
	
	public static boolean isRunning() {
		return running;
	}
	
	private static Pattern artistPageSelectPattern = Pattern.compile("<select name=\"e\".*?>(.*?)</select>", Pattern.DOTALL);
	private static Pattern artistPageOptionPattern = Pattern.compile("option value=\"(.*?)\".*?>(.*?)<", Pattern.DOTALL);

	private static void run() {
		Date startDate = new Date();
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(3);

		SimpleHttpClient httpClient = null;
		try {
			httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
			fetchedEventCount = 0;
			usedEventIds = new HashSet<Integer>();

			// get artists
			
			EIMPRedirectHandler redirectHandler = new EIMPRedirectHandler();
			httpClient.setRedirectHandler(redirectHandler);
			
			String searchUrl = "https://www.eimarketplace.com/brokers/byevent.cfm?restart=bykeyword.cfm&id=-1&b=187";
			HttpGet httpGet = new HttpGet(searchUrl);
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");
							
			HttpEntity uncompressedEntity = null;
			CloseableHttpResponse response = null;
			try{
				response = safeExecuteHttpRequest(httpClient, httpGet);
				uncompressedEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				response.close();
			}
			
			String content = EntityUtils.toString(uncompressedEntity);
			
			Matcher artistPageSelectMatcher = artistPageSelectPattern.matcher(content);
			
			if (!artistPageSelectMatcher.find()) {
				// System.out.println(">> RETURN FROM ARTIST PAGE");
				return;
			}
			
			String artistPageSelect = artistPageSelectMatcher.group(1);
			
			Matcher artistPageOptionMatcher = artistPageOptionPattern.matcher(artistPageSelect);
			
			while(artistPageOptionMatcher.find()) {
				Integer artistId = Integer.parseInt(artistPageOptionMatcher.group(1));

				//System.out.println("]]] FETCHING ARTIST ID=" + artistId);
				((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();
				executor.execute(new EiMarketPlaceArtistFetcherTask(artistId));
			}						
						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (httpClient != null) {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		
		try {
			executor.shutdown();
			// wait that all the tasks are executed
			executor.awaitTermination(2, TimeUnit.HOURS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		// for extra security we set the TTL of an expired event to one day
		// delete expired events
		Date yesterday = new Date(new Date().getTime() - 24L * 60L * 60L * 1000L);
		
		if (DAORegistry.getEiMarketPlaceEventDAO() != null) {
			System.out.println("]]]] DELETE OUTDATED EIMP EVENTS " + yesterday);
			
			DAORegistry.getEiMarketPlaceEventDAO().deleteEventsNotUpdatedAfter(startDate);
			System.out.println("]]]] FINISH TO DELETE EIMP EVENTS " + yesterday);
			
		}
	}
	
	public static int getFetchedEventCount() {
		synchronized(EiMarketPlaceEventPopulator.class) {
			return fetchedEventCount;
		}
	}
	
	public static class EiMarketPlaceArtistFetcherTask implements Runnable {
		private int artistId;

		public EiMarketPlaceArtistFetcherTask(int artistId) {
			this.artistId = artistId;
		}
		
/*

				<TR BGCOLOR=EEEEDD VALIGN="middle" ALIGN="CENTER">
					
					<TD CLASS="fieldinfo">
						<STRONG>Cobra Starship</STRONG>
						
						
							<SPAN CLASS="fineprint"><BR>

							w/ 
							30H!3</SPAN>
						
					</TD>
					
					<TD CLASS="fieldinfo">
						<STRONG>May 5, 2010</STRONG> at 05:30 PM
						<SPAN CLASS="fineprint"><BR>Wednesday</SPAN>
					</TD>
					
					
					
					<TD CLASS="fieldinfo">

						<STRONG>The Fillmore - Detroit</STRONG>
						<BR>
							<SPAN CLASS="fineprint">
							Detroit, 
							MI</SPAN>
						
					</TD>
					
					<TD CLASS="fieldinfo" ALIGN="CENTER">
					
					<A HREF="/brokers/results.cfm?restart=yes&p=949216"
					 onMouseOver="window.status='Display all tickets for 30H!3 at The Fillmore - Detroit on Wednesday, May 5, 2010 at 05:30 PM ...'; return true" 
					 onMouseOut="window.status=' '; return true"><SPAN STYLE="color: blue">View<BR>Tickets</SPAN></A>

							</TD>
						</TR>
						<TR BGCOLOR=CCCCCC>
							<TD COLSPAN=4>
					<IMG SRC="/graphics/blank.gif" ALT=" " WIDTH=500 HEIGHT=1 BORDER=0 HSPACE=0 VSPACE=0></TD>
					</TR>

*/
		//                                                                                                    event name      event detail(or no)      DATE        TIME                              VENUE NAME                                    CITY/STATE
		private Pattern eventPattern = Pattern.compile("<TR BGCOLOR=EEEE.. VALIGN=\"middle\" ALIGN=\"CENTER\">.*?<STRONG>(.*?)</STRONG>(.*?)</TD>.*?<STRONG>(.*?)</STRONG> at (.*?)<SPAN.*?</TD>.*?<TD.*?<STRONG>(.*?)</STRONG>.*?<SPAN CLASS=\"fineprint\">(.*?)</SPAN>.*?restart=yes&p=(.*?)\".*?</TR>", Pattern.DOTALL);
		private Pattern eventDetailPattern = Pattern.compile("<SPAN CLASS=\"fineprint\"><BR>(.*?)</SPAN>", Pattern.DOTALL);

		public void run() {
			SimpleHttpClient httpClient = null;			
			try {
				httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
				
				String searchUrl = "https://www.eimarketplace.com/brokers/pubsearch.cfm?E=" + artistId + "&RESTART=yes";
				HttpGet httpGet = new HttpGet(searchUrl);
				httpGet.addHeader("Host", "www.eimarketplace.com");
				httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");
								
				HttpEntity uncompressedEntity = null;
				CloseableHttpResponse response = null;
				try{
					response = safeExecuteHttpRequest(httpClient, httpGet);
					uncompressedEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					response.close();
				}
				
				String content = EntityUtils.toString(uncompressedEntity);				
				Matcher eventMatcher = eventPattern.matcher(content);
				
				Collection<EiMarketPlaceEvent> events = new ArrayList<EiMarketPlaceEvent>();
				
				while(eventMatcher.find()) {
					String eventName = eventMatcher.group(1);
					String tmpEventDetail = eventMatcher.group(2);
					Matcher eventDetailMatcher = eventDetailPattern.matcher(tmpEventDetail);
					String eventDetail = null;
					if (eventDetailMatcher.find()) {
						eventDetail = TextUtil.removeExtraWhitespaces(eventDetailMatcher.group(1)).trim();						
					}
					
					String eventDateString = eventMatcher.group(3).trim();
					String eventTimeString = eventMatcher.group(4).trim();	
					
					// Mar 12, 2009
					SimpleDateFormat ticketListingPageDateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");			
					SimpleDateFormat ticketListingPageDateFormat2 = new SimpleDateFormat("MMM dd, yyyy");
					
					
					// try to parse both date and time
					String eventDateTimeString = eventDateString + " " + eventTimeString;
					Date eventDate = null;
					try {
						eventDate = ticketListingPageDateFormat1.parse(eventDateTimeString);
					} catch (Exception e) {
					}
					
					// if it fails, only parse date
					if (eventDate == null) {
						try {
							eventDate = ticketListingPageDateFormat2.parse(eventDateString);
						} catch (Exception e) {
						}			
					}
					
					String venueName = eventMatcher.group(5).trim();
					
					String venueCityState = eventMatcher.group(6).trim();
					
					String[] venueTokens = venueCityState.split(",");
					String venueCity = venueTokens[0].trim();
					String venueState = null;
					if (venueTokens.length == 2) {
						venueState = venueTokens[1].trim();
					}
					
					Integer eventId = Integer.parseInt(eventMatcher.group(7).trim());					
					
					// System.out.println("EVENT NAME=" + eventName);
					// System.out.println("EVENT DETAIL=" + eventDetail);
					// System.out.println("EVENT DATE=" + eventDate);
					// System.out.println("VENUE NAME=" + venueName);
					// System.out.println("VENUE CITY=" + venueCity);
					// System.out.println("VENUE STATE=" + venueState);
					// System.out.println("EVENT ID=" + eventId);
					
					if (usedEventIds.contains(eventId)) {
						continue;
					}
					
					EiMarketPlaceEvent event = new EiMarketPlaceEvent();
					event.setId(eventId);
					event.setArtistId(artistId);
					event.setEventName(eventName);
					event.setEventDetail(eventDetail);
					event.setEventDate(eventDate);
					event.setVenueName(venueName);
					event.setVenueCity(venueCity);
					event.setVenueState(venueState);
					event.setUpdatedDate(new Date());
					
					events.add(event);
					
					usedEventIds.add(eventId);
					fetchedEventCount++;					
				}

				if (DAORegistry.getEiMarketPlaceEventDAO() != null) {
					DAORegistry.getEiMarketPlaceEventDAO().saveOrUpdateAll(events);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
			}

			HttpClientStore.releaseHttpClient(httpClient);						
		
		}
	}
	
	public void setCredential(EIMPCredential credential) {
		EiMarketPlaceEventPopulator.credential = credential;
	}
	
	
}
