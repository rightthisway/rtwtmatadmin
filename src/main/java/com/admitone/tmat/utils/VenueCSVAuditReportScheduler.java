package com.admitone.tmat.utils;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.CsvUploadRemoveAudit;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;


public class VenueCSVAuditReportScheduler implements InitializingBean{
	
	static MailManager mailManager = null;
	
	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 02);
		calender.add(Calendar.HOUR, 1);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						getCSvAuditList();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	}
	
	public static void getCSvAuditList(){
		try{
			
			Calendar calender = Calendar.getInstance();
			calender.set(Calendar.MINUTE, 00);
			calender.set(Calendar.SECOND, 00);
			calender.add(Calendar.HOUR, -1);
			Date fromDate = new Date(calender.getTimeInMillis());
			
			calender = Calendar.getInstance();
			calender.set(Calendar.MINUTE, 59);
			calender.set(Calendar.SECOND, 59);
			calender.add(Calendar.HOUR, -1);
			Date toDate = new Date(calender.getTimeInMillis());
			
			List<CsvUploadRemoveAudit> createdAudits = DAORegistry.getCsvUploadRemoveAuditDAO().getAllAuditsbyFilter("added", fromDate, toDate);
			List<CsvUploadRemoveAudit> updatedAudits = DAORegistry.getCsvUploadRemoveAuditDAO().getAllAuditsbyFilter("Updated", fromDate, toDate);
			/*if(createdAudits == null && updatedAudits == null) {
				return;
			}*/
			int createdCsvCount = 0,updatedCsvCount=0;
			if(createdAudits != null && !createdAudits.isEmpty()) {
				createdCsvCount = createdAudits.size();
			}
			if(updatedAudits != null && !updatedAudits.isEmpty()) {
				updatedCsvCount = updatedAudits.size();
			}
			if(createdCsvCount == 0 && updatedCsvCount == 0) {
				return;
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			getCSVAuditsList(workbook, "Created Csv's", createdAudits,"Created");
			getCSVAuditsList(workbook, "Updated Csv's", updatedAudits,"Updated");
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    try {
		        workbook.write(bos);
		    } finally {
		        bos.close();
		    }
		    byte[] bytes = bos.toByteArray();
		    MailAttachment[] mailAttachments = new MailAttachment[1];
		    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "CreatedAndUpdatedCSvList.xlsx");
				
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("createdCsvCount", createdCsvCount);
		map.put("updatedCsvCount", updatedCsvCount);
		
		String fromName  = "tmatmanagers@rightthisway.com";
		String mimeType = "text/html";
		String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com,"+
		"bmanusama@rightthisway.com,jreardon@rightthisway.com,Karthikkeyan.suga@gmail.com,TMAT02@rightthisway.com,adriandragne@gmail.com";
		String bccAddress = "AODev@rightthisway.com";
		//String toAddress ="tselvan@rightthisway.com";
		//String bccAddress = null;
		String template = "mail-csv-audit-list.html";
		String subject = "List of Created And Updated Csv's(LAST 1HOUR)";
		
		mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, mailAttachments);
		}catch(Exception e){
			e.printStackTrace();
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "List of Created And Updated Csv's(LAST 1HOUR) Mail Failed";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "All");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
		}
	}
	
	public static SXSSFWorkbook getCSVAuditsList(SXSSFWorkbook workbook,String sheetName,List<CsvUploadRemoveAudit> audits,String action) throws Exception {


		//String action="";
		///Date fromDate = new Date();
		//Date toDate = new Date();
		//List<CsvUploadRemoveAudit> audits = DAORegistry.getCsvUploadRemoveAuditDAO().getAllAuditsbyFilter(action, fromDate, toDate);

		Sheet ssSheet =  workbook.createSheet(sheetName);
		int j = 0;
		
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Created Date");
		j++;
		rowhead.createCell((int) j).setCellValue("Venue");
		j++;
		rowhead.createCell((int) j).setCellValue("Category Group");
	    j++;
	    rowhead.createCell((int) j).setCellValue("User");
	    j++;
    	rowhead.createCell((int) j).setCellValue("Action");
    	j++;
	    
	  /*  CellStyle cellamountStyle = workbook.createCellStyle();
	    cellamountStyle.setDataFormat(
	    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));*/
	    
	    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	   // DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    int rowvariable = 1;
	    if(audits != null) {
		    for (CsvUploadRemoveAudit audit : audits) {
		    	if(null == audit ){
		    		continue;
		    	}
		    	int column = 0;
				Row row = ssSheet.createRow(rowvariable);
				row.createCell((int) column).setCellValue(xlDateFormat.format(audit.getCreatedDate()));//Broker Name
		        column++;
				row.createCell((int) column).setCellValue(audit.getBuilding().replaceAll(",", "-"));//Invoice No
		        column++;
		        row.createCell((int) column).setCellValue(audit.getZonesgroup().toString().replaceAll(",", "-"));//Event Name
		        column++;
		        row.createCell((int) column).setCellValue(audit.getUsername());//Event Date
		        column++;
		    	row.createCell((int) column).setCellValue(action);//Venue
		    	column++;
		       
		        rowvariable++;
			}
	    }
		return workbook;
	
	}
}
