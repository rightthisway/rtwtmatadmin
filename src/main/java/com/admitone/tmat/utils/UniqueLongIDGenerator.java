package com.admitone.tmat.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.LoggerFactory;

/**
 * Generate a unique long ID (8 bytes) based on IP (last 2 digits) and date
 * The last 2 digits of the IP address should be enough considering that
 * all nodes in the cluster will be on the same LAN (B or C class).
 * We also append the time in second (4 bytes).
 * In case, we made several calls to getUID at the same time (same second),
 * a counter (2 bytes) will be used to differentiate them. 
 * 
 * uid =  0 | ip (2 last bytes) | date(seconds on 4 bytes) | counter(if same date on 15 bits)
 * can have at most 32768 unique id during the same date(second)
 * 
 * @author Francois Dang Ngoc
 */
 
public final class UniqueLongIDGenerator {
	private long counter = 0;
	private long baseIp;
	private long lastDate = -1;
	private static UniqueLongIDGenerator instance = null;
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(UniqueLongIDGenerator.class);
	private String ipAddress;
	
	/**
	 * @return singleton instance of the generator
	 */
	
	public static synchronized UniqueLongIDGenerator getInstance() {
		if (instance == null) {
			instance = new UniqueLongIDGenerator();
		}
		return instance;
	}
	
	public void reinit(int seed) {
		baseIp = ((long)0xff & (long)seed) << 55;
	}
	
	private UniqueLongIDGenerator() {
		InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();
			byte[] ipaddr = addr.getAddress();
			// consider only 2 last digits if IP (nodes in the cluster should be at least on
			// the same B class LAN, i.e., last 2 bytes)
			baseIp = ((long)0xff & (long)ipaddr[2]) << 55 | ((long)0xff & (long)ipaddr[3]) << 47;
			ipAddress = addr.getHostAddress();
			logger.info("IP address is " + ipAddress);
		} catch (UnknownHostException e) {
			logger.error("Could not get IP address for host\n");
			e.printStackTrace();
		}		
	}
	
	public String getIp() {
		return ipAddress;
	}

	/**
	 * Return unique identifier
	 * @return unique identifier
	 */
	public synchronized Long getUID() {
		long date = System.currentTimeMillis() / 1000;
		if (date != lastDate) {
			counter = 0L;
		}		
		lastDate = date;
		return baseIp | (date << 15) | counter++;
	}
	
	public synchronized Long getUniqueTimestamp() {
		long date = System.currentTimeMillis() / 1000;
		if (date != lastDate) {
			counter = 0L;
		}		
		lastDate = date;
		
		return date * 1000 + counter++;		
	}
}
