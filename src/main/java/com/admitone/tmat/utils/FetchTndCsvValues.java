package com.admitone.tmat.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class FetchTndCsvValues {

	public static void main(String[] args) {
		Connection connection = null;
		Connections connesctions = null;
		Statement statement = null;
		
		Date start = new Date();
		
		try{
		connesctions = new Connections();
		connection = connesctions.getTmatConnection();
		statement = connection.createStatement();
		
		}catch(Exception e){
			System.out.println("Exception to establish a connection "+e);
		}
		//System.out.println(connection);
		
		// first we will read csv file and then we add each row one by one.
		try{
		String srcFileLoc = "\\\\admit1\\shared\\MIS\\Developmentteam\\Mehul\\TND\\Feb2014.csv";
		File srcFile = new File(srcFileLoc);
		BufferedReader bufRdr = new BufferedReader(new FileReader(srcFile));
		//System.out.println(bufRdr.readLine());
		
		String line = null;
		Integer i=0;
		while ((line = bufRdr.readLine()) != null ) {
			
			if(null == line || line.isEmpty()){
				break;
			}
			if(connection == null || connesctions == null){
				break;
			}
			String[] rowValues = StringUtils.stripAll(line.split(","));
			
			if(rowValues.length == 0){
				break;
			}
			
			i++;
			/*// this is for temp use
			if(i <= 50000){
				continue;
			}*/
			
			if(i==1){
				continue;
			}
			
			for(int j=0 ; j<rowValues.length ; j++){
				if(rowValues[j].charAt(0) == '"' || rowValues[j].charAt(rowValues[j].length()-1) == '"'){
					rowValues[j] = rowValues[j].substring(1, rowValues[j].length()-1);
					//System.out.println(str);
				}
				rowValues[j] = rowValues[j].replaceAll("'", "");
			}
			//System.out.println(rowValues[7].length());
			//Integer order_id = Integer.parseInt(rowValues[2]);
			//System.out.println(rowValues[2]);
			//System.out.println(rowValues[2].substring(1, rowValues[2].length()-1));
			String sql = "insert into tndsalesreport values("+Integer.parseInt(rowValues[2])+",'"+rowValues[3]+"','"+rowValues[5]+"','"+rowValues[6]+"','"+rowValues[7]+
						 "','"+rowValues[8]+"','"+rowValues[9]+"','"+rowValues[10]+"','"+rowValues[11]+"','"+rowValues[12]+"','"+rowValues[13]+"','"+rowValues[14]+
						 "','"+rowValues[15]+"','"+rowValues[16]+"','"+rowValues[18]+"','"+rowValues[19]+"','"+rowValues[21]+"','"+rowValues[23]+"','"+rowValues[25]+
						 "','"+rowValues[26]+"','"+rowValues[27]+"','"+rowValues[28]+"')";
			try{
				statement.execute(sql);
				
			}catch(Exception e){
				System.out.println(sql + i + e);
				//e.printStackTrace();
			}
			//System.out.println(sql + i);
			/*if(i==50000){
				break;
			}*/
			
	}
		
		}catch(Exception e){
			e.printStackTrace();
		}
		Date end = new Date();
		System.out.println("Total time in seconds is "+(end.getTime()-start.getTime())/1000);
	}
}
