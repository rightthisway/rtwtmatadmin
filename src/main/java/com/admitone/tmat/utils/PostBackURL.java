package com.admitone.tmat.utils;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;

public class PostBackURL {

	private NamedCache postBackUrlMap;
	
	public PostBackURL() {
		postBackUrlMap = CacheFactory.getCache("postBackUrlMap");
	}
	public NamedCache getPostBackUrlMap() {
		return postBackUrlMap;
	}
}
