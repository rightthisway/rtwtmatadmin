package com.admitone.tmat.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.mail.MailManager;

public class TixCityNewCategorySalesReport implements InitializingBean{
	
	static MailManager mailManager = null;
		
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						//categorySalesMail(null ,null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	}
	
	// Mehul : to send manual statistics mail from report tab three parameters added on 09/29/2015
	
	public static Map<String, Object> categorySalesMail(Date startDate, Date endDate, String subject) throws Exception{
		
		try{
			Calendar cal = Calendar.getInstance();
			Date hourStartDate = cal.getTime();
		
			if(startDate == null || endDate == null || subject == null){
				cal.set(Calendar.HOUR_OF_DAY, 00);
				cal.set(Calendar.MINUTE, 00);
				cal.set(Calendar.SECOND, 00);
				startDate = cal.getTime();			
				cal = Calendar.getInstance();
				cal.add(Calendar.HOUR, -1);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
				cal.set(Calendar.MINUTE, 00);
				cal.set(Calendar.SECOND, 00);
				hourStartDate = cal.getTime();
				//subject = "Statistics of Tixcity New Cats and zones ProductLines";
			}
				
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getTixCityNewCategorySalesReportToday(startDate, endDate);
			
				int recordCount=0,hourRecordCount=0;
				boolean zoneFlag = false,flag = true;
				int autoCount=0,manualCount=0,zoneCount = 0,hourAutoCount=0,hourManualCount=0,
				hourZoneCount = 0,hourMiniCatsCount = 0,miniCatsCount = 0,vipCatsCount = 0,hourVipCatsCount = 0,
				hourVipAutoCatsCount = 0,vipAutoCatsCount = 0,lastRowMiniCats = 0,hourLastRowMiniCats = 0,
				larryLast = 0,hourLarryLast = 0,hourVividMiniCatsCount = 0,vividMiniCatsCount = 0,hourVividLastRowMiniCatsCount = 0,
				vividLastRowMiniCatsCount = 0,hourScoreBigMiniCatsCount = 0,scoreBigMiniCatsCount = 0,
				/*hourScoreBigAutoCatsCount = 0,scoreBigAutoCatsCount = 0,*/
				hourScoreBigLastRowMiniCatsCount=0,scoreBigLastRowMiniCatsCount=0,
				hourZonesLastRowCount=0,zonesLastRowCount=0,
				hourZonesCount=0,zonesCount=0, ticketCityMiniCatsCount=0,hourTicketCityMiniCatsCount=0, ticketCityVipMiniCatsCount=0, hourTicketCityVipMiniCatsCount=0,
						ticketCityLastRowMiniCatsCount=0,hourTicketCityLastRowMiniCatsCount=0, fanXchangeLastRowMiniCatsCount=0,hourFanXchangeLastRowMiniCatsCount=0;
				
				ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
				
				if(list != null && !list.isEmpty()){
				for(Object[] obj : list){
					
					Integer invoiceId = (Integer) obj[5];
					if(invoiceNo.contains(invoiceId))
						continue;
					
					invoiceNo.add(invoiceId);
						
					//recordCount += 1;
					String catType = (String) obj[12];
					String type = (String) obj[13];
					String ebayType = (String) obj[14];
					
					if(catType != null && !catType.isEmpty()){
						/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
							catType = "VIVID LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
							catType = "SCOREBIG MINICATS";
						}*/
						
						/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG AUTO";
						}*/
						
						/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG LASTROW MINICATS";
						}*/
						//else
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("AUTOCAT")){
							catType = "TN AUTO";								
						}else if(catType.equalsIgnoreCase("MINICATS") || catType.equalsIgnoreCase("ZMINICATS")){
							catType = "TN MINICATS";
						}else if(catType.equalsIgnoreCase("VIPMINICATS") || catType.equalsIgnoreCase("ZVIPMINICATS")){
							catType = "TN VIP MINICATS";
						}else if(catType.equalsIgnoreCase("VIPAUTO")){
							catType = "TN VIP AUTOCATS";
						}else if(catType.equalsIgnoreCase("LASTROW MINICATS") || catType.equalsIgnoreCase("ZLASTROW MINICATS")){
							catType = "TN LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "TN LARRYLAST";
						}else if(catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("TCZP")){
							catType = "ZONES PRICING";
						}else if(catType.equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "ZONED LASTROW MINICATS";
						}else{
							//catType = "MANUAL";
						}
					}else{
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
							catType = "SCOREBIG LASTROW MINICATS";
						}						
					}
					Date createDate = new Date(((Timestamp) obj[6]).getTime());
					if(obj[2] != null && !obj[2].equals("")){
						String section = String.valueOf(obj[2]).toLowerCase();
						if(section.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}
					
					if(obj[3] != null && !obj[3].equals("")){
						String row = String.valueOf(obj[3]).toLowerCase();
						if(row.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}			
					
					if(obj[10] != null && !obj[10].equals("")){	
							if(catType.equalsIgnoreCase("TN AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourAutoCount += 1;
									hourRecordCount += 1;
								}
									autoCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourMiniCatsCount += 1;
								}
									miniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipAutoCatsCount += 1;
								}
									vipAutoCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipCatsCount += 1;
								}
									vipCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLastRowMiniCats += 1;
								}
									lastRowMiniCats += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLarryLast += 1;
								}
									larryLast += 1;
									recordCount += 1;
							}/*else if(zoneFlag){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesCount += 1;
								}
									zonesCount += 1;
									recordCount += 1;
							}*/else if(catType.equalsIgnoreCase("ZONES PRICING")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZoneCount += 1;
								}
									zoneCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesLastRowCount += 1;
								}
									zonesLastRowCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityMiniCatsCount += 1;
								}
									ticketCityMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityVipMiniCatsCount += 1;
								}
									ticketCityVipMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityLastRowMiniCatsCount+= 1;
								}
									ticketCityLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourFanXchangeLastRowMiniCatsCount+= 1;
								}
									fanXchangeLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigLastRowMiniCatsCount+= 1;
								}
									scoreBigLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}/*else if(catType.equalsIgnoreCase("VIVID MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividMiniCatsCount += 1;
								}
									vividMiniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividLastRowMiniCatsCount += 1;
								}
									vividLastRowMiniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigMiniCatsCount += 1;
								}
									scoreBigMiniCatsCount += 1;
									recordCount += 1;
							}*//*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigAutoCatsCount += 1;
								}
									scoreBigAutoCatsCount += 1;
							}*/
							/*else{
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourManualCount += 1;
								}
									manualCount += 1;
							}*/						
					}
					
					zoneFlag = false;
				}
				}
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com";
				String bccAddress = "AODev@rightthisway.com";
				//String toAddress ="pmehul@rightthisway.com,kulaganathan@rightthisway.com";
				//String bccAddress = "tselvan@rightthisway.com";
				
				String template = "tixcity-mail-category-sales.html";
				Map<String, Object> map = new HashMap<String, Object>();
				//VIVID COUNTS
				map.put("tixCityNewHourVividMiniCatsCount", hourVividMiniCatsCount);
				map.put("tixCityNewVividMiniCatsCount", vividMiniCatsCount);
				map.put("tixCityNewHourVividLastRowMiniCatsCount", hourVividLastRowMiniCatsCount);
				map.put("tixCityNewVividLastRowMiniCatsCount", vividLastRowMiniCatsCount);
				//SCOREBIG COUNT
				map.put("tixCityNewHourScoreMiniCatsCount", hourScoreBigMiniCatsCount);
				map.put("tixCityNewScoreMiniCatsCount", scoreBigMiniCatsCount);
				/*map.put("hourScoreBigAutoCatsCount", hourScoreBigAutoCatsCount);
				map.put("scoreBigAutoCatsCount", scoreBigAutoCatsCount);*/
				map.put("tixCityNewHourScoreBigLastRowMiniCatsCount", hourScoreBigLastRowMiniCatsCount);
				map.put("tixCityNewScoreBigLastRowMiniCatsCount", scoreBigLastRowMiniCatsCount);
				// Ticket City Count
				map.put("tixCityNewTicketCityMiniCatsCount", ticketCityMiniCatsCount);
				map.put("tixCityNewHourTicketCityMiniCatsCount", hourTicketCityMiniCatsCount);
				map.put("tixCityNewTicketCityVipMiniCatsCount", ticketCityVipMiniCatsCount);
				map.put("tixCityNewHourTicketCityVipMiniCatsCount", hourTicketCityVipMiniCatsCount);
				map.put("tixCityNewTicketCityLastRowMiniCatsCount", ticketCityLastRowMiniCatsCount);
				map.put("tixCityNewHourTicketCityLastRowMiniCatsCount", hourTicketCityLastRowMiniCatsCount);
				// Fanxchange Count
				map.put("tixCityNewFanXchangeLastRowMiniCatsCount", fanXchangeLastRowMiniCatsCount);
				map.put("tixCityNewHourFanXchangeLastRowMiniCatsCount", hourFanXchangeLastRowMiniCatsCount);
				//TN COUNTS
				map.put("tixCityNewHourAutoCount", hourAutoCount);
				map.put("tixCityNewAutoCount", autoCount);
				map.put("tixCityNewHourVipCatsCount", hourVipCatsCount);
				map.put("tixCityNewVipCatsCount", vipCatsCount);
				map.put("tixCityNewHourVipAutoCatsCount", hourVipAutoCatsCount);				
				map.put("tixCityNewLarryLast", larryLast);
				map.put("tixCityNewHourLarryLast", hourLarryLast);				
				map.put("tixCityNewLastRowMiniCats", lastRowMiniCats);
				map.put("tixCityNewHourLastRowMiniCats", hourLastRowMiniCats);				
				map.put("tixCityNewVipAutoCatsCount", vipAutoCatsCount);				
				//map.put("hourManualCount", hourManualCount);
				//map.put("manualCount", manualCount);
				map.put("tixCityNewHourZoneCount", hourZoneCount);
				map.put("tixCityNewZoneCount", zoneCount);
				map.put("tixCityNewHourRecordCount", hourRecordCount);
				map.put("tixCityNewRecordCount", recordCount);
				map.put("tixCityNewHourMiniCatsCount", hourMiniCatsCount);
				map.put("tixCityNewMiniCatsCount", miniCatsCount);
				map.put("tixCityNewZonesLastRow", zonesLastRowCount);
				map.put("tixCityNewHourZonesLastRow", hourZonesLastRowCount);
				map.put("tixCityNewHourZonesCount", hourZonesCount);
				map.put("tixCityNewZonesCount", zonesCount);
				//TOTAL COUNTS
				map.put("tixCityNewTotalMinicatsCount", miniCatsCount+vividMiniCatsCount+scoreBigMiniCatsCount+ticketCityMiniCatsCount);
				map.put("tixCityNewTotalHourMinicatsCount", hourMiniCatsCount+hourVividMiniCatsCount+hourScoreBigMiniCatsCount+hourTicketCityMiniCatsCount);
				
				map.put("tixCityNewTotalAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				map.put("tixCityNewTotalHourAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				
				map.put("tixCityNewTotalLastRowMinicatsCount", lastRowMiniCats+vividLastRowMiniCatsCount+scoreBigLastRowMiniCatsCount+ticketCityLastRowMiniCatsCount+
						fanXchangeLastRowMiniCatsCount);
				map.put("tixCityNewTotalHourLastRowMinicatsCount", hourLastRowMiniCats+hourVividLastRowMiniCatsCount+hourScoreBigLastRowMiniCatsCount+
						hourTicketCityLastRowMiniCatsCount+hourFanXchangeLastRowMiniCatsCount);
				
				map.put("tixCityNewTotalHourVipCatsCount", hourVipCatsCount + hourTicketCityVipMiniCatsCount);
				map.put("tixCityNewTotalVipCatsCount", vipCatsCount + ticketCityVipMiniCatsCount);
				
				map.put("tixCityNewTotalTicketNetworkCount", autoCount+miniCatsCount+vipCatsCount+vipAutoCatsCount+zoneCount+lastRowMiniCats+larryLast+zonesLastRowCount+zonesCount);
				map.put("tixCityNewTotalHourTicketNetworkCount", hourAutoCount+hourMiniCatsCount+hourVipCatsCount+hourVipAutoCatsCount+hourZoneCount+hourLastRowMiniCats+hourLarryLast+hourZonesLastRowCount+hourZonesCount);
				
				map.put("tixCityNewTotalVividSeatsCount", vividMiniCatsCount+vividLastRowMiniCatsCount);
				map.put("tixCityNewTotalHourVividSeatsCount", hourVividMiniCatsCount+hourVividLastRowMiniCatsCount);
				
				map.put("tixCityNewTotalScoreBigCount", /*scoreBigAutoCatsCount+*/scoreBigMiniCatsCount+scoreBigLastRowMiniCatsCount);
				map.put("tixCityNewTotalHourScoreBigCount", /*hourScoreBigAutoCatsCount+*/hourScoreBigMiniCatsCount+hourScoreBigLastRowMiniCatsCount);
				
				map.put("tixCityNewTotalTicketCityCount", ticketCityMiniCatsCount + ticketCityVipMiniCatsCount + ticketCityLastRowMiniCatsCount);
				map.put("tixCityNewTotalHourTicketCityCount", hourTicketCityMiniCatsCount + hourTicketCityVipMiniCatsCount + hourTicketCityLastRowMiniCatsCount);
				
				map.put("tixCityNewTotalFanXchangeCount", fanXchangeLastRowMiniCatsCount);
				map.put("tixCityNewTotalHourFanXchangeCount", hourFanXchangeLastRowMiniCatsCount);
				
				//mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null);
				return map;
		}catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			subject = "Error while getting Hourly TixCityNew Category Sales";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "TixCity New");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);	
			throw e;
		}		
	}

}
