package com.admitone.tmat.utils;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.pojo.TNDRtfNoSaleReport;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;

public class TNDRtfNoSaleReportScheduler implements InitializingBean{
	
	public static MailManager mailManager = null;
	public static MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		TNDRtfNoSaleReportScheduler.mailIdProperty = mailIdProperty;
	}
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		TNDRtfNoSaleReportScheduler.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {

		Date now = new Date();
		
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 03);
		calender.add(Calendar.HOUR, 1);
		
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						tndRtfNoSaleEmail();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	
	}
	
	public void tndRtfNoSaleEmail() throws Exception{
		
		try {
			Calendar calender = Calendar.getInstance();
			//Tamil 11/17/2017 : send Email at 8AM, 12 PM, 4PM and 8PM only 
			if(calender.get(Calendar.HOUR_OF_DAY) == 8 || calender.get(Calendar.HOUR_OF_DAY) == 12 ||
					calender.get(Calendar.HOUR_OF_DAY) == 16 || calender.get(Calendar.HOUR_OF_DAY) == 20) {
			
			System.out.println("inside tnd rtf no sale email..."+new Date());
			SXSSFWorkbook workbook  = getTNDRtfNoSaleReport();
			Sheet ssSheet = workbook.getSheetAt(0);
			int num = ssSheet.getLastRowNum();
			if(ssSheet != null && ssSheet.getLastRowNum() >= 1) {
				System.out.println("inside tnd rtf no sale  email with data..."+new Date());
			    ByteArrayOutputStream bos = new ByteArrayOutputStream();
			    try {
			        workbook.write(bos);
			    } finally {
			        bos.close();
			    }
			    byte[] bytes = bos.toByteArray();
			    MailAttachment[] mailAttachments = new MailAttachment[1];
			    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "RTWnosalesreport.xlsx");
					
			    String resource = "mail-tnd-rtf-no-sale-report.html";
				String subject = "TND RTF No Sale Report.";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com,TMAT02@rightthisway.com,tmat01@rightthisway.com,tmat04@rightthisway.com,adriandragne@gmail.com";
				String bccAddress = "AODev@rightthisway.com";
				//String bccAddress = "AODev@rightthisway.com";
				String fromName = "tmatmanagers@rightthisway.com";
				String cc = null;//"araut@rightthisway.com";
				//String bccAddress = "AODev@rightthisway.com";
				
				//String toAddress ="tselvan@rightthisway.com";
				//String bccAddress = null;
				
				System.out.println("before tnd rtf no sale  email email..."+new Date());
				Map<String, Object> map = new HashMap<String, Object>();
				
				mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, resource, map, mimeType, mailAttachments);
				System.out.println("after tnd rtf no sale  email email..."+new Date()+"...."+mailIdProperty.getTmatmanagers());
			} else {
				 String resource = "mail-tnd-rtf-no-sale-report.html";
					String subject = "TND RTF No Sale Report No Data Found.";
					String mimeType = "text/html";
					String toAddress ="tselvan@rightthisway.com";
					String bccAddress = null;//"AODev@rightthisway.com";
					//String bccAddress = "AODev@rightthisway.com";
					String fromName = "tmatmanagers@rightthisway.com";
					String cc = null;//"araut@rightthisway.com";
					//String bccAddress = "AODev@rightthisway.com";
					
					//String toAddress ="tselvan@rightthisway.com";
					//String bccAddress = null;
					
					System.out.println("before tnd rtf no sale  email email..."+new Date());
					Map<String, Object> map = new HashMap<String, Object>();
					
					mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, resource, map, mimeType, null);
					System.out.println("No data tnd rtf no sale  email ..."+new Date());
			}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Error while downloading TND_RTF_NO_SALE_Report.";
			String template = "mail-rtw-pos-unsent-order-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("error", e);
			//String toAddress = "tselvan@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;
		}		
	}
	
	public static SXSSFWorkbook getTNDRtfNoSaleReport() throws Exception {


		List<TNDRtfNoSaleReport> list = DAORegistry.getQueryManagerDAO().getTNDRtfNoSaleeport();

		SXSSFWorkbook workbook = new SXSSFWorkbook();
		Sheet ssSheet =  workbook.createSheet();
		int j = 0;
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Event Id");
	    j++;
		rowhead.createCell((int) j).setCellValue("Event Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("EventTime");
    	j++;
    	rowhead.createCell((int) j).setCellValue("Venue Name");
    	j++;
    	rowhead.createCell((int) j).setCellValue("City");
 	    j++;
    	rowhead.createCell((int) j).setCellValue("State");
    	j++;
    	 rowhead.createCell((int) j).setCellValue("Total TN Sales");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("Total Tixcity Sales");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("TixCity Tickets Count");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("TixCity Event BroadCast");
	    j++;
 	    rowhead.createCell((int) j).setCellValue("RTF Tickets Count");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("RTF Event BroadCast");
	    j++;
	    rowhead.createCell((int) j).setCellValue("TMAT Event");
	    j++;
	    rowhead.createCell((int) j).setCellValue("CSV");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Map");
	    j++;
	    rowhead.createCell((int) j).setCellValue("ticketNetwork Crawl");
	    j++;
	    rowhead.createCell((int) j).setCellValue("TicketEvolution Crawl");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Stubhub Crawl");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Vividseats Crawl");
	    j++;
	    rowhead.createCell((int) j).setCellValue("FlashSeats Crawl");
	    j++;
	    
	    int rowvariable = 1;
	    for (TNDRtfNoSaleReport tndRtfNoSaleReport : list) {
			
	    	int column = 0;
	    	Row row = ssSheet.createRow(rowvariable);
			row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getEventId());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getEventName());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getEventDateStr());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getEventTimeStr());
	        column++;
	       
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getVenue());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getCity());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getState());
	        column++;
	        //row.createCell((int) column).setCellValue(tndEventSalesDetail.getCountry());
	        //column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTnSalesCount());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTixCitySalesCount());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTixcityTicketsCount());
	        column++;
	        String tixCityBroadcast="N";
	        if(tndRtfNoSaleReport.getAutocats96Enabled().equals("Y") || tndRtfNoSaleReport.getLarryLastEnabled().equals("Y") ||
	        		tndRtfNoSaleReport.getZoneLastRowEnabled().equals("Y")) {
	        	tixCityBroadcast="Y";
	        }
	        row.createCell((int) column).setCellValue(tixCityBroadcast);
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getRtfTicketsCount());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getRtfListingEnabled());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTmatEvent());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getCsvUploaded());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getMapUploaded());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTicketNetworkDirectCrawl());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getTicketEvolutionCrawl());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getStubhubCrawl());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getVividSeatsCrawl());
	        column++;
	        row.createCell((int) column).setCellValue(tndRtfNoSaleReport.getFlashSeatsCrawl());
	        column++;
	        
	        rowvariable++;
		}
		return workbook;
	
	}

}
