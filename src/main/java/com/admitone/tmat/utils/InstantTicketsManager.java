package com.admitone.tmat.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;

/**
 * To copy the stubhub instant tickets file periodically every 15 minutes to the specified file location in dbo.property
 * 
 *
 */

public class InstantTicketsManager {
	
	private static final int INSTANT_TIX_TIMER_INTERVAL = 15 * 60 * 1000;
	
	public InstantTicketsManager()
	{
		/*Calendar calendar = new GregorianCalendar();
		int minute = calendar.get(Calendar.MINUTE);
		
		minute = (int)(Math.ceil((double)minute / 15D)) * 15;
		
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, 0);

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(
				new TimerTask(){
				public void run(){
					try{
						String fileLoc ="";
						Collection<Ticket> instantCustomizedTickets = InstantTicketsUtil.computeCustomizedCatInstantTickets();
						Collection<Ticket> instantTickets = InstantTicketsUtil.computeInstantTickets();
//						try{
//							Property property = DAORegistry.getPropertyDAO().get("instantTickets.fileLoc");
//							fileLoc = property.getValue();
//							OutputStream output = new FileOutputStream(fileLoc, false);
//							InstantTicketsUtil.downloadInstantTicketCsvFile(instantTickets,output);
//							output.close();
//						}catch(Exception ex){
//							ex.printStackTrace();
//						}
						try{
							Property tndProperty = DAORegistry.getPropertyDAO().get("instantTicketsTND.fileLoc");
							fileLoc = tndProperty.getValue();
							OutputStream tndOutput = new FileOutputStream(fileLoc, false);
							InstantTicketsUtil.downloadInstantTicketTNDCsvFile(instantTickets, tndOutput);
							tndOutput.close();
						}catch(Exception ex){
							ex.printStackTrace();
						}
						
						try{
							Property custInstantproperty = DAORegistry.getPropertyDAO().get("instantCustomizedTickets.fileLoc");
							fileLoc = custInstantproperty.getValue();
							OutputStream custOutput = new FileOutputStream(fileLoc, false);
							InstantTicketsUtil.downloadCustomizedInstantTicketCsvFile(instantCustomizedTickets, custOutput);
							custOutput.close();
						}catch(Exception ex){
							ex.printStackTrace();
						}
						
						try{
							Property custTNDInstantproperty = DAORegistry.getPropertyDAO().get("instantCustomized.tnd.Tickets.fileLoc");
							fileLoc = custTNDInstantproperty.getValue();
							OutputStream custTNDOutput = new FileOutputStream(fileLoc, false);
							InstantTicketsUtil.downloadCustomizedTNDInstantTicketCsvFile(instantCustomizedTickets, custTNDOutput);
							custTNDOutput.close();
							
						}
						catch(Exception e){
							e.printStackTrace();
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				}
				, calendar.getTime(), INSTANT_TIX_TIMER_INTERVAL);	
	*/
	}
}
