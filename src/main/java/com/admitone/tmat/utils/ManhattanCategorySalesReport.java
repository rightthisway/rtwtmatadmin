package com.admitone.tmat.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.mail.MailManager;

public class ManhattanCategorySalesReport implements InitializingBean{
	
	static MailManager mailManager = null;
		
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						//categorySalesMail(null, null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	}
	
	// Mehul : to send manual statistics mail from report tab three parameters added on 09/29/2015
	
	public static Map<String, Object> categorySalesMail(Date startDate, Date endDate, String subject)throws Exception{
		
		try{
			Calendar cal = Calendar.getInstance();
			Date hourStartDate = cal.getTime();
		
		if(startDate == null || endDate == null || subject == null){
			cal.set(Calendar.HOUR_OF_DAY, 00);
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			startDate = cal.getTime();			
			cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, -1);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			endDate = cal.getTime();
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			hourStartDate = cal.getTime();
			//subject = "Statistics of Manhattan Cats and zones ProductLines";
		}
				
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getManhattanCategorySalesReportToday(startDate, endDate);
			/*
			Object[] zonesPricingDummySale = new Object[15];
			Object[] zonesLastRowDummySale = new Object[15];
			Object[] zonesDummySale = new Object[15];
			
			zonesPricingDummySale[0] = (Object)"Zones Pricing Dummy Event";
			zonesPricingDummySale[1] = (Object)"2015-2-20 19:00:00.000";
			zonesPricingDummySale[2] = (Object)"Upper";
			zonesPricingDummySale[3] = (Object)"A";
			zonesPricingDummySale[4] = (Object)"123456";
			zonesPricingDummySale[5] = 12345;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date zonesSalesDate = dateFormat.parse("01/30/2015 09:42:23");
			Timestamp zonestsp = new Timestamp(zonesSalesDate.getTime());
			zonesPricingDummySale[6] = zonestsp;
			zonesPricingDummySale[7] = (Object)"1234";
			zonesPricingDummySale[8] = (Object)(Object)"220";
			zonesPricingDummySale[9] = (Object)"200";
			zonesPricingDummySale[10] = (Object)"210.20";
			zonesPricingDummySale[11] = (Object)"0.00";
			zonesPricingDummySale[12] = (Object)"ZP";
			zonesPricingDummySale[13] = (Object)"NULL";
			zonesPricingDummySale[14] = (Object)"Web User";
			
			zonesLastRowDummySale[0] = (Object)"Zones Last Row Dummy Event";
			zonesLastRowDummySale[1] = (Object)"2015-2-20 20:00:00.000";
			zonesLastRowDummySale[2] = (Object)"Lower";
			zonesLastRowDummySale[3] = (Object)"B";
			zonesLastRowDummySale[4] = (Object)"654321";
			zonesLastRowDummySale[5] = 54321;
			Date zoneslastrowSalesDate = dateFormat.parse("01/30/2015 09:45:23");
			zonestsp = new Timestamp(zoneslastrowSalesDate.getTime());
			zonesLastRowDummySale[6] = zonestsp;
			zonesLastRowDummySale[7] = (Object)"4321";
			zonesLastRowDummySale[8] = (Object)"330";
			zonesLastRowDummySale[9] = (Object)"300";
			zonesLastRowDummySale[10] = (Object)"310.20";
			zonesLastRowDummySale[11] = (Object)"0.00";
			zonesLastRowDummySale[12] = (Object)"ZLR";
			zonesLastRowDummySale[13] = (Object)"NULL";
			zonesLastRowDummySale[14] = (Object)"Web User";
			
			zonesDummySale[0] = (Object)"Zones Dummy Event";
			zonesDummySale[1] = (Object)"2015-2-20 20:00:00.000";
			zonesDummySale[2] = (Object)"zone";
			zonesDummySale[3] = (Object)"B";
			zonesDummySale[4] = (Object)"159753";
			zonesDummySale[5] = 98765;
			Date zoneSalesDate = dateFormat.parse("01/30/2015 09:48:23");
			zonestsp = new Timestamp(zoneSalesDate.getTime());
			zonesDummySale[6] = zonestsp;
			zonesDummySale[7] = (Object)"4321";
			zonesDummySale[8] = (Object)"330";
			zonesDummySale[9] = (Object)"300";
			zonesDummySale[10] = (Object)"310.20";
			zonesDummySale[11] = (Object)"0.00";
			zonesDummySale[12] = (Object)"hello";
			zonesDummySale[13] = (Object)"NULL";
			zonesDummySale[14] = (Object)"Web User";
			
			list.add(zonesPricingDummySale);
			list.add(zonesLastRowDummySale);
			list.add(zonesDummySale);
			*/
				int recordCount=0,hourRecordCount=0;
				boolean zoneFlag = false,flag = true;
				int autoCount=0,manualCount=0,zoneCount = 0,hourAutoCount=0,hourManualCount=0,
				hourZoneCount = 0,hourMiniCatsCount = 0,miniCatsCount = 0,vipCatsCount = 0,hourVipCatsCount = 0,
				hourVipAutoCatsCount = 0,vipAutoCatsCount = 0,lastRowMiniCats = 0,hourLastRowMiniCats = 0,
				larryLast = 0,hourLarryLast = 0,hourVividMiniCatsCount = 0,vividMiniCatsCount = 0,hourVividLastRowMiniCatsCount = 0,
				vividLastRowMiniCatsCount = 0,hourScoreBigMiniCatsCount = 0,scoreBigMiniCatsCount = 0,
				/*hourScoreBigAutoCatsCount = 0,scoreBigAutoCatsCount = 0,*/
				hourScoreBigLastRowMiniCatsCount=0,scoreBigLastRowMiniCatsCount=0,
				hourZonesLastRowCount=0,zonesLastRowCount=0,
				hourZonesCount=0,zonesCount=0, ticketCityMiniCatsCount=0,hourTicketCityMiniCatsCount=0, ticketCityVipMiniCatsCount=0, hourTicketCityVipMiniCatsCount=0,
				ticketCityLastRowMiniCatsCount=0,hourTicketCityLastRowMiniCatsCount=0, fanXchangeLastRowMiniCatsCount=0,hourFanXchangeLastRowMiniCatsCount=0;
				
				ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
				
				if(list != null && !list.isEmpty()){
				for(Object[] obj : list){
					
					Integer invoiceId = (Integer) obj[5];
					if(invoiceNo.contains(invoiceId))
						continue;
					
					invoiceNo.add(invoiceId);
						
					//recordCount += 1;
					String catType = (String) obj[12];
					String type = (String) obj[13];
					String ebayType = (String) obj[14];
					
					if(catType != null && !catType.isEmpty()){
						/*if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
							catType = "VIVID LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
							catType = "SCOREBIG MINICATS";
						}*/
						
						/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG AUTO";
						}*/
						
						/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG LASTROW MINICATS";
						}*/
						//else
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("AUTOCAT")){
							catType = "TN AUTO";								
						}else if(catType.equalsIgnoreCase("MINICATS") || catType.equalsIgnoreCase("ZMINICATS")){
							catType = "TN MINICATS";
						}else if(catType.equalsIgnoreCase("VIPMINICATS") || catType.equalsIgnoreCase("ZVIPMINICATS")){
							catType = "TN VIP MINICATS";
						}else if(catType.equalsIgnoreCase("VIPAUTO")){
							catType = "TN VIP AUTOCATS";
						}else if(catType.equalsIgnoreCase("LASTROW MINICATS") || catType.equalsIgnoreCase("ZLASTROW MINICATS")){
							catType = "TN LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "TN LARRYLAST";
						}else if(catType.equalsIgnoreCase("ZP") || catType.equalsIgnoreCase("MZP")){
							catType = "ZONES PRICING";
						}else if(catType.equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "ZONED LASTROW MINICATS";
						}else{
							//catType = "MANUAL";
						}
					}else{
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("scorebig41")){
							catType = "SCOREBIG LASTROW MINICATS";
						}							
					}
					Date createDate = new Date(((Timestamp) obj[6]).getTime());
					if(obj[2] != null && !obj[2].equals("")){
						String section = String.valueOf(obj[2]).toLowerCase();
						if(section.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}
					
					if(obj[3] != null && !obj[3].equals("")){
						String row = String.valueOf(obj[3]).toLowerCase();
						if(row.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}			
					
					if(obj[10] != null && !obj[10].equals("")){	
							if(catType.equalsIgnoreCase("TN AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourAutoCount += 1;
									hourRecordCount += 1;
								}
									autoCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourMiniCatsCount += 1;
								}
									miniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipAutoCatsCount += 1;
								}
									vipAutoCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipCatsCount += 1;
								}
									vipCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLastRowMiniCats += 1;
								}
									lastRowMiniCats += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLarryLast += 1;
								}
									larryLast += 1;
									recordCount += 1;
							}/*else if(zoneFlag){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesCount += 1;
								}
									zonesCount += 1;
									recordCount += 1;
							}*/else if(catType.equalsIgnoreCase("ZONES PRICING")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZoneCount += 1;
								}
									zoneCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesLastRowCount += 1;
								}
									zonesLastRowCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityMiniCatsCount += 1;
								}
									ticketCityMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityVipMiniCatsCount += 1;
								}
									ticketCityVipMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityLastRowMiniCatsCount+= 1;
								}
									ticketCityLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourFanXchangeLastRowMiniCatsCount+= 1;
								}
									fanXchangeLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigLastRowMiniCatsCount+= 1;
								}
									scoreBigLastRowMiniCatsCount +=1 ;
									recordCount += 1;
							}/*else if(catType.equalsIgnoreCase("VIVID MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividMiniCatsCount += 1;
								}
									vividMiniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividLastRowMiniCatsCount += 1;
								}
									vividLastRowMiniCatsCount += 1;
									recordCount += 1;
							}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigMiniCatsCount += 1;
								}
									scoreBigMiniCatsCount += 1;
									recordCount += 1;
							}*//*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigAutoCatsCount += 1;
								}
									scoreBigAutoCatsCount += 1;
							}*/
							/*else{
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourManualCount += 1;
								}
									manualCount += 1;
							}*/						
					}
					
					zoneFlag = false;
				}
				}
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com";
				String bccAddress = "AODev@rightthisway.com";
				//String toAddress ="pmehul@rightthisway.com,kulaganathan@rightthisway.com";
				//String bccAddress = "tselvan@rightthisway.com";
				
				String template = "manhattan-mail-category-sales.html";
				Map<String, Object> map = new HashMap<String, Object>();
				//VIVID COUNTS
				map.put("manHourVividMiniCatsCount", hourVividMiniCatsCount);
				map.put("manVividMiniCatsCount", vividMiniCatsCount);
				map.put("manHourVividLastRowMiniCatsCount", hourVividLastRowMiniCatsCount);
				map.put("manVividLastRowMiniCatsCount", vividLastRowMiniCatsCount);
				//SCOREBIG COUNT
				map.put("manHourScoreMiniCatsCount", hourScoreBigMiniCatsCount);
				map.put("manScoreMiniCatsCount", scoreBigMiniCatsCount);
				/*map.put("hourScoreBigAutoCatsCount", hourScoreBigAutoCatsCount);
				map.put("scoreBigAutoCatsCount", scoreBigAutoCatsCount);*/
				map.put("manHourScoreBigLastRowMiniCatsCount", hourScoreBigLastRowMiniCatsCount);
				map.put("manScoreBigLastRowMiniCatsCount", scoreBigLastRowMiniCatsCount);
				// Ticket City Count
				map.put("manTicketCityMiniCatsCount", ticketCityMiniCatsCount);
				map.put("manHourTicketCityMiniCatsCount", hourTicketCityMiniCatsCount);
				map.put("manTicketCityVipMiniCatsCount", ticketCityVipMiniCatsCount);
				map.put("manHourTicketCityVipMiniCatsCount", hourTicketCityVipMiniCatsCount);
				map.put("manTicketCityLastRowMiniCatsCount", ticketCityLastRowMiniCatsCount);
				map.put("manHourTicketCityLastRowMiniCatsCount", hourTicketCityLastRowMiniCatsCount);
				// Fanxchange Count
				map.put("manFanXchangeLastRowMiniCatsCount", fanXchangeLastRowMiniCatsCount);
				map.put("manHourFanXchangeLastRowMiniCatsCount", hourFanXchangeLastRowMiniCatsCount);
				//TN COUNTS
				map.put("manHourAutoCount", hourAutoCount);
				map.put("manAutoCount", autoCount);
				map.put("manHourVipCatsCount", hourVipCatsCount);
				map.put("manVipCatsCount", vipCatsCount);
				map.put("manHourVipAutoCatsCount", hourVipAutoCatsCount);				
				map.put("manLarryLast", larryLast);
				map.put("manHourLarryLast", hourLarryLast);				
				map.put("manLastRowMiniCats", lastRowMiniCats);
				map.put("manHourLastRowMiniCats", hourLastRowMiniCats);				
				map.put("manVipAutoCatsCount", vipAutoCatsCount);				
				//map.put("hourManualCount", hourManualCount);
				//map.put("manualCount", manualCount);
				map.put("manHourZoneCount", hourZoneCount);
				map.put("manZoneCount", zoneCount);
				map.put("manHourRecordCount", hourRecordCount);
				map.put("manRecordCount", recordCount);
				map.put("manHourMiniCatsCount", hourMiniCatsCount);
				map.put("manMiniCatsCount", miniCatsCount);
				map.put("manZonesLastRow", zonesLastRowCount);
				map.put("manHourZonesLastRow", hourZonesLastRowCount);
				map.put("manHourZonesCount", hourZonesCount);
				map.put("manZonesCount", zonesCount);
				//TOTAL COUNTS
				map.put("manTotalMinicatsCount", miniCatsCount+vividMiniCatsCount+scoreBigMiniCatsCount+ticketCityMiniCatsCount);
				map.put("manTotalHourMinicatsCount", hourMiniCatsCount+hourVividMiniCatsCount+hourScoreBigMiniCatsCount+hourTicketCityMiniCatsCount);
				
				map.put("manTotalAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				map.put("manTotalHourAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				
				map.put("manTotalLastRowMinicatsCount", lastRowMiniCats+vividLastRowMiniCatsCount+scoreBigLastRowMiniCatsCount+ticketCityLastRowMiniCatsCount+
						fanXchangeLastRowMiniCatsCount);
				map.put("manTotalHourLastRowMinicatsCount", hourLastRowMiniCats+hourVividLastRowMiniCatsCount+hourScoreBigLastRowMiniCatsCount+
						hourTicketCityLastRowMiniCatsCount+hourFanXchangeLastRowMiniCatsCount);
				
				map.put("manTotalVipMiniCatsCount", vipCatsCount + ticketCityVipMiniCatsCount);
				map.put("manTotalHourVipMiniCatsCount", hourVipCatsCount + hourTicketCityVipMiniCatsCount);
				
				map.put("manTotalTicketNetworkCount", autoCount+miniCatsCount+vipCatsCount+vipAutoCatsCount+zoneCount+lastRowMiniCats+larryLast+zonesLastRowCount+zonesCount);
				map.put("manTotalHourTicketNetworkCount", hourAutoCount+hourMiniCatsCount+hourVipCatsCount+hourVipAutoCatsCount+hourZoneCount+hourLastRowMiniCats+hourLarryLast+hourZonesLastRowCount+hourZonesCount);
				
				map.put("manTotalVividSeatsCount", vividMiniCatsCount+vividLastRowMiniCatsCount);
				map.put("manTotalHourVividSeatsCount", hourVividMiniCatsCount+hourVividLastRowMiniCatsCount);
				
				map.put("manTotalScoreBigCount", /*scoreBigAutoCatsCount+*/scoreBigMiniCatsCount+scoreBigLastRowMiniCatsCount);
				map.put("manTotalHourScoreBigCount", /*hourScoreBigAutoCatsCount+*/hourScoreBigMiniCatsCount+hourScoreBigLastRowMiniCatsCount);
				
				map.put("manTotalTicketCityCount", ticketCityMiniCatsCount + ticketCityVipMiniCatsCount + ticketCityLastRowMiniCatsCount);
				map.put("manTotalHourTicketCityCount", hourTicketCityMiniCatsCount + hourTicketCityVipMiniCatsCount + hourTicketCityLastRowMiniCatsCount);
				
				map.put("manTotalFanXchangeCount", fanXchangeLastRowMiniCatsCount);
				map.put("manTotalHourFanXchangeCount", hourFanXchangeLastRowMiniCatsCount);
				
				//mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null);
				return map;
		}catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			subject = "Error while getting Hourly Manhattan Category Sales";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "Manhattan");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;				
		}		
	}

}
