package com.admitone.tmat.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.mail.MailManager;

public class RotCategorySalesReport implements InitializingBean{
	
	static MailManager mailManager = null;
		
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						//categorySalesMail(null, null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	}
	
	// Mehul : to send manual statistics mail from report tab three parameters added on 09/29/2015
	
	public static Map<String, Object> categorySalesMail(Date startDate, Date endDate, String subject)throws Exception{
		
		try{
			Calendar cal = Calendar.getInstance();
			Date hourStartDate = cal.getTime();
			if(startDate == null || endDate == null || subject == null){
				cal.set(Calendar.HOUR_OF_DAY, 00);
				cal.set(Calendar.MINUTE, 00);
				cal.set(Calendar.SECOND, 00);
				startDate = cal.getTime();			
				cal = Calendar.getInstance();
				cal.add(Calendar.HOUR, -1);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				endDate = cal.getTime();
				cal.set(Calendar.MINUTE, 00);
				cal.set(Calendar.SECOND, 00);
				hourStartDate = cal.getTime();
				//subject = "Statistics of ROT Cats and zones ProductLines";
			}	
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getRotCategorySalesReportToday(startDate, endDate);
			/*
			Object[] zonesPricingDummySale = new Object[15];
			Object[] zonesLastRowDummySale = new Object[15];
			Object[] zonesDummySale = new Object[15];
			
			zonesPricingDummySale[0] = (Object)"Zones Pricing Dummy Event";
			zonesPricingDummySale[1] = (Object)"2015-2-20 19:00:00.000";
			zonesPricingDummySale[2] = (Object)"Upper";
			zonesPricingDummySale[3] = (Object)"A";
			zonesPricingDummySale[4] = (Object)"123456";
			zonesPricingDummySale[5] = 12345;
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date zonesSalesDate = dateFormat.parse("01/30/2015 09:42:23");
			Timestamp zonestsp = new Timestamp(zonesSalesDate.getTime());
			zonesPricingDummySale[6] = zonestsp;
			zonesPricingDummySale[7] = (Object)"1234";
			zonesPricingDummySale[8] = (Object)(Object)"220";
			zonesPricingDummySale[9] = (Object)"200";
			zonesPricingDummySale[10] = (Object)"210.20";
			zonesPricingDummySale[11] = (Object)"0.00";
			zonesPricingDummySale[12] = (Object)"ZP";
			zonesPricingDummySale[13] = (Object)"NULL";
			zonesPricingDummySale[14] = (Object)"Web User";
			
			zonesLastRowDummySale[0] = (Object)"Zones Last Row Dummy Event";
			zonesLastRowDummySale[1] = (Object)"2015-2-20 20:00:00.000";
			zonesLastRowDummySale[2] = (Object)"Lower";
			zonesLastRowDummySale[3] = (Object)"B";
			zonesLastRowDummySale[4] = (Object)"159753";
			zonesLastRowDummySale[5] = 54321;
			Date zoneslastrowSalesDate = dateFormat.parse("01/30/2015 09:45:23");
			zonestsp = new Timestamp(zoneslastrowSalesDate.getTime());
			zonesLastRowDummySale[6] = zonestsp;
			zonesLastRowDummySale[7] = (Object)"4321";
			zonesLastRowDummySale[8] = (Object)"330";
			zonesLastRowDummySale[9] = (Object)"300";
			zonesLastRowDummySale[10] = (Object)"310.20";
			zonesLastRowDummySale[11] = (Object)"0.00";
			zonesLastRowDummySale[12] = (Object)"ZLR";
			zonesLastRowDummySale[13] = (Object)"NULL";
			zonesLastRowDummySale[14] = (Object)"Web User";
			
			zonesDummySale[0] = (Object)"Zones Dummy Event";
			zonesDummySale[1] = (Object)"2015-2-20 20:00:00.000";
			zonesDummySale[2] = (Object)"zone";
			zonesDummySale[3] = (Object)"B";
			zonesDummySale[4] = (Object)"654321";
			zonesDummySale[5] = 98765;
			Date zoneSalesDate = dateFormat.parse("01/30/2015 09:48:23");
			zonestsp = new Timestamp(zoneSalesDate.getTime());
			zonesDummySale[6] = zonestsp;
			zonesDummySale[7] = (Object)"4321";
			zonesDummySale[8] = (Object)"330";
			zonesDummySale[9] = (Object)"300";
			zonesDummySale[10] = (Object)"310.20";
			zonesDummySale[11] = (Object)"0.00";
			zonesDummySale[12] = (Object)"hello";
			zonesDummySale[13] = (Object)"NULL";
			zonesDummySale[14] = (Object)"Web User";
			
			list.add(zonesPricingDummySale);
			list.add(zonesLastRowDummySale);
			list.add(zonesDummySale);
			*/
			
				int recordCount=0,hourRecordCount=0;
				boolean zoneFlag = false,flag = true;
				int autoCount=0,manualCount=0,zoneCount = 0,hourAutoCount=0,hourManualCount=0,
				hourZoneCount = 0,hourMiniCatsCount = 0,miniCatsCount = 0,vipCatsCount = 0,hourVipCatsCount = 0,
				hourVipAutoCatsCount = 0,vipAutoCatsCount = 0,lastRowMiniCats = 0,hourLastRowMiniCats = 0,sgLastFiveRow = 0,hourSGLastFiveRow = 0,
				larryLast = 0,hourLarryLast = 0,hourVividMiniCatsCount = 0,vividMiniCatsCount = 0,hourVividLarryCount = 0,
				vividLarryCount = 0,hourScoreBigMiniCatsCount = 0,scoreBigMiniCatsCount = 0,
				/*hourScoreBigAutoCatsCount = 0,scoreBigAutoCatsCount = 0,*/
				hourScoreBigLastRowMiniCatsCount=0,scoreBigLastRowMiniCatsCount=0,
				hourZonesLastRowCount=0,zonesLastRowCount=0,
				hourZonesCount=0,zonesCount=0, ticketCityMiniCatsCount=0,hourTicketCityMiniCatsCount=0, ticketCityVipMiniCatsCount=0, hourTicketCityVipMiniCatsCount=0,
				ticketCityLastRowMiniCatsCount=0,hourTicketCityLastRowMiniCatsCount=0, fanXchangeLastRowMiniCatsCount=0,hourFanXchangeLastRowMiniCatsCount=0;
				
				ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
				
				if(list != null && !list.isEmpty()){
				for(Object[] obj : list){
					
					Integer invoiceId = (Integer) obj[5];
					if(invoiceNo.contains(invoiceId))
						continue;
					
					invoiceNo.add(invoiceId);
						
					recordCount += 1;
					String catType = (String) obj[12];
					String type = (String) obj[13];
					String ebayType = (String) obj[14];
					
					if(catType != null && !catType.isEmpty()){
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
							catType = "VIVID LARRY";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
							catType = "SCOREBIG MINICATS";
						}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
							catType = "SCOREBIG AUTO";
						}*/
						else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("AUTOCAT")){
							catType = "TN AUTO";								
						}else if(catType.equalsIgnoreCase("MINICATS") || catType.equalsIgnoreCase("ZMINICATS")){
							catType = "TN MINICATS";
						}else if(catType.equalsIgnoreCase("VIPMINICATS") || catType.equalsIgnoreCase("ZVIPMINICATS")){
							catType = "TN VIP MINICATS";
						}else if(catType.equalsIgnoreCase("VIPAUTO")){
							catType = "TN VIP AUTOCATS";
						}else if(catType.equalsIgnoreCase("LASTROW MINICATS") || catType.equalsIgnoreCase("ZLASTROW MINICATS")){
							catType = "TN LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("SGK")){
							catType = "TN SGK LASTFIVEROW";
						}else if(catType.equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "TN LARRYLAST";
						}else if(catType.equalsIgnoreCase("SCZP")){
							catType = "ZONES PRICING";
						}else if(catType.equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "ZONED LASTROW MINICATS";
						}else{
							catType = "MANUAL";
						}
					}else{
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividCats@reserveonetickets")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("VividSpecs@reserveonetickets.com")){
							catType = "VIVID LARRY";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBigLarry@Reserveone")){
							catType = "SCOREBIG MINICATS";
						}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
							catType = "SCOREBIG AUTO";
						}*/
						else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("ScoreBig2@Reserveone")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else{
							catType = "MANUAL";
						}							
					}
					Date createDate = new Date(((Timestamp) obj[6]).getTime());
					if(obj[2] != null && !obj[2].equals("")){
						String section = String.valueOf(obj[2]).toLowerCase();
						if(section.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}
					
					if(obj[3] != null && !obj[3].equals("")){
						String row = String.valueOf(obj[3]).toLowerCase();
						if(row.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
					}			
					
					if(obj[10] != null && !obj[10].equals("")){	
							if(catType.equalsIgnoreCase("TN AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourAutoCount += 1;
									hourRecordCount += 1;
								}
									autoCount += 1;
							}else if(catType.equalsIgnoreCase("TN MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourMiniCatsCount += 1;
								}
									miniCatsCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipAutoCatsCount += 1;
								}
									vipAutoCatsCount += 1;
							}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVipCatsCount += 1;
								}
									vipCatsCount += 1;
							}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLastRowMiniCats += 1;
								}
									lastRowMiniCats += 1;
							}//else if(catType.equalsIgnoreCase("TN SGK LASTFIVEROW")){
							//	if(createDate.after(hourStartDate) && createDate.before(endDate)){
							//		hourRecordCount += 1;
							//		hourSGLastFiveRow += 1;
							//	}
							//		sgLastFiveRow += 1;
							//}
							else if(catType.equalsIgnoreCase("TN LARRYLAST")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourLarryLast += 1;
								}
									larryLast += 1;
							}else if(zoneFlag){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesCount += 1;
								}
									zonesCount += 1;
							}else if(catType.equalsIgnoreCase("ZONES PRICING")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZoneCount += 1;
								}
									zoneCount += 1;
							}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourZonesLastRowCount += 1;
								}
									zonesLastRowCount +=1 ;
							}else if(catType.equalsIgnoreCase("VIVID MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividMiniCatsCount += 1;
								}
									vividMiniCatsCount += 1;
							}else if(catType.equalsIgnoreCase("VIVID LARRY")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourVividLarryCount += 1;
								}
									vividLarryCount += 1;
							}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigMiniCatsCount += 1;
								}
									scoreBigMiniCatsCount += 1;
							}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigAutoCatsCount += 1;
								}
									scoreBigAutoCatsCount += 1;
							}*/
							else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourScoreBigLastRowMiniCatsCount += 1;
								}
									scoreBigLastRowMiniCatsCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityMiniCatsCount += 1;
								}
									ticketCityMiniCatsCount +=1 ;
									//recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityVipMiniCatsCount += 1;
								}
									ticketCityVipMiniCatsCount +=1 ;
									//recordCount += 1;
							}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourTicketCityLastRowMiniCatsCount+= 1;
								}
									ticketCityLastRowMiniCatsCount +=1 ;
									//recordCount += 1;
							}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourFanXchangeLastRowMiniCatsCount+= 1;
								}
									fanXchangeLastRowMiniCatsCount +=1 ;
									//recordCount += 1;
							}else{
								if(createDate.after(hourStartDate) && createDate.before(endDate)){
									hourRecordCount += 1;
									hourManualCount += 1;
								}
									manualCount += 1;
							}						
					}
					
					zoneFlag = false;
				}
				}
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com";
				String bccAddress = "AODev@rightthisway.com";
				//String bccAddress = "AODev@rightthisway.com";
				//String toAddress ="pmehul@rightthisway.com";
				//String bccAddress = "tselvan@rightthisway.com";
				
				String template = "rot-mail-category-sales.html";
				Map<String, Object> map = new HashMap<String, Object>();
				//VIVID COUNTS
				map.put("rotHourVividMiniCatsCount", hourVividMiniCatsCount);
				map.put("rotVividMiniCatsCount", vividMiniCatsCount);
				map.put("rotHourVividLarryCount", hourVividLarryCount);
				map.put("rotVividLarryCount", vividLarryCount);
				//SCOREBIG COUNT
				map.put("rotHourScoreMiniCatsCount", hourScoreBigMiniCatsCount);
				map.put("rotScoreMiniCatsCount", scoreBigMiniCatsCount);
				/*map.put("hourScoreBigAutoCatsCount", hourScoreBigAutoCatsCount);
				map.put("scoreBigAutoCatsCount", scoreBigAutoCatsCount);*/
				map.put("rotHourScoreBigLastRowMiniCatsCount", hourScoreBigLastRowMiniCatsCount);
				map.put("rotScoreBigLastRowMiniCatsCount", scoreBigLastRowMiniCatsCount);
				// Ticket City Count
				map.put("rotTicketCityMiniCatsCount", ticketCityMiniCatsCount);
				map.put("rotHourTicketCityMiniCatsCount", hourTicketCityMiniCatsCount);
				map.put("rotTicketCityVipMiniCatsCount", ticketCityVipMiniCatsCount);
				map.put("rotHourTicketCityVipMiniCatsCount", hourTicketCityVipMiniCatsCount);
				map.put("rotTicketCityLastRowMiniCatsCount", ticketCityLastRowMiniCatsCount);
				map.put("rotHourTicketCityLastRowMiniCatsCount", hourTicketCityLastRowMiniCatsCount);
				// Fanxchange Count
				map.put("rotFanXchangeLastRowMiniCatsCount", fanXchangeLastRowMiniCatsCount);
				map.put("rotHourFanXchangeLastRowMiniCatsCount", hourFanXchangeLastRowMiniCatsCount);
				//TN COUNTS
				map.put("rotHourAutoCount", hourAutoCount);
				map.put("rotAutoCount", autoCount);
				map.put("rotHourVipCatsCount", hourVipCatsCount);
				map.put("rotVipCatsCount", vipCatsCount);
				map.put("rotHourVipAutoCatsCount", hourVipAutoCatsCount);				
				map.put("rotLarryLast", larryLast);
				map.put("rotHourLarryLast", hourLarryLast);				
				map.put("rotLastRowMiniCats", lastRowMiniCats);
				map.put("rotHourLastRowMiniCats", hourLastRowMiniCats);
				map.put("rotSGLastFiveRow", sgLastFiveRow);
				map.put("rotHourSGLastFiveRow", hourSGLastFiveRow);
				map.put("rotVipAutoCatsCount", vipAutoCatsCount);				
				map.put("rotHourManualCount", hourManualCount);
				map.put("rotManualCount", manualCount);
				map.put("rotHourZoneCount", hourZoneCount);
				map.put("rotZoneCount", zoneCount);
				map.put("rotHourRecordCount", hourRecordCount);
				map.put("rotRecordCount", recordCount);
				map.put("rotHourMiniCatsCount", hourMiniCatsCount);
				map.put("rotMiniCatsCount", miniCatsCount);
				map.put("rotZonesLastRow", zonesLastRowCount);
				map.put("rotHourZonesLastRow", hourZonesLastRowCount);
				map.put("rotHourZonesCount", hourZonesCount);
				map.put("rotZonesCount", zonesCount);
				//TOTAL COUNTS
				map.put("rotTotalMinicatsCount", miniCatsCount+vividMiniCatsCount+scoreBigMiniCatsCount+ticketCityMiniCatsCount);
				map.put("rotTotalHourMinicatsCount", hourMiniCatsCount+hourVividMiniCatsCount+hourScoreBigMiniCatsCount+hourTicketCityMiniCatsCount);
				
				map.put("rotTotalAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				map.put("rotTotalHourAutocatsCount", hourAutoCount/*+scoreBigAutoCatsCount*/);
				
				map.put("rotTotalLastRowMinicatsCount", lastRowMiniCats+scoreBigLastRowMiniCatsCount+ticketCityLastRowMiniCatsCount+
						fanXchangeLastRowMiniCatsCount);
				map.put("rotTotalHourLastRowMinicatsCount", hourLastRowMiniCats+hourScoreBigLastRowMiniCatsCount+
						hourTicketCityLastRowMiniCatsCount+hourFanXchangeLastRowMiniCatsCount);
				
				map.put("rotTotalHourVipCatsCount", hourVipCatsCount + hourTicketCityVipMiniCatsCount);
				map.put("rotTotalVipCatsCount", vipCatsCount + ticketCityVipMiniCatsCount);
				
				map.put("rotTotalHourSGLastFiveRowCount", hourSGLastFiveRow);
				map.put("rotTotalSGLastFiveRowCount", sgLastFiveRow);
				
				map.put("rotTotalHourLarryCount", hourLarryLast+hourVividLarryCount);
				map.put("rotTotalLarryCount", larryLast+vividLarryCount);
				
				map.put("rotTotalTicketNetworkCount", autoCount+miniCatsCount+vipCatsCount+vipAutoCatsCount+zoneCount+manualCount+lastRowMiniCats+larryLast+zonesLastRowCount+zonesCount);
				map.put("rotTotalHourTicketNetworkCount", hourAutoCount+hourMiniCatsCount+hourVipCatsCount+hourVipAutoCatsCount+hourZoneCount+hourManualCount+hourLastRowMiniCats+hourLarryLast+hourZonesLastRowCount+hourZonesCount);
				
				map.put("rotTotalVividSeatsCount", vividMiniCatsCount+vividLarryCount);
				map.put("rotTotalHourVividSeatsCount", hourVividMiniCatsCount+hourVividLarryCount);
				
				map.put("rotTotalScoreBigCount", /*scoreBigAutoCatsCount+*/scoreBigMiniCatsCount+scoreBigLastRowMiniCatsCount);
				map.put("rotTotalHourScoreBigCount", /*hourScoreBigAutoCatsCount+*/hourScoreBigMiniCatsCount+hourScoreBigLastRowMiniCatsCount);
				
				map.put("rotTotalTicketCityCount", ticketCityMiniCatsCount + ticketCityVipMiniCatsCount + ticketCityLastRowMiniCatsCount);
				map.put("rotTotalHourTicketCityCount", hourTicketCityMiniCatsCount + hourTicketCityVipMiniCatsCount + hourTicketCityLastRowMiniCatsCount);
				
				map.put("rotTotalFanXchangeCount", fanXchangeLastRowMiniCatsCount);
				map.put("rotTotalHourFanXchangeCount", hourFanXchangeLastRowMiniCatsCount);
				
				map.put("rotTotalSeatGeekCount", sgLastFiveRow);
				map.put("rotTotalHourSeatGeekCount", hourSGLastFiveRow);
				
				//mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null);
				return map;
			
		}catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			subject = "Error while getting Hourly RTW-2 Category Sales";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "RTW-2");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;
		}		
	}

}
