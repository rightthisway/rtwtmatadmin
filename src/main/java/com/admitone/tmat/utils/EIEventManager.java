package com.admitone.tmat.utils;

import java.util.Collection;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EIInstantEvent;
import com.admitone.tmat.data.EIInstantTour;
import com.admitone.tmat.data.InstantEvent;
import com.admitone.tmat.data.InstantTour;
import com.admitone.tmat.data.Property;


public class EIEventManager {
//	private static Double defaultMarkupPercent = 25.00;
//	private static Integer defaultExpiryTime = 60;
//	private static Double defaultMinThreshold = 100.00;
//	private static Double defaultMaxThreshold = 20000.00;
//	private static Double defaultSalesPercent = 10.00;
//	private static Double defaultShippingFee = 10.00;

	/*public static void saveEIInstantEvents(Collection<Integer> eventIds){
			
		EIInstantTour instantTour = null;
		EIInstantEvent instantEvent = null;
		
		Integer defaultExpiryTime = 0;
		Double defaultMarkupPercent = 0.0;
		Double defaultSalesPercent=0.0;
		Double defaultShippingFee=0.0;
		Double defaultMinThreshold=0.0;
		Double defaultMaxThreshold=0.0;
		
			for (Integer eventId : eventIds) {
				Event event = DAORegistry.getEventDAO().get(eventId);
				if (event == null) {
					continue;
				}

				instantEvent = DAORegistry.getEiinstantEventDAO().get(eventId);
				if (instantEvent == null) {
					instantEvent = new EIInstantEvent(eventId);
					
					Property eiExpiryProperty = DAORegistry.getPropertyDAO().get("ei.expiry");
					String eiExpiryStr = eiExpiryProperty.getValue();
					try {
						defaultExpiryTime = Integer.valueOf(eiExpiryStr);
					}catch(NumberFormatException nfe){}
					
					Property eiMarkupProperty = DAORegistry.getPropertyDAO().get("ei.markup.percent");
					String eiMarkupStr = eiMarkupProperty.getValue();
					try{
						defaultMarkupPercent = Double.valueOf(eiMarkupStr);
					}catch(NumberFormatException nfe){}
					
					Property eiSalesProperty = DAORegistry.getPropertyDAO().get("ei.sales");
					String eiSalesStr = eiSalesProperty.getValue();
					try{
						defaultSalesPercent = Double.valueOf(eiSalesStr);
					}catch(NumberFormatException nfe){}
					
					Property eiShippingProperty = DAORegistry.getPropertyDAO().get("ei.shipping");
					String eiShippingStr = eiShippingProperty.getValue();
					try{
						defaultShippingFee = Double.valueOf(eiShippingStr);
					}catch(NumberFormatException nfe){}
					
					Property eiMinThresholdProperty = DAORegistry.getPropertyDAO().get("ei.minthreshold");
					String eiMinThresholdStr = eiMinThresholdProperty.getValue();
					try{
						defaultMinThreshold = Double.valueOf(eiMinThresholdStr);
					}catch(NumberFormatException nfe){}
					
					Property eiMaxThresholdProperty = DAORegistry.getPropertyDAO().get("ei.maxthreshold");
					String eiMaxThresholdStr = eiMaxThresholdProperty.getValue();
					try{
						defaultMaxThreshold = Double.valueOf(eiMaxThresholdStr);
					}catch(NumberFormatException nfe){}
					
					
					instantEvent.setMarkupPercent(defaultMarkupPercent);
					instantEvent.setExpiryTime(defaultExpiryTime);
					instantEvent.setMinThreshold(defaultMinThreshold);
					instantEvent.setMaxThreshold(defaultMaxThreshold);
					instantEvent.setSalesPercent(defaultSalesPercent);
					instantEvent.setShippingFee(defaultShippingFee);
				//	instantEvent.setTourId(event.getTourId());
					
					instantTour = DAORegistry.getEiinstantTourDAO().get(event.getTourId());
					if (instantTour == null) {
						instantTour = new EIInstantTour(event.getTourId());
						instantTour.setMarkupPercent(defaultMarkupPercent);
						instantTour.setExpiryTime(defaultExpiryTime);
						instantTour.setMinThreshold(defaultMinThreshold);
						instantTour.setMaxThreshold(defaultMaxThreshold);
						instantTour.setSalesPercent(defaultSalesPercent);
						instantTour.setShippingFee(defaultShippingFee);
						DAORegistry.getEiinstantTourDAO().save(instantTour);
					} else {
						instantEvent.setExpiryTime(instantTour.getExpiryTime());
						instantEvent.setMinThreshold(instantTour.getMinThreshold());
						instantEvent.setMaxThreshold(instantTour.getMaxThreshold());
						instantEvent.setMarkupPercent(instantTour.getMarkupPercent());
						instantEvent.setSalesPercent(instantTour.getSalesPercent());
						instantEvent.setShippingFee(instantTour.getShippingFee());
					}
					
					DAORegistry.getEiinstantEventDAO().save(instantEvent);

				} else {
					continue;
				}
			//	getInstantTicketsInventory(eventId);
			}
		}


	public static void deleteInstantEvents(Integer eventId){
		Event event= DAORegistry.getEventDAO().get(eventId);
		Integer tourId = event.getTourId();
		DAORegistry.getEiinstantEventDAO().deleteById(eventId);
		int eventCount = DAORegistry.getEiinstantEventDAO().getInstantEventCountByTour(tourId);
		if (eventCount ==0){
			DAORegistry.getEiinstantTourDAO().deleteById(tourId);
		}
		}
	
	
	public static void deleteInstantTour(Integer tourId){
		Collection<EIInstantEvent> ievents = DAORegistry.getEiinstantEventDAO().getInstantEventsByTour(tourId);
		for (EIInstantEvent ievent:ievents){
			DAORegistry.getEiinstantEventDAO().deleteById(ievent.getEventId());
		}
		int eventCount = DAORegistry.getEiinstantEventDAO().getInstantEventCountByTour(tourId);
		
		if (eventCount ==0){
			DAORegistry.getEiinstantTourDAO().deleteById(tourId);
		}
	}
	
	
	public static void updateInstantEventsandTours(){
		System.out.println("Updateing iEvents and iTours");
		Integer defaultExpiryTime = 0;
		Double defaultMarkupPercent = 0.0;
		Double defaultSalesPercent=0.0;
		Double defaultShippingFee=0.0;
		Double defaultMinThreshold=0.0;
		Double defaultMaxThreshold=0.0;
		
		Collection<EIInstantEvent> iEvents = DAORegistry.getEiinstantEventDAO().getAll();
		Collection<EIInstantTour> iTours = DAORegistry.getEiinstantTourDAO().getAll();
		
		Property eiExpiryProperty = DAORegistry.getPropertyDAO().get("ei.expiry");
		String eiExpiryStr = eiExpiryProperty.getValue();
		try {
			defaultExpiryTime = Integer.valueOf(eiExpiryStr);
		}catch(NumberFormatException nfe){}
		
		Property eiMarkupProperty = DAORegistry.getPropertyDAO().get("ei.markup.percent");
		String eiMarkupStr = eiMarkupProperty.getValue();
		try{
			defaultMarkupPercent = Double.valueOf(eiMarkupStr);
		}catch(NumberFormatException nfe){}
		
		Property eiSalesProperty = DAORegistry.getPropertyDAO().get("ei.sales");
		String eiSalesStr = eiSalesProperty.getValue();
		try{
			defaultSalesPercent = Double.valueOf(eiSalesStr);
		}catch(NumberFormatException nfe){}
		
		Property eiShippingProperty = DAORegistry.getPropertyDAO().get("ei.shipping");
		String eiShippingStr = eiShippingProperty.getValue();
		try{
			defaultShippingFee = Double.valueOf(eiShippingStr);
		}catch(NumberFormatException nfe){}
		
		Property eiMinThresholdProperty = DAORegistry.getPropertyDAO().get("ei.minthreshold");
		String eiMinThresholdStr = eiMinThresholdProperty.getValue();
		try{
			defaultMinThreshold = Double.valueOf(eiMinThresholdStr);
		}catch(NumberFormatException nfe){}
		
		Property eiMaxThresholdProperty = DAORegistry.getPropertyDAO().get("ei.maxthreshold");
		String eiMaxThresholdStr = eiMaxThresholdProperty.getValue();
		try{
			defaultMaxThreshold = Double.valueOf(eiMaxThresholdStr);
		}catch(NumberFormatException nfe){}
		
		for(EIInstantEvent instantEvent:iEvents){
		instantEvent.setMarkupPercent(defaultMarkupPercent);
		instantEvent.setExpiryTime(defaultExpiryTime);
		instantEvent.setMinThreshold(defaultMinThreshold);
		instantEvent.setMaxThreshold(defaultMaxThreshold);
		instantEvent.setSalesPercent(defaultSalesPercent);
		instantEvent.setShippingFee(defaultShippingFee);
		}
		DAORegistry.getEiinstantEventDAO().saveOrUpdateAll(iEvents);
	
		
		for(EIInstantTour instantTour: iTours){
		instantTour.setMarkupPercent(defaultMarkupPercent);
		instantTour.setExpiryTime(defaultExpiryTime);
		instantTour.setMinThreshold(defaultMinThreshold);
		instantTour.setMaxThreshold(defaultMaxThreshold);
		instantTour.setSalesPercent(defaultSalesPercent);
		instantTour.setShippingFee(defaultShippingFee);
		}
		DAORegistry.getEiinstantTourDAO().saveOrUpdateAll(iTours);
	}*/
}
