package com.admitone.tmat.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.pojo.CategorySalesDetails;
import com.admitone.tmat.utils.mail.MailManager;

import flex.messaging.io.ArrayList;

// Mehul : All in one statistics mail.

public class AllInOneCategorySalesReport implements InitializingBean{
	
	static MailManager mailManager = null;
	
	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		/*Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						categorySalesMail(null, null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
*/	}
	
	// Mehul : removed jkt sales from report on 11/20/2015
	// Mehul : manhattan commented on 02/08/2016
	public static void categorySalesMail(Date startDate, Date endDate, String subject){
		try{
		//Map<String, Object> rotMap = new HashMap<String, Object>();
		Map<String, Object> rtwMap = new HashMap<String, Object>();
		//Map<String, Object> tixcityMap = new HashMap<String, Object>();
		//Map<String, Object> tixCityNewMap = new HashMap<String, Object>();
		//Map<String, Object> manhattanMap = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		int totalTn = 0,totalHourTn = 0, totalVivid = 0, totalHourVivid = 0, totalScoreBig = 0, totalHourScoreBig = 0, totalFanXchange = 0, totalHourFanXchange = 0,
				totalTicketCity = 0, totalHourTicketCity = 0,totalSeatGeek = 0,totalHourSeatGeek = 0;
		List<CategorySalesDetails> rtwCatSalesList = new java.util.ArrayList<CategorySalesDetails>();
		//List<CategorySalesDetails> tixcityCatSalesList = new java.util.ArrayList<CategorySalesDetails>();
		
		/*try{
			rotMap = RotCategorySalesReport.categorySalesMail(startDate, endDate, subject);
			rotMap.keySet().removeAll(map.keySet());
			map.putAll(rotMap);
			map.put("showRotSales", "block");
			map.put("showRotError", "none");
			
			totalTn += Integer.parseInt("" + map.get("rotTotalTicketNetworkCount"));
			totalHourTn += Integer.parseInt("" + map.get("rotTotalHourTicketNetworkCount"));
			totalVivid += Integer.parseInt("" + map.get("rotTotalVividSeatsCount"));
			totalHourVivid += Integer.parseInt("" + map.get("rotTotalHourVividSeatsCount"));
			totalScoreBig += Integer.parseInt("" + map.get("rotTotalScoreBigCount"));
			totalHourScoreBig += Integer.parseInt("" + map.get("rotTotalHourScoreBigCount"));
			totalTicketCity+= Integer.parseInt("" + map.get("rotTotalTicketCityCount"));
			totalHourTicketCity += Integer.parseInt("" + map.get("rotTotalHourTicketCityCount"));
			totalFanXchange+= Integer.parseInt("" + map.get("rotTotalFanXchangeCount"));
			totalHourFanXchange += Integer.parseInt("" + map.get("rotTotalHourFanXchangeCount"));
			totalSeatGeek+= Integer.parseInt("" + map.get("rotTotalSeatGeekCount"));
			totalHourSeatGeek += Integer.parseInt("" + map.get("rotTotalHourSeatGeekCount"));
		}catch(Exception e){
			e.printStackTrace();
			map.put("showRotSales", "none");
			map.put("showRotError", "block");
			map.put("rotError", e);
		}*/
		
		try{
			rtwMap = RtwCategorySalesReport.categorySalesMail(startDate, endDate, subject);
			rtwMap.keySet().removeAll(map.keySet());
			map.putAll(rtwMap);
			map.put("showRtwSales", "block");
			map.put("showRtwError", "none");
			
			totalTn += Integer.parseInt("" + map.get("rtwTotalTicketNetworkCount"));
			totalHourTn += Integer.parseInt("" + map.get("rtwTotalHourTicketNetworkCount"));
			totalVivid += Integer.parseInt("" + map.get("rtwTotalVividSeatsCount"));
			totalHourVivid += Integer.parseInt("" + map.get("rtwTotalHourVividSeatsCount"));
			totalScoreBig += Integer.parseInt("" + map.get("rtwTotalScoreBigCount"));
			totalHourScoreBig += Integer.parseInt("" + map.get("rtwTotalHourScoreBigCount"));
			totalTicketCity+= Integer.parseInt("" + map.get("rtwTotalTicketCityCount"));
			totalHourTicketCity += Integer.parseInt("" + map.get("rtwTotalHourTicketCityCount"));
			totalFanXchange+= Integer.parseInt("" + map.get("rtwTotalFanXchangeCount"));
			totalHourFanXchange += Integer.parseInt("" + map.get("rtwTotalHourFanXchangeCount"));
			totalSeatGeek+= Integer.parseInt("" + map.get("rtwTotalSeatGeekCount"));
			totalHourSeatGeek += Integer.parseInt("" + map.get("rtwTotalHourSeatGeekCount"));
			rtwCatSalesList = (List<CategorySalesDetails>)map.get("rtwCatSalesList");
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("showRtwSales", "none");
			map.put("showRtwError", "block");
			map.put("rtwError", e);
		}
		/*try{
			tixcityMap = TixcityCategorySalesReport.categorySalesMail(startDate, endDate, subject);
			tixcityMap.keySet().removeAll(map.keySet());
			map.putAll(tixcityMap);
			map.put("showTixcitySales", "block");
			map.put("showTixcityError", "none");
			
			totalTn += Integer.parseInt("" + map.get("tixcityTotalTicketNetworkCount"));
			totalHourTn += Integer.parseInt("" + map.get("tixcityTotalHourTicketNetworkCount"));
			totalVivid += Integer.parseInt("" + map.get("tixcityTotalVividSeatsCount"));
			totalHourVivid += Integer.parseInt("" + map.get("tixcityTotalHourVividSeatsCount"));
			totalScoreBig += Integer.parseInt("" + map.get("tixcityTotalScoreBigCount"));
			totalHourScoreBig += Integer.parseInt("" + map.get("tixcityTotalHourScoreBigCount"));
			totalTicketCity+= Integer.parseInt("" + map.get("tixcityTotalTicketCityCount"));
			totalHourTicketCity += Integer.parseInt("" + map.get("tixcityTotalHourTicketCityCount"));
			totalFanXchange+= Integer.parseInt("" + map.get("tixcityTotalFanXchangeCount"));
			totalHourFanXchange += Integer.parseInt("" + map.get("tixcityTotalHourFanXchangeCount"));
			totalSeatGeek+= Integer.parseInt("" + map.get("tixcityTotalSeatGeekCount"));
			totalHourSeatGeek += Integer.parseInt("" + map.get("tixcityTotalHourSeatGeekCount"));
			tixcityCatSalesList = (List<CategorySalesDetails>)map.get("tixcityCatSalesList");
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("showTixcitySales", "none");
			map.put("showTixcityError", "block");
			map.put("tixcityError", e);
		}*/
		/*
		 * commented as tix city broker has been removed - Mehul - 11/05/2015
		try{
			tixCityNewMap = TixCityNewCategorySalesReport.categorySalesMail(startDate, endDate, subject);
			tixCityNewMap.keySet().removeAll(map.keySet());
			map.putAll(tixCityNewMap);
			map.put("showTixCityNewSales", "block");
			map.put("showTixCityNewError", "none");
			
			totalTn += Integer.parseInt("" + map.get("tixCityNewTotalTicketNetworkCount"));
			totalHourTn += Integer.parseInt("" + map.get("tixCityNewTotalHourTicketNetworkCount"));
			totalVivid += Integer.parseInt("" + map.get("tixCityNewTotalVividSeatsCount"));
			totalHourVivid += Integer.parseInt("" + map.get("tixCityNewTotalHourVividSeatsCount"));
			totalScoreBig += Integer.parseInt("" + map.get("tixCityNewTotalScoreBigCount"));
			totalHourScoreBig += Integer.parseInt("" + map.get("tixCityNewTotalHourScoreBigCount"));
			totalTicketCity+= Integer.parseInt("" + map.get("tixCityNewTotalTicketCityCount"));
			totalHourTicketCity += Integer.parseInt("" + map.get("tixCityNewTotalHourTicketCityCount"));
			totalFanXchange+= Integer.parseInt("" + map.get("tixCityNewTotalFanXchangeCount"));
			totalHourFanXchange += Integer.parseInt("" + map.get("tixCityNewTotalHourFanXchangeCount"));
		}catch(Exception e){
			e.printStackTrace();
			map.put("showTixCityNewSales", "none");
			map.put("showTixCityNewError", "block");
			map.put("tixCityNewError", e);
		}*/
		// Mehul : manhattan commented on 02/08/2016
		/*
		try{
			manhattanMap = ManhattanCategorySalesReport.categorySalesMail(startDate, endDate, subject);
			manhattanMap.keySet().removeAll(map.keySet());
			map.putAll(manhattanMap);
			map.put("showManhattanSales", "block");
			map.put("showManhattanError", "none");
			
			totalTn += Integer.parseInt("" + map.get("manTotalTicketNetworkCount"));
			totalHourTn += Integer.parseInt("" + map.get("manTotalHourTicketNetworkCount"));
			totalVivid += Integer.parseInt("" + map.get("manTotalVividSeatsCount"));
			totalHourVivid += Integer.parseInt("" + map.get("manTotalHourVividSeatsCount"));
			totalScoreBig += Integer.parseInt("" + map.get("manTotalScoreBigCount"));
			totalHourScoreBig += Integer.parseInt("" + map.get("manTotalHourScoreBigCount"));
			totalTicketCity+= Integer.parseInt("" + map.get("manTotalTicketCityCount"));
			totalHourTicketCity += Integer.parseInt("" + map.get("manTotalHourTicketCityCount"));
			totalFanXchange+= Integer.parseInt("" + map.get("manTotalFanXchangeCount"));
			totalHourFanXchange += Integer.parseInt("" + map.get("manTotalHourFanXchangeCount"));
		}catch(Exception e){
			e.printStackTrace();
			map.put("showManhattanSales", "none");
			map.put("showManhattanError", "block");
			map.put("manhattanError", e);
		}*/
		// total sales till now for today
		map.put("totalOfAll", totalTn + totalVivid + totalScoreBig + totalTicketCity + totalFanXchange + totalSeatGeek);
		map.put("totalHourOfAll", totalHourTn + totalHourVivid + totalHourScoreBig + totalHourTicketCity + totalHourFanXchange + totalHourSeatGeek);
		
		// total for all exchanges
		map.put("totalTn", totalTn);
		map.put("totalHourTn", totalHourTn);
		map.put("totalVivid", totalVivid);
		map.put("totalHourVivid", totalHourVivid);
		map.put("totalScoreBig", totalScoreBig);
		map.put("totalHourScoreBig", totalHourScoreBig);
		map.put("totalTicketCity", totalTicketCity);
		map.put("totalHourTicketCity", totalHourTicketCity);
		map.put("totalFanXchange", totalFanXchange);
		map.put("totalHourFanXchange", totalHourFanXchange);
		map.put("totalSeatGeek", totalFanXchange);
		map.put("totalHourSeatGeek", totalHourFanXchange);
		map.put("totalSeatGeek", totalSeatGeek);
		map.put("totalHourSeatGeek", totalHourSeatGeek);
		map.put("rtwCatSalesList", rtwCatSalesList);
		//map.put("tixcityCatSalesList", tixcityCatSalesList);
		
		String fromName  = "tmatmanagers@rightthisway.com";
		String mimeType = "text/html";
		String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com";
				//Tamil : commented on 09/28/2017
				//",bmanusama@rightthisway.com,jreardon@rightthisway.com,TMAT02@rightthisway.com,tmat04@rightthisway.com";
		String bccAddress = "AODev@rightthisway.com";
		//String toAddress ="tselvan@rightthisway.com";
		//String bccAddress = null;
		String template = "all-in-one-mail-category-sales.html";
		if(subject == null){
			subject = "Statistics of Cats and zones ProductLines";
		}
		
		mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null);
		}catch(Exception e){
			e.printStackTrace();
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			subject = "Hourly Category Sales Mail Failed";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "All");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
		}
	}
}
