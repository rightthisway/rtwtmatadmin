package com.admitone.tmat.utils;

import java.net.InetAddress;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.com.TicketListingCrawlReceiver;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;

public class StopCrawlerOnHost implements InitializingBean {
	private TicketListingCrawlReceiver ticketListingCrawlReceiver;

	public void afterPropertiesSet() throws Exception {
		if (ticketListingCrawlReceiver == null) {
			throw new Exception("The property ticketListingCrawlReceiver must be set");
		}
		
		String hostname = "server";
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			hostname = localMachine.getHostName();
			ticketListingCrawlReceiver.sendMessage(new TicketListingCrawlerMessage(hostname, TicketListingCrawlerMessage.MSG_QUIT));
		} catch (java.net.UnknownHostException uhe) {
			uhe.printStackTrace();
		}
		
		System.exit(0);
	}

	public TicketListingCrawlReceiver getTicketListingCrawlReceiver() {
		return ticketListingCrawlReceiver;
	}

	public void setTicketListingCrawlReceiver(
			TicketListingCrawlReceiver ticketListingCrawlReceiver) {
		this.ticketListingCrawlReceiver = ticketListingCrawlReceiver;
	}
}
