package com.admitone.tmat.utils.eimarketplace;

import com.admitone.tmat.dao.DAORegistry;

public class EIMPDBCredential implements EIMPCredential {

	public String getUsername() {
		return DAORegistry.getPropertyDAO().get("eimarketplace.username").getValue();
	}

	public String getPassword() {
		return DAORegistry.getPropertyDAO().get("eimarketplace.password").getValue();
	}

}
