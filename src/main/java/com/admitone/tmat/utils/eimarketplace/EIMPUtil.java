package com.admitone.tmat.utils.eimarketplace;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie2;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.NaiveTrustManager;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class EIMPUtil {

	private static Date lastEimpLogin = null;
	private static long EIMP_LOGIN_TIMEOUT = 600000; // 10 mins
	private static long EIMP_CACHE_TIMEOUT = 3600000L; // 1 hour
	private static DefaultHttpClient eimpHttpClient = null;
//	private static Pattern eimpMapLinkPattern = Pattern.compile("href=\"(.*?)\"[^<]+<img src=\"/graphics/map.gif");
	private static Pattern eimpMapLinkPattern = Pattern.compile("href=\"/MarketPlace/maps/(.*?)\"[^<]+<img src=\"(.*?)/graphics/map.gif");
	
	// map eventId => cacheEntry 
	private static Map<Integer, CacheEntry> chartCache = new HashMap<Integer, CacheEntry>();
	
	// map eventId-seller => cacheEntry
	private static Map<String, CacheEntry> sellerCache = new HashMap<String, CacheEntry>();
	
	public static boolean login(DefaultHttpClient httpClient) throws Exception {
		String login = DAORegistry.getPropertyDAO().get("eimarketplace.username").getValue();
		String password = DAORegistry.getPropertyDAO().get("eimarketplace.password").getValue();
		return login(httpClient, login, password);
	}

	/**
	 * Login to EiMarketPlace.
	 */
	public static boolean login(DefaultHttpClient httpClient, String username, String password) throws Exception {
		EIMPRedirectHandler redirectHandler = new EIMPRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);

		// go to homepage to get session id in cookie
		/* commented for testing purpose */ 
//		HttpGet httpGet = new HttpGet("http://www.eimarketplace.com/index.cfm");
//		httpGet.addHeader("Host", "www.eimarketplace.com");
//		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
//		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
//		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
//		// httpGet.addHeader("Keep-Alive", "300");
//		// httpGet.addHeader("Connection", "keep-alive");
//		httpClient.execute(httpGet);
		/* */		
		String postData = "Username=" + username + "&Password=" + password;

		// Thread.sleep(5000);
		redirectHandler.reset();
		// log in  in eimarketplace
		HttpPost httpPost = new HttpPost("http://www.eimarketplace.com/login/login.cfm");
		httpPost.addHeader("Host", "www.eimarketplace.com");
		httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		// httpPost.addHeader("Keep-Alive", "300");
		// httpPost.addHeader("Connection", "keep-alive");
		httpPost.addHeader("Referer", "http://www.eimarketplace.com/login/index.cfm");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
				
		httpPost.setEntity(new StringEntity(postData));
		httpClient.execute(httpPost);
		
		String redirectUrl = redirectHandler.getRedirectUrl();
		
		// System.out.println("REDIRECT URL=" + redirectUrl);
		
		if (redirectUrl == null || !redirectUrl.contains("https://www.eimarketplace.com/members/index.cfm")) {
			return false;
		}
		
		Date d = new Date(new Date().getTime() - 6L * 60L * 60L * 1000L);
		
		SimpleDateFormat dateFormatBrokerTAC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		BasicClientCookie2 c = new BasicClientCookie2("BROKERTAC", URLEncoder.encode("{ts '" + dateFormatBrokerTAC.format(d)+ "'}", "ISO-8859-1").replace("%20", "+"));
		c.setDomain("www.eimarketplace.com");
		c.setPath("/");
		
		httpClient.getCookieStore().addCookie(c);	
		System.out.println("Logged in");
		
		return true;		
	}
	
	private static String getEimpPage(DefaultHttpClient httpClient, String url) {
		try {
			HttpGet httpGet = new HttpGet(url);		
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Referer", url);
			HttpResponse httpResponse;
			httpResponse = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());
			return  EntityUtils.toString(entity);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getEimpSellerPage(Ticket ticket, TicketListingFetcher fetcher) {
		Integer eventId = ticket.getEventId();
		String seller = ticket.getSeller();
		
		String key = eventId + "-" + seller;
		CacheEntry entry = sellerCache.get(key);
		
		if (entry != null && entry.getAge() < EIMP_CACHE_TIMEOUT) {
			return (String)entry.getData();
		}
		
		entry = new CacheEntry(loadEimpSellerPage(ticket, fetcher));
		sellerCache.put(key, entry);
		//System.out.println("eimp seller page 1: " + entry.getData());
		return (String)entry.getData();
	}

	private static String loadEimpSellerPage(Ticket ticket, TicketListingFetcher fetcher) {
		if (!ticket.getSiteId().equals("eimarketplace")) {
			return null;
		}
		
		try {
			Date now = new Date();
			if (lastEimpLogin == null || now.getTime() - lastEimpLogin.getTime() > EIMP_LOGIN_TIMEOUT) {
				EIMPUtil.login(getEimpHttpClientInstance());
				lastEimpLogin = now;
			}
			String url = fetcher.getTicketListingUrl(ticket.getTicketListingCrawl());
			String content = getEimpPage(getEimpHttpClientInstance(), url);

			Pattern eimpSellerLinkPattern = Pattern.compile("HelpWindow\\(\\d+,\\d+,'.*?b=(\\d+)'[^>]+>" + ticket.getSeller() + "</a>");
			Matcher matcher = eimpSellerLinkPattern.matcher(content);
			if (matcher.find()) {
				String sellerUrl = "https://www.eimarketplace.com/brokers/brokercontact.cfm?b=" + matcher.group(1);
				content = getEimpPage(getEimpHttpClientInstance(), sellerUrl);
				//System.out.println("The content is 2: " + content);
				return content;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static byte[] getEimpMap(Integer eventId) {
		CacheEntry entry = chartCache.get(eventId);
		if (entry != null && entry.getAge() < EIMP_CACHE_TIMEOUT) {
			return (byte[])entry.getData();
		}
		
		entry = new CacheEntry(loadEimpMap(eventId));
		chartCache.put(eventId, entry);
		return (byte[]) entry.getData();
	}
	
	private static byte[] loadEimpMap(Integer eventId) {
		String queryEventId = null;
		// find eimp crawler bound to the event
		for(TicketListingCrawl crawl: DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(eventId)) {
			if (crawl.getSiteId().equals("eimarketplace")) {
				queryEventId = crawl.getExtraParameter("queryEventId");
				break;
			}
		}
		
		if (queryEventId == null) {
			return null;
		}

		String url = "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + queryEventId;
		try {
			Date now = new Date();
			if (lastEimpLogin == null || now.getTime() - lastEimpLogin.getTime() > EIMP_LOGIN_TIMEOUT) {
				EIMPUtil.login(getEimpHttpClientInstance());
				lastEimpLogin = now;
			}

			String content = getEimpPage(getEimpHttpClientInstance(), url);
			Matcher matcher = eimpMapLinkPattern.matcher(content);
			
			if (matcher.find()) {
			//	String mapUrl = matcher.group(1);
				String mapUrl = "https://www.eimarketplace.com/MarketPlace/maps/" + matcher.group(1);
				ClientConnectionManager cm = getClientConnectionManager();
				HttpClient httpClient = new DefaultHttpClient(cm, new BasicHttpParams());
				HttpGet httpGet = new HttpGet(mapUrl);		
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				HttpResponse httpResponse = httpClient.execute(httpGet);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(httpResponse.getEntity());

				return IOUtils.toByteArray(entity.getContent());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static DefaultHttpClient getEimpHttpClientInstance(){
		if(eimpHttpClient == null){
			ClientConnectionManager cm = getClientConnectionManager();
			eimpHttpClient = new DefaultHttpClient(cm, new BasicHttpParams());
		}
		return eimpHttpClient;
	}
	
	private static ClientConnectionManager getClientConnectionManager(){
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		HttpParams hp = new BasicHttpParams();
		SSLContext sslContext = null;
		
		try {
			TrustManager[] tm = new TrustManager[]{ new NaiveTrustManager()};
			sslContext = SSLContext.getInstance("TLS");
//			sslContext.init(null, new TrustManager[]{new NaiveTrustManager()}, null);
			sslContext.init(null, tm, null);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SSLSocketFactory sslsf = new SSLSocketFactory(sslContext);
		
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", sslsf, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(hp,schemeRegistry);
		return cm;	
	}

	
	public static class CacheEntry {
		private Object data;
		private Date created;
		
		public Object getData() {
			return data;
		}
		
		public void setData(Object data) {
			this.data = data;
		}
		
		public Date getCreated() {
			return created;
		}
		
		public void setCreated(Date created) {
			this.created = created;
		}
		
		public CacheEntry(Object data) {
			super();
			this.data = data;
			this.created = new Date();
		}

		// age in ms
		public Long getAge() {
			return System.currentTimeMillis() - created.getTime();
		}
	}
}
