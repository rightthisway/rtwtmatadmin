package com.admitone.tmat.utils;

import com.admitone.tmat.web.pojo.WebTicketRow;
import java.util.Comparator;

public class WebTicketRowANSorting implements Comparator {
	/**
	 * The compare method that compares the WebTicketRow
	 */
	public int compare(Object firstObjToCompare,Object secondObjToCompare) {
		WebTicketRow obj1 = (WebTicketRow)firstObjToCompare;
		WebTicketRow obj2 = (WebTicketRow)secondObjToCompare;
		
		//AlphanumericSorting ans = new AlphanumericSorting();
		//return ans.compare(obj1.getCategory().getSymbol(),obj2.getCategory().getSymbol());
		if(obj1.getCategorySymbol() != null && obj2.getCategorySymbol() != null){
			if(obj1.getCategorySymbol().equalsIgnoreCase("Premium")){
				return -1;
			}else if(obj2.getCategorySymbol().equalsIgnoreCase("Premium")){
				return 1;
			}else{
				return obj1.getCategorySymbol().compareTo(obj2.getCategorySymbol());
			}
			
		}else{
			if(obj1.getCategorySymbol() == null){
				return -1;
			}else{
				return 1;
			}
		
		}
		
	}
	
}