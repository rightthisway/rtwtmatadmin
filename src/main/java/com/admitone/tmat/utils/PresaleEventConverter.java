package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.PresaleEvent;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.mail.MailManager;

public class PresaleEventConverter extends QuartzJobBean implements StatefulJob{
	
	private static SharedProperty sharedProperty;
	private static MailManager mailManager = null;
	
	@Override
	protected void executeInternal(JobExecutionContext context)
	throws JobExecutionException {

		
		try{
			System.out.println("PRESALEEVENTJOB : Presale Event converter: Job Started....."+new Date());
			processPresaleEvents();
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.println("Error Occured in Presale Event converter Scheduler : "+ new Date());
			try {
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String subject = "TMAT Presale Event converter Job Failed : ";
				String template = "common-mail-error.html";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("bodyContent", "Following error occured while processing Presale Event converter Scheduler : ");
				map.put("error", e);
				//String toAddress = "pmehul@rightthisway.com";
				String toAddress = "AODev@rightthisway.com";
				mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			}catch (Exception ee) {
				ee.printStackTrace();
				System.err.println("Error Occured in Presale Event converter Scheduler While sending error email : "+ new Date());
			}
		}
		
	}
	
	public void processPresaleEvents() throws Exception {
	try {
		Date date = new Date();
		List<Event> events = new ArrayList<Event>();
		List<EventAudit> eventAudits = new ArrayList<EventAudit>();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -4);
		Date fourDaysBeforeDate = new Date(cal.getTimeInMillis());

		int count = 0;
		Collection<PresaleEvent> presaleEvents = DAORegistry.getPresaleEventDAO().getAllEligiblePresaleEvents(fourDaysBeforeDate);
		String url=PresaleEventConverter.sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=updateCrawl&crawls=";
		boolean flag = false;
		for(PresaleEvent presaleEvent : presaleEvents){
			Event event = presaleEvent.getEvent();
			if(event.getPresaleEvent()) {
				event.setPresaleEvent(Boolean.FALSE);
				if(presaleEvent.getAssignToVenueCategory() != null) {
					event.setVenueCategoryId(presaleEvent.getAssignToVenueCategory().getId());	
				}
				event.setLastUpdate(date);
				events.add(event);
				
				String username = "PRESALE JOB";
				Date now = new Date();
				EventAudit ea=new EventAudit();
				ea.setEventId(event.getId());
				ea.setUserName(username);
				ea.setAction("PRESALE Update");
				ea.setModifiedDate(now);
				ea.setEventName(event.getName());
				ea.setArtistId(event.getArtistId());
				ea.setArtistName(event.getArtist().getName());
				ea.setEventDate(event.getLocalDate());
				ea.setEventTime(event.getLocalTime());
				ea.setPresaleEvent(event.getPresaleEvent());
				ea.setVenueId(event.getVenueId());
				ea.setVenueName(DAORegistry.getVenueDAO().get(event.getVenueId()).getBuilding());
				if(event.getVenueCategoryId()!= null) {
					ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
				}
				if(event.getEventType()!=null && event.getEventType().name()!= null) {
					ea.setEventType(event.getEventType().name());
				}
				if(event.getNoPrice()!= null){
					ea.setNoPrice("YES");
				} else {
					ea.setNoPrice("NO");
				}
				if(event.getAdmitoneId()!= null) {
				    ea.setAdmitoneId(event.getAdmitoneId());
				}
				//DAORegistry.getEventAuditDAO().save(ea);
				eventAudits.add(ea);
				
				count++;
			}
			presaleEvent.setStatus("PROCESSED");
			for (TicketListingCrawl crawl : DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(presaleEvent.getEvent().getId())) {
				if(!crawl.getAutomaticCrawlFrequency() && crawl.getCrawlFrequency()==3600){
					crawl.setAutomaticCrawlFrequency(true);
					DAORegistry.getTicketListingCrawlDAO().update(crawl);
					url+= crawl.getId() + ",";
					flag = true;
				}
				
			}
		}
		if(flag){
			HttpClient client;
			url = url.substring(0,url.length()-1);
			try {
				client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				System.out.println(client.execute(get));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
		}
		
		DAORegistry.getEventDAO().updateAll(events);
		DAORegistry.getPresaleEventDAO().updateAll(presaleEvents);
		
		System.out.println("Presale Event Convert Job Completed successfully : "+count+" : "+new Date());
		
	} catch (Exception e) {
		System.out.println("PRESALEEVENTJOB : Error in Presale Event Job..."+new Date());
		e.printStackTrace();
		/*String fromName  = "tmatmanagers@rightthisway.com";
		String mimeType = "text/html";
		String subject = "Presale Event Converter Job Failed : ";
		String template = "common-mail-error.html";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bodyContent", "Following error occured in Presale Event Converter Job:-");
		map.put("error", e);
		
		//String toAddress = "pmehul@rightthisway.com";
		String toAddress = "AODev@rightthisway.com";
		mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);*/
		
		throw e;
	}
	}

	public SharedProperty getSharedProperty() {
		return PresaleEventConverter.sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		PresaleEventConverter.sharedProperty = sharedProperty;
	}
	public MailManager getMailManager() {
		return PresaleEventConverter.mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		PresaleEventConverter.mailManager = mailManager;
	}

}
