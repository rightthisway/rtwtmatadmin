package com.admitone.tmat.utils;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EiMarketPlaceEvent;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.eimarketplace.EIMPRedirectHandler;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class OldEiMarketPlaceEventPopulator {
	private static Pattern datePattern = Pattern.compile("(\\d+)\\+");
	private static boolean running = false;
	private static int fetchedEventCount;
	private static EIMPCredential credential;
	private static Set<Integer> usedEventIds;
	private static Set<Integer> dbEventIds;
	
	public static boolean isRunning() {
		return running;
	}	
	
	public static int getFetchedEventCount() {
		return fetchedEventCount;
	}
	
	public void setCredential(EIMPCredential credential) {
		OldEiMarketPlaceEventPopulator.credential = credential;
	}
	
	public static void start() {
		if (running) {
			return;
		}
		
		Collection<Integer> eventIds = DAORegistry.getEiMarketPlaceEventDAO().getEiMarketPlaceEventIds();
		dbEventIds = Collections.synchronizedSet(new HashSet<Integer>(eventIds));
		for(Integer id: dbEventIds) {
			System.out.println("]] EVENT ID:" + id);
		}
		
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.execute(new Runnable() {
			
			public void run() {				
				System.out.println("]]]] STARTING EIMP FETCH");
				running = true;	
				usedEventIds = java.util.Collections.synchronizedSet(new HashSet<Integer>());
					
				SimpleHttpClient httpClient = null;
				try {
					fetchedEventCount = 0;
					httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());

					System.out.println("]]]] STEP 1");

					Date startDate = new Date();
					parseSearchResultPage(httpClient);
					System.out.println("]]]] STEP 2");
					
					// delete expired events
					if (DAORegistry.getEiMarketPlaceEventDAO() != null) {
						System.out.println("]]]] STEP 3");
						
						DAORegistry.getEiMarketPlaceEventDAO().deleteEventsNotUpdatedAfter(startDate);
						System.out.println("]]]] STEP 4");
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					running = false;
				}
				System.out.println("]]]] FINISH EIMP FETCH");
				
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
		});
	}

	public static boolean addUsedEventIds(Integer eventId) {
		return usedEventIds.add(eventId);
	}

	public static boolean hasUsedEventIds(Integer eventId) {
		return usedEventIds.contains(eventId);
	}
	
	private static void parseSearchResultPage(SimpleHttpClient httpClient) throws Exception{
		
		EIMPRedirectHandler redirectHandler = new EIMPRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		String searchUrl = "https://www.eimarketplace.com/brokers/byevent.cfm?restart=bykeyword.cfm&id=-1&b=187";
		HttpGet httpGet = new HttpGet(searchUrl);
		httpGet.addHeader("Host", "www.eimarketplace.com");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		httpGet.addHeader("Referer", "https://www.eimarketplace.com/members/index.cfm");

		CloseableHttpResponse response = null;
		HttpEntity entity = null;
		try{
			response = safeExecuteHttpRequest(httpClient, httpGet);
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			response.close();
		}
		
		System.out.println("PARSE ARTIST PAGE");
		parseArtistPage(httpClient, entity, searchUrl);
	}

	/**
	 * Parse Artist Page (1st step).
	 */
	private static Pattern artistPageSelectPattern = Pattern.compile("<select name=\"e\".*?>(.*?)</select>", Pattern.DOTALL);
	private static Pattern artistPageOptionPattern = Pattern.compile("option value=\"(.*?)\".*?>(.*?)<", Pattern.DOTALL);
	
	private static void parseArtistPage(SimpleHttpClient httpClient, HttpEntity entity, 
			String refererUrl) throws Exception {
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);

		Matcher artistPageSelectMatcher = artistPageSelectPattern.matcher(content);
		
		if (!artistPageSelectMatcher.find()) {
			System.out.println(">> RETURN FROM ARTIST PAGE");
			return;
		}
		
		String artistPageSelect = artistPageSelectMatcher.group(1);
		
		Matcher artistPageOptionMatcher = artistPageOptionPattern.matcher(artistPageSelect);
		
		System.out.println("]]] ABOUT TO PARSE PAGE FOR ARTIST IDS");
		while(artistPageOptionMatcher.find()) {
			Integer artistId = Integer.parseInt(artistPageOptionMatcher.group(1));

			System.out.println("]]] FETCHING ARTIST ID=" + artistId);
			((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();
			
			String venueUrl = "https://www.eimarketplace.com/brokers/byvenue.cfm";			
			HttpPost httpPost = new HttpPost(venueUrl);
			httpPost.addHeader("Host", "www.eimarketplace.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpPost.addHeader("Referer", refererUrl);
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateFormatString = URLEncoder.encode("{ts '" + dateFormat.format(new Date(new Date().getTime() - 60L * 60L * 1000L)) + "'}", "ISO-8859-1").replace("%20", "+");
			String postData = "history=byevent.cfm&venue=1&datespan=0&startdate=" + dateFormatString + "&e=" + artistId + "&next=byvenue.cfm";
			
			httpPost.setEntity(new StringEntity(postData));
			CloseableHttpResponse response = null;
			try{
				response = safeExecuteHttpRequest(httpClient, httpPost);
				if (response == null) {
					return;
				}
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				response.close();
			}

			String currentUrl = venueUrl;
			if (((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl() != null) {
				currentUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
			}
			
			if (currentUrl.contains("/brokers/byvenue.cfm")) {
				parseVenuePage(httpClient, entity, venueUrl, artistId);
			} else if (currentUrl.contains("/brokers/bydate.cfm")) {
				parseDatePage(httpClient, entity, artistId);
			} else if (currentUrl.contains("/brokers/results.cfm")) {
				parseTicketListingPage(httpClient, entity, artistId);				
			} else {
				System.out.println("]]] NOTHING IN ARTIST " + artistId);
			}

		}		
	}
	
	/**
	 * Parse Venue Page (2nd step).
	 */
	private static Pattern venuePageEventIdPattern = Pattern.compile("<INPUT.*?NAME=\"event\" VALUE=\"(.*?)\">", Pattern.DOTALL);
	private static Pattern venuePageSelectPattern = Pattern.compile("<SELECT NAME=\"v\".*?>(.*?<)/SELECT>", Pattern.DOTALL);
	private static Pattern venuePageOptionPattern = Pattern.compile("<OPTION VALUE=\"(.*?)\".*?>(.*?)</OPTION>", Pattern.DOTALL);

	private static Collection<EventHit> parseVenuePage(SimpleHttpClient httpClient, HttpEntity entity, String refererUrl, Integer artistId) throws Exception {
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);

		Matcher venuePageEventIdMatcher = venuePageEventIdPattern.matcher(content);
		venuePageEventIdMatcher.find();
		String eventId = venuePageEventIdMatcher.group(1);

		Matcher venuePageSelectMatcher = venuePageSelectPattern.matcher(content);
		if (!venuePageSelectMatcher.find()) {
			System.out.println("]]]] NO VENUE IN PAGE");
			return null;
		}

		String venuePageSelect = venuePageSelectMatcher.group(1);
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		Collection<VenuePageFetcherTask> venuePageFetcherTasks = new ArrayList<VenuePageFetcherTask>();
		ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(4); 
		
		Matcher venuePageOptionMatcher = venuePageOptionPattern.matcher(venuePageSelect);
		
		while(venuePageOptionMatcher.find()) {
			String venueId = venuePageOptionMatcher.group(1);
			String venueName = venuePageOptionMatcher.group(2);

			VenuePageFetcherTask venuePageFetcherTask = new VenuePageFetcherTask(eventId, venueId,
					refererUrl, artistId);
			venuePageFetcherTasks.add(venuePageFetcherTask);
			
			// System.out.println("BEFORE EXECUTE " + venuePageFetcherTask.getId() + "/" + eventId + "/" + venueId);
			threadPoolExecutor.execute(venuePageFetcherTask);
			// System.out.println("AFTER EXECUTE " + venuePageFetcherTask.getId());
						
		}
		
		System.out.println("*** WAIT FOR THREAD TERMINATION");
		threadPoolExecutor.shutdown();
		try {
			
		threadPoolExecutor.awaitTermination(5, TimeUnit.MINUTES);
		} catch (Exception e) {
			System.out.println(">>>>>>>>>>>>>> ERROR WHILE WAITING TERMINATION <<<<<<<<<<<<<<<<");
			e.printStackTrace();
		}

		System.out.println("******** ADDING EVENT HITS ****");
		for (VenuePageFetcherTask venuePageFetcherTask: venuePageFetcherTasks) {
			if (venuePageFetcherTask.getEventHits() != null) {
				eventHits.addAll(venuePageFetcherTask.getEventHits());
			}
		}
						
		System.out.println("******** DONE  ****");
		return eventHits;
	}

	/**
	 * Execute the http request agains if it fails. Do it 5 times.
	 */
	public static CloseableHttpResponse safeExecuteHttpRequest(SimpleHttpClient httpClient, HttpUriRequest request) {
		for(int i = 0 ; i < 5 ; i++) {
			CloseableHttpResponse response = null;
			try {
				response = httpClient.execute(request);
				if (response != null) {
					return response;
				}
			} catch (Exception e) {
				continue;
			}
		}
		return null;
	}
			

	static class VenuePageFetcherTask implements Runnable {
		private Collection<EventHit> eventHits = new ArrayList<EventHit>();
		private String venueId;
		private String eventId;
		private String refererUrl;
		private Integer artistId;
		
		private static int idCounter = 0;
		private int id;
		
		public VenuePageFetcherTask(String eventId, String venueId,
				String refererUrl, Integer artistId) {
			this.venueId = venueId;
			this.eventId = eventId;
			this.refererUrl = refererUrl;
			this.artistId = artistId;
						
			this.id = idCounter++;
			
			System.out.println("*** CREATED THREAD " + id);
		}
		
		public int getId() {
			return id;
		}
		
		public void run() {
			System.out.println("*** START THREAD " + id);
			SimpleHttpClient httpClient = null;

			try {
				httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
				httpClient.setRedirectHandler(new EIMPRedirectHandler());
				System.out.println("EIMARKETPLACE VENUEID=" + venueId);
				
				((EIMPRedirectHandler)httpClient.getRedirectHandler()).reset();
					
				String searchByVenueUrl = "https://www.eimarketplace.com/brokers/results.cfm";			
				HttpPost httpPost = new HttpPost(searchByVenueUrl);
				httpPost.addHeader("Host", "www.eimarketplace.com");
				httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
				httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				// httpGet.addHeader("Keep-Alive", "300");
				// httpGet.addHeader("Connection", "keep-alive");
				httpPost.addHeader("Referer", refererUrl);
				httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
				// history=byevent.cfm&venue=1&datespan=0&startdate=%7Bts+%272009-05-07+15%3A44%3A35%27%7D&e=759&next=byvenue.cfm
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateFormatString = URLEncoder.encode("{ts '" + dateFormat.format(new Date(new Date().getTime() - 60L * 60L * 1000L)) + "'}", "ISO-8859-1").replace("%20", "+");
				String postData = "history=byvenue.cfm&event=" + eventId + "&datespan=0&startdate=" + dateFormatString + "&v=" + venueId + "&next=bydate.cfm";

				httpPost.setEntity(new StringEntity(postData));
				
				CloseableHttpResponse response = null;
				HttpEntity entity = null;
				try{
					response = safeExecuteHttpRequest(httpClient, httpPost);
					entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					response.close();
				}
				
				String currentUrl = searchByVenueUrl;
				
				if (((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl() != null) {
					currentUrl = ((EIMPRedirectHandler)httpClient.getRedirectHandler()).getRedirectUrl();
				}
				
				if (currentUrl.contains("/brokers/bydate.cfm")) {
					parseDatePage(httpClient, entity, artistId);
				} else if (currentUrl.contains("/brokers/results.cfm")) {
					parseTicketListingPage(httpClient, entity, artistId);				
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
				// System.out.println("*** STOP THREAD " + id);
			}

		}		
		
		private Collection<EventHit> getEventHits() {
			return this.eventHits;
		}
	}

	
	/**
	 * Parse Venue Page (3rd step).
	 */
	
	private static Pattern datePageSelectPattern = Pattern.compile("<SELECT NAME=\"p\".*?>(.*?)</SELECT>", Pattern.DOTALL);
	private static Pattern datePageOptionPattern = Pattern.compile("<OPTION VALUE=\"(.*?)\">(.*?)</OPTION>", Pattern.DOTALL);
	
	private static void parseDatePage(SimpleHttpClient httpClient, HttpEntity entity, Integer artistId) throws Exception {
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);
		String content = EntityUtils.toString(uncompressedEntity);
		
		Matcher datePageSelectMatcher = datePageSelectPattern.matcher(content);
		if (!datePageSelectMatcher.find()) {
			return;
		}
		
		String datePageSelect = datePageSelectMatcher.group(1);
		
		Matcher datePageOptionMatcher = datePageOptionPattern.matcher(datePageSelect);
		
		while(datePageOptionMatcher.find()) {
			Integer eventId = Integer.parseInt(datePageOptionMatcher.group(1));

			if (hasUsedEventIds(eventId)) {
				continue;
			}

			if (dbEventIds.contains(eventId)) {
				DAORegistry.getEiMarketPlaceEventDAO().updateDate(eventId, new Date());
				fetchedEventCount++;				
				continue;
			}

			String ticketListingUrl = "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + eventId;			
			HttpGet httpGet = new HttpGet(ticketListingUrl);
			httpGet.addHeader("Host", "www.eimarketplace.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			// httpGet.addHeader("Keep-Alive", "300");
			// httpGet.addHeader("Connection", "keep-alive");
			httpGet.addHeader("Referer", "http://www.eimarketplace.com/brokers/results.cfm");
			
			CloseableHttpResponse response = null;
			try{
				response = safeExecuteHttpRequest(httpClient, httpGet);
				if (response == null) {
					return;
				}
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				response.close();
			}
			
			parseTicketListingPage(httpClient, entity, artistId);
		}
	}

	/**
	 * Parse Venue Page (4th step).
	 */
	private static Pattern ticketListingPageEventIdPattern = Pattern.compile("<input.*?name=\"p\" value=\"(.*?)\">", Pattern.DOTALL);
	
//	<strong class="accentplus2">Nov 14, 2009</strong>
//	<span class="fineprint"> at 08:30 PM<br>Saturday</span>
//
//</td><td class="fieldinfo">
//	<table cellspacing="0" cellpadding="0" border="0" align="RIGHT">
//	<tr>
//		<td align="CENTER" valign="TOP">
//			<a href="/search/venuedetail.cfm?restart=yes&v=504&b=187" onmouseover="window.status='Show detailed information on Electric Factory ...'; return true" onmouseout="window.status=' '; return true"><img align="LEFT" src="/graphics/info.gif" width="15" height="15" alt="Venue Seating Chart for Electric Factory" border="0" hspace="5" vspace="2"></a>
//	
//		
//		<a href="http://www.ticketsnow.com/maps/504-1-2-main.gif" target="_blank" onmouseover="window.status='View a Venue map for GA ...'; return true" onmouseout="window.status=' '; return true"><img src="/graphics/map.gif" width="24" height="19" alt="Venue Seating Chart for Electric Factory" border="0" hspace="0" vspace="0"></a>
//	
//
//	</td>
//	</tr>
//	</table>
//
//	
//<strong class="accentplus2">Electric Factory</strong>
//
//		<br>
//		<span class="fineprint">
//	
//			Philadelphia, 
//	
//			PA
//		</span>
//
//	</td>
//</tr>
	//                                                                                                                                                                                        event name                                event desc (optional)                                                          event date                                  event time                                                 venue id                                         venue name    venue location
	private static Pattern ticketListingPageVenuePattern = Pattern.compile("<tr bgcolor=\"EEEEDD\" valign=\"TOP\" align=\"CENTER\">.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">(.*?)</span>.*?</td>.*?<td class=\"fieldinfo\">.*?<strong class=\"accentplus2\">(.*?)</strong>.*?<span class=\"fineprint\">(.*?)<.*?<a href=\"/search/venuedetail.cfm[?]restart=yes&v=(\\d+?)&.*?</table>.*?<strong class=\"accentplus2\">(.*?)</strong>(.*?)</td>", Pattern.DOTALL);
	private static Pattern ticketListingPageVenueLocationPattern = Pattern.compile("<span class=\"fineprint\">(.*?)</span>", Pattern.DOTALL);

	private static void parseTicketListingPage(SimpleHttpClient httpClient, HttpEntity entity, Integer artistId) throws Exception {
		HttpEntity uncompressedEntity = HttpEntityHelper.getUncompressedEntity(entity);		
		String content = EntityUtils.toString(uncompressedEntity);
		
		Matcher ticketListingPageEventIdMatcher = ticketListingPageEventIdPattern.matcher(content);
		ticketListingPageEventIdMatcher.find();
		Integer eventId = Integer.parseInt(ticketListingPageEventIdMatcher.group(1));

		if (hasUsedEventIds(eventId)) {
			return;
		}
		
		if (dbEventIds.contains(eventId)) {
			DAORegistry.getEiMarketPlaceEventDAO().updateDate(eventId, new Date());
			fetchedEventCount++;
			return;
		}
		
		Matcher ticketListingPageVenueMatcher = ticketListingPageVenuePattern.matcher(content);
		if (!ticketListingPageVenueMatcher.find()) {
			return;
		}
		
		String eventName = ticketListingPageVenueMatcher.group(1);
		String tmpEventDetail = ticketListingPageVenueMatcher.group(2);
		String[] tokens = tmpEventDetail.split("<br>");
		String eventDetail = null;
		if (tokens.length == 2) {
			eventDetail = tokens[1].trim();
		}
		//System.out.println("1 EVENT NAME=" + eventName);
		String eventDateString = ticketListingPageVenueMatcher.group(3);
		String eventTimeString = ticketListingPageVenueMatcher.group(4).substring(3);
		Integer venueId = Integer.parseInt(ticketListingPageVenueMatcher.group(5));
		String venueName = ticketListingPageVenueMatcher.group(6);
		
		String rawVenueCityState = ticketListingPageVenueMatcher.group(7);
		Matcher ticketListingPageVenueLocationMatcher = ticketListingPageVenueLocationPattern.matcher(rawVenueCityState);
		String venueCityState = "";
		if (ticketListingPageVenueLocationMatcher.find()) {
			venueCityState = ticketListingPageVenueLocationMatcher.group(1);
		}
		
		String[] venueTokens = venueCityState.split(",");
		String venueCity = null;
		String venueState = null;
		if (venueTokens.length > 0) {
			venueCity = venueTokens[0].trim();
		}
		if (venueTokens.length > 1) {
			venueState = venueTokens[1].trim();
		}
		
		// Mar 12, 2009
		SimpleDateFormat ticketListingPageDateFormat1 = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");			
		SimpleDateFormat ticketListingPageDateFormat2 = new SimpleDateFormat("MMM dd, yyyy");
		
		
		// try to parse both date and time
		String eventDateTimeString = eventDateString + " " + eventTimeString;
		Date venueDate = null;
		try {
			venueDate = ticketListingPageDateFormat1.parse(eventDateTimeString);
		} catch (Exception e) {
		}
		
		// if it fails, only parse date
		if (venueDate == null) {
			try {
				venueDate = ticketListingPageDateFormat2.parse(eventDateString);
			} catch (Exception e) {
			}			
		}

		System.out.println("+ EVENT ID=" + eventId);
		System.out.println("+ ARTIST ID=" + artistId);
		System.out.println("+ EVENT NAME=" + eventName);
		System.out.println("+ EVENT DETAIL=" + eventDetail);
		System.out.println("+ EVENT DATE=" + venueDate);
		System.out.println("+ VENUE ID=" + venueId);
		System.out.println("+ VENUE NAME=" + venueName);
		System.out.println("+ VENUE CITY=" + venueCity);
		System.out.println("+ VENUE STATE=" + venueState);
		
		EiMarketPlaceEvent event = new EiMarketPlaceEvent();
		event.setId(eventId);
		event.setArtistId(artistId);
		event.setEventName(eventName);
		event.setEventDetail(eventDetail);
		event.setEventDate(venueDate);
		event.setVenueName(venueName);
		event.setVenueCity(venueCity);
		event.setVenueState(venueState);
		event.setUpdatedDate(new Date());
		
		if (DAORegistry.getEiMarketPlaceEventDAO() != null) {
			DAORegistry.getEiMarketPlaceEventDAO().saveOrUpdate(event);
		}
		
		addUsedEventIds(eventId);
		fetchedEventCount++;
	}
	
}
