package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.StubHubFeedTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.StubHubWebTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkTicketListingFetcher;
import com.admitone.tmat.web.pojo.FeedAndCrawlComparatorVO;

public class SHWebAndWSTicketListingComparator {
	
	private static SHWebAndWSTicketListingComparator instance = null;

	public static SHWebAndWSTicketListingComparator getInstance() {
		if(instance==null){
			instance = new SHWebAndWSTicketListingComparator();
		}
		return instance;
	}
	
	public FeedAndCrawlComparatorVO compare(Integer shEventId, String url){
		
		ComparatorTicketHitIndexer feedTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		ComparatorTicketHitIndexer webTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		TicketListingFetcher feedTicketListingFetcher = new StubHubFeedTicketListingFetcher();
		TicketListingFetcher webTicketListingFetcher = new StubHubWebTicketListingFetcher();
				
		
		Set<String> feedTicketIds = new HashSet<String>();
		Set<String> webTicketIds = new HashSet<String>();	
		
		System.out.println("TICKET LISTING URL=" + url);
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setSiteId(Site.STUB_HUB);
		ticketListingCrawl.setQueryUrl(url);
		ticketListingCrawl.resetStats();
		
		try {
			feedTicketListingFetcher.fetchTicketListing(feedTicketHitIndexer, ticketListingCrawl);
			webTicketListingFetcher.fetchTicketListing(webTicketHitIndexer, ticketListingCrawl);
		} catch (InterruptedTicketListingCrawlException e) {
			// do nothing
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Collection<TicketHit> feedTicketHits = feedTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> feedTicketById = new HashMap<String, TicketHit>();
		for(TicketHit feedTicketHit: feedTicketHits) {
			feedTicketIds.add(feedTicketHit.getItemId());
			feedTicketById.put(feedTicketHit.getItemId(), feedTicketHit);
		}
		
		Collection<TicketHit> webTicketHits = webTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> webTicketById = new HashMap<String, TicketHit>();
		for(TicketHit webTicketHit: webTicketHits) {
			webTicketIds.add(webTicketHit.getItemId());
			webTicketById.put(webTicketHit.getItemId(), webTicketHit);
		}
		
		Set<String> diffWebTicketIds = new HashSet<String>(webTicketIds);
		Set<String> diffFeedTicketIds = new HashSet<String>(feedTicketIds);

		System.out.println("TICKET INDEXED FROM FEED=" + feedTicketHits.size());
		System.out.println("TICKET INDEXED FROM WEB=" + webTicketHits.size());

		// see which ids appears in feed but not in web
		for(String ticketId: feedTicketIds) {
			diffWebTicketIds.remove(ticketId);
		}
		
		System.out.println("EXTRA TICKET IN FEED : " + diffWebTicketIds.size());
		ArrayList<TicketHit> extraTicketInFeed = new ArrayList<TicketHit>();
		for(String ticketId: diffWebTicketIds) {
			TicketHit ticketHit = webTicketById.get(ticketId);
			extraTicketInFeed.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}
		
		// see which ids appears in web but not in feed
		for(String ticketId: webTicketIds) {
			diffFeedTicketIds.remove(ticketId);
		}

		System.out.println("MISSING TICKET IN FEED : " + diffFeedTicketIds.size());
		ArrayList<TicketHit> missingTicketInFeed = new ArrayList<TicketHit>();
		for(String ticketId: diffFeedTicketIds) {
			TicketHit ticketHit = feedTicketById.get(ticketId);
			missingTicketInFeed.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}
		
		FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = new FeedAndCrawlComparatorVO();
		
		feedAndCrawlComparatorVO.setTicketIndexedFromFeed(feedTicketHits.size());
		feedAndCrawlComparatorVO.setTicketIndexedFromWeb(webTicketHits.size());
		feedAndCrawlComparatorVO.setExtraTicketInFeed(extraTicketInFeed);
		feedAndCrawlComparatorVO.setMissingTicketInFeed(missingTicketInFeed);
		
		
		feedTicketHitIndexer.reset();
		webTicketHitIndexer.reset();
		return feedAndCrawlComparatorVO;
				
		
	}
	
	

}
