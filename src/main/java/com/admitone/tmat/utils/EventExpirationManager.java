package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.acegisecurity.context.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourStatus;

public class EventExpirationManager implements InitializingBean {
//	private TicketListingCrawler ticketListingCrawler;
	public static final Long EXPIRE_PERIOD = 24L * 3600L * 1000L;
	private Timer timer = new Timer();
	private Logger logger = LoggerFactory.getLogger(EventExpirationManager.class); 
	
	public void afterPropertiesSet() throws Exception {
		/*if (ticketListingCrawler == null) {
			throw new RuntimeException("Property ticketListingCrawler must be set");
		}*/
		
		Calendar currentCal = new GregorianCalendar();
		Calendar cal = new GregorianCalendar(currentCal.get(Calendar.YEAR),
											 currentCal.get(Calendar.MONTH),
											 currentCal.get(Calendar.DATE) + 1,
											 0,
											 30);
		timer.scheduleAtFixedRate(new EventExpirationManagerTimerTask(this), cal.getTime(), EXPIRE_PERIOD);
	}
	
	public void expireEvent(Event event) {
		event.setEventStatus(EventStatus.EXPIRED);
		event.setLastUpdate(new Date());
		DAORegistry.getEventDAO().update(event);
		String username = "AUTO";
		String userAction = "AUTO";
		try{
			username = SecurityContextHolder.getContext().getAuthentication().getName();
			userAction = "Manual Expire";
		}catch (Exception e) {
			// TODO: handle exception
		}
        Date now = new Date();
		EventAudit ea=new EventAudit();
		ea.setEventId(event.getId());
		ea.setUserName(username);
		ea.setAction(userAction);
		ea.setModifiedDate(now);
		ea.setEventName(event.getName());
		/*ea.setTourId(event.getTourId());
		ea.setTourName(event.getTour().getName());
		ea.setArtistId(event.getTour().getArtistId());
		ea.setArtistName(event.getTour().getArtist().getName());*/
		ea.setArtistId(event.getArtistId());
		ea.setArtistName(event.getArtist().getName());
		ea.setEventDate(event.getLocalDate());
		ea.setEventTime(event.getLocalTime());
		ea.setVenueId(event.getVenueId());
		ea.setVenueName(event.getVenue().getBuilding());
		if(event.getVenueCategoryId()!= null)
		{
			ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
		}
		else
		{
		ea.setVenueCategory(null);
		}
		if(event.getEventType().name()!= null)
		{
		ea.setEventType(event.getEventType().name());
		}
		else
		{
		ea.setEventType(null);
		}
		if(event.getNoPrice()!= null){
		ea.setNoPrice("YES");
		}
		else
		{
		ea.setNoPrice("NO");
		}
		if(event.getAdmitoneId()!= null)
		{
		ea.setAdmitoneId(event.getAdmitoneId());
		}
		else
		{
		ea.setAdmitoneId(null);
		}
		DAORegistry.getEventAuditDAO().save(ea);
		// disabled  crawls
		Collection<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(event.getId());
		if (crawls != null){
			for (TicketListingCrawl crawl: crawls) {
				//  Neews to remove from admin only..
//				SpringUtil.getTicketListingCrawler().removeTicketListingCrawl(crawl);
				
				
				//DAORegistry.getTicketDAO().disableAndUnassignCrawlTickets(crawl.getId());
				//DAORegistry.getTicketListingCrawlDAO().delete(crawl);
				DAORegistry.getTicketDAO().disableTickets(crawl.getId());
			}	
			DAORegistry.getTicketListingCrawlDAO().deleteByEventId(event.getId());
		}
		
		DAORegistry.getBookmarkDAO().deleteEventBookmarks(event.getId());
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksWithEventId(event.getId());

		// expire alerts
		DAORegistry.getUserAlertDAO().expireAlertsByEventId(event.getId());
		
		// ebay inventory
//		DAORegistry.getEbayInventoryEventDAO().realDeleteByEventId(event.getId());
		
	}

	/*public void expireTour(Tour tour) {
		// loop on all the events and remove ebay_inventory_tour
		Collection<Event> events = tour.getEvents();
		if (events != null) {
			for(Event event: events) {
				expireEvent(event);
			}
		}
		tour.setTourStatus(TourStatus.EXPIRED);
		DAORegistry.getTourDAO().update(tour);
		DAORegistry.getEbayInventoryTourDAO().deleteById(tour.getId());
	}*/

	/*public void expireArtist(Artist artist) {
		// loop on all the tour of the artists
		Collection<Tour> tours = artist.getTours();
		if (tours != null) {
			for(Tour tour: tours) {
				expireTour(tour);
			}
		}
	}*/
	
	public Collection<Event> getExpiredEvents() {
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByDates(null, new Date(new Date().getTime() - 72L * 60L * 60L * 1000L), null);
		ArrayList<Event> expiredEvents = new ArrayList<Event>();
		for (Event event: events) {
			if (!event.getEventStatus().equals(EventStatus.ACTIVE)) {
				expiredEvents.add(0, event);
			}
		}
		
		return expiredEvents;
	}
	
	public void expireEvent(Integer id) {
		Event event = DAORegistry.getEventDAO().get(id);
		expireEvent(event);
	}
	
	public void expireEvents(Integer[] eventIds) {
		for (Integer id: eventIds) {
			expireEvent(id);
		}
	}

	/*public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}*/
	
	public void expireEvents() {
		logger.info("*** event expiration");
		for (Event event: getExpiredEvents()) {
			try { 
				logger.info("*** EXPIRE EVENT " + event.getName());
				expireEvent(event);
			} catch(Exception e) {
				logger.warn("Error while expiring event " + event.getName(), e);
			}
		}
	}

	public static class EventExpirationManagerTimerTask extends TimerTask {
		private EventExpirationManager eventExpirationManager;
		private Date lastUpdate = new Date(new Date().getTime() - 3600000L);
		
		public EventExpirationManagerTimerTask(EventExpirationManager eventExpirationManager) {
			this.eventExpirationManager = eventExpirationManager;
		}
		
		public synchronized void run() {
			Date now = new Date();
			if (now.getTime() - lastUpdate.getTime() < 30000) {
				return;
			}
			
			lastUpdate = now;
			eventExpirationManager.expireEvents();
		}
	}
}
