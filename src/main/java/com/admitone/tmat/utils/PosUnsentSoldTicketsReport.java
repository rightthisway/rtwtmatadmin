package com.admitone.tmat.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.pojo.PosEvent;
import com.admitone.tmat.pojo.ShortLongTicketGroup;
import com.admitone.tmat.pojo.ShortTicketGroup;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;

public class PosUnsentSoldTicketsReport implements InitializingBean{
	
	static MailManager mailManager = null;
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		PosUnsentSoldTicketsReport.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Date now = new Date();
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 01);
		calender.set(Calendar.HOUR_OF_DAY, 8);
		
		if(now.getHours() >= 8) {
			calender.add(Calendar.DATE, 1);	
		}
		
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						PosUnsentSoldTicketDetailsEmail();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60 * 24); //24 hour
	}
	
	public void PosUnsentSoldTicketDetailsEmail() throws Exception{
		
		try {
			System.out.println("inside next 15 days unsent order email..."+new Date());
			SXSSFWorkbook workbook = getRtwPosUnsentOrderDetails();
			Sheet ssSheet = workbook.getSheetAt(0);
			int num = ssSheet.getLastRowNum();
			if(ssSheet != null && ssSheet.getLastRowNum() >= 1) {
				System.out.println("inside if next 15 days unsent order email..."+new Date());
			    ByteArrayOutputStream bos = new ByteArrayOutputStream();
			    try {
			        workbook.write(bos);
			    } finally {
			        bos.close();
			    }
			    byte[] bytes = bos.toByteArray();
			    MailAttachment[] mailAttachments = new MailAttachment[1];
			    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "PosUnSentSoldTicketDetails.xlsx");
					
			    String resource = "mail-rtw-pos-unsent-order-details.html";
				String subject = "Orders for events falling in next 15 days that have not been fulfilled yet.";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com,bmanusama@rightthisway.com,jreardon@rightthisway.com";
				//String bccAddress = "AODev@rightthisway.com";
				//String bccAddress = "AODev@rightthisway.com";
				//String toAddress ="tselvan@rightthisway.com,amit.raut@rightthisway.com";
				String fromName = "tmatmanagers@rightthisway.com";
				String cc = null;//"araut@rightthisway.com";
				String bccAddress = "AODev@rightthisway.com";
				System.out.println("before next 15 days unsent order email..."+new Date());
				Map<String, Object> map = new HashMap<String, Object>();
				
				mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, resource, map, mimeType, mailAttachments);
				System.out.println("after next 15 days unsent order email..."+new Date()+"...."+mailIdProperty.getTmatmanagers());
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Error while downloading Orders for events falling in next 15 days that have not been fulfilled yet.";
			String template = "mail-rtw-pos-unsent-order-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("error", e);
			//String toAddress = "tselvan@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;
		}		
	}
	
	public static SXSSFWorkbook getRtwPosUnsentOrderDetails() throws Exception {


		List<Object[]> list = DAORegistry.getQueryManagerDAO().getRtwPosUnSentInvocieTicketDetails();

		SXSSFWorkbook workbook = new SXSSFWorkbook();
		Sheet ssSheet =  workbook.createSheet("Next_15_days_unsent_orders");
		int j = 0;
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Broker");
	    j++;
		rowhead.createCell((int) j).setCellValue("Event Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("EventTime");
    	j++;
    	rowhead.createCell((int) j).setCellValue("Venue Name");
    	j++;
    	rowhead.createCell((int) j).setCellValue("section");
 	    j++;
    	rowhead.createCell((int) j).setCellValue("Row");
    	j++;
	    rowhead.createCell((int) j).setCellValue("Seat");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Qty");
	    j++;
	   /* rowhead.createCell((int) j).setCellValue("Retail Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Cost");
	    j++;*/
	    /*rowhead.createCell((int) j).setCellValue("Wholesale Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Actual Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Total Price");
	    j++;*/
	    /*rowhead.createCell((int) j).setCellValue("ETicket");
	    j++;*/
	    rowhead.createCell((int) j).setCellValue("On Hand");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Stock Type");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Ticket request Id");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Invoice Id");
	    j++;
	    rowhead.createCell((int) j).setCellValue("PO Created Date");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Sales Date");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Sales Person");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Vendor Company");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Vendor Email");
	    j++;
	    
	    rowhead.createCell((int) j).setCellValue("Customer Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Customer Email");
	    j++;
	    CellStyle cellamountStyle = workbook.createCellStyle();
	    cellamountStyle.setDataFormat(
	    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
	    
	    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    int rowvariable = 1;
	    for (Object[] obj : list) {
			 
	    	if(null == obj || obj.length == 0){
	    		continue;
	    	}
	    	int column = 0;
			Row row = ssSheet.createRow(rowvariable);
			row.createCell((int) column).setCellValue(obj[19].toString().replaceAll(",", "-"));//Broker
	        column++;
	        row.createCell((int) column).setCellValue(obj[0].toString().replaceAll(",", "-"));//Event Name
	        column++;
	        row.createCell((int) column).setCellValue(obj[1].toString());//Event Date
	        column++;
        	row.createCell((int) column).setCellValue(obj[2].toString());//Event Time
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[3].toString().replaceAll(",", "-"));//Venue
	    	column++;
	    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	    	shortSectionCell.setCellValue(obj[4].toString().replaceAll(",", "-"));//Section
	        column++;
	        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        shortRowCell.setCellValue(obj[5].toString().replaceAll(",", "-"));//row
	        column++;
	        Cell seatCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        seatCell.setCellValue(obj[6].toString().replaceAll(",", "-"));//seat
	        column++;
	        row.createCell((int) column).setCellValue(obj[7].toString());//Quantity
	        column++;
	        
	        /*Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj[8].toString()));//Retail Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj[9].toString()));//Cost
	        amountCell.setCellStyle(cellamountStyle);
	        column++;*/
	        /*Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj[10].toString()));//Wholesale Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj[11].toString()));//Actual Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj[12].toString()));//Total Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;*/
	        /*row.createCell((int) column).setCellValue(obj[13].toString());//ETicket
	        column++;*/
	        row.createCell((int) column).setCellValue(obj[20].toString().replaceAll(",", "-"));//On Hand
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[21].toString().replaceAll(",", "-"));//Stock Type
	    	column++;
	    	if(obj[22] != null) {
	    		row.createCell((int) column).setCellValue(obj[22].toString());//Ticket Request Id
		    	column++;
	    	} else {
	    		row.createCell((int) column).setCellValue("");//Ticket Request Id
		    	column++;
	    	}
	    	
	        row.createCell((int) column).setCellValue(obj[14].toString());//Invoice Id
	        column++;
	        row.createCell((int) column).setCellValue(obj[23].toString());//PO create date
	        column++;
	        
	        String salesDate = obj[15].toString();//Sales Date
	        if(salesDate != null && !salesDate.isEmpty()) {
	        	 row.createCell((int) column).setCellValue(xlDateFormat.format(dbDateTimeFormat.parse(salesDate)));
	        	 column++;
	        } else {
	        	 row.createCell((int) column).setCellValue("");
	        	 column++;
	        }

	        row.createCell((int) column).setCellValue(obj[16].toString().replaceAll(",", "-"));//Sales Person
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[17].toString().replaceAll(",", "-"));//vendor company name
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[18].toString().replaceAll(",", "-"));//Vendor Email
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[24].toString().replaceAll(",", "-"));//Customer Name
	    	column++;
	    	row.createCell((int) column).setCellValue(obj[25].toString().replaceAll(",", "-"));//Customer Email
	    	column++;
	       
	        rowvariable++;
		}
		return workbook;
	
	}

}
