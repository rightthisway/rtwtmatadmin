package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.pojo.ActionIndicator;
import com.admitone.tmat.pojo.TicketEValStat;
import com.admitone.tmat.web.pojo.WebTicketRow;


public class ActionIndicatorUtil {
	
	private static final int MAX_QTY = 11;

	/** OverPriced Low Priority, OverPriced Medium Priority, OverPriced High Priority,
	 * UnderPriced Low Priority, UnderPriced Medium Priority, UnderPriced High Priority  **/
	private static final String[] AI_VALUE = {"OL","OM","OH","UL","UM","UH","CO"};
	private static Map<Integer, ArrayList<Integer>> matchingQuantities;
	
	//1 = 1,3,4,5,6,7,8,9,10,11
	//2 = 2,4,5,6,7,8,9,10,11
	//3 = 3,5,6,7,8,9,10,11
	//4 = 4,6,7,8,9,10,11
	//5 = 5,7,8,9,10,11
	//6 = 6,8,9,10,11
	//7 = 7,9,10,11
	//8 = 8,10,11
	//9 = 9,11
	//10 = 10,11
	//11 = 11
	static {
		matchingQuantities = new HashMap<Integer, ArrayList<Integer>>();
		matchingQuantities.put(new Integer(0), new ArrayList<Integer>());
		for(int i = 1; i <= MAX_QTY ; i++) {
			ArrayList<Integer> tmpQtys = new ArrayList<Integer>();
			for(int qty = i ; qty <= MAX_QTY ; qty++){
				if(qty != (i + 1) || i == 10){
					tmpQtys.add(new Integer(qty));
				}
			}
			matchingQuantities.put(new Integer(i), tmpQtys);
		}
	}

	/**
	 * getActionsByEvent
	 * 
	 * NOTE: Will only calc AdmitOne Tickets at first
	 * 
	 * Will calculate and return the action indicators for all tickets
	 * in this event;
	 * 
	 * @param tourId
	 * @return Map of ticketId to ActionIndicator
	 */
	public static Map<Integer, Map<String, Integer>> getActionsByTour(Collection<Event> events) {
		
		Map<Integer, Map<String, Integer>> eventToActions = new HashMap<Integer, Map<String, Integer>>();
	
		if(events == null || events.isEmpty()){
			return null;
		}
		for(Event event : events){
			try {
				int overHigh = 0;
				int overMed = 0;
				int overLow = 0;
				int underHigh = 0;
				int underMed = 0;
				int underLow = 0;
				int correct = 0;
				
				boolean acquired = false;
				Collection<Ticket> tickets = null;
				tickets	= DAORegistry.getTicketDAO().getAllActiveRegularTicketsByEvent(event.getId());
				Map<Integer, ActionIndicator> actions = getActionsForTickets(tickets, false);
				//System.out.println("ACTIONUTIL: " + actions);
				//System.out.println("matchingQuantities: " + matchingQuantities);
				
				if(actions == null || actions.isEmpty()){
					continue;
				}
				for(ActionIndicator action: actions.values()){
					if(action.getTgCheaper() <= 0){
						if(action.getTgCheaper() < 2){
							correct++;
						} else {
							if(action.getTgCheaper() < 4){
								overLow++;
							} else {
								if(action.getTgCheaper() < 7){
									overMed++;
								} else {
									overHigh++;
								}
							}
						}
					} else {
						if(action.getPriceDifference() <= 5.00){
							correct++;
						} else {
							if(action.getPriceDifference() <= 10.00){
								underLow++;
							} else {
								if(action.getPriceDifference() <= 20.00) {
									underMed++;
								} else {
									underHigh++;
								}
							}
						}
					}
				}
				Map<String, Integer> tempMap = new HashMap<String, Integer>();
				tempMap.put(AI_VALUE[0], new Integer(overLow));
				tempMap.put(AI_VALUE[1], new Integer(overMed));
				tempMap.put(AI_VALUE[2], new Integer(overHigh));
				tempMap.put(AI_VALUE[3], new Integer(underLow));
				tempMap.put(AI_VALUE[4], new Integer(underMed));
				tempMap.put(AI_VALUE[5], new Integer(underHigh));
				tempMap.put(AI_VALUE[6], new Integer(correct));
				
				eventToActions.put(event.getId(), tempMap);
			} catch (Exception e) {
				System.out.println("Caught Exception waiting for scheduler in ActionIndicatorUtil");
			}
		}
		return eventToActions;
	}

	/**
	 * Get Actions Indicators for Tickets
	 * @param unfTickets
	 * @param filteredTix
	 * @return
	 */
	public static Map<Integer, ActionIndicator> getActionsForTickets(List<WebTicketRow> unfTickets, boolean filteredTix) {

		// Map of ticketId to action
		Map<Integer, ActionIndicator> actions = new HashMap<Integer, ActionIndicator>();

		//To start, A1Tix will be the only tickets to get AI's
		if(!TicketUtil.containsAdmitOneWebTickets(unfTickets)){
			return null;
		}
		Collection<WebTicketRow> aOneTickets = new ArrayList<WebTicketRow>();
		
		//Map of evalNumber to eval
		Map<Integer, TicketEValStat> eValStats = new HashMap<Integer, TicketEValStat>();
		
		//Create the stats
		for(WebTicketRow ticket : unfTickets){
			if(ticket.isAdmitOneTicket()){
				aOneTickets.add(ticket);
			}
			Integer valFactor = ticket.getValuationFactor();
			if(valFactor == null){
				continue;
			}
			((WebTicketRow)ticket).setEval(valFactor);
			TicketEValStat eValStat = eValStats.get(valFactor);
			if(eValStat == null){
				TicketEValStat newStat = new TicketEValStat(valFactor, ticket);
				eValStats.put(valFactor, newStat);
			} else {
				eValStat.addeValTicket(ticket);
				if(ticket.getAdjustedCurrentPrice() > eValStat.getMaxPrice()){
					eValStat.setMaxPrice(ticket.getAdjustedCurrentPrice());
				} else if(ticket.getAdjustedCurrentPrice() < eValStat.getMinPrice()){
					eValStat.setMinPrice(ticket.getAdjustedCurrentPrice());
				}
			}
		}
		
		for(WebTicketRow a1Ticket : aOneTickets){
			Integer valFactor = a1Ticket.getEval();
			if(valFactor != null){
					
				ActionIndicator action = new ActionIndicator();
		
				action.seteVal(valFactor);
				setAction(action, a1Ticket, eValStats, filteredTix);
				actions.put(a1Ticket.getId(), action);
			}
		}
		return actions;
	}
	
	/**
	 * getActionsByEvent
	 * 
	 * NOTE: Will only calc AdmitOne Tickets at first
	 * 
	 * Will calculate and return the action indicators for all tickets
	 * in this event;
	 * 
	 * @param tickets - Collection of tickets to compute
	 * @param event - event of the tickets
	 * @return Map of ticketId to ActionIndicator
	 */
	public static Map<Integer, ActionIndicator> getActionsForTickets(Collection<Ticket> unfTickets, boolean filteredTix) {
		
		Collection<Ticket> tickets = TicketUtil.removeDuplicateTickets(unfTickets);
		
		// Map of ticketId to action
		Map<Integer, ActionIndicator> actions = new HashMap<Integer, ActionIndicator>();

		//To start, A1Tix will be the only tickets to get AI's
		if(!TicketUtil.containsAdmitOneTickets(tickets)){
			return null;
		}
		Collection<Ticket> aOneTickets = new ArrayList<Ticket>();
		
		//Map of evalNumber to eval
		Map<Integer, TicketEValStat> eValStats = new HashMap<Integer, TicketEValStat>();
		
		//Create the stats
		for(Ticket ticket : tickets){
			if(ticket.isAdmitOneTicket()){
				aOneTickets.add(ticket);
			}
			Integer valFactor = ticket.getValuationFactor();
			if(valFactor == null){
				continue;
			}
			TicketEValStat eValStat = eValStats.get(valFactor);
			if(eValStat == null){
				TicketEValStat newStat = new TicketEValStat(valFactor, ticket);
				eValStats.put(valFactor, newStat);
			} else {
				eValStat.addeValTicket(ticket);
				if(ticket.getAdjustedCurrentPrice().doubleValue() > eValStat.getMaxPrice()){
					eValStat.setMaxPrice(ticket.getAdjustedCurrentPrice().doubleValue());
				} else if(ticket.getAdjustedCurrentPrice().doubleValue() < eValStat.getMinPrice()){
					eValStat.setMinPrice(ticket.getAdjustedCurrentPrice().doubleValue());
				}
			}
		}
		

		//Apply the stats to find TGCheaper and Underprice
		if(aOneTickets.isEmpty()){
			return null;
		}
		//System.out.println("ACTIONUTILSIZE for id: " + ((ArrayList<Ticket>)aOneTickets).get(1).getEventId() + " size: " + actions.size());
		
		for(Ticket a1Ticket : aOneTickets){
			Integer valFactor = a1Ticket.getValuationFactor();
			if(valFactor != null){
					
				ActionIndicator action = new ActionIndicator();
		
				action.seteVal(valFactor);
				setAction(action, a1Ticket, eValStats, filteredTix);
				actions.put(a1Ticket.getId(), action);
			}
		}
		return actions;
	}
	
	private static void setAction(ActionIndicator action, Ticket ticketEvaluated, Map<Integer, TicketEValStat> eValStats, boolean filteredTix){
	
		double price = ticketEvaluated.getAdjustedCurrentPrice().doubleValue();
		double underprice = Double.MAX_VALUE;
		double overprice = Double.MAX_VALUE;
		double priceDiff = Double.MAX_VALUE;
		int tgCheaper = 0;

		ArrayList<Integer> qtys = matchingQuantities.get(ticketEvaluated.getRemainingQuantity());
		if(qtys == null && ticketEvaluated.getRemainingQuantity().intValue() >= 11){
			qtys = new ArrayList<Integer>();
		}

		for(TicketEValStat eVal : eValStats.values()){
			if(eVal.geteVal() > action.geteVal()){
				continue;
			}
			if((price < eVal.getMinPrice()) && (underprice <= eVal.getMinPrice() || price > eVal.getMaxPrice())){
				continue;
			}
			for(Ticket tgTicket : eVal.geteValTickets()){
				if(filteredTix || (tgTicket.getRemainingQuantity().intValue() >= 11) 
						&& !qtys.contains(tgTicket.getRemainingQuantity())) {
					continue;
				}
				if(tgTicket.isAdmitOneTicket()){
					continue;
				}
				
				if(price >= tgTicket.getAdjustedCurrentPrice()){
						if(tgTicket.getAdjustedCurrentPrice() < overprice){
							if(overprice != Double.MAX_VALUE){
								action.setOverpriceNext(overprice);
							}
							overprice = tgTicket.getAdjustedCurrentPrice();
						} else if((tgTicket.getAdjustedCurrentPrice() < action.getOverpriceNext())
							|| (action.getOverpriceNext() == 0.0)){
							action.setOverpriceNext(tgTicket.getAdjustedCurrentPrice());
						}
						tgCheaper++;
				} else {
					if(underprice >= tgTicket.getAdjustedCurrentPrice()){
						if(underprice != Double.MAX_VALUE){
							action.setUnderpriceNext(underprice);
						}
						underprice = tgTicket.getAdjustedCurrentPrice();
						priceDiff = tgTicket.getAdjustedCurrentPrice() - ticketEvaluated.getAdjustedCurrentPrice().doubleValue();
					} else if((action.getUnderpriceNext() >= tgTicket.getAdjustedCurrentPrice())
							|| (action.getUnderpriceNext() == 0.0)){
						action.setUnderpriceNext(tgTicket.getAdjustedCurrentPrice());
					}
				}
			}
		}
		if(underprice != Double.MAX_VALUE) {
			action.setUnderprice(underprice);
			action.setPriceDifference(priceDiff);
		}
		if(overprice != Double.MAX_VALUE) {
			action.setOverprice(overprice);
		}
		action.setTgCheaper(tgCheaper);
	}
	
}
