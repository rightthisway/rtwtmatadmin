package com.admitone.tmat.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class TndFeedEventsBackUp extends QuartzJobBean implements StatefulJob {

	private boolean running;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		if(!running){
			try{
				File file = new File("\\\\C:\\external files\\TNDataFeed\\TicketNetworkDataFeed-Events.csv");
				System.out.println(file.exists());
				SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy-hh-mmaa");
				if(file.exists()){
					File file2 = new File("\\\\C:\\external files\\TNDataFeed\\backup\\TicketNetworkDataFeed-Events-"+format.format(new Date())+".csv");
					FileChannel src = new FileInputStream(file).getChannel();
					FileChannel dest = new FileOutputStream(file2).getChannel();
					dest.transferFrom(src, 0, src.size());
					
					src.close();
					dest.close();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

}
