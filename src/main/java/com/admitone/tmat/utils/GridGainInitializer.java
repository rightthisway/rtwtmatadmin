package com.admitone.tmat.utils;

import org.gridgain.grid.GridFactory;

public class GridGainInitializer {
	public GridGainInitializer() throws Exception {
		GridFactory.start();
		System.out.println("*** GRIDGAIN started");
	}
}
