package com.admitone.tmat.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Connections {
	private static Logger logger = LoggerFactory.getLogger(Connections.class);
	
	private static Connection tmatConnection;
	private static Connection rtwPosConnection;
	private static Connection rtwTwoPosConnection;
	private static Connection tixCityPosConnection;
	private static Connection rewardTheFanConnection;
	
	public static Connection getTmatConnection(){
		try {
			if(tmatConnection!=null && !tmatConnection.isClosed()){
				return tmatConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("TMAT Conection Exception....");
			logger.error("TMAT Conection Exception....");
		}
		//System.out.println("TMAT New Conection....");
		logger.info("TMAT Nwe Conection....");
		
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tmat.db.url");
		String username =resourceBundle.getString("tmat.db.user.name");
		String password =resourceBundle.getString("tmat.db.password");
		*/
		String url ="jdbc:jtds:sqlserver://10.0.1.51:1433/tmatprodscale;sendStringParametersAsUnicode=false";
		String username ="Tayo";
		String password ="po01ro02ro30";
		
		
		/*System.out.println("url....."+url);
		System.out.println("username....."+username);
		System.out.println("password....."+password);*/
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tmatConnection = DriverManager.getConnection(url, username, password);
			return tmatConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	public static Connection getRewardTheFanConnection(){
		try {
			if(rewardTheFanConnection!=null && !rewardTheFanConnection.isClosed()){
				return rewardTheFanConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("RTF Conection Exception....");
			logger.error("RTF Conection Exception....");
		}
		//System.out.println("TMAT New Conection....");
		logger.info("RTF Nwe Conection....");
		
		/*ResourceBundle resourceBundle = ResourceBundle.getBundle("commonsettings");
		String url =resourceBundle.getString("tmat.db.url");
		String username =resourceBundle.getString("tmat.db.user.name");
		String password =resourceBundle.getString("tmat.db.password");
		*/
		String url ="jdbc:jtds:sqlserver://10.0.1.49:1433/ZonesApiPlatformV2;sendStringParametersAsUnicode=false";
		String username ="Tayo";
		String password ="po01ro02ro30";
		
		
		/*System.out.println("url....."+url);
		System.out.println("username....."+username);
		System.out.println("password....."+password);*/
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rewardTheFanConnection = DriverManager.getConnection(url, username, password);
			return rewardTheFanConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public static Connection getRTWPOSConnection(){
		try {
			if(rtwPosConnection!=null && !rtwPosConnection.isClosed()){
				return rtwPosConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("RTW POS Conection Exception....");
			logger.error("RTW POS Conection Exception....");
		}
		logger.info("RTW POS Nwe Conection....");
		
		
		//String url ="jdbc:jtds:sqlserver://206.71.237.195:1433/indux;sendStringParametersAsUnicode=false";
		String url ="jdbc:jtds:sqlserver://172.254.101.202:1433/indux;sendStringParametersAsUnicode=false";
		String username ="Tayo";
		String password ="po01ro02ro30";
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtwPosConnection = DriverManager.getConnection(url, username, password);
			return rtwPosConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	//RTW - 172.254.101.202
	//RTW 2 - 172.254.101.200
	
	public static Connection getRTWTwoPOSConnection(){
		try {
			if(rtwTwoPosConnection!=null && !rtwTwoPosConnection.isClosed()){
				return rtwTwoPosConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("RTW-2 POS Conection Exception....");
			logger.error("RTW-2 POS Conection Exception....");
		}
		logger.info("RTW-2 POS Nwe Conection....");
		
		
		//String url ="jdbc:jtds:sqlserver://206.71.237.202:1433/indux;sendStringParametersAsUnicode=false";
		String url ="jdbc:jtds:sqlserver://172.254.101.200:1433/indux;sendStringParametersAsUnicode=false";
		String username ="Tayo";
		String password ="po01ro02ro30";
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			rtwTwoPosConnection = DriverManager.getConnection(url, username, password);
			return rtwTwoPosConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	public static Connection getTixCityPOSConnection(){
		try {
			if(tixCityPosConnection!=null && !tixCityPosConnection.isClosed()){
				return tixCityPosConnection;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("TixCity POS Conection Exception....");
			logger.error("TixCity POS Conection Exception....");
		}
		logger.info("TixCity POS Nwe Conection....");
		
		
		String url ="jdbc:jtds:sqlserver://TIXCITY.DDNS.NET:1433/indux;sendStringParametersAsUnicode=false";
		String username ="Tayo";
		String password ="po01ro02ro30";
		
		try{
			String driver = "net.sourceforge.jtds.jdbc.Driver";
			Class.forName(driver);
			tixCityPosConnection = DriverManager.getConnection(url, username, password);
			return tixCityPosConnection;
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	public static Connection getPosConnectionByBrokername(String brokerName) {
		
		if(brokerName.equalsIgnoreCase("RTW")) {
			return getRTWPOSConnection();
		} else if(brokerName.equalsIgnoreCase("RTW-2")) {
			return getRTWTwoPOSConnection();
		} else if(brokerName.equalsIgnoreCase("Tix City")) {
			return getTixCityPOSConnection();
		}
		return null;
	}
	public static ResultSet getResultSet(Statement statement,String sql){
		try{
			return statement.executeQuery(sql);
		}catch(SQLException sqlEx){
			return null;
		}
	}
	

	public static CallableStatement getCallableStatement(Connection connnection,String sql){
		try{
			return connnection.prepareCall(sql);
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			return null;
		}
		
	}
}
