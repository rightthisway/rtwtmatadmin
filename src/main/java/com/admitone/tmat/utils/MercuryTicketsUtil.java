package com.admitone.tmat.utils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.dao.hibernate.MercuryTicketsQueryManager;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TNInstantEvent;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;

/**
 * a. Computing the final set of TicketNetwork mercury tickets
 * b. Writing the TicketNetwork mercury tickets to CSV file
 */


public class MercuryTicketsUtil {

	private MercuryTicketsUtil() {}
	
	public static void downloadMercuryTicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=MercuryTickets_" + today + ".csv");
		Collection<Ticket> mercuryTickets = computeMercuryTickets();
		downloadMercuryTicketCsvFile(mercuryTickets, response.getOutputStream());
	}
	
	/*public static void downloadCustomizedMercuryTicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=CustomizedMercuryTickets_" + today + ".csv");
		Collection<Ticket> mercuryTickets = computeCustomizedCatMercuryTickets();
		downloadCustomizedMercuryTicketCsvFile(mercuryTickets, response.getOutputStream());
	}*/
	
	public static  void downloadMercuryTicketCsvFile(Collection<Ticket> mercuryTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> mercuryTickets = computeMercuryTickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
//		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		StringBuffer row = new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n");
		int i=0;
		Property notesProperty = DAORegistry.getPropertyDAO().get("mercuryTickets.notes");
		for (Ticket mercuryTicket: mercuryTickets) {
			i++;
			String timeStr;
			if (mercuryTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(mercuryTicket.getEventTime());
			}

			String seatFrom = "";
			String seatThru = "";

			int quantity = mercuryTicket.getRemainingQuantity();
			double varPrice = 10.00;
			String price = numberFormat.format(((mercuryTicket.getCurrentPrice())*1.15)+(varPrice/quantity));
	//		double price = (((mercuryTicket.getCurrentPrice())*1.15)+(varPrice/quantity));
			
			String notes = null;
			if (mercuryTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
			notes = "";
			}
			
			row.append("y,");
			row.append(mercuryTicket.getEventName() + ","); 
			row.append(mercuryTicket.getVenue() + "," );
			row.append(dateFormat.format(mercuryTicket.getEventDate()) + ",");
			row.append(timeStr.trim() + ",");
			row.append(mercuryTicket.getRemainingQuantity() + ",");
			row.append(mercuryTicket.getNormalizedSection() + ",");
			row.append(mercuryTicket.getRow() + ",");
			row.append(seatFrom + ",");
			row.append(seatThru + ",");
			row.append(notes +",");
			row.append(price + ",");
			row.append(mercuryTicket.getId()+ "\n");
			if(i>200){
				i=0;
				outputStream.write(row.toString().getBytes());
				row=null;
				row= new StringBuffer("");
			}
		}
		outputStream.write(row.toString().getBytes());
	}
	
	public static  void downloadMercuryTNDTicketCsvFile(Collection<Ticket> mercuryTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> mercuryTickets = computeMercuryTickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
//		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		StringBuffer row = new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n");
		int i=0;
		Date now = new Date();
		Property notesProperty = DAORegistry.getPropertyDAO().get("mercuryTickets.notes");
		
		
		for (Ticket mercuryTicket: mercuryTickets) {
			i++;
			String timeStr;
			if (mercuryTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(mercuryTicket.getEventTime());
			}

			String seatFrom = "";
			String seatThru = "";

			int quantity = mercuryTicket.getRemainingQuantity();
			double varPrice = 10.00;
			String price = numberFormat.format(((mercuryTicket.getCurrentPrice())*1.15)+(varPrice/quantity));
	//		double price = (((mercuryTicket.getCurrentPrice())*1.15)+(varPrice/quantity));
			
			String notes = null;
			if (mercuryTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
			notes = "";
			}
			
			int emailOption=1;
			long hrs=(mercuryTicket.getEventDate().getTime()- now.getTime()) /(1000*60*60);
			if(hrs > 50)
				emailOption=0;
			
			row.append("y,");
			row.append(mercuryTicket.getEventName() + ","); 
			row.append(mercuryTicket.getVenue() + "," );
			row.append(dateFormat.format(mercuryTicket.getEventDate()) + ",");
			row.append(timeStr.trim() + ",");
			row.append(mercuryTicket.getRemainingQuantity() + ",");
			row.append(mercuryTicket.getNormalizedSection() + ",");
			row.append(mercuryTicket.getRow() + ",");
			row.append(seatFrom + ",");
			row.append(seatThru + ",");
			row.append(notes +",");
			row.append(price + ",");
			row.append(mercuryTicket.getId()+ ",");
			row.append(",");
			row.append(",");
			row.append(emailOption +",");
			row.append(2 +",");
			row.append(dateFormat.format(now) +",");
			row.append("\n");
			if(i>200){
				i=0;
				outputStream.write(row.toString().getBytes());
				row=null;
				row= new StringBuffer("");
			}
		}
		outputStream.write(row.toString().getBytes());
	}
	
	public static  void downloadCustomizedMercuryTicketCsvFile(Collection<Ticket> mercuryTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> mercuryTickets = computeCustomizedCatMercuryTickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
//		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		StringBuffer row = new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n");
		int i=0;
		Property notesProperty = DAORegistry.getPropertyDAO().get("mercuryTickets.notes");
		
		for (Ticket mercuryTicket: mercuryTickets) {
			i++;
			String timeStr;
			if (mercuryTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(mercuryTicket.getEventTime());
			}
			String seatFrom = "";
			String seatThru = "";
		
			String price = numberFormat.format(mercuryTicket.getCurrentPrice());
			
			String notes = null;
			if (mercuryTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
			notes = "";
			}
			
			row.append("y,");
			row.append(mercuryTicket.getEventName() + ","); 
			row.append(mercuryTicket.getVenue() + "," );
			row.append(dateFormat.format(mercuryTicket.getEventDate()) + ",");
			row.append(timeStr.trim() + ",");
			row.append(mercuryTicket.getRemainingQuantity() + ",");
			row.append(mercuryTicket.getNormalizedSection() + ",");
			row.append(mercuryTicket.getRow() + ",");
			row.append(seatFrom + ",");
			row.append(seatThru + ",");
			row.append(notes +",");
			row.append(price + ",");
			row.append(mercuryTicket.getId()+ "\n");
			if(i>200){
				i=0;
				outputStream.write(row.toString().getBytes());
				row=null;
				row= new StringBuffer("");
			}
		}
		outputStream.write(row.toString().getBytes());
	}
	
	public static  void downloadCustomizedMercuryTNDTicketCsvFile(Collection<Ticket> mercuryTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> mercuryTickets = computeCustomizedCatMercuryTickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
//		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		StringBuffer row = new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n");
		int i=0;
		Date now = new Date();
		Property notesProperty = DAORegistry.getPropertyDAO().get("mercuryTickets.notes");
		
		for (Ticket mercuryTicket: mercuryTickets) {
			i++;
			String timeStr;
			if (mercuryTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(mercuryTicket.getEventTime());
			}
			String seatFrom = "";
			String seatThru = "";
		
			String price = numberFormat.format(mercuryTicket.getCurrentPrice());
			
			String notes = null;
			if (mercuryTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
			notes = "";
			}
			
			int emailOption=1;
			long hrs=(mercuryTicket.getEventDate().getTime()- now.getTime()) /(1000*60*60);
			if(hrs > 50)
				emailOption=0;
			
			row.append("y,");
			row.append(mercuryTicket.getEventName() + ","); 
			row.append(mercuryTicket.getVenue() + "," );
			row.append(dateFormat.format(mercuryTicket.getEventDate()) + ",");
			row.append(timeStr.trim() + ",");
			row.append(mercuryTicket.getRemainingQuantity() + ",");
			row.append(mercuryTicket.getNormalizedSection() + ",");
			row.append(mercuryTicket.getRow() + ",");
			row.append(seatFrom + ",");
			row.append(seatThru + ",");
			row.append(notes +",");
			row.append(price + ",");
			row.append(mercuryTicket.getId()+ ",");
			row.append(",");
			row.append(",");
			row.append(emailOption +",");
			row.append(2 +",");
			row.append(dateFormat.format(now) +",");
			row.append("\n");
			
			if(i>200){
				i=0;
				outputStream.write(row.toString().getBytes());
				row=null;
				row= new StringBuffer("");
			}
		}
		outputStream.write(row.toString().getBytes());
	}
	
	/*Only non duplicate, non AO, categorized TicketNetwork Mercury tickets are considered*/
	public static Collection<Ticket> computeMercuryTickets() {
	
		Collection<Ticket> filteredMercuryTickets = new ArrayList<Ticket>();
		Collection<Ticket> filteredUniqueMercuryTickets = new ArrayList<Ticket>();
		HashMap<Integer,Collection<Ticket>> mercuryTicketsByEvent = null;//DAORegistry.getMercuryTicketsQueryManager().getAllMercuryTickets();
		Property property = DAORegistry.getPropertyDAO().get("mercuryTickets.eventIds");
		String eventIds = property.getValue();
		for (String eventIdStr: eventIds.split(",")) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
			Integer eventId = Integer.valueOf(eventIdStr.trim());
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
				if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
	
			Collection<Ticket> mercuryTickets = mercuryTicketsByEvent.get(eventId);
			
//			if(mercuryTickets != null){
//				System.out.println("Size of mercuryTickets for event id: " + eventId + " is: " + mercuryTickets.size());
//			}else{
//				System.out.println("Size of mercuryTickets for event id: " + eventId + " is: null");
//			}
	
			List<String> catScheme = Categorizer.getCategoryGroupsByVenueId(event.getVenueId());
			
		//	Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		//	List<Ticket> nonTNTickets  = new ArrayList<Ticket>();
		//	for (Ticket ticket:tickets){
			//	if (!ticket.getSiteId().equalsIgnoreCase("ticketnetwork")){
			//		nonTNTickets.add(ticket);
			//	}
			//}
			List<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		//	tmpFilteredTickets = TicketUtil.removeDuplicateTickets(nonTNTickets, RemoveDuplicatePolicy.SUPER_SMART);
		//	Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(eventId);
	
			for (String catscheme : catScheme) {
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catscheme);
				Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
				Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
				Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
				for(CategoryMapping mapping:categoryMappingList){
					List<CategoryMapping> list =catMappingMap.get(mapping.getId());
					if(list==null){
						list = new ArrayList<CategoryMapping>();
					}
					list.add(mapping);
					catMappingMap.put(mapping.getId(), list);
				}
				if (mercuryTickets != null && !mercuryTickets.isEmpty()){
				for (Ticket mercuryTicket : mercuryTickets) {
		//				if(!TicketUtil.isInInventory(mercuryTicket, aotickets)){
					//		if (ticket.getId().equals(mercuryTicket.getTicketId())){
								int quantity = mercuryTicket.getRemainingQuantity();
								double varPrice = 10.00 ;
								double price = ((mercuryTicket.getCurrentPrice()*1.15)+(varPrice/quantity));
								if(quantity*price>100 && price<20000){
									Category cat = Categorizer.computeCategory(event.getVenueCategory(), mercuryTicket.getNormalizedSection(),mercuryTicket.getRow(), catscheme,categoryMap,catMappingMap);	
									if (cat != null) {
										filteredMercuryTickets.add(mercuryTicket);		
									}
								}
							}
						}
					}
				}
			}
			}
	//	}	
	//	System.out.println("Mercury tix filtered coll size" +filteredMercuryTickets.size());
	//	return filteredMercuryTickets;
		filteredUniqueMercuryTickets = getUniqueValues (filteredMercuryTickets);
		return filteredUniqueMercuryTickets;
	}

	/*public static Collection<Ticket> computeCustomizedMercuryTickets(Integer eventId, Collection<Ticket> mercuryTickets) {
		Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();		
			Event event = DAORegistry.getEventDAO().get(eventId);
			TNInstantEvent instantEvent = DAORegistry.getTninstantEventDAO().get(eventId);
			Integer expiryTime = instantEvent.getExpiryTime();
			
			long expiryEventTime  = expiryTime * 60L * 60L * 1000L ;
			
			if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
		//		if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
					Date date = event.getDate();
					Date time = null;
					
					if (event.getTime()==null){
						time = event.getDate();
					}else{
						time = event.getTime();
						}
						Calendar calDate = new GregorianCalendar();
						Calendar calTime = new GregorianCalendar();
						calDate.setTime(date);
						calTime.setTime(time);
					
						Calendar caldatetime = new GregorianCalendar();
						caldatetime.set(Calendar.YEAR, calDate.get(Calendar.YEAR));
						caldatetime.set(Calendar.MONTH, calDate.get(Calendar.MONTH));
						caldatetime.set(Calendar.DATE, calDate.get(Calendar.DATE));
						caldatetime.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
						caldatetime.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
						caldatetime.set(Calendar.SECOND, calTime.get(Calendar.SECOND));
						Date eventDateTime = caldatetime.getTime();
						long difference = eventDateTime.getTime() - new Date().getTime();
						
						if (difference>expiryEventTime){
				//	if (event.getDate().getTime()- new Date().getTime()>expiryEventTime){	
						Double markup = instantEvent.getMarkupPercent();
						Double minThreshold = instantEvent.getMinThreshold();
						Double maxThreshold = instantEvent.getMaxThreshold();
						Double salesPercent = instantEvent.getSalesPercent();
						Double shippingFee = instantEvent.getShippingFee();
		
			List<String> catScheme = Categorizer.getCategoryGroupsByVenueId(event.getVenueId());
					
			for (String catscheme : catScheme) {
				
				*//** 
				 *  If it is used from any place, make sure you use correct scheme and remove for loop..
				 *//*
				Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
				
				Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
				Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
				for(CategoryMapping mapping:categoryMappingList){
					List<CategoryMapping> list =catMappingMap.get(mapping.getId());
					if(list==null){
						list = new ArrayList<CategoryMapping>();
					}
					list.add(mapping);
					catMappingMap.put(mapping.getId(), list);
				}
				
				
				if (mercuryTickets!=null){
				for (Ticket mercuryTicket : mercuryTickets) {
								int quantity = mercuryTicket.getRemainingQuantity();
								double markupPrice = 0, salesPrice = 0,  shippingPrice = 0, totalPrice = 0;
								double price = mercuryTicket.getCurrentPrice();
							
								if (markup!= null){
									markupPrice = (price * ((100 + markup) / 100)) - price;
								}
								if (salesPercent != null){
									salesPrice  =( price * (100 +salesPercent) /100 ) - price;
								}
								if (shippingFee!=null || quantity!=0){
									shippingPrice = shippingFee/quantity;
								}
								totalPrice = price + markupPrice + salesPrice + shippingPrice ;
								mercuryTicket.setCurrentPrice(totalPrice);
								if(quantity*totalPrice>minThreshold && totalPrice<maxThreshold){
									Category cat = Categorizer.computeCategory(event.getVenueCategory(), mercuryTicket.getNormalizedSection(),mercuryTicket.getRow(), catscheme,categoryMap,catMappingMap);	
									if (cat != null) {
										mercuryTicket.setCategoryId(cat.getId());
										mercuryTicket.setCategory(cat);
										tmpFilteredTickets.add(mercuryTicket);
									}
								}
							}		
				}
				}
					}
				}
			else if (event!=null && !event.getEventStatus().equals(EventStatus.ACTIVE)){
				TNEventManager.deleteInstantEvents(eventId);
			}
		return tmpFilteredTickets;
	}


	public static Collection<Ticket> computeCustomizedCatMercuryTickets(){
		Collection<Ticket> filteredCatTickets = new ArrayList<Ticket>();
		Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		Collection<Ticket> tnFilteredTickets = new ArrayList<Ticket>();
		Collection<Ticket> filteredUniqueTNTickets = new ArrayList<Ticket>();
		Collection<TNInstantEvent> instantEvents = DAORegistry.getTninstantEventDAO().getAll();
		HashMap<Integer,Collection<Ticket>> mercuryTicketsByEvent = DAORegistry.getMercuryTicketsQueryManager().getAllMercuryTickets();
		for (TNInstantEvent instantEvent:instantEvents){
				Integer eventId = instantEvent.getEventId();
				tmpFilteredTickets = computeCustomizedMercuryTickets(eventId, mercuryTicketsByEvent.get(eventId));
				filteredCatTickets = getCatTNTickets(tmpFilteredTickets);
				tnFilteredTickets.addAll(filteredCatTickets);
			}
		filteredUniqueTNTickets = getUniqueValues (tnFilteredTickets);
		return filteredUniqueTNTickets;
	}*/
	
	
	public static Collection<Ticket> getCatTNTickets(Collection<Ticket> tmpFilteredTickets){
		Map<Integer, List<Ticket>> catTickets = new HashMap<Integer, List<Ticket>>();
		Collection<Ticket> filteredInstantTickets = new ArrayList<Ticket>();
				for (Ticket instantTicket:tmpFilteredTickets){
					List<Ticket> list = (List<Ticket>) catTickets.get(instantTicket.getCategory().getId());
					if (list == null) {
					list = new ArrayList<Ticket>();
						catTickets.put(instantTicket.getCategory().getId(), list);
				}
					list.add(instantTicket);
				}
				filteredInstantTickets = orderCatTickets(catTickets);
			return filteredInstantTickets;
	}
	
	
	public static Collection<Ticket> orderCatTickets(Map<Integer, List<Ticket>> catTicketList){
		Collection<Ticket> orderedTickets = new ArrayList<Ticket>();
		List<Ticket> ticketsByCat = new ArrayList<Ticket>();
		for (Integer categoryId: catTicketList.keySet()){
			ticketsByCat= catTicketList.get(categoryId);
				Collections.sort(ticketsByCat, new Comparator<Ticket>(){
					@Override
					public int compare(Ticket t1, Ticket t2){
						if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())>0){
							return -1;
						} else if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())<0){
							return 1;
						}else 
							return 0;
					}
				});
				if (ticketsByCat.size()>1){
					ticketsByCat.remove(ticketsByCat.size()-1);
					orderedTickets.addAll(ticketsByCat);
				}else {
					orderedTickets.addAll(ticketsByCat);
				}
		}
		return orderedTickets;
	}
	
	public static void downloadCustomizedScrubedMercuryTNDTicketCsvFile(OutputStream outputStream) {
		Collection<Ticket> tickets= null;// DAORegistry.getMercuryTicketsQueryManager().getScrubedMercuryTickets();
		int i=0;
//		BufferedWriter writer = new FileWriter(outputStream);
		try{
			DateFormat dateFormat= new SimpleDateFormat("MM/dd/yyyy");
			DateFormat timeFormat= new SimpleDateFormat("hh:mm:ss aa");
			
		StringBuffer buffer= new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n");
		for(Ticket temp: tickets){
			if(++i%200==0){
				outputStream.write(buffer.toString().getBytes());
				buffer=null;
				buffer =new StringBuffer("Y," + temp.getEventName() + "," +  temp.getVenue() + "," + dateFormat.format(temp.getEventDate()) + "," + timeFormat.format(temp.getEventTime()) + "," + temp.getQuantity()+ "," +temp.getSection()+ "," + temp.getRow() + ",,,," + temp.getBuyItNowPrice() + "," + temp.getId() + "\n");
				
		    }else{
		    	buffer.append("Y," + temp.getEventName() + "," +  temp.getVenue() + "," + dateFormat.format(temp.getEventDate()) + "," + timeFormat.format(temp.getEventTime()) + "," + temp.getQuantity()+ "," +temp.getSection()+ "," + temp.getRow() + ",,,," + temp.getBuyItNowPrice() + "," + temp.getId() + "\n");
		    }
		}
		outputStream.write(buffer.toString().getBytes());
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			tickets=null;
		}
	}
	public static Collection<Ticket> getUniqueValues(Collection<Ticket> filteredMercuryTickets){
	    return (Collection<Ticket>)Union(filteredMercuryTickets, filteredMercuryTickets);
	}

	public static Collection<Ticket> Union(Collection<Ticket> coll1, Collection<Ticket> coll2){
	    Set<Ticket> union = new HashSet<Ticket>(coll1);
	    union.addAll(new HashSet<Ticket>(coll2));
	    return new ArrayList<Ticket>(union);
	}
}