package com.admitone.tmat.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class TimezoneUtil {
	private static final String[] WESTERN_EUROPE = {
		"IS", "PT", "IE", "UK", "GB"
	};

	private static final String[] CENTRAL_EUROPE = {
		"AL", "AD", "AT", "BE", "BA",
		"HR", "CZ", "DK", "DE", "FR", "GI",  
		"HU", "IT", "LI", "LU", "MK",
		"MT", "MC", "MN", "NL", "NO",
		"PO", "SM", "SK", "ES", "SE",
		"CH", "VA"
	};

	private static final String[] EASTERN_EUROPE = {
		"BY", "BG", "CY", "EE", "GR", "LV",
		"LT", "MD", "RO", "UA" 
	};
	
	private static final String[] MOSCOW = {
		"RU"
	};

	private static boolean isInArray(String value, String[] values) {
		for (String cVal: values) {
			if (value.equals(cVal)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isEuropean(String countryCode) {
		if (isInArray(countryCode, WESTERN_EUROPE)) {
			return true;
		}

		if (isInArray(countryCode, CENTRAL_EUROPE)) {
			return true;
		}

		if (isInArray(countryCode, EASTERN_EUROPE)) {
			return true;
		}

		if (isInArray(countryCode, MOSCOW)) {
			return true;
		}

		return false;
	}
	
	public static Date getSummerTimeChangeDate(int year) {
		int day = 31 - (((5 * year) / 4) % 7);
		Calendar cal = new GregorianCalendar(year, 2, day, 1, 0, 0);
		return cal.getTime();
	}
	
	/*
	 * Formula to get the date at which we change from winter to summer time is:
	 * Sunday (31 - (5y / 4 + 4) mod 7) March at 01:00 GMT
	 */
	public static boolean isSummerTime(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		
		int year = cal.get(Calendar.YEAR);
		Date change = getSummerTimeChangeDate(year);
		if (date.after(change)) {
			return true;
		}
		
		return false;
	}
	
	public static Date getLocalTimeFromGMT(Date date, String countryCode) {
		long offset = 0L;
		
		if (isInArray(countryCode, CENTRAL_EUROPE)) {
			offset = 1L;
		} else if (isInArray(countryCode, EASTERN_EUROPE)) {
			offset = 2L;
		} else if (isInArray(countryCode, MOSCOW)) {
			offset = 3L;
		}
		
		return new Date(date.getTime() + offset * 3600000L);
	}
}
