package com.admitone.tmat.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.mail.MailManager;

public class JktCategorySalesReportDaily implements InitializingBean{
	
	static MailManager mailManager = null;
		
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();		
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.DATE, 1);
		calender.set(Calendar.MINUTE, 30);
		calender.set(Calendar.HOUR_OF_DAY, 00);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						//categorySalesMail();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60 * 24); //every day at 12.30 am
	}
	
	public static Map<String, Object> categorySalesMail()throws Exception{
		
		try{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			cal.set(Calendar.HOUR_OF_DAY, 00);
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			Date startDate = cal.getTime();
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			Date endDate = cal.getTime();
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			Date hourStartDate = cal.getTime();
				
			List<Object[]> list = DAORegistry.getQueryManagerDAO().getJktCategorySalesReportToday(startDate, endDate);
				/*		
			Object[] zonesPricingDummySale = new Object[15];
			Object[] zonesLastRowDummySale = new Object[15];
			Object[] zonesDummySale = new Object[15];
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			
			zonesPricingDummySale[0] = (Object)"Zones Pricing Dummy Event";
			Date zonesEventDate = dateFormat.parse("01/29/2015 08:42:23");
			Timestamp zoneeventstsp = new Timestamp(zonesEventDate.getTime());
			zonesPricingDummySale[1] = zoneeventstsp;
			zonesPricingDummySale[2] = (Object)"Upper";
			zonesPricingDummySale[3] = (Object)"A";
			zonesPricingDummySale[4] = (Object)"123456";
			zonesPricingDummySale[5] = 12345;
			
			Date zonesSalesDate = dateFormat.parse("01/30/2015 09:42:23");
			Timestamp zonestsp = new Timestamp(zonesSalesDate.getTime());
			zonesPricingDummySale[6] = zonestsp;
			zonesPricingDummySale[7] = (Object)"1234";
			zonesPricingDummySale[8] = (Object)(Object)"220";
			zonesPricingDummySale[9] = (Object)"200";
			zonesPricingDummySale[10] = (Object)"210.20";
			zonesPricingDummySale[11] = (Object)"0.00";
			zonesPricingDummySale[12] = (Object)"ZP";
			zonesPricingDummySale[13] = (Object)"NULL";
			zonesPricingDummySale[14] = (Object)"Web User";
			
			zonesLastRowDummySale[0] = (Object)"Zones Last Row Dummy Event";
			Date zonesZLREventDate = dateFormat.parse("01/29/2015 08:45:23");
			Timestamp zoneZLReventstsp = new Timestamp(zonesZLREventDate.getTime());
			zonesLastRowDummySale[1] = zoneZLReventstsp;
			zonesLastRowDummySale[2] = (Object)"Lower";
			zonesLastRowDummySale[3] = (Object)"B";
			zonesLastRowDummySale[4] = (Object)"654321";
			zonesLastRowDummySale[5] = 54321;
			Date zoneslastrowSalesDate = dateFormat.parse("01/30/2015 09:45:23");
			zonestsp = new Timestamp(zoneslastrowSalesDate.getTime());
			zonesLastRowDummySale[6] = zonestsp;
			zonesLastRowDummySale[7] = (Object)"4321";
			zonesLastRowDummySale[8] = (Object)"330";
			zonesLastRowDummySale[9] = (Object)"300";
			zonesLastRowDummySale[10] = (Object)"310.20";
			zonesLastRowDummySale[11] = (Object)"0.00";
			zonesLastRowDummySale[12] = (Object)"ZLR";
			zonesLastRowDummySale[13] = (Object)"NULL";
			zonesLastRowDummySale[14] = (Object)"Web User";
			
			zonesDummySale[0] = (Object)"Zones Dummy Event";
			Date zonessEventDate = dateFormat.parse("01/29/2015 08:45:23");
			Timestamp zoneseventstsp = new Timestamp(zonessEventDate.getTime());
			zonesDummySale[1] = zoneseventstsp;
			zonesDummySale[2] = (Object)"zone";
			zonesDummySale[3] = (Object)"B";
			zonesDummySale[4] = (Object)"159753";
			zonesDummySale[5] = 98765;
			Date zoneSalesDate = dateFormat.parse("01/30/2015 09:48:23");
			zonestsp = new Timestamp(zoneSalesDate.getTime());
			zonesDummySale[6] = zonestsp;
			zonesDummySale[7] = (Object)"4321";
			zonesDummySale[8] = (Object)"330";
			zonesDummySale[9] = (Object)"300";
			zonesDummySale[10] = (Object)"310.20";
			zonesDummySale[11] = (Object)"0.00";
			zonesDummySale[12] = (Object)"hello";
			zonesDummySale[13] = (Object)"NULL";
			zonesDummySale[14] = (Object)"Web User";
			
			list.add(zonesPricingDummySale);
			list.add(zonesLastRowDummySale);
			list.add(zonesDummySale);
			*/
				int recordCount=0,hourRecordCount=0;
				boolean zoneFlag = false,flag = true;
				int autoCount=0,manualCount=0,zoneCount = 0,hourAutoCount=0,hourManualCount=0,
				hourZoneCount = 0,hourMiniCatsCount = 0,miniCatsCount = 0,vipCatsCount = 0,hourVipCatsCount = 0,
				hourVipAutoCatsCount = 0,vipAutoCatsCount = 0,lastRowMiniCats = 0,hourLastRowMiniCats = 0,
				larryLast = 0,hourLarryLast = 0,hourVividMiniCatsCount = 0,vividMiniCatsCount = 0,hourVividLastRowMiniCatsCount = 0,
				vividLastRowMiniCatsCount = 0,hourScoreBigMiniCatsCount = 0,scoreBigMiniCatsCount = 0,
				/*hourScoreBigAutoCatsCount = 0,scoreBigAutoCatsCount = 0,*/
				hourScoreBigLastRowMiniCatsCount=0,scoreBigLastRowMiniCatsCount=0,
				hourZonesLastRowCount=0,zonesLastRowCount=0,
				hourZonesCount=0,zonesCount=0, ticketCityMiniCatsCount=0,hourTicketCityMiniCatsCount=0, ticketCityVipMiniCatsCount=0, hourTicketCityVipMiniCatsCount=0,
				ticketCityLastRowMiniCatsCount=0,hourTicketCityLastRowMiniCatsCount=0, fanXchangeLastRowMiniCatsCount=0,hourFanXchangeLastRowMiniCatsCount=0;
				
				ArrayList<Integer> invoiceNo = new ArrayList<Integer>();
				StringBuilder excelData = new StringBuilder();
				
				double invoiceTotal=0.00,autoInvoiceCount=0.00,manualInvoiceCount=0.00,vipcatsInvoiceCount=0.00,
				zoneInvoiceCount = 0.00,tgCatsInvoiceCount = 0.00,vipAuto = 0.00,				
				lastRowMinicatsCount = 0.00,larryLastCount = 0.00,vividLastRowMiniCatsInvoiceCount = 0.00,
				vividMiniCatsInvoiceCount = 0.00,scoreBigMinicatsCount = 0.00,scoreBigLastRowMiniCatsInvoiceCount = 0.00,/*scoreBigAutocatsCount = 0.00,*/
				zonesLastRowInvoiceCount=0.00,
				zonesInvoiceCount=0.00, ticketCityMiniCatsInvoiceCount=0.00, ticketCityVipMiniCatsInvoiceCount=0.00, ticketCityLastRowMiniCatsInvoiceCount=0.00,
				fanXchangeLastRowMiniCatsInvoiceCount=0.00;
				excelData.append("Event Name,Event Date,Section,Row,Category TicketGroup Id,Invoice Id,Sales Date,Purchase Order Id,Retail Price,Actual Price,Invoice Total,Po Total,Processed As");
				excelData.append("\n");
				DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
				
				if(list != null && !list.isEmpty()){
				for(Object[] obj : list){
					
					Integer invoiceId = (Integer) obj[5];
					if(invoiceNo.contains(invoiceId))
						continue;
					
					invoiceNo.add(invoiceId);
						
					recordCount += 1;
					String catType = (String) obj[12];
					String type = (String) obj[13];
					String ebayType = (String) obj[14];
					
					if(catType != null && !catType.isEmpty()){
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
							catType = "VIVID LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
							catType = "SCOREBIG MINICATS";
						}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG AUTO";
						}*/
						else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("AUTOCAT")){
							catType = "TN AUTO";								
						}else if(catType.equalsIgnoreCase("MINICATS") || catType.equalsIgnoreCase("ZMINICATS")){
							catType = "TN MINICATS";
						}else if(catType.equalsIgnoreCase("VIPMINICATS") || catType.equalsIgnoreCase("ZVIPMINICATS")){
							catType = "TN VIP MINICATS";
						}else if(catType.equalsIgnoreCase("VIPAUTO")){
							catType = "TN VIP AUTOCATS";
						}else if(catType.equalsIgnoreCase("LASTROW MINICATS") || catType.equalsIgnoreCase("ZLASTROW MINICATS")){
							catType = "TN LASTROW MINICATS";
						}else if(catType.equalsIgnoreCase("LarryLast NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "TN LARRYLAST";
						}else if(catType.equalsIgnoreCase("SCZP")){
							catType = "ZONES PRICING";
						}else if(catType.equalsIgnoreCase("ZLR NOSTUB NOTNOW NOVIVID NOTEVO")){
							catType = "ZONED LASTROW MINICATS";
						}else{
							catType = "MANUAL";
						}
					}else{
						if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Cats")){
							catType = "VIVID MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Vivid Seats Specs")){
							catType = "VIVID LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig3")){
							catType = "SCOREBIG MINICATS";
						}/*else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG AUTO";
						}*/
						else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Scorebig2")){
							catType = "SCOREBIG LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City42")){
							catType = "TICKET CITY MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City43")){
							catType = "TICKET CITY VIPMINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("Ticket City41")){
							catType = "TICKET CITY LASTROW MINICATS";
						}else if((type != null && !type.isEmpty()) && type.equalsIgnoreCase("fanexchange41")){
							catType = "FANXCHANGE LASTROW MINICATS";
						}else{
							catType = "MANUAL";
						}							
					}
					Date createDate = new Date(((Timestamp) obj[6]).getTime());
					
					if(obj[0] != null && !obj[0].equals(""))
						excelData.append((String.valueOf(obj[0])).replaceAll(",", " ")+ ",");
					else
						excelData.append(",");
					if(obj[1] != null && !obj[1].equals(""))							
						excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[1]).getTime()))) + ",");
					else
						excelData.append(",");
					
					if(obj[2] != null && !obj[2].equals("")){
						String section = String.valueOf(obj[2]).toLowerCase();
						if(section.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
						excelData.append((String.valueOf(obj[2])).replaceAll(",", " ")+ ",");
					}else{
						excelData.append(",");
					}
					
					if(obj[3] != null && !obj[3].equals("")){
						String row = String.valueOf(obj[3]).toLowerCase();
						if(row.contains("zone") && catType.equals("MANUAL"))
							zoneFlag = true;
						excelData.append((String.valueOf(obj[3])).replaceAll(",", " ")+ ",");
					}else{
						excelData.append(",");
					}
					if(obj[4] != null && !obj[4].equals(""))
						excelData.append((String.valueOf(obj[4])).replaceAll(",", " ")+ ",");
					else
						excelData.append(",");
					if(obj[5] != null && !obj[5].equals(""))
						excelData.append((String.valueOf(obj[5])).replaceAll(",", " ")+ ",");
					else
						excelData.append(",");
					if(obj[6] != null && !obj[6].equals(""))
						excelData.append(String.valueOf(format.format(new Date(((Timestamp) obj[6]).getTime()))) + ",");
					else
						excelData.append(",");
					if(obj[7] != null && !obj[7].equals(""))
						excelData.append((String.valueOf(obj[7])).replaceAll(",", " ")+ ",");
					else
						excelData.append(",");
					if(obj[8] != null && !obj[8].equals("")){
						excelData.append((String.valueOf(obj[8])).replaceAll(",", " ")+ ",");
					}
					else{
						excelData.append(",");
					}
					if(obj[9] != null && !obj[9].equals("")){
						excelData.append((String.valueOf(obj[9])).replaceAll(",", " ")+ ",");
					}
					else{
						excelData.append(",");
					}
										
					if(catType.equalsIgnoreCase("TN AUTO")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourAutoCount += 1;
							hourRecordCount += 1;
						}
							autoCount += 1;
					}else if(catType.equalsIgnoreCase("TN MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourMiniCatsCount += 1;
						}
							miniCatsCount += 1;
					}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourVipAutoCatsCount += 1;
						}
							vipAutoCatsCount += 1;
					}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourVipCatsCount += 1;
						}
							vipCatsCount += 1;
					}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourLastRowMiniCats += 1;
						}
							lastRowMiniCats += 1;
					}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourLarryLast += 1;
						}
							larryLast += 1;
					}else if(zoneFlag){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourZonesCount += 1;
						}
							zonesCount += 1;
					}else if(catType.equalsIgnoreCase("ZONES PRICING")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourZoneCount += 1;
						}
							zoneCount += 1;
					}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourZonesLastRowCount += 1;
						}
							zonesLastRowCount +=1 ;
					}else if(catType.equalsIgnoreCase("VIVID MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourVividMiniCatsCount += 1;
						}
							vividMiniCatsCount += 1;
					}else if(catType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourVividLastRowMiniCatsCount += 1;
						}
							vividLastRowMiniCatsCount += 1;
					}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourScoreBigMiniCatsCount += 1;
						}
							scoreBigMiniCatsCount += 1;
					}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourScoreBigAutoCatsCount += 1;
						}
							scoreBigAutoCatsCount += 1;
					}*/
					else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourScoreBigLastRowMiniCatsCount += 1;
						}
							scoreBigLastRowMiniCatsCount += 1;
					}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourTicketCityMiniCatsCount += 1;
						}
							ticketCityMiniCatsCount +=1 ;
							recordCount += 1;
					}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourTicketCityVipMiniCatsCount += 1;
						}
							ticketCityVipMiniCatsCount +=1 ;
							recordCount += 1;
					}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourTicketCityLastRowMiniCatsCount+= 1;
						}
							ticketCityLastRowMiniCatsCount +=1 ;
							recordCount += 1;
					}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourFanXchangeLastRowMiniCatsCount+= 1;
						}
							fanXchangeLastRowMiniCatsCount +=1 ;
							recordCount += 1;
					}else{
						if(createDate.after(hourStartDate) && createDate.before(endDate)){
							hourRecordCount += 1;
							hourManualCount += 1;
						}
							manualCount += 1;
					}				
					if(obj[10] != null && !obj[10].equals("")){
						excelData.append((String.valueOf(obj[10])).replaceAll(",", " ")+ ",");
						invoiceTotal = invoiceTotal  + Double.parseDouble(String.valueOf(obj[10]));
						if(catType.equalsIgnoreCase("TN AUTO")){
							autoInvoiceCount = autoInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TN MINICATS")){
							tgCatsInvoiceCount = tgCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TN VIP MINICATS")){
							vipcatsInvoiceCount = vipcatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TN VIP AUTOCATS")){
							vipAuto = vipAuto + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TN LASTROW MINICATS")){
							lastRowMinicatsCount = lastRowMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TN LARRYLAST")){
							larryLastCount = larryLastCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(zoneFlag){
							zonesInvoiceCount = zonesInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("ZONES PRICING")){
							zoneInvoiceCount = zoneInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
							zonesLastRowInvoiceCount = zonesLastRowInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("MANUAL")){
							manualInvoiceCount = manualInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("VIVID MINICATS")){
							vividMiniCatsInvoiceCount = vividMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
							vividLastRowMiniCatsInvoiceCount = vividLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("SCOREBIG MINICATS")){
							scoreBigMinicatsCount = scoreBigMinicatsCount + Double.parseDouble(String.valueOf(obj[10]));
						}
						else if(catType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
							scoreBigLastRowMiniCatsInvoiceCount = scoreBigLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TICKET CITY MINICATS")){
							ticketCityMiniCatsInvoiceCount = ticketCityMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
							ticketCityVipMiniCatsInvoiceCount = ticketCityVipMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
							ticketCityLastRowMiniCatsInvoiceCount = ticketCityLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}else if(catType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
							fanXchangeLastRowMiniCatsInvoiceCount = fanXchangeLastRowMiniCatsInvoiceCount + Double.parseDouble(String.valueOf(obj[10]));
						}/*else if(catType.equalsIgnoreCase("SCOREBIG AUTO")){
							scoreBigAutocatsCount = scoreBigAutocatsCount + Double.parseDouble(String.valueOf(obj[10]));
						}*/
					}else{
						excelData.append(",");
					}
					if(obj[11] != null && !obj[11].equals("")){
						excelData.append((String.valueOf(obj[11])).replaceAll(",", " ")+ ",");
					}
					else{
						excelData.append(",");
					}
					if(catType != null && !catType.isEmpty()){
						if(zoneFlag && catType.equals("MANUAL"))
							excelData.append("ZONE");
						else
							excelData.append(catType.replaceAll(",", " "));
					}
					
					excelData.append("\n");
					
					zoneFlag = false;
				}
				}
				
				excelData.append("\n");
				excelData.append(",Total Percentage\n,TN AUTO CATS(%) : "+Math.round(autoInvoiceCount*100/invoiceTotal)+
						"\n,TN MINICATS(%) : "+Math.round(tgCatsInvoiceCount*100/invoiceTotal)+
						"\n,TN VIP MINICATS(%) : "+Math.round(vipcatsInvoiceCount*100/invoiceTotal)+
						"\n,TN VIP AUTOCATS(%) : "+Math.round(vipAuto*100/invoiceTotal)+
						"\n,TN LASTROW MINICATS(%) : "+Math.round(lastRowMinicatsCount*100/invoiceTotal)+
						"\n,TN LARRYLAST(%) : "+Math.round(larryLastCount*100/invoiceTotal)+
						"\n,VIVID MINI CATS(%) : "+Math.round(vividMiniCatsCount*100/invoiceTotal)+
						"\n,VIVID LASTROW MINICATS(%) : "+Math.round(vividLastRowMiniCatsCount*100/invoiceTotal)+
						/*"\n,SCOREBIG AUTO(%) : "+Math.round(scoreBigAutocatsCount*100/invoiceTotal)+*/
						"\n,SCOREBIG LASTROW MINICATS(%) : "+Math.round(scoreBigLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
						"\n,SCOREBIG MINICATS(%) : "+Math.round(scoreBigMinicatsCount*100/invoiceTotal)+
						"\n,ZONES PRICING(%) : "+Math.round(zoneInvoiceCount*100/invoiceTotal)+
						"\n,ZONED LASTROW MINICATS(%) : "+Math.round(zonesLastRowInvoiceCount*100/invoiceTotal)+
						"\n,ZONE CATS(%) : "+Math.round(zonesInvoiceCount*100/invoiceTotal)+
						"\n,MANUAL CATS(%) : "+Math.round(manualInvoiceCount*100/invoiceTotal)+
						"\n,TICKET CITY MINICATS(%) : "+Math.round(ticketCityMiniCatsInvoiceCount*100/invoiceTotal)+
						"\n,TICKET CITY VIPMINICATS(%) : "+Math.round(ticketCityVipMiniCatsInvoiceCount*100/invoiceTotal)+
						"\n,TICKET CITY LASTROW MINICATS(%) : "+Math.round(ticketCityLastRowMiniCatsInvoiceCount*100/invoiceTotal)+
						"\n,FANXCHANGE LASTROW MINICATS(%) : "+Math.round(fanXchangeLastRowMiniCatsInvoiceCount*100/invoiceTotal));
				
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com";
				String bccAddress = "AODev@rightthisway.com";
				//String toAddress ="pmehul@rightthisway.com";
				//String bccAddress = "tselvan@rightthisway.com";
				String subject = "End Of the Day Statistic for all JKT product lines....";
				String template = "jkt-mail-category-sales.html";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("jktHourAutoCount", hourAutoCount);
				map.put("jktAutoCount", autoCount);
				map.put("jktHourVipCatsCount", hourVipCatsCount);
				map.put("jktVipCatsCount", vipCatsCount);
				map.put("jktHourVipAutoCatsCount", hourVipAutoCatsCount);
				
				//VIVID COUNTS
				map.put("jktHourVividMiniCatsCount", hourVividMiniCatsCount);
				map.put("jktVividMiniCatsCount", vividMiniCatsCount);
				map.put("jktHourVividLastRowMiniCatsCount", hourVividLastRowMiniCatsCount);
				map.put("jktVividLastRowMiniCatsCount", vividLastRowMiniCatsCount);
				//SCOREBIG COUNT
				map.put("jktHourScoreMiniCatsCount", hourScoreBigMiniCatsCount);
				map.put("jktScoreMiniCatsCount", scoreBigMiniCatsCount);
				/*map.put("hourScoreBigAutoCatsCount", hourScoreBigAutoCatsCount);
				map.put("scoreBigAutoCatsCount", scoreBigAutoCatsCount);*/
				map.put("jktHourScoreBigLastRowMiniCatsCount", hourScoreBigLastRowMiniCatsCount);
				map.put("jktScoreBigLastRowMiniCatsCount", scoreBigLastRowMiniCatsCount);
				// Ticket City Count
				map.put("jktTicketCityMiniCatsCount", ticketCityMiniCatsCount);
				map.put("jktHourTicketCityMiniCatsCount", hourTicketCityMiniCatsCount);
				map.put("jktTicketCityVipMiniCatsCount", ticketCityVipMiniCatsCount);
				map.put("jktHourTicketCityVipMiniCatsCount", hourTicketCityVipMiniCatsCount);
				map.put("jktTicketCityLastRowMiniCatsCount", ticketCityLastRowMiniCatsCount);
				map.put("jktHourTicketCityLastRowMiniCatsCount", hourTicketCityLastRowMiniCatsCount);
				// Fanxchange Count
				map.put("jktFanXchangeLastRowMiniCatsCount", fanXchangeLastRowMiniCatsCount);
				map.put("jktHourFanXchangeLastRowMiniCatsCount", hourFanXchangeLastRowMiniCatsCount);
				//TN COUNTS
				map.put("jktHourAutoCount", hourAutoCount);
				map.put("jktAutoCount", autoCount);
				map.put("jktHourVipCatsCount", hourVipCatsCount);
				map.put("jktVipCatsCount", vipCatsCount);
				map.put("jktHourVipAutoCatsCount", hourVipAutoCatsCount);				
				map.put("jktLarryLast", larryLast);
				map.put("jktHourLarryLast", hourLarryLast);				
				map.put("jktLastRowMiniCats", lastRowMiniCats);
				map.put("jktHourLastRowMiniCats", hourLastRowMiniCats);				
				map.put("jktVipAutoCatsCount", vipAutoCatsCount);				
				map.put("jktHourManualCount", hourManualCount);
				map.put("jktManualCount", manualCount);
				map.put("jktHourZoneCount", hourZoneCount);
				map.put("jktZoneCount", zoneCount);
				map.put("jktHourRecordCount", hourRecordCount);
				map.put("jktRecordCount", recordCount);
				map.put("jktHourMiniCatsCount", hourMiniCatsCount);
				map.put("jktMiniCatsCount", miniCatsCount);
				map.put("jktZonesLastRow", zonesLastRowCount);
				map.put("jktHourZonesLastRow", hourZonesLastRowCount);
				map.put("jktHourZonesCount", hourZonesCount);
				map.put("jktZonesCount", zonesCount);
				//TOTAL COUNTS
				map.put("jktTotalMinicatsCount", miniCatsCount+vividMiniCatsCount+scoreBigMiniCatsCount+ticketCityMiniCatsCount);
				map.put("jktTotalHourMinicatsCount", hourMiniCatsCount+hourVividMiniCatsCount+hourScoreBigMiniCatsCount+hourTicketCityMiniCatsCount);
				
				map.put("jktTotalVipMiniCatsCount", vipCatsCount + ticketCityVipMiniCatsCount);
				map.put("jktTotalHourVipMiniCatsCount", hourVipCatsCount + hourTicketCityVipMiniCatsCount);
				
				map.put("jktTotalAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				map.put("jktTotalHourAutocatsCount", autoCount/*+scoreBigAutoCatsCount*/);
				
				map.put("jktTotalLastRowMinicatsCount", lastRowMiniCats+vividLastRowMiniCatsCount+scoreBigLastRowMiniCatsCount+ticketCityLastRowMiniCatsCount+
						fanXchangeLastRowMiniCatsCount);
				map.put("jktTotalHourLastRowMinicatsCount", hourLastRowMiniCats+hourVividLastRowMiniCatsCount+hourScoreBigLastRowMiniCatsCount+
						hourTicketCityLastRowMiniCatsCount+hourFanXchangeLastRowMiniCatsCount);
				
				map.put("jktTotalTicketNetworkCount", autoCount+miniCatsCount+vipCatsCount+vipAutoCatsCount+zoneCount+manualCount+lastRowMiniCats+larryLast+zonesLastRowCount+zonesCount);
				map.put("jktTotalHourTicketNetworkCount", hourAutoCount+hourMiniCatsCount+hourVipCatsCount+hourVipAutoCatsCount+hourZoneCount+hourManualCount+hourLastRowMiniCats+hourLarryLast+hourZonesLastRowCount+hourZonesCount);
				
				map.put("jktTotalVividSeatsCount", vividMiniCatsCount+vividLastRowMiniCatsCount);
				map.put("jktTotalHourVividSeatsCount", hourVividMiniCatsCount+hourVividLastRowMiniCatsCount);
				
				map.put("jktTotalScoreBigCount", /*scoreBigAutoCatsCount+*/scoreBigMiniCatsCount+scoreBigLastRowMiniCatsCount);
				map.put("jktTotalHourScoreBigCount", /*hourScoreBigAutoCatsCount+*/hourScoreBigMiniCatsCount+hourScoreBigLastRowMiniCatsCount);
				
				map.put("jktTotalTicketCityCount", ticketCityMiniCatsCount + ticketCityVipMiniCatsCount + ticketCityLastRowMiniCatsCount);
				map.put("jktTotalHourTicketCityCount", hourTicketCityMiniCatsCount + hourTicketCityVipMiniCatsCount + hourTicketCityLastRowMiniCatsCount);
				
				map.put("jktTotalFanXchangeCount", fanXchangeLastRowMiniCatsCount);
				map.put("jktTotalHourFanXchangeCount", hourFanXchangeLastRowMiniCatsCount);
				
				//mailManager.sendShippingMail(fromName, toAddress, null, bccAddress, subject,template, map, mimeType, new MailAttachment[]{new MailAttachment(IOUtils.toByteArray(excelData.toString()),"application/vnd.ms-excel","categorySalesReport.csv")});
				//mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, null /*new MailAttachment[]{new MailAttachment(IOUtils.toByteArray(excelData.toString()),"application/vnd.ms-excel","JktcategorySalesReport.csv")}*/);
				return map;
		}catch (Exception e) {
			e.printStackTrace();
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Error while getting End Of Day JKT Category Sales";
			String template = "statistics-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("broker", "JKT");
			map.put("error", e);
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;			
		}		
	}

}
