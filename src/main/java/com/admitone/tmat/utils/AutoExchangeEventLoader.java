package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AutoScoreBigExchangeEvent;
import com.admitone.tmat.data.AutoTNExchangeEvent;
import com.admitone.tmat.data.AutoVividSeatExchangeEvent;
import com.admitone.tmat.data.ExchangeEventAudit;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.LarryMiniTNExchangeEvent;
import com.admitone.tmat.data.LastFiveRowMCStubhubExchangeEvent;
import com.admitone.tmat.data.LastRowMiniTNExchangeEvent;
import com.admitone.tmat.data.MiniScoreBigExchangeEvent;
import com.admitone.tmat.data.MiniTNExchangeEvent;
import com.admitone.tmat.data.MiniVividSeatExchangeEvent;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VipAutoScoreBigExchangeEvent;
import com.admitone.tmat.data.VipAutoTNExchangeEvent;
import com.admitone.tmat.data.VipAutoVividSeatExchangeEvent;
import com.admitone.tmat.data.VipMiniScoreBigExchangeEvent;
import com.admitone.tmat.data.VipMiniTNExchangeEvent;
import com.admitone.tmat.data.VipMiniVividSeatExchangeEvent;
import com.admitone.tmat.data.ZonedLastRowMiniTNExchangeEvent;
import com.admitone.tmat.data.ZonesPricingTNExchangeEvent;
import com.admitone.tmat.pojo.DefaultAutoPricing;
import com.admitone.tmat.pojo.EventDTO;

public class AutoExchangeEventLoader extends QuartzJobBean implements StatefulJob {
	private boolean running;
	@Override 
	public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException{
		
		System.out.println("running...in ExchangeEventLoader");
		if(!running){
		/*if(true) {
			return ;
		}*/
			
			try{
				running = true;
				List<EventDTO> eventDTOs = DAORegistry.getQueryManagerDAO().getTNTGCatsAutoEvents();
				
				if(eventDTOs != null && !eventDTOs.isEmpty()){
					System.out.println("event size........."+eventDTOs.size());
					
					Map<Integer, GrandChildTourCategory> tmatGrandChildCategoryMap = new HashMap<Integer, GrandChildTourCategory>();
					Collection<GrandChildTourCategory> tmatGrandChildCategories = DAORegistry.getGrandChildTourCategoryDAO().getAll();
					if(tmatGrandChildCategories != null && !tmatGrandChildCategories.isEmpty()){
						for(GrandChildTourCategory grandChildCategory:tmatGrandChildCategories){
							tmatGrandChildCategoryMap.put(grandChildCategory.getId(), grandChildCategory);
						}
					}
					Collection<Venue> tmatVenues=DAORegistry.getVenueDAO().getAll();
					Map<Integer, Venue> tmatVenueMap = new HashMap<Integer, Venue>();
					for(Venue venue:tmatVenues){
						tmatVenueMap.put(venue.getId(), venue);
					}
					
//					Date currentdate=new Date();
					Map<String,DefaultAutoPricing>defaultMap = getDefaultAutoPricingValues(); 
					DefaultAutoPricing defaultAutoPricing  = null;
					Integer defaultShippingDays =Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.shipping.days").getValue());
					Boolean existingEvent = false;
					Boolean newExchange = false;
//					String oldExchanges,newExchanges;
					String exchanges;
					
					Set<Integer> existingEventIds = new HashSet<Integer>();
					
					Map<Integer,AutoTNExchangeEvent> existingAutoTNEventMap = new HashMap<Integer, AutoTNExchangeEvent>();
					List<AutoTNExchangeEvent> newAutoTNEvents = new ArrayList<AutoTNExchangeEvent>();
					Collection<AutoTNExchangeEvent> existingAutoTNEvents = DAORegistry.getAutoTNExchangeEventDAO().getAll();
					if(existingAutoTNEvents != null) {
						for (AutoTNExchangeEvent tnExEvent : existingAutoTNEvents) {
							existingEventIds.add(tnExEvent.getEvent().getId());
							existingAutoTNEventMap.put(tnExEvent.getEvent().getId(), tnExEvent);
						}	
					}
					
					Map<Integer,MiniTNExchangeEvent> existingMiniTNEventMap = new HashMap<Integer, MiniTNExchangeEvent>();
					List<MiniTNExchangeEvent> newMiniTNEvents = new ArrayList<MiniTNExchangeEvent>();
					Collection<MiniTNExchangeEvent> existingMiniTNEvents = DAORegistry.getMiniTNExchangeEventDAO().getAll();
					if(existingMiniTNEvents != null) {
						for (MiniTNExchangeEvent tnExEvent : existingMiniTNEvents) {
							existingEventIds.add(tnExEvent.getEvent().getId());
							existingMiniTNEventMap.put(tnExEvent.getEvent().getId(), tnExEvent);
						}	
					}
					
					Map<Integer,VipMiniTNExchangeEvent> existingVipMiniTNEventMap = new HashMap<Integer, VipMiniTNExchangeEvent>();
					List<VipMiniTNExchangeEvent> newVipMiniTNEvents = new ArrayList<VipMiniTNExchangeEvent>();
					Collection<VipMiniTNExchangeEvent> existingVipMiniTNEvents = DAORegistry.getVipMiniTNExchangeEventDAO().getAll();
					if(existingVipMiniTNEvents != null) {
						for (VipMiniTNExchangeEvent tnExEvent : existingVipMiniTNEvents) {
							existingEventIds.add(tnExEvent.getEvent().getId());
							existingVipMiniTNEventMap.put(tnExEvent.getEvent().getId(), tnExEvent);
						}	
					}
					
					Map<Integer,VipAutoTNExchangeEvent> existingVipAutoTNEventMap = new HashMap<Integer, VipAutoTNExchangeEvent>();
					List<VipAutoTNExchangeEvent> newVipAutoTNEvents = new ArrayList<VipAutoTNExchangeEvent>();
					Collection<VipAutoTNExchangeEvent> existingVipAutoTNEvents = DAORegistry.getVipAutoTNExchangeEventDAO().getAll();
					if(existingVipAutoTNEvents != null) {
						for (VipAutoTNExchangeEvent tnExEvent : existingVipAutoTNEvents) {
							existingEventIds.add(tnExEvent.getEvent().getId());
							existingVipAutoTNEventMap.put(tnExEvent.getEvent().getId(), tnExEvent);
						}	
					}
					
					Map<Integer,LastRowMiniTNExchangeEvent> existingLastRowMiniTNEventMap = new HashMap<Integer, LastRowMiniTNExchangeEvent>();
					List<LastRowMiniTNExchangeEvent> newLastRowMiniTNEvents = new ArrayList<LastRowMiniTNExchangeEvent>();
					Collection<LastRowMiniTNExchangeEvent> existingLastRowMiniTNEvents = DAORegistry.getLastRowMiniTNExchangeEventDAO().getAll();
					if(existingLastRowMiniTNEvents != null) {
						for (LastRowMiniTNExchangeEvent tnExEvent : existingLastRowMiniTNEvents) {
							existingEventIds.add(tnExEvent.getEvent().getId());
							existingLastRowMiniTNEventMap.put(tnExEvent.getEvent().getId(), tnExEvent);
						}	
					}
					
					Map<Integer,AutoVividSeatExchangeEvent> existingAutoVividSeatEventMap = new HashMap<Integer, AutoVividSeatExchangeEvent>();
					List<AutoVividSeatExchangeEvent> newAutoVividSeatEvents = new ArrayList<AutoVividSeatExchangeEvent>();
					Collection<AutoVividSeatExchangeEvent> existingAutoVividSeatEvents = DAORegistry.getAutoVividSeatExchangeEventDAO().getAll();
					if(existingAutoVividSeatEvents != null) {
						for (AutoVividSeatExchangeEvent VividSeatExEvent : existingAutoVividSeatEvents) {
							existingEventIds.add(VividSeatExEvent.getEvent().getId());
							existingAutoVividSeatEventMap.put(VividSeatExEvent.getEvent().getId(), VividSeatExEvent);
						}	
					}
					
					Map<Integer,MiniVividSeatExchangeEvent> existingMiniVividSeatEventMap = new HashMap<Integer, MiniVividSeatExchangeEvent>();
					List<MiniVividSeatExchangeEvent> newMiniVividSeatEvents = new ArrayList<MiniVividSeatExchangeEvent>();
					Collection<MiniVividSeatExchangeEvent> existingMiniVividSeatEvents = DAORegistry.getMiniVividSeatExchangeEventDAO().getAll();
					if(existingMiniVividSeatEvents != null) {
						for (MiniVividSeatExchangeEvent VividSeatExEvent : existingMiniVividSeatEvents) {
							existingEventIds.add(VividSeatExEvent.getEvent().getId());
							existingMiniVividSeatEventMap.put(VividSeatExEvent.getEvent().getId(), VividSeatExEvent);
						}	
					}
					
					Map<Integer,VipMiniVividSeatExchangeEvent> existingVipMiniVividSeatEventMap = new HashMap<Integer, VipMiniVividSeatExchangeEvent>();
					List<VipMiniVividSeatExchangeEvent> newVipMiniVividSeatEvents = new ArrayList<VipMiniVividSeatExchangeEvent>();
					Collection<VipMiniVividSeatExchangeEvent> existingVipMiniVividSeatEvents = DAORegistry.getVipMiniVividSeatExchangeEventDAO().getAll();
					if(existingVipMiniVividSeatEvents != null) {
						for (VipMiniVividSeatExchangeEvent VividSeatExEvent : existingVipMiniVividSeatEvents) {
							existingEventIds.add(VividSeatExEvent.getEvent().getId());
							existingVipMiniVividSeatEventMap.put(VividSeatExEvent.getEvent().getId(), VividSeatExEvent);
						}	
					}
					
					Map<Integer,VipAutoVividSeatExchangeEvent> existingVipAutoVividSeatEventMap = new HashMap<Integer, VipAutoVividSeatExchangeEvent>();
					List<VipAutoVividSeatExchangeEvent> newVipAutoVividSeatEvents = new ArrayList<VipAutoVividSeatExchangeEvent>();
					Collection<VipAutoVividSeatExchangeEvent> existingVipAutoVividSeatEvents = DAORegistry.getVipAutoVividSeatExchangeEventDAO().getAll();
					if(existingVipAutoVividSeatEvents != null) {
						for (VipAutoVividSeatExchangeEvent VividSeatExEvent : existingVipAutoVividSeatEvents) {
							existingEventIds.add(VividSeatExEvent.getEvent().getId());
							existingVipAutoVividSeatEventMap.put(VividSeatExEvent.getEvent().getId(), VividSeatExEvent);
						}	
					}
					
					/*Map<Integer,VividExchangeEvent> existingVividEvents = new HashMap<Integer, VividExchangeEvent>();
					List<VividExchangeEvent> newVividEvents = new ArrayList<VividExchangeEvent>();
					Collection<VividExchangeEvent> existingVividEvent = DAORegistry.getVividExchangeEventDAO().getAll();
					if(existingVividEvent != null) {
						for (VividExchangeEvent vividExEvent : existingVividEvent) {
							existingEventIds.add(vividExEvent.getEvent().getId());
							existingVividEvents.put(vividExEvent.getEvent().getId(), vividExEvent);
						}	
					}
					
					Map<Integer,TickPickExchangeEvent> existingTickPickEvents = new HashMap<Integer, TickPickExchangeEvent>();
					List<TickPickExchangeEvent> newTickPickEvents = new ArrayList<TickPickExchangeEvent>();
					Collection<TickPickExchangeEvent> existingTickPickEvent = DAORegistry.getTickPickExchangeEventDAO().getAll();
					if(existingTickPickEvent != null) {
						for (TickPickExchangeEvent tickPickEvent : existingTickPickEvent) {
							existingEventIds.add(tickPickEvent.getEvent().getId());
							existingTickPickEvents.put(tickPickEvent.getEvent().getId(), tickPickEvent);
						}	
					}*/
					
					/*Map<Integer,AutoTickPickExchangeEvent> existingAutoTickPickEventMap = new HashMap<Integer, AutoTickPickExchangeEvent>();
					List<AutoTickPickExchangeEvent> newAutoTickPickEvents = new ArrayList<AutoTickPickExchangeEvent>();
					Collection<AutoTickPickExchangeEvent> existingAutoTickPickEvents = DAORegistry.getAutoTickPickExchangeEventDAO().getAll();
					if(existingAutoTickPickEvents != null) {
						for (AutoTickPickExchangeEvent TickPickExEvent : existingAutoTickPickEvents) {
							existingEventIds.add(TickPickExEvent.getEvent().getId());
							existingAutoTickPickEventMap.put(TickPickExEvent.getEvent().getId(), TickPickExEvent);
						}	
					}
					
					Map<Integer,MiniTickPickExchangeEvent> existingMiniTickPickEventMap = new HashMap<Integer, MiniTickPickExchangeEvent>();
					List<MiniTickPickExchangeEvent> newMiniTickPickEvents = new ArrayList<MiniTickPickExchangeEvent>();
					Collection<MiniTickPickExchangeEvent> existingMiniTickPickEvents = DAORegistry.getMiniTickPickExchangeEventDAO().getAll();
					if(existingMiniTickPickEvents != null) {
						for (MiniTickPickExchangeEvent TickPickExEvent : existingMiniTickPickEvents) {
							existingEventIds.add(TickPickExEvent.getEvent().getId());
							existingMiniTickPickEventMap.put(TickPickExEvent.getEvent().getId(), TickPickExEvent);
						}	
					}
					
					Map<Integer,VipMiniTickPickExchangeEvent> existingVipMiniTickPickEventMap = new HashMap<Integer, VipMiniTickPickExchangeEvent>();
					List<VipMiniTickPickExchangeEvent> newVipMiniTickPickEvents = new ArrayList<VipMiniTickPickExchangeEvent>();
					Collection<VipMiniTickPickExchangeEvent> existingVipMiniTickPickEvents = DAORegistry.getVipMiniTickPickExchangeEventDAO().getAll();
					if(existingVipMiniTickPickEvents != null) {
						for (VipMiniTickPickExchangeEvent TickPickExEvent : existingVipMiniTickPickEvents) {
							existingEventIds.add(TickPickExEvent.getEvent().getId());
							existingVipMiniTickPickEventMap.put(TickPickExEvent.getEvent().getId(), TickPickExEvent);
						}	
					}*/
					
					/*Map<Integer,ScoreBigExchangeEvent> existingScoreBigEvents = new HashMap<Integer, ScoreBigExchangeEvent>();
					List<ScoreBigExchangeEvent> newScoreBigEvents = new ArrayList<ScoreBigExchangeEvent>();
					Collection<ScoreBigExchangeEvent> existingScoreBigEvent = DAORegistry.getScoreBigExchangeEventDAO().getAll();
					if(existingScoreBigEvent != null) {
						for (ScoreBigExchangeEvent scoreBigEvent : existingScoreBigEvent) {
							existingEventIds.add(scoreBigEvent.getEvent().getId());
							existingScoreBigEvents.put(scoreBigEvent.getEvent().getId(), scoreBigEvent);
						}	
					}*/
					
					Map<Integer,AutoScoreBigExchangeEvent> existingAutoScoreBigEventMap = new HashMap<Integer, AutoScoreBigExchangeEvent>();
					List<AutoScoreBigExchangeEvent> newAutoScoreBigEvents = new ArrayList<AutoScoreBigExchangeEvent>();
					Collection<AutoScoreBigExchangeEvent> existingAutoScoreBigEvents = DAORegistry.getAutoScoreBigExchangeEventDAO().getAll();
					if(existingAutoScoreBigEvents != null) {
						for (AutoScoreBigExchangeEvent ScoreBigExEvent : existingAutoScoreBigEvents) {
							existingEventIds.add(ScoreBigExEvent.getEvent().getId());
							existingAutoScoreBigEventMap.put(ScoreBigExEvent.getEvent().getId(), ScoreBigExEvent);
						}	
					}
					
					Map<Integer,MiniScoreBigExchangeEvent> existingMiniScoreBigEventMap = new HashMap<Integer, MiniScoreBigExchangeEvent>();
					List<MiniScoreBigExchangeEvent> newMiniScoreBigEvents = new ArrayList<MiniScoreBigExchangeEvent>();
					Collection<MiniScoreBigExchangeEvent> existingMiniScoreBigEvents = DAORegistry.getMiniScoreBigExchangeEventDAO().getAll();
					if(existingMiniScoreBigEvents != null) {
						for (MiniScoreBigExchangeEvent ScoreBigExEvent : existingMiniScoreBigEvents) {
							existingEventIds.add(ScoreBigExEvent.getEvent().getId());
							existingMiniScoreBigEventMap.put(ScoreBigExEvent.getEvent().getId(), ScoreBigExEvent);
						}	
					}
					
					Map<Integer,VipMiniScoreBigExchangeEvent> existingVipMiniScoreBigEventMap = new HashMap<Integer, VipMiniScoreBigExchangeEvent>();
					List<VipMiniScoreBigExchangeEvent> newVipMiniScoreBigEvents = new ArrayList<VipMiniScoreBigExchangeEvent>();
					Collection<VipMiniScoreBigExchangeEvent> existingVipMiniScoreBigEvents = DAORegistry.getVipMiniScoreBigExchangeEventDAO().getAll();
					if(existingVipMiniScoreBigEvents != null) {
						for (VipMiniScoreBigExchangeEvent ScoreBigExEvent : existingVipMiniScoreBigEvents) {
							existingEventIds.add(ScoreBigExEvent.getEvent().getId());
							existingVipMiniScoreBigEventMap.put(ScoreBigExEvent.getEvent().getId(), ScoreBigExEvent);
						}	
					}
					
					Map<Integer,VipAutoScoreBigExchangeEvent> existingVipAutoScoreBigEventMap = new HashMap<Integer, VipAutoScoreBigExchangeEvent>();
					List<VipAutoScoreBigExchangeEvent> newVipAutoScoreBigEvents = new ArrayList<VipAutoScoreBigExchangeEvent>();
					Collection<VipAutoScoreBigExchangeEvent> existingVipAutoScoreBigEvents = DAORegistry.getVipAutoScoreBigExchangeEventDAO().getAll();
					if(existingVipAutoScoreBigEvents != null) {
						for (VipAutoScoreBigExchangeEvent ScoreBigExEvent : existingVipAutoScoreBigEvents) {
							existingEventIds.add(ScoreBigExEvent.getEvent().getId());
							existingVipAutoScoreBigEventMap.put(ScoreBigExEvent.getEvent().getId(), ScoreBigExEvent);
						}	
					}
					
					Map<Integer,LarryMiniTNExchangeEvent> existingLarryMiniTNEvents = new HashMap<Integer, LarryMiniTNExchangeEvent>();
					List<LarryMiniTNExchangeEvent> newLarryMiniTNEvents = new ArrayList<LarryMiniTNExchangeEvent>();
					Collection<LarryMiniTNExchangeEvent> existingLarryMiniTNEvent = DAORegistry.getLarryMiniTNExchangeEventDAO().getAll();
					if(existingLarryMiniTNEvent != null) {
						for (LarryMiniTNExchangeEvent otherExEvent : existingLarryMiniTNEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingLarryMiniTNEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					
					Map<Integer,LastFiveRowMCStubhubExchangeEvent> existinglast5MCStubEvents = new HashMap<Integer, LastFiveRowMCStubhubExchangeEvent>();
					List<LastFiveRowMCStubhubExchangeEvent> newlast5MCStubEvents = new ArrayList<LastFiveRowMCStubhubExchangeEvent>();
					Collection<LastFiveRowMCStubhubExchangeEvent> existinglast5MCStubEvent = DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO().getAll();
					if(existinglast5MCStubEvent != null) {
						for (LastFiveRowMCStubhubExchangeEvent otherExEvent : existinglast5MCStubEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existinglast5MCStubEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					
					Map<Integer,ZonesPricingTNExchangeEvent> existingZonesPricingTNEvents = new HashMap<Integer, ZonesPricingTNExchangeEvent>();
					List<ZonesPricingTNExchangeEvent> newZonesPricingTNEvents = new ArrayList<ZonesPricingTNExchangeEvent>();
					Collection<ZonesPricingTNExchangeEvent> existingZonesPricingTNEvent = DAORegistry.getZonesPricingTNExchangeEventDAO().getAll();
					if(existingZonesPricingTNEvent != null) {
						for (ZonesPricingTNExchangeEvent otherExEvent : existingZonesPricingTNEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingZonesPricingTNEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					
					Map<Integer,ZonedLastRowMiniTNExchangeEvent> existingZonedLastrowMiniTNEvents = new HashMap<Integer, ZonedLastRowMiniTNExchangeEvent>();
					List<ZonedLastRowMiniTNExchangeEvent> newZonedLastrowMiniTNEvents = new ArrayList<ZonedLastRowMiniTNExchangeEvent>();
					Collection<ZonedLastRowMiniTNExchangeEvent> existingZonedLastrowMiniTNEvent = DAORegistry.getZonedLastRowMiniTNExchangeEventDAO().getAll();
					if(existingZonedLastrowMiniTNEvent != null) {
						for (ZonedLastRowMiniTNExchangeEvent otherExEvent : existingZonedLastrowMiniTNEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingZonedLastrowMiniTNEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					
					/*	Map<Integer,EbayExchangeEvent> existingEbayEvents = new HashMap<Integer, EbayExchangeEvent>();
					List<EbayExchangeEvent> newEbayEvents = new ArrayList<EbayExchangeEvent>();
					Collection<EbayExchangeEvent> existingEbayEvent = DAORegistry.getEbayExchangeEventDAO().getAll();
					if(existingEbayEvent != null) {
						for (EbayExchangeEvent otherExEvent : existingEbayEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingEbayEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					
					Map<Integer,ExchangeFourExchangeEvent> existingExchangeFourEvents = new HashMap<Integer, ExchangeFourExchangeEvent>();
					List<ExchangeFourExchangeEvent> newExchangeFourEvents = new ArrayList<ExchangeFourExchangeEvent>();
					Collection<ExchangeFourExchangeEvent> existingExchangeFourEvent = DAORegistry.getExchangeFourExchangeEventDAO().getAll();
					if(existingExchangeFourEvent != null) {
						for (ExchangeFourExchangeEvent otherExEvent : existingExchangeFourEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingExchangeFourEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}
					Map<Integer,ExchangeFiveExchangeEvent> existingExchangeFiveEvents = new HashMap<Integer, ExchangeFiveExchangeEvent>();
					List<ExchangeFiveExchangeEvent> newExchangeFiveEvents = new ArrayList<ExchangeFiveExchangeEvent>();
					Collection<ExchangeFiveExchangeEvent> existingExchangeFiveEvent = DAORegistry.getExchangeFiveExchangeEventDAO().getAll();
					if(existingExchangeFiveEvent != null) {
						for (ExchangeFiveExchangeEvent otherExEvent : existingExchangeFiveEvent) {
							existingEventIds.add(otherExEvent.getEvent().getId());
							existingExchangeFiveEvents.put(otherExEvent.getEvent().getId(), otherExEvent);
						}	
					}*/
					
					GrandChildTourCategory tmatGrandChildCategory = null;
					AutoTNExchangeEvent autoTNExchangeEvent = null;
					AutoVividSeatExchangeEvent autoVividSeatExchangeEvent = null;
					//AutoTickPickExchangeEvent autoTickPickExchangeEvent =null;
					AutoScoreBigExchangeEvent autoScoreBigExchangeEvent = null;
					
					MiniTNExchangeEvent miniTNExchangeEvent = null;
					MiniVividSeatExchangeEvent miniVividSeatExchangeEvent = null;
					//MiniTickPickExchangeEvent miniTickPickExchangeEvent =null;
					MiniScoreBigExchangeEvent miniScoreBigExchangeEvent = null;
					
					VipMiniTNExchangeEvent vipMiniTNExchangeEvent = null;
					VipMiniVividSeatExchangeEvent vipMiniVividSeatExchangeEvent = null;
					//VipMiniTickPickExchangeEvent vipMiniTickPickExchangeEvent =null;
					VipMiniScoreBigExchangeEvent vipMiniScoreBigExchangeEvent = null;
					
					VipAutoTNExchangeEvent vipAutoTNExchangeEvent = null;
					VipAutoVividSeatExchangeEvent vipAutoVividSeatExchangeEvent = null;
					VipAutoScoreBigExchangeEvent vipAutoScoreBigExchangeEvent = null;
					
					LastRowMiniTNExchangeEvent lastRowMiniTNExchangeEvent = null;
					LarryMiniTNExchangeEvent larryMiniTNExchangeEvent = null;
					LastFiveRowMCStubhubExchangeEvent last5MCStubExchangeEvent = null;
					ZonesPricingTNExchangeEvent zonesPricingTNExchangeEvent = null;
					ZonedLastRowMiniTNExchangeEvent zonedLastRowMiniTNExchangeEvent = null;
					
					
					/*EbayExchangeEvent ebayExchangeEvent = null;
					ExchangeTwoExchangeEvent exTwoExchangeEvent = null;
					ExchangeThreeExchangeEvent exThreeExchangeEvent = null;
					ExchangeFourExchangeEvent exFourExchangeEvent = null;
					ExchangeFiveExchangeEvent exFiveExchangeEvent = null;*/
					
					List<ExchangeEventAudit> auditList = new ArrayList<ExchangeEventAudit>();
					ExchangeEventAudit audit = null;
					String username = "AUTO";//SecurityContextHolder.getContext().getAuthentication().getName();
					Date now = new Date();
					
					for(EventDTO eventDTO:eventDTOs ){
						if(eventDTO.getGrandChildCategoryId()==null){
							continue;
						}
						if(eventDTO.getVenueCategoryId()==null || eventDTO.getNoOfCrawls()==0){
							continue;
						}
						/*if(eventDTO.getAdmitoneId() == null) {
							continue;
						}*/
						tmatGrandChildCategory = tmatGrandChildCategoryMap.get(eventDTO.getGrandChildCategoryId());
						String parentCategoryString = tmatGrandChildCategory.getChildTourCategory().getTourCategory().getName().toLowerCase();
						/*if((!parentCategoryString.equalsIgnoreCase("theater") && !parentCategoryString.equalsIgnoreCase("sports") && !parentCategoryString.equalsIgnoreCase("concerts")) ){
							continue;
						}*/
						
//						existingEvent = false;
						newExchange = false;
						exchanges = "";
//						newExchanges = "";
						
						/*if(parentCategoryString.equalsIgnoreCase("sports")) {
							defaultAutoPricing = defaultMap.get("sports");
						} else if(parentCategoryString.equalsIgnoreCase("concerts")) {
							defaultAutoPricing = defaultMap.get("concerts");
						} else if(parentCategoryString.equalsIgnoreCase("las vegas")) {
							defaultAutoPricing = defaultMap.get("las vegas");
						} else if(parentCategoryString.equalsIgnoreCase("default")){
							defaultAutoPricing = defaultMap.get("default");
						} else if(parentCategoryString.equalsIgnoreCase("theater")){
							defaultAutoPricing = defaultMap.get("theater");
						}*/
						
						
						autoTNExchangeEvent = existingAutoTNEventMap.remove(eventDTO.getId());
						miniTNExchangeEvent = existingMiniTNEventMap.remove(eventDTO.getId());
						vipMiniTNExchangeEvent = existingVipMiniTNEventMap.remove(eventDTO.getId());
						vipAutoTNExchangeEvent = existingVipAutoTNEventMap.remove(eventDTO.getId());
						lastRowMiniTNExchangeEvent = existingLastRowMiniTNEventMap.remove(eventDTO.getId());
						
//						vividExchangeEvent = existingVividEvents.remove(eventDTO.getId());
						autoVividSeatExchangeEvent = existingAutoVividSeatEventMap.remove(eventDTO.getId());
						miniVividSeatExchangeEvent = existingMiniVividSeatEventMap.remove(eventDTO.getId());
						vipMiniVividSeatExchangeEvent = existingVipMiniVividSeatEventMap.remove(eventDTO.getId());
						vipAutoVividSeatExchangeEvent = existingVipAutoVividSeatEventMap.remove(eventDTO.getId());
						
//						tickPickExchangeEvent = existingTickPickEvents.remove(eventDTO.getId());
						/*autoTickPickExchangeEvent = existingAutoTickPickEventMap.remove(eventDTO.getId());
						miniTickPickExchangeEvent = existingMiniTickPickEventMap.remove(eventDTO.getId());
						vipMiniTickPickExchangeEvent = existingVipMiniTickPickEventMap.remove(eventDTO.getId());*/
						
//						scoreBigExchangeEvent = existingScoreBigEvents.remove(eventDTO.getId());
						autoScoreBigExchangeEvent = existingAutoScoreBigEventMap.remove(eventDTO.getId());
						miniScoreBigExchangeEvent = existingMiniScoreBigEventMap.remove(eventDTO.getId());
						vipMiniScoreBigExchangeEvent = existingVipMiniScoreBigEventMap.remove(eventDTO.getId());
						vipAutoScoreBigExchangeEvent = existingVipAutoScoreBigEventMap.remove(eventDTO.getId());
						
						larryMiniTNExchangeEvent = existingLarryMiniTNEvents.remove(eventDTO.getId());
						last5MCStubExchangeEvent = existinglast5MCStubEvents.remove(eventDTO.getId());
						
						zonesPricingTNExchangeEvent = existingZonesPricingTNEvents.remove(eventDTO.getId());
						zonedLastRowMiniTNExchangeEvent = existingZonedLastrowMiniTNEvents.remove(eventDTO.getId());
						
						/*	ebayExchangeEvent = existingEbayEvents.remove(eventDTO.getId());
						exThreeExchangeEvent = existingExchangeThreeEvents.remove(eventDTO.getId());
						exFourExchangeEvent = existingExchangeFourEvents.remove(eventDTO.getId());
						exFiveExchangeEvent = existingExchangeFiveEvents.remove(eventDTO.getId());*/
						
						if(( eventDTO.getAdmitoneId() != null &&  eventDTO.getIsZoneEvent() != null && !eventDTO.getIsZoneEvent() //!parentCategoryString.equalsIgnoreCase("theater") &&
								&& (autoTNExchangeEvent == null || miniTNExchangeEvent ==null || vipMiniTNExchangeEvent ==null || vipAutoTNExchangeEvent ==null || lastRowMiniTNExchangeEvent == null))
								|| autoVividSeatExchangeEvent == null || miniVividSeatExchangeEvent ==null || vipMiniVividSeatExchangeEvent ==null || vipAutoVividSeatExchangeEvent ==null 
								//|| autoTickPickExchangeEvent == null || miniTickPickExchangeEvent ==null || vipMiniTickPickExchangeEvent ==null
								|| autoScoreBigExchangeEvent == null || miniScoreBigExchangeEvent == null || vipMiniScoreBigExchangeEvent ==null || vipAutoScoreBigExchangeEvent ==null  
								
								|| ( eventDTO.getAdmitoneId() != null &&  eventDTO.getIsZoneEvent() != null && eventDTO.getIsZoneEvent() 
								&& larryMiniTNExchangeEvent == null)
								|| (parentCategoryString.equalsIgnoreCase("theater") && last5MCStubExchangeEvent == null)
								|| (parentCategoryString.equalsIgnoreCase("theater")&&  eventDTO.getIsZoneEvent() != null && eventDTO.getIsZoneEvent() 
										&& zonesPricingTNExchangeEvent == null)
								|| (eventDTO.getAdmitoneId() != null && eventDTO.getIsZoneEvent() != null && eventDTO.getIsZoneEvent() 
										&& zonedLastRowMiniTNExchangeEvent == null)
								 
								/* || ebayExchangeEvent == null
								 || exThreeExchangeEvent == null || exFourExchangeEvent == null
								|| exFiveExchangeEvent == null*/ ) {
							//newExchange = true;
						} else {
							continue;
						}
//						boolean isAuditCreated = false;
//						boolean isMissingExchange = false;
						audit = new ExchangeEventAudit();
						
						defaultAutoPricing = defaultMap.get(parentCategoryString);
						if(autoTNExchangeEvent == null || miniTNExchangeEvent ==null || vipMiniTNExchangeEvent ==null || vipAutoTNExchangeEvent ==null || lastRowMiniTNExchangeEvent == null) {
							if(eventDTO.getAdmitoneId() != null && 
									eventDTO.getIsZoneEvent() != null && !eventDTO.getIsZoneEvent()) {//!parentCategoryString.equalsIgnoreCase("theater") && 
								if(autoTNExchangeEvent==null){
									autoTNExchangeEvent = new AutoTNExchangeEvent();
									autoTNExchangeEvent.setEvent(eventDTO.getEvent());
									
									autoTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getAutoExposure(), defaultAutoPricing.getRptFactor(),
											defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
											defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
											defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
											defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
			
									autoTNExchangeEvent.setStatus("ACTIVE");
									newAutoTNEvents.add(autoTNExchangeEvent);
									newExchange = true;
								}
								audit.setAutoExposure(defaultAutoPricing.getAutoExposure());
								exchanges = exchanges + ",AutoTNE";
								
								if(miniTNExchangeEvent==null){
									miniTNExchangeEvent = new MiniTNExchangeEvent();
									miniTNExchangeEvent.setEvent(eventDTO.getEvent());
									miniTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getMiniExposure(), defaultAutoPricing.getRptFactor(),
											defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
											defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
											defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
											defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
			
									miniTNExchangeEvent.setStatus("ACTIVE");
									newMiniTNEvents.add(miniTNExchangeEvent);
									newExchange = true;
								}
								audit.setMiniExposure(defaultAutoPricing.getMiniExposure());
								exchanges = exchanges + ",MiniTNE";
								
								if(vipMiniTNExchangeEvent==null){
									vipMiniTNExchangeEvent = new VipMiniTNExchangeEvent();
									vipMiniTNExchangeEvent.setEvent(eventDTO.getEvent());
									vipMiniTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipMiniExposure(), defaultAutoPricing.getRptFactor(),
											defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
											defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
											defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
											defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
			
									vipMiniTNExchangeEvent.setStatus("ACTIVE");
									newVipMiniTNEvents.add(vipMiniTNExchangeEvent);
									newExchange = true;
								}
								audit.setVipMiniExposure(defaultAutoPricing.getVipMiniExposure());
								exchanges = exchanges + ",VipMiniTNE";
								
								if(vipAutoTNExchangeEvent==null){
									vipAutoTNExchangeEvent = new VipAutoTNExchangeEvent();
									vipAutoTNExchangeEvent.setEvent(eventDTO.getEvent());
									vipAutoTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipAutoExposure(), defaultAutoPricing.getRptFactor(),
											defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
											defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
											defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
											defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
			
									vipAutoTNExchangeEvent.setStatus("ACTIVE");
									newVipAutoTNEvents.add(vipAutoTNExchangeEvent);
									newExchange = true;
								}
								audit.setVipAutoExposure(defaultAutoPricing.getVipAutoExposure());
								exchanges = exchanges + ",VipAutoTNE";
								
								if(lastRowMiniTNExchangeEvent==null){
									lastRowMiniTNExchangeEvent = new LastRowMiniTNExchangeEvent();
									lastRowMiniTNExchangeEvent.setEvent(eventDTO.getEvent());
									lastRowMiniTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getLastRowMiniExposure(), defaultAutoPricing.getRptFactor(),
											defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
											defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
											defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
											defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
			
									lastRowMiniTNExchangeEvent.setStatus("ACTIVE");
									newLastRowMiniTNEvents.add(lastRowMiniTNExchangeEvent);
									newExchange = true;
								}
								audit.setLastRowMiniExposure(defaultAutoPricing.getLastRowMiniExposure());
								exchanges = exchanges + ",LastRowMiniTNE";
								
							}
							
						} /*else {   // Not sure what is the use of else
							
							if(tnExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(tnExchangeEvent.getExposure(), tnExchangeEvent.getRptFactor(),
											tnExchangeEvent.getPriceBreakup(), tnExchangeEvent.getLowerMarkup(),
											tnExchangeEvent.getUpperMarkup(), tnExchangeEvent.getLowerShippingFees(),
											tnExchangeEvent.getUpperShippingFees(), tnExchangeEvent.getIsFrequentEvent());
								}
								
								existingEvent = true;
								oldExchanges = oldExchanges + ",TNE";
								newExchanges = newExchanges + ",TNE";
							}
						}*/
						
						
						/*if(autoTickPickExchangeEvent==null){
							autoTickPickExchangeEvent = new AutoTickPickExchangeEvent();
							autoTickPickExchangeEvent.setEvent(eventDTO.getEvent());
							autoTickPickExchangeEvent.setAutoPricingValues(defaultAutoPricing.getAutoExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							autoTickPickExchangeEvent.setStatus("ACTIVE");
							newAutoTickPickEvents.add(autoTickPickExchangeEvent);
							newExchange = true;
						}
						audit.setAutoExposure(defaultAutoPricing.getAutoExposure());
						exchanges = exchanges + ",AutoTickPickE";
						
						if(miniTickPickExchangeEvent==null){
							miniTickPickExchangeEvent = new MiniTickPickExchangeEvent();
							miniTickPickExchangeEvent.setEvent(eventDTO.getEvent());
							miniTickPickExchangeEvent.setAutoPricingValues(defaultAutoPricing.getMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							miniTickPickExchangeEvent.setStatus("ACTIVE");
							newMiniTickPickEvents.add(miniTickPickExchangeEvent);
							newExchange = true;
						}
						audit.setMiniExposure(defaultAutoPricing.getMiniExposure());
						exchanges = exchanges + ",MiniTickPickE";
						if(vipMiniTickPickExchangeEvent==null){
							vipMiniTickPickExchangeEvent = new VipMiniTickPickExchangeEvent();
							vipMiniTickPickExchangeEvent.setEvent(eventDTO.getEvent());
							vipMiniTickPickExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							vipMiniTickPickExchangeEvent.setStatus("ACTIVE");
							newVipMiniTickPickEvents.add(vipMiniTickPickExchangeEvent);
							newExchange = true;
						}
						audit.setVipMiniExposure(defaultAutoPricing.getVipMiniExposure());
						exchanges = exchanges + ",VipMiniTickPickE";*/
						
						if(autoScoreBigExchangeEvent==null){
							autoScoreBigExchangeEvent = new AutoScoreBigExchangeEvent();
							autoScoreBigExchangeEvent.setEvent(eventDTO.getEvent());
							autoScoreBigExchangeEvent.setAutoPricingValues(defaultAutoPricing.getAutoExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							autoScoreBigExchangeEvent.setStatus("ACTIVE");
							newAutoScoreBigEvents.add(autoScoreBigExchangeEvent);
							newExchange = true;
						}
						audit.setAutoExposure(defaultAutoPricing.getAutoExposure());
						exchanges = exchanges + ",AutoScoreBigE";
						
						if(miniScoreBigExchangeEvent==null){
							miniScoreBigExchangeEvent = new MiniScoreBigExchangeEvent();
							miniScoreBigExchangeEvent.setEvent(eventDTO.getEvent());
							miniScoreBigExchangeEvent.setAutoPricingValues(defaultAutoPricing.getMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							miniScoreBigExchangeEvent.setStatus("ACTIVE");
							newMiniScoreBigEvents.add(miniScoreBigExchangeEvent);
							newExchange = true;
						}
						audit.setMiniExposure(defaultAutoPricing.getMiniExposure());
						exchanges = exchanges + ",MiniScoreBigE";
						
						if(vipMiniScoreBigExchangeEvent==null){
							vipMiniScoreBigExchangeEvent = new VipMiniScoreBigExchangeEvent();
							vipMiniScoreBigExchangeEvent.setEvent(eventDTO.getEvent());
							vipMiniScoreBigExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							vipMiniScoreBigExchangeEvent.setStatus("ACTIVE");
							newVipMiniScoreBigEvents.add(vipMiniScoreBigExchangeEvent);
							newExchange = true;
						}
						audit.setVipMiniExposure(defaultAutoPricing.getVipMiniExposure());
						exchanges = exchanges + ",VipMiniScoreBigE";
						
						if(vipAutoScoreBigExchangeEvent==null){
							vipAutoScoreBigExchangeEvent = new VipAutoScoreBigExchangeEvent();
							vipAutoScoreBigExchangeEvent.setEvent(eventDTO.getEvent());
							vipAutoScoreBigExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipAutoExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							vipAutoScoreBigExchangeEvent.setStatus("ACTIVE");
							newVipAutoScoreBigEvents.add(vipAutoScoreBigExchangeEvent);
							newExchange = true;
						}
						audit.setVipAutoExposure(defaultAutoPricing.getVipAutoExposure());
						exchanges = exchanges + ",VipAutoScoreBigE";
						
						if(autoVividSeatExchangeEvent==null){
							autoVividSeatExchangeEvent = new AutoVividSeatExchangeEvent();
							autoVividSeatExchangeEvent.setEvent(eventDTO.getEvent());
							autoVividSeatExchangeEvent.setAutoPricingValues(defaultAutoPricing.getAutoExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							autoVividSeatExchangeEvent.setStatus("ACTIVE");
							newAutoVividSeatEvents.add(autoVividSeatExchangeEvent);
							newExchange = true;
						}
						audit.setAutoExposure(defaultAutoPricing.getAutoExposure());
						exchanges = exchanges + ",AutoVividSeatE";
						
						if(miniVividSeatExchangeEvent==null){
							miniVividSeatExchangeEvent = new MiniVividSeatExchangeEvent();
							miniVividSeatExchangeEvent.setEvent(eventDTO.getEvent());
							miniVividSeatExchangeEvent.setAutoPricingValues(defaultAutoPricing.getMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							miniVividSeatExchangeEvent.setStatus("ACTIVE");
							newMiniVividSeatEvents.add(miniVividSeatExchangeEvent);
							newExchange = true;
						}
						audit.setMiniExposure(defaultAutoPricing.getMiniExposure());
						exchanges = exchanges + ",MiniVividSeatE";

						if(vipMiniVividSeatExchangeEvent==null){
							vipMiniVividSeatExchangeEvent = new VipMiniVividSeatExchangeEvent();
							vipMiniVividSeatExchangeEvent.setEvent(eventDTO.getEvent());
							vipMiniVividSeatExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipMiniExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							vipMiniVividSeatExchangeEvent.setStatus("ACTIVE");
							newVipMiniVividSeatEvents.add(vipMiniVividSeatExchangeEvent);
							newExchange = true;
						}
						audit.setVipMiniExposure(defaultAutoPricing.getVipMiniExposure());
						exchanges = exchanges + ",VipMiniVividSeatE";
						
						if(vipAutoVividSeatExchangeEvent==null){
							vipAutoVividSeatExchangeEvent = new VipAutoVividSeatExchangeEvent();
							vipAutoVividSeatExchangeEvent.setEvent(eventDTO.getEvent());
							vipAutoVividSeatExchangeEvent.setAutoPricingValues(defaultAutoPricing.getVipAutoExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
	
							vipAutoVividSeatExchangeEvent.setStatus("ACTIVE");
							newVipAutoVividSeatEvents.add(vipAutoVividSeatExchangeEvent);
							newExchange = true;
						}
						audit.setVipAutoExposure(defaultAutoPricing.getVipAutoExposure());
						exchanges = exchanges + ",VipAutoVividSeatE";
						
						if(eventDTO.getIsZoneEvent() !=  null && eventDTO.getIsZoneEvent()){
							if(larryMiniTNExchangeEvent == null) {
								larryMiniTNExchangeEvent = new LarryMiniTNExchangeEvent();
								larryMiniTNExchangeEvent.setEvent(eventDTO.getEvent());
								larryMiniTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getMiniExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
										defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
		
								larryMiniTNExchangeEvent.setStatus("ACTIVE");
								newLarryMiniTNEvents.add(larryMiniTNExchangeEvent);
								newExchange = true;
							} 
							audit.setMiniExposure(defaultAutoPricing.getMiniExposure());
							exchanges = exchanges + ",LarryMini";
						}
						
						if(parentCategoryString.equalsIgnoreCase("theater")) {
							if(last5MCStubExchangeEvent == null) {
								last5MCStubExchangeEvent = new LastFiveRowMCStubhubExchangeEvent();
								last5MCStubExchangeEvent.setEvent(eventDTO.getEvent());
								last5MCStubExchangeEvent.setAutoPricingValues(defaultAutoPricing.getLastFiveRowMiniExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
										defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
		
								last5MCStubExchangeEvent.setStatus("ACTIVE");
								newlast5MCStubEvents.add(last5MCStubExchangeEvent);
								newExchange = true;
							} 
							audit.setLastFiveRowMiniExposure(defaultAutoPricing.getLastFiveRowMiniExposure());
							exchanges = exchanges + ",LastFiveRowMCStub";
						}
						
						if(parentCategoryString.equalsIgnoreCase("theater") &&
								eventDTO.getIsZoneEvent() !=  null && eventDTO.getIsZoneEvent()) {
							if(zonesPricingTNExchangeEvent == null) {
								zonesPricingTNExchangeEvent = new ZonesPricingTNExchangeEvent();
								zonesPricingTNExchangeEvent.setEvent(eventDTO.getEvent());
								zonesPricingTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getZonesPricingExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
										defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
		
								zonesPricingTNExchangeEvent.setStatus("ACTIVE");
								newZonesPricingTNEvents.add(zonesPricingTNExchangeEvent);
								newExchange = true;
							} 
							audit.setZonesPricingExposure(defaultAutoPricing.getZonesPricingExposure());
							exchanges = exchanges + ",ZonesPricingTN";
						}
						
						if(eventDTO.getAdmitoneId() != null &&
								eventDTO.getIsZoneEvent() !=  null && eventDTO.getIsZoneEvent()) {
							if(zonedLastRowMiniTNExchangeEvent == null) {
								zonedLastRowMiniTNExchangeEvent = new ZonedLastRowMiniTNExchangeEvent();
								zonedLastRowMiniTNExchangeEvent.setEvent(eventDTO.getEvent());
								zonedLastRowMiniTNExchangeEvent.setAutoPricingValues(defaultAutoPricing.getZonedLastRowMiniExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
										defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
		
								zonedLastRowMiniTNExchangeEvent.setStatus("ACTIVE");
								newZonedLastrowMiniTNEvents.add(zonedLastRowMiniTNExchangeEvent);
								newExchange = true;
							} 
							audit.setZonedLastRowMiniExposure(defaultAutoPricing.getZonedLastRowMiniExposure());
							exchanges = exchanges + ",ZonedLastRowTN";
						}
							
						
						/*if(vividExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",VIVID";
							
							vividExchangeEvent = new VividExchangeEvent();
							vividExchangeEvent.setEvent(eventDTO.getEvent());
							vividExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							vividExchangeEvent.setStatus("ACTIVE");
							newVividEvents.add(vividExchangeEvent);
						} else {
							
							if(vividExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(vividExchangeEvent.getExposure(), vividExchangeEvent.getRptFactor(),
											vividExchangeEvent.getPriceBreakup(), vividExchangeEvent.getLowerMarkup(),
											vividExchangeEvent.getUpperMarkup(), vividExchangeEvent.getLowerShippingFees(),
											vividExchangeEvent.getUpperShippingFees(), vividExchangeEvent.getIsFrequentEvent());
								}
								
								existingEvent = true;
								oldExchanges = oldExchanges + ",VIVID";
								newExchanges = newExchanges + ",VIVID";
							}
						}
						
						if(tickPickExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",TPE";
							
							tickPickExchangeEvent = new TickPickExchangeEvent();
							tickPickExchangeEvent.setEvent(eventDTO.getEvent());
							
							if(existingEvent) {
								tickPickExchangeEvent.setAutoPricingValues(audit.getOldExposure(), audit.getOldRptFactor(),
										audit.getOldPriceBreakup(), audit.getOldLowerMarkup(),
										audit.getOldUpperMarkup(), audit.getOldLowerShippingFees(),
										audit.getOldUpperShippingFees(), audit.getOldIsFrequentEvent());	
							} else {
								tickPickExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false);
							}
							
							tickPickExchangeEvent.setStatus("ACTIVE");
							newTickPickEvents.add(tickPickExchangeEvent);
						} else {
							if(tickPickExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(tickPickExchangeEvent.getExposure(), tickPickExchangeEvent.getRptFactor(),
											tickPickExchangeEvent.getPriceBreakup(), tickPickExchangeEvent.getLowerMarkup(),
											tickPickExchangeEvent.getUpperMarkup(), tickPickExchangeEvent.getLowerShippingFees(),
											tickPickExchangeEvent.getUpperShippingFees(), tickPickExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",TPE";
								newExchanges = newExchanges + ",TPE";
							}
						}
						
						if(scoreBigExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",SBE";
							
							scoreBigExchangeEvent = new ScoreBigExchangeEvent();
							scoreBigExchangeEvent.setEvent(eventDTO.getEvent());
							
							if(existingEvent) {
								scoreBigExchangeEvent.setAutoPricingValues(audit.getOldExposure(), audit.getOldRptFactor(),
										audit.getOldPriceBreakup(), audit.getOldLowerMarkup(),
										audit.getOldUpperMarkup(), audit.getOldLowerShippingFees(),
										audit.getOldUpperShippingFees(), audit.getOldIsFrequentEvent());	
							} else {
								scoreBigExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
										defaultAutoPricing.getUpperShippingFees(), false);
							}
	
							scoreBigExchangeEvent.setStatus("ACTIVE");
							newScoreBigEvents.add(scoreBigExchangeEvent);
						} else {
							if(scoreBigExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(scoreBigExchangeEvent.getExposure(), scoreBigExchangeEvent.getRptFactor(),
											scoreBigExchangeEvent.getPriceBreakup(), scoreBigExchangeEvent.getLowerMarkup(),
											scoreBigExchangeEvent.getUpperMarkup(), scoreBigExchangeEvent.getLowerShippingFees(),
											scoreBigExchangeEvent.getUpperShippingFees(), scoreBigExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",SBE";
								newExchanges = newExchanges + ",SBE";
							}
						}
						*/
						/*	if(ebayExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",EBAY";
							
							ebayExchangeEvent = new EbayExchangeEvent();
							ebayExchangeEvent.setEvent(eventDTO.getEvent());
							ebayExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							ebayExchangeEvent.setStatus("ACTIVE");
							newEbayEvents.add(ebayExchangeEvent);
						} else {
							if(ebayExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(ebayExchangeEvent.getExposure(), ebayExchangeEvent.getRptFactor(),
											ebayExchangeEvent.getPriceBreakup(), ebayExchangeEvent.getLowerMarkup(),
											ebayExchangeEvent.getUpperMarkup(), ebayExchangeEvent.getLowerShippingFees(),
											ebayExchangeEvent.getUpperShippingFees(), ebayExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",EBAY";
							}
						}
						
						
						if(exThreeExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",EXTHREE";
							
							exThreeExchangeEvent = new ExchangeThreeExchangeEvent();
							exThreeExchangeEvent.setEvent(eventDTO.getEvent());
							exThreeExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							exThreeExchangeEvent.setStatus("ACTIVE");
							newExchangeThreeEvents.add(exThreeExchangeEvent);
						} else {
							if(exThreeExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(exThreeExchangeEvent.getExposure(), exThreeExchangeEvent.getRptFactor(),
											exThreeExchangeEvent.getPriceBreakup(), exThreeExchangeEvent.getLowerMarkup(),
											exThreeExchangeEvent.getUpperMarkup(), exThreeExchangeEvent.getLowerShippingFees(),
											exThreeExchangeEvent.getUpperShippingFees(), exThreeExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",EXTHREE";
							}
						}
						if(exFourExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",EXFOUR";
							
							exFourExchangeEvent = new ExchangeFourExchangeEvent();
							exFourExchangeEvent.setEvent(eventDTO.getEvent());
							exFourExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							exFourExchangeEvent.setStatus("ACTIVE");
							newExchangeFourEvents.add(exFourExchangeEvent);
						} else {
							if(exFourExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(exFourExchangeEvent.getExposure(), exFourExchangeEvent.getRptFactor(),
											exFourExchangeEvent.getPriceBreakup(), exFourExchangeEvent.getLowerMarkup(),
											exFourExchangeEvent.getUpperMarkup(), exFourExchangeEvent.getLowerShippingFees(),
											exFourExchangeEvent.getUpperShippingFees(), exFourExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",EXFOUR";
							}
						}
						if(exFiveExchangeEvent == null) {
							newExchange = true;
							newExchanges = newExchanges + ",EXFIVE";
							
							exFiveExchangeEvent = new ExchangeFiveExchangeEvent();
							exFiveExchangeEvent.setEvent(eventDTO.getEvent());
							exFiveExchangeEvent.setAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false);
	
							exFiveExchangeEvent.setStatus("ACTIVE");
							newExchangeFiveEvents.add(exFiveExchangeEvent);
						} else {
							if(exFiveExchangeEvent.getStatus().equals("ACTIVE")) {
								if(!existingEvent) {
									audit.setOldAutoPricingValues(exFiveExchangeEvent.getExposure(), exFiveExchangeEvent.getRptFactor(),
											exFiveExchangeEvent.getPriceBreakup(), exFiveExchangeEvent.getLowerMarkup(),
											exFiveExchangeEvent.getUpperMarkup(), exFiveExchangeEvent.getLowerShippingFees(),
											exFiveExchangeEvent.getUpperShippingFees(), exFiveExchangeEvent.getIsFrequentEvent());
								}
								existingEvent = true;
								oldExchanges = oldExchanges + ",EXFIVE";
							}
						}*/
						
						if(newExchange) {
							audit.setUserName(username);
							audit.setCreatedDate(now);
							audit.setEvent(eventDTO.getEvent());
							audit.setExchanges(exchanges.substring(1));
							audit.setDefaultExchangeEventAudit(defaultAutoPricing.getRptFactor(),
									defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
									defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
									defaultAutoPricing.getUpperShippingFees(), false,defaultAutoPricing.getShippingMethod(),
									defaultAutoPricing.getNearTermDisplayOption(),false,defaultShippingDays);
							
							if(existingEvent) {
								audit.setAction("Auto updated");
//								audit.setOldExchanges(oldExchanges.substring(1));
//								
//								audit.setNewAutoPricingValues(audit.getOldExposure(), audit.getOldRptFactor(),
//										audit.getOldPriceBreakup(), audit.getOldLowerMarkup(),
//										audit.getOldUpperMarkup(), audit.getOldLowerShippingFees(),
//										audit.getOldUpperShippingFees(), audit.getOldIsFrequentEvent());
							} else {
								audit.setAction("Auto created");
								
//								audit.setNewAutoPricingValues(defaultAutoPricing.getExposure(), defaultAutoPricing.getRptFactor(),
//										defaultAutoPricing.getPriceBreakup(), defaultAutoPricing.getLowerMarkup(),
//										defaultAutoPricing.getUpperMarkup(), defaultAutoPricing.getLowerShippingFees(),
//										defaultAutoPricing.getUpperShippingFees(), false);
							}
							auditList.add(audit);
						}
						
					}
//					DAORegistry.getTnExchangeEventDAO().saveBulkEvents(newTNEvents);
//					DAORegistry.getVividExchangeEventDAO().saveBulkEvents(newVividEvents);
//					DAORegistry.getTickPickExchangeEventDAO().saveBulkEvents(newTickPickEvents);
//					DAORegistry.getScoreBigExchangeEventDAO().saveBulkEvents(newScoreBigEvents);
					
					DAORegistry.getAutoTNExchangeEventDAO().saveBulkEvents(newAutoTNEvents);
					DAORegistry.getMiniTNExchangeEventDAO().saveBulkEvents(newMiniTNEvents);
					DAORegistry.getVipMiniTNExchangeEventDAO().saveBulkEvents(newVipMiniTNEvents);
					DAORegistry.getVipAutoTNExchangeEventDAO().saveBulkEvents(newVipAutoTNEvents);
					DAORegistry.getLastRowMiniTNExchangeEventDAO().saveBulkEvents(newLastRowMiniTNEvents);
					
					DAORegistry.getAutoVividSeatExchangeEventDAO().saveBulkEvents(newAutoVividSeatEvents);
					DAORegistry.getMiniVividSeatExchangeEventDAO().saveBulkEvents(newMiniVividSeatEvents);
					DAORegistry.getVipMiniVividSeatExchangeEventDAO().saveBulkEvents(newVipMiniVividSeatEvents);
					DAORegistry.getVipAutoVividSeatExchangeEventDAO().saveBulkEvents(newVipAutoVividSeatEvents);
					
					DAORegistry.getAutoScoreBigExchangeEventDAO().saveBulkEvents(newAutoScoreBigEvents);
					DAORegistry.getMiniScoreBigExchangeEventDAO().saveBulkEvents(newMiniScoreBigEvents);
					DAORegistry.getVipMiniScoreBigExchangeEventDAO().saveBulkEvents(newVipMiniScoreBigEvents);
					DAORegistry.getVipAutoScoreBigExchangeEventDAO().saveBulkEvents(newVipAutoScoreBigEvents);
					
					DAORegistry.getLarryMiniTNExchangeEventDAO().saveBulkEvents(newLarryMiniTNEvents);
					DAORegistry.getLastFiveRowMCStubhubExchangeEventDAO().saveBulkEvents(newlast5MCStubEvents);
					DAORegistry.getZonesPricingTNExchangeEventDAO().saveBulkEvents(newZonesPricingTNEvents);
					DAORegistry.getZonedLastRowMiniTNExchangeEventDAO().saveBulkEvents(newZonedLastrowMiniTNEvents);
					//DAORegistry.getAutoTickPickExchangeEventDAO().saveBulkEvents(newAutoTickPickEvents);
					//DAORegistry.getMiniTickPickExchangeEventDAO().saveBulkEvents(newMiniTickPickEvents);
					//DAORegistry.getVipMiniTickPickExchangeEventDAO().saveBulkEvents(newVipMiniTickPickEvents);
					
					/*DAORegistry.getEbayExchangeEventDAO().saveBulkEvents(newEbayEvents);
					DAORegistry.getExchangeThreeExchangeEventDAO().saveBulkEvents(newExchangeThreeEvents);
					DAORegistry.getExchangeFourExchangeEventDAO().saveBulkEvents(newExchangeFourEvents);
					DAORegistry.getExchangeFiveExchangeEventDAO().saveBulkEvents(newExchangeFiveEvents);*/
			
					
					DAORegistry.getExchangeEventAuditDAO().saveBulkAudits(auditList);
					
					System.out.println("ExchangeEvent added new  events :"+auditList.size());
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			running = false;
		}
		
	}
	
	/*public Map<String,DefaultAutoPricing> getDefaultAutoPricingValuesOld() throws Exception {
		Map<String,DefaultAutoPricing> map = new HashMap<String, DefaultAutoPricing>();
		
		DefaultAutoPricing defaultAutoPricing = new DefaultAutoPricing();
		
		String eventType = "concerts";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts").getValue());
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.concerts").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.concerts").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.concerts").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.concerts").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.concerts").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.concerts").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		eventType = "sports";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports").getValue());
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.sports").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.sports").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.sports").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.sports").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.sports").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.sports").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		eventType = "las vegas";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.lasvegas").getValue());
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.lasvegas").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.lasvegas").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.lasvegas").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.lasvegas").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.lasvegas").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.lasvegas").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		eventType = "default";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.default").getValue());
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.default").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.default").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.default").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.default").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.default").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.default").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		eventType = "theater";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater").getValue());
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.theater").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.theater").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.theater").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.theater").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.theater").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.theater").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		return map;
		
	}*/
	
	public Map<String,DefaultAutoPricing> getDefaultAutoPricingValues() throws Exception {
		Map<String,DefaultAutoPricing> map = new HashMap<String, DefaultAutoPricing>();
		
		DefaultAutoPricing defaultAutoPricing = new DefaultAutoPricing();
		
		String eventType = "concerts";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.auto").getValue());
		defaultAutoPricing.setMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.mini").getValue());
		defaultAutoPricing.setVipMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.vipmini").getValue());
		defaultAutoPricing.setVipAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.vipauto").getValue());
		defaultAutoPricing.setLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.lastrowmini").getValue());
		defaultAutoPricing.setLastFiveRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.lastfiverowmini").getValue());
		defaultAutoPricing.setZonesPricingExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.zonespricing").getValue());
		defaultAutoPricing.setZonedLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.concerts.zonedlastrowmini").getValue());
		defaultAutoPricing.setShippingMethod(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.shippingmethod.concerts").getValue()));
		defaultAutoPricing.setNearTermDisplayOption(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.neartermdisplayoption.concerts").getValue()));
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.concerts").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.concerts").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.concerts").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.concerts").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.concerts").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.concerts").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		
		eventType = "sports";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.auto").getValue());
		defaultAutoPricing.setMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.mini").getValue());
		defaultAutoPricing.setVipMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.vipmini").getValue());
		defaultAutoPricing.setVipAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.vipauto").getValue());
		defaultAutoPricing.setLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.lastrowmini").getValue());
		defaultAutoPricing.setLastFiveRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.lastfiverowmini").getValue());
		defaultAutoPricing.setZonesPricingExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.zonespricing").getValue());
		defaultAutoPricing.setZonedLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.sports.zonedlastrowmini").getValue());
		defaultAutoPricing.setShippingMethod(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.shippingmethod.sports").getValue()));
		defaultAutoPricing.setNearTermDisplayOption(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.neartermdisplayoption.sports").getValue()));
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.sports").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.sports").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.sports").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.sports").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.sports").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.sports").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		
		eventType = "theater";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.auto").getValue());
		defaultAutoPricing.setMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.mini").getValue());
		defaultAutoPricing.setVipMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.vipmini").getValue());
		defaultAutoPricing.setVipAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.vipauto").getValue());
		defaultAutoPricing.setLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.lastrowmini").getValue());
		defaultAutoPricing.setLastFiveRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.lastfiverowmini").getValue());
		defaultAutoPricing.setZonesPricingExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.zonespricing").getValue());
		defaultAutoPricing.setZonedLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.zonedlastrowmini").getValue());
		defaultAutoPricing.setShippingMethod(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.shippingmethod.theater").getValue()));
		defaultAutoPricing.setNearTermDisplayOption(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.neartermdisplayoption.theater").getValue()));
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.theater").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.theater").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.theater").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.theater").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.theater").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.theater").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		eventType = "other";
		defaultAutoPricing = new DefaultAutoPricing();
		defaultAutoPricing.setAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.exposure.theater.auto").getValue());
		defaultAutoPricing.setMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.auto.exposure.default").getValue());
		defaultAutoPricing.setVipMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.vip.mini.exposure.default").getValue());
		defaultAutoPricing.setVipAutoExposure(DAORegistry.getPropertyDAO().get("exchange.default.vip.auto.exposure.default").getValue());
		defaultAutoPricing.setLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.lastrow.mini.exposure.default").getValue());
		defaultAutoPricing.setLastFiveRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.lastfiverow.mini.exposure.default").getValue());
		defaultAutoPricing.setZonesPricingExposure(DAORegistry.getPropertyDAO().get("exchange.default.zonespricing.exposure.default").getValue());
		defaultAutoPricing.setZonedLastRowMiniExposure(DAORegistry.getPropertyDAO().get("exchange.default.zonedlastrowmini.exposure.default").getValue());
		defaultAutoPricing.setShippingMethod(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.shippingmethod.default").getValue()));
		defaultAutoPricing.setNearTermDisplayOption(Integer.valueOf(DAORegistry.getPropertyDAO().get("exchange.default.neartermdisplayoption.default").getValue()));
		defaultAutoPricing.setRptFactor(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.rpt.default").getValue()));
		defaultAutoPricing.setPriceBreakup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.pricebreakup.default").getValue()));
		defaultAutoPricing.setLowerMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowermarkup.default").getValue()));
		defaultAutoPricing.setUpperMarkup(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppermarkup.default").getValue()));
		defaultAutoPricing.setLowerShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.lowershippingfees.default").getValue()));
		defaultAutoPricing.setUpperShippingFees(Double.parseDouble(DAORegistry.getPropertyDAO().get("exchange.default.uppershippingfees.default").getValue()));
		map.put(eventType, defaultAutoPricing);
		
		return map;
		
	}
	
}