package com.admitone.tmat.utils;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.TourType;

public class EventInfo {
	
	private Integer eventId;
	private Integer systemEventId;
	private String name;
//	private String tourName;
	private String artistName;
	private Date date;
	private Time time;
	private TourType tourType;
	private String building;
	private String city;
	private String state;
	private String country;
	private String zipcode;
	private boolean success = false;
	private String errorMessage;
	private String message;
	private boolean created = false;
//	private Integer tourId;
	private Integer venueId;
	private List<VenueCategory> venueCategoryList;
	private Integer artistId;
	
	/*public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}*/

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	public List<VenueCategory> getVenueCategoryList() {
		return venueCategoryList;
	}

	public void setVenueCategoryList(List<VenueCategory> venueCategoryList) {
		this.venueCategoryList = venueCategoryList;
	}

	
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public EventInfo(Integer eventId, String name, /*String tourName,*/ String artistName,
			Date date, Time time, TourType tourType, String building,
			String city, String state, String country,String zipcode) {
		super();
		this.eventId = eventId;
		this.name = name;
//		this.tourName = tourName;
		this.artistName = artistName;
		this.date = date;
		this.time = time;
		this.tourType = tourType;
		this.building = building;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipcode =zipcode;
		if (artistName == null || artistName.trim().length() == 0) {
			// do some processings on name and artist name
			int vsPos = name.toLowerCase().indexOf(" vs.");
			if (vsPos < 0) {
				this.artistName = name;
			} else {
				this.artistName = name.substring(0, vsPos).trim();
			}
		}
		
		/*if (tourName == null || tourName.trim().length() == 0) {
			this.tourName = this.artistName;
		}*/
	}
	
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/*public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}*/
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public TourType getTourType() {
		return tourType;
	}
	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isCreated() {
		return created;
	}

	public void setCreated(boolean created) {
		this.created = created;
	}

	public Integer getSystemEventId() {
		return systemEventId;
	}

	public void setSystemEventId(Integer systemEventId) {
		this.systemEventId = systemEventId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}
