package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.InstantEvent;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TnowInstantEvent;

public class TNowInstantEventManager {
/*
	public static void saveTNowInstantEvents(Collection<Event> eventList){
		
//		InstantTour instantTour = null;
		TnowInstantEvent tnowInstantEvent = null;
		Integer defaultExpiryTime = 0;
		Double defaultMarkupPercent = 0.0;
		Double defaultSalesPercent=0.0;
		Double defaultShippingFee=0.0;
		Double defaultMinThreshold=0.0;
		Double defaultMaxThreshold=0.0;
		Property shExpiryProperty = DAORegistry.getPropertyDAO().get("sh.expiry");
		Property shMarkupProperty = DAORegistry.getPropertyDAO().get("sh.markup.percent");
		Property shSalesProperty = DAORegistry.getPropertyDAO().get("sh.sales");
		Property shShippingProperty = DAORegistry.getPropertyDAO().get("sh.shipping");
		Property shMaxThresholdProperty = DAORegistry.getPropertyDAO().get("sh.maxthreshold");
		Property shMinThresholdProperty = DAORegistry.getPropertyDAO().get("sh.minthreshold");
		String shExpiryStr = shExpiryProperty.getValue();
		try {
			defaultExpiryTime = Integer.valueOf(shExpiryStr);
		}catch(NumberFormatException nfe){}
		
		
		String shMarkupStr = shMarkupProperty.getValue();
		try{
			defaultMarkupPercent = Double.valueOf(shMarkupStr);
		}catch(NumberFormatException nfe){}
		
		
		String shSalesStr = shSalesProperty.getValue();
		try{
			defaultSalesPercent = Double.valueOf(shSalesStr);
		}catch(NumberFormatException nfe){}
		
		
		String shShippingStr = shShippingProperty.getValue();
		try{
			defaultShippingFee = Double.valueOf(shShippingStr);
		}catch(NumberFormatException nfe){}
		String shMinThresholdStr = shMinThresholdProperty.getValue();
		try{
			defaultMinThreshold = Double.valueOf(shMinThresholdStr);
		}catch(NumberFormatException nfe){}
		
		
		String shMaxThresholdStr = shMaxThresholdProperty.getValue();
		try{
			defaultMaxThreshold = Double.valueOf(shMaxThresholdStr);
		}catch(NumberFormatException nfe){}
		
		List<TnowInstantEvent> tnowInstantEvents = new ArrayList<TnowInstantEvent>();
		for (Event event : eventList) {
			tnowInstantEvent = new TnowInstantEvent(event);
			tnowInstantEvent.setMarkupPercent(defaultMarkupPercent);
			tnowInstantEvent.setExpiryTime(defaultExpiryTime);
			tnowInstantEvent.setMinThreshold(defaultMinThreshold);
			tnowInstantEvent.setMaxThreshold(defaultMaxThreshold);
			tnowInstantEvent.setSalesPercent(defaultSalesPercent);
			tnowInstantEvent.setShippingFee(defaultShippingFee);
			tnowInstantEvent.setTour(event.getTour());
			tnowInstantEvents.add(tnowInstantEvent);
		}
		DAORegistry.getTnowInstantEventDAO().saveAll(tnowInstantEvents);
		}

*/
}
