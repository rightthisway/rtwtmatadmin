package com.admitone.tmat.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletResponse;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.InstantEvent;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketDeliveryType;


/**
 * a.  Computing the final set of Stubhub instant tickets
 * b.  Writing the StubHub instant tickets to CSV file
 */

public class InstantTicketsUtil {
	// prevent instantiation since it is a utility class
	private InstantTicketsUtil() {}
	
	/*public static void downloadInstantTicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=InstantTickets_" + today + ".csv");
		Collection<Ticket> instantTickets = computeInstantTickets();
		downloadInstantTicketCsvFile(instantTickets, response.getOutputStream());
	}
	
	public static void downloadInstantTNDTicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=InstantTickets_TND_" + today + ".csv");
		Collection<Ticket> instantTickets = computeInstantTickets();
		downloadInstantTicketTNDCsvFile(instantTickets, response.getOutputStream());
	}

	public static void downloadCustomizedInstantTicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=CustomizedInstantTickets_" + today + ".csv");
		Collection<Ticket> instantTickets = computeCustomizedCatInstantTickets();
		downloadCustomizedInstantTicketCsvFile(instantTickets, response.getOutputStream());
	}
	
	
	public static  void downloadInstantTicketCsvFile(Collection<Ticket> instantTickets,OutputStream outputStream) throws IOException{
//		Collection<Ticket> instantTickets = computeInstantTickets();
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		for (Ticket instantTicket: instantTickets) {
			String timeStr;
			if (instantTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(instantTicket.getEventTime());
			}
			String seatFrom = "";
			String seatThru = "";
			String price = numberFormat.format((instantTicket.getCurrentPrice())*1.2);
			
			String notes = null;
			if (instantTicket.getRemainingQuantity()>=3){
			Property notesProperty = DAORegistry.getPropertyDAO().get("instantTickets.notes");
			notes = notesProperty.getValue();
			}else{
				notes = "";
			}

			String row = "y" + ","
						+ instantTicket.getEventName() + "," 
						+ instantTicket.getVenue() + "," 
						+ dateFormat.format(instantTicket.getEventDate()) + ","
						+ timeStr.trim() + ","
						+ instantTicket.getRemainingQuantity() + ","
						+ instantTicket.getNormalizedSection() + ","
						+ instantTicket.getRow() + ","
						+ seatFrom + ","
						+ seatThru + ","
						+ notes +","
						+ price + ","
						+ instantTicket.getId() 
						+ "\n";
			outputStream.write(row.getBytes());
		}
	}
	
	
	public static  void downloadInstantTicketTNDCsvFile(Collection<Ticket> instantTickets,OutputStream outputStream) throws IOException{
	
//			Collection<Ticket> instantTickets = computeInstantTickets();
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat timeFormat = new SimpleDateFormat("h:mma");
			NumberFormat numberFormat = NumberFormat.getInstance();
			numberFormat.setGroupingUsed(false);
			numberFormat.setMinimumFractionDigits(2);
			numberFormat.setMaximumFractionDigits(2);
			outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n".getBytes());
			for (Ticket instantTicket: instantTickets) {
	
				String timeStr;
				if (instantTicket.getEventTime() == null) {
					timeStr = "TBD";
				} else {
					timeStr = timeFormat.format(instantTicket.getEventTime());
				}

				String	seatFrom = "";
				String 	seatThru = "";

				String price = numberFormat.format((instantTicket.getCurrentPrice())*1.2);
				
				String notes = null;
				if (instantTicket.getRemainingQuantity()>=3){
				Property notesProperty = DAORegistry.getPropertyDAO().get("instantTickets.notes");
				notes = notesProperty.getValue();
				}else{
					notes = "";
				}
				
				int emailOption=1;
				long hrs=(instantTicket.getEventDate().getTime()- new Date().getTime()) /(1000*60*60);
				if(hrs > 50)
					emailOption=0;
				
				String row = "y" + ","
							+ instantTicket.getEventName() + "," 
							+ instantTicket.getVenue() + "," 
							+ dateFormat.format(instantTicket.getEventDate()) + ","
							+ timeStr.trim() + ","
							+ instantTicket.getRemainingQuantity() + ","
							+ instantTicket.getNormalizedSection() + ","
							+ instantTicket.getRow() + ","
							+ seatFrom + ","
							+ seatThru + ","
							+ notes +","
							+ price + ","
							+ instantTicket.getId() + ","
							+ " " +","
							+ " " +","
							+ emailOption +","
							+ 2 +","
							+ dateFormat.format(new Date()) +","
							+ " " 
							+ "\n";
				outputStream.write(row.getBytes());
			}
		}
	
	
public static  void downloadCustomizedInstantTicketCsvFile(Collection<Ticket> instantTickets,OutputStream outputStream) throws IOException{
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		for (Ticket instantTicket: instantTickets) {
			String timeStr;
			if (instantTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(instantTicket.getEventTime());
			}

				String seatFrom = "";
				String seatThru = "";

			String price = numberFormat.format((instantTicket.getCurrentPrice()));
			
			String notes = null;
			if (instantTicket.getRemainingQuantity()>=3){
			Property notesProperty = DAORegistry.getPropertyDAO().get("instantTickets.notes");
			notes = notesProperty.getValue();
			}else{
				notes = "";
			}

			String row = "y" + ","
						+ instantTicket.getEventName() + "," 
						+ instantTicket.getVenue() + "," 
						+ dateFormat.format(instantTicket.getEventDate()) + ","
						+ timeStr.trim() + ","
						+ instantTicket.getRemainingQuantity() + ","
						+ instantTicket.getNormalizedSection() + ","
						+ instantTicket.getRow() + ","
						+ seatFrom + ","
						+ seatThru + ","
						+ notes +","
						+ price + ","
						+ instantTicket.getId() 
						+ "\n";
			outputStream.write(row.getBytes());
		}
	}
	

public static  void downloadCustomizedTNDInstantTicketCsvFile(Collection<Ticket> instantTickets,OutputStream outputStream) throws IOException{
	
	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	DateFormat timeFormat = new SimpleDateFormat("h:mma");
	NumberFormat numberFormat = NumberFormat.getInstance();
	numberFormat.setGroupingUsed(false);
	numberFormat.setMinimumFractionDigits(2);
	numberFormat.setMaximumFractionDigits(2);
	int i=0;
	Date now = new Date();
	Property notesProperty = DAORegistry.getPropertyDAO().get("instantTickets.notes");
	
	StringBuffer row= new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n");
	for (Ticket instantTicket: instantTickets) {
		String timeStr;
		if (instantTicket.getEventTime() == null) {
			timeStr = "TBD";
		} else {
			timeStr = timeFormat.format(instantTicket.getEventTime());
		}

			String seatFrom = "";
			String seatThru = "";

		String price = numberFormat.format((instantTicket.getCurrentPrice()));
		
		String notes = null;
		if (instantTicket.getRemainingQuantity()>=3){
			notes = notesProperty.getValue();
		}else{
			notes = "";
		}
		int emailOption=1;
		long hrs=(instantTicket.getEventDate().getTime()- now.getTime()) /(1000*60*60);
		if(hrs > 50)
			emailOption=0;
		
		row.append("y,");
		row.append(instantTicket.getEventName() + ","); 
		row.append(instantTicket.getVenue() + ","); 
		row.append(dateFormat.format(instantTicket.getEventDate()) + ",");
		row.append(timeStr.trim() + ",");
		row.append(instantTicket.getRemainingQuantity() + ",");
		row.append(instantTicket.getNormalizedSection() + ",");
		row.append(instantTicket.getRow() + ",");
		row.append(seatFrom + ",");
		row.append(seatThru + ",");
		row.append(notes +",");
		row.append(price + ",");
		row.append(instantTicket.getId()+ ","); 
		row.append("1,"); //TicketGroupTypeID 
		row.append("2,"); //TicketGroupStockTypeID 
		row.append("1,"); //ShippingMethodSpecialID  : emailOption
		row.append("0,"); //ShowNearTermOptionID 
//		row.append(dateFormat.format(now) +","); InHandDate
		row.append("Y,");
		row.append("0 \n"); //TicketGroupObstructionDescriptionID 
		if(i>200){
			i=0;
			outputStream.write(row.toString().getBytes());
			row=null;
			row= new StringBuffer("");  //does delete free memory?
		}
		
	}
	outputStream.write(row.toString().getBytes());
	row=null;
	}
*/

	/*Only non duplicate, non AO, categorized stubhub instant tickets are considered*/
/*	public static Collection<InstantTickets> computeInstantTickets() {
		Collection<InstantTickets> filteredInstantTickets = new ArrayList<InstantTickets>();
		Property property = DAORegistry.getPropertyDAO().get("instantTickets.eventIds");
		String eventIds = property.getValue();
		for (String eventIdStr: eventIds.split(",")) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
			Integer eventId = Integer.valueOf(eventIdStr.trim());
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
				if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
			Collection<InstantTickets> instantTickets = DAORegistry
			.getInstantTicketsQueryManager().getInstantTickets(eventId);
			List<String> catScheme = Categorizer.getCategoryGroupsByEvent(eventId);
			Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
			List<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
			tmpFilteredTickets = TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
			Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(eventId);
			for (String catscheme : catScheme) {
				Collection<Category> categories = DAORegistry.getCategoryDAO()
				.getAllCategories(event.getTourId(), catscheme);
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
				for (Ticket ticket:tmpFilteredTickets){	
					for (InstantTickets instantTicket : instantTickets) {
						if(!TicketUtil.isInInventory(ticket, aotickets)){
							if (ticket.getId().equals(instantTicket.getTicketId())){
								int quantity = instantTicket.getQuantity();
								double price = instantTicket.getCurrentPrice()*1.2;
								if(quantity*price>200 && price<20000){
									Integer catId = Categorizer.computeCategory(eventId, instantTicket.getSection(),instantTicket.getRow(), catscheme);	
									if (catId != null) {
										instantTicket.setCategory(categoryMap.get(catId));
										filteredInstantTickets.add(instantTicket);
									}
								}
							}
						}
					}
				}
			}
		}	
			}
		}
	//	System.out.println("Instant tix filtered coll size" +filteredInstantTickets.size());
		return filteredInstantTickets;
	}
	*/
/*	
	
public static Collection<Ticket> computeCustomizedInstantTickets(Integer eventId) {	
	Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		Event event = DAORegistry.getEventDAO().get(eventId);
		InstantEvent instantEvent = DAORegistry.getInstantEventDAO().get(eventId);
		Integer expiryTime = instantEvent.getExpiryTime();
	
		long expiryEventTime  = expiryTime * 60L * 60L * 1000L ;
		
		if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
			Date date = event.getDate();
			Date time = null;
			
			if (event.getTime()==null){
				time = event.getDate();
			}else{
				time = event.getTime();
				}
				Calendar calDate = new GregorianCalendar();
				Calendar calTime = new GregorianCalendar();
				calDate.setTime(date);
				calTime.setTime(time);
			
				Calendar caldatetime = new GregorianCalendar();
				caldatetime.set(Calendar.YEAR, calDate.get(Calendar.YEAR));
				caldatetime.set(Calendar.MONTH, calDate.get(Calendar.MONTH));
				caldatetime.set(Calendar.DATE, calDate.get(Calendar.DATE));
				caldatetime.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
				caldatetime.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
				caldatetime.set(Calendar.SECOND, calTime.get(Calendar.SECOND));
				Date eventDateTime = caldatetime.getTime();
				long difference = eventDateTime.getTime() - new Date().getTime();
			
				
				if (difference>expiryEventTime){	
					Double markup = instantEvent.getMarkupPercent();
					Double minThreshold = instantEvent.getMinThreshold();
					Double maxThreshold = instantEvent.getMaxThreshold();
					Double salesPercent = instantEvent.getSalesPercent();
					Double shippingFee = instantEvent.getShippingFee();
					
					Collection<Ticket> instantTickets = DAORegistry.getInstantTicketsQueryManager().getInstantTickets(eventId);
					List<String> catScheme = Categorizer.getCategoryGroupsByVenueId(eventId);
					for (String catscheme : catScheme) {
						Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
						Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
						for (Category category : categories) {
							categoryMap.put(category.getId(), category);
						}
						
						Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
						Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
						for(CategoryMapping mapping:categoryMappingList){
							List<CategoryMapping> list =catMappingMap.get(mapping.getId());
							if(list==null){
								list = new ArrayList<CategoryMapping>();
							}
							list.add(mapping);
							catMappingMap.put(mapping.getId(), list);
						}
						if (instantTickets !=null){
							for (Ticket instantTicket : instantTickets) {
								double markupPrice = 0, salesPrice = 0,  totalPrice = 0;
								int quantity = instantTicket.getRemainingQuantity();
								double price = instantTicket.getCurrentPrice();
								if (markup!= null){
									markupPrice =( price * ((100 + markup) / 100) )- price;
								}
								if (salesPercent != null){
									salesPrice  =( price * (100 +salesPercent) /100 ) - price;
								}
								
								totalPrice = price + markupPrice + salesPrice + shippingFee ; 
							
								instantTicket.setCurrentPrice(totalPrice);
								
								if(quantity*totalPrice>minThreshold && totalPrice<maxThreshold){
									Category cat = Categorizer.computeCategory(event.getVenueCategory(), instantTicket.getNormalizedSection(),instantTicket.getRow(), catscheme,categoryMap,catMappingMap);
									if (cat != null) {
										instantTicket.setCategoryId(cat.getId());
										instantTicket.setCategory(cat);
										tmpFilteredTickets.add(instantTicket);
									}
								}
							}	
						}
					}
				}
			}else if (!event.getEventStatus().equals(EventStatus.ACTIVE)){
				InstantEventManager.deleteInstantEvents(eventId);
			}
		return tmpFilteredTickets;
	}

public static Collection<Ticket> computeCustomizedCatInstantTickets(){
	Collection<Ticket> filteredCatTickets = new ArrayList<Ticket>();
	Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
	Collection<Ticket> instantFilteredTickets = new ArrayList<Ticket>();
	Collection<InstantEvent> instantEvents = DAORegistry.getInstantEventDAO().getAll();
	HashMap<Integer, Collection<Ticket>> instantTicketsByEventId = DAORegistry.getInstantTicketsQueryManager().getAllInstantTickets();
	for (InstantEvent instantEvent:instantEvents){
		Integer eventId = instantEvent.getEvent().getId();		
		tmpFilteredTickets = instantTicketsByEventId.get(eventId);
		if(tmpFilteredTickets != null){
			filteredCatTickets = getCatInstantTickets(tmpFilteredTickets);
			instantFilteredTickets.addAll(filteredCatTickets);
		}
	}
	return instantFilteredTickets;
}

public static Collection<Ticket> getCatInstantTickets(Collection<Ticket> tmpFilteredTickets){
	Map<Integer, List<Ticket>> catTickets = new HashMap<Integer, List<Ticket>>();
	Collection<Ticket> filteredInstantTickets = new ArrayList<Ticket>();
			for (Ticket instantTicket:tmpFilteredTickets){
				List<Ticket> list = (List<Ticket>) catTickets.get(instantTicket.getCategory().getId());
				if (list == null) {
				list = new ArrayList<Ticket>();
					catTickets.put(instantTicket.getCategory().getId(), list);
			}
				list.add(instantTicket);
			}	
			filteredInstantTickets = orderCatTickets(catTickets);
		return filteredInstantTickets;
}


public static Collection<Ticket> orderCatTickets(Map<Integer, List<Ticket>> catTicketList){
	Collection<Ticket> orderedTickets = new ArrayList<Ticket>();
	List<Ticket> ticketsByCat = new ArrayList<Ticket>();
	for (Integer categoryId: catTicketList.keySet()){
		ticketsByCat= catTicketList.get(categoryId);
		
			Collections.sort(ticketsByCat, new Comparator<Ticket>(){
				@Override
				public int compare(Ticket t1, Ticket t2){
					if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())>0){
						return -1;
					} else if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())<0){
						return 1;
					}else 
						return 0;
				}
			});
			if (ticketsByCat.size()>1){
				ticketsByCat.remove(ticketsByCat.size()-1);
				orderedTickets.addAll(ticketsByCat);
			}else {
				orderedTickets.addAll(ticketsByCat);
			}
			}
	return orderedTickets;
}
*/
	/*Only non duplicate, non AO, categorized stubhub instant tickets are considered*/
		public static Collection<Ticket> computeInstantTickets() {
	
			Collection<Ticket> filteredInstantTickets = new ArrayList<Ticket>();
		//	Collection<Ticket> filteredUniqueInstantTickets = new ArrayList<Ticket>();
			Property property = DAORegistry.getPropertyDAO().get("instantTickets.eventIds");
			HashMap<Integer, Collection<Ticket>> instantTicketsByEvent = null;//DAORegistry.getInstantTicketsQueryManager().getAllInstantTickets();
			String eventIds = property.getValue();
			for (String eventIdStr: eventIds.split(",")) {
				if (eventIdStr.trim().isEmpty()) {
					continue;
				}
				Integer eventId = Integer.valueOf(eventIdStr.trim());
				Event event = DAORegistry.getEventDAO().get(eventId);
				if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
					Date date = event.getDate();
					Date time = null;
					
					if (event.getTime()==null){
							time = event.getDate();
						}
					else{
						time = event.getTime();
					}
					Calendar calDate = new GregorianCalendar();
					Calendar calTime = new GregorianCalendar();
					calDate.setTime(date);
					calTime.setTime(time);
				
					Calendar caldatetime = new GregorianCalendar();
					caldatetime.set(Calendar.YEAR, calDate.get(Calendar.YEAR));
					caldatetime.set(Calendar.MONTH, calDate.get(Calendar.MONTH));
					caldatetime.set(Calendar.DATE, calDate.get(Calendar.DATE));
					caldatetime.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
					caldatetime.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
					caldatetime.set(Calendar.SECOND, calTime.get(Calendar.SECOND));
					Date eventDateTime = caldatetime.getTime();
					
					long difference = eventDateTime.getTime() - new Date().getTime();
					if (difference>24L*60L*60L*1000L){
			//		if (event.getDate().getTime()- new Date().getTime()>24L*60L*60L*1000L){
				Collection<Ticket> instantTickets = instantTicketsByEvent.get(eventId);
				/*
				 * The below line was commented on 24/03/2011
				 */
				//Collection<Ticket> instantTickets = DAORegistry.getInstantTicketsQueryManager().getInstantTickets(eventId);
				List<String> catScheme = Categorizer.getCategoryGroupsByVenueId(event.getVenueId());
		//		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		//		List<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		//		tmpFilteredTickets = TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		//		Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(eventId);
				for (String catscheme : catScheme) {
					VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catscheme);
					Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
					Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
					for (Category category : categories) {
						categoryMap.put(category.getId(), category);
					}
					Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
					Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
					for(CategoryMapping mapping:categoryMappingList){
						List<CategoryMapping> list =catMappingMap.get(mapping.getId());
						if(list==null){
							list = new ArrayList<CategoryMapping>();
						}
						list.add(mapping);
						catMappingMap.put(mapping.getId(), list);
					}
						for (Ticket instantTicket : instantTickets) {
		//					if(!TicketUtil.isInInventory(instantTicket, aotickets)){
		//						if (ticket.getId().equals(instantTicket.getTicketId())){
									int quantity = instantTicket.getRemainingQuantity();
									double price = instantTicket.getCurrentPrice()*1.2;
									if(quantity*price>100 && price<20000){
										Category cat = Categorizer.computeCategory(event.getVenueCategory(), instantTicket.getNormalizedSection(),instantTicket.getRow(), catscheme,categoryMap,catMappingMap);	
										if (cat != null) {
									//		instantTicket.setCategory(categoryMap.get(catId));
											filteredInstantTickets.add(instantTicket);
										//	filteredUniqueInstantTickets = getUniqueValues (filteredInstantTickets);
										}
									}
								}
							//}
						}
					}
				}
		//	}	
			}	
			return filteredInstantTickets;
		//	return filteredUniqueInstantTickets;
		}

		
/*		public static Collection<Ticket> getUniqueValues(Collection<Ticket> filteredInstantTickets){
		    return (Collection<Ticket>)Union(filteredInstantTickets, filteredInstantTickets);
		}

		public static Collection<Ticket> Union(Collection<Ticket> coll1, Collection<Ticket> coll2){
		    Set<Ticket> union = new HashSet<Ticket>(coll1);
		    union.addAll(new HashSet<Ticket>(coll2));
		    return new ArrayList<Ticket>(union);
		} */
		
	/*Method to create new worksheet in an excel workbook when the number of rows exceed 65000 */
	/*	private Sheet createSheet(WritableWorkbook workbook, String name, List<InstantTickets> instantTickets, int page) throws RowsExceededException, WriteException {
 		DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
 		DateFormat timeFormat = new SimpleDateFormat("h:mm a");
		String today = format.format(new Date());
		NumberFormat numberFormat = NumberFormat.getInstance();
		
		WritableSheet sheet = workbook.createSheet(name, page);
	
		if (instantTickets == null) {
			return null;
		}
		sheet.addCell(new Label(0, 0, "Edit"));
		sheet.addCell(new Label(1, 0, "Event"));
		sheet.addCell(new Label(2, 0, "Venue"));
		sheet.addCell(new Label(3, 0, "EventDate"));
		sheet.addCell(new Label(4, 0, "EventTime"));
		sheet.addCell(new Label(5, 0, "Quantity"));
		sheet.addCell(new Label(6, 0, "Section"));
		sheet.addCell(new Label(7, 0, "Row"));
		sheet.addCell(new Label(8, 0, "SeatFrom"));
		sheet.addCell(new Label(9, 0, "SeatThru"));
		sheet.addCell(new Label(10, 0, "Cost"));
		sheet.addCell(new Label(11, 0, "TicketId"));

		int rowNum = 1;
		for(InstantTickets instanttickets : instantTickets) {		
			rowNum++;
			
	//		Event event = instantticket.getEvent();
			String timeStr;
			if (instanttickets.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(instanttickets.getEventTime());
			}
		
			String seat = instanttickets.getSeat();
			String seatFrom = null;
			String seatThru = null;
			
			if ((seat == null)||(seat.isEmpty())){
				seatFrom = "";
				seatThru = "";
			} else {
				String[] seatToken = seat.split("-");
				int tokenLength = seatToken.length;
				if (tokenLength == 1)
				{
					seatFrom = seatThru = seatToken[0];
				}
				else
				if (tokenLength == 2)
				{
					seatFrom = seatToken[0];
					seatThru = seatToken[1];
				}
			}	
			String price = numberFormat.format((instanttickets.getCurrentPrice())*1.2);
			String edit = "y";
			
			sheet.addCell(new Label(0, rowNum, edit));
			sheet.addCell(new Label(1, rowNum, instanttickets.getEventName()));
			sheet.addCell(new Label(2, rowNum, instanttickets.getVenue()));
			sheet.addCell(new Label(3, rowNum, format.format(instanttickets.getEventDate())));
			sheet.addCell(new Label(4, rowNum, timeStr.trim()));
			sheet.addCell(new Label(5, rowNum, instanttickets.getQuantity().toString()));
			sheet.addCell(new Label(6, rowNum, instanttickets.getSection()));
			sheet.addCell(new Label(7, rowNum, instanttickets.getRow()));
			sheet.addCell(new Label(8, rowNum, seatFrom));
			sheet.addCell(new Label(9, rowNum, seatThru));
			sheet.addCell(new Label(10, rowNum, price));
			sheet.addCell(new Label(11, rowNum, instanttickets.getTicketId().toString()));
		}
		return sheet;
	}
	*/
		/* Method to download instant tickets from stubhub in excel format*/	
/*		public ModelAndView downloadInstantTicketXlsFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		String today = format.format(new Date());
		
		String[] eventIds = request.getParameter("eventIds").split(",");
		for (String eventIdStr: eventIds) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
		Integer eventId = Integer.valueOf(eventIdStr.trim());
		
		
	//	List<InstantTickets> instantTickets = DAORegistry.getInstantTicketsDAO().getAllInstantTickets();
		Collection<InstantTickets> instantTickets= DAORegistry.getInstantTicketsQueryManager().getInstantTickets(eventId);
		List<String> catScheme = Categorizer.getAllCategoryGroups();
		Event event = DAORegistry.getEventDAO().get(eventId);
		for (String catscheme: catScheme ){
				Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catscheme);
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				
				for (Category category: categories) {
					categoryMap.put(category.getId(), category);
				}
				
				for (InstantTickets instantticket: instantTickets) {
					Integer catId = Categorizer.computeCategory(instantticket, catscheme);
					if (catId!=null){
					instantticket.setCategory(categoryMap.get(catId));
					}
					else{
						instantTickets.remove(instantticket);
					}
				}
		}
	}
	
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-disposition",
				"attachment; filename=InstantTickets_" + today + ".xls");

		WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());

			int page = 0;
			for (int offset = 0; offset < instantTickets.size(); offset+= 65000) {
				page++;
				List<InstantTickets> rows = ((List<InstantTickets>) instantTickets).subList(offset, offset + Math.min(instantTickets.size() - offset, 65000));
				String sheetName = "page" + page;

				try {
					createSheet(workbook, sheetName, rows, page);
				} catch (Exception e) {
					throw new ServletException(e);
				}
			}

			try {
				workbook.write();
				workbook.close();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new ServletException(e);
			}
		return null;
	}
		*/

			
}
