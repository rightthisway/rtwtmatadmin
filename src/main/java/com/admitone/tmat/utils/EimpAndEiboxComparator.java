package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.EIBoxTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.EIMarketPlaceTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.OldEIMarketPlaceTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.eimarketplace.EIMPDBCredential;
import com.admitone.tmat.utils.eimarketplace.EIMPSimpleCredential;
import com.admitone.tmat.web.pojo.FeedAndCrawlComparatorVO;

public class EimpAndEiboxComparator {
	
	private static EimpAndEiboxComparator instance = null;
	
	private EimpAndEiboxComparator(){
		
	}
	
	public static EimpAndEiboxComparator getInstance() {
		if(instance == null){
			instance = new EimpAndEiboxComparator();
		}
		return instance;
	}

	public FeedAndCrawlComparatorVO compare(Integer eimpEventId, Integer eiboxEventId, Integer eiboxVenueId, Date eiboxEventDate){
		ComparatorTicketHitIndexer eiboxTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		ComparatorTicketHitIndexer eimpTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		//TicketListingFetcher feedTicketListingFetcher = new StubHubFeedTicketListingFetcher();
		EIMarketPlaceTicketListingFetcher eimpTicketListingFetcher = new EIMarketPlaceTicketListingFetcher();
		//eimpTicketListingFetcher.setCredential(new EIMPSimpleCredential("admit1", "trash1412"));
		eimpTicketListingFetcher.setCredential(new EIMPDBCredential());
		TicketListingFetcher eiboxTicketListingFetcher = new EIBoxTicketListingFetcher();
				
		
		Set<String> eiboxTicketIds = new HashSet<String>();
		Set<String> eimpTicketIds = new HashSet<String>();	
		
		System.out.println("EIMP EventId=" + eimpEventId);
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setSiteId(Site.EI_MARKETPLACE);
		ticketListingCrawl.setExtraParameter("queryEventId", eimpEventId.toString());
		ticketListingCrawl.resetStats();
		
		TicketListingCrawl eiboxTicketListingCrawl = new TicketListingCrawl();
		eiboxTicketListingCrawl.setSiteId(Site.EIBOX);
		//eiboxTicketListingCrawl.setQueryUrl(eimpUrl);
		eiboxTicketListingCrawl.setExtraParameter("queryEventId", eiboxEventId.toString());
		eiboxTicketListingCrawl.setExtraParameter("queryVenueId", eiboxVenueId.toString());
		eiboxTicketListingCrawl.setExtraParameter("queryEventDate", Integer.toString(eiboxEventDate.getDate()));
		
		try {
			eimpTicketListingFetcher.fetchTicketListing(eimpTicketHitIndexer, ticketListingCrawl);
		} catch (InterruptedTicketListingCrawlException e) {
			System.out.println("Inturupted exception while fetching Eimp");
			// do nothing
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception while fetching Eimp");
		}
		
		try{
			eiboxTicketListingFetcher.fetchTicketListing(eiboxTicketHitIndexer, eiboxTicketListingCrawl);
		} catch (InterruptedTicketListingCrawlException e) {
			System.out.println("Inturupted exception while fetching Eibox");
			// do nothing
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception while fetching Eimp");
		}
		
		Collection<TicketHit> eimpTicketHits = eimpTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> eimpTicketById = new HashMap<String, TicketHit>();
		for(TicketHit eimpTicketHit: eimpTicketHits) {
			eimpTicketIds.add(eimpTicketHit.getItemId());
			eimpTicketById.put(eimpTicketHit.getItemId(), eimpTicketHit);
		}
		
		
		Collection<TicketHit> eiboxTicketHits = eiboxTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> eiboxTicketById = new HashMap<String, TicketHit>();
		for(TicketHit eiboxTicketHit: eiboxTicketHits) {
			eiboxTicketIds.add(eiboxTicketHit.getItemId());
			eiboxTicketById.put(eiboxTicketHit.getItemId(), eiboxTicketHit);
		}
		
		Set<String> diffEimpTicketIds = new HashSet<String>(eimpTicketIds);
		Set<String> diffEiboxTicketIds = new HashSet<String>(eiboxTicketIds);

		System.out.println("TICKET INDEXED FROM FEED=" + eiboxTicketHits.size());
		System.out.println("TICKET INDEXED FROM WEB=" + eimpTicketHits.size());

		// see which ids appears in feed but not in web
		for(String ticketId: eiboxTicketIds) {
			diffEimpTicketIds.remove(ticketId);
		}
		
		System.out.println("EXTRA TICKET IN FEED : " + diffEimpTicketIds.size());
		ArrayList<TicketHit> extraTicketInEibox = new ArrayList<TicketHit>();
		for(String ticketId: diffEimpTicketIds) {
			TicketHit ticketHit = eimpTicketById.get(ticketId);
			extraTicketInEibox.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}
		
		// see which ids appears in web but not in feed
		for(String ticketId: eimpTicketIds) {
			diffEiboxTicketIds.remove(ticketId);
		}

		System.out.println("MISSING TICKET IN FEED : " + diffEiboxTicketIds.size());
		ArrayList<TicketHit> missingTicketInEibox = new ArrayList<TicketHit>();
		for(String ticketId: diffEiboxTicketIds) {
			TicketHit ticketHit = eiboxTicketById.get(ticketId);
			missingTicketInEibox.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}
		
		FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = new FeedAndCrawlComparatorVO();
		
		feedAndCrawlComparatorVO.setTicketIndexedFromFeed(eiboxTicketHits.size());
		feedAndCrawlComparatorVO.setTicketIndexedFromWeb(eimpTicketHits.size());
		feedAndCrawlComparatorVO.setExtraTicketInFeed(extraTicketInEibox);
		feedAndCrawlComparatorVO.setMissingTicketInFeed(missingTicketInEibox);
		
		
		eiboxTicketHitIndexer.reset();
		eimpTicketHitIndexer.reset();
		return feedAndCrawlComparatorVO;		
		
		
	}


}
