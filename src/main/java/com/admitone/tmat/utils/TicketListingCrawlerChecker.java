package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlQueue;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.User;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.web.Constants;

/**
 * Class to check that the crawler is working correctly.
 * Sends warning emails when the number of ready crawls or error crawls exceed threshold limits
 * 
 *  Checks:
 *  - ready crawls.
 *  - error crawls.
 */
public class TicketListingCrawlerChecker implements InitializingBean{
	private Logger logger = LoggerFactory.getLogger(TicketListingCrawlerChecker.class);
	
	private Timer timer = null;
	private TicketListingCrawler ticketListingCrawler;
	private static Date lastRun = new Date();
	private static TicketListingCrawlerChecker instance = null;
	private static Date lastSendMailCrawlThreadCountErrorDate = null;
	
	private static Date lastSendCrawlThresholdErrorDate = null;
	private static Date lastSendCrawl2ThresholdErrorDate = null;
	private static Date lastSendCrawl3ThresholdErrorDate = null;
	private static final long DEFAULT_TIMER_WAIT = 10L* 60* 1000L; //change to checkFrequency
	private static final long DELAY = 20L* 60* 1000L; // to prevent sending emails immediately after startup when the crawls are in ready state (change)
	
	public TicketListingCrawlerChecker() {		
	}
	
	public void afterPropertiesSet() throws Exception {
		reset();
	}

	public void reset() {
		if (timer != null) {
			timer.cancel();
		}
		
		Property checkFrequencyProperty = DAORegistry.getPropertyDAO().get("crawler.error.checkFrequency");
		int checkFrequency = Integer.parseInt(checkFrequencyProperty.getValue()) * 1000;
		
		logger.info("RESET CHECKER WITH FREQ OF " + checkFrequency);

		timer = new Timer();
	//	timer.scheduleAtFixedRate(new CrawlerCheckerTask(), DELAY, DEFAULT_TIMER_WAIT);
		timer.scheduleAtFixedRate(new CrawlerCheckerTask(), DELAY, checkFrequency);
	}
	
	class CrawlerCheckerTask extends TimerTask {		
		@Override
		public void run() {
			try{
			logger.info("Starting crawler checker task");
			System.out.println("Starting crawler cjecker task");

			String[] emailTo = DAORegistry.getPropertyDAO().get("crawler.email.to").getValue().split(",");
			int crawlDurationThreshold = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.crawlDurationThreshold").getValue());
			int readyCrawlCountThreshold = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.readyCrawlCountThreshold").getValue());
			
	//		boolean noTicketsFoundError ="1".equals(DAORegistry.getPropertyDAO().get("crawler.error.noTicketsFoundError").getValue());
			
			Date now = new Date();	
			 
/*			To check if it is working or not
 			List<TicketListingCrawlStat> stats = new ArrayList<TicketListingCrawlStat>(ticketListingCrawler.getTicketListingCrawlDispatcher().getStatMap().values());
			String clockError = "";
			for(TicketListingCrawlStat stat: stats) {
				int diffMinutes = (int)((now.getTime() - stat.getLastUpdate().getTime()) / (60L * 1000L)); 
				if (diffMinutes > 5) {
					clockError += stat.getHostname() + " runs " + diffMinutes + " mn backward.\n";
				} else if (diffMinutes < -5) {
					clockError += stat.getHostname() + " runs " + diffMinutes + " mn ahead.\n";					
				}
			}
			
			if (!clockError.isEmpty()) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("hostName", Constants.getInstance().getHostName());
				map.put("hostAddress", Constants.getInstance().getIpAddress());
				map.put("clockError", clockError);
				SpringUtil.getMailManager().sendMail("TMAT Clock Checker", emailTo, "[" + Constants.getInstance().getHostName() + "]", "mail-clock-error.txt", map, "text/plain");
			}			

*/			
	
			//check for ready crawls and slow running crawls
			Collection<TicketListingCrawl> failedCrawls = new ArrayList<TicketListingCrawl>();
			Collection<TicketListingCrawl> slowRunningCrawls = new ArrayList<TicketListingCrawl>();
			Map<String, Collection<TicketListingCrawl>> failedCrawlsByCreator = new HashMap<String, Collection<TicketListingCrawl>>();
			Map<String, Collection<TicketListingCrawl>> failedCrawlsByLastUpdater = new HashMap<String, Collection<TicketListingCrawl>>();
			
			int readyCrawlCount = 0;
		
			for (TicketListingCrawl crawl: TicketListingCrawlerChecker.getInstance().getTicketListingCrawler().getTicketListingCrawls()) {
				int threshold = crawlDurationThreshold;
				if(crawl.getSiteId().equals("ebay")){
					threshold = 900;
				}
				
				if (!crawl.isEnabled()) {
					continue;
				}
				
		
				if (crawl.getCrawlState().equals(CrawlState.STOPPED)
						&& (crawl.getEndCrawl() == null || (now.getTime() - crawl.getEndCrawl().getTime()) > crawl.getCrawlFrequency() * 1000L)) {
					readyCrawlCount++;
				}
				
				
				if (crawl.getCrawlState().equals(CrawlState.RUNNING)
						&& crawl.getStartCrawl() != null
						&& (now.getTime() - crawl.getStartCrawl().getTime()) / 1000 > threshold) {
					slowRunningCrawls.add(crawl);
				}
				
				if (crawl.getCrawlState().isError()){
					failedCrawls.add(crawl);
					}
				
//				if (crawl.getEndCrawl() == null 
//						|| crawl.getEndCrawl().before(lastRun) || crawl.getEndCrawl().after(now)
//						|| !crawl.getCrawlState().isError()
//						|| (!noTicketsFoundError && crawl.getCrawlState().isError() && "No tickets found".equals(crawl.getErrorMessage()))) {
//					continue;
//				}
				
				
				
		Collection<TicketListingCrawl> failedCrawlsOfSameCreator = failedCrawlsByCreator.get(crawl.getCreator());
				if (failedCrawlsOfSameCreator == null) {
					failedCrawlsOfSameCreator = new ArrayList<TicketListingCrawl>();
					failedCrawlsByCreator.put(crawl.getCreator(), failedCrawlsOfSameCreator);
				}
				failedCrawlsOfSameCreator.add(crawl);

		Collection<TicketListingCrawl> failedCrawlsOfSameLastUpdater = failedCrawlsByLastUpdater.get(crawl.getLastUpdater());
				if (failedCrawlsOfSameLastUpdater == null) {
					failedCrawlsOfSameLastUpdater = new ArrayList<TicketListingCrawl>();
					failedCrawlsByLastUpdater.put(crawl.getLastUpdater(), failedCrawlsOfSameLastUpdater);
				}
				failedCrawlsOfSameLastUpdater.add(crawl);
			}
			
			if (ticketListingCrawler.isRunning()
					&& (lastSendMailCrawlThreadCountErrorDate == null || now.getTime() - lastSendMailCrawlThreadCountErrorDate.getTime() > 20 * 60 * 1000)) {
			
		//	if (lastSendMailCrawlThreadCountErrorDate == null || now.getTime() - lastSendMailCrawlThreadCountErrorDate.getTime() > 30 * 60 * 1000) {
				boolean error = false;

				// FIXME: COMMENTED THIS PART BECAUSE IT DOES NOT WORK FOR NOW DUE TO THE CURRENT
				// CRAWLER REFACTORING
				/*
				Property crawlerThreadCountProperty = DAORegistry.getPropertyDAO().get("crawler.thread.count");
				int crawlThreadCount = Integer.parseInt(crawlerThreadCountProperty.getValue());
	
				
				if (ticketListingCrawler.getActiveThreadCount() != crawlThreadCount) {
					error = true;
					message += "Some theads might have died:\n"
							+ "- active thread count:" + ticketListingCrawler.getActiveThreadCount() + "\n"
							+ "- expected thread count:" + crawlThreadCount + "\n\n";
				}
				*/
				
				Map<String, Object> map = new HashMap<String, Object>();
	
				if (readyCrawlCount > readyCrawlCountThreshold) {
					error = true;
					map.put("readyCrawlCount", readyCrawlCount);
				}
				
				if (slowRunningCrawls.size() > 0) {
					error = true;
					map.put("slowRunningCrawls", slowRunningCrawls);
				}
				
				map.put("hostName", Constants.getInstance().getHostName());
				map.put("hostAddress", Constants.getInstance().getIpAddress());				
				
				if (error) {
					SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawler ready threshold reached", "mail-crawler-error.txt", map, "text/plain");
					lastSendMailCrawlThreadCountErrorDate = now;
				}
			}

			//error crawls by site
			
	Collection<TicketListingCrawl> runningTicketListingCrawls = new ArrayList<TicketListingCrawl>();
		
			int readyCrawlCount1 = 0;
			
			
			Map<String, Long> failedTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> runningTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> totalTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> readyTicketListingCrawlCountBySiteId = new HashMap<String, Long>();
			Map<String, Long> errorThresholdBySiteId = new HashMap<String, Long>();
			Map<String, Long> errorTotalBySiteId = new HashMap<String, Long>();
			Map<String, Long> errorFailedBySiteId = new HashMap<String, Long>();
			Map<String, String> errorSites = new HashMap<String, String>();
			
			String[] sites = Constants.getInstance().getSiteIds();
		
			 
		//	for(String sid: Constants.getInstance().getExtendedSiteIds()) {
			for(String sid: sites) {
				runningTicketListingCrawlCountBySiteId.put(sid, 0L);
				totalTicketListingCrawlCountBySiteId.put(sid, 0L);
				failedTicketListingCrawlCountBySiteId.put(sid, 0L);
				readyTicketListingCrawlCountBySiteId.put(sid, 0L);
				errorThresholdBySiteId.put(sid, 0L);
				errorTotalBySiteId.put(sid,0L);
				errorFailedBySiteId.put(sid,0L);
			}
			
			int failedTicketListingCrawlCount = 0;
			int totalTicketListingCrawlCount = 0;				
			
			for (TicketListingCrawl crawl: TicketListingCrawlerChecker.getInstance().getTicketListingCrawler().getTicketListingCrawls()) {
				totalTicketListingCrawlCount++;
				if (!crawl.isEnabled()) {
					continue;
				} 
				
totalTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), totalTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);

		
				if (crawl.getCrawlState().equals(CrawlState.RUNNING)) {
					runningTicketListingCrawls.add(crawl);
					runningTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), runningTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
				} else if (crawl.getCrawlState().isError()) {
					failedTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), failedTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
					failedTicketListingCrawlCount++;
				} else if (crawl.getCrawlState().equals(CrawlState.STOPPED)
						&& (crawl.getEndCrawl() == null
								|| (now.getTime() - crawl.getEndCrawl().getTime()) > crawl.getCrawlFrequency() * 1000L)) {
					readyTicketListingCrawlCountBySiteId.put(crawl.getSiteId(), readyTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
					readyCrawlCount1++;
				} 
			}
			
		
			Map<String, Object> map = new HashMap<String, Object>();
	
			long errorCrawlThreshold = 0;
			boolean isErrorThreshold = false;

			for(String siteId: sites) {
					Long failedCrawlsbySite = failedTicketListingCrawlCountBySiteId.get(siteId);
					Long totalCrawlsbySite = totalTicketListingCrawlCountBySiteId.get(siteId);
			
					if (totalCrawlsbySite > 0){
					errorCrawlThreshold = failedCrawlsbySite/totalCrawlsbySite *100;
					System.out.println("errorCrawlThreshold " +errorCrawlThreshold);
					
					if(errorCrawlThreshold>=10){
						isErrorThreshold = true;
						System.out.println("Error true ");
						errorSites.put(siteId,siteId);
						errorTotalBySiteId.put(siteId, totalCrawlsbySite);
						errorFailedBySiteId.put(siteId, failedCrawlsbySite);
						errorThresholdBySiteId.put(siteId, errorCrawlThreshold);
						System.out.println("Error sites " +errorSites);
						System.out.println("errorTotalBySiteId " +errorTotalBySiteId);
						System.out.println("errorFailedBySiteId " +errorFailedBySiteId);
						System.out.println("errorThresholdBySiteId " +errorThresholdBySiteId);
					}
					}
					map.put("errorSites",errorSites);
					map.put("errorTotalBySiteId", errorTotalBySiteId);
					map.put("errorFailedBySiteId", errorFailedBySiteId);
					map.put("errorThresholdBySiteId", errorThresholdBySiteId);
					System.out.println("map " +map );
			}
			
			
			if (isErrorThreshold){
				if 	((errorCrawlThreshold>=10) && ((lastSendCrawlThresholdErrorDate==null)||(now.getTime()-lastSendCrawlThresholdErrorDate.getTime()>10*60*1000))) {
		//		if ((errorThresholdBySiteId.get(siteId)>=10) && (errorThresholdBySiteId.get(siteId)<15)){	
				SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawler error threshold reached", "mail-crawler-error-threshold.txt", map, "text/plain");
				lastSendCrawlThresholdErrorDate = now;
		}
			
		//	if (errorThresholdBySiteId.get(siteId) >=0 && (now.getTime()-lastSendCrawlThresholdErrorDate.getTime()>1*60*1000)){
		//		if 	(errorCrawlThreshold>=0 &&	(now.getTime()-lastSendCrawlThresholdErrorDate.getTime()>1*60*1000) && lastSendCrawl2ThresholdErrorDate == null){
			//	if ((errorThresholdBySiteId.get(siteId)>=15) && (errorThresholdBySiteId.get(siteId)<20)){
		//		SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawler 15% error threshold reached", "mail-crawler-error-threshold.txt", map, "text/plain");
		//		lastSendCrawl2ThresholdErrorDate = now;
		//	}
		//	
		//	if (errorThresholdBySiteId.get(siteId) >=0 && (now.getTime()-lastSendCrawlThresholdErrorDate.getTime()>2*60*1000)&&(now.getTime()-lastSendCrawl2ThresholdErrorDate.getTime()>1*60*1000)){
		//		if 	(errorCrawlThreshold>=0 && (now.getTime()-lastSendCrawlThresholdErrorDate.getTime()>2*60*1000)&&(now.getTime()-lastSendCrawl2ThresholdErrorDate.getTime()>1*60*1000) && lastSendCrawl3ThresholdErrorDate ==null){
			//	if (errorThresholdBySiteId.get(siteId)>=20) {
		//		SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawler 20% error threshold reached", "mail-crawler-error-threshold.txt", map, "text/plain");
		//		lastSendCrawl3ThresholdErrorDate = now;
		//	}
			}
	//		}
			
			
			// for disabling the crawls after the error threshold is reached
//			Collection<TicketListingCrawl> updadetCrawls;
//			if (lastSendCrawl3ThresholdErrorDate!=null && (now.getTime()-lastSendCrawl3ThresholdErrorDate.getTime()>2*60*1000)){
//				for (String siteId: errorSites.keySet()) {
//					Collection<TicketListingCrawl> ticketListingSiteCrawls = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlBySite(siteId);
//					System.out.println("ticketListingSiteCrawls " +ticketListingSiteCrawls.size());
//					for (TicketListingCrawl ticketListingSiteCrawl: ticketListingSiteCrawls){
//						Integer crawlId = ticketListingSiteCrawl.getId();
//						ticketListingSiteCrawl.setEnabled(false);
//						DAORegistry.getTicketDAO().disableTickets(crawlId);
//						DAORegistry.getTicketListingCrawlDAO().update(ticketListingSiteCrawl);
//						updadetCrawls = DAORegistry.getTicketListingCrawlDAO().getAllEnabled();
//						System.out.println("updadetCrawls "+updadetCrawls.size());
//					}
//				}
//				SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawls disabled", "mail-crawler-error-threshold.txt", map, "text/plain");
//			}
			
			//
			// check if there are errors in crawls
			//

			boolean sendMailOnCrawlErrors = DAORegistry.getPropertyDAO().get("crawler.email.error.enabled").getValue().equals("1");
			if (!sendMailOnCrawlErrors || failedCrawls.size() == 0) {
				return;
			}
			
		//	sendCrawlErrorEmails("admin", emailTo, failedCrawls);
			
			
			// send email to creator
			boolean crawlerEmailCreatorEnabled = DAORegistry.getPropertyDAO().get("crawler.email.creator.enabled").getValue().equals("1");
			if (crawlerEmailCreatorEnabled) {
				for(Map.Entry<String, Collection<TicketListingCrawl>> entry: failedCrawlsByCreator.entrySet()) {
					String creator = entry.getKey();
					User user = DAORegistry.getUserDAO().getUserByUsername(creator);
					if (user.getEmailCrawlErrorCreatorEnabled()) {
						Collection<TicketListingCrawl> crawls  = entry.getValue();
						sendCrawlErrorEmails("creator", new String[] {creator}, crawls);
					}
				}
			}

			// send email to last updater
			boolean crawlerEmailLastUpdaterEnabled = DAORegistry.getPropertyDAO().get("crawler.email.lastUpdater.enabled").getValue().equals("1");
			if (crawlerEmailLastUpdaterEnabled) {
				for(Map.Entry<String, Collection<TicketListingCrawl>> entry: failedCrawlsByLastUpdater.entrySet()) {
					String lastUpdater = entry.getKey();
					User user = DAORegistry.getUserDAO().getUserByUsername(lastUpdater);
					if (user.getEmailCrawlErrorLastUpdaterEnabled()) {
						Collection<TicketListingCrawl> crawls  = entry.getValue();
						sendCrawlErrorEmails("lastUpdater", new String[] {lastUpdater}, crawls);
					}
				}
			}
			
	//		if ((lastSendCrawlThresholdErrorDate!=null) && (lastSendCrawl2ThresholdErrorDate!=null) && (lastSendCrawl3ThresholdErrorDate!=null)){
	//			lastSendCrawlThresholdErrorDate = null;
	//			lastSendCrawl2ThresholdErrorDate = null;
	//			lastSendCrawl3ThresholdErrorDate = null;
				
	//		}

			logger.info("Finished to run crawl checker");
			System.out.println("Finished to run checker");

			lastRun = now;		
			
//			map.clear();
//			lastSendCrawlThresholdErrorDate = null;
//			lastSendCrawl2ThresholdErrorDate = null;
//			lastSendCrawl3ThresholdErrorDate = null;
//			lastSendMailCrawlThreadCountErrorDate = null;
		}	catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
	
	public static TicketListingCrawlerChecker getInstance() {
		if (instance == null) {
			instance = new TicketListingCrawlerChecker();
		}
		return instance;
	}
	
	private void sendCrawlErrorEmails(String reason, String[] emailTo, Collection<TicketListingCrawl> failedCrawls) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("crawls", failedCrawls);
		map.put("hostName", Constants.getInstance().getHostName());
		map.put("hostAddress", Constants.getInstance().getIpAddress());
		map.put("reason", reason);
		SpringUtil.getMailManager().sendMail("TMAT Crawler Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Crawl Errors (" + reason + ")", "mail-crawl-error.txt", map, "text/plain");		
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}
	
}
