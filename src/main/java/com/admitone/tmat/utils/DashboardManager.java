package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;

public class DashboardManager {
	// every 15 minutes
	public static final long TOP_TICKET_COUNT_EVENTS_TASK_FREQUENCY = 15 * 60 * 1000;
	
	private List<Event> topTicketCountEvents = new ArrayList<Event>(); 
		
	public DashboardManager() {
		Timer timer = new Timer();
		timer.schedule(new TopTicketCountEventsTask(), 0, TOP_TICKET_COUNT_EVENTS_TASK_FREQUENCY);
	}	
	
	public List<Event> getTopTicketCountEvents() {
		return topTicketCountEvents;
	}
	
	public class TopTicketCountEventsTask extends TimerTask {
		public void run() {
			try {
				topTicketCountEvents = DAORegistry.getEventDAO().getTopTicketCountEvents(100);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	}
}
