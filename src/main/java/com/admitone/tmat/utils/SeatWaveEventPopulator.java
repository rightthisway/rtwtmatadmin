package com.admitone.tmat.utils;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.SeatWaveEvent;
import com.admitone.tmat.data.SeatWaveSeason;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

public class SeatWaveEventPopulator {
	private static Pattern datePattern = Pattern.compile("(\\d+)\\+");
	private static boolean running = false;
	private static int fetchedEventCount;
	private static Set<Integer> eventIdSet;
	private static Set<Integer> seasonIdSet;
	
	public static void start() {
		if (running) {
			return;
		}
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.execute(new Runnable() {
			
			public void run() {
				running = true;				
				try {
					SeatWaveEventPopulator.run();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					running = false;
				}
			}
		});
	}
	
	public static boolean isRunning() {
		return running;
	}
	
	/**
	 * Execute the http request again if it fails. Do it 5 times.
	 */
	public static CloseableHttpResponse safeExecuteHttpRequest(SimpleHttpClient httpClient, HttpUriRequest request) {
		for(int i = 0 ; i < 5 ; i++) {
			CloseableHttpResponse response = null;
			try {
				response = httpClient.execute(request);
				if (response != null) {
					return response;
				}
				// wait for one seconds before retrying...
				Thread.sleep(1000);
			} catch (Exception e) {
				continue;
			}
		}
		return null;
	}
	
	private static void run() {
		Date startDate = new Date();
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(10);

		fetchedEventCount = 0;
		eventIdSet = new HashSet<Integer>();
		seasonIdSet = new HashSet<Integer>();

		SimpleHttpClient httpClient = null;
		try {
			httpClient = HttpClientStore.createHttpClient("seatwave");
			// get genres
			// to get the session in the cookie
			
			int seasonPage = 1;
			while(true) {
				try {
					// System.out.println("PAGE=" + seasonPage);
					HttpGet httpGet = new HttpGet("http://api.seatwave.com/service.svc/v1/json/00000000011/seasons?page=" + seasonPage + "&size=1000");
					httpGet.addHeader("Host", "api.seatwave.com");		
					httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
					httpGet.addHeader("Accept-Encoding", "gzip,deflate");
					
					UncompressedHttpEntity entity = null;
					CloseableHttpResponse response = null;
					try{
						response = safeExecuteHttpRequest(httpClient, httpGet);
						
						if (response == null) {
							continue;
						}
						
						int statusCode = response.getStatusLine().getStatusCode();
						if (statusCode != 200) {
							break;
						}
						entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						response.close();
					}
					
					String content = EntityUtils.toString(entity);
					
					JSONArray seasonJSONArray = new JSONArray(content);
					if (seasonJSONArray.length() == 0) {
						break;
					}
					
					for(int i = 0 ; i < seasonJSONArray.length() ; i++) {
						
						JSONObject seasonJSON = seasonJSONArray.getJSONObject(i);
						Integer seasonId = seasonJSON.getInt("ID");
						Integer genreId = seasonJSON.getInt("GenreID");
						
						if (seasonIdSet.contains(seasonId)) {
							continue;
						}
						
						if (seasonJSON.getInt("Tickets") == 0) {
							continue;
						}
						
						seasonIdSet.add(seasonId);
						
						
						String seasonName = seasonJSON.getString("Name");
						String seasonUrl = seasonJSON.getString("URL");
						
						//System.out.println("SEASON NAME=" + seasonName);
						//System.out.println("SEASON URL=" + seasonUrl);
																
						executor.execute(new SeatWaveSeasonFetcherTask(seasonId, genreId, seasonName, seasonUrl));
					}
					
					
					seasonPage++;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
						

		if (httpClient != null) {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		
		try {
			executor.shutdown();
			// wait that all the tasks are executed
			executor.awaitTermination(2, TimeUnit.HOURS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DAORegistry.getSeatWaveEventDAO().deleteEventsNotUpdatedAfter(startDate);
		DAORegistry.getSeatWaveSeasonDAO().deleteSeasonsNotUpdatedAfter(startDate);
		
		
	}
	
	public static int getFetchedEventCount() {
		synchronized(SeatWaveEventPopulator.class) {
			return fetchedEventCount;
		}
	}
	
	public static class SeatWaveSeasonFetcherTask implements Runnable {
		private int seasonId;
		private String seasonName;
		private String seasonUrl;
		private int genreId;
		
		public SeatWaveSeasonFetcherTask(int seasonId, int genreId, String seasonName, String seasonUrl) {
			this.seasonId = seasonId;
			this.genreId = genreId;
			this.seasonName = seasonName;
			this.seasonUrl = seasonUrl;
		}
		
		public void run() {
			int performancePage = 1;
			
			SeatWaveSeason season = new SeatWaveSeason();
			season.setId(seasonId);
			season.setGenreId(genreId);
			season.setName(seasonName);
			season.setUrl(seasonUrl);
			season.setUpdatedDate(new Date());
			DAORegistry.getSeatWaveSeasonDAO().saveOrUpdate(season);
			// System.out.println("SEASON: id=" + seasonId + ", seasonName=" + seasonName);

			SimpleHttpClient httpClient = null;
			try {
				httpClient = HttpClientStore.createHttpClient("seatwave");
				while(true) {
					//System.out.println("START PERFORMANCE PAGE " + performancePage);
					try {
						HttpGet httpGet = new HttpGet("http://api.seatwave.com/service.svc/v1/json/00000000011/seasons/" + seasonId + "/performances?page=" + performancePage + "&size=1000");
						httpGet.addHeader("Host", "api.seatwave.com");		
						httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
						httpGet.addHeader("Accept-Encoding", "gzip,deflate");
												
						UncompressedHttpEntity entity = null;
						CloseableHttpResponse response = null;
						try{
							response = safeExecuteHttpRequest(httpClient, httpGet);
							
							if (response == null) {
								continue;
							}
							
							int statusCode = response.getStatusLine().getStatusCode();
							if (statusCode != 200) {
								break;
							}
							
							entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
						}catch(Exception e){
							e.printStackTrace();
						}finally{
							response.close();
						}
						
						String content = EntityUtils.toString(entity);
						// System.out.println("CONTENT=" + content);
						
						JSONArray performanceJSONArray = new JSONArray(content);
						if (performanceJSONArray.length() == 0) {
							break;
						}
						
						for(int j = 0 ; j < performanceJSONArray.length() ; j++) {
							JSONObject performanceJSON = performanceJSONArray.getJSONObject(j);
							int performanceId = performanceJSON.getInt("ID");
							
							synchronized(SeatWaveEventPopulator.class) {
								if (eventIdSet.contains(performanceId)) {
									continue;
								}
							}
	
							String performanceURL = performanceJSON.getString("URL");
							String comments = performanceJSON.getString("Comments");
							String performanceDateString = performanceJSON.getString("Date");
							
							Matcher dateMatcher = datePattern.matcher(performanceDateString);
							dateMatcher.find();
							Long dateInMilliSeconds = Long.parseLong(dateMatcher.group(1));
													
							JSONObject venueJSON = performanceJSON.getJSONObject("venue");
		
							Integer venueId = venueJSON.getInt("ID");
							String venueName = venueJSON.getString("Name");
							String venueTown = venueJSON.getString("Town");
							String venueCountry = venueJSON.getString("Country");
							String venueCountryCode = venueJSON.getString("CouISO2");
	
							Date performanceDate = new Date(dateInMilliSeconds + 5L * 3600000L);	
	
							SeatWaveEvent event = new SeatWaveEvent();
							event.setId(performanceId);
							event.setSeasonId(seasonId);
							event.setName(seasonName);
							event.setComments(comments);
							event.setEventDate(performanceDate);
							event.setVenueId(venueId);
							event.setVenueCountry(venueCountry);
							event.setVenueCountryCode(venueCountryCode);
							event.setVenueName(venueName);
							event.setVenueTown(venueTown);
							event.setUrl(performanceURL);
							event.setUpdatedDate(new Date());
							
							// System.out.println("EVENT=" + event);
		
							DAORegistry.getSeatWaveEventDAO().saveOrUpdate(event);
							synchronized(SeatWaveEventPopulator.class) {
								fetchedEventCount++;
								eventIdSet.add(performanceId);
							}
						}
						performancePage++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			HttpClientStore.releaseHttpClient(httpClient);						
		
		}
	}
}
