package com.admitone.tmat.utils;

import java.io.IOException;
import java.lang.StringBuffer;
import java.util.StringTokenizer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.HashMapFeedInfoCache;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.io.FeedException;

public class CurrencyRSSFetcher {
	private FeedFetcherCache feedInfoCache = null;
	private FeedFetcher feedFetcher = null;
	private SyndFeed feed = null;
	public CurrencyRSSFetcher(String currencyURL) throws FeedException, FetcherException {
		try {
			//Cache will only fetch feed if it is updated.
			feedInfoCache = HashMapFeedInfoCache.getInstance();
		 	feedFetcher = new HttpURLFeedFetcher(feedInfoCache);
		 	feed = feedFetcher.retrieveFeed(new URL(currencyURL));
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public Double getCurrency() throws FeedException, FetcherException {

	 	StringBuffer display = new StringBuffer("");
	 	StringTokenizer st = null;
	 	//Loop through object to get details
	 	for(SyndEntry entry : (List<SyndEntry>) feed.getEntries()){
	 		if(entry.getDescription().getValue().length() > 0){
	 			st = new StringTokenizer(entry.getDescription().getValue());
	 			display.append(entry.getDescription().getValue());
	 		}
	 	}
	 	String dollarsInAPound = "";
	 	for(int i = 0 ; i < 4 && st.hasMoreTokens() ; i++){
	 		String token = st.nextToken();
	 		if(i == 3){
	 			dollarsInAPound = token; 
	 		}
	 	}
	 	Double conversionRate = new Double(dollarsInAPound);
	 	// System.out.println("conversionRate: " + conversionRate);
	 	return conversionRate;
	}
}
	