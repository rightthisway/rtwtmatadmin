package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TicketQuote;
import com.admitone.tmat.web.pojo.QuotesEvent;

public final class TicketQuotesUtil {
	private TicketQuotesUtil() {}
	
	public static Collection<QuotesEvent> getQuotesEvent(Collection<TicketQuote> ticketQuotes) {
		Map<Integer, QuotesEvent> quotesEventMap = new HashMap<Integer, QuotesEvent>();
		
		for (TicketQuote quote: ticketQuotes) {
			Ticket ticket = quote.getTicket();
			// skip tickets which have been moved to historical or disappearead
			// for whatever reason
			if (ticket == null) {
				continue;
			}
			Integer eventId = ticket.getEventId();
			
			QuotesEvent quotesEvent = quotesEventMap.get(eventId);
			if (quotesEvent == null) {
				quotesEvent = new QuotesEvent(quote.getTicket().getEvent());
				quotesEventMap.put(eventId, quotesEvent);
			}
			
			quotesEvent.addQuote(quote);
		}
		
		return quotesEventMap.values();
	}
	
	public static Collection<java.sql.Date> getDistinctDates(Collection<TicketQuote> ticketQuotes) {
		Set<java.sql.Date> dates = new HashSet<java.sql.Date>();
		for (TicketQuote ticketQuote: ticketQuotes) {
			dates.add(ticketQuote.getDate());
		}
		
		return dates;
	}
}
