package com.admitone.tmat.utils;

import java.util.HashMap;
import java.util.Map;

public class QueryUtil {	
	
	public static boolean matchAllTerms(String query, String content) {
		if (query == null) {
			return true;
		}
		
		String formattedQuery = query.trim().toLowerCase();
		
		if (formattedQuery.length() == 0) {
			return true;
		}
		
		if (content == null) {
			return false;
		}

		String formattedContent = content.trim().toLowerCase();		
		
		String[] keywords = formattedQuery.split(" ");
		// check if every keywords match the content
		for (String keyword: keywords) {
			if (!formattedContent.contains(keyword)) {
				return false;
			}
		}
		return true;
	}

	public static boolean matchAnyTerms(String query, String content) {
		if (query == null || query.isEmpty()) {
			return true;
		}
		
		String formattedQuery = query.trim().toLowerCase();
		
		if (formattedQuery.isEmpty()) {
			return true;
		}
		
		if (content == null) {
			return false;
		}
		
		String formattedContent = " " + content.trim().toLowerCase() + " ";		
		
		String[] keywords = formattedQuery.split(" ");
		// check if every keywords match the content
		for (String keyword: keywords) {
			if (formattedContent.contains(" " + keyword + " ")) {
				return true;
			}
		}
		return false;
	}

	public static boolean matchAtleastHalfTerms(String query, String content) {
		if (query == null || query.isEmpty()) {
			return true;
		}
		
		String formattedQuery = query.trim().toLowerCase();
		
		if (formattedQuery.isEmpty()) {
			return true;
		}
		
		if (content == null) {
			return false;
		}
		
		String formattedContent = " " + content.trim().toLowerCase() + " ";		
		
		String[] keywords = formattedQuery.split(" ");
		// check if every keywords match the content
		int counter=0;
		for (String keyword: keywords) {
			if (formattedContent.contains(" " + keyword + " ")) {
				counter++;
			}
		}
		if(counter>=keywords.length/2){
			return true;
		}
		return false;
	}
	/**
	 * Return matching score.
	 * For now, we use this basic rule:
	 * - for each query term which matches the content, we increase the matching score by one.
	 * @param query
	 * @param content
	 * @return
	 */
	public static int getMatchingScore(String query, String content) {
		return getMatchingScore(query, content, true);
	}

	public static int getMatchingScore(String query, String content, boolean normalize) {
		if (query == null) {
			return 0;
		}
		
		String formattedQuery = query;
		if (normalize) {
			formattedQuery = normalizeQuery(query);
		}
		
		if (formattedQuery.length() == 0) {
			return 0;
		}
		
		if (content == null) {
			return 0;
		}

		String formattedContent = content;
		if (normalize) {		
			formattedContent = normalizeQuery(content);
		}
		
		int matchingScore = 0;
		
		String[] contentKeywords = formattedContent.split(" ");
		String[] queryKeywords = formattedQuery.split(" ");
		// check if every keywords match the content
		for (String queryKeyword: queryKeywords) {
			for(String contentKeyword: contentKeywords) {
				if (queryKeyword.equals(contentKeyword)) {
					matchingScore++;
					break;
				}
			}
		}
		return matchingScore;
	}

	/*
	public static boolean matchDate(Date fromDate,Date toDate, Date date) {
		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.setTime(fromDate);
		fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
		fromCalendar.set(Calendar.MINUTE, 0);
		fromCalendar.set(Calendar.SECOND, 0);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(toDate);
		toCalendar.set(Calendar.HOUR_OF_DAY, 23);
		toCalendar.set(Calendar.MINUTE, 59);
		toCalendar.set(Calendar.SECOND, 59);

		return !(date.before(fromCalendar.getTime()) || date.after(toCalendar.getTime()));
	}
	*/

	private static String[] stopWords = {
		"and", "the", "in", "for", "of", "vs", "game", "games", "football", "baseball", "basketball", "us", "new"
	};
	private static Map<String, Boolean> isStopWord = new HashMap<String, Boolean>();
	static {
		for(String word: stopWords) {
			isStopWord.put(word, true);
		}
	}
	
	public static String normalizeQuery(String str) {
		// remove special character
		str = str.toLowerCase();
		str = str.replaceAll("(?:\\W|\\s)+", " ").trim();
		String[] tokens = str.split(" ");

		String result = "";
		for(String token: tokens) {
			if (isStopWord.containsKey(token)) {
				continue;
			}
			
			// check if the token is a number: 2010 or 3
			if (token.matches("^\\d+$")) {
				continue;
			}
			result += token + " ";
		}
		return result.trim();
		
		/*
		String[] tokens = str.split("\\s");
		for(str)
		return str.replaceAll("\\s", "");
		*/
	}

	public static void main(String[] args) {
		//matchAnyTerms("winter games", "Winter Games/Ice Hockey");
		System.out.println("MATCH=" + matchAnyTerms(normalizeQuery("new"), normalizeQuery("mamma mia")));
		System.out.println("norm: " + normalizeQuery("mamma mia"));
	}

}
