package com.admitone.tmat.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.pojo.SVGFileMissingZoneData;
import com.admitone.tmat.web.TMATReportController;

public class RTFMissingSvgAndCsvZones extends QuartzJobBean implements StatefulJob{
	
	private static TMATReportController tMATReportController = new TMATReportController();

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		String today = dateFormat.format(new Date());
		FileOutputStream out = null;
		try{
			Map<Integer,List<VenueCategory>> venueCategoriesMap = new HashMap<Integer, List<VenueCategory>>();
			
			 Collection<Venue> venues = DAORegistry.getVenueDAO().getAllVenuesForUSandCA();
			 List<VenueCategory> venueCategoriesList = DAORegistry.getVenueCategoryDAO().getAllVenueCategoriesForUSandCA();
			 
			 for (VenueCategory venueCategory : venueCategoriesList) {
				 Integer venueId = venueCategory.getVenue().getId();
				 List<VenueCategory> tempVenueCats = venueCategoriesMap.get(venueId);
				 if(tempVenueCats == null) {
					 tempVenueCats = new ArrayList<VenueCategory>();
				 }
				 tempVenueCats.add(venueCategory);
				 venueCategoriesMap.put(venueId, tempVenueCats);
			}
			 
			 FileReader fReader = null;
			 BufferedReader reader = null;
			 String svgText = "",str="";
			 List<SVGFileMissingZoneData> missingSvgZonesList = new ArrayList<SVGFileMissingZoneData>();
			 List<SVGFileMissingZoneData> missingCSVZonesList = new ArrayList<SVGFileMissingZoneData>();
			 List<SVGFileMissingZoneData> invalidFormatSVGFiles = new ArrayList<SVGFileMissingZoneData>();
			 
			 int totVenues=venues.size(),processedVenues=0;
			 for (Venue venue : venues) {
				 Integer venueId = venue.getId();
				 
				 processedVenues++;
				 if(processedVenues%100 == 0) {
					 System.out.println(processedVenues+" / "+totVenues+" : SVG and CSV Missing Venue : "+venueId);
				 }
				 
				 List<VenueCategory> venueCategories = venueCategoriesMap.get(venueId);
				 if(venueCategories == null) {
					 continue;
				 }
				 //List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueIdOrderByCategory(venueId);
			
				 for (VenueCategory venueCatObj : venueCategories) {
					 try {
						 String fileName = venueId+"_"+venueCatObj.getCategoryGroup();
						 List<String> missingCsvZones = new ArrayList<String>();
						 List<String> missingSvgZones = new ArrayList<String>();
						
						 
						 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
						 Map<String, Boolean> csvZoneMap = new HashMap<String, Boolean>();
						 
						 List<String> zoneList = DAORegistry.getQueryManagerDAO().getAllZoneByVenueCategory(venueId);
						 if(zoneList != null) {
							 for (String zone : zoneList) {
								 csvZoneMap.put(zone.toUpperCase(), true);
							}
						 }
						 
						 str ="";
						 File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//"+fileName+".txt");
						 if(svgTxtFile != null && svgTxtFile.exists()) {
							fReader = new FileReader(svgTxtFile);
							reader = new BufferedReader(fReader);
							while((str = reader.readLine()) != null) {
								svgText += str;
							}
							try {
								svgZoneMap = tMATReportController.getAllZonesFromText(svgText);
							} catch(Exception e) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 invalidFormatSVGFiles.add(zoneData);
								 continue;
							}
						 }
						 
						 for (String key : svgZoneMap.keySet()) {
							 Boolean isZonethere = csvZoneMap.remove(key);
							 if(null == isZonethere || !isZonethere){
								 missingCsvZones.add(key);
							 }
						 }
						 for (String key : csvZoneMap.keySet()) {
							 missingSvgZones.add(key);
						 }
						 
						 if(!missingSvgZones.isEmpty()) {
							 for (String zone : missingSvgZones) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 zoneData.setZone(zone);
								 missingSvgZonesList.add(zoneData);
								 
							}
						 }
						 if(!missingCsvZones.isEmpty()) {
							 for (String zone : missingCsvZones) {
								 SVGFileMissingZoneData zoneData = new SVGFileMissingZoneData(venue);
								 zoneData.setVenueCategoryId(venueCatObj.getId());
								 zoneData.setCategoryGroupName(venueCatObj.getCategoryGroup());
								 zoneData.setZone(zone);
								 missingCSVZonesList.add(zoneData);
								 
							}
						 }
					 } catch (Exception e) {
						 e.printStackTrace();
					}
				}
			 }
			 SXSSFWorkbook workbook = new SXSSFWorkbook();
			 tMATReportController.generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Zones not in CSV"),missingCSVZonesList,true);
			 tMATReportController.generateMissingSVGandCSVzonesFile(workbook.createSheet("CSV Zones not in SVG"),missingSvgZonesList,true);
			 tMATReportController.generateMissingSVGandCSVzonesFile(workbook.createSheet("SVG Files with Invalid Format Data"),invalidFormatSVGFiles,false);
			
			File file = new File("c:\\external files\\uncategorized\\SVG_and_CSV_Missing_Zones." + today + ".xlsx");
			if(!file.exists()){
				file.createNewFile();
			}
			out = new FileOutputStream(file);
			workbook.write(out);
			out.flush();
		 }catch (Exception e){
			 e.printStackTrace();
		 }finally{
			 try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		
	}

}
