package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ValuationFactor;

public class ValuationFactorUtil {
	private static Pattern numberOrLetterBlock = Pattern.compile("([^\\d]+)|(\\d+)");
	private static Map<Integer, Collection<ValuationFactor>> valuationFactorMapByEventId = new HashMap<Integer, Collection<ValuationFactor>>();
	
	public void init() {
		for (ValuationFactor factor: DAORegistry.getValuationFactorDAO().getAll()) {
			Collection<ValuationFactor> list = valuationFactorMapByEventId.get(factor.getEventId());
			if (list == null) {
				list = new ArrayList<ValuationFactor>();
				valuationFactorMapByEventId.put(factor.getEventId(), list);
			}
			
			list.add(factor);
		}
	}
	
	public static void update(Integer artistId) {
		for (Event event: DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId)) {
			Collection<ValuationFactor> list = DAORegistry.getValuationFactorDAO().getAllValuationFactorsFromEvent(event.getId());
			valuationFactorMapByEventId.put(event.getId(), list);			
		}
	}
	
	public static Integer getValuationFactor(Integer eventId, String section, String row) {
		Collection<ValuationFactor> factors = valuationFactorMapByEventId.get(eventId);
		if (factors == null) { 
			return null;
		}
		
		for (ValuationFactor factor: factors) {
			Integer cmp1 = compare(factor.getStartSection(), section);
			Integer cmp2 = compare(section, factor.getEndSection());
			Integer cmp3 = compare(factor.getStartRow(), row);
			Integer cmp4 = compare(row, factor.getEndRow());
			if (cmp1 == null || cmp2 == null
				|| cmp3 == null || cmp4 == null) {
					continue;
			}
			
			if (cmp1 <= 0 && cmp2 <= 0 && cmp3 <= 0 && cmp4 <= 0) {
				return factor.getValue();
			}
		}
		return null;
	}
	
	/*
	 * Compare by letter blocks and number blocks
	 * (e.g., ab56 > ab8 even though alphabetically it's not the case)
	 * we compare block per block so in this case first "ab" and "ab" then 56 and 8
	 * However if we have to compare things like: 20C and 19, then these are not comparable
	 * 
	 * So the idea is when we compare 20C and 19
	 * is first to compare that potentially 19 is less than 20. Then keep going and see that these 2 are not
	 * comparable
	 * 
	 * Same thing for 20C and 21BT, these 2 are not comparable
	 * However 20C and 21B are comparable
	 * 
	 * so we parse block per block (of letters of numbers) and whenever we
	 * find which is one is greater than the others, we keep going to check
	 * that the 2 symbols are comparable.
	 * 
	 * New addition: now, we have one more restriction: 12C and 14D are not comparable
	 * However 12C and 14C are.
	 * 
	 * They are comparable if:
	 * - they have the same structure: same alternance of number and letter blocks
	 * - letters block which are compared are of the same size (cannot compare C and CD for instance)
	 */
	
	public static Integer compare(String symbol1, String symbol2) {
		if (symbol1 == null || symbol2 == null) {
			return null;
		}
		
		symbol1 = symbol1.replaceAll(" ", "").toLowerCase();
		symbol2 = symbol2.replaceAll(" ", "").toLowerCase();

		if (symbol1.equals("*")) {
			return 0;
		}

		if (symbol2.equals("*")) {
			return 0;
		}
		
		if (symbol1.length() == 0 || symbol2.length() == 0) {
			return null;
		}
		
		if (symbol1.charAt(0) == '>') {
			for (String token: symbol1.split(">")) {
				
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				
				if (symbol2.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		if (symbol2.charAt(0) == '>') {
			for (String token: symbol2.split(">")) {
				token = token.trim();
				if (token.isEmpty()) {
					continue;
				}
				if (symbol1.indexOf(token) >= 0) {
					return 0;
				}
			}
			
			return null;
		}

		Matcher matcher1 = numberOrLetterBlock.matcher(symbol1);
		Matcher matcher2 = numberOrLetterBlock.matcher(symbol2);
		Integer offset1 = 0;
		Integer offset2 = 0;
		
		Integer result = 0;
		while (true) {
			Boolean match1 = matcher1.find(offset1);
			Boolean match2 = matcher2.find(offset2);
			
//			System.out.println("MATCH " + match1 + " " + match2);
			if (match1 == false && match2 == false) {
				return result;
			}

			// if one has a next block and not the other => not comparable
			if (match1 == false || match2 == false) {
				return null;
			}

			String letters1 = matcher1.group(1);
			String letters2 = matcher2.group(1);

			// block of numbers to compare
			if (letters1 == null && letters2 == null) {
				String numberStr1 = matcher1.group(2);
				String numberStr2 = matcher2.group(2);
				Integer number1  = Integer.parseInt(numberStr1);
				Integer number2  = Integer.parseInt(numberStr2);
				
				if (number1 < number2) {
					// result already found
					if (result == 0) {
						result = -1;
					} else { // not comparable
						return null;
					}
				} else if (number1 > number2) {
					// result already found
					if (result == 0) {
						result = 1;
					} else { // not comparable
						return null;
					}
				}

				offset1 += numberStr1.length();
				offset2 += numberStr2.length();
			} else if (letters1 != null & letters2 != null) {
				// block of letters to compare
				// must have the same length othewise not comparable
				if (letters1.length() != letters2.length()) {
					return null;
				}
				
				Integer cmp = letters1.compareToIgnoreCase(letters2);
				if (cmp < 0 || cmp > 0) {
					// result already found
					if (result == 0) {
						result = cmp;
					} else { // not comparable
						return null;						
					}
				}
				
				offset1 += letters1.length();
				offset2 += letters2.length();
			} else {
				// letter vs. number
				return null;				
			}
		}
		
	}
}
