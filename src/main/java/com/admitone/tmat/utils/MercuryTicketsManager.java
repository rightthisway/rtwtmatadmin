package com.admitone.tmat.utils;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;



/**
 * To copy the TicketNetwork mercury tickets file periodically every 15 minutes to the specified file location in dbo.property
 *
 */
public class MercuryTicketsManager {
private static final int MERCURY_TIX_TIMER_INTERVAL = 24* 60 * 60 * 1000;
	
	/*public MercuryTicketsManager()
	{
		Calendar calendar = new GregorianCalendar();
		
		Calendar todayfourAM = new GregorianCalendar();
		todayfourAM.set(Calendar.HOUR, 4);
		todayfourAM.set(Calendar.MINUTE, 0);
		todayfourAM.set(Calendar.SECOND, 0);
		todayfourAM.set(Calendar.MILLISECOND, 0);

		Calendar firstTimeToRun = new GregorianCalendar();

		if(todayfourAM.after(calendar)){
			firstTimeToRun = todayfourAM;
		}else{
			Calendar tomorrowfourAM = todayfourAM;
			tomorrowfourAM.add(Calendar.DATE, 1);
			firstTimeToRun = tomorrowfourAM;
		}

		int minute = calendar.get(Calendar.MINUTE);
		
		minute = (int)(Math.ceil((double)minute / 15D)) * 15;
		
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, 0);

		Timer timer = new Timer();
		System.out.println("Starting Mercury Tickets Manager......The first run will be at: " + firstTimeToRun.toString());
		timer.scheduleAtFixedRate(
				new TimerTask(){
				public void run(){
					try{
						System.out.println("About to compute Mercury Tickets..the time is: " + new Date());
						Collection<Ticket> mercuryTickets = MercuryTicketsUtil.computeMercuryTickets();
						System.out.println("About to compute customized Mercury Tickets..the time is: " + new Date());
						Collection<Ticket> mercuryCustomizedTickets = MercuryTicketsUtil.computeCustomizedCatMercuryTickets();
						System.out.println("Size of mercuryTickets: " + mercuryTickets.size());
						System.out.println("Size of mercuryCustomizedTickets: " + mercuryCustomizedTickets.size());
						String fileLoc ="";
						OutputStream custOutput =null;
//						OutputStream scrubbedOutput=null;
						OutputStream output =null;
						try{
							Property property = DAORegistry.getPropertyDAO().get("mercuryTickets.fileLoc");
							fileLoc = property.getValue();
							output = new FileOutputStream(fileLoc, false);
							System.out.println("About to download Mercury Tickets CSV file.. the time is: " + new Date());
							MercuryTicketsUtil.downloadMercuryTicketCsvFile(mercuryTickets,output);
							System.out.println("Finished download of Mercury Tickets CSV file.. the time is: " + new Date());
							
						}catch(Exception ex){
							ex.printStackTrace();
						}finally{
							output.close();
						}
						
						try{
							Property property = DAORegistry.getPropertyDAO().get("mercury.tnd.Tickets.fileLoc");
							fileLoc = property.getValue();
							output = new FileOutputStream(fileLoc, false);
							System.out.println("About to download Mercury TND Tickets CSV file.. the time is: " + new Date());
							MercuryTicketsUtil.downloadMercuryTNDTicketCsvFile(mercuryTickets,output);
							System.out.println("Finished download of Mercury TND Tickets CSV file.. the time is: " + new Date());
						}catch(Exception ex){
							ex.printStackTrace();
						}finally{
							output.close();
						}
						
						try{
							Property custProperty = DAORegistry.getPropertyDAO().get("mercuryCustomizedTickets.fileLoc");
							fileLoc = custProperty.getValue();
							custOutput = new FileOutputStream(fileLoc, false);
							System.out.println("About to download Customized Mercury Tickets CSV file.. the time is: " + new Date());
							MercuryTicketsUtil.downloadCustomizedMercuryTicketCsvFile(mercuryCustomizedTickets,custOutput);
							System.out.println("Finished download of Customized Mercury Tickets CSV file.. the time is: " + new Date());
							
						}
						catch(Exception e){
							e.printStackTrace();
						}finally{
							custOutput.close();
						}
						
						try{
							Property custProperty = DAORegistry.getPropertyDAO().get("mercury.tnd.CustomizedTickets.fileLoc");
							fileLoc = custProperty.getValue();
							custOutput = new FileOutputStream(fileLoc, false);
							System.out.println("About to download Customized Mercury TND Tickets CSV file.. the time is: " + new Date());
							MercuryTicketsUtil.downloadCustomizedMercuryTNDTicketCsvFile(mercuryCustomizedTickets,custOutput);
							System.out.println("Finished download of Customized Mercury TND Tickets CSV file.. the time is: " + new Date());
						}
						catch(Exception e){
							e.printStackTrace();
						}finally{
							custOutput.close();	
						}
						
						*//**
						 * Gets unique tickets for  TN Mercury
						 *//*
						
						try{
							Property scrubbedTickets=DAORegistry.getPropertyDAO().get("mercury.tnd.ScrubedTickets.fileLoc");
							fileLoc=scrubbedTickets.getValue();
							scrubbedOutput = new FileOutputStream(fileLoc, false);
							MercuryTicketsUtil.downloadCustomizedScrubedMercuryTNDTicketCsvFile(scrubbedOutput);
						}catch(Exception e){
							e.printStackTrace();
						}finally{
							scrubbedOutput.close();
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				}
				, firstTimeToRun.getTime(), MERCURY_TIX_TIMER_INTERVAL);	
	}*/
}
