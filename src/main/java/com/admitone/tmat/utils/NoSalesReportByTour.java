package com.admitone.tmat.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.mail.MailAttachment;

public class NoSalesReportByTour  extends QuartzJobBean implements StatefulJob{
		
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		
		/*List<Object[]> list = new ArrayList<Object[]>();
		DateFormat format = new SimpleDateFormat("MM/dd/yy HH:mm");
		list = DAORegistry.getTourDAO().getNoSalesReportByTour();
		StringBuilder sb = new StringBuilder();
		sb.append("Tour Name,StartDate,EndDate,Tour Type,Venue Name");
		sb.append("\n");
		
		if(list != null && !list.isEmpty()){
			for(Object[] listOb : list){
				if(listOb[0] != null)
					sb.append(((String) listOb[0]).replaceAll(",", " ")+",");
				else
					sb.append(",");
				
				if(listOb[1] != null)
					sb.append(String.valueOf(format.format(new Date(((Timestamp) listOb[1]).getTime()))) + ",");
				else
					sb.append(",");
				
				if(listOb[2] != null)
					sb.append(String.valueOf(format.format(new Date(((Timestamp) listOb[2]).getTime()))) + ",");
				else
					sb.append(",");
				
				if(listOb[3] != null)
					sb.append((String.valueOf(listOb[3])).replaceAll(",", " ")+ ",");
				else
					sb.append(",");
				
				if(listOb[4] != null)
					sb.append((String.valueOf(listOb[4])).replaceAll(",", " ")+ ",");
				else
					sb.append(",");
				
				sb.append("\n");
			}
		}
		
		String fromName  = "tmatmanagers@rightthisway.com";
		String mimeType = "text/html";
		String toAddress ="pdhebar@rightthisway.com";
		String ccAddress = "";
		String subject = "No Sales Report...";
		String template = "no-sales-report.html";
		
		try {
			String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
			Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
			String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
			String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
			String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
			Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
			String transportProtocol = "";
			
			SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
					smtpUsername, smtpPassword, null, fromName, toAddress, ccAddress, null,subject,template, 
					new HashMap<String, Object>(), mimeType,new MailAttachment[]{new MailAttachment(IOUtils.toByteArray(sb.toString()),"application/vnd.ms-excel","NoSalesReport.csv")});
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}

	
	
}
