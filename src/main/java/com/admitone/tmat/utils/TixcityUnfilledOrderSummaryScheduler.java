package com.admitone.tmat.utils;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.TixcityUnfilledOrderSummary;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;

public class TixcityUnfilledOrderSummaryScheduler  extends QuartzJobBean implements StatefulJob{

	private static SharedProperty sharedProperty;
	private static MailManager mailManager;
	
	
	
	public static SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		TixcityUnfilledOrderSummaryScheduler.sharedProperty = sharedProperty;
	}
	public static MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		TixcityUnfilledOrderSummaryScheduler.mailManager = mailManager;
	}



	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			
			String message = processTixcityUnfilledOrderSummary();
			if(message != null) {
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String subject = "Tixcity UnFilled Order Summary Report Failed : ";
				String template = "common-mail-error.html";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("bodyContent", "Following error occured while processing Tixcity UnFilledOrder Summary Report : ");
				map.put("error", message);
				//String toAddress = "pmehul@rightthisway.com";
				String toAddress = "AODev@rightthisway.com";
				mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error Occured in Tixcity UnFilled Order Summary Report Scheduler : "+ new Date());
			try {
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String subject = "Tixcity UnFilled Order Summary Report Failed : ";
				String template = "common-mail-error.html";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("bodyContent", "Following error occured while processing Tixcity UnFilledOrder Summary Report : ");
				map.put("error", e);
				//String toAddress = "pmehul@rightthisway.com";
				String toAddress = "AODev@rightthisway.com";
				mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			}catch (Exception ee) {
				ee.printStackTrace();
				System.err.println("Error Occured in while processing Tixcity UnFilledOrder Summary Repor : "+ new Date());
			}
		}
	}
	public static String processTixcityUnfilledOrderSummary() throws Exception {
		try {

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			
			Date summaryDate = new Date(cal.getTimeInMillis());
			int frequencydays = 1;
			
			boolean flag = true;
			double lastweekTotalAmt = 0;
			TixcityUnfilledOrderSummary tixcityOrderSummaryDb = DAORegistry.getTixcityUnfilledOrderSummaryDAO().getLastWeekUnfilledOrderSummary();
			if(tixcityOrderSummaryDb != null) {
				if((summaryDate.getTime() - tixcityOrderSummaryDb.getSummaryDate().getTime()) < (1000*60*60*24*frequencydays)) {
					flag = false;
					return "Last Report generated date was within "+frequencydays+": "+tixcityOrderSummaryDb.getSummaryDate();
					//System.out.println("frequencydays : "+frequencydays);
				}
				lastweekTotalAmt = tixcityOrderSummaryDb.getTotalAmount();
			}
			
			DateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String summaryDateStr = dbDateFormat.format(summaryDate);
			Double totalAmount = DAORegistry.getQueryManagerDAO().getTixCityUnfilledTotalWholesaleAmount(summaryDateStr);
			if(totalAmount != null) {
				double grossAmt = totalAmount - lastweekTotalAmt;
				double grosPerc = (double) Math.round((grossAmt * 100 / totalAmount)*100)/100;
				
				TixcityUnfilledOrderSummary tixcityOrderSummary = new TixcityUnfilledOrderSummary();
				tixcityOrderSummary.setCreatedDate(new Date());
				tixcityOrderSummary.setTotalAmount(totalAmount);
				tixcityOrderSummary.setSummaryDate(summaryDate);
				tixcityOrderSummary.setGrossAmount(grossAmt);
				tixcityOrderSummary.setGrossPercentage(grosPerc);
				DAORegistry.getTixcityUnfilledOrderSummaryDAO().save(tixcityOrderSummary);
			} else {
				flag = false;
				return "Error Occured While getting Summary Amount";
			}
			
			SXSSFWorkbook workbook = getAllTixcityUnfilledORderSummaryReport();
		    ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    try {
		        workbook.write(bos);
		    } finally {
		        bos.close();
		    }
		    byte[] bytes = bos.toByteArray();
		    MailAttachment[] mailAttachments = new MailAttachment[1];
		    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "TixcityUnfilledOrderSummary.xlsx");
				
			
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Tixcity UnFilled Order Summary";
			String template = "mail-tixcity-unfilled-order-summary.html";
			Map<String, Object> map = new HashMap<String, Object>();
			String toAddress = "leor@rewardthefan.com,amit.raut@rightthisway.com";
			String bccAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, template, map, mimeType, mailAttachments);
		
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
			
	}
	public static SXSSFWorkbook getAllTixcityUnfilledORderSummaryReport() throws Exception {
		try {
			List<TixcityUnfilledOrderSummary> summaryList = DAORegistry.getTixcityUnfilledOrderSummaryDAO().getAllUnfilldedOrderSummary();
			SXSSFWorkbook workbook = new SXSSFWorkbook();
			SXSSFSheet ssSheet = (SXSSFSheet) workbook.createSheet();
			int j = 0;
			 Row rowhead =   ssSheet.createRow((int)0);
			 rowhead.createCell((int) j).setCellValue("Date");
			 j++;
			 rowhead.createCell((int) j).setCellValue("Total Unfilled Wholesale Price");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Plus/Minus");
		    j++;
		    rowhead.createCell((int) j).setCellValue("Plus/Minus %");
		    j++;
		    
		    CellStyle cellamountStyle = workbook.createCellStyle();
		    cellamountStyle.setDataFormat(
		    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
		    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		    int i =1;
		    for (TixcityUnfilledOrderSummary summaryObj : summaryList) {
			
		    	int column = 0;
		    	Row row = ssSheet.createRow(i);
				row.createCell((int) column).setCellValue(xlDateFormat.format(summaryObj.getSummaryDate()));
		        column++;
		        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(summaryObj.getTotalAmount());
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(summaryObj.getGrossAmount());
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
		        amountCell.setCellValue(summaryObj.getGrossPercentage());
		        amountCell.setCellStyle(cellamountStyle);
		        column++;
		        i++;
			}
		    
		    return workbook;
		    
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static void main(String[] args) {
		
		Date date1 = new Date();
		Date date2 = new Date();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DATE, -7);
		
		date2 = new Date(cal.getTimeInMillis());
		System.out.println("cal : "+cal.getTime() +" : "+date2+" : date1 : "+date1);
		int frequencydays = 7;
		if((date1.getTime()-date2.getTime())<= (1000*60*60*24*frequencydays)) {
			System.out.println("Inside if : "+(date2.getTime()-date1.getTime()));
		} else {
			System.out.println("Inside else : "+(date2.getTime()-date1.getTime()));
		}
	}
}
