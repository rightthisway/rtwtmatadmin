package com.admitone.tmat.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.utils.mail.MailManager;

public class TmatUnCategorizedTickets extends QuartzJobBean implements StatefulJob{
	
	private final Logger logger = LoggerFactory.getLogger(TmatUnCategorizedTickets.class);
	MailManager mailManager = null;
	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}
	
	@Override
 protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		System.out.println("TMAT-UNCATEGORIZED TICKETS FILE SCHEDULER STARTED--------------------------");
		logger.info("TMAT-UNCATEGORIZED TICKETS FILE SCHEDULER STARTED--------------------------");
		List<Event> sportsEventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentType("Sports");
		try {
			unCategorizedTickets(sportsEventList,"Sports");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("sports- done--------------------------");
		
		List<Event> concertsEventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentType("Concerts");
		try {
			unCategorizedTickets(concertsEventList,"Concerts");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("concerts done--------------------------");
		List<Event> theaterEventList = DAORegistry.getEventDAO().getAllActiveEventsWithParentType("Theater");
		try {
			unCategorizedTickets(theaterEventList,"Theater");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("theater done--------------------------");
		
		System.out.println("TMAT-UNCATEGORIZED TICKETS FILE SCHEDULER ENDS--------------------------");
		logger.info("TMAT-UNCATEGORIZED TICKETS FILE SCHEDULER ENDS--------------------------");
		String fromName  = "tmatmanagers@rightthisway.com";
		String mimeType = "text/html";
		String toAddress ="leor@rewardthefan.com,ezippo@rightthisway.com";
		String ccAddress ="kulaganathan@rightthisway.com,araut@rightthisway.com";
		String subject = "UnCategorized tickets for sports,concerts and theater...";
		String template = "mail-uncategorized-tickets.html";
		
		try {
			String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue();
			Integer smtpPort = Integer.valueOf(DAORegistry.getPropertyDAO().get("smtp.port").getValue());
			String smtpUsername = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
			String smtpPassword = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
			String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
			Boolean smtpAuth = Boolean.valueOf(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
			String transportProtocol = "";
			
			SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth,
					smtpUsername, smtpPassword, null, fromName, toAddress, ccAddress, null,subject,template, 
					new HashMap<String, Object>(), mimeType,null);
			System.out.println("UnCategorized tickets mail sent successfully--------------------------");	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block		
			System.out.println("MAIL ERROR--------------------------");
			e.printStackTrace();
		}finally{
		}
		
	}

	public void unCategorizedTickets(List<Event> eventList,String parentType) throws Exception{
		
		try{			
			SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			List<Ticket> finalTickets = new ArrayList<Ticket>();
			StringBuilder sb = new StringBuilder();
			int j=1;
			Pattern regex = Pattern.compile("[$*&+,:;=?|]");
			Matcher matcher;
			
			Date now = new Date();
			now = dbDateFormat.parse(dbDateFormat.format(now));
			
			int eventSize =eventList.size();
			
			for (Event event : eventList) {
				Date date = null;
				if(event.getDate()!=null){
					date = dbDateFormat.parse(dbDateFormat.format(event.getDate()));	
				}
				
				if(date!=null && ((date.getTime()-now.getTime()) <= 3 * 24 * 60 * 60 * 1000)) {
					continue;
				}
				
				String venueCategory ="";
				
				if(null != event.getVenueCategory()){
					venueCategory = event.getVenueCategory().getCategoryGroup();
				}
				
				Collection<Ticket> tempTicketList = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
				Collection<Ticket> ticketList=new ArrayList<Ticket>();
				if(null != tempTicketList && !tempTicketList.isEmpty()) {
					
					for (Ticket ticket : tempTicketList) {
						
						if(ticket.getNormalizedSection().contains("-")) {
							continue;
						}
						
						if(ticket.getNormalizedSection().toLowerCase().contains(" or ")) {
							continue;
						}
						matcher = regex.matcher(ticket.getNormalizedSection());
						if(matcher.find()) {
							
							continue;
						}
						
						if(ticket.getNormalizedRow().contains("-")) {
							continue;
						}
						
						if(ticket.getNormalizedRow().toLowerCase().contains(" or ")) {
							continue;
						}
						matcher = regex.matcher(ticket.getNormalizedRow());
						if(matcher.find()) {
							
							continue;
						}
						if( null != ticket.getNormalizedRow() && (ticket.getNormalizedRow().toUpperCase().contains("TBD") || ticket.getNormalizedRow().toUpperCase().contains("WC") || ticket.getNormalizedRow().toUpperCase().contains("ADA") ||
								ticket.getNormalizedRow().toLowerCase().contains("person") ||ticket.getNormalizedRow().toLowerCase().contains("passenger") ||
								ticket.getNormalizedRow().toLowerCase().contains("greet") || ticket.getNormalizedRow().toLowerCase().contains("access"))) {
							continue;
						}
						if(null != ticket.getNormalizedRow() && (ticket.getNormalizedRow().toLowerCase().contains("reflection") ||
								ticket.getNormalizedRow().toLowerCase().contains("couch") ||ticket.getNormalizedRow().toLowerCase().contains("includes") ||
								ticket.getNormalizedRow().toLowerCase().contains("lounge") || ticket.getNormalizedRow().toLowerCase().contains("piggyback"))) {
							continue;
						}
						if(null != ticket.getNormalizedRow() && (ticket.getNormalizedRow().equalsIgnoreCase("zz top") || ticket.getNormalizedRow().equalsIgnoreCase("wristband") ||
								ticket.getNormalizedRow().equalsIgnoreCase("wristban") || ticket.getNormalizedRow().equalsIgnoreCase("wheelchair") ||
								ticket.getNormalizedRow().equalsIgnoreCase("gold") || ticket.getNormalizedRow().equalsIgnoreCase("__/__"))) {
							continue;
						}
						
						if(ticket.getNormalizedSection().toLowerCase().contains("suite") ||ticket.getNormalizedSection().toLowerCase().contains("ste") ||
								ticket.getNormalizedSection().toLowerCase().contains("wheelchair") ||  ticket.getNormalizedSection().toLowerCase().contains("park") ||
								ticket.getNormalizedSection().toLowerCase().contains("parking") || ticket.getNormalizedSection().toLowerCase().contains("lot") ||
								ticket.getNormalizedSection().toLowerCase().contains("lots") || ticket.getNormalizedSection().toUpperCase().contains("WC") ||
								ticket.getNormalizedSection().toLowerCase().contains("vouch") ) {
							continue;
						}
						
						if(ticket.getNormalizedSection().contains("VIP Tent") ||ticket.getNormalizedSection().contains("VIP Private") ||
								ticket.getNormalizedSection().contains("VIP Party Deck") ||  ticket.getNormalizedSection().toLowerCase().contains("diamond") ||
								ticket.getNormalizedSection().toLowerCase().contains("package") || ticket.getNormalizedSection().toLowerCase().contains("beer garden") ||
								ticket.getNormalizedSection().toLowerCase().contains("audi") || ticket.getNormalizedSection().toLowerCase().contains("beer garden") ||
								ticket.getNormalizedSection().toLowerCase().contains("blue") || ticket.getNormalizedSection().toLowerCase().contains("bronze") ) {
							continue;
						}
						
						if(ticket.getNormalizedSection().toLowerCase().contains("luxury") ||
								ticket.getNormalizedSection().toLowerCase().contains("deluxe") || ticket.getNormalizedSection().toLowerCase().contains("hotel") ||
								ticket.getNormalizedSection().toLowerCase().contains("lounge") || ticket.getNormalizedSection().toLowerCase().contains("package") ||
								ticket.getNormalizedSection().toLowerCase().contains("private") || ticket.getNormalizedSection().toLowerCase().contains("valet") ) {
							continue;
						}
						if(ticket.getNormalizedSection().equalsIgnoreCase("__/or__") || ticket.getNormalizedSection().equalsIgnoreCase("meet and greet") ||
								ticket.getNormalizedSection().equalsIgnoreCase("meet greet") ) {
							continue;
						}
						if(ticket.getNormalizedSection().toLowerCase().contains(" am") ||
								ticket.getNormalizedSection().toLowerCase().contains(" pm") || ticket.getNormalizedSection().toLowerCase().contains("am ") ||
								ticket.getNormalizedSection().toLowerCase().contains("1130am") || ticket.getNormalizedSection().toLowerCase().contains("12pm") ||
								ticket.getNormalizedSection().toLowerCase().contains("am 1130") || ticket.getNormalizedSection().toLowerCase().contains("pm 1200")) {
							continue;
						}
						ticket.setVenueCategoryGroup(venueCategory);
						ticketList.add(ticket);
					}	
				}
				
				Collection<Category> categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
				
				ticketList = TicketUtil.removeDuplicateTickets(ticketList);
				
				ticketList = TicketUtil.uncategorizedTickets(ticketList, event, categoryList);
				
				finalTickets.addAll(ticketList);
				
				System.out.println(parentType+"..Total Events.."+eventSize+"................Completed Events....."+j+"......"+finalTickets.size());
				j++;
			}

			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("hh:mm aa");
			
			Date curDate = new Date();
			
			String dateStr = df.format(curDate);
			dateStr =dateStr.replaceAll("/", "-");
			
			System.out.println("TMAT-UNCATEGORIZED TICKETS FILE WRITING BEGINS--------------------------"+parentType);
			logger.info("TMAT-UNCATEGORIZED TICKETS FILE WRITING BEGINS--------------------------"+parentType);
			
			//String fileLocation ="\\\\192.168.0.152\\shared\\ECommerce\\uncategorized\\";
			String fileLocation ="C:\\external files\\UncategorizedTickets\\";
			
			if(parentType.equalsIgnoreCase("Sports")){
				fileLocation = fileLocation+"SportsUncategorizedTickets-"+dateStr+".csv";
				
			}else if(parentType.equalsIgnoreCase("Concerts")){
				fileLocation = fileLocation+"ConcertsUncategorizedTickets-"+dateStr+".csv";
			}else{
				fileLocation = fileLocation+"TheaterUncategorizedTickets-"+dateStr+".csv";
			}
			
			
			 FileOutputStream fop = null;
			 File file1= new File(fileLocation);
			 fop = new FileOutputStream(file1);
			 fop.write("EventName,EventDate,EventTime,Venue,VenueCategory,Section,Row , Seat,Qty \n".getBytes());
			 for (Ticket ticket : finalTickets) {
				String csvData = ticket.getEvent().getName().replaceAll(",", "-")
						   +"," + ((null != ticket.getEvent().getLocalDate())?df.format(ticket.getEvent().getLocalDate()):"TBD")
						   + "," + ((null != ticket.getEvent().getLocalTime())?tf.format(ticket.getEvent().getLocalTime()):"TBD")
						   + "," + ticket.getEvent().getVenue().getBuilding().replaceAll(",", "-")
						   +","+ ticket.getVenueCategoryGroup().replaceAll(",", "-")
						   + "," + (null != ticket.getSection()?ticket.getSection().replaceAll(",", "-"):"")
						   + "," + ((null != ticket.getRow())?ticket.getRow():"")
						   + "," + ((null != ticket.getSeat())?ticket.getSeat():"")+","+ticket.getQuantity()
						   +" \n";
				fop.write(csvData.getBytes());
			 }
			 fop.flush();
			 fop.close();
			 System.out.println("TMAT-UNCATEGORIZED TICKETS FILE WRITING ENDS--------------------------"+parentType);
			logger.info("TMAT-UNCATEGORIZED TICKETS FILE WRITING ENDS--------------------------"+parentType);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception{
		String fileLocation ="";
		
		
		fileLocation ="\\\\192.168.0.152\\shared\\ECommerce\\uncategorized\\SportsUncategorizedTickets-07-02-2015.csv";
		 FileOutputStream fop = null;
		 File file1= new File(fileLocation);
		 fop = new FileOutputStream(file1);
		 fop.write("EventName,EventDate,EventTime,Venue,VenueCategory,Section,Row , Seat,Qty \n".getBytes());
		 fop.flush();
		 fop.close();
		
	}

}
