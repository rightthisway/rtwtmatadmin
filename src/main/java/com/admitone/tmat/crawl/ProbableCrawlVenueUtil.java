package com.admitone.tmat.crawl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ProbableVenue;


public class ProbableCrawlVenueUtil {
	
	public Map<Integer, List<ProbableVenue>> venueSynonymMap = new HashMap<Integer, List<ProbableVenue>>();
	public List<Integer> venueIds = new ArrayList<Integer>();
	
	
	public List<ProbableVenue> getVenueSynonyms(Integer venueId){
		List<ProbableVenue> list = venueSynonymMap.get(venueId);
		if(null == list){
			list = (List<ProbableVenue>) DAORegistry.getProbableVenueDAO().getSynonymByVenueId(venueId);
			if(list != null){
				venueSynonymMap.put(venueId, list);
			}
		}
		return list;
	}
}
