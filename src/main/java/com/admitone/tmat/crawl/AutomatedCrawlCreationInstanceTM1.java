package com.admitone.tmat.crawl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventHitAudit;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingManager;
import com.admitone.tmat.eventfetcher.EventListingResult;
import com.admitone.tmat.utils.Connections;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.mail.MailManager;

// Ulaganathan : Automated Crawl Creation Process For TMAT Events.

public class AutomatedCrawlCreationInstanceTM1 implements InitializingBean{
	
	private CreationEventListManager creationEventListManager;
	private EventListingManager eventListingManager;
	private SharedProperty sharedProperty;
	static MailManager mailManager = null;
	static Date lastRunTime = null;
	static Integer matchingCrawlCount = 0;
	static Integer probableCrawlCount = 0;
	static Integer newMatchingCrawlCount = 0;
	static Collection<String> siteList=null;
	static List<EventHit> additionalEventDataList = null;
	static boolean firstTime = false;
	
	static{
		siteList = new ArrayList<String>();
		//siteList.add(Site.VIVIDSEAT);
		//siteList.add(Site.TICKETS_NOW);
		//siteList.add(Site.TICKET_EVOLUTION);
		//siteList.add(Site.STUB_HUB);
		siteList.add(Site.TICKET_MASTER);
	}
	public EventListingManager getEventListingManager() {
		return eventListingManager;
	}

	public void setEventListingManager(EventListingManager eventListingManager) {
		this.eventListingManager = eventListingManager;
	}
	 
	public CreationEventListManager getcreationEventListManager() {
		return creationEventListManager;
	}

	public void setcreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		if (creationEventListManager == null) {
			throw new RuntimeException("Parameter creationEventListManager must be set");
		}

		if (eventListingManager == null) {
			throw new RuntimeException("Parameter eventListingManager must be set");
		}
		
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						createCrawls();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 300); //Every 15mins
	}
	
	public static void main(String[] args) {
		/*
		07/06/2018 18:30-mercedes-benz superdome: new orleans: la
		03/03/2018 20:00-palace theater - ct: waterbury: ct
		04/28/2018 18:05-at&t park: san francisco: ca
		03/02/2018 20:00-john m. belk arena: davidson: nc
		06/12/2018 19:00-eugene o'neill theatre: new york: ny
		*/
		 String searchKey = "03/02/2018 20:00-john m. belk arena: davidson: nc";
		
		 
		 searchKey = searchKey.replaceAll("\\W", "");
		 
		 System.out.println(searchKey);
		 
		
	}
	
	
	
/**
* if(event.getVenueId() > 12000) {
				continue;
			}
select sum(case when ID <= 12000 then 1 else 0 end) as range1,
sum(case when ID > 12000 and ID <= 15000 then 1 else 0 end) as range2,
sum(case when ID > 15000 and ID <= 18000 then 1 else 0 end) as range3,
sum(case when ID > 18000 and ID <= 21000 then 1 else 0 end) as range4,
sum(case when ID > 21000 and ID <= 24000 then 1 else 0 end) as range5
from venue

**/	
	
	public void createCrawls() throws Exception{
		System.out.println(" *********** AutoCrawler for  TM1 :  " );
		matchingCrawlCount = 0;
		probableCrawlCount = 0;
		newMatchingCrawlCount = 0;
		additionalEventDataList=  new ArrayList<EventHit>();
		
		System.out.println("ACCI_6 : AUTOMATED CRAWL CREATION INSTANCE1: Job Started....."+new Date());
		
		long startTime = new Date().getTime();
		
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.DAY_OF_MONTH, -1);
		
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByEventCreationDates(null, null, EventStatus.ACTIVE);
		List<Integer> eventIds = DAORegistry.getQueryManagerDAO().AllEventsWhichHasTMCrawls(); // Added Ticket MASTER CONDITION  
		
		Map<Integer, List<Event>> venueEventMap = new HashMap<Integer, List<Event>>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		int i=0,size =events.size();
		for (Event event : events) {
			if(eventIds.contains(event.getId()) ||  null == event.getLocalDate()){
				continue;
			}
			
			if(event.getVenueId() > 12000) {
				System.out.println("Skipping TM1 : Venue Id not in Range "  + event.getVenueId());
				continue;
			}
			i++;
			List<Event> eventList = venueEventMap.get(event.getVenueId());
			if(null != eventList && eventList.size() > 0 ){
				venueEventMap.get(event.getVenueId()).add(event);
				continue;
			}
			eventList = new ArrayList<Event>();
			eventList.add(event);
			venueEventMap.put(event.getVenueId(),eventList);
			eventMap.put(event.getId(), event);
		}
		
		String[] siteIds = null;
		Collection<String> toBeCrawlSiteList;
		
		Integer runningEvent = 0, totalEvents = eventMap.size();
		
		for (Integer venueId : venueEventMap.keySet()) {
			List<Event> eventList = venueEventMap.get(venueId);
			
			for (Event event : eventList) {
				
				
				Integer eventId = event.getId();
				
				
				
/**
select sum(case when ID <= 12000 then 1 else 0 end) as range1,
       sum(case when ID > 12000 and ID <= 15000 then 1 else 0 end) as range2,
       sum(case when ID > 15000 and ID <= 18000 then 1 else 0 end) as range3,
	   sum(case when ID > 18000 and ID <= 21000 then 1 else 0 end) as range4,
	   sum(case when ID > 21000 and ID <= 24000 then 1 else 0 end) as range5
from venue

	**/			
				
				
				
				
				
				System.out.println("ACCI_6 : EVENT ID: "+eventId+"======================= "+new Date());
				toBeCrawlSiteList= new ArrayList<String>(siteList);
				List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(event.getId());
				for (TicketListingCrawl crawlObj : crawls) {
					if(toBeCrawlSiteList.contains(crawlObj.getSiteId())){
						toBeCrawlSiteList.remove(crawlObj.getSiteId());
					}
				}
				if(null == toBeCrawlSiteList || toBeCrawlSiteList.isEmpty()){
					continue;
				}
				siteIds = toBeCrawlSiteList.toArray(new String[1]);
				List<Event> eventListNew = new ArrayList<Event>();
				eventListNew.add(event);
				System.out.println("ACCI_6 : Real Search Begins SITEIID :"+toBeCrawlSiteList+", Time :"+new Date());
				createCrawls(event, eventListNew, siteIds,crawls);
				System.out.println("ACCI_6 : Real Search Begins SITEIID :"+toBeCrawlSiteList+", Time :"+new Date());
				runningEvent++;
				System.out.println("ACCI_6: TotalEvents :"+totalEvents+", Running Eevnt: "+runningEvent+", MatchingCrawlCount: "+matchingCrawlCount+", NewMatchingCrawlCount: "+newMatchingCrawlCount+", ProbableCrawlCount: "+probableCrawlCount+", SiteIds: "+siteIds);
			}
			
		}
		
		long lastCompleteTime = new Date().getTime()-(startTime);
		System.out.println("Time : "+lastCompleteTime);
		firstTime=false;
		System.out.println("ACCI_6 :  AUTOMATED CRAWL CREATION INSTANCE1 : Job End....."+new Date());
	}	
	
	
	public void createCrawls(Event tevent,
			List<Event> eventList,String[] siteIds,List<TicketListingCrawl> crawls){
		System.out.println("ACCI_6 :   createCrawls  ...1  "); 
			String artistName = tevent.getArtist().getName();
			Date fromDate = tevent.getLocalDate();
			Date toDate = tevent.getLocalDate();
			long crrawlStartTime = new Date().getTime();
			Collection<EventListingResult> eventListingResults = eventListingManager.search(artistName, siteIds, null, fromDate, toDate,eventList,false,false);
			long crawlCompleteTime = new Date().getTime()-(crrawlStartTime);
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
			List<EventHit> additionalEvents = new ArrayList<EventHit>();
			for(EventListingResult result: eventListingResults) {
				if (result.getEventHits() != null) {
					for(EventHit eventhit:result.getEventHits()){
						
						if(TextUtil.isNotValidCrawl(eventhit.getName())){
							System.out.println("ACCI_6 :  isNotValidCrawl  ");
							continue;
						}
						
						if(eventhit.getBestMatchEventId()!=null){
							System.out.println("ACCI_6 :  getBestMatchEventId()!=null  ");
							eventHits.add(eventhit);
						}else{
							if(CrawlCreationUtils.findBestMatchEvent(tevent, eventhit,false)){
								eventhit.setBestMatchEventId(tevent.getId());
								newMatchingCrawlCount++;
								eventHits.add(eventhit);
								System.out.println("ACCI_6 :  findBestMatchEvent  ");
							}else{
								probableCrawlCount++;
								additionalEvents.add(eventhit);
								System.out.println("ACCI_6 :  probableCrawlCount++  ");
							}
						}
					}
				}
			}
			
			if(additionalEvents.size()>0 && firstTime){
				System.out.println("ACCI_6 :  additionalEvents.size()>0 && firstTime  ");
				try{
					List<EventHitAudit> toBeSavedList = new ArrayList<EventHitAudit>();
					String key =tevent.getFormatedDate().replaceAll("EST", "").replaceAll("EDT", "").trim() + "-" + tevent.getVenue().getBuilding().toLowerCase().trim()+ ", " + tevent.getVenue().getCity().toLowerCase().trim() + ", " + tevent.getVenue().getState().toLowerCase().trim();
					for (EventHit eventHit : additionalEvents) {
						EventHitAudit audit = new EventHitAudit(eventHit);
						audit.setEventId(tevent.getId());
						audit.setArtistId(tevent.getArtistId());
						audit.setVenueId(tevent.getVenueId());
						audit.setSearchKey(key);
						audit.setModified_date(new Date());
						audit.setSearchType("EVENT");
						toBeSavedList.add(audit);
					}
				
					DAORegistry.getEventHitAuditDAO().saveAll(toBeSavedList);
					System.out.println("ACCI_6 :  DAORegistry.getEventHitAuditDAO().saveAll  " + toBeSavedList.size() );
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			String crawlerId="";
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls=";
			for (EventHit obj : eventHits) {
				if(obj.getBestMatchEventId().equals(tevent.getId())){
					System.out.println("ACCI_6 : obj.getBestMatchEventId().equals(tevent.getId())  ");
					TicketListingCrawl ticketListingCrawl = new TicketListingCrawl(obj, tevent, "AUTO");
					boolean flag=true;
					for(TicketListingCrawl crawl:crawls){
						if(crawl.isEquivalent(ticketListingCrawl)){
							flag=false;
						}
					}
					if(flag){  
						
						System.out.println("ACCI_6 :if(flag)   288  ");
						if(obj.getId().contains(Site.AOP)){
							Map<String, String> map =new HashMap<String, String>();
							ticketListingCrawl.setExtraParameterMap(map);
						}
						DAORegistry.getTicketListingCrawlDAO().save(ticketListingCrawl);
						System.out.println("****** SAVED Crawl ID: "+ticketListingCrawl.getId());
						crawlerId+=ticketListingCrawl.getId()+",";
						url+=ticketListingCrawl.getId()+",";
						creationEventListManager.addSitesInSitesPerEventMap(tevent.getId(), ticketListingCrawl.getSiteId());
						matchingCrawlCount++;
					}
				}
			}
			
			if(crawlerId != null && !crawlerId.isEmpty()){
				try {
					System.out.println("ACCI_6: AUTOMATED CRAWL CREATION TMAT CRL START :"+url+" : "+new Date()+" : "+"AUTO");
					url=url.substring(0,url.length()-1);
					SimpleHttpClient client = HttpClientStore.createHttpClient();
					HttpGet get = new HttpGet(url);
					System.out.println("ACCI_6: AUTOMATED CRAWL CREATION TMAT CRL EXE: "+client.executeWithOneHit(get,true));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			 
		}
		 
	public static Comparator<Event> eventComparatorFordate = new Comparator<Event>() {

		public int compare(Event event1, Event event2) {
			try{
				int cmp =event1.getLocalDate().compareTo(
						event1.getLocalDate());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				if (cmp == 0) {
					return 1;
				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				return event1.getId().compareTo(event2.getId());
		    }};
		    	    
		    
	
}
