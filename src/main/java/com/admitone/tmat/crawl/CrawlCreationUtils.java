package com.admitone.tmat.crawl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ProbableVenue;
import com.admitone.tmat.data.VenueSynonym;
import com.admitone.tmat.eventfetcher.EventHit;


public class CrawlCreationUtils {
	
	protected static DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
	public static void main(String[] args) throws Exception {
		Date tmatDate = dateTimeFormat.parse("02/27/2018 11:30");
		Date exDate = dateTimeFormat.parse("02/27/2018 12:30");
		
		Calendar tmatCalAdd = Calendar.getInstance();
		tmatCalAdd.setTime(tmatDate);
		
		Calendar tmatCalSub = Calendar.getInstance();
		tmatCalSub.setTime(tmatDate);
		
		tmatCalAdd.add(Calendar.MINUTE, 60);
		tmatCalSub.add(Calendar.MINUTE, -60);
		
		System.out.println(tmatDate+"=========="+exDate);
		
		if((exDate.after(tmatCalSub.getTime()) || exDate.equals(tmatCalSub.getTime())) && 
				(exDate.before(tmatCalAdd.getTime()) || exDate.equals(tmatCalAdd.getTime()))){
			System.out.println("Fall Between "+tmatCalSub.getTime()+" and "+tmatCalAdd.getTime());
		}else{
			System.out.println("Nope  "+tmatCalSub.getTime()+" and "+tmatCalAdd.getTime());
		}
		
	}
	
	public static boolean findBestMatchEvent(Event tmatEvent, EventHit eventHit, boolean isVenueSearch){

		String exKey = eventHit.getLocation().toLowerCase().replaceAll(" the", "-").replaceAll("the ", "-").trim();
		exKey = exKey.replaceAll("\\W", "");
		
		
		String tmatKey = tmatEvent.getVenue().getBuilding().toLowerCase().trim()+ ", " + tmatEvent.getVenue().getCity().toLowerCase().trim() + ", " + tmatEvent.getVenue().getState().toLowerCase().trim();
		tmatKey = tmatKey.replaceAll(" the", "-").replaceAll("the ", "-").replaceAll("\\W", "");
		
		if(!tmatKey.equals(exKey)){
			/*if(isVenueSearch){
				String eventHitLocation = eventHit.getLocation();
				
				List<ProbableVenue> probableVenues = DAORegistry.getProbableVenueDAO().getSynonymByVenueId(tmatEvent.getVenueId());
				
				ProbableVenue probableVenueObj = null;
				if(null != probableVenues && !probableVenues.isEmpty() ){
					String tmatLocationKey = tmatEvent.getVenue().getBuilding().toLowerCase().trim()+ ", " + tmatEvent.getVenue().getCity().toLowerCase().trim() + ", " + tmatEvent.getVenue().getState().toLowerCase().trim();
					boolean isFound = false;
					for (ProbableVenue obj : probableVenues) {
						if(obj.getProbleName().toLowerCase().equals(eventHitLocation.toLowerCase())){
							isFound = true;
							break;
						}
					}
					if(!isFound){
						probableVenueObj = new ProbableVenue();
						probableVenueObj.setProbleName(eventHitLocation);
						probableVenueObj.setSiteId(eventHit.getSiteId());
						probableVenueObj.setMarkedAsSynonym(false);
						probableVenueObj.setCreatedDate(new Date());
						probableVenueObj.setUpdatedDate(new Date());
						probableVenueObj.setVenueId(tmatEvent.getVenueId());
					}
				}else{
					probableVenueObj = new ProbableVenue();
					probableVenueObj.setProbleName(eventHitLocation);
					probableVenueObj.setSiteId(eventHit.getSiteId());
					probableVenueObj.setMarkedAsSynonym(false);
					probableVenueObj.setCreatedDate(new Date());
					probableVenueObj.setUpdatedDate(new Date());
					probableVenueObj.setVenueId(tmatEvent.getVenueId());
				}
			}*/
			try{
				List<ProbableVenue> probableVenues = DAORegistry.getProbableVenueDAO().getAllAddedVenueSynonymsByVenue(Boolean.TRUE,tmatEvent.getVenueId());
				if(probableVenues == null || probableVenues.isEmpty() || probableVenues.size() <= 0){
					return false;
				}
				boolean noMatchFound = true;
				for (ProbableVenue obj : probableVenues) {
					tmatKey = obj.getProbleName().toLowerCase().replaceAll(" the", "-").replaceAll("the ", "-").replaceAll("\\W", "");
					if(tmatKey.equalsIgnoreCase(exKey)){
						noMatchFound = false;
						break;
					} 
				}
				if(noMatchFound){
					return false;
				}
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}
		 
		String tmatEventDateStr = tmatEvent.getFormatedDate().replaceAll("EST", "").replaceAll("EDT", "").trim();
		String exEventDateStr = eventHit.getDateAndTime().trim();
		
		if(tmatEventDateStr.equals(exEventDateStr)){
			return true;
		}
		
		if(tmatEventDateStr.contains("TBD") || exEventDateStr.contains("TBD")){
			return false;
		}
		try{
			String parentCategory = tmatEvent.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName();
			Date tmatDate = dateTimeFormat.parse(tmatEventDateStr);
			Date exDate = dateTimeFormat.parse(exEventDateStr);
			
			Calendar tmatCalAdd = Calendar.getInstance();
			tmatCalAdd.setTime(tmatDate);
			
			Calendar tmatCalSub = Calendar.getInstance();
			tmatCalSub.setTime(tmatDate);
			
			if(parentCategory.equals("Sports")){
				tmatCalAdd.add(Calendar.MINUTE, 60);
				tmatCalSub.add(Calendar.MINUTE, -60);
			}else{
				tmatCalAdd.add(Calendar.MINUTE, 45);
				tmatCalSub.add(Calendar.MINUTE, -45);
			}
			
			if((exDate.after(tmatCalSub.getTime()) || exDate.equals(tmatCalSub.getTime())) && 
					(exDate.before(tmatCalAdd.getTime()) || exDate.equals(tmatCalAdd.getTime()))){
				return true;
			}
			
		}catch (Exception e) {
			return false;
		}
		return false;
	}
	
public static boolean findMismatchingExchangeVenue(List<Event> events, EventHit eventHit){
		Event tmatEvent = events.get(0);
		String tmatVenueName = tmatEvent.getVenue().getBuilding();
		String exVenueName = eventHit.getLocation();
		String tmatKey = tmatVenueName.toLowerCase().trim()+ ", " + tmatEvent.getVenue().getCity().toLowerCase().trim() + ", " + tmatEvent.getVenue().getState().toLowerCase().trim();
		String exKey = exVenueName.toLowerCase().trim();
		
		if(!tmatKey.equals(exKey)){
			if(exVenueName.contains(tmatVenueName.toLowerCase()) 
					|| (exKey.contains(tmatEvent.getVenue().getCity().toLowerCase()) && 
					exKey.contains(tmatEvent.getVenue().getState().toLowerCase())) ){
				
				
				String eventHitLocation = eventHit.getLocation();
				List<ProbableVenue> probableVenues = DAORegistry.getProbableVenueDAO().getSynonymByVenueId(tmatEvent.getVenueId());
				ProbableVenue probableVenueObj = null;
				if(null != probableVenues && !probableVenues.isEmpty() ){
					boolean isFound = false;
					for (ProbableVenue obj : probableVenues) {
						if(obj.getProbleName().toLowerCase().equals(eventHitLocation.toLowerCase())){
							isFound = true;
							break;
						}
					}
					if(!isFound){
						probableVenueObj = new ProbableVenue();
						probableVenueObj.setProbleName(eventHitLocation);
						probableVenueObj.setSiteId(eventHit.getSiteId());
						probableVenueObj.setMarkedAsSynonym(false);
						probableVenueObj.setCreatedDate(new Date());
						probableVenueObj.setUpdatedDate(new Date());
						probableVenueObj.setVenueId(tmatEvent.getVenueId());
					}
				}else{
					probableVenueObj = new ProbableVenue();
					probableVenueObj.setProbleName(eventHitLocation);
					probableVenueObj.setSiteId(eventHit.getSiteId());
					probableVenueObj.setMarkedAsSynonym(false);
					probableVenueObj.setCreatedDate(new Date());
					probableVenueObj.setUpdatedDate(new Date());
					probableVenueObj.setVenueId(tmatEvent.getVenueId());
				}
				
				if(null != probableVenueObj){
					DAORegistry.getProbableVenueDAO().saveOrUpdate(probableVenueObj);
				}
			}
			 
		}
		return false;
	}

}
