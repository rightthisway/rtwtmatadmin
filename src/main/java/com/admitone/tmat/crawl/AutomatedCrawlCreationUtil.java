package com.admitone.tmat.crawl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingManager;
import com.admitone.tmat.eventfetcher.EventListingResult;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.mail.MailManager;
import com.admitone.tmat.web.Constants;

// Ulaganathan : Automated Crawl Creation Process For TMAT Events.

public class AutomatedCrawlCreationUtil implements InitializingBean{
	
	private CreationEventListManager creationEventListManager;
	private EventListingManager eventListingManager;
	private SharedProperty sharedProperty;
	static MailManager mailManager = null;
	static Date lastRunTime = null;
	static Integer matchingCrawlCount = 0;
	static Integer probableCrawlCount = 0;
	static Integer newMatchingCrawlCount = 0;
	
	public EventListingManager getEventListingManager() {
		return eventListingManager;
	}

	public void setEventListingManager(EventListingManager eventListingManager) {
		this.eventListingManager = eventListingManager;
	}
	 
	public CreationEventListManager getcreationEventListManager() {
		return creationEventListManager;
	}

	public void setcreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		if (creationEventListManager == null) {
			throw new RuntimeException("Parameter creationEventListManager must be set");
		}

		if (eventListingManager == null) {
			throw new RuntimeException("Parameter eventListingManager must be set");
		}
		
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		/*calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);*/
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						createCrawls();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 15); //Every 15mins
	}
	
	public static void main(String[] args) {
		Date endDate = new Date();
		Date startDate = null;
		Calendar apiCal= Calendar.getInstance();
		apiCal.add(Calendar.DAY_OF_MONTH, -1);
		startDate = apiCal.getTime();
		
		System.out.println(startDate+"====="+endDate);
	}
	
	public void createCrawls(){
		
		System.out.println("ACCU1 : AUTOMATED CRAWL CREATION : Job Started....."+new Date());
		
		long startTime = new Date().getTime();
		
		Date endDate = new Date();
		Date startDate = null;
		/*if(lastRunTime == null){
			Calendar apiCal= Calendar.getInstance();
			apiCal.add(Calendar.MINUTE, -1200);
			//apiCal.add(Calendar.DAY_OF_MONTH, -1);
			startDate = apiCal.getTime();
		}else{
			startDate = lastRunTime;
		}*/
		
		Calendar apiCal= Calendar.getInstance();
		apiCal.add(Calendar.MINUTE, -60);
		//apiCal.add(Calendar.DAY_OF_MONTH, -1);
		startDate = apiCal.getTime();
		
		matchingCrawlCount = 0;
		probableCrawlCount = 0;
		newMatchingCrawlCount = 0;
		
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByEventCreationDates(startDate, endDate, EventStatus.ACTIVE);
		List<Integer> eventIds = DAORegistry.getQueryManagerDAO().AllEventsWhichHasSH_TE_TS_VDCrawls();
		
		Map<Integer, List<Event>> artistEventMap = new HashMap<Integer, List<Event>>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		for (Event event : events) {
			if(eventIds.contains(event.getId()) || null == event.getLocalDate()){
				continue;
			}
			List<Event> eventList = artistEventMap.get(event.getArtistId());
			if(null != eventList && eventList.size() > 0 ){
				artistEventMap.get(event.getArtistId()).add(event);
				continue;
			}
			eventList = new ArrayList<Event>();
			eventList.add(event);
			artistEventMap.put(event.getArtistId(),eventList);
			eventMap.put(event.getId(), event);
		}
		
		String[] siteIds = null;
		Collection<String> siteList = new ArrayList<String>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			if(siteId.equals(Site.VIVIDSEAT) || siteId.equals(Site.TICKET_EVOLUTION)
					|| siteId.equals(Site.STUB_HUB) ){
				siteList.add(siteId);
			}
		}
		siteIds = siteList.toArray(new String[1]); 
		
		int k=1,artistSize = artistEventMap.size();
		for (Integer artistId : artistEventMap.keySet()) {
				System.out.println("ACCU1 : ARTIST ID: "+artistId+"======================= Search Begins :"+new Date());
				List<Event> eventList = artistEventMap.get(artistId);
				Integer eventSize = eventList.size();
				System.out.println("ACCU1 : : Tot : "+artistSize+", Current : "+k+", Artist ID : "+artistId+", Event Size: "+eventSize);
				Collections.sort(eventList,AutomatedCrawlCreationUtil.eventComparatorFordate);
				try{
					System.out.println("ACCU1 : Real Search Begins  Time :"+new Date());
					createCrawls(artistEventMap, eventMap, eventList, siteIds, null);
					System.out.println("ACCU1 : ARTIST ID: "+artistId+"======================= Search Ends :"+new Date());
				}catch(Exception e){
					e.printStackTrace();
				}
				k++;
				System.out.println("ACCU1 : TotalArtists :"+artistSize+", Running ARTIST: "+k+", MatchingCrawlCount: "+matchingCrawlCount+", NewMatchingCrawlCount: "+newMatchingCrawlCount+",   ProbableCrawlCount: "+probableCrawlCount+", SiteIds: "+siteIds);
		}
		long lastCompleteTime = new Date().getTime()-(startTime);
		System.out.println("Time : "+lastCompleteTime);
		lastRunTime = endDate;
		System.out.println("ACCU1 :  AUTOMATED CRAWL CREATION : Job End....."+new Date());
	}
	
	
	public void createCrawls(Map<Integer, List<Event>> artistEventMap,Map<Integer,Event> eventMap,
				List<Event> eventList,String[] siteIds,Map<String,Integer> crawlsUrlwithEventap ){
				String artistName = eventList.get(0).getArtist().getName();
				Date fromDate = eventList.get(0).getLocalDate();
				Date toDate = eventList.get(eventList.size()-1).getLocalDate();
				//long crrawlStartTime = new Date().getTime();
				Collection<EventListingResult> eventListingResults = eventListingManager.search(artistName, siteIds, null, fromDate, toDate,eventList,false,false);
				//long crawlCompleteTime = new Date().getTime()-(crrawlStartTime);
				//System.out.println("ACCU1 : CrawlCompleteTime======>"+crawlCompleteTime);
				//System.out.println("ACC: EVENT RESULTS LEN=" + eventListingResults.size()+", ARTISTNAME="+artistName);
				Collection<EventHit> eventHits = new ArrayList<EventHit>();
				for(EventListingResult result: eventListingResults) {
					if (result.getEventHits() != null) {
						for(EventHit eventhit:result.getEventHits()){
							if(TextUtil.isNotValidCrawl(eventhit.getName())){
								continue;
							}
							
							if(eventhit.getBestMatchEventId()!=null){
								eventHits.add(eventhit);
							}else{
								boolean isMatchingFound = false;
								for (Event eventObj : eventList) {
									if(CrawlCreationUtils.findBestMatchEvent(eventObj, eventhit,false)){
										isMatchingFound = true;
										eventhit.setBestMatchEventId(eventObj.getId());
										break;
									}
								}
								if(isMatchingFound){
									newMatchingCrawlCount++;
									eventHits.add(eventhit);
								}else{
									probableCrawlCount++;
								}
							}
						}
					}
				}
				
				String crawlerId="";
				String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls=";
				for (EventHit obj : eventHits) {
					Event tevent= eventMap.get(obj.getBestMatchEventId());
					if(null != tevent){
						TicketListingCrawl ticketListingCrawl = new TicketListingCrawl(obj, tevent, "AUTO");
						boolean flag=true;
						List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(tevent.getId());
						for(TicketListingCrawl crawl:crawls){
							if(crawl.isEquivalent(ticketListingCrawl)){
								flag=false;
							}
						}
						if(flag){  
							if(obj.getId().contains(Site.AOP)){
								Map<String, String> map =new HashMap<String, String>();
								ticketListingCrawl.setExtraParameterMap(map);
							}
							DAORegistry.getTicketListingCrawlDAO().save(ticketListingCrawl);
							crawlerId+=ticketListingCrawl.getId()+",";
							url+=ticketListingCrawl.getId()+",";
							creationEventListManager.addSitesInSitesPerEventMap(tevent.getId(), ticketListingCrawl.getSiteId());
							matchingCrawlCount++;
						}
					}
				}
				
				if(crawlerId != null && !crawlerId.isEmpty()){
					try {
						System.out.println("ACCU1 : AUTOMATED CRAWL CREATION TMAT CRL START :"+url+" : "+new Date()+" : "+"AUTO");
						url=url.substring(0,url.length()-1);
						SimpleHttpClient client = HttpClientStore.createHttpClient();
						HttpGet get = new HttpGet(url);
						System.out.println("ACCU1 : AUTOMATED CRAWL CREATION TMAT CRL EXE: "+client.executeWithOneHit(get,true));
					} catch (Exception e) {
						//System.out.println("AUTOMATED CRAWL CREATION TMAT CRL ERROR : "+url);
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			 
		}
		 
	public static Comparator<Event> eventComparatorFordate = new Comparator<Event>() {

		public int compare(Event event1, Event event2) {
			try{
				int cmp =event1.getLocalDate().compareTo(
						event1.getLocalDate());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				if (cmp == 0) {
					return 1;
				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				return event1.getId().compareTo(event2.getId());
		    }};
	
		    
	
}
