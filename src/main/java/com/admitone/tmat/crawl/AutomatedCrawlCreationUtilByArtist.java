package com.admitone.tmat.crawl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.eventcreation.CreationEventListManager;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingManager;
import com.admitone.tmat.eventfetcher.EventListingResult;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.mail.MailManager;

// Ulaganathan : Automated Crawl Creation Process For TMAT Events.

public class AutomatedCrawlCreationUtilByArtist implements InitializingBean{
	
	private CreationEventListManager creationEventListManager;
	private EventListingManager eventListingManager;
	private SharedProperty sharedProperty;
	static MailManager mailManager = null;
	static Date lastRunTime = null;
	static Collection<String> siteList=null;
	static Integer matchingCrawlCount = 0;
	static Integer probableCrawlCount = 0;
	
	static{
		siteList = new ArrayList<String>();
		siteList.add(Site.VIVIDSEAT);
		siteList.add(Site.TICKETS_NOW);
		siteList.add(Site.TICKET_EVOLUTION);
		siteList.add(Site.STUB_HUB);
	}
	
	public EventListingManager getEventListingManager() {
		return eventListingManager;
	}

	public void setEventListingManager(EventListingManager eventListingManager) {
		this.eventListingManager = eventListingManager;
	}
	 
	public CreationEventListManager getcreationEventListManager() {
		return creationEventListManager;
	}

	public void setcreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		if (creationEventListManager == null) {
			throw new RuntimeException("Parameter creationEventListManager must be set");
		}

		if (eventListingManager == null) {
			throw new RuntimeException("Parameter eventListingManager must be set");
		}
		
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		/*calender.set(Calendar.MINUTE, 04);
		calender.add(Calendar.HOUR, 1);*/
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						//createCrawls();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //Every 15mins
	}
	
	public static void main(String[] args) {
		Integer eventSize = 78;
		long div = (78 + 50 - 1) / 50;
		Integer loopSzie = (int)(Math.round(eventSize / 50));
		System.out.println(loopSzie+"---"+div);
	}
	
	
	
	public static Comparator<Event> eventComparatorFordate = new Comparator<Event>() {

		public int compare(Event event1, Event event2) {
			try{
				int cmp =event1.getLocalDate().compareTo(
						event1.getLocalDate());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				
				if (cmp == 0) {
					return 1;
				}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
				return event1.getId().compareTo(event2.getId());
		    }
	};
	
public void createCrawls(){
		
		System.out.println("ACCU2 : AUTOMATED CRAWL CREATION : Job Started....."+new Date());
		
		matchingCrawlCount = 0;
		probableCrawlCount=0;
		
		
		long startTime = new Date().getTime();
		
		Date endDate = new Date();
		Date startDate = null;
		if(lastRunTime == null){
			Calendar apiCal= Calendar.getInstance();
			apiCal.add(Calendar.MINUTE, -15);
			startDate = apiCal.getTime();
		}else{
			startDate = lastRunTime;
		}
		
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.DAY_OF_MONTH, -1);
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByEventCreationDates(null, null, EventStatus.ACTIVE);
		List<Integer> eventIds = DAORegistry.getQueryManagerDAO().AllEventsWhichHasSH_TE_TS_VDCrawls();
		List<Integer> tnEventIds = DAORegistry.getQueryManagerDAO().AllTNEventIdsWhichHasMoreThan5Sales();
		
		Map<Integer, List<Event>> artistEventMap = new HashMap<Integer, List<Event>>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		int i=0,size =events.size();
		for (Event event : events) {
			if(eventIds.contains(event.getId()) || !tnEventIds.contains(event.getAdmitoneId()) || 
					null == event.getLocalDate()){
				continue;
			}
			i++;
			List<Event> eventList = artistEventMap.get(event.getArtistId());
			if(null != eventList && eventList.size() > 0 ){
				artistEventMap.get(event.getArtistId()).add(event);
				continue;
			}
			eventList = new ArrayList<Event>();
			eventList.add(event);
			artistEventMap.put(event.getArtistId(),eventList);
			eventMap.put(event.getId(), event);
		}
		
		String[] siteIds = null;
		Collection<String> toBeCrawlSiteList;
		
		Integer runningEvent = 0, totalEvents = artistEventMap.size();
		
		for (Integer artistId : artistEventMap.keySet()) {
			
			System.out.println("ACCU2 : ARTIST ID: "+artistId+"======================= Search Begins :"+new Date());
			
			List<Event> eventList  = artistEventMap.get(artistId);
			
			toBeCrawlSiteList= new ArrayList<String>(siteList);
			List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllCrawlsByArtistId(artistId);
			/*for (TicketListingCrawl crawlObj : crawls) {
				if(toBeCrawlSiteList.contains(crawlObj.getSiteId())){
					toBeCrawlSiteList.remove(crawlObj.getSiteId());
				}
			}*/
			if(null == toBeCrawlSiteList || toBeCrawlSiteList.isEmpty()){
				continue;
			}
			siteIds = toBeCrawlSiteList.toArray(new String[1]);
			System.out.println("ACCU2 : Real Search Begins SITEIID :"+toBeCrawlSiteList+", Time :"+new Date());
			createCrawls(eventMap, eventList, siteIds,crawls);
			System.out.println("ACCU2 : ARTIST ID: "+artistId+"======================= Search Ends :"+new Date());
			runningEvent++;
			System.out.println("ACCU2 : TotalEvents :"+totalEvents+", Running ARTIST: "+runningEvent+", MatchingCrawlCount: "+matchingCrawlCount+", ProbableCrawlCount: "+probableCrawlCount+", SiteIds: "+siteIds);
		}
		
		long lastCompleteTime = new Date().getTime()-(startTime);
		System.out.println("Time : "+lastCompleteTime);
		lastRunTime = endDate;
		System.out.println("ACCU2 : AUTOMATED CRAWL CREATION : Job End....."+new Date());
	}
	

	public void createCrawls( Map<Integer,Event> eventMap,
		List<Event> eventList,String[] siteIds,List<TicketListingCrawl> crawls){
		String artistName = eventList.get(0).getArtist().getName();
		Date fromDate = eventList.get(0).getLocalDate();
		Date toDate = eventList.get(eventList.size()-1).getLocalDate();
		long crrawlStartTime = new Date().getTime();
		Collection<EventListingResult> eventListingResults = eventListingManager.search(artistName, siteIds, null, fromDate, toDate,eventList,false,false);
		long crawlCompleteTime = new Date().getTime()-(crrawlStartTime);
		System.out.println("ACCU2 : ACC: EVENT RESULTS LEN=" + eventListingResults.size()+", ARTISTNAME="+artistName+", FROMDATE="+fromDate+", TODATE:"+toDate);
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		List<EventHit> additionalEvents = new ArrayList<EventHit>();
		for(EventListingResult result: eventListingResults) {
			if (result.getEventHits() != null) {
				for(EventHit eventhit:result.getEventHits()){
					if(eventhit.getBestMatchEventId()!=null){
						if(TextUtil.isNotValidCrawl(eventhit.getName())){
							continue;
						}
						eventHits.add(eventhit);
					}else{
						probableCrawlCount++;
						additionalEvents.add(eventhit);
					}
				}
			}
		}
		
		String crawlerId="";
		String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls=";
		for (EventHit obj : eventHits) {
			Event tevent = eventMap.get(obj.getBestMatchEventId());
			if(null != tevent){
				TicketListingCrawl ticketListingCrawl = new TicketListingCrawl(obj, tevent, "AUTO");
				boolean flag=true;
				for(TicketListingCrawl crawl:crawls){
					if(crawl.isEquivalent(ticketListingCrawl)){
						flag=false;
					}
				}
				if(flag){  
					if(obj.getId().contains(Site.AOP)){
						Map<String, String> map =new HashMap<String, String>();
						ticketListingCrawl.setExtraParameterMap(map);
					}
					DAORegistry.getTicketListingCrawlDAO().save(ticketListingCrawl);
					crawlerId+=ticketListingCrawl.getId()+",";
					url+=ticketListingCrawl.getId()+",";
					creationEventListManager.addSitesInSitesPerEventMap(tevent.getId(), ticketListingCrawl.getSiteId());
					matchingCrawlCount++;
				}
			}
		}
		
		if(crawlerId != null && !crawlerId.isEmpty()){
			try {
				System.out.println("ACCU2 : AUTOMATED CRAWL CREATION TMAT CRL START :"+url+" : "+new Date()+" : "+"AUTO");
				url=url.substring(0,url.length()-1);
				SimpleHttpClient client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				System.out.println("ACCU2 : AUTOMATED CRAWL CREATION TMAT CRL EXE: "+client.executeWithOneHit(get,true));
			} catch (Exception e) {
				//System.out.println("AUTOMATED CRAWL CREATION TMAT CRL ERROR : "+url);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		 
	}
		    
	
}
