package com.admitone.tmat.pojo;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.data.Ticket;


public class TicketEValStat{
	private double minPrice = Double.MAX_VALUE;
	private double maxPrice = -1.0;
	private int eVal = 0;
	private Collection<Ticket> eValTickets;
	
	public TicketEValStat(Integer newEVal, Ticket ticket){
		eVal = newEVal.intValue();
		minPrice = ticket.getAdjustedCurrentPrice().doubleValue();
		maxPrice = ticket.getAdjustedCurrentPrice().doubleValue();
		eValTickets = new ArrayList<Ticket>();
		eValTickets.add(ticket);
	}
	
	public double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}
	public double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}
	public int geteVal() {
		return eVal;
	}
	public void seteVal(int eVal) {
		this.eVal = eVal;
	}
	public Collection<Ticket> geteValTickets() {
		return eValTickets;
	}
	public void seteValTickets(Collection<Ticket> eValTickets) {
		this.eValTickets = eValTickets;
	}
	public void addeValTicket(Ticket ticket) {
		this.eValTickets.add(ticket);
	}
	@Override
	public String toString() {
		return "TicketEValStat [eVal=" + eVal 
				+ ", maxPrice=" + maxPrice
				+ ", minPrice=" + minPrice 
				+ ", eValTickets=" + eValTickets + "]";
	}
}
