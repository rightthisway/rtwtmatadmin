package com.admitone.tmat.pojo;

import java.io.Serializable;

public class ActionIndicator implements Serializable {

	private int tgCheaper = 0;
	private int eVal = 0;
	private double underprice;
	private double overprice;
	private double underpriceNext;
	private double overpriceNext;
	private double priceDifference;
	
	public ActionIndicator(){}
	
	public ActionIndicator(int tgCheaper, int eVal,
			double underprice, double overprice, double priceDifference, 
			 double overpriceNext, double underpriceNext, String ticketId) {
		super();
		this.tgCheaper = tgCheaper;
		this.eVal = eVal;
		this.underprice = underprice;
		this.overprice = overprice;
		this.priceDifference = priceDifference;
		this.underpriceNext = underpriceNext;
		this.overpriceNext = overpriceNext;
	}

	public int getTgCheaper() {
		return tgCheaper;
	}

	public void setTgCheaper(int tgCheaper) {
		this.tgCheaper = tgCheaper;
	}

	public int geteVal() {
		return eVal;
	}

	public void seteVal(int eVal) {
		this.eVal = eVal;
	}

	public double getUnderprice() {
		return underprice;
	}

	public void setUnderprice(double underprice) {
		this.underprice = underprice;
	}

	public double getOverprice() {
		return overprice;
	}

	public void setOverprice(double overprice) {
		this.overprice = overprice;
	}

	public double getPriceDifference() {
		return priceDifference;
	}

	public void setPriceDifference(double priceDifference) {
		this.priceDifference = priceDifference;
	}

	public double getUnderpriceNext() {
		return underpriceNext;
	}

	public void setUnderpriceNext(double underpriceNext) {
		this.underpriceNext = underpriceNext;
	}

	public double getOverpriceNext() {
		return overpriceNext;
	}

	public void setOverpriceNext(double overpriceNext) {
		this.overpriceNext = overpriceNext;
	}

	@Override
	public String toString() {
		return "ActionIndicator [eVal=" + eVal 
				+ ", tgCheaper=" + tgCheaper 
				+ ", underprice=" + underprice 
				+ ", overprice=" + overprice
				+ ", underpriceNext=" + underpriceNext 
				+ ", overpriceNext=" + overpriceNext
				+ ", priceDifference=" + priceDifference+ "]";
	}

}
