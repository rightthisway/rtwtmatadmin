package com.admitone.tmat.auto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentEvent;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.PresaleEventConverter;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.mail.MailManager;

public class AutoEventCorrector  extends QuartzJobBean implements StatefulJob{

	private static SharedProperty sharedProperty;
	private static MailManager mailManager;
	
	
	
	public static SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		AutoEventCorrector.sharedProperty = sharedProperty;
	}
	public static MailManager getMailManager() {
		return mailManager;
	}
	public void setMailManager(MailManager mailManager) {
		AutoEventCorrector.mailManager = mailManager;
	}



	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try {
			processAutoCorrectEvents();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error Occured in AutoEvent Corrector Scheduler : "+ new Date());
			try {
				String fromName  = "tmatmanagers@rightthisway.com";
				String mimeType = "text/html";
				String subject = "TMAT AutoEvent Corrector Job Failed : ";
				String template = "common-mail-error.html";
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("bodyContent", "Following error occured while processing Auto Event Corrector Scheduler : ");
				map.put("error", e);
				//String toAddress = "pmehul@rightthisway.com";
				String toAddress = "AODev@rightthisway.com";
				mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			}catch (Exception ee) {
				ee.printStackTrace();
				System.err.println("Error Occured in AutoEvent Corrector Scheduler While sending error email : "+ new Date());
			}
		}
	}
	public void processAutoCorrectEvents() throws Exception {
		try {
		Map<Integer, AdmitoneEvent> eventMaps= new HashMap<Integer, AdmitoneEvent>();
		Map<Integer, Venue> venueMap= new HashMap<Integer, Venue>();
		Map<Integer, Venue> tnVenueIdMap = new HashMap<Integer, Venue>();
		System.out.println("auto correct starts :" + new Date());
		
		List<Event> events = DAORegistry.getEventDAO().getToBeAutoCorrectEvents();
		if(events!=null && !events.isEmpty()){
			Collection<AdmitoneEvent> admitoneEvents = DAORegistry.getAdmitoneEventDAO().getAll();
			for(AdmitoneEvent admitoneEvent: admitoneEvents){
				eventMaps.put(admitoneEvent.getEventId(), admitoneEvent);
			}
			Collection<Venue> venues = DAORegistry.getVenueDAO().getAll();
			for (Venue venue : venues) {
				venueMap.put(venue.getId(), venue);
				if(venue.getTnVenueId() != null) {
					tnVenueIdMap.put(venue.getTnVenueId(), venue);	
				}
			}
			List<Event> saveList = new ArrayList<Event>();
			List<EventAudit> eventAuditList = new ArrayList<EventAudit>();
			List<TicketListingCrawl> crawlsList = new ArrayList<TicketListingCrawl>();
			
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
			DateFormat df = new SimpleDateFormat("HH:mm");
			Date now = new Date();
			int eventCount=events.size(),processdCount=0,mismatchCount=0,venueMismatchCount=0,disbaledCrlsCount = 0;
			String crawlIds = "";
			for(Event event:events){
				boolean flag=false;
				AdmitoneEvent admitoneEvent = eventMaps.get(event.getAdmitoneId());
				if(admitoneEvent==null){
					continue;
				}
				processdCount++;
				if(admitoneEvent.getEventTime()!=null && df.format(admitoneEvent.getEventTime()).equals("03:30")){
					admitoneEvent.setEventTime(null);
				}
				if(event.getLocalDate()==null || !event.getLocalDate().equals(admitoneEvent.getEventDate())){
					event.setLocalDate(admitoneEvent.getEventDate());
					flag=true;
				}
				
				if(!event.getIgnoreTBDTime()){
					if(event.getLocalTime()==null && admitoneEvent.getEventTime()!=null) {
						event.setLocalTime(admitoneEvent.getEventTime());
						flag=true;
					} else if (event.getLocalTime() != null && admitoneEvent.getEventTime() == null) {
						event.setLocalTime(admitoneEvent.getEventTime());
						flag=true;
					} else if (event.getLocalTime() != null && admitoneEvent.getEventTime()!=null &&
							!event.getLocalTime().equals(admitoneEvent.getEventTime())) {
						event.setLocalTime(admitoneEvent.getEventTime());
						flag=true;
					}
					/*if((event.getLocalTime()==null && admitoneEvent.getEventTime()!=null) ||
							(event.getLocalTime() != null && admitoneEvent.getEventTime() == null) ||
							!event.getLocalTime().equals(admitoneEvent.getEventTime())){
						event.setLocalTime(admitoneEvent.getEventTime());
						flag=true;
					}*/
				}
				String eventName = "",admitoneEventName="";
				if(event.getName() != null) {
					eventName = TextUtil.removeExtraWhitespaces(event.getName());
				}
				if(admitoneEvent.getEventName() != null) {
					admitoneEventName = TextUtil.removeExtraWhitespaces(admitoneEvent.getEventName());
				}
				if(!eventName.equals(admitoneEventName)) {
					event.setName(admitoneEventName);
					flag=true;
				}
				
				Venue venue = venueMap.get(event.getVenueId());
				if(venue == null || venue.getTnVenueId() == null || !venue.getTnVenueId().equals(admitoneEvent.getVenueId())) {
					Venue tempVenue = tnVenueIdMap.get(admitoneEvent.getVenueId());
					if(tempVenue == null) {
						tempVenue = new Venue();
						tempVenue.setBuilding(admitoneEvent.getVenueName());
						tempVenue.setCity(admitoneEvent.getCity());
						tempVenue.setCountry(admitoneEvent.getCountry());
						tempVenue.setState(admitoneEvent.getStateShortDesc());
						tempVenue.setTnVenueId(admitoneEvent.getVenueId());
						tempVenue.setZipcode(admitoneEvent.getZipcode());
						tempVenue.setVenueType(TourType.OTHER);
						DAORegistry.getVenueDAO().save(tempVenue);
						//venueId= venue.getId();
						tnVenueIdMap.put(tempVenue.getTnVenueId(),tempVenue);
						venueMap.put(tempVenue.getId(),tempVenue);
					}
					event.setVenueId(tempVenue.getId());
					event.setVenue(tempVenue);
					event.setVenueCategoryId(null);
					flag=true;
					venueMismatchCount++;
					
					List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(event.getId());
					if(crawls != null) {
						for (TicketListingCrawl crawl : crawls) {
							crawl.setEnabled(Boolean.FALSE);
							crawl.setLastUpdated(now);
							crawl.setLastUpdater("AUTO EVENT CORRECT");
							
							crawlsList.add(crawl);
							crawlIds += crawl.getId() + ",";
							disbaledCrlsCount++;
						}
					}
				}
				try{
					if(flag){
						mismatchCount++;
						saveList.add(event);
						if(property!=null){
							String dependents = property.getValue();
							if(dependents!= null && !dependents.isEmpty()){
								List<TMATDependentEvent> dependentEvents = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
								Map<String, TMATDependentEvent> map = new HashMap<String, TMATDependentEvent>();
								for(TMATDependentEvent dependentEvent:dependentEvents){
									map.put(dependentEvent.getDependent(), dependentEvent);
								}
								String dependentList[] = dependents.split(",");
								for(String dependent:dependentList){
									if(dependent == null || dependent.isEmpty()){
										continue;
									}
									
									TMATDependentEvent dependentEvent =map.get(dependent);
									if(dependentEvent==null){
										dependentEvent = new TMATDependentEvent();
									}
									if(dependentEvent.isAdd()==null || !dependentEvent.isAdd()){
										dependentEvent.setEventId(event.getId());
										dependentEvent.setDependent(dependent);
										dependentEvent.setAdd(true);
										tmatDependents.add(dependentEvent);
									}
									
								}
								
							}
						}
						EventAudit ea=new EventAudit();
						ea.setEventId(event.getId());
						ea.setUserName("AUTO");
						ea.setAction("Auto Correct");
						ea.setModifiedDate(now);
						ea.setEventName(event.getName());
						ea.setArtistId(event.getArtistId());
						ea.setArtistName(event.getArtist().getName());
						ea.setEventDate(event.getLocalDate());
						ea.setEventTime(event.getLocalTime());
						ea.setVenueId(event.getVenueId());
						ea.setVenueName(event.getVenue().getBuilding());
						if(event.getVenueCategory()!= null) {
							ea.setVenueCategory(event.getVenueCategory().getCategoryGroup().toString());
						} else {
							ea.setVenueCategory(null);
						}
						if(event.getEventType()!=null && event.getEventType().name()!= null){
							ea.setEventType(event.getEventType().name());
						} else{
							ea.setEventType(null);
						}
						if(event.getNoPrice()!= null){
							ea.setNoPrice("YES");
						}else{
							ea.setNoPrice("NO");
						}
						if(event.getAdmitoneId()!= null){
							ea.setAdmitoneId(event.getAdmitoneId());
						}else{
							ea.setAdmitoneId(null);
						}
						eventAuditList.add(ea);
					}
				}catch (Exception e) {
					System.out.println(e.fillInStackTrace());
					throw e;
				}
				
				
			}
			if(!crawlIds.isEmpty()) {
				String url=AutoEventCorrector.sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=disableCrawl&crawls=";
				try {
					crawlIds = crawlIds.substring(0,crawlIds.length()-1);
					url = url + crawlIds;
					
					SimpleHttpClient client = HttpClientStore.createHttpClient();
					HttpGet get = new HttpGet(url);
					System.out.println("TMAT AUTOCORRECT 2 EXE: "+client.executeWithOneHit(get,true));
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw e;
				}
			}
			if(!crawlsList.isEmpty()) {
				DAORegistry.getTicketListingCrawlDAO().updateAll(crawlsList);
			}
			if(!tmatDependents.isEmpty()){
				DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
			}
			System.out.println("AUTO CORRECT : count : "+eventCount+" : proc : "+processdCount+" : mismatch : "+mismatchCount+" : "+" :venueMismatchCount: "+" : disCrlCount: "+disbaledCrlsCount+" : "+new Date());
			DAORegistry.getEventDAO().updateAll(saveList);
			DAORegistry.getEventAuditDAO().saveAll(eventAuditList);
			
		}
		events = null;
		// Initially we used to unlink an event if that event is deleted in TN.. As per new logic given by matt
		// now we will delete those events which is linked but not found in POS ..
		
		Collection<Event> eventsToBeUnLinked = DAORegistry.getEventDAO().getAllEventsToBeUnlinked();
//		for(Event event:eventsToBeUnLinked){
//			event.setAdmitoneId(null);
//			
//		}
		DAORegistry.getEventDAO().deleteAll(eventsToBeUnLinked);
		if(eventsToBeUnLinked != null) {
			System.out.println("AUTO CORRECT REMOVE : "+eventsToBeUnLinked.size());	
		}
		
		System.out.println("auto correct ends :" + new Date());
		
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
			
		}
	}
}
