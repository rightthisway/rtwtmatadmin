package com.admitone.tmat.zones;

import java.util.Date;
import java.util.Timer;

import org.springframework.beans.factory.InitializingBean;

public class ZoneTicketDataDownloader implements InitializingBean{

	//Set the download interval to 3600 seconds (1 hour)
	private Integer ticketDataDownloadInterval;
	private ZonesManager zonesManager;

	public Integer getTicketDataDownloadInterval() {
		return ticketDataDownloadInterval;
	}

	public void setTicketDataDownloadInterval(Integer ticketDataDownloadInterval) {
		this.ticketDataDownloadInterval = ticketDataDownloadInterval;
	}

	public ZonesManager getZonesManager() {
		return zonesManager;
	}

	public void setZonesManager(ZonesManager zonesManager) {
		this.zonesManager = zonesManager;
	}

	
	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}
	
	private void start(){
		
//		System.out.println("In start about to schedule");
//		ZoneTicketDownloadTask zonePlatformTicketDownloadTask = new ZoneTicketDownloadTask();
//		zonePlatformTicketDownloadTask.setZonesManager(zonesManager);
//		Timer timer = new Timer();
//		timer.scheduleAtFixedRate(zonePlatformTicketDownloadTask, new Date(), 600000);
//		System.out.println("After scheduling zoneTicketDownloadTask");
	}
	
	
}
