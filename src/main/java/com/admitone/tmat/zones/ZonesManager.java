package com.admitone.tmat.zones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;



public class ZonesManager implements InitializingBean {

	Map<String, List<ZoneData>> map = new HashMap<String, List<ZoneData>>();
	String srcFileLoc = "\\\\ADMIT1\\Shared\\ExchangeFilesView\\zone\\src\\ZonePricing.csv";
	String destFileLoc = "\\\\ADMIT1\\Shared\\ExchangeFilesView\\zone\\dest\\ZonePricing.csv";
	String destDirLoc = "\\\\ADMIT1\\Shared\\ExchangeFilesView\\zone\\dest";
	String firefoxProfile = "vinit";
	String url = "https://admin.ticketnetwork.com/ZonePricing.aspx";
	String userName = "araut@ticketgallery.com";
	String passwd = "Admit123";
	DateFormat formatDate = new SimpleDateFormat("M/d/yyyy hh:mm:ss a");

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	public void downloadZonesTicket() {/*
		// System.setProperty("webdriver.firefox.profile", firefoxProfile);
		// WebDriver driver = new FirefoxDriver();
		File profileDir = new File("C://Users/vkumar/AppData/Roaming/Mozilla/Firefox/Profiles/1jnn6a1u.vinit");
		FirefoxProfile profile = new FirefoxProfile(profileDir);
		WebDriver driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		driver.get(url);
		driver.findElement(By.id("tbUsername")).clear();
		driver.findElement(By.id("tbUsername")).sendKeys(userName);
		driver.findElement(By.id("tbPassword")).clear();
		driver.findElement(By.id("tbPassword")).sendKeys(passwd);
		driver.findElement(By.id("btnLogin")).click();
		driver.findElement(By.id("btnExport")).click();

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(
				By.cssSelector("#userControlNav1_ctl00_menuNav_95 > nobr"))
				.click();
		// Close the browser
		driver.quit();

	*/}

	public void copyCSVFile() {
		File destFile = new File(destFileLoc);
		File srcFile = new File(srcFileLoc);
		File destDir = new File(destDirLoc);

		if (srcFile.exists() && srcFile.length() > 100) {
			try {
				
				FileUtils.copyFileToDirectory(srcFile, destDir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (destFile.exists() && destFile.length() > 100)
			{
				
				srcFile.delete();
			}
		}
	}

	public Map<String, List<ZoneData>> parseCSVFile() {
		/*try {
			map.clear();
			Long timebefore = System.currentTimeMillis();
			File file = new File(destFileLoc);
			BufferedReader bufRdr = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = bufRdr.readLine()) != null) {
				ZoneData data = new ZoneData();
				StringTokenizer st = new StringTokenizer(line, ",");
				data.setEvent_name(StringUtils.strip(st.nextToken(),"\""));
				data.setVenue_name(StringUtils.strip(st.nextToken(),"\""));
				data.setEvent_date(StringUtils.strip(st.nextToken(),"\""));
				data.setDay_of_week(StringUtils.strip(st.nextToken(),"\""));
				data.setTicket(StringUtils.strip(st.nextToken(),"\""));
				data.setZone(StringUtils.strip(st.nextToken(),"\""));
				data.setYour_price(StringUtils.strip(st.nextToken(),"\""));
				data.setLowest_price(StringUtils.strip(st.nextToken(),"\""));
				data.setWill_show(StringUtils.strip(st.nextToken(),"\""));
				if (map.get(data.getEvent_name()) != null) {
					List<ZoneData> zoneList = map.get(data.getEvent_name());
					zoneList.add(data);
				} else {
					List<ZoneData> zoneList = new ArrayList<ZoneData>();
					zoneList.add(data);
					map.put(data.getEvent_name(), zoneList);
				}
			}

			// close the file
			bufRdr.close();
			//System.out.println("List Size: " + map.size());
			Long timeafter = System.currentTimeMillis();
			System.out.println("Time Taken:" + (timeafter - timebefore));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		 return map;
	}

	public List<String> findEventNameList() {
		if(map == null || map.isEmpty())
			map = parseCSVFile();
		Iterator<String> itr = map.keySet().iterator();
		List<String> eventNameList = new ArrayList<String>();
		while (itr.hasNext()) {
			String eventName = (String) itr.next();			
				eventNameList.add(eventName);
		}
		///System.out.println("EVntName: " + eventNameList.size());
		return eventNameList;
	}

	public List<ZoneData> getZoneTickets(String[] tourNames,String isAllTour,String[] venueSelected,String isAllVenue,Date fromDate,Date toDate,String daySelected,String quantitySelected,String zoneSelected,String isShow){
		List<ZoneData> result = new ArrayList<ZoneData>();
		List<ZoneData> zoneDatas = new ArrayList<ZoneData>();
		Set<String> keys = new HashSet<String>();
		result.clear();
		keys.clear();
		if(tourNames==null && !Boolean.parseBoolean(isAllTour)){
			keys = map.keySet();	
		}
		if((isAllTour!=null && Boolean.parseBoolean(isAllTour) && tourNames!=null) || (isAllVenue!=null && Boolean.parseBoolean(isAllVenue) && venueSelected!=null)){
			keys = map.keySet();
			}else if((isAllTour!=null && !Boolean.parseBoolean(isAllTour) && tourNames!=null && tourNames.length>0) || (isAllVenue!=null && !Boolean.parseBoolean(isAllVenue) && venueSelected!=null && venueSelected.length>0)){
				for(String tourName:tourNames){
					if(tourName!=null && !"".equals(tourName))
						keys.add(tourName);	
				}
				Set<String> tempKeys = new HashSet<String>();
				if(venueSelected!=null && venueSelected.length>0){
				List<String> venueList = Arrays.asList(venueSelected);
				
				
					tempKeys=map.keySet();
					map = parseCSVFile();
					for(String key:tempKeys){
						zoneDatas = map.get(key);
					
						for(ZoneData zoneData:zoneDatas){
						if(venueList.contains(zoneData.getVenue_name())){
							try{
							if((fromDate.getTime()<=formatDate.parse(zoneData.getEvent_date()).getTime()) && (toDate.getTime()>=formatDate.parse(zoneData.getEvent_date()).getTime())){
								
								result.add(zoneData);
							}
						}catch (Exception e) {
							// TODO: handle exception
						}
						}
					}
					}
				}
				
			}
			
			for(String key:keys){
				zoneDatas.clear();
				if(map.containsKey(key)){					
				zoneDatas = map.get(key);
				
				//result.addAll(zoneDatas);
				try{
				for(ZoneData zoneData:zoneDatas){
					if(formatDate.isLenient()){
					if((fromDate.getTime()<=formatDate.parse(zoneData.getEvent_date()).getTime()) && (toDate.getTime()>=formatDate.parse(zoneData.getEvent_date()).getTime())){
						
							result.add(zoneData);
					}
					}
				}
				}catch (Exception e) {
					// TODO: handle exception
					//e.printStackTrace();
				}
				}
								
			}
			List<ZoneData> tempList = new ArrayList<ZoneData>();
			if(daySelected!=null && !"ALL".equals(daySelected)){
				for(ZoneData zoneData:result){
					if(!zoneData.getDay_of_week().equalsIgnoreCase(daySelected)){
						tempList.add(zoneData);
					
					}
				}
			}
			if(tempList.size()>0){
			synchronized (result) {
				result.removeAll(tempList);
			}
			}
			tempList.clear();
			if(quantitySelected!=null && !"ALL".equals(quantitySelected)){
				for(ZoneData zoneData:result){
					if(Integer.parseInt(zoneData.getTicket())!= Integer.parseInt(quantitySelected)){
						tempList.add(zoneData);
					}
				}
			}
			if(tempList.size()>0){
				synchronized (result) {
					result.removeAll(tempList);
				}
				}
				tempList.clear();
			if(zoneSelected!=null && !"ALL".equals(zoneSelected)){
				for(ZoneData zoneData:result){
					if(!zoneData.getZone().equalsIgnoreCase(zoneSelected)){
						tempList.add(zoneData);
					}
				}
			}
			if(tempList.size()>0){
				synchronized (result) {
					result.removeAll(tempList);
				}
				}
				tempList.clear();
			
		return result;
	}
	public List<String> getVenueByTour(String[] tourNames,String isAllVenue) {
		List<String> resultVenue = new ArrayList<String>();
		List<ZoneData> zoneDatas = new ArrayList<ZoneData>();
		Set<String> keys = new HashSet<String>();
		keys = map.keySet();
		map=parseCSVFile();
		resultVenue.clear();
		for(String key:keys){	
			zoneDatas.clear();
			zoneDatas.addAll(map.get(key));
			for(ZoneData zoneData:zoneDatas){
				if(!resultVenue.contains(zoneData.getVenue_name())){
					resultVenue.add(zoneData.getVenue_name());
				}
			}
			
		}
		
		Collections.sort(resultVenue);
		return resultVenue;
	}
}
