package com.admitone.tmat.zones;

public class ZoneData {
	
	String event_name;
	String event_date;
	String venue_name;
	String day_of_week;
	String zone;
	String your_price;
	String lowest_price;
	String ticket;
	String will_show;
	
	
	public String getEvent_name() {
		return event_name;
	}
	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}
	public String getEvent_date() {
		return event_date;
	}
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	public String getVenue_name() {
		return venue_name;
	}
	public void setVenue_name(String venue_name) {
		this.venue_name = venue_name;
	}
	public String getDay_of_week() {
		return day_of_week;
	}
	public void setDay_of_week(String day_of_week) {
		this.day_of_week = day_of_week;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getYour_price() {
		return your_price;
	}
	public void setYour_price(String your_price) {
		this.your_price = your_price;
	}
	public String getLowest_price() {
		return lowest_price;
	}
	public void setLowest_price(String lowest_price) {
		this.lowest_price = lowest_price;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getWill_show() {
		if("1".equals(will_show))
			return "Y";
		else
			return "N";
	}
	public void setWill_show(String will_show) {
		this.will_show = will_show;
	}
	@Override
	public String toString() {
		return "ZoneData [event_name=" + event_name + ", event_date="
				+ event_date + ", venue_name=" + venue_name + ", day_of_week="
				+ day_of_week + ", zone=" + zone + ", your_price=" + your_price
				+ ", lowest_price=" + lowest_price + ", ticket=" + ticket + "]";
	}

}
