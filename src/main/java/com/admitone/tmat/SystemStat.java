package com.admitone.tmat;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemStat implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long maxMemory;
	private Long freeMemory;
	private Long totalMemory;
	private Map<String, Integer> freeHttpClientCountBySiteId;
	private Map<String, Integer> allocatedHttpClientCountBySiteId;
	private Map<String, Double> averageHttpClientLifeTimeBySiteId;
	private Long dBConnectionCount;
	private Long dBBusyConnectionCount;
	private Long dBIdleConnectionCount;
	private Long dBUnclosedOrphanedConnectionCount;
	private Long dBCloseStatementCount;
	private Long dBFlushCount;
	private Long dBConnectCount;
	private Long dBQueryExecutionCount;
	private Long dBTransactionCount;
	private Long dBSuccessfulTransactionCount;
	private Long dBSessionOpenCount;
	private Long dBSessionCloseCount;
	private Long dBPrepareStatementCount;
	private Long dBOptimisticFailureCount;
	private Long dBQueryExecutionMaxTime;
	private String dBQueryExecutionMaxTimeQueryString;	
	private String hostname;
	private String ipAddress;
	private Logger logger = LoggerFactory.getLogger(SystemStat.class);
	
	public SystemStat() {
		try {
			InetAddress addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
	    	byte[] b = addr.getAddress();
	    	ipAddress = (b[0] & 0xFF) + "." + (b[1] & 0xFF) + "." + (b[2] & 0xFF) + "." + (b[3] & 0xFF);
		} catch (UnknownHostException e) {
			logger.error("Cannot get hostname or ip address", e);
		}
	}
	
	public String getHostname() {
		return hostname;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public Long getMaxMemory() {
		return maxMemory;
	}
	public void setMaxMemory(Long maxMemory) {
		this.maxMemory = maxMemory;
	}
	public Long getFreeMemory() {
		return freeMemory;
	}
	public void setFreeMemory(Long freeMemory) {
		this.freeMemory = freeMemory;
	}
	public Long getTotalMemory() {
		return totalMemory;
	}
	public void setTotalMemory(Long totalMemory) {
		this.totalMemory = totalMemory;
	}

	public Map<String, Integer> getAllocatedHttpClientCountBySiteId() {
		return allocatedHttpClientCountBySiteId;
	}
	public void setAllocatedHttpClientCountBySiteId(
			Map<String, Integer> allocatedHttpClientCountBySiteId) {
		this.allocatedHttpClientCountBySiteId = allocatedHttpClientCountBySiteId;
	}
	public Map<String, Double> getAverageHttpClientLifeTimeBySiteId() {
		return averageHttpClientLifeTimeBySiteId;
	}
	public void setAverageHttpClientLifeTimeBySiteId(
			Map<String, Double> averageHttpClientLifeTimeBySiteId) {
		this.averageHttpClientLifeTimeBySiteId = averageHttpClientLifeTimeBySiteId;
	}
	public Map<String, Integer> getFreeHttpClientCountBySiteId() {
		return freeHttpClientCountBySiteId;
	}
	public void setFreeHttpClientCountBySiteId(
			Map<String, Integer> freeHttpClientCountBySiteId) {
		this.freeHttpClientCountBySiteId = freeHttpClientCountBySiteId;
	}

	public Long getDBConnectionCount() {
		return dBConnectionCount;
	}

	public void setDBConnectionCount(Long connectionCount) {
		dBConnectionCount = connectionCount;
	}

	public Long getDBBusyConnectionCount() {
		return dBBusyConnectionCount;
	}

	public void setDBBusyConnectionCount(Long busyConnectionCount) {
		dBBusyConnectionCount = busyConnectionCount;
	}

	public Long getDBIdleConnectionCount() {
		return dBIdleConnectionCount;
	}

	public void setDBIdleConnectionCount(Long idleConnectionCount) {
		dBIdleConnectionCount = idleConnectionCount;
	}

	public Long getDBUnclosedOrphanedConnectionCount() {
		return dBUnclosedOrphanedConnectionCount;
	}

	public void setDBUnclosedOrphanedConnectionCount(
			Long unclosedOrphanedConnectionCount) {
		dBUnclosedOrphanedConnectionCount = unclosedOrphanedConnectionCount;
	}

	public Long getDBCloseStatementCount() {
		return dBCloseStatementCount;
	}

	public void setDBCloseStatementCount(Long closeStatementCount) {
		dBCloseStatementCount = closeStatementCount;
	}

	public Long getDBFlushCount() {
		return dBFlushCount;
	}

	public void setDBFlushCount(Long flushCount) {
		dBFlushCount = flushCount;
	}

	public Long getDBConnectCount() {
		return dBConnectCount;
	}

	public void setDBConnectCount(Long connectCount) {
		dBConnectCount = connectCount;
	}

	public Long getDBQueryExecutionCount() {
		return dBQueryExecutionCount;
	}

	public void setDBQueryExecutionCount(Long queryExecutionCount) {
		dBQueryExecutionCount = queryExecutionCount;
	}

	public Long getDBTransactionCount() {
		return dBTransactionCount;
	}

	public void setDBTransactionCount(Long transactionCount) {
		dBTransactionCount = transactionCount;
	}

	public Long getDBSuccessfulTransactionCount() {
		return dBSuccessfulTransactionCount;
	}

	public void setDBSuccessfulTransactionCount(Long successfulTransactionCount) {
		dBSuccessfulTransactionCount = successfulTransactionCount;
	}

	public Long getDBSessionOpenCount() {
		return dBSessionOpenCount;
	}

	public void setDBSessionOpenCount(Long sessionOpenCount) {
		dBSessionOpenCount = sessionOpenCount;
	}

	public Long getDBSessionCloseCount() {
		return dBSessionCloseCount;
	}

	public void setDBSessionCloseCount(Long sessionCloseCount) {
		dBSessionCloseCount = sessionCloseCount;
	}

	public Long getDBPrepareStatementCount() {
		return dBPrepareStatementCount;
	}

	public void setDBPrepareStatementCount(Long prepareStatementCount) {
		dBPrepareStatementCount = prepareStatementCount;
	}

	public Long getDBOptimisticFailureCount() {
		return dBOptimisticFailureCount;
	}

	public void setDBOptimisticFailureCount(Long optimisticFailureCount) {
		dBOptimisticFailureCount = optimisticFailureCount;
	}

	public Long getDBQueryExecutionMaxTime() {
		return dBQueryExecutionMaxTime;
	}

	public void setDBQueryExecutionMaxTime(Long queryExecutionMaxTime) {
		dBQueryExecutionMaxTime = queryExecutionMaxTime;
	}

	public String getDBQueryExecutionMaxTimeQueryString() {
		return dBQueryExecutionMaxTimeQueryString;
	}

	public void setDBQueryExecutionMaxTimeQueryString(
			String queryExecutionMaxTimeQueryString) {
		dBQueryExecutionMaxTimeQueryString = queryExecutionMaxTimeQueryString;
	}

}
