package com.admitone.tmat.util;

public class VividListedEvent {
	private Integer id;
	private String name;
	private String eventDate;
	private String eventTime;
	private String frequentEvent;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String isFrequentEvent() {
		return frequentEvent;
	}
	public void setFrequentEvent(String frequentEvent) {
		this.frequentEvent = frequentEvent;
	}

}
