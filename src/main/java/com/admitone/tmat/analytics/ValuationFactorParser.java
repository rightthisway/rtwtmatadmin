package com.admitone.tmat.analytics;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.admitone.tmat.analytics.ValuationFactorComputation.HomeTeamSectionBlock;
import com.admitone.tmat.analytics.ValuationFactorComputation.Mirror;
import com.admitone.tmat.analytics.ValuationFactorComputation.SectionBlock;
import com.admitone.tmat.analytics.ValuationFactorComputation.SectionInput;

public class ValuationFactorParser {
	public static final int TARGET_NONE = 0;
	public static final int TARGET_MIRRORS = 1;
	public static final int TARGET_SECTIONS = 2;
	public static final int TARGET_ROWS = 3;
	public static final int TARGET_HOMETEAM_SECTIONS = 4;
	public static final int TARGET_VENUE = 5;
	public static final int TARGET_DATE = 6;
	
	private static final Pattern targetPattern = Pattern.compile("\\[(.*)\\]");
	private static final Pattern sectionBlockPattern = Pattern.compile("(\\d+)-(\\d+)");
	private static final Pattern sectionPattern = Pattern.compile("(\\d+)");
	private static final Pattern homeTeamSectionBlockPattern = Pattern.compile("(\\d+)-(\\d+)\\s*,\\s*(\\d+)");
//	private static final Pattern sectionPattern = Pattern.compile("(?:(\\d+)\\s*,\\s*)+([><=])\\s*,\\s*([A-Za-z0-9]+)");
	private static final Pattern sectionInputPattern = Pattern.compile("(\\d+)(?:-(\\d+))?\\s*,\\s*([><=])\\s*,\\s*([A-Za-z0-9]+)");
	
	public int getTargetFromName(String name) {
		if (name.equalsIgnoreCase("MIRRORS")) {
			return TARGET_MIRRORS;
		}

		if (name.equalsIgnoreCase("HOMETEAM_SECTIONS")) {
			return TARGET_HOMETEAM_SECTIONS;
		}

		if (name.equalsIgnoreCase("SECTIONS")) {
			return TARGET_SECTIONS;
		}

		if (name.equalsIgnoreCase("ROWS")) {
			return TARGET_ROWS;
		}

		if (name.equalsIgnoreCase("VENUE")) {
			return TARGET_VENUE;
		}

		if (name.equalsIgnoreCase("DATE")) {
			return TARGET_DATE;
		}

		return TARGET_NONE;
	}
	
	public ValuationFactorComputation parse(InputStream inputStream) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		int lineNum = 0;
		int target = TARGET_NONE;
		int groupId = 0;
		int index = 0;
		
		ValuationFactorComputation valuationFactorComputation = new ValuationFactorComputation();
		
		Collection<Mirror> mirrors = new ArrayList<Mirror>();
		Collection<String> rows = new ArrayList<String>();
		Collection<HomeTeamSectionBlock> homeTeamSectionBlocks = new ArrayList<HomeTeamSectionBlock>();
//		HomeTeamSectionManager homeTeamSectionManager = new HomeTeamSectionManager();
		Collection<SectionInput> sectionInputs = new ArrayList<SectionInput>();
		
		valuationFactorComputation.setMirrors(mirrors);
		valuationFactorComputation.setRows(rows);
		valuationFactorComputation.setHomeTeamSectionBlocks(homeTeamSectionBlocks);
		valuationFactorComputation.setSectionInputs(sectionInputs);
		
		while ((line = reader.readLine()) != null) {
			lineNum++;

			line = line.trim();
			if (line.isEmpty() || line.startsWith("--")) {
				continue;
			}
			
			Matcher matcher = targetPattern.matcher(line);
			if (matcher.find()) {
				String targetName = matcher.group(1);
				target = getTargetFromName(targetName);
				if (target == TARGET_NONE) {
					throw new Exception(lineNum + ":" + line + ": target is undefined");
				}
				continue;
			}
			switch(target) {
				case TARGET_MIRRORS:
					Mirror mirror = new Mirror();
					boolean found = false;
					matcher = sectionBlockPattern.matcher(line);
					while (matcher.find()) {
						Integer startSection = Integer.valueOf(matcher.group(1));
						Integer endSection = Integer.valueOf(matcher.group(2));
						mirror.addSectionBlock(new SectionBlock(startSection, endSection));
						mirrors.add(mirror);
						found = true;
					}
					
					if (!found) {
						matcher = sectionPattern.matcher(line);
						while (matcher.find()) {
							Integer section = Integer.valueOf(matcher.group(1));
							mirror.addSectionBlock(new SectionBlock(section, section));
							mirrors.add(mirror);							
						}
					}
				break;

				case TARGET_HOMETEAM_SECTIONS:
					matcher = homeTeamSectionBlockPattern.matcher(line);
					if (matcher.find()) {
						Integer startSection = Integer.valueOf(matcher.group(1));
						Integer endSection = Integer.valueOf(matcher.group(2));
						Integer advantage = Integer.valueOf(matcher.group(3));
						HomeTeamSectionBlock sectionBlock = new HomeTeamSectionBlock(startSection, endSection, advantage);
						homeTeamSectionBlocks.add(sectionBlock);
					} else {
						throw new Exception(lineNum + ":" + line + ": syntax error (should be A-B,weight)");
					}
					break;

				case TARGET_ROWS:
					String row = line;
					rows.add(row);
				break;

				case TARGET_VENUE:
					valuationFactorComputation.setVenue(line);
				break;

				case TARGET_DATE:
					if (!line.contains("-")) {
						valuationFactorComputation.setStartDate(dateFormat.parse(line));
						valuationFactorComputation.setEndDate(dateFormat.parse(line));
					} else {
						String tokens[] = line.split("-");
						valuationFactorComputation.setStartDate(dateFormat.parse(tokens[0]));
						valuationFactorComputation.setEndDate(dateFormat.parse(tokens[1]));
					}
				break;

				case TARGET_SECTIONS:
					matcher = sectionInputPattern.matcher(line);
					groupId++;
					if (matcher.find()) {
						String operator;
						String overlap;

						if (matcher.group(2) != null) {
							Integer startSection = Integer.valueOf(matcher.group(1));
							Integer endSection = Integer.valueOf(matcher.group(2));
							operator = matcher.group(3);
							overlap = matcher.group(4);
							
							if (startSection < endSection) {
								for (int i = startSection; i <= endSection; i++) {
									SectionInput sectionInput = new SectionInput(i, groupId++ + "", operator, overlap, index++);
									sectionInputs.add(sectionInput);
								}
							} else {
								for (int i = startSection; i >= endSection; i--) {
									SectionInput sectionInput = new SectionInput(i, groupId++ + "", operator, overlap, index++);
									sectionInputs.add(sectionInput);
								}								
							}
						} else {
							Integer sectionNumber = Integer.valueOf(matcher.group(1));
							operator = matcher.group(3);
							overlap = matcher.group(4);							
							SectionInput sectionInput = new SectionInput(sectionNumber, groupId++ + "", operator, overlap, index++);
							sectionInputs.add(sectionInput);
						}
						
						
//						for (int i = 2; i <= matcher.groupCount() - 1; i++) {
//							Integer sectionNumber = Integer.valueOf(matcher.group(i));
//							SectionInput sectionInput = new SectionInput(sectionNumber, groupId + "", operator, overlap, index++);
//							sectionInputs.add(sectionInput);
//							System.out.println(sectionNumber + " " + operator + " " + overlap);
//						}
					} else {
						throw new Exception("line " + lineNum + ": " + line + ": syntax error");
					}
					break;
			}
		}

		// check for duplicated in the list
		for (SectionInput sectionInput1: sectionInputs) {
			for (SectionInput sectionInput2: sectionInputs) {
				if (sectionInput1 == sectionInput2) {
					continue;
				}
				
				if (sectionInput1.getSection().equals(sectionInput2.getSection())) {
					throw new RuntimeException("The section " + sectionInput2.getSection() + " is duplicated");
				}
			}
		}		

		return valuationFactorComputation;
	}
}
