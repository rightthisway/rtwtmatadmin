package com.admitone.tmat.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class OldValuationFactorComputation {
	private Collection<Factor> factors = new ArrayList<Factor>();
	
	public void init() {
//		int[] rows = {15, 14, 13, 16, 12, 11, 10, 17, 9, 8, 18, 7, 6, 5, 19, 4, 3, 2, 1, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41};
//		int[] sections = {111, 110, 109, 108, 107, 106, 105, 104, 101, 102, 103};
//		String[] rows = {"1-8", "9-14", "15-22", "23-28", "29-34", "35-37", "38-41"};
//
//		String[][] sections = {{"111", null, "=", "38-41"},
//				            {"110", null, ">", "35-37"},
//				            {"109", null, ">", "35-37"},
//				            {"108", null, ">", "35-37"},
//				            {"107", null, ">", "35-37"},
//				            {"105", null, ">", "35-37"},
//				            {"104", "1", null, null},
//				            {"101", "1", null, null},
//				            {"102", "1", null, null},
//				            {"103", "1", null, null}};

		// ordered rows
		String[] rows = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"};

		// ordered section
		// section, groupId, overlap operator, overlap row
		// if overlap operator is > then it means the overlap for the next
		// section starts after the overlap row
		// if it is = then it means it starts at the overlap row
		String[][] sections = {{"53", null, ">", "M"},
							{"54", null, ">", "M"},
				            {"82", null, ">", "M"},
				            {"55", null, ">", "M"},
				            {"56", null, ">", "M"},
				            {"57", null, ">", "M"},
				            {"79", null, ">", "M"},
				            {"78", null, ">", "M"},
				            {"77", null, ">", "M"},
				            {"84", null, ">", "M"},
				            {"85", null, ">", "M"},
				            {"86", null, ">", "M"},
				            {"50", null, ">", "M"},
				            {"49", null, ">", "M"},
				            {"48", null, ">", "M"},
				            {"58", null, ">", "M"},
				            {"59", null, ">", "M"},
				            {"60", null, ">", "M"},
				            {"76", null, ">", "M"},
				            {"75", null, ">", "M"},
				            {"74", null, ">", "M"},
				            {"87", null, ">", "M"},
				            {"88", null, ">", "M"},
				            {"89", null, ">", "M"},
				            {"47", null, ">", "M"},
				            {"46", null, ">", "M"},
				            {"45", null, ">", "M"},
				            {"107", null, ">", "M"},
				            {"108", null, ">", "M"},
				            {"124", null, ">", "M"},
				            {"125", null, ">", "M"},
				            {"61", null, ">", "M"},
				            {"62", null, ">", "M"},
				            {"63", null, ">", "M"},
				            {"73", null, ">", "M"},
				            {"72", null, ">", "M"},
				            {"71", null, ">", "M"},
				            {"90", null, ">", "M"},
				            {"91", null, ">", "M"},
				            {"92", null, ">", "M"},
				            {"44", null, ">", "M"},
				            {"43", null, ">", "M"},
				            {"42", null, ">", "M"},
				            {"109", null, ">", "M"},
				            {"121", null, ">", "M"},
				            {"126", null, ">", "M"},
				            {"104", null, ">", "M"},
				            {"64", null, ">", "M"},
				            {"65", null, ">", "M"},
				            {"66", null, ">", "M"},
				            {"67", null, ">", "M"},
				            {"93", null, ">", "M"},
				            {"94", null, ">", "M"},
				            {"95", null, ">", "M"},
				            {"96", null, ">", "M"},
				            {"207", null, ">", "M"},
				            {"224", null, ">", "M"},
				            {"208", null, ">", "M"},
				            {"222", null, ">", "M"},
				            {"225", null, ">", "M"},
				            {"205", null, ">", "M"},
				            {"110", null, ">", "M"},
				            {"120", null, ">", "M"},
				            {"127", null, ">", "M"},
				            {"103", null, ">", "M"},
				            {"209", null, ">", "M"},
				            {"221", null, ">", "M"},
				            {"226", null, ">", "M"},
				            {"204", null, ">", "M"},
				            {"334", null, ">", "M"},
				            {"309", null, ">", "M"},
				            {"210", null, ">", "M"},
				            {"220", null, ">", "M"},
				            {"227", null, ">", "M"},
				            {"203", null, ">", "M"},
				            {"111", null, ">", "M"},
				            {"128", null, ">", "M"},
				            {"311", null, ">", "M"},
				            {"333", null, ">", "M"},
				            {"336", null, ">", "M"},
				            {"308", null, ">", "M"},
				            {"313", null, ">", "M"},
				            {"314", null, ">", "M"},
				            {"315", null, ">", "M"},
				            {"331", null, ">", "M"},
				            {"330", null, ">", "M"},
				            {"329", null, ">", "M"},
				            {"338", null, ">", "M"},
				            {"339", null, ">", "M"},
				            {"340", null, ">", "M"},
				            {"306", null, ">", "M"},
				            {"305", null, ">", "M"},
				            {"304", null, ">", "M"},
				            {"316", null, ">", "M"},
				            {"317", null, ">", "M"},
				            {"318", null, ">", "M"},
				            {"328", null, ">", "M"},
				            {"327", null, ">", "M"},
				            {"326", null, ">", "M"},
				            {"341", null, ">", "M"},
				            {"342", null, ">", "M"},
				            {"343", null, ">", "M"},
				            {"303", null, ">", "M"},
				            {"302", null, ">", "M"},
				            {"301", null, ">", "M"},
				            {"423", null, ">", "M"},
				            {"424", null, ">", "M"},
				            {"425", null, ">", "M"},
				            {"426", null, ">", "M"},
				            {"406", null, ">", "M"},
				            {"407", null, ">", "M"},
				            {"325", null, ">", "M"},
				            {"324", null, ">", "M"},
				            {"323", null, ">", "M"},
				            {"322", null, ">", "M"},
				            {"344", null, ">", "M"},
				            {"345", null, ">", "M"},
				            {"346", null, ">", "M"},
				            {"347", null, ">", "M"},
				            {"420", null, ">", "M"},
				            {"419", null, ">", "M"},
				            {"418", null, ">", "M"},
				            {"417", null, ">", "M"},
				            {"416", null, ">", "M"},
				            {"415", null, ">", "M"}};
				            
		// sectionId => Collection of factors
		Map<String, LinkedList<Factor>> map = new HashMap<String, LinkedList<Factor>>();

		String oldGroupId = null;
		Integer oldOverlapValue = null;
		int value = 1;
		int oldValue = 1; 
		
		for (String[] section: sections) {
			String sectionId = section[0];
			String groupId = section[1];
			String overlapOperator = section[2];
			String overlapRow = section[3];
			
			LinkedList<Factor> factors = new LinkedList<Factor>();
			map.put(sectionId, factors);
			
			if (oldOverlapValue != null) {
				value = oldOverlapValue;
			}

			if (oldGroupId != null && groupId != null && groupId.equals(oldGroupId)) {
				value = oldValue;
			}
			
			int beforeValue = value;
			for (String row: rows) {
				Factor factor = new Factor(sectionId, row, overlapRow, overlapRow);
//				System.out.println(sectionId + "," + row + "," + value);
				factors.add(factor);
				if (overlapRow != null && overlapOperator != null && overlapRow.equals(row)) {
					if (overlapOperator.equals("=")) {
						oldOverlapValue = value;
					} else if (overlapOperator.equals(">")){
						value++;
						oldOverlapValue = value;
					}
				}
				value++;
			}
			
			oldValue = beforeValue;
			oldGroupId = groupId;
//			System.out.println();
		}
		
//		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("extra/Giants.csv"));
//		String line;
//		int lineCount = 0;
//		while ((line = reader.readLine()) != null) {
//		}
	}
	
	public static void main(String[] args) {
		OldValuationFactorComputation comp = new OldValuationFactorComputation();
		comp.init();
	}
}

class Factor {
	private String section;
	private String row;
	private String overlapOperator;
	private String overlapRow;
	
	public Factor(String section, String row, String overlapOperator,
			String overlapRow) {
		super();
		this.section = section;
		this.row = row;
		this.overlapOperator = overlapOperator;
		this.overlapRow = overlapRow;
	}
	
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getOverlapOperator() {
		return overlapOperator;
	}
	public void setOverlapOperator(String overlapOperator) {
		this.overlapOperator = overlapOperator;
	}
	public String getOverlapRow() {
		return overlapRow;
	}
	public void setOverlapRow(String overlapRow) {
		this.overlapRow = overlapRow;
	}
}