package com.admitone.tmat.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ValuationFactorComputation {
	private Collection<String> rows;
	private Collection<SectionInput> sectionInputs;
	private Collection<Mirror> mirrors;
	private Collection<HomeTeamSectionBlock> homeTeamSectionBlocks;
	private String venue;
	private Date startDate;
	private Date endDate;
	
	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Collection<HomeTeamSectionBlock> getHomeTeamSectionBlocks() {
		return homeTeamSectionBlocks;
	}

	public void setHomeTeamSectionBlocks(
			Collection<HomeTeamSectionBlock> homeTeamSectionBlocks) {
		this.homeTeamSectionBlocks = homeTeamSectionBlocks;
	}

	public Collection<String> getRows() {
		return rows;
	}

	public void setRows(Collection<String> rows) {
		this.rows = rows;
	}

	public Collection<SectionInput> getSectionInputs() {
		return sectionInputs;
	}

	public void setSectionInputs(Collection<SectionInput> sectionInputs) {
		this.sectionInputs = sectionInputs;
	}

	public Collection<Mirror> getMirrors() {
		return mirrors;
	}

	public void setMirrors(Collection<Mirror> mirrors) {
		this.mirrors = mirrors;
	}

	public Collection<ValuationFactor> computeValuationFactors() throws Exception {
		HomeTeamSectionManager homeTeamSectionManager = new HomeTeamSectionManager(homeTeamSectionBlocks);
				
		// compute mirrored sections
		ArrayList<SectionInput> processedSectionInputs = new ArrayList<SectionInput>(sectionInputs);
		for (Mirror mirror: mirrors) {
			processedSectionInputs.addAll(mirror.getAllSectionInputs(sectionInputs));
		}

		// re-sort section inputs
		Collections.sort(processedSectionInputs, new Comparator<SectionInput>() {

			public int compare(SectionInput sectionInput1, SectionInput sectionInput2) {
				return sectionInput1.getIndex().compareTo(sectionInput2.getIndex());
			}
			
		});
		
		// compute the factors
		Map<Integer, LinkedList<Factor>> map = new HashMap<Integer, LinkedList<Factor>>();

		Collection<ValuationFactor> valuationFactors = new ArrayList<ValuationFactor>();

		String oldGroupId = null;
		Integer oldOverlapValue = null;
		int value = 1;
		int oldValue = 1;
		
		for (SectionInput sectionInput: processedSectionInputs) {
			LinkedList<Factor> factors = new LinkedList<Factor>();
			map.put(sectionInput.getSection(), factors);
			
			if (oldOverlapValue != null) {
				value = oldOverlapValue;
			}

			if (oldGroupId != null && sectionInput.getGroupId() != null && sectionInput.getGroupId().equals(oldGroupId)) {
				value = oldValue;
			}
			
			int beforeValue = value;
			int homeTeamBoost = homeTeamSectionManager.getAdvantage(sectionInput.getSection());
						
			for (String row: rows) {
				ValuationFactor factor = new ValuationFactor(venue, startDate, endDate, sectionInput.getSection() + "", row, value + homeTeamBoost);
				valuationFactors.add(factor);
				
				if (sectionInput.getOverlapRow() != null && sectionInput.getOverlapOperator() != null && sectionInput.getOverlapRow().equals(row)) {
					if (sectionInput.getOverlapOperator().equals("=")) {
						oldOverlapValue = value;
					} else if (sectionInput.getOverlapOperator().equals(">")){
						value++;
						oldOverlapValue = value;
					}
				}
				value++;
			}
			
			oldValue = beforeValue;
			oldGroupId = sectionInput.getGroupId();
		}
		
		return valuationFactors;
	}
	
	public static class HomeTeamSectionManager {
		private Collection<HomeTeamSectionBlock> sectionBlocks = new ArrayList<HomeTeamSectionBlock>();
		
		public HomeTeamSectionManager() {}
		
		public HomeTeamSectionManager(Collection<HomeTeamSectionBlock> sectionBlocks) {
			this.sectionBlocks = sectionBlocks;
		}
		
		public void addSectionBlock(HomeTeamSectionBlock sectionBlock) {
			sectionBlocks.add(sectionBlock);
		}

		public int getAdvantage(Integer sectionNumber) {
			for (HomeTeamSectionBlock sectionBlock: sectionBlocks) {
				if (sectionBlock.contains(sectionNumber)) {
					return sectionBlock.getAdvantage();
				}
			}	
			return 0;
		}

		public boolean contains(Integer sectionNumber) {
			for (HomeTeamSectionBlock sectionBlock: sectionBlocks) {
				if (sectionBlock.contains(sectionNumber)) {
					return true;
				}
			}
			
			return false;
		}
	}
	
	public static class HomeTeamSectionBlock extends SectionBlock {
		private Integer advantage;
		
		public HomeTeamSectionBlock(Integer startSection, Integer endSection, Integer advantage) {
			super(startSection, endSection);
			this.advantage = advantage;
		}
		
		public Integer getAdvantage() {
			return advantage;
		}

		public void setAdvantage(Integer advantage) {
			this.advantage = advantage;
		}
	}
	
	public static class SectionBlock {
		// best section
		private Integer startSection;
		
		// worst section
		private Integer endSection;

		public SectionBlock(Integer startSection, Integer endSection) {
			super();
			this.startSection = startSection;
			this.endSection = endSection;
		}

		public Integer getStartSection() {
			return startSection;
		}
		public void setStartSection(Integer startSection) {
			this.startSection = startSection;
		}
		public Integer getEndSection() {
			return endSection;
		}
		public void setEndSection(Integer endSection) {
			this.endSection = endSection;
		}
		
		public boolean contains(Integer section) {
			// TODO: improve this as some sections might be letters
			Integer start = Integer.valueOf(startSection);
			Integer end = Integer.valueOf(endSection);
			
			Integer n = Integer.valueOf(section);
			if ((start <= n && n <= end)
			    || (end <= n && n <= start)) {
				return true;
			}
			
			return false;
		}
		
		public Integer getOffsetSection(SectionInput sectionInput) {
			Integer section = Integer.valueOf(sectionInput.getSection());
			
			if (startSection < endSection) {
				return (section - startSection);
			} else {
				return (startSection - section);
			}
		}
		
		public Integer getSectionFromOffset(Integer offset) {
			if (startSection < endSection) {
				return startSection + offset;
			} else {
				return startSection - offset;
			}
		}
	}

	public static class Factor {
		private String section;
		private String row;
		private String overlapOperator;
		private String overlapRow;
		
		public Factor(String section, String row, String overlapOperator,
				String overlapRow) {
			super();
			this.section = section;
			this.row = row;
			this.overlapOperator = overlapOperator;
			this.overlapRow = overlapRow;
		}
		
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public String getRow() {
			return row;
		}
		public void setRow(String row) {
			this.row = row;
		}
		public String getOverlapOperator() {
			return overlapOperator;
		}
		public void setOverlapOperator(String overlapOperator) {
			this.overlapOperator = overlapOperator;
		}
		public String getOverlapRow() {
			return overlapRow;
		}
		public void setOverlapRow(String overlapRow) {
			this.overlapRow = overlapRow;
		}
	}	
	
	public static class SectionInput {
		private Integer section;
		private String groupId;
		private String overlapOperator;
		private String overlapRow;
		private Integer index;

		public SectionInput(Integer section, String groupId, String overlapOperator,
				String overlapRow, Integer index) {
			super();
			this.section = section;
			this.groupId = groupId;
			this.overlapOperator = overlapOperator;
			this.overlapRow = overlapRow;
			this.index = index;
		}
		
		public Integer getSection() {
			return section;
		}
		public void setSection(Integer section) {
			this.section = section;
		}
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getOverlapOperator() {
			return overlapOperator;
		}
		public void setOverlapOperator(String overlapOperator) {
			this.overlapOperator = overlapOperator;
		}
		public String getOverlapRow() {
			return overlapRow;
		}
		public void setOverlapRow(String overlapRow) {
			this.overlapRow = overlapRow;
		}

		public Integer getIndex() {
			return index;
		}

		public void setIndex(Integer index) {
			this.index = index;
		}

		public String toString(){
			return "Section: " + section;
		}
	}

	public static class Mirror {
		private Collection<SectionBlock> sectionBlocks = new ArrayList<SectionBlock>();
		
		public void addSectionBlock(SectionBlock mirrorBlock) {
			sectionBlocks.add(mirrorBlock);
		}

		public Collection<SectionInput> getAllSectionInputs(Collection<SectionInput> sectionInputs) {
			Collection<SectionInput> result = new ArrayList<SectionInput>();
			for (SectionInput sectionInput: sectionInputs) {
				Collection<SectionInput> sectionInputsToBeAdded = getAllSectionInputs(sectionInput);
				// check if they are already there in sectionInputs
				for (SectionInput sectionInput1: sectionInputs) {
					for (SectionInput sectionInput2: sectionInputsToBeAdded) {
						if (sectionInput1.getSection().equals(sectionInput.getSection())) {
							continue;
						}
						if (sectionInput1.getSection().equals(sectionInput2.getSection())) {
							throw new RuntimeException("The section " + sectionInput2.getSection() + " created from section " + sectionInput.getSection() + " is duplicated (check the mirrors)");
						}
					}
				}
				result.addAll(sectionInputsToBeAdded);					
			}
			
			return result;
		}

		// return section inputs created from the mirror rule
		public Collection<SectionInput> getAllSectionInputs(SectionInput sectionInput) {
			Collection<SectionInput> results = new ArrayList<SectionInput>();

			// check if it belongs to a mirror block
			SectionBlock origMirrorBlock = getMirrorBlock(sectionInput);
			if (origMirrorBlock == null) {
				return results;
			}

			Integer offset = origMirrorBlock.getOffsetSection(sectionInput);
			
			String groupId = sectionInput.getGroupId();
			if (groupId == null) {
				groupId = "AUTO_" + sectionInput.getSection();
				sectionInput.setGroupId(groupId);
			}

			// create the mirrored sections
			for (SectionBlock sectionBlock: sectionBlocks) {
				if (sectionBlock != origMirrorBlock) {
					SectionInput mirroredSectionInput = new SectionInput(sectionBlock.getSectionFromOffset(offset),
							 groupId, ">", "M", sectionInput.getIndex());
					results.add(mirroredSectionInput);
				}
			}
			return results;
		}	
		
		public SectionBlock getMirrorBlock(SectionInput sectionInput) {
			for (SectionBlock mirrorBlock: sectionBlocks) {
				if (mirrorBlock.contains(sectionInput.getSection())) {
					return mirrorBlock;
				}
			}
			return null;
		}
	}
	
	public static class ValuationFactor {
		private String venue;
		private Date startDate;
		private Date endDate;
		private String section;
		private String row;
		private Integer factor;
		
		public ValuationFactor(String venue, Date startDate, Date endDate,
				String section, String row, Integer factor) {
			super();
			this.venue = venue;
			this.startDate = startDate;
			this.endDate = endDate;
			this.section = section;
			this.row = row;
			this.factor = factor;
		}
		
		public String getVenue() {
			return venue;
		}
		public void setVenue(String venue) {
			this.venue = venue;
		}
		public Date getStartDate() {
			return startDate;
		}
		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}
		public Date getEndDate() {
			return endDate;
		}
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public String getRow() {
			return row;
		}
		public void setRow(String row) {
			this.row = row;
		}
		public Integer getFactor() {
			return factor;
		}
		public void setFactor(Integer factor) {
			this.factor = factor;
		}
	}
}
