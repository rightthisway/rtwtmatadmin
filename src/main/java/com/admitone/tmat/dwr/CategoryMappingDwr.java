package com.admitone.tmat.dwr;

import java.util.Collection;
import java.util.regex.Pattern;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.SpringUtil;

@RemoteProxy
public class CategoryMappingDwr {
	/*private static Pattern numberPattern = Pattern.compile("^\\d+$");
	private static Pattern letterNumberPattern = Pattern.compile("^(\\[A-Za-z \\.]+)(\\d+)$");
	
	@RemoteMethod
	public Collection<CategoryMapping> getAllCategoryMappings(Integer categoryId) {
		try {
			return DAORegistry.getCategoryMappingDAO().getAllCategoryMappings(categoryId);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RemoteMethod
	public Collection<CategoryMapping> getAllCategoryMappingsFromEventAndCat(Integer eventId, Integer categoryId) {
		try {
			return DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsFromEventAndCat(eventId, categoryId);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RemoteMethod
	public Collection<CategoryMapping> getAllCategoryMappingsFromEventAndScheme(Integer eventId, String catScheme) {
		try {
			return DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsFromEventAndScheme(eventId, catScheme);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@RemoteMethod
	public String addCategoryMapping(Integer eventId, Integer categoryId, String startSection, String endSection,
									 String startRow, String endRow, String startSeat, String endSeat) {
		try {

			if (endSection.length() > 0 && !startSection.equals("*") && numberPattern.matcher(startSection).find() != numberPattern.matcher(endSection).find()) {
				return "KO|start and end sections are not compatible";
			}
			
			if (endRow.length() > 0 && !startRow.equals("*") &&  numberPattern.matcher(startRow).find() != numberPattern.matcher(endRow).find()) {
					return "KO|start and end sections are not compatible";
			}
	
			CategoryMapping mapping = new CategoryMapping(categoryId, eventId, startSection, endSection,
													  startRow, endRow,startSeat,endSeat);
			DAORegistry.getCategoryMappingDAO().save(mapping);
			Event event = DAORegistry.getEventDAO().get(eventId);
			
			Categorizer.update(event.getTourId());
			TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
			msg.setObject(event.getTourId());
			SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}

	@RemoteMethod
	public String updateCategoryMapping(Integer categoryMappingEventId, Integer eventId, Integer categoryId, String startSection, String endSection,
									 String startRow, String endRow, String startSeat, String endSeat) {
		try {
			if (endSection.length() > 0 && !startSection.equals("*") && numberPattern.matcher(startSection).find() != numberPattern.matcher(endSection).find()) {
				return "KO|start and end sections are not compatible";
			}
			
			if (endRow.length() > 0 && !startRow.equals("*") &&  numberPattern.matcher(startRow).find() != numberPattern.matcher(endRow).find()) {
					return "KO|start and end sections are not compatible";
			}

			CategoryMapping mapping = DAORegistry.getCategoryMappingDAO().get(categoryMappingEventId);
			mapping.setStartSection(startSection);
			mapping.setEndSection(endSection);
			mapping.setStartRow(startRow);
			mapping.setEndRow(endRow);
			mapping.setStartSeat(startSeat);
			mapping.setEndSeat(endSeat);
			DAORegistry.getCategoryMappingDAO().update(mapping);
			Event event = DAORegistry.getEventDAO().get(eventId);
			Categorizer.update(event.getTourId());
			TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
			msg.setObject(event.getTourId());
			SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}

	@RemoteMethod
    public String removeCategoryMapping(Integer categoryMappingId) {
    	CategoryMapping mapping = DAORegistry.getCategoryMappingDAO().get(categoryMappingId);
    	DAORegistry.getCategoryMappingDAO().delete(mapping);
		Event event = DAORegistry.getEventDAO().get(mapping.getEventId());
		Categorizer.update(event.getTourId());
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
		msg.setObject(event.getTourId());
		SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);
    	return "OK";
    }

	@RemoteMethod
    public String removeEventCategoryMapping(Integer eventId) {
		System.out.println("Category mapping event id" + eventId);
    	DAORegistry.getCategoryMappingDAO().deleteAllByEvent(eventId);
		Event event = DAORegistry.getEventDAO().get(eventId);;
		Categorizer.update(event.getTourId());
    	return "OK";
    }
	
	@RemoteMethod
    public String removeAllCategoryMapping(Integer tourId, String catScheme) {
    	DAORegistry.getCategoryMappingDAO().deleteAllByTour(tourId, catScheme);
		Categorizer.update(tourId);
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
		msg.setObject(tourId);
		SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);
    	return "OK";
    }*/
}
