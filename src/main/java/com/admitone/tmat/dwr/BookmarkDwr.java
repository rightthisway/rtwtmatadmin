package com.admitone.tmat.dwr;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Bookmark;
import com.admitone.tmat.enums.BookmarkType;

@RemoteProxy
public class BookmarkDwr {
	@RemoteMethod
	public String toggleBookmarkArtist(Integer artistId) {
		return toggleBookmark(BookmarkType.ARTIST, artistId);
	}

	@RemoteMethod
	public String toggleBookmarkTour(Integer tourId) {
		return toggleBookmark(BookmarkType.TOUR, tourId);
	}

	@RemoteMethod
	public String toggleBookmarkEvent(Integer eventId) {
		return toggleBookmark(BookmarkType.EVENT, eventId);
	}

	@RemoteMethod
	public String toggleBookmarkTicket(Integer ticketId) {
		return toggleBookmark(BookmarkType.TICKET, ticketId);
	}

	//private String toggleBookmark(BookmarkType type, String objectId) {
	private String toggleBookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();

//		if (objectId == null) {
//			return "Off";
//		}
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
		if (b != null) {
			DAORegistry.getBookmarkDAO().delete(bookmark);
			return "Off";
		} else {
			DAORegistry.getBookmarkDAO().save(bookmark);			
			return "On";
		}
	}

	//private String unbookmark(BookmarkType type, String objectId) {
		private String unbookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
		if (b != null) {
			DAORegistry.getBookmarkDAO().delete(bookmark);
		}	
		
		return "Off";
	}
	
	//private String bookmark(BookmarkType type, String objectId) {
		private String bookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		try{
		if (DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId) == null) {
			DAORegistry.getBookmarkDAO().save(bookmark);
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return "On";
	}
}
