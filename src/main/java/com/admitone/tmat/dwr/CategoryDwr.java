package com.admitone.tmat.dwr;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategorySynonym;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.SectionRowStripper;
import com.admitone.tmat.utils.SpringUtil;

@RemoteProxy
public final class CategoryDwr {
	/*
	private static final Pattern numberPattern = Pattern.compile("^\\d+$");
	
	@RemoteMethod
	public Collection<Category> getCategories(Integer tourId, String catScheme) {
		try{
			return DAORegistry.getCategoryDAO().getAllCategories(tourId, catScheme);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RemoteMethod
	public Collection<CategorySynonym> getAliases(Integer catId) {
		try{
			return DAORegistry.getCategorySynonymDAO().getSynonyms(catId);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RemoteMethod
	public String removeAlias(Integer synId) {
		try {
			CategorySynonym catSyn = DAORegistry.getCategorySynonymDAO().get(synId);
			DAORegistry.getCategorySynonymDAO().delete(catSyn);
		} catch (Exception e) {
			e.printStackTrace();
			return "KO";
		}
		return "OK";		
	}
	
	@RemoteMethod
	public String addCategory(Integer tourId, String symbol, String description, String equivalency, String catScheme, String catQty) {
		try {
			Category testCategory = DAORegistry.getCategoryDAO().getCategoryBySymbol(tourId, symbol, catScheme);
			if (testCategory != null) {
				return "Category already exists";
			}
			Category category = new Category(tourId, symbol, description, equivalency, catScheme);
			if(catQty != null && !catQty.isEmpty()) {
				category.setSeatQuantity(Integer.parseInt(catQty));
			}
			DAORegistry.getCategoryDAO().save(category);
		} catch (Exception e) {
			e.printStackTrace();
			return "KO";
		}
		return "OK";
	}
	
	@RemoteMethod
	public String addAlias(Integer catId, String startSyn, String endSyn) {
		try {
			Category testCategory = DAORegistry.getCategoryDAO().get(catId);
			if (testCategory == null) {
				return "Category does not exist";
			}
			
			// if start section and end section cannot be compared
			// (e.g., a number and string with letters) => error
			if (endSyn.length() > 0
					&& !startSyn.equals("*")
					&& numberPattern.matcher(startSyn).find() != numberPattern
							.matcher(endSyn).find()) {
				return "Cannot Compare Start and End Aliases";
			}

			Collection<CategorySynonym> syns = DAORegistry.getCategorySynonymDAO().getSynonyms(catId);
			if(syns != null){
				for(CategorySynonym syn : syns){
					if(syn.getStartSynonym().equalsIgnoreCase(startSyn)){
						return "Duplicate Alias";
					}
				}
			}
			
			CategorySynonym catSyn = new CategorySynonym();

			catSyn.setCatId(catId);
			catSyn.setStartSynonym(startSyn);
			if(endSyn != null){
				catSyn.setEndSynonym(endSyn);
			}
			
			DAORegistry.getCategorySynonymDAO().save(catSyn);
		} catch (Exception e) {
			e.printStackTrace();
			return "FAILED, CHECK LOGS";
		}
		return "OK";
	}
	
	@RemoteMethod
	public String editCategory(Integer categoryId, Integer tourId, String symbol, String description, String equivalency, String catScheme, String catQty) {
		try {
			Category testCategory = DAORegistry.getCategoryDAO().getCategoryBySymbol(tourId, symbol, catScheme);
			if (testCategory != null && !testCategory.getId().equals(categoryId)) {
				return "Category already exists " + testCategory.getId() + " " + categoryId;
			}
			Category category = DAORegistry.getCategoryDAO().get(categoryId);
			category.setSymbol(symbol);
			category.setDescription(description);
			category.setEqualCats(equivalency);
			if(catQty != null && !catQty.isEmpty()) {
				category.setSeatQuantity(Integer.parseInt(catQty));
			}
			DAORegistry.getCategoryDAO().update(category);
		} catch (Exception e) {
			e.printStackTrace();
			return "KO";
		}
		return "OK";
	}

	@RemoteMethod
	public String removeCategory(Integer categoryId) {
		try {						
			Category category = DAORegistry.getCategoryDAO().get(categoryId);
			DAORegistry.getCategoryDAO().delete(category);
			Categorizer.update(category.getTourId());
			TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
			msg.setObject(category.getTourId());
			SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);

		} catch (Exception e) {
			e.printStackTrace();
			return "KO";
		}
		return "OK";		
	}
	
	@RemoteMethod
	public String removeCategories(Integer tourId, String catScheme) {
		for (Category category: DAORegistry.getCategoryDAO().getAllCategories(tourId, catScheme)) {
			removeCategory(category.getId());
		}
		Categorizer.update(tourId);
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION);
		msg.setObject(tourId);
		SpringUtil.getTicketListingCrawler().getTicketListingCrawlDispatcher().dispatch(msg);
		
		return "OK";
	}
	
	@RemoteMethod
	public String getCategory(Integer eventId, String section, String row,String seat, String catScheme) {
		Event event = DAORegistry.getEventDAO().get(eventId);
		TourType type = event.getEventType(); 
		section = SectionRowStripper.strip(type, section);
		Integer categoryId = Categorizer.computeCategory(eventId, section, row,seat, catScheme);
		if (categoryId == null) {
			return null;
		}
		
		Category category = DAORegistry.getCategoryDAO().get(categoryId);
		return "normalized section: " + section + "\nnormalized row: " + row +  "\nnormalized seat: " + seat + "\nCategory: " + category.getSymbol();
	}
	
	
	@RemoteMethod
	public String categoryUpdateRequestToZone(Integer tourId){
		
		try{
			String postbackurl = DAORegistry.getPropertyDAO().get("CATEGORY_UPDATE_POSTBACK").getValue();
			//postbackurl = "http://192.168.0.21:8080/zoneplatform-1/zp/WSPostTickets";
			System.out.println("The postbackurl for category update is: " + postbackurl);
			HttpClient hc = new DefaultHttpClient();

			HttpPost hp = new HttpPost(postbackurl);
			NameValuePair nameValuePair = new BasicNameValuePair("tourId", tourId.toString());
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(nameValuePair);
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
			hp.setEntity(entity);
			HttpResponse response = hc.execute(hp);
			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer();
			String line = "";
			String lineSeparator = System.getProperty("line.separator");
			while((line=in.readLine()) != null){
				sb.append(line + lineSeparator);
			}
			String responseString = sb.toString();
			System.out.println("The response string is: " + responseString);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "OK";
	}*/
}