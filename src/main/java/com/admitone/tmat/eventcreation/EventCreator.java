package com.admitone.tmat.eventcreation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.context.SecurityContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.EventSynonym;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.ManageBrokerAudit;
import com.admitone.tmat.data.PresaleEvent;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentEvent;
import com.admitone.tmat.data.VenueCategory;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.EventInfo;
import com.admitone.tmat.utils.TextUtil;

public final class EventCreator {
	private final static Logger logger = LoggerFactory.getLogger(EventCreator.class);
	
	public static Event getSimilarEvent(EventInfo eventInfo, Collection<Event> events, Map<Integer, Venue> venueMap) {
		Collection<Object[]> result = new ArrayList<Object[]>();
		if (eventInfo.getDate() == null) {
			return null;
		}
		for (Event event: events) {
			if (event.getLocalDate() == null || eventInfo.getDate() == null) {
				continue;
			}
			
			if (!event.getLocalDate().equals(eventInfo.getDate())) {
				continue;
			}
			
			Venue venue = venueMap.get(event.getVenueId());
			String eventLocation = venue.getBuilding() + " " + venue.getCity() + " " + venue.getState() + " " + venue.getCountry();
			String eventInfoLocation = eventInfo.getBuilding() + " " + eventInfo.getCity() + " " + eventInfo.getState();// + " " + eventInfo.getCountry();
			Integer eventNameSimilarity = TextUtil.computeSimilarity(eventInfo.getName(), event.getName());
			Integer locationSimilarity = TextUtil.computeSimilarity(eventLocation,eventInfoLocation);
			boolean artistSimilarity = eventInfo.getArtistName().equalsIgnoreCase(event.getArtist().getName());

			if (eventNameSimilarity == 0 || locationSimilarity == 0 || !artistSimilarity) {
				continue;
			}
			
			// TODO: optimize this
			if (!TextUtil.isSimilar(eventInfo.getName(), event.getName())
				|| !TextUtil.isSimilar(venue.getBuilding(), eventLocation)) {
				continue;
			}

			// both are TBD, most likely candidate (+5)
			if (event.getLocalTime() == null && eventInfo.getTime() == null) {
				result.add(new Object[] {eventNameSimilarity + locationSimilarity + 5, event});
			} else if (event.getLocalTime() == null || eventInfo.getTime() == null) {
				// one event is TBD, the other is set => potential candidate (+0)
				result.add(new Object[] {eventNameSimilarity + locationSimilarity, event});
			} else {
				Calendar hiCal = new GregorianCalendar();
				hiCal.setTime(event.getLocalTime());
	
				Calendar resCal = new GregorianCalendar();
				resCal.setTime(eventInfo.getTime());
	
				if (hiCal.get(Calendar.HOUR_OF_DAY) != resCal.get(Calendar.HOUR_OF_DAY)
						|| hiCal.get(Calendar.MINUTE) != resCal.get(Calendar.MINUTE)) {
						continue;
				}

				// exactly same date (+5)
				result.add(new Object[] {eventNameSimilarity + locationSimilarity + 5, event});
			}
		}
		
		if (result.size() == 0) {
			return null;
		}
		
		Object[] best = result.iterator().next();
		for (Object[] obj: result) {
			if ((Integer)obj[0] > (Integer)best[0]) {
				best = obj;
			}
		}
		
		return (Event)best[1];
	}
	
	public synchronized static void createEvents(Collection<AutoEventListingResult> eventInfos,CreationType creationType) {
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
//		Map<String, Tour> tourMap = new HashMap<String, Tour>();
		Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();
		Map<Integer, Venue> venueIdMap = new HashMap<Integer, Venue>();
		Map<String, Artist> artistMap = new HashMap<String, Artist>();

		// hash table to contain eventName + eventDate + venue => event
		for (Event event : DAORegistry.getEventDAO().getAllEventByAdmitoneId()) {
			eventMap.put(event.getAdmitoneId(), event);										
		}

		// hash table to contain tourName => tour
		for (Artist artist : DAORegistry.getArtistDAO().getAllArtistsWithGrandChildBySqlQuery()) {
			artistMap.put(artist.getName().toLowerCase().trim(), artist);
		}

		// hash table to contain venueName => venue
		for (Venue venue : DAORegistry.getVenueDAO().getAll()) {
			venueMap.put(venue.getTnVenueId(), venue);
			venueIdMap.put(venue.getId(), venue);
		}

		// hash table to contain artistName => artist
		GrandChildTourCategory grandChildtourCategory = null;
		GrandChildTourCategory otherGrandChildtourCategory = null;
		GrandChildTourCategory sportsDefaultGrandChildtourCategory = null;
		GrandChildTourCategory concertDefaultGrandChildtourCategory = null;
		GrandChildTourCategory theaterDefaultGrandChildtourCategory = null;
		for (AutoEventListingResult eventInfo: eventInfos){
			if (eventInfo.isCreated()) {
				eventInfo.setMessage("Event already exists.");
				continue;
			}
			//If this event has been merged it should not be added
			EventSynonym eventMerged = DAORegistry.getEventSynonymDAO().getEventSynonymByMerged(eventInfo.getEventId());

			if(eventMerged != null){
				eventInfo.setEventId(eventMerged.getEventId());
			}
			Event event = null;
			
			if (eventInfo.getEventId() != null) {
				event = eventMap.get(eventInfo.getEventId());
			}
			Artist artist = null;
//			boolean isNewEvent = false;
			Venue venue=null;
			
			String userName = "AUTO";
			try{
				userName = SecurityContextHolder.getContext().getAuthentication().getName();
			}catch (Exception e) {
				
			}
	         Date now = new Date();
	         
			if (event == null) {
//				isNewEvent = true;
				event = new Event();
				event.setId(DAORegistry.getEventDAO().getUnusedId());
				
				event.setEventStatus(EventStatus.ACTIVE);
				event.setEventType(eventInfo.getTourType());
				
				event.setAutoCorrect(true);
				event.setVenueCategory(eventInfo.getVenueCategory());
				Event similarEvent = getSimilarEvent(eventInfo, eventMap.values(), venueIdMap);

				if (similarEvent != null) {
					event.setId(similarEvent.getId());
					eventInfo.setMessage("Event already exists (similar event).");
					eventInfo.setEventId(similarEvent.getId());
					eventInfo.setSystemEventId(similarEvent.getId());
				} else {
					artist = artistMap.get(eventInfo.getArtistName().toLowerCase().trim());
					if(artist == null){
						artist = new Artist();
						artist.setName(eventInfo.getArtistName());
						artistMap.put(eventInfo.getArtistName().toLowerCase(), artist);
						TourType tourType = eventInfo.getTourType();
						
						if(tourType!=null){
							if(tourType.equals(TourType.SPORT)){
								if(sportsDefaultGrandChildtourCategory==null){
									sportsDefaultGrandChildtourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(89);
								}
								grandChildtourCategory = sportsDefaultGrandChildtourCategory;
							}else if(tourType.equals(TourType.CONCERT)){
								if(concertDefaultGrandChildtourCategory==null){
									concertDefaultGrandChildtourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(72);
								}
								grandChildtourCategory = concertDefaultGrandChildtourCategory;
							}else if(tourType.equals(TourType.THEATER)){
								if(theaterDefaultGrandChildtourCategory==null){
									theaterDefaultGrandChildtourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(48);
								}
								grandChildtourCategory = theaterDefaultGrandChildtourCategory;
							}else{
								if(otherGrandChildtourCategory==null){
									otherGrandChildtourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(51);
								}
								grandChildtourCategory = otherGrandChildtourCategory;	
							}
							
						}else if(otherGrandChildtourCategory==null){
							otherGrandChildtourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(51);
							grandChildtourCategory = otherGrandChildtourCategory;
						}
						artist.setGrandChildTourCategoryId(grandChildtourCategory.getId());
						DAORegistry.getArtistDAO().save(artist);
						eventInfo.setArtistId(artist.getId());
					}
					
					event.setArtistId(artist.getId());
					event.setArtist(artist);
					String venueString = eventInfo.getBuilding().trim();
					String tempBefore[] = venueString.split("\\(");
					if(tempBefore.length==2){
						String tempAfter[]=venueString.split("\\)");
						if(tempAfter.length==1){
								venueString=tempBefore[0];// + " " + tempAfter[0];
						}else{
							venueString=tempBefore[0] + " " + tempAfter[1];;
						}
					}
//					String venueCompleteString = venueString.toLowerCase() .trim() +  "," + eventInfo.getCity().toLowerCase().trim() + "," + eventInfo.getState().toLowerCase().trim();
					venue = venueMap.get(eventInfo.getOriginalVenueId());
					if (venue == null) {
						venue = new Venue();
						// check if there is a similar venue
						venue.setBuilding(venueString);
						venue.setCity(eventInfo.getCity());
						venue.setState(eventInfo.getState());
						venue.setCountry(eventInfo.getCountry());
						venue.setVenueType(eventInfo.getTourType());
						venue.setZipcode(eventInfo.getZipcode());
						DAORegistry.getVenueDAO().saveOrUpdate(venue);
						event.setEventType(venue.getVenueType());
						venueMap.put(eventInfo.getVenueId(), venue);
//						venueMap.put(eventInfo.getBuilding().toLowerCase().trim() + "," + eventInfo.getCity().toLowerCase().trim() + "," + eventInfo.getState().trim(), venue);
					}
					//to set event type to venue type if venue type is there
					else{
						if(venue.getVenueType() != null){
							event.setEventType(venue.getVenueType());
						}
					}
//					if(!venue.getCity().equals(eventInfo.getCity()) && !venue.getState().equals(eventInfo.getState())){
					venueIdMap.put(venue.getId(), venue);
					event.setLocalDate(eventInfo.getDate());
					event.setAdmitoneId(eventInfo.getEventId());
					event.setLocalTime(eventInfo.getTime());
					event.setName(TextUtil.removeExtraWhitespaces(eventInfo.getName()));
//					event.setTourId(tour.getId());
					event.setLastUpdate(new Date());
					logger.info(event.getLastUpdate() + ":" + event.getId() +":" + artist.getId() + ": from merge createEvents in EventCreator");
					if(null != eventInfo.getVenueCategory()){
						event.setVenueCategoryId(eventInfo.getVenueCategory().getId());
					}	
					event.setVenueId(venue.getId());
					event.setCreationType(creationType);
					eventInfo.setSystemEventId(event.getId());
					
					
					String tourType =artist.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName();
					//System.out.println(Tourtype);
					/*if ( tourType.equalsIgnoreCase("Sports"))
					{
						event.setBrokerId(2);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourType.equalsIgnoreCase("Concerts"))
					{
						event.setBrokerId(2);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourType.equalsIgnoreCase("Theater"))
					{
						event.setBrokerId(3);
						event.setBrokerStatus("ACTIVE");
					}
					else if ( tourType.equalsIgnoreCase("Other"))
					{
						event.setBrokerId(3);
						event.setBrokerStatus("ACTIVE");
					}*/
					
					event.setPresaleEvent(Boolean.FALSE);
					if(tourType.equalsIgnoreCase("Concerts")){
						if(event.getVenueCategoryId()==null){
							
							//VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venue.getId(), "END_STAGE");
							VenueCategory venueCategory = null;
							//Tamil 09/18/2018 : for concert events csv order is END_STAGE,CENTER_STAGE,GA if exists any one then assign it
							//Tamil 04/27/2018 : for concert events csv order is CSNTER_STAGE,END_STAGE,GA if exists any one then assign it 
							//Tamil : if END_STAGE csv is exists then assign it else get PRESALE_GA
							List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesForPresaleEvent(venue.getId());
							if(venueCategories != null && !venueCategories.isEmpty() && venueCategories.size() > 0) {
								Map<String,VenueCategory> veneCategoryMap = new HashMap<String, VenueCategory>();
								for (VenueCategory tempVenueCategory : venueCategories) {
									veneCategoryMap.put(tempVenueCategory.getCategoryGroup().toUpperCase(), tempVenueCategory);
								}
								venueCategory = veneCategoryMap.get("END_STAGE");
								if(venueCategory == null) {
									venueCategory = veneCategoryMap.get("CENTER_STAGE");
								}
								if(venueCategory == null) {
									venueCategory = veneCategoryMap.get("GA");
								}
								/*if(venueCategory == null) {
									venueCategory = veneCategoryMap;
								}*/
							}
							if(venueCategory!=null){
								event.setVenueCategoryId(venueCategory.getId());
								//event.setPresaleEvent(Boolean.TRUE);
							}
						}
					}else if ( tourType.equalsIgnoreCase("Theater")){
						if(event.getVenueCategoryId()==null){
							List<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venue.getId());
							if(venueCategories!=null && venueCategories.size()==1){
								event.setVenueCategoryId((venueCategories.get(0)).getId());
							}
						}	
					}
					
					DAORegistry.getEventDAO().saveOrUpdate(event);
					
					//ManageBrokerAudit manageBrokerAudit = new ManageBrokerAudit();
					//manageBrokerAudit.setEventId(event.getId());
					//manageBrokerAudit.setBrokerId(event.getBrokerId());
					//manageBrokerAudit.setBrokerStatus(event.getBrokerStatus());
					//manageBrokerAudit.setUserName(userName);
					//manageBrokerAudit.setCreatedDate(now);
					//manageBrokerAudit.setAction("Auto Created");
					//DAORegistry.getManageBrokerAuditDAO().save(manageBrokerAudit);
					
					
					//Tamil 04/27/2018 Removed Presale Logic
					/*if(event.getPresaleEvent()) {
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.DATE, 7);
						cal.set(Calendar.HOUR_OF_DAY, 0);
						cal.set(Calendar.MINUTE, 0);
						cal.set(Calendar.SECOND, 0);
						Date applyTime = new Date(cal.getTimeInMillis());
						
						PresaleEvent presaleEvent = new PresaleEvent();
						presaleEvent.setEvent(event);
						presaleEvent.setApplyDate(applyTime);
						presaleEvent.setApplyTime(applyTime);
						presaleEvent.setStatus("ACTIVE");
						presaleEvent.setCreatedBy("AUTO");
						presaleEvent.setCreatedDate(event.getCreationDate());
						presaleEvent.setLastUpdatedBy("AUTO");
						presaleEvent.setLastUpdated(event.getCreationDate());
						DAORegistry.getPresaleEventDAO().save(presaleEvent);
					}*/
					
					
					EventAudit ea=new EventAudit();
					ea.setEventId(event.getId());
					ea.setUserName(userName);
					ea.setAction("Auto Create");
					ea.setModifiedDate(now);
					ea.setEventName(event.getName());
					/*ea.setTourId(event.getTourId());
					ea.setTourName(event.getTour().getName());
					ea.setArtistId(event.getTour().getArtistId());
					ea.setArtistName(event.getTour().getArtist().getName());*/
					ea.setArtistId(event.getArtistId());
					ea.setArtistName(event.getArtist().getName());
					ea.setEventDate(event.getLocalDate());
					ea.setEventTime(event.getLocalTime());
					ea.setVenueId(event.getVenueId());
					ea.setVenueName(event.getVenue().getBuilding());
					ea.setPresaleEvent(event.getPresaleEvent());
					if(event.getVenueCategoryId()!= null)
					{
						ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
					}
					else
					{
					ea.setVenueCategory(null);
					}
					if(event.getEventType()!=null && event.getEventType().name()!= null){
						ea.setEventType(event.getEventType().name());
					}
					else{
						ea.setEventType(null);
					}
					if(event.getNoPrice()!= null){
						ea.setNoPrice("YES");
					}
					else{
						ea.setNoPrice("NO");
					}
					if(event.getAdmitoneId()!= null){
						ea.setAdmitoneId(event.getAdmitoneId());
					}
					else{
						ea.setAdmitoneId(null);
					}
					DAORegistry.getEventAuditDAO().save(ea);
					Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
					if(property!=null){
						String dependents = property.getValue();
						if(dependents!= null && !dependents.isEmpty()){
							String dependentList[] = dependents.split(",");
							List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
							for(String dependent:dependentList){
								if(dependent == null || dependent.isEmpty()){
									continue;
								}
								TMATDependentEvent dependentEvent = new TMATDependentEvent();
								dependentEvent.setEventId(event.getId());
								dependentEvent.setDependent(dependent);
								dependentEvent.setAdd(true);
								tmatDependents.add(dependentEvent);
							}
							if(!tmatDependents.isEmpty()){
								DAORegistry.getTmatDependentEventDAO().saveAll(tmatDependents);
							}
						}
					}
//					eventInfo.setExists(true);
					eventInfo.setMessage("Event created with ID=" + event.getId());
					eventMap.put(event.getId(), event);
				}
				
				
				
			}else{
				eventInfo.setMessage("Event already exists (same id).");
				artist = event.getArtist();
				artistMap.remove(artist.getName());
				artistMap.put(eventInfo.getArtistName().toLowerCase(), artist);
				eventInfo.setEventId(event.getId());
			}

//			String =eventInfo.getBuilding().toLowerCase().trim() +
			
			
			
			
			eventInfo.setCreated(true);
			eventInfo.setSuccess(true);
//			eventInfo.setTourId(tour.getId());
			eventInfo.setArtistId(event.getArtistId());
			
			// copy category mappings
			/*if (isNewEvent) {
				Event sameVenueEvent = DAORegistry.getEventDAO().getEventWithSameTourAndVenueButDifferentFromEvent(tour.getId(), venue.getId(), event.getId());
				if (sameVenueEvent != null) {
					DAORegistry.getCategoryMappingDAO().copyCategoryMappings(sameVenueEvent.getId(), event.getId());
				}
			}*/
			
		}
	}
}
