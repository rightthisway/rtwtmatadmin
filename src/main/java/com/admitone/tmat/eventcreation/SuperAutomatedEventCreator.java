package com.admitone.tmat.eventcreation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.web.Constants;

public class SuperAutomatedEventCreator implements InitializingBean {
	private Logger logger = LoggerFactory.getLogger(SuperAutomatedEventCreatorThread.class);
	private CreationEventListManager creationEventListManager;
	private Collection<AutoEventListingResult> errorEvents = new ArrayList<AutoEventListingResult>();
	private Collection<AutoEventListingResult> createdEvents = new ArrayList<AutoEventListingResult>();
	private Date startDate;
	private int eventToBeCreatedCount;
	private int eventToBeUpdatedCount;
	private SuperAutomatedEventCreatorThread thread;
	private boolean running = false;
	private String status;
	private Integer startHour;
	private Integer stopHour;
	private Timer timer = new Timer();

	public int getEventToBeUpdatedCount() {
		return eventToBeUpdatedCount;
	}

	public void setEventToBeUpdatedCount(int eventToBeUpdatedCount) {
		this.eventToBeUpdatedCount = eventToBeUpdatedCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public void setEventToBeCreatedCount(int eventToBeCreatedCount) {
		this.eventToBeCreatedCount = eventToBeCreatedCount;
	}

	public void resetProperties() {
		Property startHourProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.startHour");
		Property stopHourProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.stopHour");
		Property modeProperty = DAORegistry.getPropertyDAO().get("eventcreation.auto.mode");
	
		timer.cancel();
		timer = new Timer();
		
		if (modeProperty.getValue().equals("ALWAYS")) {
			start();
		} else if (modeProperty.getValue().equals("NONE")) {
			stop();
		} else {
			startHour = Integer.valueOf(startHourProperty.getValue());
			stopHour = Integer.valueOf(stopHourProperty.getValue());
			
			Calendar cal = new GregorianCalendar();
			int hour = cal.get(Calendar.HOUR_OF_DAY);

			Calendar startCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE),
															  startHour, 0, 0);

			Calendar stopCal = new GregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE),
					  								 stopHour, 0, 0);

			if (startHour < stopHour) {
				if (startHour <= hour && hour < stopHour) {
					start();
					startCal.add(Calendar.DATE, 1);
				} else {
					stop();
					stopCal.add(Calendar.DATE, 1);
				}
			} else {
				if (stopHour <= hour && hour < startHour) {
					stop();
					stopCal.add(Calendar.DATE, 1);					
				} else {
					start();
					startCal.add(Calendar.DATE, 1);					
				}				
			}
	
			timer.scheduleAtFixedRate(new StartCreatorTask(this), startCal.getTime(), 24L * 3600L * 1000L);
			timer.scheduleAtFixedRate(new StopCreatorTask(this), stopCal.getTime(), 24L * 3600L * 1000L);
			
		}
	}
	
	
	
	public void afterPropertiesSet() throws Exception {
		if (creationEventListManager == null) {
			throw new RuntimeException("The property creationEventListManager must be set");
		}
		
		// dirty hack to wait that the property dao is ready
		// on spring the depends-on doesn't seem to work properly
		while (DAORegistry.getPropertyDAO() == null) {
			Thread.sleep(1000);
		}

		resetProperties();
	}

	public Collection<AutoEventListingResult> getCreatedEvents() {
		return createdEvents;
	}

	public Collection<AutoEventListingResult> getErrorEvents() {
		return errorEvents;
	}
	
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public synchronized void start() {
		if (running) {
			return;
		}

		thread = new SuperAutomatedEventCreatorThread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop() {
		if (!running) {
			return;
		}
		
		thread.cancel();
		running = false;
	}

	public CreationEventListManager getCreationEventListManager() {
		return creationEventListManager;
	}

	public void setCreationEventListManager(
			CreationEventListManager creationEventListManager) {
		this.creationEventListManager = creationEventListManager;
	}

	public static class SuperAutomatedEventCreatorThread extends Thread {
		private final int UPDATE_NUM_DAYS = 14;
		private boolean running = true;
		private SuperAutomatedEventCreator superAutomatedEventCreator;
		private Logger logger = LoggerFactory.getLogger(SuperAutomatedEventCreatorThread.class);
		private String[] siteIds;
		
		public SuperAutomatedEventCreatorThread(SuperAutomatedEventCreator creator) {
			this.superAutomatedEventCreator = creator;
			
			Constants constants = new Constants();
			siteIds = new String[constants.getSiteIds().length - constants.getDisabledSiteIds().length];
			String disabledString = constants.getDisabledSiteIdsString();
			int i = 0;
			for (String siteId: constants.getSiteIds()) {
				if (!disabledString.contains("[" + siteId + "]")) {
					siteIds[i++] = siteId;
				}
			}
		}
		
		public synchronized void cancel() {
			if (running) {
				running = false;
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.interrupt();
		}

		private void createEvents(Collection<CreationEventHit> eventHits, boolean creation) {
			for (CreationEventHit eventHit: eventHits) {
				if (running == false) {
					break;
				}
				try {
					String[] filteredSiteIds = new String[siteIds.length - eventHit.getSiteCount()];
					int i = 0;
					for (String siteId: siteIds) {
						if (eventHit.getSites() == null || !eventHit.getSites().contains(siteId)) {
							filteredSiteIds[i++] = siteId;
						}
					}
					
					AutoEventListingResult result = superAutomatedEventCreator.getCreationEventListManager().getEventListingResults(eventHit.getEventId(), filteredSiteIds,/*new String[0],*/true,true,true,true,true,true,null,null);
					if (result.isExists()) {
						continue;
					}
					if (result.isEnabled()) {
						Collection<AutoEventListingResult> results = new ArrayList<AutoEventListingResult>();
						results.add(result);
						superAutomatedEventCreator.getCreationEventListManager().createEventsFromAutoResults("admin", results,true,true,true,true,true,true,CreationType.AUTO);
						logger.info("Event " + result.getName() + " " + result.getDate() + " created.");
						superAutomatedEventCreator.getCreatedEvents().add(result);
					} else {
						superAutomatedEventCreator.getErrorEvents().add(result);
					}
					Thread.sleep(1000);
				} catch (Exception e) {
					logger.warn("Error during creation of event " + eventHit.getName() + " " + eventHit.getDate(), e);
					try {
						Thread.sleep(10000);
					} catch (InterruptedException ie) {}
				} finally {
					if (creation) {
						superAutomatedEventCreator.setEventToBeCreatedCount(superAutomatedEventCreator.getEventToBeCreatedCount() - 1);
					} else {
						superAutomatedEventCreator.setEventToBeUpdatedCount(superAutomatedEventCreator.getEventToBeUpdatedCount() - 1);
					}
				}
			}			
		}
	
		public void run() {
			superAutomatedEventCreator.getErrorEvents().clear();
			superAutomatedEventCreator.getCreatedEvents().clear();
			superAutomatedEventCreator.setStartDate(new Date());
			
			while(running) {
				try {
					Collection<CreationEventHit> eventHits = new ArrayList<CreationEventHit>(superAutomatedEventCreator.getCreationEventListManager().getAllUncreatedEventHits());
					superAutomatedEventCreator.setEventToBeCreatedCount(eventHits.size());
					superAutomatedEventCreator.setStatus("CREATION");
					createEvents(eventHits, true);
					
					eventHits = new ArrayList<CreationEventHit>(superAutomatedEventCreator.getCreationEventListManager().getAllCreatedEventHits(UPDATE_NUM_DAYS));
					superAutomatedEventCreator.setStatus("UPDATE");
					superAutomatedEventCreator.setEventToBeUpdatedCount(eventHits.size());
					createEvents(eventHits, false);
					
					superAutomatedEventCreator.setStatus("IDLE");
					Thread.sleep(60000L);
				} catch (Exception e) {
					logger.warn("Error while auto creating events", e);
					try {
						Thread.sleep(30000L);
					} catch (InterruptedException ie) {}
				}
			}
			logger.info("Super Automated Event Creator Thread stopped");
		}
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getEventCreatedCount() {
		return createdEvents.size();
	}

	public int getErrorEventCount() {
		return errorEvents.size();
	}
	
	public int getEventToBeCreatedCount() {
		return eventToBeCreatedCount;
	}

	public static class StartCreatorTask extends TimerTask {
		private Date lastUpdate = null;
		private SuperAutomatedEventCreator superAutomatedEventCreator;
		
		public StartCreatorTask(SuperAutomatedEventCreator superAutomatedEventCreator) {
			this.superAutomatedEventCreator = superAutomatedEventCreator;
		}
		
		public void run() {
			Date now = new Date();
			if (lastUpdate != null && now.getTime() - lastUpdate.getTime() < 30000L) {
				return;
			}
			lastUpdate = now;
			superAutomatedEventCreator.start();
		}		
	}
	
	public static class StopCreatorTask extends TimerTask {
		private Date lastUpdate = null;
		private SuperAutomatedEventCreator superAutomatedEventCreator;
		
		public StopCreatorTask(SuperAutomatedEventCreator superAutomatedEventCreator) {
			this.superAutomatedEventCreator = superAutomatedEventCreator;
		}
		
		public void run() {
			Date now = new Date();
			if (lastUpdate != null && now.getTime() - lastUpdate.getTime() < 30000L) {
				return;
			}
			lastUpdate = now;
			superAutomatedEventCreator.stop();
		}
	}	
}