package com.admitone.tmat.eventcreation;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.TNDataFeedEvent;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.eventfetcher.EventHit;
import com.admitone.tmat.eventfetcher.EventListingManager;
import com.admitone.tmat.eventfetcher.EventListingResult;
import com.admitone.tmat.utils.EiMarketPlaceEventPopulator;
import com.admitone.tmat.utils.EventInfo;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.SeatWaveEventPopulator;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkWebServiceClient;
import com.admitone.tmat.web.Constants;
import com.admitone.tmat.web.Constants.State;
import com.opencsv.CSVReader;

public class CreationEventListManager implements InitializingBean {
	private static Logger logger = LoggerFactory.getLogger(CreationEventListManager.class); 
	private EventListingManager eventListingManager;
//	private TicketListingCrawler ticketListingCrawler;

	private static Collection<CreationEventHit> seatwaveEventHits = new ArrayList<CreationEventHit>();
	private static Collection<CreationEventHit> stubhubEventHits = new ArrayList<CreationEventHit>();
	private static Collection<CreationEventHit> ticketNetworkEventHits = new ArrayList<CreationEventHit>();
	private static Collection<CreationEventHit> ticketNetworkDataFeedEventHits = new ArrayList<CreationEventHit>();
	private static Collection<CreationEventHit> ticketNetworkPOSEventHits = new ArrayList<CreationEventHit>();
	private static Collection<CreationEventHit> existingEventHits = new ArrayList<CreationEventHit>();
	private TicketNetworkWebServiceClient ticketNetworkWebServiceClient = null;
	private static Map<Integer, String> countries = null;
	private static Map<Integer, Map<Integer, String>> countryStates = null;
	Map<Integer, Collection<String>> sitesPerEventMap = new HashMap<Integer, Collection<String>>();
	
	public final static Long TIME_MATCH_TOLERANCE = 90L * 60L * 1000L; // 1h30
	
	public final static Long REFRESH_RATE = 1L * 3600L * 1000L; // 1 hours
	public final static Long CACHE_TIMEOUT = 3600L * 1000L; // 1 hour
	
	public final static Integer OFFSET_STUBHUB_EVENT_ID = 100000000;
	public final static Integer OFFSET_SEATWAVE_EVENT_ID = 200000000;
	public final static Integer OFFSET_TICKETNETWORK_EVENT_ID = 300000000;
	private Timer timer = new Timer();
	private boolean refreshing = false;
	private static Pattern teamAtPattern = Pattern.compile("(.*) at (.*)");
	
	public final Integer MAX_CONCURRENT_SEARCH = 3;
	
	// small cache
	private Map<Integer, AutoEventListingResult> autoEventListingResultMap = new HashMap<Integer, AutoEventListingResult>();
	
	private SharedProperty sharedProperty;
	
	public CreationEventListManager() {
	}
	
	private static Collection<CreationEventHit> getExistingEventList(Collection<Event> events, Map<Integer, Venue> venueIdMap,
			Map<Integer, Artist> artistIdMap, /*Map<Integer, Tour> tourIdMap,*/
			Map<Integer, Collection<String>> sitesPerEventMap) {
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
		Date now = new Date();
		for (Event event: events) {
			// TBD issue.. Need to discuss with business
			if (event.getLocalDate()==null || event.getLocalDate().before(now)) {
				continue;
			}
			/*Tour tour = tourIdMap.get(event.getTourId());
			if(tour==null){
				continue;
			}*/
			Artist artist = artistIdMap.get(event.getArtistId());
			if(artist==null){
				continue;
			}
			Venue venue = venueIdMap.get(event.getVenueId());
			CreationEventHit eventHit = new CreationEventHit(event.getId(), /*event.getId(),*/ artist.getId(), venue.getId(), event.getName(),
					/*event.getName(),*/ artist.getName(), new java.sql.Date(event.getLocalDate().getTime()), event.getLocalTime(),
					artist.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getTourType(), venue.getBuilding(), venue.getCity(), venue.getState(), venue.getCountry(),venue.getZipcode(), "BrowseTickets?eventId=" + event.getId());
			eventHit.setOriginalId(null);
			eventHit.setSites(sitesPerEventMap.get(event.getId()));
			eventHit.setLastUpdate(event.getLastUpdate());
			eventHit.setExists(true);
			result.add(eventHit);
		}
		
		return result;
	}

	public static Collection<CreationEventHit> getPOSEventList(Map<Integer,Event> eventMap) {
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
		Date now = new Date();
		Collection<AdmitoneEvent> events = DAORegistry.getAdmitoneEventDAO().getAll();
		for (AdmitoneEvent event: events) {
			if(eventMap.containsKey(event.getEventId())){
				continue;
			}
			
			if (event.getEventDate()==null || event.getEventDate().before(now)) {
				continue;
			}
			
			TourType tourType = null;
			if(event.getParentCategory().equals("SPORTS")){
				tourType=TourType.SPORT;
			}else if(event.getParentCategory().equals("CONCERTS")){
				tourType=TourType.CONCERT;
			}else if(event.getParentCategory().equals("THEATRE")){
				tourType=TourType.THEATER;
			}else{
				tourType=TourType.OTHER;
			}
			String state = event.getStateShortDesc();
			if(null == state || state.isEmpty()){
				state = event.getState();
			}
			
			String country = event.getCountry();
			if(null == country || country.isEmpty()){
				country = event.getState();
			}
			
			String zipcode = event.getZipcode();
			CreationEventHit eventHit = new CreationEventHit(event.getEventId(), /*event.getPerformanceId(),*/ event.getPerformanceId(), 
					event.getVenueId(), event.getEventName(),
					event.getPerformanceName(), /*event.getPerformanceName(),*/ new java.sql.Date(event.getEventDate().getTime()),event.getEventTime(),
					tourType, event.getVenueName(), event.getCity(), state, country,zipcode, "");
			result.add(eventHit);
		}
		
		return result;
	}
	
	/*private static Collection<CreationEventHit> getSeatwaveEventList(Collection<Event> events, Map<Integer, Venue> venueIdMap) {
		Date now = new Date();
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (SeatWaveEvent seatWaveEvent: DAORegistry.getSeatWaveEventDAO().getAll()) {
			SeatWaveSeason seatWaveSeason=seatWaveEvent.getSeason();
			if (seatWaveSeason == null) {
				continue;
			}			
			if (seatWaveEvent.getEventDate().before(now)) {
				continue;
			}
			
			if (!TimezoneUtil.isEuropean(seatWaveEvent.getVenueCountryCode())) {
				continue;
			}

			Integer eventId = seatWaveEvent.getId();
			Integer tourId = seatWaveEvent.getSeasonId();
			Integer artistId = seatWaveEvent.getSeasonId();
			java.sql.Date date = new java.sql.Date(seatWaveEvent.getEventDate().getTime()); 
			java.sql.Time time = new java.sql.Time(seatWaveEvent.getEventDate().getTime());
			TourType tourType = TourType.OTHER;
			// can
			switch(seatWaveSeason.getGenreId()) {
				case SeatWaveSeason.GENRE_CONCERTS_ID:
						tourType = TourType.CONCERT;
						break;
						
				case SeatWaveSeason.GENRE_SPORT_ID:
					tourType = TourType.SPORT;
					break;
					
				case SeatWaveSeason.GENRE_THEATRE_ID:
					tourType = TourType.THEATER;
					break;											
			}

			String eventLocation = seatWaveEvent.getVenueName();
			String eventCity = seatWaveEvent.getVenueTown();
			String eventState = "";
			String eventCountry = seatWaveEvent.getVenueCountryCode();
			String url = seatWaveEvent.getUrl();
			
			Integer venueId = seatWaveEvent.getSeasonId();
			String name = seatWaveEvent.getName();
			// can 
			String tourName = seatWaveSeason.getName();
			String artistName = tourName;

			CreationEventHit eventHit = new CreationEventHit(eventId + CreationEventListManager.OFFSET_SEATWAVE_EVENT_ID, tourId, artistId, venueId, name, tourName, artistName,
															 date, time, tourType, eventLocation, eventCity, eventState, eventCountry, url);
			eventHit.setOriginalId(eventId);
			eventHit.setSiteId("seatwave");

			Event event = EventCreator.getSimilarEvent(eventHit, events, venueIdMap);
			if (event != null) {
				eventHit.setExists(true);
				eventHit.setEventId(event.getId());
				eventHit.setLastUpdate(event.getLastUpdate());
			} 

			eventHit.setName(name);					
			result.add(eventHit);
		}
		
		return result;
	}*/

	private static Collection<CreationEventHit> getStubhubEventList(String feedUrl, Collection<Event> events, Map<Integer, Venue> venueIdMap) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
		SimpleHttpClient httpClient = null;
		boolean flag=false;
		try {
			Date now = new Date();
			httpClient = HttpClientStore.createHttpClient();
			HttpPost httpPost = new HttpPost(feedUrl);
			// System.out.println("stubhub>" + feedUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");			
			
			CloseableHttpResponse response = null;
			JSONObject jsonObject = null;
			try{
				response = httpClient.execute(httpPost);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					jsonObject = new JSONObject(EntityUtils.toString(entity));
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
			
			JSONArray docsJSONArray = jsonObject.getJSONObject("response").getJSONArray("docs");
			for (int i = 0 ; i < docsJSONArray.length(); i++) {
				flag=true;
				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
				
				String active = docJSONObject.getString("active");
				if (active.equals("0")) {
					continue;
				}
				
				/*
				String maxPrice = docJSONObject.getString("maxPrice");
				if (maxPrice.equals("0.0")) {
					continue;
				}
				*/

				String name = scrubName(docJSONObject.getString("description"));
				String tourName;
				
				try {
//					tourName = scrubName(docJSONObject.getString("act_primary"));
					tourName = scrubName(docJSONObject.getString("genre_parent_name"));
				} catch(Exception e) {
					tourName = name;
				}


				
				String artistName;
				
				try {
//					artistName = scrubName(docJSONObject.getString("genre_parent_name"));
					artistName = scrubName(docJSONObject.getString("act_primary"));
				} catch(Exception e) {
					artistName = tourName;
				}

				Integer venueId;
				
				try {
					venueId = Integer.valueOf(docJSONObject.getString("venue_config_id"));
				} catch(Exception e) {
					venueId = null;
				}
				
				Integer tourId = Integer.valueOf(docJSONObject.getString("genre_parent"));
				Integer artistId = Integer.valueOf(docJSONObject.getString("genre_grand_parent_id"));
				String typeStr = docJSONObject.getString("channelUrlPath");
				String dateStr = docJSONObject.getString("event_date_local");
				String timeStr = docJSONObject.getString("event_time_local");
				String[] timeTokens = timeStr.split(":");
				java.sql.Date date = new java.sql.Date(format.parse(dateStr).getTime());

				if (date.before(now)) {
					continue;
				}
				
				Time time;
				if (timeTokens.length == 3) {
					time = new Time(Integer.valueOf(timeTokens[0]), Integer.valueOf(timeTokens[1]), Integer.valueOf(timeTokens[2]));
				} else {
					time = null;
				}
				
				String eventLocation = docJSONObject.getString("eventGeoDescription");
				String eventCity = null;
				try {
					eventCity = docJSONObject.getString("city");
				} catch (Exception e) {}
				
				String eventState = null;
				try {
					eventState = docJSONObject.getString("state");
				}  catch (Exception e) {}

				TourType tourType;
				if (typeStr.toLowerCase().indexOf("sport") >= 0) {
					tourType = TourType.SPORT;
				} else if (typeStr.toLowerCase().indexOf("theater") >= 0) {
					tourType = TourType.THEATER;					
				} else if (typeStr.toLowerCase().indexOf("concert") >= 0) {
					tourType = TourType.CONCERT;					
				} else {
					tourType = TourType.OTHER;					
				}
				
				Integer eventId = Integer.valueOf(docJSONObject.getString("event_id"));
				String url = "http://www.stubhub.com/?gAct=buy&event_id=" + eventId;

				String eventCountry = null;
				
				if (eventState == null) {
					eventCountry = "US";
				} else {
					// if the state is not a US state, then the country = state
					for (State state: Constants.getInstance().getStates()) {
						if (eventState.equals(state.getSymbol())) {
							eventCountry = "US";
							break;
						}
					}
				}
				
				// canadian provinces
				if (eventCountry == null && (eventState.equals("BC") || eventState.equals("ON") || eventState.equals("PQ")
					|| eventState.equals("QC") || eventState.equals("AB") || eventState.equals("MB")
					|| eventState.equals("NB") || eventState.equals("SK") || eventState.equals("NS"))) {
					eventCountry = "CA";
				}
				
				// UK
				if (eventCountry == null && (eventCity.equals("London") && eventState.equals(""))) {
					eventCountry = "UK";
				}
				if (eventCity.equalsIgnoreCase("London") && (eventState== null || eventState.equals(""))) {
					eventCountry = "UK";
				}
				
				// when stubhub has an event with the name mirror, it means the event name
				// has the same name than the tour
				if (tourName.equals("mirror") || tourName.isEmpty()) {
					tourName = artistName;
				}

				
				if (name.equals("mirror") || name.isEmpty()) {
					name = tourName;
				}
				
				String zipcode =""; // Not in use as of now..
				CreationEventHit eventHit = new CreationEventHit(eventId + CreationEventListManager.OFFSET_STUBHUB_EVENT_ID, /*tourId,*/ artistId, venueId, name, /*tourName,*/ artistName,
						new java.sql.Date(date.getTime()), time, tourType, eventLocation, eventCity, eventState, eventCountry,zipcode, url);
				eventHit.setOriginalId(eventId);
				eventHit.setSiteId("stubhub");

				Event event = EventCreator.getSimilarEvent(eventHit, events, venueIdMap);
				if (event != null) {
					eventHit.setExists(true);
					eventHit.setEventId(event.getId());
					eventHit.setLastUpdate(event.getLastUpdate());
				} 

				eventHit.setName(name);					

				if (eventHit.getTourType().equals(TourType.SPORT)) {
					if (eventHit.getName().toLowerCase().contains(" at ")) {
						String title = TextUtil.scrubSportingEventName(eventHit.getName()); 
						Matcher matcher = teamAtPattern.matcher(title);
						if (matcher.find()) {
							String homeTeam = matcher.group(2);
							String awayTeam = matcher.group(1);
							
							if (artistName != null) {
								homeTeam = artistName;
//								eventHit.setTourName(artistName);
							}
							
							try {
								awayTeam = scrubName(docJSONObject.getString("act_secondary"));
							} catch(Exception e) {
							}

							title = homeTeam + " vs. " + awayTeam;
							eventHit.setName(title);
						}
					}
				} 
				eventHit.setName(TextUtil.removeExtraWhitespaces(eventHit.getName()));
				result.add(eventHit);
			}		
		} catch (Exception e) {
			logger.warn("Error while refreshing events", e);
			return result;
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		if(flag){
			return result;
		}else{
			return null;
		}
	}
	
	public static List<CreationEventHit> getTicketNetworkEvents(Map<Integer,Event> eventMap, Map<Integer, Venue> venueIdMap) throws Exception{
		List<CreationEventHit> result ;
		List<CreationEventHit> finalResult= new ArrayList<CreationEventHit>();
		Calendar startCal=Calendar.getInstance();
		
		Calendar endCal=Calendar.getInstance();
		endCal.add(Calendar.DATE,4);
		
		for(int i=0;i<100;i++){
			Date startDate= startCal.getTime();
			Date endDate= endCal.getTime();
			result = getTicketNetworkEvents(startDate,endDate,eventMap);
			finalResult.addAll(result);
			startCal.add(Calendar.DATE,5);
			endCal.add(Calendar.DATE,5);
		}
		return finalResult;
	}
	
	public static List<CreationEventHit> getTicketNetworkDataFeedEvents(Map<Integer,Event> eventMap, Map<Integer, Venue> venueIdMap,Map<Integer,CreationEventHit> ticketNetworkMap) throws Exception{
		try {
			List<TNDataFeedEvent> result = new ArrayList<TNDataFeedEvent>();
			List<CreationEventHit> finalResult= new ArrayList<CreationEventHit>();
			Collection<TNDataFeedEvent> tnDataFeedEvents = DAORegistry.getTnDataFeedEventDAO().getAll();
			Map<Integer,TNDataFeedEvent> tnDataFeedEventMap = new HashMap<Integer,TNDataFeedEvent>();
			Map<Integer,TNDataFeedEvent> existingEventTNDataFeedEventMap = new HashMap<Integer,TNDataFeedEvent>();
			Map<Integer,TNDataFeedEvent> existingEventTNDataFeedEventMapFromDB = new HashMap<Integer,TNDataFeedEvent>();
			for(TNDataFeedEvent feedEvent:tnDataFeedEvents){
				if(!eventMap.containsKey(feedEvent.getExchangeEventId())) {
					if(!tnDataFeedEventMap.containsKey(feedEvent.getExchangeEventId())){
						tnDataFeedEventMap.put(feedEvent.getExchangeEventId(), feedEvent);
					}
				}else{
					existingEventTNDataFeedEventMapFromDB.put(feedEvent.getExchangeEventId(), feedEvent);
				}
			}
			Collection<TNDataFeedEvent> latestTNDataFeedEvents = getLatestTNDataFeedEvent(ticketNetworkMap);
			Map<Integer,TNDataFeedEvent> latestTNDataFeedEventMap = new HashMap<Integer,TNDataFeedEvent>();
			
			for(TNDataFeedEvent feedEvent:latestTNDataFeedEvents){
				if(!eventMap.containsKey(feedEvent.getExchangeEventId())){
					latestTNDataFeedEventMap.put(feedEvent.getExchangeEventId(), feedEvent);	
				}else{
					existingEventTNDataFeedEventMap.put(feedEvent.getExchangeEventId(), feedEvent);
				}
			}
			List<Integer> existingKeys = new ArrayList(existingEventTNDataFeedEventMap.keySet());
			Collection<TNDataFeedEvent> existingTNDataFeedEvents = new ArrayList<TNDataFeedEvent>();
			for(Integer key:existingKeys){
				TNDataFeedEvent event = existingEventTNDataFeedEventMapFromDB.remove(key);
				if(event==null){
					event =  existingEventTNDataFeedEventMap.remove(key);
				}
				if(event.getId() == null || event.getStatus().equals("ACTIVE")) {
					event.setStatus("EXISTING");
					existingTNDataFeedEvents.add(event);
				}
			}
			for(TNDataFeedEvent event:existingEventTNDataFeedEventMapFromDB.values()){
				if(event.getStatus().equals("ACTIVE")) {
					event.setStatus("EXISTING");
					existingTNDataFeedEvents.add(event);
				}
				
			}
			if(!existingTNDataFeedEvents.isEmpty()){
				DAORegistry.getTnDataFeedEventDAO().saveOrUpdateAll(existingTNDataFeedEvents);
			}
			List<Integer> keys = new ArrayList(latestTNDataFeedEventMap.keySet());
			for(Integer key:keys){
				TNDataFeedEvent event = tnDataFeedEventMap.remove(key);
				if(event==null){
					event =  latestTNDataFeedEventMap.remove(key);
					event.setStatus("ACTIVE");				
					result.add(event);
				}
	//			tnDataFeedEventMap.remove(key);
				TourType tourType = null;
				if(event.getParentCategory().equalsIgnoreCase("SPORTS")){
					tourType= TourType.SPORT;
				}else if(event.getParentCategory().equalsIgnoreCase("CONCERTS")){
					tourType= TourType.CONCERT;
				}else if(event.getParentCategory().equalsIgnoreCase("THEATRE")){
					tourType= TourType.THEATER;
				}else{
					tourType= TourType.OTHER;
				}
				CreationEventHit eventHit = new CreationEventHit(event.getExchangeEventId(), /*event.getExchangeTourId(),*/ event.getExchangePerformerId(), event.getExchangeVenueId(), event.getExchangeEventName(), event.getExchangePerformerName(), event.getExchangeEventDate(), event.getExchangeEventTime(), tourType, event.getExchangeVenueName(), event.getCity(), event.getState(), event.getCountry(),event.getZipcode(), "");
				finalResult.add(eventHit);
			}
			for(TNDataFeedEvent dataFeedEvent:tnDataFeedEventMap.values()){
				dataFeedEvent.setStatus("DELETED");
				result.add(dataFeedEvent);
				
			}
			DAORegistry.getTnDataFeedEventDAO().saveOrUpdateAll(result);
			/*Calendar startCal=Calendar.getInstance();
			
			Calendar endCal=Calendar.getInstance();
			endCal.add(Calendar.DATE,4);
			
			for(int i=0;i<100;i++){
				Date startDate= startCal.getTime();
				Date endDate= endCal.getTime();
				result = getTicketNetworkEvents(startDate,endDate,eventMap);
				finalResult.addAll(result);
				startCal.add(Calendar.DATE,5);
				endCal.add(Calendar.DATE,5);
			}*/
			return finalResult;
			
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
	}
	
	public static Collection<TNDataFeedEvent> getLatestTNDataFeedEvent(Map<Integer,CreationEventHit> ticketNetworkMap1){
		FileReader fileReader = null;
		BufferedReader reader = null;
		String line ="";
		Collection<TNDataFeedEvent> tnDataFeedEvents = new ArrayList<TNDataFeedEvent>();
		int errorDataCount=0;
		try {
			
			try{
				//Getting Ticket Network State Details from Web Service.
				getTicketNetworkStates();
			}catch(Exception e){
				e.printStackTrace();
			}
			
			fileReader = new FileReader("C:\\external files\\TNDataFeed\\TicketNetworkDataFeed-Events.csv");
			reader = new BufferedReader(fileReader);
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
//			DateFormat datetimeFormat = new SimpleDateFormat("HH:mm:ss");
			DateFormat tf = new SimpleDateFormat("HH:mm");
//			DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			Date now = new Date();
			boolean isFirst = true;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, 100);
			Date passDate = cal.getTime();
//			System.out.println(passDate);
			int lineNo =0;
			CSVReader csvReader = new CSVReader(fileReader);
            // Reading Records One by One in a String array
            String[] token;
            while ((token = csvReader.readNext()) != null) {
            	
           // }
			//while((line=reader.readLine())!=null){
				try {
				lineNo++;
				if(isFirst){
					isFirst = false;
					continue;
				}
				//String token[] = line.split(",");
				TNDataFeedEvent tnDataFeedEvent = new TNDataFeedEvent();
				String exchagneEventId = token[0];
				if(exchagneEventId==null || exchagneEventId.isEmpty()){
					continue;
				}
				/*if(ticketNetworkMap.containsKey(Integer.parseInt(exchagneEventId))){
					continue;   
				}*/  // Commetned for auto creation process
				tnDataFeedEvent.setExchangeEventId(Integer.parseInt(exchagneEventId));
				String exchangeEventName = token[1];
				if(exchangeEventName==null || exchangeEventName.isEmpty()){
					continue;
				}
				exchangeEventName = exchangeEventName.replaceAll("\"", "");
				tnDataFeedEvent.setExchangeEventName(exchangeEventName.trim());
				String exchangePerformerId = token[2];
				if(exchangePerformerId==null || exchangePerformerId.isEmpty()){
					continue;
				}
				//System.out.println(lineNo+" : 0 : "+token[0] +" :1: "+token[1]+" :2: "+token[2]+" :3: "+token[3]+" :4: "+token[4]);
				tnDataFeedEvent.setExchangePerformerId(Integer.parseInt(exchangePerformerId));
				
				String exchangePerformerName = token[3];
				if(exchangePerformerName==null){
					exchangePerformerName ="";
				}
				exchangePerformerName = exchangePerformerName.replaceAll("\"", "");
				tnDataFeedEvent.setExchangePerformerName(exchangePerformerName.trim());
				
				String exchangeVenueName = token[4];
				if(exchangeVenueName==null || exchangeVenueName.isEmpty()){
					continue;
				}
				exchangeVenueName = exchangeVenueName.replaceAll("\"", "");
				tnDataFeedEvent.setExchangeVenueName(exchangeVenueName.trim());
				String exchangeVenueId = token[5];
				if(exchangeVenueId==null || exchangeVenueId.isEmpty()){
					continue;
				}
				tnDataFeedEvent.setExchangeVenueId(Integer.parseInt(exchangeVenueId));
				
				String exchangeEventDateString = token[7];
				if(exchangeEventDateString==null || exchangeEventDateString.isEmpty()){
					continue;
				}else{
					if (!exchangeEventDateString.equals("TBA")) {
						String  dateTokens[] = exchangeEventDateString.split(" ");
						Date date = df.parse(dateTokens[0]);
						if(date.after(passDate) || date.before(now)){
							continue;
						}
						tnDataFeedEvent.setExchangeEventDate(new java.sql.Date(date.getTime()));
						Time time = new Time(tf.parse(dateTokens[1]).getTime());
						tnDataFeedEvent.setExchangeEventTime(time);
					}
				}
				
				String exchangeEventTypeId = token[8];
				if(exchangeEventTypeId==null || exchangeEventTypeId.isEmpty()){
					continue;
				}
				exchangeEventTypeId = exchangeEventTypeId.replaceAll("\"", "");
				TourType tourType;
				if(exchangeEventTypeId.equals("1")){
					tourType=TourType.SPORT;
				}else if(exchangeEventTypeId.equals("2")){
					tourType=TourType.CONCERT;
				}else if(exchangeEventTypeId.equals("3")){
					tourType=TourType.THEATER;
				}else{
					tourType=TourType.OTHER;
				}
				tnDataFeedEvent.setExchangeEventType(tourType);
				tnDataFeedEvent.setParentCategoryId(Integer.parseInt(exchangeEventTypeId));
				String parentCategory = token[9];
				if(parentCategory==null){
					parentCategory ="";
				}
				parentCategory = parentCategory.replaceAll("\"", "");
				
				String childCategoryId = token[10];
				
				String childCategory = token[11];
				if(childCategory==null){
					childCategory ="";
				}
				childCategory = childCategory.replaceAll("\"", "");
				
				String grandChildCategoryId = token[12];
				
				exchangePerformerName = exchangePerformerName.replaceAll("\"", "");
				
				String grandChildCategory = token[13];
				if(grandChildCategory==null){
					grandChildCategory ="";
				}
				grandChildCategory = grandChildCategory.replaceAll("\"", "");
				
				if(parentCategory==null || parentCategory.isEmpty()){
					continue;
				}
				if(childCategoryId==null || childCategoryId.isEmpty()){
					continue;
				}
				if(childCategory==null || childCategory.isEmpty()){
					continue;
				}
				if(grandChildCategoryId==null || grandChildCategoryId.isEmpty()){
					continue;
				}
				if(grandChildCategoryId==null || grandChildCategoryId.isEmpty()){
					continue;
				}
				
				String city = token[14];
				String state = token[15];
				String stateId = token[16];
				String coutry = token[17];
				String countryId = token[18];
				String zipcode = token[19];
				if(city==null || city.isEmpty() || state==null || state.isEmpty() || coutry==null || coutry.isEmpty() || 
						stateId == null || stateId.isEmpty() || countryId == null ||countryId.isEmpty() || zipcode == null ||zipcode.isEmpty()){
					continue;
				}
				
				
				
				try{
					if(null != countries && !countries.isEmpty()){
						
						countryId = countryId.replaceAll("\"","");
						Integer countryIdNew= Integer.valueOf(countryId.trim());
						
						coutry = countries.get(countryIdNew);
						
						if(null != countryStates.get(countryIdNew) && !countryStates.get(countryIdNew).isEmpty()){
							
							stateId = stateId.replaceAll("\"","");
							state = countryStates.get(countryIdNew).get(Integer.valueOf(stateId.trim()));
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				tnDataFeedEvent.setParentCategoryId(Integer.parseInt(exchangeEventTypeId));
				tnDataFeedEvent.setParentCategory(parentCategory.trim());
				
				tnDataFeedEvent.setChildCategoryId(Integer.parseInt(childCategoryId));
				tnDataFeedEvent.setChildCategory(childCategory.trim());
				tnDataFeedEvent.setZipcode(zipcode);
				tnDataFeedEvent.setGrandChildCategoryId(Integer.parseInt(grandChildCategoryId));
				tnDataFeedEvent.setGrandChildCategory(grandChildCategory.trim());
				
				tnDataFeedEvent.setCity(city.replaceAll("\"","").trim());
				tnDataFeedEvent.setState(state.replaceAll("\"","").trim());
				tnDataFeedEvent.setCountry(coutry.replaceAll("\"","").trim());
				tnDataFeedEvent.setLastUpdateDate(new Date());
				
				tnDataFeedEvents.add(tnDataFeedEvent);
				} catch (Exception e) {
					errorDataCount++;
					System.err.println("Error in TNDATA FEED CSV : "+lineNo + " : "+line);
					e.printStackTrace();
				}
				
			}
			logger.info("data fed file count............"+tnDataFeedEvents.size());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("data fed file not found.........");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		finally {
			try {
				if(fileReader != null) {
					fileReader.close();
				}
				if(reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
		return tnDataFeedEvents;
	}
	public static Map<Integer, String> getTicketNetworkCountries() {
		countries = TicketNetworkWebServiceClient.getTicketNetworkCountries();
		/*if(countries == null || countries.isEmpty()){
			String webServiceHost = "tnwebservices.ticketnetwork.com";
			String webServiceBaseDir = "tnwebservice/v3.2";
			String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
			String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
			String websiteConfigIDIN ="3320";
			
			//to get full venue location
			String getVenueSOAPMessage = 
				"<?xml version='1.0' encoding='utf-8'?>\n"
				+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
				+ "<soap:Body>\n"
				+ 	"<GetCountries xmlns='" +soapActionRootUrl+ "'>\n"
			    + 		"<websiteConfigID>%1$s</websiteConfigID>\n"
			    + 	"</GetCountries>\n"
			    + "</soap:Body>\n"
			    + "</soap:Envelope>\n";
			
			String countryMsg = String.format(getVenueSOAPMessage, 
					websiteConfigIDIN);
			
			try{
				HttpPost httpPost = new HttpPost(webServiceUrl);
				httpPost.addHeader("Host", webServiceHost);		
				httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
				httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetCountries" + "\"");
				
				httpPost.setEntity(new StringEntity(countryMsg));
				HttpClient httpClient =  HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);	
				HttpResponse xmlResponse = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(xmlResponse.getEntity());
		
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				Document xmlDocument = documentBuilder.parse(entity.getContent());
				
				NodeList countryNodeList = xmlDocument.getElementsByTagName("GetCountriesResult");
				
				
				countries = new HashMap<Integer, String>();
				
				Node countriesNode = countryNodeList.item(0);
				for (int i = 0 ; i < countriesNode.getChildNodes().getLength() ; i++) {
					Node countryNode = countriesNode.getChildNodes().item(i);
					boolean isCountryDone=false;
					boolean isCountryIDDone=false;
					String country=null;
					String id=null;
					for (int j = 0 ; j < countryNode.getChildNodes().getLength() ; j++) {
						Node node = countryNode.getChildNodes().item(j);
						String nodeName = node.getNodeName();
						if (nodeName.equals("Abbreviation")) {
							country = node.getTextContent();
							isCountryDone=true;;
						}
						if (nodeName.equals("ID")) {
							id = node.getTextContent();
							isCountryIDDone=true;;
						}
						if(isCountryDone && isCountryIDDone){
							countries.put(Integer.parseInt(id), country);
							break;
						}
					}
				}
				return countries;
				
			}catch(Exception e){
				e.printStackTrace();
				return countries;
			}
		}*/
		return countries;
		
	}
	
 public static Map<Integer, Map<Integer, String>> getTicketNetworkStates() throws Exception{
	 countryStates = TicketNetworkWebServiceClient.getTicketNetworkStates();
		/*if(countryStates == null || countryStates.isEmpty()){
			String webServiceHost = "tnwebservices.ticketnetwork.com";
			String webServiceBaseDir = "tnwebservice/v3.2";
			String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
			String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
			String websiteConfigIDIN ="3320";
			
			if(countries == null || countries.isEmpty()){
				getTicketNetworkCountries();
			}
			
			countryStates = new HashMap<Integer, Map<Integer, String>>();
			
			for (Integer countryID : countries.keySet()) {
				
				Map<Integer, String> statesMap = new HashMap<Integer, String>();
				
				//to get full venue location
				String getVenueSOAPMessage = 
					"<?xml version='1.0' encoding='utf-8'?>\n"
					+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
					+ "<soap:Body>\n"
					+ 	"<GetStates xmlns='" +soapActionRootUrl+ "'>\n"
				    + 		"<websiteConfigID>%1$s</websiteConfigID>\n"
				    +		"<countryID>%2$s</countryID>\n"
				    + 	"</GetStates>\n"
				    + "</soap:Body>\n"
				    + "</soap:Envelope>\n";
				
				String countryMsg = String.format(getVenueSOAPMessage, 
						websiteConfigIDIN,countryID);
				
				try{
					HttpPost httpPost = new HttpPost(webServiceUrl);
					httpPost.addHeader("Host", webServiceHost);		
					httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
					httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetStates" + "\"");
					
					httpPost.setEntity(new StringEntity(countryMsg));
					HttpClient httpClient =  HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);	
					HttpResponse xmlResponse = httpClient.execute(httpPost);
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(xmlResponse.getEntity());
			
					DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
					Document xmlDocument = documentBuilder.parse(entity.getContent());
					
					NodeList stateNodeList = xmlDocument.getElementsByTagName("GetStatesResult");
					
					Node statesNode = stateNodeList.item(0);
					for (int i = 0 ; i < statesNode.getChildNodes().getLength() ; i++) {
						Node stateNode = statesNode.getChildNodes().item(i);
						boolean isStateShortDescDone=false;
						boolean isStateIDDone=false;
						//boolean isStateLongDescDone=false;
						String stateShortDesc=null; //stateLongDesc=null;
						String id=null;
						for (int j = 0 ; j < stateNode.getChildNodes().getLength() ; j++) {
							Node node = stateNode.getChildNodes().item(j);
							String nodeName = node.getNodeName();
							if (nodeName.equals("StateProvinceLongDesc")) {
								stateLongDesc = node.getTextContent();
								isStateLongDescDone=true;;
							}
							if (nodeName.equals("StateProvinceShortDesc")) {
								stateShortDesc = node.getTextContent();
								isStateShortDescDone=true;;
							}
							if (nodeName.equals("StateProvinceID")) {
								id = node.getTextContent();
								isStateIDDone=true;;
							}
							if(isStateIDDone && isStateShortDescDone){
								statesMap.put(Integer.parseInt(id), stateShortDesc);
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				countryStates.put(countryID, statesMap);
				
			}
			return countryStates;
			
		}*/
		return countryStates;
	}
	
	
	public static List<CreationEventHit> getTicketNetworkEvents(Date startDate, Date endDate,Map<Integer,Event> eventMap) throws Exception {
		// POST /webservices/TNPubServicesXML.asmx HTTP/1.1
		// Host: tnwebservicespublic-test.ticketsoftware.net
		// Content-Type: text/xml; charset=utf-8
		// Content-Length: length
		// SOAPAction: "http://www.ticketnetworkdirect.com/webservices/GetEvents"
		String webServiceHost = "tnwebservices.ticketnetwork.com";
		String webServiceBaseDir = "tnwebservice/v3.2";
		String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
		String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
		String websiteConfigIDIN ="3320";
		String getEventsSOAPMessage = 
			"<?xml version='1.0' encoding='utf-8'?>\n"
			+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
			+ "	<soap:Body>\n"
			+ "		<GetEvents xmlns='" + soapActionRootUrl + "'>\n"
		    + "			<websiteConfigID>%1$s</websiteConfigID>\n"
		    + "			<beginDate>%2$s</beginDate>\n"
		    + "			<endDate>%3$s</endDate>\n"
			+ "		</GetEvents>\n"
			+ "	</soap:Body>\n"
			+ "</soap:Envelope>\n";
		
		
		
		DateFormat displayDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat displayTimeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
//		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		String startDateString = "";
		if (startDate != null) {
			startDateString = dateFormat.format(startDate) + "T00:00:00";
		}

		String endDateString = "";
		if (endDate != null) {
			endDateString = dateFormat.format(endDate) + "T23:59:59";
		}
		
		System.out.println(startDateString + "->" + endDateString);

		String msg = String.format(getEventsSOAPMessage, 
				websiteConfigIDIN,startDateString, endDateString);
		/*, 
				(parentCatID == null)?"":parentCatID,
				(childCatID == null)?"":childCatID,
				(grandChildCatID == null)?"":grandChildCatID);*/
		
		HttpPost httpPost = new HttpPost(webServiceUrl);
		httpPost.addHeader("Host", webServiceHost);		
		httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
		httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetEvents" + "\"");
		
		httpPost.setEntity(new StringEntity(msg));
		SimpleHttpClient httpClient =  HttpClientStore.createHttpClient();

		CloseableHttpResponse response = null;
		Document xmlDocument = null;
		try{
			response = httpClient.execute(httpPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				xmlDocument = documentBuilder.parse(entity.getContent());
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		List<CreationEventHit> ticketNetworkEvents = new ArrayList<CreationEventHit>();
		// soap:Envelope > soap:Body > GetEventsResponse > GetEventsResult
		
		NodeList eventNodeList = xmlDocument.getElementsByTagName("Event");
		
		//		  <Event>
		//		    	<EventName>string</EventName>
		//		    	<DisplayDate>string</DisplayDate>
		//		   	 	<Venue>string</Venue>
		//		    	<City>string</City>
		//		    	<State>string</State>
		//		    	<ParentCategoryID>string</ParentCategoryID>
		//		    	<Map>string</Map>
		//		    	<EventID>string</EventID>
		//		    	<VenueID>string</VenueID>
		//		    	<StateID>string</StateID>
		//		    	<EventDate>dateTime</EventDate>
		//		    	<VenueConfigurationID>string</VenueConfigurationID>
		//		    	<Clicks>int</Clicks>
		//		  </Event>

		String resultEventName = null;
		String displayDate = null;
		String venue = null;
		String city = null;
		String state = null;
		String parentCategoryID = null;
//		String map = null;
		String eventID = null;
		String venueID = null;
		Integer stateID = null;
		Integer countryID = null;
		String eventDateString = null;
		String country = null;
		String zipcode = null;
		
		System.out.println("EVENT NODE LIST=" + eventNodeList.getLength());
//		 countries = getTicketNetworkCountries();
//		 countryStates = getTicketNetworkStates();
//	    SimpleDateFormat eventDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0 ; i < eventNodeList.getLength() ; i++) {
			Node eventNode = eventNodeList.item(i);
			TourType tourType=null;
			for (int j = 0 ; j < eventNode.getChildNodes().getLength() ; j++) {
				Node node = eventNode.getChildNodes().item(j);

				String nodeName = node.getNodeName();
				if (nodeName.equals("Name")) {
					resultEventName = node.getTextContent();
				} else if (nodeName.equals("DisplayDate")) {
					displayDate = node.getTextContent();
				} else if (nodeName.equals("Venue")) {
					venue = node.getTextContent();
				} else if (nodeName.equals("City")) {
					city = node.getTextContent();	
				} else if (nodeName.equals("StateProvince")) {
					state = node.getTextContent();
//					if(state.length()>2){
//						System.out.println(state);
//					}
				} else if (nodeName.equals("ParentCategoryID")) {
					parentCategoryID = node.getTextContent();
					if(parentCategoryID.equals("1")){
						tourType=TourType.SPORT;
					}else if(parentCategoryID.equals("2")){
						tourType=TourType.CONCERT;
					}else if(parentCategoryID.equals("3")){
						tourType=TourType.THEATER;
					}else{
						tourType=TourType.OTHER;
					}
//				} else if (nodeName.equals("MapURL")) {
//					map = node.getTextContent();
				} else if (nodeName.equals("ID")) {
					eventID = node.getTextContent();
				} else if (nodeName.equals("VenueID")) {
					venueID = node.getTextContent();
				} else if (nodeName.equals("StateProvinceID")) {
					stateID = Integer.parseInt(node.getTextContent());
				} else if (nodeName.equals("CountryID")) {
					countryID=Integer.parseInt(node.getTextContent());
					country = countries.get(Integer.parseInt(node.getTextContent()));
				}else if (nodeName.equals("Date")) {
					eventDateString = node.getTextContent();
				}else if (nodeName.equals("zipcode")) {
					zipcode = node.getTextContent();
				}
			}
			if(eventMap.get(Integer.valueOf(eventID))!=null){
//				creationEventHit.setExists(true);
				continue;
			}
			
			if(null != countryID){
				
				if(null != countryStates.get(countryID) && !countryStates.get(countryID).isEmpty()){
					state = countryStates.get(countryID).get(stateID);
				}
			}
			
			
//			String venueLocation = venue + "," + city + "," + state;
			java.sql.Date eventDate = null;
			Time time=null;
			if (eventDateString != null) {
				String[] dateTokens = displayDate.split(" ");
				if (dateTokens.length != 2) {
					eventDate = null;
				} else {
					Date date = displayDateFormat.parse(dateTokens[0]);
					
					Time origTime = new Time(0, 0, 0);  
					time = origTime;  
					if (!dateTokens[1].equals("TBA")) {
						time = new Time(displayTimeFormat.parse(dateTokens[1]).getTime());
					}
					
					eventDate = new java.sql.Date(date.getTime() + time.getTime() - origTime.getTime());
				}
			}
//			Integer originalTourId=null;
			Integer originalArtistId=null;
			Integer originalVenueId=Integer.valueOf(venueID);
//			String name=null;
//			String tourName=null;
			String artistName=null;
			/*if(resultEventName!=null && !resultEventName.isEmpty()){
				if(tourType==TourType.SPORT){
					String temp [] = resultEventName.split("vs.");
					tourName=temp[0];
					artistName=temp[0];
				}else{
					tourName=resultEventName;
				}
			}*/
			
//			java.sql.Date date=null;
//			Date time=displayTimeFormat.parse(displayDate);
//			String building=null;
//			String city=null;
//			String state=null;
			//String country="UK";
			String url=null;
//			if(resultEventName.toLowerCase().contains("mets")){
//				System.out.println("1");
//			}
			
			
			CreationEventHit creationEventHit = new CreationEventHit(Integer.valueOf(eventID), /*originalTourId,*/ originalArtistId, originalVenueId,
					resultEventName, /*tourName, */artistName,
					eventDate, time, tourType, venue,
					city, state, country,zipcode, url);
//			System.out.println(eventID +":" + resultEventName);
			
			ticketNetworkEvents.add(creationEventHit);	
		}
		return ticketNetworkEvents;
	}
	
	private static Collection<CreationEventHit> getStubhubEventList(Collection<Event> events, Map<Integer, Venue> venueIdMap) {
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
		/*
		 * Stubhub
		 */
		
//		String feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+venue_config_id+channelUrlPath+genre_parent_name+genre_grand_parent_name+active+genre_grand_parent_id+genre_parent+maxPrice&wt=json";
		String feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent%0D%0A%0D%0A;event_id%20asc&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+venue_config_id+channelUrlPath+genre_parent_name+active+genre_parent+act_primary+act_secondary+genre_grand_parent_id&start=START&rows=COUNT&wt=json";
		Collection<CreationEventHit> subResult;
		int count = 500;
		int start = 0;
		do {
			String subFeedUrl = feedUrl.replaceAll("START", start + "").replaceAll("COUNT", count + "");
			subResult = getStubhubEventList(subFeedUrl, events, venueIdMap);
			start += count;
			if(subResult!=null){
				System.out.println("FETCHING URL: " + subFeedUrl + "==>" + subResult.size());
				result.addAll(subResult);
			}
		} while (subResult!=null);

		return result;
	}
	
	public synchronized void refreshEventList() {
		// for testing
		// if (true) {
		//	return;
		// }
		try {
			refreshing = true;
			logger.info("Refreshing Events");
			/* Events were assigning to  old tours */
//			Collection<Event> events = DAORegistry.getEventDAO().getAll();
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEvents();
//			Collection<AdmitoneEvent> admitoneEvents = DAORegistry.getAdmitoneEventDAO().getAll();
			Map<Integer, Venue> venueIdMap = new HashMap<Integer, Venue>();
			for (Venue venue : DAORegistry.getVenueDAO().getAll()) {
				venueIdMap.put(venue.getId(), venue);
			}
	
			/*Map<Integer, Tour> tourIdMap = new HashMap<Integer, Tour>();
			for (Tour tour: DAORegistry.getTourDAO().getAll()) {
				tourIdMap.put(tour.getId(), tour);
			}*/
	
			Map<Integer, Artist> artistIdMap = new HashMap<Integer, Artist>();
			for (Artist artist: DAORegistry.getArtistDAO().getAll()) {
				artistIdMap.put(artist.getId(), artist);
			}
	
			
			for (TicketListingCrawl crawl: DAORegistry.getTicketListingCrawlDAO().getAll()) {
				Collection<String> sites = sitesPerEventMap.get(crawl.getEventId());
				if (sites == null) {
					sites = new TreeSet<String>(new Comparator<String>() {
	
						public int compare(String s1, String s2) {
							return s1.compareTo(s2);
						}
						
					});
					sitesPerEventMap.put(crawl.getEventId(), sites);
				}
				if(crawl.isEnabled()){
					sites.add(crawl.getSiteId()+"-1");
				}else{
					sites.add(crawl.getSiteId()+"-0");
				}
			}
			try{
				Collection<CreationEventHit> localExistingEventHits = getExistingEventList(events, venueIdMap, artistIdMap, /*tourIdMap,*/
					sitesPerEventMap);
				existingEventHits = localExistingEventHits;
			}catch(Exception ex){
				logger.info("Error: ", ex);
			}
			Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
			for(Event event:DAORegistry.getEventDAO().getAllEventByAdmitoneId()){
				eventMap.put(event.getAdmitoneId(),event);
			}
			getTicketNetworkCountries();
			getTicketNetworkStates();
			try{
				Collection<CreationEventHit> localTicketNetworkEventHits = getTicketNetworkEvents(eventMap, venueIdMap);
				if(ticketNetworkEventHits!=null){
					ticketNetworkEventHits.clear();
				}
				ticketNetworkEventHits = localTicketNetworkEventHits;
			}catch(Exception ex){
				logger.info("Error in TicketNetowrk EventCreation: ", ex);
			}
			Map<Integer, CreationEventHit> ticketNetworkMap = new HashMap<Integer, CreationEventHit>();
			if(ticketNetworkEventHits != null && !ticketNetworkEventHits.isEmpty()){
				for(CreationEventHit hit:ticketNetworkEventHits){
					ticketNetworkMap.put(hit.getEventId(), hit);
				}
			}
			try{
				Collection<CreationEventHit> localDataFeedEventHits = getTicketNetworkDataFeedEvents(eventMap, venueIdMap,ticketNetworkMap);
				if(ticketNetworkDataFeedEventHits!=null){
					ticketNetworkDataFeedEventHits.clear();
					System.out.println("REFRESH: localDataFeedEventHits......."+localDataFeedEventHits.size());
				}
				ticketNetworkDataFeedEventHits = localDataFeedEventHits;
				
				
				/*for (CreationEventHit creationEventHit : localDataFeedEventHits) {
					
					System.out.println("REFRESH: getBuilding..."+creationEventHit.getBuilding());
					System.out.println("REFRESH: getCountry....."+creationEventHit.getCountry());
					System.out.println("REFRESH: getState..."+creationEventHit.getState());
					System.out.println("REFRESH: getLocation..."+creationEventHit.getCity());
					System.out.println("REFRESH: getLocation..."+creationEventHit.getLocation());
					System.out.println("REFRESH: getOriginalVenueId..."+creationEventHit.getOriginalVenueId());
					System.out.println("REFRESH: ======================================================================================");
				}*/
			}catch(Exception ex){
				ex.printStackTrace();
				System.out.println(ex.fillInStackTrace());
				logger.info("Error: ", ex);
			}
			
			Collection<CreationEventHit> localPOSEventHits =  getPOSEventList(eventMap);
			if(ticketNetworkPOSEventHits!=null){
				ticketNetworkPOSEventHits.clear();
			}
			ticketNetworkPOSEventHits = localPOSEventHits;
			/*
			try{
				Collection<CreationEventHit> localStubhubEventHits = getStubhubEventList(events, venueIdMap);
				stubhubEventHits = localStubhubEventHits;
			}catch(Exception ex){
				logger.info("Error: ", ex);
			}
			
			
			try{
				Collection<CreationEventHit> localSeatwaveEventHits = getSeatwaveEventList(events, venueIdMap);
				seatwaveEventHits = localSeatwaveEventHits;
			}catch(Exception ex){
				logger.info("Error: ", ex);
			}*/
			logger.info("Refresh event done!");
		} catch(Exception e) {
			logger.info("Error: ", e);
		} finally {
			refreshing = false;
		}
	}
	
	public synchronized void createEventsFromFeed(){
		
		Collection<Venue> venueList = DAORegistry.getVenueDAO().getAll();
		Map<Integer, String> venueCategoryMap = new HashMap<Integer, String>();
		Map<Integer, Integer> venueMap = new HashMap<Integer, Integer>();
//		Map<String, Integer> venueMap = new HashMap<String, Integer>();
		
		for(Venue venue : venueList){
			venueMap.put(venue.getTnVenueId(),venue.getId());
//			venueMap.put(venue.getBuilding()+ "--" + venue.getCity() + "--" + venue.getState(),venue.getId());
		}
		
		
		Collection<AutoEventListingResult> toBeCreateList = new ArrayList<AutoEventListingResult>();
		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DATE, 2);
		Date date = cal.getTime();
		for(CreationEventHit eventHit: new ArrayList<CreationEventHit>(ticketNetworkDataFeedEventHits)){
			try{
			Integer venueId = venueMap.get(eventHit.getOriginalVenueId());
			if(venueId==null){
				Venue venue = new Venue();
				venue.setBuilding(eventHit.getBuilding());
				venue.setCity(eventHit.getCity());
				venue.setCountry(eventHit.getCountry());
				venue.setState(eventHit.getState());
				venue.setTnVenueId(eventHit.getOriginalVenueId());
				venue.setZipcode(eventHit.getZipcode());
				venue.setVenueType(TourType.OTHER);
				DAORegistry.getVenueDAO().save(venue);
				venueId= venue.getId();
				venueMap.put(venue.getTnVenueId(),venueId);
				
			}/*else{
				Venue venue = DAORegistry.getVenueDAO().get(venueId);
				venue.setTnVenueId(eventHit.getOriginalVenueId());
				DAORegistry.getVenueDAO().update(venue);
			}*/
			if(eventHit.getDate().before(date)){
				continue;
			}
			
			Map<Integer, String> venueIdMap = new HashMap<Integer, String>();
			venueIdMap.put(eventHit.getEventId(), eventHit.getEventId()+"-"+venueId);
			venueCategoryMap.put(eventHit.getEventId(), "0");
			AutoEventListingResult result = getEventListingResults(eventHit.getEventId(), new String[]{""}, false, false, false, false, true/*includeTicketNetworkDataFeedEvents*/, false, venueIdMap, venueCategoryMap);
			if(result!=null){
				toBeCreateList.add(result);
			}
			if(toBeCreateList.size()>=100){
				createEventsFromAutoResults("AUTO", toBeCreateList , false, false, false, false, true, false,CreationType.AUTO);
				toBeCreateList.clear();
				/*try {
					Thread.sleep(10* 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		if(!toBeCreateList.isEmpty()){
			createEventsFromAutoResults("AUTO", toBeCreateList , false, false, false, false, true, false,CreationType.AUTO);
		}
	}
	public static void updatePOSEvent(Collection<CreationEventHit> localPOSEventHits){
		if(ticketNetworkPOSEventHits!=null){
			ticketNetworkPOSEventHits.clear();
		}
		ticketNetworkPOSEventHits = localPOSEventHits;
	}
	public boolean isRefreshing() {
		return refreshing;
	}
	
	public synchronized void updateEventList(Collection<AutoEventListingResult> list,boolean includeExistingEvents,boolean includeStubhubEvents,boolean includeSeatwaveEvents,boolean includeTicketNetworkEvents,boolean includeTicketNetworkDataFeedEvents,boolean includeTicketNetworkPOSEvents) {
		List<CreationEventHit> eventHits = new ArrayList<CreationEventHit>();
		// first try to match the existing event, then if it does not exist,
		// try to match the stubhub event
		// and finally try to match the seatwave event.
		if(includeExistingEvents){
			eventHits.addAll(existingEventHits);
		}
		if(includeStubhubEvents){
			eventHits.addAll(stubhubEventHits);
		}
		if(includeSeatwaveEvents){
			eventHits.addAll(seatwaveEventHits);
		}
		if(includeTicketNetworkEvents){
			eventHits.addAll(ticketNetworkEventHits);
		}
		
		if(includeTicketNetworkDataFeedEvents){
			eventHits.addAll(ticketNetworkDataFeedEventHits);
		}
		
		if(includeTicketNetworkPOSEvents){
			eventHits.addAll(ticketNetworkPOSEventHits);
		}
		for (AutoEventListingResult autoResult: list) {
			
			CreationEventHit eventHit = getEventHit(autoResult.getOriginalId(),eventHits);
			if(eventHit==null){
				continue;
			}
			// move it to existing event hits
			if (!existingEventHits.contains(eventHit)) {
				eventHit.setExists(true);
				eventHit.setEventId(autoResult.getEventId());
				existingEventHits.add(eventHit);
			}
			
			// remove it from stubhub
			if (stubhubEventHits.contains(eventHit)) {
				stubhubEventHits.remove(eventHit);
			}
			if (seatwaveEventHits.contains(eventHit)) {
				seatwaveEventHits.remove(eventHit);
			}
			
			if (ticketNetworkEventHits.contains(eventHit)) {
				ticketNetworkEventHits.remove(eventHit);
			}

			if (ticketNetworkDataFeedEventHits.contains(eventHit)) {
				ticketNetworkDataFeedEventHits.remove(eventHit);
			}
			if (ticketNetworkPOSEventHits.contains(eventHit)) {
				ticketNetworkPOSEventHits.remove(eventHit);
			}
			
			// update crawls
			Collection<String> sites = new ArrayList<String>();
			for (TicketListingCrawl crawl: DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(eventHit.getEventId())) {
				sites.add(crawl.getSiteId());
			}
			eventHit.setSites(sites);
		}
	}
	
	public CreationEventHit getEventHit(Integer eventId,List<CreationEventHit> eventHits) {
		
		
		for (CreationEventHit hit: eventHits) {
			if (hit.getEventId().equals(eventId)) {
				return hit;
			}
		}
		return null;
	}
	
	public static String scrubName(String name) {
		// remove tickets
		if (name == null) {
			return null;
		}
		name = name.replaceAll("[tT][iI][cC][kK][eE][tT](?:[sS])", "");
		name = name.replaceAll("[eE][vV][eE][nN][tT](?:[sS])", "");
		name = name.replaceAll("[-]", " ").replaceAll("\\s++", " ");
		// if we have A - B, then only keep A
//		name = name.replaceAll(" - .*", "");
		
		return name;
	}
	/*
	private int computeSimilarity(String name1, String name2) {
		// see how many words are present in name2
		for (String token: name1.split(" ")) {
//			String scrubbedToken = token.replaceAll("", replacement)
		}
		return 0;
	}	
	*/
	public void clearUnusedAutoEventListingResultInMap() {
		Collection<AutoEventListingResult> results = new ArrayList<AutoEventListingResult>(autoEventListingResultMap.values());
		Date now = new Date();
		for (AutoEventListingResult result: results) {
			if (now.getTime() - result.getCreationDate().getTime() > CACHE_TIMEOUT) {
				logger.info("removing from cache " + result.getEventId());
				autoEventListingResultMap.remove(result.getOriginalId());
			}
		}
	}
	
	public AutoEventListingResult getAutoEventListingResult(Integer id) {
		return autoEventListingResultMap.get(id); 
	}
	
	public AutoEventListingResult getEventListingResults(Integer eventId, String[] siteIds,/*String[] tourNames,*/boolean includeExistingEvents,
			boolean includeStubhubEvents,boolean includeSeatwaveEvents,boolean includeTicketNetworkEvents,boolean includeTicketNetworkDataFeedEvents,boolean includeTicketNetworkPOSEvents,
			Map<Integer,String> venueMap ,Map<Integer,String> venueCategoryMap) {
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.setCorePoolSize(MAX_CONCURRENT_SEARCH);
//		String tourName="";
		Integer venueId=0;
		/*for(String temp:tourNames){
			if(temp==null){
				continue;
			}
			if(temp.contains(eventId + "-")){
				tourName=temp.replaceAll(eventId+"-", "");
				break;
			}
		}*/
		for(String tempVenue:venueMap.values()){
			if(tempVenue==null){
				continue;
			}
			if(tempVenue.contains(eventId + "-")){
				venueId = Integer.parseInt(tempVenue.replaceAll(eventId+"-", ""));
				break;
			}
		}
		List<CreationEventHit> actualEventHits = new ArrayList<CreationEventHit>();
		// first try to match the existing event, then if it does not exist,
		// try to match the stubhub event
		// and finally try to match the seatwave event.
		if(includeExistingEvents){
			actualEventHits.addAll(existingEventHits);
		}
		if(includeStubhubEvents){
			actualEventHits.addAll(stubhubEventHits);
		}
		if(includeSeatwaveEvents){
			actualEventHits.addAll(seatwaveEventHits);
		}
		
		if(includeTicketNetworkEvents){
			actualEventHits.addAll(ticketNetworkEventHits);
		}
		
		if(includeTicketNetworkDataFeedEvents){
			actualEventHits.addAll(ticketNetworkDataFeedEventHits);
		}
		
		if(includeTicketNetworkPOSEvents){
			actualEventHits.addAll(ticketNetworkPOSEventHits);
		}
		CreationEventHit eventHit = getEventHit(eventId,actualEventHits);
		
		if (eventHit == null) {
			return null;
		}
		String venueNames = eventHit.getBuilding();
		Integer venueIds = eventHit.getVenueId();
		
		
		if(eventHit.getVenueId()==null || !eventHit.getVenueId().equals(venueId)){
			Venue venue = DAORegistry.getVenueDAO().get(venueId);
			if(venue==null){
				return null;
			}
				eventHit.setVenueId(venueId);
				eventHit.setBuilding(venue.getBuilding());
				eventHit.setCity(venue.getCity());
				eventHit.setState(venue.getState());
				eventHit.setCountry(venue.getCountry());
		}
		
		String venueCategoryGroup = venueCategoryMap.get(eventId);
		VenueCategory venueCategory = null;
		if(eventHit.getVenueCategoryList()!=null && !eventHit.getVenueCategoryList().isEmpty()){
			for(VenueCategory venueCategoryItr: eventHit.getVenueCategoryList()){
				if(venueCategoryItr.getCategoryGroup().equals(venueCategoryGroup)){
					venueCategory=venueCategoryItr;
					break;
				}
			}
		}else if(venueCategoryGroup!=null && venueCategoryGroup!="" && venueCategoryGroup != "0"){
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(venueId,venueCategoryGroup);
		}
		/*if(tourName!=null && !tourName.equals("")){
			if(eventHit.getOriginalTourName()==null){
				eventHit.setOriginalTourName(eventHit.getTourName());
			}
			eventHit.setTourName(tourName);
		}else if(eventHit.getOriginalTourName()!=null){
				eventHit.setTourName(eventHit.getOriginalTourName());
		}*/
		
		
//		eventHit.setVenueCategoryGroup(venueCategoryGroup);
		final AutoEventListingResult autoResult = new AutoEventListingResult(eventHit,venueCategory);
		// in stubhub the event name is not always relevant => consider the tour name
		logger.info("EVENT NAME: " + TextUtil.stripName(eventHit.getName()) + " "  + eventHit.getDate());
		Calendar fromCal = new GregorianCalendar();
		fromCal.setTime(eventHit.getDate());
		fromCal.set(Calendar.HOUR_OF_DAY, 0);
		fromCal.set(Calendar.MINUTE, 0);
		fromCal.set(Calendar.SECOND, 0);
		
		Calendar toCal = new GregorianCalendar(fromCal.get(Calendar.YEAR), fromCal.get(Calendar.MONTH), fromCal.get(Calendar.DATE), 23, 59, 59);

		// loop on all 
		for (EventListingResult result: eventListingManager.search(TextUtil.stripName(eventHit.getName()), siteIds, null, fromCal.getTime(), toCal.getTime(),null,false,false)) {
			if (result.getEventHits() == null) {
				continue;
			}
			
			List<EventHit> eventHits = new ArrayList<EventHit>();
			
			for (EventHit hit: result.getEventHits()) {
				// discriminate entries based on time 
				if (hit.getTime() != null) {
					Calendar hitCal = new GregorianCalendar();
					hitCal.setTime(hit.getTime());

					Calendar resultCal = new GregorianCalendar();
					resultCal.setTime(autoResult.getTime());

					// In case it is TBD, we say that the time matches (whatever time it is on the other exchange)
					if(!((hitCal.get(Calendar.HOUR_OF_DAY) == 23) && (hitCal.get(Calendar.MINUTE) == 59))
							&& !((hitCal.get(Calendar.HOUR_OF_DAY) == 0) && (hitCal.get(Calendar.MINUTE) == 0) && (hitCal.get(Calendar.SECOND) == 0))){

						
						// if the time of the matching event is greater than 8 skip it.
						/*java.util.Date hitDate = new Date(
								hit.getDate().getYear(),
								hit.getDate().getMonth(),
								hit.getDate().getDay(),
								hit.getTime().getHours(),
								hit.getTime().getMinutes(),
								hit.getTime().getSeconds()
						);

						java.util.Date autoResultDate = new Date(
								autoResult.getDate().getYear(),
								autoResult.getDate().getMonth(),
								autoResult.getDate().getDay(),
								autoResult.getTime().getHours(),
								autoResult.getTime().getMinutes(),
								autoResult.getTime().getSeconds()
						);*/

						if (Math.abs(hit.getDate().getTime() - autoResult.getDate().getTime()) > TIME_MATCH_TOLERANCE) {
							System.out.println(">> SKIP EVENT ");
							continue;
						}
					}
				}
				
				if (!TextUtil.checkMenWomensCompatibility(hit.getName(), eventHit.getName())) {
					continue;
				}
				
				// check paralympic event - both should contain the word or not
				
				if (hit.getLocation() != null && eventHit.getLocation() != null && hit.getLocation().toLowerCase().contains("paralympic") != eventHit.getLocation().toLowerCase().contains("paralympic")) {
					continue;
				}

				logger.info(hit.getSiteId() + "=>" + hit.getLocation() + " /" + TextUtil.getKeywordsFromName(autoResult.getBuilding()) + " / " + TextUtil.getKeywordsFromName(hit.getLocation()));
				// location must contain stadium
				if (hit.getSiteId().equals("ebay")) {
					eventHits.add(hit);
				} else if (TextUtil.isSimilar(autoResult.getBuilding(), hit.getLocation())) {
//					|| (autoResult.getCity() != null && hit.getLocation().toLowerCase().contains(autoResult.getCity().toLowerCase()))) {
					eventHits.add(hit);
					logger.info("Adding " + autoResult.getBuilding() +  "/" + hit.getLocation()  + "/");
				}
			}

			Collections.sort(eventHits, new Comparator<EventHit>() {
				@Override
				public int compare(EventHit eventHit1, EventHit eventHit2) {
					Integer score1 = QueryUtil.getMatchingScore(autoResult.getName(), eventHit1.getName());
					Integer score2 = QueryUtil.getMatchingScore(autoResult.getName(), eventHit2.getName());
					
					int comp = score1.compareTo(score2);
					
					if (comp == 0) {
						if (eventHit1.getTime() == null) {
							return 1;
						}
						
						if (eventHit2.getTime() == null) {
							return -1;
						}
						
						if (autoResult.getTime() == null) {
							return 0; // no way to tell which one is best
						}
						
						Long timeDiff1 = Math.abs(eventHit1.getTime().getTime() - autoResult.getTime().getTime());
						Long timeDiff2 = Math.abs(eventHit2.getTime().getTime() - autoResult.getTime().getTime());
						
						return timeDiff1.compareTo(timeDiff2);
					}
					
					return comp;
				}
			});

			if (!eventHits.isEmpty()) {
				autoResult.addEventHit(eventHits.get(0));
			}
		}
		eventHit.setBuilding(venueNames);
		eventHit.setVenueId(venueIds);
		autoEventListingResultMap.put(autoResult.getEventId(), autoResult);
		return autoResult;
	}

	public Collection<AutoEventListingResult> getEventListingResults(Integer[] eventIds, final String[] siteIds,/*final String[] tourNames,*/final boolean includeExistingEvents,final boolean includeStubhubEvents,
			final boolean includeSeatwaveEvents,final boolean includeTicketNetworkEvents,final boolean includeTicketNetworkDataFeedEvents,final boolean includeTicketNetworkPOSEvents,
			final Map<Integer,String> venueMap ,final Map<Integer,String> venueCategoryMap ) {
		ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		executor.setCorePoolSize(2);
		
		clearUnusedAutoEventListingResultInMap();
		final Collection<AutoEventListingResult> results = Collections.synchronizedCollection(new ArrayList<AutoEventListingResult>());
		for (final Integer eventId: eventIds) {
			
			executor.execute(new Runnable() {

				public void run() {
					AutoEventListingResult result = getEventListingResults(eventId, siteIds,includeExistingEvents,includeStubhubEvents,includeSeatwaveEvents,includeTicketNetworkEvents,includeTicketNetworkDataFeedEvents,includeTicketNetworkPOSEvents,venueMap,venueCategoryMap);
					if (result == null) {
						return;
					}
					
					results.add(result);
				}
				
			});
		}
		
		executor.shutdown();
		try {
			executor.awaitTermination(600, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			executor.shutdownNow();
			logger.info("Timeout for auto crawl creation", e);
		}
		
		return results;
	}
	
	public Collection<CreationEventHit> getEventHits(TourType type, String keywords, String location,
													boolean includeExistingEvents,
													boolean includeStubhubEvents, boolean includeStubhubDuplicatedEvents,
													boolean includeSeatwaveEvents,boolean includeTicketNetworkEvents,boolean includeTicketNetworkDataFeedEvents,boolean includeTicketNetworkPOSEvents,
													boolean hideEventsWithACrawl, 
													Date startDate, Date endDate,
													boolean matchAll,
													boolean locationMatchAll,/*Collection<Artist> artists,*/Collection<Venue> venues) {
		if (keywords == null) {
			keywords = "";
		}

		if (location == null) {
			location = "";
		}
		
		Collection<CreationEventHit> result = new ArrayList<CreationEventHit>();
				
		startDate = new Date(startDate.getTime() - 1000);
		endDate = new Date(endDate.getTime() + 1000);

		Collection<CreationEventHit> eventHits = new ArrayList<CreationEventHit>();
		if (includeExistingEvents) {
			eventHits.addAll(existingEventHits);
		}

		if (includeStubhubEvents) {
			eventHits.addAll(stubhubEventHits);
		}

		if (includeSeatwaveEvents) {
			eventHits.addAll(seatwaveEventHits);
		}
		
		if (includeTicketNetworkEvents) {
			eventHits.addAll(ticketNetworkEventHits);
		}
		if(includeTicketNetworkDataFeedEvents){
			
			System.out.println("ticketNetworkDataFeedEventHits......."+ticketNetworkDataFeedEventHits.size());
			
			/*for (CreationEventHit creationEventHit : ticketNetworkDataFeedEventHits) {
				
				System.out.println("getBuilding..."+creationEventHit.getBuilding());
				System.out.println("getCountry....."+creationEventHit.getCountry());
				System.out.println("getState..."+creationEventHit.getState());
				System.out.println("getLocation..."+creationEventHit.getCity());
				System.out.println("getLocation..."+creationEventHit.getLocation());
				System.out.println("getOriginalVenueId..."+creationEventHit.getOriginalVenueId());
				System.out.println("======================================================================================");
			}*/
			
			eventHits.addAll(ticketNetworkDataFeedEventHits);
		}
		if(includeTicketNetworkPOSEvents){
			eventHits.addAll(ticketNetworkPOSEventHits);
		}
//		Collection<Tour> tours = null;
////		if(keywords!=null || !"".equals(keywords)){
//			tours = DAORegistry.getTourDAO().getAllActiveTours();
////		}
		/*Map<String, Tour> tourMap = new HashMap<String, Tour>();
		for(Tour tour:tours){
			tourMap.put(tour.getName(), tour);
		}*/
		Map<String, Venue> venueMap = new HashMap<String, Venue>();
		for(Venue venue:venues){
			venueMap.put(venue.getBuilding().toLowerCase(), venue);
		}
		
		Map<Integer, List<VenueCategory>> venueCategoryMap = new HashMap<Integer, List<VenueCategory>>();
		Map<Integer, CreationEventHit> dupMap = new HashMap<Integer, CreationEventHit>();
		for (CreationEventHit eventHit: eventHits) {
			/*if(eventHit.getName()!=null && (eventHit.getName().contains("Jersey Boys Washington"))){
				System.out.println(eventHit.getName() +":"+ eventHit.getLocation());
			}*/
			if(dupMap.containsKey(eventHit.getEventId())){
				continue;
			}
			if(eventHit.getName().equalsIgnoreCase("Excision")){
				if(eventHit.getCity().equalsIgnoreCase("London")){
					System.out.println("====="+eventHit.getName()+"=======");
				}
				
			}
			if (((matchAll && QueryUtil.matchAllTerms(keywords, eventHit.getName())) || (!matchAll && (QueryUtil.matchAnyTerms(keywords, eventHit.getName())
//					|| QueryUtil.matchAnyTerms(keywords, eventHit.getTourName())
					|| QueryUtil.matchAnyTerms(keywords, eventHit.getArtistName()))))
					&& (!locationMatchAll || QueryUtil.matchAllTerms(location, eventHit.getLocation()))
					&& QueryUtil.matchAnyTerms(location, eventHit.getLocation())
					&& (!eventHit.isExists() || includeExistingEvents)
					&& ((startDate == null && endDate == null) || (eventHit.getDate().after(startDate) && eventHit.getDate().before(endDate)))
					&& (type == null || (type.equals(eventHit.getTourType())))
					&& (eventHit.getSiteCount() == 0 || !hideEventsWithACrawl)) {
//				String tourName=eventHit.getTourName();
				String venueName=eventHit.getBuilding();
//				for(Tour tour:tours){
//					String tourName=tour.getName();
//					if(tourName.equals(keyword)){
//						eventHit.setTourId(tour.getId());
//						break;
//					}
//				}
				/*Tour tour = tourMap.get(tourName);
				if(tour!=null){
					eventHit.setTourId(tour.getId());
				}*/
				Venue venue = venueMap.get(venueName.toLowerCase());
				if(venue!=null){
					eventHit.setVenueId(venue.getId());
					List<VenueCategory> venueCategoryList = venueCategoryMap.get(venue.getId());
					if(venueCategoryList==null){
						venueCategoryList = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venue.getId());
					}
					venueCategoryMap.put(venue.getId(), venueCategoryList);
					eventHit.setVenueCategoryList(venueCategoryList);
				}
				dupMap.put(eventHit.getEventId(), eventHit);
				result.add(eventHit);
			}
		}
		
		return result;
	}

	public EventListingManager getEventListingManager() {
		return eventListingManager;
	}

	public void setEventListingManager(EventListingManager eventListingManager) {
		this.eventListingManager = eventListingManager;
	}
	
	public void afterPropertiesSet() throws Exception {
		if (eventListingManager == null) {
			throw new RuntimeException("Parameter eventListingManager must be set");
		}

//		if (ticketListingCrawler == null) {
//			throw new RuntimeException("Parameter ticketListingCrawler must be set");
//		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MINUTE,15);
		
		timer.scheduleAtFixedRate(new StubHubEventFetcherTask(this), cal.getTime(), REFRESH_RATE);

				
	}

	public Collection<CreationEventHit> getAllCreatedEventHits() {
		return existingEventHits;
	}

	
	public static void removeFromExistingEvents(CreationEventHit eventHit,Integer admitoneId){
		CreationEventHit eventTobeDeleted=null;
		for (CreationEventHit hit: existingEventHits) {
			System.out.println(hit.getEventId() + ":" + eventHit.getEventId());
			if(hit.getEventId().equals(eventHit.getEventId())){
				eventTobeDeleted = hit;
				break;
			}
		}
		existingEventHits.remove(eventTobeDeleted);
		if(admitoneId!=null){	
			eventHit.setEventId(admitoneId);
			ticketNetworkEventHits.add(eventHit);
		}
	}
	
	public static void addInExistingEvents(CreationEventHit eventHit,Integer eventId){
		ticketNetworkEventHits.remove(eventHit);
//		if(eventId!=null){	
			eventHit.setEventId(eventId);
			existingEventHits.add(eventHit);
//		}
	}
	
	public Collection<CreationEventHit> getAllUncreatedEventHits() {
		List<CreationEventHit> result = new ArrayList<CreationEventHit>();
		for (CreationEventHit hit: stubhubEventHits) {
			if (!hit.isExists()) {
				result.add(hit);
			}
		}
		Collections.sort(result, new Comparator<CreationEventHit>() {

			public int compare(CreationEventHit c1, CreationEventHit c2) {
				if (c1.getDate() == null) {
					return 1;
				}
				
				if (c2.getDate() == null) {
					return -1;
				}
				
				return c1.getDate().compareTo(c2.getDate());
			}
			
		});
		
		return result;
	}
	
	public Collection<CreationEventHit> getAllCreatedEventHits(int numDaysSinceLastUpdate) {
		Date now = new Date();
		List<CreationEventHit> result = new ArrayList<CreationEventHit>();
		for (CreationEventHit hit: existingEventHits) {
			if (now.getTime() - hit.getLastUpdate().getTime() > 24 * 3600 * numDaysSinceLastUpdate) {
				result.add(hit);
			}
		}
		Collections.sort(result, new Comparator<CreationEventHit>() {

			public int compare(CreationEventHit c1, CreationEventHit c2) {
				if (c1.getLastUpdate() == null) {
					return 1;
				}
				
				if (c2.getLastUpdate() == null) {
					return -1;
				}
				
				return c1.getLastUpdate().compareTo(c2.getLastUpdate());
			}
			
		});
		
		return result;
	}	
	
	public void createEventsFromAutoResults(String creator, Collection<AutoEventListingResult> results,boolean includeExistingEvents,boolean includeStubhubEvents,boolean includeSeatwaveEvents,boolean includeTicketNetworkEvents,boolean includeTicketNetworkDataFeedEvents,boolean includeTicketNetworkPOSEvents,CreationType creationType) {
		Collection<AutoEventListingResult> eventsToBeCreated = new ArrayList<AutoEventListingResult>();
		Map<Integer, AutoEventListingResult> resultMap = new HashMap<Integer, AutoEventListingResult>();
		for (AutoEventListingResult result: results) {
			resultMap.put(result.getEventId(), result);

			if (!result.isExists()) {
				// create event if not exists
//				EventInfo eventInfo = new EventInfo(result.getEventId(), result.getName(), result.getTourName(),
//						result.getArtistName(), result.getDate(), result.getTime(), result.getTourType(),
//						result.getBuilding(), result.getCity(), result.getState(), result.getCountry(),result.getv);
//				eventInfo.setMessage("Event not created because it already exists.");
				eventsToBeCreated.add(result);
			} 
		}
		System.out.println(eventsToBeCreated + "Events to be created");
		EventCreator.createEvents(eventsToBeCreated,creationType);
		// for all successful events create crawls
		String eventIds="";
		String crawlIds="";
		try{
			for (EventInfo eventInfo: eventsToBeCreated) {
				if (!eventInfo.isSuccess()) {
					continue;
				}
				// create the crawls
				AutoEventListingResult result = resultMap.get(eventInfo.getEventId());
				if (result == null) {
					continue;
				}
				
				result.setMessage(eventInfo.getMessage());
				result.setErrorMessage(eventInfo.getErrorMessage());
//				result.setTourId(eventInfo.getTourId());
				result.setArtistId(eventInfo.getArtistId());
				result.setSystemEventId(eventInfo.getSystemEventId());
				Event event = DAORegistry.getEventDAO().getEventByAdmitoneId(Integer.valueOf(eventInfo.getEventId()));
				if (event == null) {
					eventInfo.setErrorMessage("Event not created because of error. Please contact the tech team with the info: id=" + eventInfo.getEventId() + " " + eventInfo.getName() + " " + eventInfo.getDate());
					logger.warn("*** " + eventInfo.getErrorMessage());
					eventInfo.setSuccess(false);
					continue;
				}
				
				eventIds+= event.getId()+",";
				String url = "http://www.ticketnetwork.com/tix/tix-" + event.getAdmitoneId() + ".aspx";
				TicketListingCrawl crawl = new TicketListingCrawl(new EventHit(eventInfo,Site.TICKET_NETWORK_DIRECT,url), event, creator);
				result.setOriginalId(result.getEventId());
				result.setEventId(event.getId());
				
//				result.setExists(true);
							
				crawl.setAutomaticCrawlFrequency(true);
					
				DAORegistry.getTicketListingCrawlDAO().save(crawl);
				logger.info("Crawl created with id ::" + crawl.getId());
				crawlIds = crawlIds+ "," + crawl.getId();
				result.getCrawlErrorMessageMap().put(crawl.getSiteId(), "");
				
			}
		}catch (Exception e) {
			logger.info("===Error while creating events..===");
			logger.info(e.fillInStackTrace().getMessage());
		}
		if(crawlIds!=null && !crawlIds.isEmpty()){
			crawlIds = crawlIds.substring(1);
			String url=sharedProperty.getBrowseUrl()+"EditorManageCrawlers?action=createCrawl&crawls=" + crawlIds;
			logger.info("url:" + url);
			try {
				/*HttpClient client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				HttpResponse response = client.execute(get);
				logger.info(EntityUtils.toString(response.getEntity()));*/
				SimpleHttpClient client = HttpClientStore.createHttpClient();
				HttpGet get = new HttpGet(url);
				System.out.println("TMAT CRL EXE: "+client.executeWithOneHit(get,true));
				
			} catch (Exception e) {
				logger.info("Error while communicating with tmat:"+url+" :ex : " +e.fillInStackTrace());
				e.printStackTrace();
			}
		}	
		updateEventList(results,includeExistingEvents,includeStubhubEvents,includeSeatwaveEvents,includeTicketNetworkEvents,includeTicketNetworkDataFeedEvents,includeTicketNetworkPOSEvents);
	}
	
//	public TicketListingCrawler getTicketListingCrawler() {
//		return ticketListingCrawler;
//	}
//
//	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
//		this.ticketListingCrawler = ticketListingCrawler;
//	}

	public static class StubHubEventFetcherTask extends TimerTask {
		private CreationEventListManager manager;
		private Date lastUpdate = new Date(new Date().getTime() - 3600000);

		private StubHubEventFetcherTask(CreationEventListManager manager) {
			this.manager = manager;
		}
		
		public synchronized void run() {
			Date now = new Date();
			if (now.getTime() - lastUpdate.getTime() < 300000) {
				return;
			}
			lastUpdate = now;
			try {
				countries=getTicketNetworkCountries();
				countryStates=getTicketNetworkStates();
				
				manager.refreshEventList();
				manager.createEventsFromFeed();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static class SeatWaveEventFetcherTask extends TimerTask {
//		private CreationEventListManager manager;
		private Date lastUpdate = new Date(new Date().getTime() - 3600000);

		public synchronized void run() {
			Date now = new Date();
			if (now.getTime() - lastUpdate.getTime() < 300000) {
				return;
			}
			lastUpdate = now;
			try {				
				SeatWaveEventPopulator.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	

	public static class EiMarketPlaceEventFetcherTask extends TimerTask {
		private Date lastUpdate = new Date(new Date().getTime() - 3600000);

		public synchronized void run() {
			Date now = new Date();
			if (now.getTime() - lastUpdate.getTime() < 300000) {
				return;
			}
			lastUpdate = now;
			try {				
				EiMarketPlaceEventPopulator.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	

	public Collection<String> getSitesPerEventMapByEventId(Integer eventId) {
		return sitesPerEventMap.get(eventId);
	}

	public Map<Integer, Collection<String>> getSitesPerEventMap() {
		return sitesPerEventMap;
	}
	public void setSitesPerEventMap(
			Map<Integer, Collection<String>> sitesPerEventMap) {
		this.sitesPerEventMap = sitesPerEventMap;
	}
	
	public void addSitesInSitesPerEventMap(Integer eventId,String site) {
		Collection<String> sites = this.sitesPerEventMap.get(eventId);
		if(sites==null){
			sites = new TreeSet<String>(new Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareTo(s2);
				}
			});
		}
		sites.add(site+"-1");
		sites.remove(site+"-0");
		this.sitesPerEventMap.put(eventId, sites);
	}
	
	public void disableSitesInSitesPerEventMap(Integer eventId,String site) {
		Collection<String> sites = this.sitesPerEventMap.get(eventId);
		if(sites==null){
			sites = new TreeSet<String>(new Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareTo(s2);
				}
			});
		}
		sites.remove(site+"-1");
		sites.add(site+"-0");
		this.sitesPerEventMap.put(eventId, sites);
	}
	public void deleteSitesFromSitesPerEventMap(Integer eventId,String site) {
		Collection<String> sites = this.sitesPerEventMap.get(eventId);
		if(sites==null){
			sites = new TreeSet<String>(new Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareTo(s2);
				}
			});
		}
		sites.remove(site+"-1");
		sites.remove(site+"-0");
		this.sitesPerEventMap.put(eventId, sites);
	}
	
	public Collection<CreationEventHit> getTicketNetworkEventHits() {
		return ticketNetworkEventHits;
	}

	public TicketNetworkWebServiceClient getTicketNetworkWebServiceClient() {
		return ticketNetworkWebServiceClient;
	}

	public void setTicketNetworkWebServiceClient(
			TicketNetworkWebServiceClient ticketNetworkWebServiceClient) {
		this.ticketNetworkWebServiceClient = ticketNetworkWebServiceClient;
	}

	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}

	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}

	/*public void setTicketNetworkEventHits(
			Collection<CreationEventHit> ticketNetworkEventHits) {
		this.ticketNetworkEventHits = ticketNetworkEventHits;
	}*/
}
