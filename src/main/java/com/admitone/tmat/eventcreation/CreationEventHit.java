package com.admitone.tmat.eventcreation;

import java.sql.Date;
import java.sql.Time;
import java.util.Collection;

import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.EventInfo;

public class CreationEventHit extends EventInfo {
	private Integer originalId;
	private java.util.Date creationDate = new java.util.Date();
	private boolean exists = false;
//	private Integer originalTourId;
	private Integer originalArtistId;
	private Integer originalVenueId;
//	private String originalTourName;
	private Collection<String> sites;
	private String url;
	private java.util.Date lastUpdate;
	private String siteId; // the site the events originates from. if local then set it to null
	
	public CreationEventHit(Integer eventId, /*Integer originalTourId,*/ Integer originalArtistId, Integer originalVenueId,
			String name,/* String tourName,*/ String artistName,
			Date date, Time time, TourType tourType, String building,
			String city, String state, String country,String zipcode, String url) {
		super(eventId, name, /*tourName,*/ artistName, date, time, tourType, building, city, state, country,zipcode);
		/*this.originalTourId = originalTourId;*/
		this.originalArtistId = originalArtistId;
		this.originalVenueId = originalVenueId;
		this.url = url;
	}
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public java.util.Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(java.util.Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Integer getOriginalVenueId() {
		return originalVenueId;
	}

	public void setOriginalVenueId(Integer originalVenueId) {
		this.originalVenueId = originalVenueId;
	}

	public java.util.Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(java.util.Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Integer getOriginalId() {
		return originalId;
	}

	public void setOriginalId(Integer originalId) {
		this.originalId = originalId;
	}

	public boolean isExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	/*public Integer getOriginalTourId() {
		return originalTourId;
	}

	public void setOriginalTourId(Integer originalTourId) {
		this.originalTourId = originalTourId;
	}*/

	public Integer getOriginalArtistId() {
		return originalArtistId;
	}

	public void setOriginalArtistId(Integer originalArtistId) {
		this.originalArtistId = originalArtistId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLocation() {
		return getBuilding() + ", " + getCity() + ", " + getState() + ", " + getCountry(); 
	}

	public Collection<String> getSites() {
		return sites;
	}

	public void setSites(Collection<String> sites) {
		this.sites = sites;
	}
	
	public int getSiteCount() {
		if (sites == null) {
			return 0;
		}
		return sites.size();
	}

	/*public String getOriginalTourName() {
		return originalTourName;
	}

	public void setOriginalTourName(String originalTourName) {
		this.originalTourName = originalTourName;
	}*/

	//	private String tourName;
//	private TourType tourType;
//	private String artistName;
//	private String tourId;
//	private String artistId;
//	private String venueId;
//	private String building;
//
//	public StubhubEventHit() {
//		super();
//	}
//
//	public StubhubEventHit(String id, String name, Date date, Time time, TourType tourType,
//			  String tourId, String tourName, String artistId, String artistName,
//			  String venueId, String building, String city, String state, String url) {
//		super(id, name, date, time, building + ", " + city + ", " + state, "stubhub", url);
//		this.setId(id);
//		this.setTourType(tourType);
//		this.setTourId(tourId);
//		this.setTourName(tourName);
//		this.setArtistId(artistId);
//		this.setArtistName(artistName);
//		this.setVenueId(venueId);
//		this.setBuilding(building);
//		this.setCity(city);
//		this.setState(state);
//	}
//
//	public String getVenueId() {
//		return venueId;
//	}
//
//	public void setVenueId(String venueId) {
//		this.venueId = venueId;
//	}
//
//	public String getBuilding() {
//		return building;
//	}
//
//	public void setBuilding(String building) {
//		this.building = building;
//	}
//
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getState() {
//		return state;
//	}
//
//	public void setState(String state) {
//		this.state = state;
//	}
//
//	private String city;
//	private String state;
//
//	public String getTourName() {
//		return tourName;
//	}
//
//	public void setTourName(String tourName) {
//		this.tourName = tourName;
//	}
//
//	public TourType getTourType() {
//		return tourType;
//	}
//
//	public void setTourType(TourType tourType) {
//		this.tourType = tourType;
//	}
//
//	public String getArtistName() {
//		return artistName;
//	}
//
//	public void setArtistName(String artistName) {
//		this.artistName = artistName;
//	}
//
//	public String getTourId() {
//		return tourId;
//	}
//
//	public void setTourId(String tourId) {
//		this.tourId = tourId;
//	}
//
//	public String getArtistId() {
//		return artistId;
//	}
//
//	public void setArtistId(String artistId) {
//		this.artistId = artistId;
//	}
//
}
