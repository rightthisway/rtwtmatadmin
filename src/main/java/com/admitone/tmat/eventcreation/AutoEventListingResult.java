package com.admitone.tmat.eventcreation;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.eventfetcher.EventHit;

/**
 * Container for all the event search on the ticket sites 
 * @author frad
 *
 */
public class AutoEventListingResult extends CreationEventHit {
	private Map<String, Collection<EventHit>> eventHitMap = new ConcurrentHashMap<String, Collection<EventHit>>();
	private String errorMessage;
	private Map<String, String> crawlErrorMessageMap = new HashMap<String, String>();
	private boolean enabled = true;
//	private Integer tourId;
	private Integer venueId;
	private VenueCategory venueCategory;
	private Integer artistId;
	private Integer systemEventId;

	public AutoEventListingResult(CreationEventHit eventHit,VenueCategory venueCategory) {
		this(eventHit.getEventId(), eventHit.getName(), eventHit.getDate(), eventHit.getTime(),
				   eventHit.getTourType(),/* eventHit.getOriginalTourId(), eventHit.getTourName(), */eventHit.getOriginalArtistId(), eventHit.getArtistName(), eventHit.getOriginalVenueId(),
				   eventHit.getBuilding(), eventHit.getCity(), eventHit.getState(), eventHit.getCountry(),eventHit.getZipcode(), eventHit.getUrl());
		this.setEventId(eventHit.getEventId());
		this.setOriginalId(eventHit.getOriginalId());
		this.setExists(eventHit.isExists());
		this.setVenueCategory(venueCategory);
	}

	public AutoEventListingResult(Integer id, String name, java.sql.Date date,
			Time time, TourType tourType, /*Integer tourId, String tourName,*/
			Integer artistId, String artistName, Integer venueId,
			String building, String city, String state, String country,String zipcode, String url) {
		super(id, artistId, venueId, name, artistName, date, time, tourType, 
				building, city, state, country,zipcode, url);
	}

	public void addEventHit(EventHit eventHit) {
		Collection<EventHit> eventHits = eventHitMap.get(eventHit.getSiteId());
		if (eventHits == null) {
			eventHits = new ArrayList<EventHit>();
			eventHitMap.put(eventHit.getSiteId(), eventHits);
		} else {
			this.enabled = false;
		}
		
		eventHits.add(eventHit);
	}
	
	public Map<String, Collection<EventHit>> getEventHitMap() {
		return eventHitMap;
	}

	public void setEventHitMap(Map<String, Collection<EventHit>> eventHitMap) {
		this.eventHitMap = eventHitMap;
	}
	
	public Collection<EventHit> getEventHitsBySiteId(String siteId) {
		return eventHitMap.get(siteId);
	}
	
	public Collection<EventHit> getAllEventHits() {
		Collection <EventHit> results = new ArrayList<EventHit>();
		for (Collection <EventHit> eventHits: eventHitMap.values()) {
			results.addAll(eventHits);
		}
		return results;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public Map<String, String> getCrawlErrorMessageMap() {
		return crawlErrorMessageMap;
	}

	public void setCrawlErrorMessageMap(Map<String, String> crawlErrorMessageMap) {
		this.crawlErrorMessageMap = crawlErrorMessageMap;
	}
	
	public Set<Entry<String, String>> getCrawlErrorMessageEntries() {
		return crawlErrorMessageMap.entrySet();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/*public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}*/

	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	public Integer getSystemEventId() {
		return systemEventId;
	}

	public void setSystemEventId(Integer systemEventId) {
		this.systemEventId = systemEventId;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public VenueCategory getVenueCategory() {
		return venueCategory;
	}

	public void setVenueCategory(VenueCategory venueCategory) {
		this.venueCategory = venueCategory;
	}
}
