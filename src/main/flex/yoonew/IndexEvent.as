package yoonew
{
	import flash.events.Event;

	public class IndexEvent extends Event {
	
		public var indexName:String;
		
		public function IndexEvent(type:String, indexName:String) {
			super(type);
			this.indexName = indexName;
		}
		
		public function getIndexName():String {
			return indexName;
		}
	}		
}