#!/bin/sh

if [ ! $# -eq 1 ]
then
  echo "Usage: $0 <port>"
  exit 0
fi

java -classpath tmat-crawler-launcher.jar com.admitone.tmat.launcher.TMATCrawlerLauncherServer $1
