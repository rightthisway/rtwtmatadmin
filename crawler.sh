#!/bin/sh

echo Running the crawler. Output redirected to crawler.log
java -Xmx512m -jar target/final_package/tmat.jar start > crawler.log
